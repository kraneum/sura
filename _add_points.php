<?php 
$email = $user_info['email'];

$pay_rate = getData('get_uic_pay_commission')['pay_commission'];
?>
<!--Modal -->
<div class = "modal fade" id = "pointsModal" tabindex = "-1" role = "dialog" aria-labelledby = "pointsLabel" aria-hidden = "true">
    <div class = "modal-dialog">
        <div class = "modal-content">
            <div class = "modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div style = "display: inline" >
                    <h4 class = "modal-title pull-left" id = "pointsLabel">Add Points</h4>
                    <div class = "pull-right" style="margin-right: 3em">
                        <label class = "radio-inline">
                            <input type = "radio" name = "pointsOptions" id = "pin" value = "pin"> Redeem Pin
                        </label>
                        <label class = "radio-inline">
                            <input type = "radio" name = "pointsOptions" id = "card" value = "card"> Credit/Debit Card
                        </label>

                    </div>
                </div>
            </div>
            
            <div class = "modal-body">
                
                <div id = "pinSection">
                    <div class="text-center">
                        <form id="redeemPinForm">
                            <h3 class="text-muted">Please Input Pin Code</h3>
                            <div id="infoPoints"></div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="form-group col-md-8">
                                    <input type="text" name="pinCode" class="form-control" size="16" maxlength="16" id="pinCode" placeholder="Enter Pin Code" data-parsley-required data-parsley-type="digits" data-parsley-length="[16, 16]" data-parsley-length-message="This value must be 16 digits"/>
                                </div>
                                <div id="tick-img" class="col-md-2" style="margin-top: 5%; padding-right: 5%; padding-left: 0%; display: none"><img src="img/accept_tick.png" alt=""/></div>
                            </div>
                            <div class="row">
                                <span class="col-md-6 small text-muted">By clicking "Top Up", you agree to our <a href = "#" >Terms</a> and <a href = "#" >Privacy Policy</a></span>
                                <div class="col-md-6 text-right">
                                    <input id="redeemPinBtn" type="submit" class="btn btn-md btn-success" value="Top Up"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                
                <div id="cardSection">
                    <form id="buyPointsForm">
                        <script src="https://js.paystack.co/v1/inline.js"></script>
                        <input type="hidden" id="pay_id" value="<?= $id?>"/>
                        <input type="hidden" id="pay_rate" value="<?= $pay_rate?>"/>
                        <div class="row">
                            <div class = "form-group col-md-6">
                                <label for = "pay_email" class = "control-label">Email</label>
                                <input class="form-control" readonly id="pay_email" name="pay_email" value="<?= $email ?>"/>
                            </div>
                            
                            <div class = "col-md-6 text-right">
                                <img src = "img/logos/mastercard.jpg" alt = "" width="25px" height="17px"/>
                                <img src = "img/logos/visa.jpg" alt = "" width="25px" height="17px"/>
                                <img src = "img/logos/verve.png" alt = "" width="35px" height="17px"/>
                                <img src = "img/logos/discover.jpg" alt = "" width="25px" height="17px"/>
                            </div>
                        </div>
                        <div class = "row">
                            <div class = "form-group col-md-6">
                                <label for = "pay_amt" class = "control-label">Number of iPoints</label>
                                <input  class="form-control" id="pay_amt" name="pay_amt" value="" data-parsley-required data-parsley-type="digits"/>
                                <span class="help-block text-success" >Charging amount: &#8358;<span>0</span></span>
                            </div>

                            
                        </div>

                        <div class="row">
                            <span class="col-md-6 small text-muted">By clicking "Top Up", you agree to our <a href = "#" >Terms</a> and <a href = "#" >Privacy Policy</a></span>
                            <div class="col-md-6 text-right">
                                <button type="submit" class="btn btn-md btn-success" >Top Up</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="pay_info" style="position: absolute;
    bottom: 1%;
    right: 2%;
    display: none;"></div>

<script type="text/javascript" src="js/moment.min.js"></script>


