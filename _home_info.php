<?php
require_once './php/suracommands/get_my_transactions.php';
require_once './php/suracommands/get_transaction_type_for_trans.php';
require_once './php/suracommands/get_my_policies.php';
?>
<div id="home-main" class="container">
    <div id="north" class="row">
        <div id="north-sidebar" class="col-sm-3">
            Live Overview
            <hr/>
            <div class="box-shadow--2dp content-white">
                <p class="text-center" style="margin-left: 10px;">Policies</p>
                <?php
                $data = runSimpleFetchQuery($con, ['id', 'policy_id'], 'user_signups', ['user_id'], ['='], [$id], '', '', '')['result'];
                ?>
                <span class="text-center center-block" style="margin-top: 20px;">
                    <?php
                    count($data) > 0 ? print('<span class="glyphicon glyphicon-triangle-top fa-2x" style="color: #33ff00"></span>') : "";
                    ?>
                    <h1 class="text-center center-block" style="display: inline; margin-left: 8px;">
                        <?php
                        echo count($data);
                        ?>
                    </h1>
                </span>

                <p class="text-center" style="margin-top: 20px;">Loyalty Points</p>

                <span class="text-center center-block" style="margin-top: 20px; margin-bottom: 20px;">
                    <?php
                    if ($LP > 0) {
                        if ($transactionType === 'credit') {
                            echo '<span class="glyphicon glyphicon-triangle-top fa-2x" style="color: #33ff00"></span>';
                        } else {
                            echo '<span class="glyphicon glyphicon-triangle-bottom fa-2x" style="color: #ff0000"></span>';
                        }
                    }
                    ?>
                    <h1 style="display: inline">&nbsp;<?php echo number_format($LP); ?></h1>
                </span>

            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="policyModal" tabindex="-1" role="dialog" aria-labelledby="policyLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div id="add-policy" class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add Policy</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row text-center">
                            <div class="col-sm-2"></div>
                            <div class="thumbnail col-sm-2 home-modal">
                                <p>
                                    <a href="addnewpolicy.php?type=auto"><img src="img/icons/auto.png" alt=""/></a>
                                <p>
                                <p class="caption" style="padding-top: 0">AUTO</p>
                            </div>

                            <div class="col-sm-1"></div>
                            <div class="thumbnail col-sm-3 home-modal" style="width: 17%;">
                                <p>
                                    <a href="addnewpolicy.php?type=health"><img src="img/icons/health.png" alt=""/></a>
                                </p>
                                <p class="caption">HEALTH</p>
                            </div>
                            <div class="col-sm-1"></div>

                            <div class="thumbnail col-sm-2 home-modal">
                                <p>
                                    <a href="addnewpolicy.php?type=life"><img src="img/icons/life.png" alt=""/></a>
                                </p>
                                <p class="caption">LIFE</p>
                            </div>
                            <div class="col-sm-2"></div>
                        </div>
                        <!--                        <div>
                                                    <a class="btn btn-block btn-primary" href="">Next</a>
                                                </div>-->
                    </div>
                </div>
            </div>
        </div>
        <div id="north-content" class="col-sm-8 col-sm-offset-1">
            <span>Policies <a class="pull-right" data-toggle="modal" data-target="#policyModal">Add Policy <strong style="font-size: 14px;">+</strong></a> </span>

            <hr/>
            <div class="box-shadow--2dp content-white">
                <?php
                $policies = get_my_policies();
                $count = 0;
                if (!empty($policies)) {
                    echo '<div class="table-condensed">';
                    echo '<table class="table table-striped">';
                    for ($i = count($policies) - 1; $i >= 0; $i--) {
                        if ($count == 4) {
                            break;
                        }
                        $policyID = $policies[$i]['policy_id'];
                        $signup_active = $policies[$i]['active'];
                        $insurerID = runSimpleFetchQuery($con, ['insurer_id'], 'policies', ['id'], ['='], [$policyID], '', '', '')['result'];
                        $insurer = runSimpleFetchQuery($con, ['name'], 'insurer', ['id'], ['='], [$insurerID], '', '', '')['result'];
                        echo '<tr>';
                        echo "<td class=\"pull-left\">$insurer</td>";
                        echo '<td class="pull-right">', ($signup_active == 1 ? '<span style="color:green">Active</span>' : '<span style="color:red">Inactive</span>'), '</td>';
                        echo '</tr>';
                        $count++;
                    }
                    echo '</table>';
                    echo '</div>';
                } else {
                    echo '<p style="padding: 50px;" class="text-center text-muted text-italic">No Policies Signed up for</p>';
                }
                ?>
            </div>
        </div>
    </div>

    <div id="south" class="row">
        <div id="south-sidebar" class="col-sm-3">
            Transaction
            <span class="pull-right" style="color: #cccccc;"><i class="fa fa-gear"></i></span>
            <hr/>
            <div class="box-shadow--2dp content-white">
                <?php
                $transactionInfo = get_my_transactions(7, 0);

                if (!empty($transactionInfo)) {
                    for ($i = 0; $i < count($transactionInfo); $i++) {
                        $type_id = $transactionInfo[$i]['type_id'];
                        $issuer = null;
                        if ($type_id == 5) {
                            $issuer = $transactionInfo[$i]['from_whom_info'][0]['phone'];
                        } else if ($type_id == 4) {
                            $issuer = $transactionInfo[$i]['to_who_info'][0]['phone'];
                        } else {
                            $issuer = $transactionInfo[$i]['from_whom_info'][0]['name'];
                        }
                        $amount = $transactionInfo[$i]['amount'];
                        $points = $transactionInfo[$i]['point_worth'];
                        $_date = (new DateTime($transactionInfo[$i]['date_transacted']))->format('j/n/Y');
                        $str = explode('/', $_date);
                        $date = $str[0] . ' ' . toMonth($str[1]) . ', ' . $str[2];
                        $tType = null;
                        if ($type_id == '1') {
                            $tType = '<span style="color:green;">POINTS TOP-UP</span>';
                        } else if ($type_id == '2') {
                            $tType = '<span style="color:red;">POLICY PURCHASE</span>';
                        } else if ($type_id == '3') {
                            $tType = '<span style="color:red;">POLICY RENEWAL</span>';
                        } else if ($type_id == '4') {
                            $tType = '<span style="color:red;">POINTS TRANSFER</span>';
                        } else if ($type_id == '5') {
                            $tType = '<span style="color:green;">POINTS RECEIVED</span>';
                        }

                        echo '<span style="font-size:14px;"><strong>' . $issuer . '</strong>' . '<span class="pull-right"><strong>' . $points . ' LP</strong></span></span><br>';
                        echo '<span style="font-size:11px;">' . 'Transaction type: ' . $tType . '</span><br>';
                        echo '<span class="text-muted">' . $date . '</span><br><br>';
                    }
                } else {
                    echo '<p style="padding: 50px;" class="text-center text-muted text-italic">No Transaction Performed</p>';
                }
                ?>
            </div>
        </div>
        <div id="south-content" class="col-sm-8 col-sm-offset-1">
            Recent Activity
            <hr/>
            <div id="recent_activity" class="box-shadow--2dp content-white">
                <p style="padding: 50px;" class="text-center text-muted text-italic">No Recent Activity</p>
            </div>
        </div>
    </div>
</div>
<?php
include './footer.php';
?>




<script src="js/jquery-3.1.1.js" type="text/javascript"></script>
<script src="js/jquery-ui.js" type="text/javascript"></script>
<script src="js/parsley.min.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
<script src="js/spin.min.js" type="text/javascript"></script>
<script src="js/spinner-config.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>

</body>
</html>
