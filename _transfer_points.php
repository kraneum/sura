
<!--Modal -->
<div class = "modal fade" id = "transferModal" tabindex = "-1" role = "dialog" aria-labelledby = "pointsLabel" aria-hidden = "true">
    <div class = "modal-dialog">
        <div class = "modal-content">
            <div class = "modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class = "modal-title pull-left" id = "pointsLabel">Transfer Points</h4>
            </div>

            <div class = "modal-body">
                <form id="transferForm" class="form-horizontal">
                    <h4 class="text-muted text-center">Enter Recipient Number and Amount</h4>
                    <div id="info"></div>
                    <div class="row">
                        <div class="col-sm-2"></div>
                        <div class = "form-group col-sm-8">
                            <label for = "phone" class = "control-label">Phone Number: </label>
                            <input type="text" name="phone" class="form-control" id="phone" placeholder="Enter phone number" data-parsley-required data-parsley-minlength="11" data-parsley-maxlength="11"  data-parsley-type="digits" data-parsley-minlength-message="This value should be 11 digits" data-parsley-maxlength-message="This value should be 11 digits" data-parsley-type-message="This value should contain digits only"/>
                        </div><div class="col-sm-2"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2"></div>
                        <div class = "form-group col-sm-8">
                            <label for = "amount" class = "control-label">Amount: </label>
                            <input type = "text" class = "form-control" name="amount" id = "amount" placeholder="Enter amount in iPoints" data-parsley-required data-parsley-type="digits" data-parsley-type-message="This value should contain digits only"/>
                        </div>
                        <div class="col-sm-2"></div>
                    </div>

                    <div class="row">
                        <div class="col-sm-2"></div>
                        <div class="form-group col-sm-8">
                            <label for="wallet">Select Wallet </label>
                            <select class="form-control" id="wallet" name="wallet"  data-parsley-required>
                                <option disabled selected></option>
                                <option value="health">Health</option>
                                <option value="general">General</option>
                            </select>
                        </div>
                        <div class="col-sm-2"></div>
                    </div>

                    <div class="row">
                        <span class="col-md-6 small text-muted">By clicking "Transfer", you agree to our <a href = "#" >Terms</a> and <a href = "#" >Privacy Policy</a></span>
                        <div class="col-md-6 text-right">
                            <input id="redeemPinBtn" type="submit" style="border-radius: 7px;" class="btn btn-md btn-success" value="Transfer"/>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>



