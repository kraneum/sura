<div id="profile-pic">
    <form id="profilePicForm" enctype="multipart/form-data">
        <div class="row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3" for="pic">Profile Pic <span class="text-muted text-italic">(JPEG / PNG format)</span> </label>
            <input class="col-sm-5 settings-input" id="pic" name="pic"  type="file" accept="image/jpeg, image/png" />
            <div class="col-sm-2">
                <button id="pic_save" type="submit" class="btn btn-success btn-xs box-shadow--2dp settings-btn btn-rounded pull-right" style="letter-spacing: 2px; padding: 7px; margin-top: 5px;" name="pic_save"><strong>SAVE</strong></button>
            </div>
            <div class="col-sm-1"></div>
        </div>

        <!--         This wraps the whole cropper 
                <div id="image-cropper">
                     This is where the preview image is displayed 
                    <div class="cropit-preview"></div>
        
                     This range input controls zoom 
                     You can add additional elements here, e.g. the image icons 
                    <input type="range" class="cropit-image-zoom-input" />
        
                     This is where user selects new image 
                    <input type="file" class="cropit-image-input" />
        
                </div>-->

    </form>
</div>

