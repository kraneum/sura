<?php
require_once('./header.php');
include_once './_add_points.php';
?>
<div id="home-main">


    <div class="container" style="padding-bottom: 0px; padding-top: 20px;">
        <div style="margin-bottom: 20px">
            <h4><strong><i class="fa fa-industry"></i> View enlisted policies</strong></h4>
            <small>&emsp;&emsp;You can view and add new insurance polices</small>
        </div>
        <div class="row">
            <a href="policies.php" class="btn btn-xs btn-default btn-rounded box-shadow--2dp" ><i class="fa fa-arrow-left"></i> Back to Policies</a>
            <span class="pull-right text-bold">Health iPoints: <?php echo $health_loyalty_points ?> | General iPoints: <?php echo $general_loyalty_points ?></span>
        </div>
    </div><br>
    <div id="add-new-policy-main" class="container box-shadow--4dp n_format" style="margin-bottom: 70px;">
        <div class="row" style="padding: 0 15px;">
            <div id="policy-links" class="col-sm-2">

                <h4 class="text-center"><strong>Select Policy Type</strong></h4>
                <div id="policy-link-icon" style="margin-top: 15%;" class="text-center">

                    <div id="healthLink" class="center-block policy-link" title="Health" data-typeid="<?php echo getData('get_insurance_type_id_from_meta', 'health'); ?>">
                        <span id="health" class="btn btn-success"><img src="img/icons/health.png" alt="health logo" height="70px" width="80px"></span>
                    </div>

                    <div id="generalLink" class="center-block policy-link" title="General" data-typeid="<?php echo getData('get_insurance_type_id_from_meta', 'general'); ?>">
                        <span id="auto" class="btn btn-success" style="margin-top: 20%"><i class="fa fa-car fa-5x"></i></span>
                    </div>

                </div>
            </div>
            <div id="content" class="col-sm-offset-1 col-sm-9">
                <h4 style="padding: 10px;"><strong id="content-header">Select Policy</strong></h4>
                <br/>

                <div id="info"></div>

                <div id="policiesView" data-typeid="" >

                </div>
                <div id="secondaryView" ></div>
                <div id="pagination" class="center-block"></div>
            </div>
        </div>

        <!-- Modal for confirm purchase -->
        <div class="modal fade" id="confirmPurchaseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>Confirm Purchase</h4>
                    </div>
                    <div class="modal-body">
                        This policy cost  <span id="price"></span> Loyalty Points. Having read the <a href="#">Terms</a> and <a href="#">Conditions</a>, do you wish to continue?
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default btn-rounded box-shadow--2dp" data-dismiss="modal">Cancel</button>
                        <button id="continue"  class="btn btn-success btn-rounded box-shadow--2dp" data-policyid = "" data-insurerid="" data-policytype="">Continue</button>
                    </div>
                </div>
            </div>
        </div> 

        <!-- Modal for more policy details -->
        <div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="text-center">Policy details</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <img id="logo" src="" alt="" class="text-center"/><br/><br/>
                            <strong>Company Name: </strong><span id="company"></span><br/>
                            <strong>Company Email: </strong><span id="email"></span><br/>
                            <strong>Company Phone: </strong><span id="phone"></span><br/>
                            <strong>Company Description: </strong><span id="desc"></span><br/>
                            <span id="company-more-info" class="small italic"></span>
                            <h4>Policy Information</h4>
                            <strong>Policy Name: </strong><span id="title"></span><br/>
                            <strong>Policy Description: </strong><span id="description"></span><br/>
                            <strong>Policy Price: </strong><span id="pric"></span><br/>
                            <strong>Policy Tenure: </strong><span id="tenure"></span><br/>
                            <span id="policy-more-info" class="small italic"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div> 

    </div>
</div>
<?php
include './footer.php';
?>

<script src="js/jquery-3.1.1.js" type="text/javascript"></script>
<script src="js/jquery.simplePagination.js" type="text/javascript"></script>
<script src="js/spin.min.js" type="text/javascript"></script>
<script src="js/spinner-config.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
</body>
</html>
