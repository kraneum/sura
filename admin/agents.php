<?php
session_name('UICIMA');
session_start();

if (!isset($_SESSION['id'])) {
    header("Location: login.php?goto=" . urlencode($_SERVER['REQUEST_URI']));
}

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';
require_once '../php/curl.php';
require_once './fragments/header.php';
?>
<link href="../css/bootcomplete.css" rel="stylesheet" type="text/css"/>
<link href="../css/simplePagination.css" rel="stylesheet" type="text/css"/>
<link href="../css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
<link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>

<div id="home-main" class="container">
    <div style="margin-bottom: 30px;">
        <h4><strong><i class="fa fa-users"></i> View Agents</strong></h4>
        <small>&emsp;&emsp;You can also approved and validate them</small>
    </div>
    
    <div id="primary">

        <div class="row" style="margin: 1% 0;">
            <div class="col-sm-2">
                <a href="export.php?entity=agent" target="_blank" id="export" class="btn btn-sm btn-success">Export Agents</a>
            </div>
            <div class="input-group col-sm-offset-10 col-sm-2">
                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
                <input id="search" type="text" class="form-control" placeholder="Search" aria-describedby="basic-addon1">
            </div>
        </div>

        <div class="table-responsive">

            <table class="table table-hover ">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>Last Name</th>
                        <th>Gender</th>
                        <th>Date of birth</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Date Joined</th>
                        <th>Last Login</th>
                        <th>Points</th>
                        <th>Approved</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>


    <div id="secondary" style="display: none"></div>

    <div id="pagination" style="display: none"></div>

    <div id="info"></div> <!-- for feedback purposes ...-->

</div>

<?php
require_once './fragments/footer.php';
?>
<script src="../js/jquery.bootcomplete.js" type="text/javascript"></script>
<script src="../js/jquery.simplePagination.js" type="text/javascript"></script>
<script src="../js/moment.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/working_alert.js"></script>
<script src="../js/parsley.min.js" type="text/javascript"></script>
<script src="views/js/admin_verification.js" type="text/javascript"></script>
<script src="views/js/_globals.js" type="text/javascript"></script>
<script type="text/javascript" src="views/js/agents.js"></script>

</body>
</html>