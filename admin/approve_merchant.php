<?php

session_name('UICIMA');
session_start();

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';
require_once '../php/user_permissions.php';

if ((!isset($_SESSION['id']) || !UserPermissions::hasPermission('UPDATE', +$_SESSION['user_permissions']))) {
    echo '{"status" : "false", "err":"NO_PERMISSION", "message":"You do not have the permission to perform this operation"}';
    exit();
} else {

    $con = makeConnection();
    autoCommit($con, false);

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                echo "{'status' : false, 'err' : 'VALIDATION_ERROR', 'message' : 'A validation error occurred'}";
            }

            $merchant_id = $_POST['merchantid'];
            $state = $_POST['state'];

            if ($state == 'true') {
                $state = '0';
            } else {
                $state = '1';
            }

            ////////////////////////////////////////////

            $merchantInfo = runSimpleFetchQuery($con, ['id', 'name'], 'merchants', ['id'], ['='], [$merchant_id], '', '', 1)['result'];
            if (empty($merchantInfo)) {
                disconnectConnection($con);
                echo "{'status' : false, 'err' : 'NO_SUCH_MERCHANT', 'message' : 'Enter a valid merchant ID'}";
                exit();
            }
            $merchant_name = $merchantInfo[0]['name'];

            $selfInfo = runSimpleFetchQuery($con, ['id', 'email'], 'admins', ['id'], ['='], [$_SESSION['id']], '', '', 1)['result'];
            if (empty($selfInfo)) {
                disconnectConnection($con);
                echo "{'status' : false, 'err' : 'NO_SUCH_ADMIN', 'message' : 'Enter correct admin login details'}";
                exit();
            }
            $self_email = $selfInfo[0]['email'];

            ////////////////////////////////////////////////

            $flag = true;
            $date = (new DateTime())->format(DateTime::ISO8601);

            do {
                
                $res = runSimpleUpdateQuery($con, 'merchants', ['deleted'], ["'$state'"], ['id'], ['='], [$merchant_id]);
                if ($res['err']['code']) {
                    $flag = false;
                    break;
                }

                $activity_id = null;
                $summary = $state == '0' ? 'approve_merchant' : 'disapprove_merchant';
                $res = runSimpleInsertQuery($con, 'admins_activities', ['admin', 'entity', 'summary', 'datetimezone'], 
                        ["'$self_email'", "'$merchant_name'", "'$summary'", "'$date'"]);
                if ($res['err']['code']) {
                    $flag = false;
                    break;
                } else {
                    $activity_id = $res['insertedId'];
                }

                $action = $state == '0' ? 'admin_approve' : 'admin_disapprove';
                $res = runSimpleInsertQuery($con, 'merchants_recent_activity', ['activity_summary', 'datetimezone', 'activity_type', 'activity_id', 'merchant_id'], 
                        ["'".json_encode(['admin'=>$self_email , 'merchant' => $merchant_name, 'summary' => $action])."'", "'$date'", "'UICI_ACTIONS'", "'$activity_id'", "'$merchant_id'"]);
                if ($res['err']['code']) {
                    $flag = false;
                }
                
            } while (false);
            /////////////////////////////////////////////

            if ($flag) {
                commit($con);
                $response['status'] = true;
                $response['message'] = $state == '0' ? 'Merchant approved successfully' : 'Merchant disapproved successfully';
            } else {
                rollback($con);
                $response['status'] = false;
                $response['err'] = 'ERROR_OCCURRED';
                $response['message'] = 'An error occurred. Please try again later';
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
            $response['message'] = 'The request was sent with a wrong type';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
        $response['message'] = 'Error connecting to database';
    }

    autoCommit($con, true);

    disconnectConnection($con);

    echo json_encode($response, JSON_PRETTY_PRINT);
}

