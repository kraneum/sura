<?php

session_name('UICIMA');
session_start();

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';
require_once '../php/user_permissions.php';
require_once('../php/form_validate.php');

if ((!isset($_SESSION['id']) || !UserPermissions::hasPermission('UPDATE', +$_SESSION['user_permissions']))) {
    echo '{"status" : "false", "err":"NO_PERMISSION", "message":"You do not have the permission to perform this operation"}';
} else {

    $con = makeConnection();
    autoCommit($con, false);

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                echo "['status' => false, 'err' => 'VALIDATION_ERROR', 'message' => 'A validation error occurred']";
            }

            $entity_id = $_POST['entityid'];
            $entity = $_POST['entity'];
            $current_name = $_POST['current_name'];
            $changed_name = $_POST['changed_name'];

            ////////////////////////////////////////////

            $validationResult = $form_validate([
                'entityid' => 'required|maxlength:100',
                'entity' => 'required',
                'current_name' => 'required|maxlength:100',
                'changed_name' => 'required|maxlength:100',
                    ], [
                'entityid' => $entity_id,
                'entity' => $entity,
                'current_name' => $current_name,
                'changed_name' => $changed_name,
            ]);

            if (!empty($validationResult)) {
                $response['status'] = false;
                $response['err'] = 'VALIDATION_ERROR';
                $response['errVal'] = $validationResult;
                $response['message'] = 'A validation error occurred';
                return $response;
            }

            ////////////////////////////////////////////

            $entityInfo = runSimpleFetchQuery($con, ['id', 'name'], $entity, ['id'], ['='], [$entity_id], '', '', 1)['result'];
            if (empty($entityInfo)) {
                disconnectConnection($con);
                return ['status' => false, 'err' => 'NO_SUCH_ENTITY', 'message' => 'Enter a valid ' . $entity . ' ID'];
            }
            $entity_name = $entityInfo[0]['name'];

            $selfInfo = runSimpleFetchQuery($con, ['id', 'email'], 'admins', ['id'], ['='], [$_SESSION['id']], '', '', 1)['result'];
            if (empty($selfInfo)) {
                disconnectConnection($con);
                return ['status' => false, 'err' => 'NO_SUCH_ADMIN', 'message' => 'Enter correct admin login details'];
            }
            $self_email = $selfInfo[0]['email'];

            ////////////////////////////////////////////////

            $flag = true;
            $date = (new DateTime())->format(DateTime::ISO8601);

            do {

                $res = runSimpleUpdateQuery($con, $entity, ['name', 'name_change'], ["'$changed_name'", "''"], ['id'], ['='], [$entity_id]);
                if ($res['err']['code']) {
                    $flag = false;
                    break;
                }


                $activity_id = null;
                $res = runSimpleInsertQuery($con, 'admins_activities', ['admin', 'entity', 'summary', 'details', 'datetimezone'], 
                        ["'$self_email'", "'$entity_name'", "'approve_name'", "'$changed_name'", "'$date'"]);
                if ($res['err']['code']) {
                    $flag = false;
                    break;
                } else {
                    $activity_id = $res['insertedId'];
                }

                if ($entity != 'merchants') {
                    $res = runSimpleInsertQuery($con, $entity . '_log', ['action', 'info', 'date', $entity . '_id'], 
                            ["'ADMIN_NAME_APPROVE'", "''", "'$date'", "'$entity_id'"]);
                } else {
                    $res = runSimpleInsertQuery($con, 'merchants_recent_activity', ['activity_summary', 'datetimezone', 'activity_type', 'activity_id', 'merchant_id'], 
                            ["'" . json_encode(['admin' => $self_email, 'merchant' => $entity_name, 'summary' => 'admin_approve_name_change']) . "'",
                        "'$date'", "'UICI_ACTIONS'", "'$activity_id'", "'$entity_id'"]);
                }
                if ($res['err']['code']) {
                    $flag = false;
                    break;
                }
            } while (false);

            /////////////////////////////////////////////

            if ($flag) {
                commit($con);
                $response['status'] = true;
                $response['message'] = ucfirst($entity) . ' name approved successfully';
            } else {
                rollback($con);
                $response['status'] = false;
                $response['err'] = 'ERROR_OCCURRED';
                $response['message'] = 'An error occurred. Please try again later';
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
            $response['message'] = 'The request was sent with a wrong type';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
        $response['message'] = 'Error connecting to database';
    }

    autoCommit($con, true);

    disconnectConnection($con);

    echo json_encode($response, JSON_PRETTY_PRINT);
}

