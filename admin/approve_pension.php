<?php

session_name('UICIMA');
session_start();

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';
require_once '../php/user_permissions.php';

if ((!isset($_SESSION['id']) || !UserPermissions::hasPermission('UPDATE', +$_SESSION['user_permissions']))) {
    echo '{"status" : "false", "err":"NO_PERMISSION", "message":"You do not have the permission to perform this operation"}';
} else {

    $con = makeConnection();
    autoCommit($con, false);

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                echo "['status' => false, 'err' => 'VALIDATION_ERROR', 'message' => 'A validation error occurred']";
            }

            $pension_id = $_POST['pensionid'];
            $state = $_POST['state'];

            ////////////////////////////////////////////

            $pensionInfo = runSimpleFetchQuery($con, ['id', 'name'], 'pension', ['id'], ['='], [$pension_id], '', '', 1)['result'];
            if (empty($pensionInfo)) {
                disconnectConnection($con);
                return ['status' => false, 'err' => 'NO_SUCH_PENSION_MANAGER', 'message' => 'Enter a valid pension ID'];
            }
            $pension_name = $pensionInfo[0]['name'];

            $selfInfo = runSimpleFetchQuery($con, ['id', 'email'], 'admins', ['id'], ['='], [$_SESSION['id']], '', '', 1)['result'];
            if (empty($selfInfo)) {
                disconnectConnection($con);
                return ['status' => false, 'err' => 'NO_SUCH_ADMIN', 'message' => 'Enter correct admin login details'];
            }
            $self_email = $selfInfo[0]['email'];

            ////////////////////////////////////////////////

            $flag = true;
            $date = (new DateTime())->format(DateTime::ISO8601);

            do {

                $res = runSimpleUpdateQuery($con, 'pension', ['approved'], ["'" . strtoupper($state) . "'"], ['id'], ['='], [$pension_id]);
                if ($res['err']['code']) {
                    $flag = false;
                    break;
                }
                
                $action = $state == 'true' ? 'ADMIN_APPROVE' : 'ADMIN_DISAPPROVE';
                $res = runSimpleInsertQuery($con, 'pension_log', ['action', 'info', 'date', 'pension_id'], 
                        ["'$action'", "''", "'$date'", "'$pension_id'"]);
                if ($res['err']['code']) {
                    $flag = false;
                    break;
                }

                $summary = $state == 'true' ? 'approve_pension' : 'disapprove_pension';
                $res = runSimpleInsertQuery($con, 'admins_activities', ['admin', 'entity', 'summary', 'datetimezone'], 
                        ["'$self_email'", "'$pension_name'", "'$summary'", "'$date'"]);
                if ($res['err']['code']) {
                    $flag = false;
                    break;
                }
            } while (false);

            /////////////////////////////////////////////

            if ($flag) {
                commit($con);
                $response['status'] = true;
                $response['message'] = $state == 'true' ? 'Pension manager approved successfully' : 'Pension manager disapproved successfully';
            } else {
                rollback($con);
                $response['status'] = false;
                $response['err'] = 'ERROR_OCCURRED';
                $response['message'] = 'An error occurred. Please try again later';
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
            $response['message'] = 'The request was sent with a wrong type';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
        $response['message'] = 'Error connecting to database';
    }

    autoCommit($con, true);

    disconnectConnection($con);

    echo json_encode($response, JSON_PRETTY_PRINT);
}

