<?php

session_name('UICIMA');
session_start();

require_once '../php/user_permissions.php';
require_once 'approve_merchant.php';

if ((!isset($_SESSION['id']) || !UserPermissions::hasPermission('UPDATE', +$_SESSION['user_permissions']))) {
    exit();
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = json_decode(file_get_contents("php://input"), JSON_OBJECT_AS_ARRAY);
    $merchantId = +filter_var($data['id'], FILTER_VALIDATE_INT);
    $opr = +filter_var($data['opr'], FILTER_VALIDATE_INT);
    $name = filter_var($data['name'], FILTER_SANITIZE_STRING);

    if ($merchantId && $opr !== null && $name) {
        $response = approveMerchant($merchantId, $opr, $name);
        
        if(!$response['err']){
            echo '{"status" : "SUCCESS"}';
        }else{
            echo '{"status" : "'.$response['err'].'"}';
        }
    }
}
