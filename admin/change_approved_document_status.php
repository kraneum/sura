<?php

session_name('UICIMA');
session_start();

require_once '../php/user_permissions.php';

if ((!isset($_SESSION['id']) || !UserPermissions::hasPermission('UPDATE', +$_SESSION['user_permissions']))) {
    exit();
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = json_decode(file_get_contents("php://input"), JSON_OBJECT_AS_ARRAY);
    $merchantId = +filter_var($data['id'], FILTER_VALIDATE_INT);
    $opr = +filter_var($data['opr'], FILTER_VALIDATE_INT);
    $docName = filter_var($data['docName'], FILTER_SANITIZE_STRING);
    $name = filter_var($data['name'], FILTER_SANITIZE_STRING);

    if ($merchantId && $opr !== null && $name && $docName) {
        //0-not uploaded and 2-pending are invalid operations
        if ($opr !== 0 && $opr !== 2) {
            require_once '../php/sura_config.php';
            require_once '../php/sura_functions.php';

            $con = makeConnection();

            $fetchDocumentsListResult = runSimpleFetchQuery(makeConnection(), ['documents'], 'merchants', ['id'], ['='], [$merchantId], null, null, 1);

            if (!$fetchDocumentsListResult['err']['code']) {
                $documentsList = json_decode($fetchDocumentsListResult['result'], JSON_OBJECT_AS_ARRAY);
                if (+$documentsList[$docName] !== $opr) {
                    $documentsList[$docName] = $opr;

                    $approveQueryResult = runSimpleUpdateQuery($con, 'merchants', ['documents'], ["'" . json_encode($documentsList) . "'"], ['id'], ['='], [$merchantId], 1);

                    if (!$approveQueryResult['err']['code']) {
                        if ($approveQueryResult['result']) {
                            //require_once '../php/kickout_admin.php';
                            require_once 'save_admin_actions.php';

                            //TODO: look for all admins of that merchant and kick them all out
                            //kickoutAdmin($con, +$data['id']);
                            //status is either 1-approved or 3-rejected
                            saveAdminActions($con, $opr === 1 ? 'approve_document' : 'reject_document', $_SESSION['email'], $name, ['document_name' => $docName, 'merchant_id'=>$merchantId]);

                            if ($opr === 3) {
                                require_once 'approve_merchant.php';

                                $response = approveMerchant($merchantId, 0, $name);

                                if (!$response['err'] || $response['err'] === 'MERCHANTNOTFOUNDORNOCHANGES') {
                                    echo '{"status" : "SUCCESS"}';
                                } else {
                                    echo '{"status" : "' . $response['err'] . '"}';
                                }
                            } else {
                                echo '{"status" : "SUCCESS"}';
                            }
                        } else {
                            echo '{"status" : "MERCHANTNOTFOUNDORNOCHANGES"}';
                        }
                    } else {
                        echo '{"status" : "DBERROR"}';
                    }
                } else {
                    echo '{"status" : "NOCHANGES"}';
                }
            } else {
                echo '{"status" : "DBERROR"}';
            }
        } else {
            echo '{"status" : "INVALIDOPERATION"}';
        }
    }
}
