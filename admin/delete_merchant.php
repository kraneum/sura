<?php
session_name('UICIMA');
session_start();

require_once '../php/user_permissions.php';

if ((!isset($_SESSION['id']) || !UserPermissions::hasPermission('UPDATE',+$_SESSION['user_permissions']))) {
    exit();
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = json_decode(file_get_contents("php://input"), JSON_OBJECT_AS_ARRAY);

    if (isset($data['id'])) {
        require_once '../php/sura_config.php';
        require_once '../php/sura_functions.php';

        $con = makeConnection();

        $deleteMerchantQueryResult = runSimpleUpdateQuery($con, 'merchants', ['deleted'], ["'1'"], ['id'], ['='], [+$data['id']], 1);

        if (!$deleteMerchantQueryResult['err']['code']) {
            if ($deleteMerchantQueryResult['result']) {
                //require_once '../php/kickout_admin.php';
                require_once 'save_admin_actions.php';

                //TODO: look for all admins of that merchant and kick them all out
                //kickoutAdmin($con, +$data['id']);
                saveAdminActions($con, 'delete_merchant', $_SESSION['email'], filter_var($data['name'], FILTER_SANITIZE_STRING));

                echo '{"status" : "SUCCESS"}';
            } else {
                echo '{"status" : "MERCHANTNOTFOUNDORNOCHANGES"}';
            }
        } else {
            echo '{"status" : "DBERROR"}';
        }
    } else {
        echo '{"status" : "INVALID_REQUEST_DATA"}';
    }
}