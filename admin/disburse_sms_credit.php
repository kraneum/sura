<?php

session_name('UICIMA');
session_start();

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';
require_once '../php/user_permissions.php';

if ((!isset($_SESSION['id']) || !UserPermissions::hasPermission('UPDATE', +$_SESSION['user_permissions']))) {
    echo '{"status" : "false", "err":"NO_PERMISSION", "message":"You do not have the permission to perform this operation"}';
} else {

    $con = makeConnection();
    autoCommit($con, false);

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                echo '{"status" : "false", "err" : "VALIDATION_ERROR", "message" : "A validation error occurred"}';
                exit();
            }

            $insurer_id = $_POST['insurerid'];
            $amount = $_POST['amount'];

            ////////////////////////////////////////////

            $insurerInfo = runSimpleFetchQuery($con, ['id', 'name', 'sms_wallet'], 'insurer', ['id'], ['='], [$insurer_id], '', '', 1)['result'];
            if (empty($insurerInfo)) {
                disconnectConnection($con);
                echo json_encode(['status' => false, 'err' => 'NO_SUCH_INSURER', 'message' => 'Enter a valid insurer ID']);
                exit();
            }
            $insurer_name = $insurerInfo[0]['name'];
            $sms_wallet = intval($insurerInfo[0]['sms_wallet']);
            
            $smsInfo = runSimpleFetchQuery($con, ['id', 'point_worth'], 'sms_transactions', ['insurer_id', 'status'], ['=', '='], [$insurer_id, "'PENDING'"], '', 'id DESC', 1)['result'];
            if (empty($smsInfo)) {
                disconnectConnection($con);
                echo json_encode(['status' => false, 'err' => 'NO_PENDING_TRANSACTIONS', 'message' => 'Operation failed due to absense of pending transactions']);
                exit();
            }
            $pending_trans_id = $smsInfo[0]['id'];
            $point_worth = $smsInfo[0]['point_worth'];
            
            if ($amount != $point_worth) {
                disconnectConnection($con);
                echo json_encode(['status' => false, 'err' => 'INVALID_TRANSACTION', 'message' => 'The last pending transaction has point worth of ' . $point_worth]);
                exit();
            }

            $selfInfo = runSimpleFetchQuery($con, ['id', 'email'], 'admins', ['id'], ['='], [$_SESSION['id']], '', '', 1)['result'];
            if (empty($selfInfo)) {
                disconnectConnection($con);
                echo json_encode(['status' => false, 'err' => 'NO_SUCH_ADMIN', 'message' => 'Enter correct admin login details']);
                exit();
            }
            $self_email = $selfInfo[0]['email'];

            ////////////////////////////////////////////////

            $flag = true;
            $ref = 'UICI-'.$insurer_id.round(microtime(true) * 1000);
            $date = (new DateTime())->format(DateTime::ISO8601);

            do {
                $new_sms_wallet = $sms_wallet + $amount;
                
                $res = runSimpleUpdateQuery($con, 'insurer', ['sms_wallet'], ["'$new_sms_wallet'"], ['id'], ['='], [$insurer_id]);
                if ($res['err']['code']) {
                    $flag = false;
                    break;
                }
                
                $res = runSimpleUpdateQuery($con, 'sms_transactions', ['status'], ["'COMPLETED'"], ['id'], ['='], [$pending_trans_id]);
                if ($res['err']['code']) {
                    $flag = false;
                    break;
                }

                $res = runSimpleInsertQuery($con, 'insurer_log', ['action', 'info', 'date', 'insurer_id'],
                        ["'RECEIVED_SMS_CREDIT'", "'$amount'", "'$date'", "'$insurer_id'"]);
                if ($res['err']['code']) {
                    $flag = false;
                    break;
                }

                $res = runSimpleInsertQuery($con, 'admins_activities', ['admin', 'entity', 'summary', 'details', 'datetimezone'], 
                        ["'$self_email'", "'$insurer_name'", "'disbursed_sms_credit'", "'$amount'", "'$date'"]);
                if ($res['err']['code']) {
                    $flag = false;
                    break;
                }
            } while (false);

            /////////////////////////////////////////////

            if ($flag) {
                commit($con);
                $response['status'] = true;
                $response['message'] = 'SMS credit disburse successfully';
                $response['sms_wallet'] = $new_sms_wallet;
            } else {
                rollback($con);
                $response['status'] = false;
                $response['err'] = 'ERROR_OCCURRED';
                $response['message'] = 'An error occurred. Please try again later';
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
            $response['message'] = 'The request was sent with a wrong type';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
        $response['message'] = 'Error connecting to database';
    }

    autoCommit($con, true);

    disconnectConnection($con);

    echo json_encode($response, JSON_PRETTY_PRINT);
}

