<?php

session_name('UICIMA');
session_start();

if (!isset($_SESSION['id'])) {
    header("Location: login.php?goto=" . urlencode($_SERVER['REQUEST_URI']));
}

require_once '../php/user_permissions.php';

if (!UserPermissions::isSuperuserPermission(+$_SESSION['user_permissions'])) {
    header("Location: login.php?id=1");
}

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';
require_once '../php/parse_user_permission.php';

$operationStatus = ['err' => false];
$getId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT, ['min_range' => 1]);
$id = $getId ? $getId : filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT, ['min_range' => 1]);
$employee = null;

$con = makeConnection();

if (isset($_POST['btn_register']) && $id) {
    //We dnt want admins to edit other admins, but we've prevented that when u first got to the edit_employee page, so it might be unnecessary here
    //if (!UserPermissions::isSuperuserPermission(intval($employee['user_permissions'])) || (+$id === +$_SESSION['id'])) {}else{header("Location: login.php?id=3");}
    require_once '../php/form_validate.php';

    $employee = unserialize($_POST['employee']);

    // clean user inputs to prevent sql injections
    $cleanedUserInputMap = array_map(function($value) {
        return htmlspecialchars(strip_tags(trim($_POST[$value])));
    }, ['first_name' => 'first_name', 'middle_name' => 'middle_name', 'last_name' => 'last_name', 'dob' => 'dob', 'phone' => 'phone', 'country' => 'country', 'password' => 'password', 'password_confirm' => 'password_confirm', 'security_question' => 'security_question', 'security_answer' => 'security_answer']);

    $validationResult = $form_validate([
        'first_name' => 'maxlength:100',
        'last_name' => 'maxlength:100',
        'dob' => 'dob|maxlength:10',
        'phone' => 'phone|maxlength:50',
        'country' => 'maxlength:60|alphabets',
        'security_question' => 'maxlength:200',
        'security_answer' => 'maxlength:100',
        'password' => 'maxlength:20',
        'password_confirm' => 'equalsToField:password|maxlength:20'
            ], $cleanedUserInputMap);

    if (empty($validationResult)) {
        $validationResult = $form_validate([
            'display_picture' => 'filemaxmegabytes:2|filemimetypes:image/jpeg,image/png|imagedimensions:min_width=100,min_height=100'
                ], ['display_picture' => $_FILES['display_picture']]);

        if (empty($validationResult)) {
            require_once '../php/if_value_same_as_default_unset.php';

            if ($_FILES["display_picture"]["tmp_name"]) {
                $displayPictureFileName = $employee['email'] . '.' . str_replace('jpeg', 'jpg', substr(getimagesize($_FILES["display_picture"]["tmp_name"])["mime"], 6));

                if (move_uploaded_file($_FILES["display_picture"]["tmp_name"], '../img/admins/' . $displayPictureFileName)) {
                    //edit the editable
                    if (+$id === +$_SESSION['id']) {
                        $operationStatus['display_picture_changed'] = true;
                        $_SESSION['display_picture_file_name'] = $displayPictureFileName;
                    }

                    $employee['display_picture_file_name'] !== $displayPictureFileName && $cleanedUserInputMap['display_picture_file_name'] = $displayPictureFileName;
                } else {
                    $operationStatus['err'] = true;
                }
            }

            if (!$operationStatus['err']) {
                require_once 'save_admin_actions.php';

                $ifValueSameAsDefaultUnset($cleanedUserInputMap['country'], $employee['country']);
                $ifValueSameAsDefaultUnset($cleanedUserInputMap['security_question'], $employee['security_question']);

                if (empty($employee['SUPER'])) {
                    $postUserPermissions = isset($_POST['user_permissions']) ? $_POST['user_permissions'] : [];
                    ($postUserPermissions !== $employee['user_permissions']) && (
                            $cleanedUserInputMap['user_permissions'] = $postUserPermissions ? array_reduce($postUserPermissions, function($userPermission, $permission) {
                                return UserPermissions::grantUserPermission($permission, $userPermission);
                            }, 0) : 0);
                }

                unset($cleanedUserInputMap['password_confirm']);

                //unset($cleanedUserInputMap);
                $filteredCleanedUserInputMap = [];
                foreach ($cleanedUserInputMap as $key => $value) {
                    if ($key == 'password') {
                        strlen($value) && ($employee[$key] !== $value) && ($filteredCleanedUserInputMap[$key] = password_hash($value, PASSWORD_BCRYPT));
                        continue;
                    }
                    strlen($value) && ($employee[$key] !== $value) && ($filteredCleanedUserInputMap[$key] = $value);
                }

                if (!empty($filteredCleanedUserInputMap)) {
                    $fields = array_keys($filteredCleanedUserInputMap);
                    $values = array_map(function($value) {
                        return '"' . $value . '"';
                    }, array_values($filteredCleanedUserInputMap));

                    $updateResult = runSimpleUpdateQuery($con, 'admins', $fields, $values, ['id'], ['='], [$id], 1);
                    if (!$updateResult['err']['code']) {
                        $changes = $filteredCleanedUserInputMap;
                        isset($changes['password']) && $changes['password'] = null;
                        $_FILES["display_picture"]["tmp_name"] && $changes['display_picture_file_name'] = null;

                        saveAdminActions($con, 'edit_employee', $_SESSION['email'], $employee['email'], $changes);

                        foreach ($filteredCleanedUserInputMap as $key => $value) {
                            $employee[$key] = $value;
                        }

                        isset($cleanedUserInputMap['user_permissions']) && ($employee['user_permissions'] = $parseUserPermission($employee['user_permissions'] ? $employee['user_permissions'] : 0, $employee));

                        if (+$id !== +$_SESSION['id']) {
//                            require_once 'kickout_employee.php';
//                            $kickoutEmployee($con, +$id);
                        } else {
                            //Edit the editables
                            isset($cleanedUserInputMap['email']) && $_SESSION['email'] = $cleanedUserInputMap['email'];
                            isset($cleanedUserInputMap['user_permissions']) && $_SESSION['user_permissions'] = $cleanedUserInputMap['user_permissions'];
                        }
                    } else {
                        $operationStatus['err'] = true;
                        $operationStatus['type'] = 'updateError';
                        $operationStatus['message'] = 'Something went wrong, please retry';
                    }
                } else {
                    if (!$_FILES["display_picture"]["tmp_name"]) {
                        $operationStatus['err'] = true;
                        $operationStatus['type'] = 'noInputError';
                        $operationStatus['message'] = 'There were no changes';
                    } else {
                        saveAdminActions($con, 'edit_employee', $_SESSION['email'], $employee['email'], ['display_picture_file_name' => null]);
                    }
                }
            } else {
                $operationStatus['type'] = 'imageUploadError';
                $operationStatus['message'] = 'There was an problem uploading your display picture, please retry';
            }
        } else {
            $operationStatus['err'] = true;
            $operationStatus = array_merge($validationResult, ['type' => 'validationError']);
        }
    } else {
        $operationStatus['err'] = true;
        $operationStatus = array_merge($validationResult, ['type' => 'validationError']);
    }

    require_once './views/edit_employee.php';
} else if ($id) {
    $fetchEmployeeResult = runSimpleFetchQuery($con, ['first_name', 'middle_name', 'last_name', 'dob', 'phone', 'email', 'country', 'password', 'security_question', 'security_answer', 'user_permissions', 'display_picture_file_name'], 'admins', ['id'], ['=', '='], [$id], '', '', 1);
    if (!$fetchEmployeeResult['err']['code']) {
        $employee = $fetchEmployeeResult['result'][0];
        $employee['email'] = strtolower($employee['email']);
        $security_questionInArray = false;
        $employee['security_questions'] = array_map(function($value)use($employee, &$security_questionInArray) {
            !$security_questionInArray && ($value['question'] === $employee['security_question']) && ($security_questionInArray = true);
            return $value['question'];
        }, runSimpleFetchQuery($con, ['question'], 'security_questions', [], [], [], '', '', '')['result']);
        !$security_questionInArray && ($employee['security_questions'][] = $employee['security_question']);
        //unset($security_questionInArray);
        ($employee['user_permissions'] = $parseUserPermission($employee['user_permissions'], $employee));
        //unset($permissions);
        //unset($userPermissions);

        require_once './views/edit_employee.php';
    } else {
        //Display, an error occured, pls reload
    }
} else {
    header("Location: home.php");
}