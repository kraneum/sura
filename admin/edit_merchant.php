<?php

session_name('UICIMA');
session_start();

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';

require_once('../php/form_validate.php');

require_once '../php/user_permissions.php';

if ((!isset($_SESSION['id']) || !UserPermissions::hasPermission('UPDATE', +$_SESSION['user_permissions']))) {
    echo '{"status" : "false", "err":"NO_PERMISSION", "message":"You do not have the permission to perform this operation"}';
} else {

    $con = makeConnection();
    autoCommit($con, false);

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                disconnectConnection($con);
                echo "{'status' => false, 'err' => 'VALIDATION_ERROR', 'message' => 'A validation error occurred'}";
                exit();
            }

            $merchant_id = $_POST['id'];
            $name = $_POST['name'];
            $desc = $_POST['desc'];
            $phone = $_POST['phone'];
            $email = $_POST['email'];
            $rc = $_POST['rc'];
            $logo = file_exists($_FILES['logo']['tmp_name']) || is_uploaded_file($_FILES['logo']['tmp_name']) ? $_FILES['logo'] : null;

            ////////////////////////////////////////////

            $merchantInfo = runSimpleFetchQuery($con, ['id', 'name'], 'merchants', ['id'], ['='], [$merchant_id], '', '', 1)['result'];
            if (empty($merchantInfo)) {
                disconnectConnection($con);
                echo "{status' : false, 'err' : 'NO_SUCH_MERCHANT', 'message' : 'Enter a valid merchant ID'}";
                exit();
            }
            $merchant_name = $merchantInfo[0]['name'];

            $selfInfo = runSimpleFetchQuery($con, ['id', 'email'], 'admins', ['id'], ['='], [$_SESSION['id']], '', '', 1)['result'];
            if (empty($selfInfo)) {
                disconnectConnection($con);
                echo "{'status' : false, 'err' : 'NO_SUCH_ADMIN', 'message' : 'Enter correct admin login details'}";
                exit();
            }
            $self_email = $selfInfo[0]['email'];

            ////////////////////////////////////////////////

            $validationResult = null;

            $validationResult = $form_validate([
                'name' => 'required|maxlength:100',
                'desc' => 'required|maxlength:200',
                'phone' => 'required|maxlength:11',
                'email' => 'required|maxlength:100',
                'rc' => 'required|maxlength:20',
                    ], [
                'name' => $name,
                'desc' => $desc,
                'phone' => $phone,
                'email' => $email,
                'rc' => $rc,
            ]);

            if ($logo) {
                if (!$logo["tmp_name"]) {
                    echo "{'status' : false, 'err' : 'FILE_ERROR', 'message' : 'This file is corrupt'}";
                    exit();
                }
                $picValidationResult = $form_validate([
                    'logo' => 'filemaxmegabytes:2|filemimetypes:image/jpeg,image/png|imagedimensions:min_width=100,min_height=100'
                        ], [
                    'logo' => $logo
                ]);
                $validationResult = array_merge($validationResult, $picValidationResult);
            }

            if (!empty($validationResult)) {
                $response['status'] = false;
                $response['err'] = 'VALIDATION_ERROR';
                $response['errVal'] = $validationResult;
                $response['message'] = 'A validation error occurred';
            } else {
                if ($logo) { // if logo was sent ...
                    $uploaddir = '../img/merchants/';
                    $logo_name = 'logo_merchant_id_' . $merchant_id . '.' . pathinfo(basename($logo["name"]), PATHINFO_EXTENSION);
                    $uploadfile = $uploaddir . $logo_name;

                    if (file_exists($uploadfile)) {
                        unlink($uploadfile);
                    }

                    if (!(move_uploaded_file($logo["tmp_name"], $uploadfile))) {
                        echo "{'status' : false, 'err' : 'UPLOAD_ERROR', 'message' : 'Error occurred during the file upload'}";
                        exit();
                    }
                }


                $flag = true;
                $date = (new DateTime())->format(DateTime::ISO8601);

                do {
                    $res = null;

                    if ($logo) {
                        $res = runSimpleUpdateQuery($con, "merchants", ['name', 'write_up', 'contact_email', 'contact_phone', 'rc_number', 'display_picture'], 
                                ["'$name'", "'$desc'", "'$email'", "'$phone'", "'$rc'", "'$logo_name'"], ['id'], ['='], [$merchant_id]);
                    } else {
                        $res = runSimpleUpdateQuery($con, "merchants", ['name', 'write_up', 'contact_email', 'contact_phone', 'rc_number'], 
                                ["'$name'", "'$desc'", "'$email'", "'$phone'", "'$rc'"], ['id'], ['='], [$merchant_id]);
                    }
                    if ($res['err']['code']) {
                        $flag = false;
                        break;
                    }

                    $activity_id = null;
                    $res = runSimpleInsertQuery($con, 'admins_activities', ['admin', 'entity', 'summary', 'datetimezone'], 
                            ["'$self_email'", "'$merchant_name'", "'edit_merchant'", "'$date'"]);
                    if ($res['err']['code']) {
                        $flag = false;
                        break;
                    } else {
                        $activity_id = $res['insertedId'];
                    }

                    $res = runSimpleInsertQuery($con, 'merchants_recent_activity', ['activity_summary', 'datetimezone', 'activity_type', 'activity_id', 'merchant_id'], 
                            ["'" . json_encode(['admin' => $self_email, 'merchant' => $merchant_name, 'summary' => 'admin_edit']) . "'",
                                "'$date'", "'UICI_ACTIONS'", "'$activity_id'", "'$merchant_id'"]);
                    if ($res['err']['code']) {
                        $flag = false;
                        break;
                    }
                } while (false);

                if ($flag) {
                    commit($con);
                    $response['status'] = true;
                } else {
                    rollback($con);
                    $response['status'] = false;
                    $response['err'] = 'FAILURE';
                    $response['message'] = 'Error inserting into the database';
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
            $response['message'] = 'Request was sent with the wrong request type (Use POST method)';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
        $response['message'] = 'Error connecting into the database';
    }

    autoCommit($con, true);

    disconnectConnection($con);

    echo json_encode($response, JSON_PRETTY_PRINT);
}