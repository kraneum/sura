<?php

session_name('UICIMA');
session_start();

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';

require_once('../php/form_validate.php');

require_once '../php/user_permissions.php';

if ((!isset($_SESSION['id']) || !UserPermissions::hasPermission('UPDATE', +$_SESSION['user_permissions']))) {
    echo '{"status" : "false", "err":"NO_PERMISSION", "message":"You do not have the permission to perform this operation"}';
} else {

    $con = makeConnection();
    autoCommit($con, false);

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                echo "{'status' => false, 'err' => 'VALIDATION_ERROR', 'message' => 'A validation error occurred'}";
            }

            $store_id = $_POST['id'];
            $name = $_POST['name'];
            $desc = $_POST['desc'];
            $phone = $_POST['phone'];
            $email = $_POST['email'];
            $account_no = $_POST['account_no'];

            $api_key = null;
            $password = null;

            if ($store_id == 'null') {
                $password = random_str(10);
                $api_key = getGUID();
            }

            ////////////////////////////////////////////
            $store_name = $name;

            if ($store_id != 'null') {
                $storeInfo = runSimpleFetchQuery($con, ['id', 'name'], 'e_commerce', ['id'], ['='], [$store_id], '', '', 1)['result'];
                if (empty($storeInfo)) {
                    disconnectConnection($con);
                    echo "{status' : false, 'err' : 'NO_SUCH_STORE', 'message' : 'Enter a valid store ID'}";
                    exit();
                }
            }

            $selfInfo = runSimpleFetchQuery($con, ['id', 'email'], 'admins', ['id'], ['='], [$_SESSION['id']], '', '', 1)['result'];
            if (empty($selfInfo)) {
                disconnectConnection($con);
                echo "{'status' : false, 'err' : 'NO_SUCH_ADMIN', 'message' : 'Enter correct admin login details'}";
                exit();
            }
            $self_email = $selfInfo[0]['email'];

            ////////////////////////////////////////////////

            $validationResult = null;

            $validationResult = $form_validate([
                'name' => 'required|maxlength:100',
                'desc' => 'required|maxlength:200',
                'phone' => 'required|maxlength:11',
                'email' => 'required|maxlength:100',
                'account_no' => 'required|maxlength:10'
                    ], [
                'name' => $name,
                'desc' => $desc,
                'phone' => $phone,
                'email' => $email,
                'account_no' => $account_no
            ]);


            if (!empty($validationResult)) {
                $response['status'] = false;
                $response['err'] = 'VALIDATION_ERROR';
                $response['errVal'] = $validationResult;
                $response['message'] = 'A validation error occurred';
            } else {

                $flag = true;
                $date = (new DateTime())->format(DateTime::ISO8601);

                do {
                    $res = null;

                    if ($store_id == 'null') {
                        $res = runSimpleInsertQuery($con, "e_commerce", ['name', 'write_up', 'email', 'phone', 'account_no', 'password', 'api_key', 'date_joined'], ["'$name'", "'$desc'", "'$email'", "'$phone'", "'$account_no'", "'$password'", "'$api_key'", "'$date'"]);
                        if ($res['err']['code']) {
                            $flag = false;
                            break;
                        }
                        $insertedID = $res['insertedId'];
                        
                        $res = runSimpleInsertQuery($con, 'e_commerce_log', ['action', 'info', 'date', 'e_commerce_id'], ["'ADMIN_REGISTER'", "''", "'$date'", "'$insertedID'"]);
                        if ($res['err']['code']) {
                            $flag = false;
                            break;
                        }

                        $res = runSimpleInsertQuery($con, 'admins_activities', ['admin', 'entity', 'summary', 'datetimezone'], ["'$self_email'", "'$store_name'", "'register_store'", "'$date'"]);
                        if ($res['err']['code']) {
                            $flag = false;
                            break;
                        }
                    } else {
                        $res = runSimpleUpdateQuery($con, "e_commerce", ['name', 'write_up', 'email', 'phone', 'account_no'], ["'$name'", "'$desc'", "'$email'", "'$phone'", "'$account_no'"], ['id'], ['='], [$store_id]);

                        if ($res['err']['code']) {
                            $flag = false;
                            break;
                        }

                        $res = runSimpleInsertQuery($con, 'e_commerce_log', ['action', 'info', 'date', 'e_commerce_id'], ["'ADMIN_EDIT'", "''", "'$date'", "'$store_id'"]);
                        if ($res['err']['code']) {
                            $flag = false;
                            break;
                        }

                        $res = runSimpleInsertQuery($con, 'admins_activities', ['admin', 'entity', 'summary', 'datetimezone'], ["'$self_email'", "'$store_name'", "'EDIT_STORE'", "'$date'"]);
                        if ($res['err']['code']) {
                            $flag = false;
                            break;
                        }
                    }
                } while (false);

                if ($flag) {
                    commit($con);
                    $response['status'] = true;
                    if ($store_id == 'null') {
                        require 'send_mail.php';
                        $subject = 'Registration Successful';
                        $message = '<html><body style="padding: 0 20%; font-size:14px"><div><h3>Hi ' . $name . ', </h3>'
                                . 'Thanks for registrating your store on UICI platform.<br><br>'
                                . '<div style="text-align:center; margin:30px 0;">'
                                . 'Your password is <strong>' . $password . '</strong>'
                                . '</div>'
                                . '<br><br><div style="font-size: .8em">If you’ve received this email by mistake, simply ignore it.'
                                . 'Your account won\'t be created if you don\'t click the confirmation link above.'
                                . '<br><br>Thanks for your loyalty.</div></div></html></body>';

                        send_mail($email, $subject, $message);
                    }
                } else {
                    rollback($con);
                    $response['status'] = false;
                    $response['err'] = 'FAILURE';
                    $response['message'] = 'Error inserting into the database';
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
            $response['message'] = 'Request was sent with the wrong request type (Use POST method)';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
        $response['message'] = 'Error connecting into the database';
    }

    autoCommit($con, true);

    disconnectConnection($con);

    echo json_encode($response, JSON_PRETTY_PRINT);
}

function random_str($length, $keyspace = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
 . '0123456789`-=~!@#$%^&*()_+,./<>?;:[]{}\|'
) {
    $str = '';
    $max = mb_strlen($keyspace, '8bit') - 1;
    if ($max < 1) {
        throw new Exception('$keyspace must be at least two characters long');
    }
    for ($i = 0; $i < $length; ++$i) {
        $str .= $keyspace[random_int(0, $max)];
    }
    return $str;
}

function getGUID() {
    if (function_exists('com_create_guid')) {
        return str_replace(['{', '}', '-'], '', com_create_guid());
    } else {
        mt_srand((double) microtime() * 10000); //optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $uuid = substr($charid, 0, 8)
                . substr($charid, 8, 4)
                . substr($charid, 12, 4)
                . substr($charid, 16, 4)
                . substr($charid, 20, 12);
        return $uuid;
    }
}
