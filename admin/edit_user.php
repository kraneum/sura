<?php

session_name('UICIMA');
session_start();

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';

require_once('../php/form_validate.php');

require_once '../php/user_permissions.php';

if ((!isset($_SESSION['id']) || !UserPermissions::hasPermission('UPDATE', +$_SESSION['user_permissions']))) {
    echo '{"status" : "false", "err":"NO_PERMISSION", "message":"You do not have the permission to perform this operation"}';
} else {

    $con = makeConnection();
    autoCommit($con, false);

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                echo "{'status' => false, 'err' => 'VALIDATION_ERROR', 'message' => 'A validation error occurred'}";
            }

            $user_id = $_POST['id'];
            $fname = $_POST['fname'];
            $midname = $_POST['mname'];
            $lname = $_POST['lname'];
            $gender = $_POST['gender'];
            $dob = $_POST['dob'];
            $phone = $_POST['phone'];
            $email = $_POST['email'];
            $pic = file_exists($_FILES['pic']['tmp_name']) || is_uploaded_file($_FILES['pic']['tmp_name']) ? $_FILES['pic'] : null;

            ////////////////////////////////////////////

            $userInfo = runSimpleFetchQuery($con, ['id', 'f_name', 'l_name'], 'users', ['id'], ['='], [$user_id], '', '', 1)['result'];
            if (empty($userInfo)) {
                disconnectConnection($con);
                echo "{status' : false, 'err' : 'NO_SUCH_USER', 'message' : 'Enter a valid user ID'}";
                exit();
            }
            $user_name = $userInfo[0]['f_name'] . ' ' . $userInfo[0]['l_name'];

            $selfInfo = runSimpleFetchQuery($con, ['id', 'email'], 'admins', ['id'], ['='], [$_SESSION['id']], '', '', 1)['result'];
            if (empty($selfInfo)) {
                disconnectConnection($con);
                echo "{'status' : false, 'err' : 'NO_SUCH_ADMIN', 'message' : 'Enter correct admin login details'}";
                exit();
            }
            $self_email = $selfInfo[0]['email'];

            ////////////////////////////////////////////////

            $validationResult = null;

            $validationResult = $form_validate([
                'fname' => 'required|maxlength:100',
                'midname' => 'maxlength:100',
                'lname' => 'required|maxlength:100',
                'gender' => 'required|maxlength:100',
                'dob' => 'required|dob|maxlength:100',
                'phone' => 'required|phone|maxlength:11',
                'email' => 'required|email|maxlength:100'
                    ], [
                'fname' => $fname,
                'midname' => $midname,
                'lname' => $lname,
                'gender' => $gender,
                'dob' => $dob,
                'phone' => $phone,
                'email' => $email
            ]);

            if ($pic) {
                if (!$pic["tmp_name"]) {
                    echo "{'status' : false, 'err' : 'FILE_ERROR', 'message' : 'This file is corrupt'}";
                    exit();
                }

                $picValidationResult = $form_validate([
                    'pic' => 'filerequired|filemaxmegabytes:4|filemimetypes:image/jpeg,image/png|imagedimensions:min_width=100,min_height=100'
                        ], [
                    'pic' => $pic
                ]);
                $validationResult = array_merge($validationResult, $picValidationResult);
            }

            if (!empty($validationResult)) {
                $response['status'] = false;
                $response['err'] = 'VALIDATION_ERROR';
                $response['errVal'] = $validationResult;
                $response['message'] = 'A validation error occurred';
            } else {
                if ($pic) { // if pic was sent ...
                    $uploaddir = '../img/users';

                    if (!file_exists($uploaddir)) {
                        mkdir($uploaddir, 0777, true);
                    }

                    $uploaddir = $uploaddir . '/';
                    $pic_name = 'pic_user_id_' . $user_id . '.' . pathinfo(basename($pic["name"]), PATHINFO_EXTENSION);
                    $uploadfile = $uploaddir . $pic_name;

                    if (file_exists($uploadfile)) {
                        unlink($uploadfile);
                    }

                    if (!(move_uploaded_file($pic["tmp_name"], $uploadfile))) {
                        echo "{'status' : false, 'err' : 'UPLOAD_ERROR', 'message' : 'Error occurred during the file upload'}";
                        exit();
                    }
                }


                $flag = true;
                $date = (new DateTime())->format(DateTime::ISO8601);

                do {
                    $res = null;

                    if ($pic) {
                        $res = runSimpleUpdateQuery($con, "users", ['f_name', 'm_name', 'l_name', 'gender', 'dob', 'phone', 'email', 'profile_pic'], 
                                ["'$fname'", "'$midname'", "'$lname'", "'$gender'", "'$dob'", "'$phone'", "'$email'", "'$pic'"], ['id'], ['='], [$user_id]);
                    } else {
                        $res = runSimpleUpdateQuery($con, "users", ['f_name', 'm_name', 'l_name', 'gender', 'dob', 'phone', 'email'], 
                                ["'$fname'", "'$midname'", "'$lname'", "'$gender'", "'$dob'", "'$phone'", "'$email'"], ['id'], ['='], [$user_id]);
                    }
                    if ($res['err']['code']) {
                        $flag = false;
                        break;
                    }

                    $res = runSimpleInsertQuery($con, 'user_log', ['action', 'info', 'date', 'user_id'], ["'ADMIN_EDIT'", "''", "'$date'", "'$user_id'"]);
                    if ($res['err']['code']) {
                        $flag = false;
                        break;
                    }

                    $res = runSimpleInsertQuery($con, 'admins_activities', ['admin', 'entity', 'summary', 'datetimezone'], ["'$self_email'", "'$user_name'", "'edit_user'", "'$date'"]);
                    if ($res['err']['code']) {
                        $flag = false;
                        break;
                    }
                } while (false);

                if ($flag) {
                    commit($con);
                    $response['status'] = true;
                } else {
                    rollback($con);
                    $response['status'] = false;
                    $response['err'] = 'FAILURE';
                    $response['message'] = 'Error inserting into the database';
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
            $response['message'] = 'Request was sent with the wrong request type (Use POST method)';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
        $response['message'] = 'Error connecting into the database';
    }

    autoCommit($con, true);

    disconnectConnection($con);

    echo json_encode($response, JSON_PRETTY_PRINT);
}