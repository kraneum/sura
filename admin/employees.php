<?php
session_name('UICIMA');
session_start();

if (!isset($_SESSION['id'])) {
    header("Location: login.php?goto=".urlencode($_SERVER['REQUEST_URI']));
}

require_once '../php/user_permissions.php';

if(!UserPermissions::isSuperuserPermission(+$_SESSION['user_permissions'])){
    header("Location: login.php?id=1");
}

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';

$con = makeConnection();

$employeesQuery = runSimpleFetchQuery($con, ['id', 'first_name', 'last_name', 'email', 'suspended', 'user_permissions'], 'admins', ['deleted'], ['='], [0], '', '', '');
$employees = ['err' => $employeesQuery['err']['code'], 'res' => $employeesQuery['result'], 'ct' => 0, 'isSuperuserPermission' => false];

require_once './views/employees.php';
?>