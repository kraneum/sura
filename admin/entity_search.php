<?php

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';

$con = makeConnection();

$res = null;

if ($con) {

    $_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
    if (!$_GET) {
        echo "{'status' => false, 'err' => 'VALIDATION_ERROR', 'message' => 'A validation error occurred'}";
    }

    $entity = $_GET['entity'];
    $spec = $_GET['spec'].'%';
    $limit = 10;
    ////////////////////////////////////////////////////////

    if ($entity == 'users') {
        $res = runSimpleFetchQuery($con, ['users.id', 'users.f_name','users.m_name','users.l_name','users.gender','users.dob','users.email','users.phone','users.point_balance','users.dedicated_points','users.pension_points',  'pension.name as pension_manager', 'insurer.name as health_insurer', 'users.date_joined','users.last_login','users.active','users.approved'], 'users', ['f_name', 'l_name'], ['=', '='], ["'$spec'", "'$spec'"], '', '', 10)['result'];
        
    } else if ($entity == 'agents') {
         
    } else if ($entity == 'insurers') {
         
    } else if ($entity == 'pensions') {
         
    } else if ($entity == 'ecommerce') {
         
    } else if ($entity == 'merchants') {
         
    }

    /////////////////////////////////////////////////////////
} else {
    $response['status'] = false;
    $response['err'] = 'DATABASE_CONNECT_ERROR';
    $response['message'] = 'Error connecting into the database';
}

disconnectConnection($con);

echo json_encode($res, JSON_PRETTY_PRINT);
