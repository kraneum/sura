<?php

session_name('UICIMA');
session_start();

if (!$_SESSION) {
    exit('Error 403 - You are not authorized to perform this operation');
}

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';

$entity = filter_input(INPUT_GET, 'entity', FILTER_SANITIZE_STRING);

$con = makeConnection();
$persons = null;

if ($entity == 'insurer' || $entity == 'pension') {
    $persons = runSimpleFetchQuery($con, ['id', 'name', 'email', 'phone'], $entity, [], [], [], '', '', '')['result'];
} else if ($entity == 'merchants') {
    $persons = runSimpleFetchQuery($con, ['id', 'name', 'contact_email as email', 'contact_phone as phone'], $entity, [], [], [], '', '', '')['result'];
} else {
    $persons = runSimpleFetchQuery($con, ['id', 'concat(f_name, " ", l_name) as name', 'email', 'phone'], $entity, [], [], [], '', '', '')['result'];
}
$columnHeader = '';
$columnHeader = "#" . "\t" . "Full Name" . "\t" . "Email" . "\t" . "Phone" . "\t";


$setData = '';

foreach ($persons as $person) {
    $rowData = '"' . $person['id'] . '"' . "\t" .
            '"' . $person['name'] . '"' . "\t" .
            '"' . $person['email'] . '"' . "\t" .
            '"\'' . $person['phone'] . '\'"' . "\t";

    $setData .= trim($rowData) . "\n";
}

//echo print_r($persons);
//echo ($columnHeader);
//echo ($setData);

header("Content-type: application/xls");
header("Content-Disposition: attachment; filename=".ucfirst($entity).".xls");
header("Pragma: no-cache");
header("Expires: 0");

echo ucwords($columnHeader) . "\n" . $setData . "\n";
