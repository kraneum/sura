<?php
require_once '../php/user_permissions.php';
require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';
$pageFileName = strrchr($_SERVER['PHP_SELF'], "/");
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $_SESSION['email']; ?>UICI Portal</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="../img/favicon.png" />
        <link rel="icon" type="image/png" href="../img/favicon.png">
        <link rel="apple-touch-icon" href="../img/favicon.png">

        <!--begin::Web font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

        <link href="../css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>

        <script src="../js/Chart.bundle.min.js" type="text/javascript"></script>

        <link href="../css/style_admin.css" rel="stylesheet" type="text/css"/>
        <link href="../css/sweetalert.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/sweetalert.min.js" type="text/javascript"></script>

        <style type="text/css">
            .swal2-popup{margin-top: 10px !important;}
            table tr td {
                padding: 25px 2px !important;
            }
        </style>


    </head>
    <body>
        <div class="body_">
            <div class="container">
                <div class="row" >
                    <div class="col-lg-6"><a class="navbar-brand" href="home.php" style="float: left;"><img src="../img/logos/uic2.png" height="35px" /></a></div>

                    <div class="col-lg-6" style="padding-right: 0px;">
                        <ul class="rightc">

                            <li class="nav-icon">
                                <a id="logout">
                                    <i class="fa fa-power-off fa-2x"></i>
                                </a>
                            </li>
                            <li class="dropdown dropdown-hover">
                                <?php echo '<div ' . (!$_SESSION['unapprvd_merchants_ct'] ? 'style="display:none"' : 'style="display:none"') . ' id="unapprvd_merchants_ct">' . ($_SESSION['unapprvd_merchants_ct'] ? $_SESSION['unapprvd_merchants_ct'] : '') . '</div>'; ?>
                                <a><img id="profileDpImg" class="img-responsive img-circle" width="55px" height="55px" src="../img/admins/<?php echo $_SESSION['display_picture_file_name']; ?>" alt=""/></a>

                            </li>  
                        </ul>

                    </div>

                </div>
            </div>
        </div>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav" id="navMenu"> 
                        <?php
                        $pageName = '';

                        echo array_reduce([['home.php', 'DASHBOARD', 'DUMMY'], ['transactions.php', 'TRANSACTONS', 'VIEW'], ['employees.php', 'ADMINS', 'SUPER'], ['merchants.php', 'MERCHANTS', 'VIEW'], ['underwriters.php', 'UNDERWRITERS', 'VIEW'], ['agents.php', 'AGENTS', 'VIEW'], ['subscribers.php', 'SUBSCRIBERS', 'VIEW'], ['notifications.php', '<i class="fa fa-bell"></i>', 'VIEW'], ['settings.php', '<i class="fa fa-gears"></i>', 'VIEW']], function($carry, $item) use ($pageFileName, &$pageName) {
                            ('/' . $item[0] === $pageFileName) && ($pageName = $item[1]);
                            return $carry . ((UserPermissions::hasPermission($item[2], intval($_SESSION['user_permissions'])) ? '<li><a' . ('/' . $item[0] === $pageFileName ? ' style="border-bottom:2px solid #72be58; color:#72be58;"' : '') . ' href="' . $item[0] . '">' . $item[1] . ('/' . $item[0] === $pageFileName ? '<span class="sr-only">(current)</span>' : '') . '</a></li>' : ''));
                        }, '');
                        ?>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            Last Login: <?php echo $_SESSION['last_login'] ? date("d-m-y", strtotime($_SESSION['last_login'])) : 'Never' ?>      
                        </li>
                    </ul>
                </div>
            </div>
        </nav>