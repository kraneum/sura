<?php

session_name('UICIMA');
session_start();

require_once '../php/user_permissions.php';

if (!isset($_SESSION['id']) || !UserPermissions::hasPermission('VIEW', +$_SESSION['user_permissions'])) {
    header("Location: login.php?goto=" . urlencode($_SERVER['REQUEST_URI']));
}

//To prevent sql injection
//htmlspecialchars(strip_tags(trim()));
$response = [];
$docId = filter_input(INPUT_GET, 'i', FILTER_VALIDATE_INT);
$docName = filter_input(INPUT_GET, 'n', FILTER_SANITIZE_STRING);

if ($docId && $docName) {
    require_once '../php/sura_config.php';
    require_once '../php/sura_functions.php';

    $dir = '../merchant_docs/' . $docId;

    if (file_exists($dir)) {
        $documentName = $dir . '/';

        foreach (scandir($dir) as $fileName) {
            if (!strncmp($fileName, $docName, strlen($docName))) {
                $documentName .= $fileName;
                break;
            }
        }

        if (file_exists($documentName)) {
            require_once '../php/mime_content_type.php';

            header('Content-Type: ' . mime_content_type($documentName));
            header('Content-disposition: attachment; filename=' . basename($documentName));
            header('Content-Length: ' . filesize($documentName));
            readfile($documentName);
            exit;
        } else {
            $response['result'] = null;
            $response['err'] = ['error' => 'NOTFOUND', 'msg' => 'The merchant\'s document doesn\'t exist'];
        }
    } else {
        $response['result'] = null;
        $response['err'] = ['error' => 'NOTFOUND', 'msg' => 'The merchant\'s directory doesn\'t exist'];
    }
} else {
    $response['result'] = null;
    $response['err'] = ['error' => 'DB', 'msg' => 'An error occured, please try again'];
}

echo json_encode($response);
