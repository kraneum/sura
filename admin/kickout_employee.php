<?php
session_name('UICIMA');
session_start();

require_once '../php/user_permissions.php';

if ((!isset($_SESSION['id']) || !UserPermissions::isSuperuserPermission(+$_SESSION['user_permissions']))) {
    exit();
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = json_decode(file_get_contents("php://input"), JSON_OBJECT_AS_ARRAY);
    if (isset($data['id']) && isset($data['email'])) {
        require_once '../php/kickout_admin.php';
        require_once 'save_admin_actions.php';

        $con = makeConnection();

        saveAdminActions($con, 'logout', $_SESSION['email'], htmlspecialchars(strip_tags(trim($data['email']))));

        echo json_encode(kickoutAdmin($con, filter_var($data['id'], FILTER_VALIDATE_INT, array('min_range' => 0))));
    }
}
