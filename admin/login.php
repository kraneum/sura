<?php

session_name('UICIMA');
session_start();

if (isset($_SESSION['id']) && !isset($_GET['id'])) {
    header("Location: home.php");
}

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';
require_once '../php/form_validate.php';

$operationStatus = ['err' => false];

if (isset($_POST['btn_login'])) {
    // clean user inputs to prevent sql injections
    $cleanedUserInputMap = array_map(function($value) {
        return htmlspecialchars(strip_tags(trim($_POST[$value])));
    }, ['email' => 'email', 'password' => 'password']);

    $validationResult = $form_validate([
        'email' => 'required',
        'password' => 'required',
            ], $cleanedUserInputMap);

    if (empty($validationResult)) {
        $cleanedUserInputMap['email'] = strtolower($cleanedUserInputMap['email']);
        $password = $cleanedUserInputMap['password'];
        unset($cleanedUserInputMap['password']);

        //$cleanedUserInputMap['password'] = hash('sha256', $cleanedUserInputMap['password']); // password hashing using SHA256
        $fields = array_keys($cleanedUserInputMap);
        $fields[] = 'deleted';
        $values = array_map(function($value) {
            return '"' . $value . '"';
        }, array_values($cleanedUserInputMap));
        $values[] = 0;

        $con = makeConnection();
        $isValid = false;

        $queryResult = runSimpleFetchQuery($con, ['id', 'user_permissions', 'suspended', 'display_picture_file_name', 'last_login', 'password'], 'admins', $fields, ['=', '=', '='], $values, null, null, 1);
        !empty($queryResult['result']) && $isValid = password_verify($password, $queryResult['result'][0]['password']);

        if ($isValid) {
            $employeeInfo = $queryResult['result'][0];
            if (+$employeeInfo['suspended'] === 0) {
                //Update encrypt sessionId and last login time to DB
                $updateLogin = runSimpleUpdateQuery($con, 'admins', ['session_id'], ["'" . session_id() . "'"], ['id'], ['='], [$employeeInfo['id']], 1);

                if (!$updateLogin['err']['code']) {
                    $runUnapprovedMerchantsCtRes = runUnapprovedMerchantsCt($con);

                    if (!$runUnapprovedMerchantsCtRes['err']['code']) {
                        $_SESSION['id'] = +$employeeInfo['id'];
//                        $_SESSION['entity']['id'] = +$employeeInfo['id'];
                        $_SESSION['email'] = $cleanedUserInputMap['email'];
                        $_SESSION['password'] = $employeeInfo['password'];
                        $_SESSION['user_permissions'] = +$employeeInfo['user_permissions'];
                        $_SESSION['display_picture_file_name'] = $employeeInfo['display_picture_file_name'];
                        $_SESSION['last_login'] = $employeeInfo['last_login'];
                        $_SESSION['unapprvd_merchants_ct'] = +$runUnapprovedMerchantsCtRes['result'][0];

                        header("Location: home.php");
                    } else {
                        $operationStatus['err'] = true;
                        $operationStatus['type'] = 'fetchUnapprvdCtError';

                        $operationStatus['message'] = 'Something went wrong, please retry';
                    }
                } else {
                    $operationStatus['err'] = true;
                    $operationStatus['type'] = 'updateError';

                    $operationStatus['message'] = 'Something went wrong, please retry';
                }
            } else {
                $operationStatus['err'] = true;
                $operationStatus['type'] = 'employeeSuspendedError';

                $operationStatus['message'] = 'Your account is suspended, please contact your administrator';
            }
        } else if (!$queryResult['err']['code']) {
            $operationStatus['err'] = true;
            $operationStatus['type'] = 'employeeNotFoundError';

            $operationStatus['message'] = 'Email/password comination is incorrect';
        } else {
            $operationStatus['err'] = true;
            $operationStatus['type'] = 'queryError';

            $operationStatus['message'] = 'Something went wrong, please retry';
        }
    } else {
        $operationStatus['err'] = true;
        $operationStatus = array_merge($validationResult, ['type' => 'validationError']);
    }
}

require_once './views/login.php';
