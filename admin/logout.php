<?php
session_name('UICIMA');
session_start();
if (!isset($_SESSION['id'])) {
    header("Location: login.php");
} else {
    require_once '../php/sura_config.php';
    require_once '../php/sura_functions.php';
    
    date_default_timezone_set('Africa/Lagos');
    $con = makeConnection();
    $updateLogin = runSimpleUpdateQuery($con, 'admins', ['session_id', 'last_login'], [null, '"'.(new DateTime())->format(DateTime::ISO8601).'"'], ['id'], ['='], [+$_SESSION['id']], 1);
    if (!$updateLogin['err']['code']) {
        unset($_SESSION['id']);
        unset($_SESSION['user_type_id']);
        unset($_SESSION['company_id']);
        unset($_SESSION['user_permissions']);
        session_unset();
        session_destroy();
        header("Location: login.php");
    } else {
        header("Location: " . $_SERVER['HTTP_REFERER']);
    }
}
