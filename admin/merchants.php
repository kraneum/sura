<?php
session_name('UICIMA');
session_start();

if (!isset($_SESSION['id'])) {
    header("Location: login.php?goto=" . urlencode($_SERVER['REQUEST_URI']));
}

require_once '../php/user_permissions.php';

/* if(!UserPermissions::isSuperuserPermission(+$_SESSION['user_permissions'])){
  header("Location: login.php?id=1");
  } */

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';

$con = makeConnection();

$merchantsQuery = runSimpleFetchQuery($con, ['id', 'name', 'write_up', 'point_balance', 'contact_phone', 'contact_email'/*, 'date_joined', 'rc_number', 'operating_country', 'head_office_address' , 'display_picture' */, 'documents', 'approved'], 'merchants', ['deleted'], ['='], [0], '', '', '');
$merchants = ['err' => $merchantsQuery['err']['code'], 'res' => $merchantsQuery['result'], 'ct' => 0, 'isEditPermission' => false];

require_once './views/merchants.php';
?>