<?php

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';

$con = makeConnection();

$res = null;

if ($con) {
    $_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
    if (!$_GET) {
        echo "{'status' => false, 'err' => 'VALIDATION_ERROR', 'message' => 'A validation error occurred'}";
    }

    $queryString = $_GET['query'];
    $table = $_GET['entity'];
    
    if ($table == 'users' || $table == 'agent') {
        $res = runSimpleFetchORQuery($con, ['f_name as id', "concat(f_name,' ', l_name) as label"], $table, ['f_name', 'l_name'], 
                [' LIKE ', ' LIKE '], ["'".$queryString."%'", "'".$queryString."%'"], "", "", "")['result'];
    } else {
        $res = runSimpleFetchQuery($con, ['name as id', 'name as label'], $table, ['name'], [' LIKE '], ["'".$queryString."%'"], "", "", "")['result'];
    }
    
} else {
    $response['status'] = false;
    $response['err'] = 'DATABASE_CONNECT_ERROR';
    $response['message'] = 'Error connecting into the database';
}

autoCommit($con, true);

disconnectConnection($con);

echo json_encode($res, JSON_PRETTY_PRINT);
