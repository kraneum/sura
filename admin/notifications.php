<?php
session_name('UICIMA');
session_start();

if (!isset($_SESSION['id'])) {
    header("Location: login.php?goto=" . urlencode($_SERVER['REQUEST_URI']));
}

require_once '../php/curl.php';
require_once './fragments/header.php';
?>

<link href="../css/simplePagination.css" rel="stylesheet" type="text/css"/>
<link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
<link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>

<div id="home-main" class="container">
    
    <div style="margin-bottom: 30px;">
        <h4><strong><i class="fa fa-bell"></i> Forward Notifications</strong></h4>
        <small>&emsp;&emsp;You can also send targeted notifications</small>
    </div>

    <div class="row" style="margin-left: 10px;">
        <div class="col-sm-3" >
            <form id="searchFilter" style="margin: 3%">

                <div class="row">
                    <label for="active">Entity </label>
                    <select class="form-control" id="entity" name="entity">
                        <option value="users" selected>Users</option>
                        <option value="agent">Agents</option>
                        <option value="merchants">Merchants</option>
                        <option value="insurer">Insurers</option>
                        <option value="pension">Pension Managers</option>
                        <option value="e_commerce">Stores</option>
                    </select>
                </div><br/>

                <div class="row">
                    <label for="search">Search </label>
                    <input id="search" type="text" class="form-control" placeholder="Search">
                </div><br/>

                <div class="row">
                    <button id="filterSubmit" type="submit" class="btn btn-sm btn-success btn-rounded pull-right" style="margin-top: 20px" name="btn-register">
                        <i class="fa fa-filter btn-icon-left btn-rounded"></i> Query</button>
                </div>

            </form>
        </div>

        <div id="query_display" class="col-sm-offset-1 col-sm-8" style="margin: 3%">
            <div class="row"  style="background-color: white; padding: 3%; border: #e4e3e3 solid 2px; max-height: 500px; overflow-y:auto;">

                <div class="table-responsive">
                    <table class="table table-hover ">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!--populated via javascript-->
                        </tbody>
                    </table>
                </div>

            </div>

            <br/>

                <div id="pagination" class="center-block hide"></div>

                <!--            <button id="mailAll" type="submit" class="btn btn-sm btn-primary btn-rounded pull-left disabled" name="btn-mail-all" data-toggle = "modal" data-target = "#mailModal"><i class="fa fa-envelope-o btn-icon-left btn-rounded"></i>Mail All</button>-->

                <button id="notifyAll" type="submit" class="btn btn-sm btn-success btn-rounded pull-right disabled" name="btn-notify-all" ><i class="fa fa-envelope-o btn-icon-left btn-rounded"></i>Notify All</button>

        </div>

    </div>

    <div id="info"></div>


    <!---- Mail Modal ---->
    <div class="modal fade" id="mailModal" tabindex="-1" role="dialog" aria-labelledby="myModal1Label" data-userids = "">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Send Email to <span></span></h4>
                </div>

                <form id="mailForm">
                    <div class="modal-body">
                        <input type="hidden" name="id" value=""  />
                        <input type="hidden" name="entity" value=""  />
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input type="text" class="form-control" id="subject" placeholder="Enter subject" name="subject" value=""/>
                        </div>
                        <div class="from-group">
                            <label for="mail">Message</label>
                            <textarea id="mail" name="mail" class="form-control" rows="5" maxlength="200" data-parsley-required data-parsley-minlength="4" data-parsley-maxlength="100" data-parsley-maxlength-message="This value should be less than or equal 100 characters" data-parsley-minlength-message="This value should be at least 4 characters"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="mailSend" type="submit" class="btn btn-sm btn-primary btn-rounded pull-right" name="mailSend">
                            <i class="fa fa-envelope-o btn-icon-left btn-rounded"></i>Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!---- Notification Modal ---->
    <div class="modal fade" id="notifModal" tabindex="-1" role="dialog" aria-labelledby="myModal2Label" data-userids = "">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title text-center">Send Notification to <span></span></h4>
                </div>

                <form id="notifForm">
                    <div class="modal-body">
                        <input type="hidden" name="id" value=""  />
                        <input type="hidden" name="entity" value=""  />
                        <textarea id="notif" class="form-control" name="notif" rows="5" cols="65" maxlength="200" data-parsley-required data-parsley-minlength="4" data-parsley-maxlength="100" data-parsley-maxlength-message="This value should be less than or equal 100 characters" data-parsley-minlength-message="This value should be at least 4 characters"></textarea>

                    </div>
                    <div class="modal-footer">
                        <button id="notifSend" type="submit" class="btn btn-sm btn-primary btn-rounded pull-right" name="notifSend">
                            <i class="fa fa-bell-o btn-icon-left btn-rounded"></i>Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<div style="margin-top: 2.4%"></div>
<?php
require_once './fragments/footer.php';
?>
<script src="../js/jquery.bootcomplete.js" type="text/javascript"></script>
<script src="../js/working_alert.js"  type="text/javascript"></script>
<script src="../js/jquery.simplePagination.js" type="text/javascript"></script>
<script src="views/js/admin_verification.js" type="text/javascript"></script>
<script src="views/js/notifications.js" type="text/javascript" ></script>


</body>
</html>
