<?php

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';

$con = makeConnection();

$res = null;

if ($con) {

    $_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
    if (!$_GET) {
        echo "{'status' => false, 'err' => 'VALIDATION_ERROR', 'message' => 'A validation error occurred'}";
    }

//    $queryString = $_GET['query'];
    $entity = $_GET['entity'];
    $queryString = $_GET['search_name'];
    $limit = !empty($_GET['limit'])? $_GET['limit'] : '';
    $offset = !empty($_GET['offset'])? $_GET['offset'] : '';
    ////////////////////////////////////////////////////////

    $res = runQuerySearch($con, $entity, $queryString, $limit, $offset)['result'];

    /////////////////////////////////////////////////////////
} else {
    $response['status'] = false;
    $response['err'] = 'DATABASE_CONNECT_ERROR';
    $response['message'] = 'Error connecting into the database';
}

autoCommit($con, true);

disconnectConnection($con);

echo json_encode($res, JSON_PRETTY_PRINT);
