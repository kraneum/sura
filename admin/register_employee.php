<?php
session_name('UICIMA');
session_start();

if (!isset($_SESSION['id'])) {
    header("Location: login.php?goto=".urlencode($_SERVER['REQUEST_URI']));
}

require_once '../php/user_permissions.php';

if(!UserPermissions::isSuperuserPermission(intval($_SESSION['user_permissions']))){
    header("Location: login.php?id=1");
}

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';

$operationStatus = ['err' => false];

$con = makeConnection();

if (isset($_POST['btn_register'])) {
    require_once '../php/form_validate.php';

    // clean user inputs to prevent sql injections
    $cleanedUserInputMap = array_map(function($value) {
        return htmlspecialchars(strip_tags(trim($_POST[$value])));
    }, ['first_name' => 'first_name', 'middle_name' => 'middle_name', 'last_name' => 'last_name', 'dob' => 'dob', 'phone' => 'phone', 'email' => 'email', 'country' => 'country', 'security_question' => 'security_question', 'security_answer' => 'security_answer', 'password' => 'password', 'password_confirm' => 'password_confirm']);

    $validationResult = $form_validate([
        'first_name' => 'required|maxlength:100',
        'last_name' => 'required|maxlength:100',
        'dob' => 'required|dob|maxlength:10',
        'phone' => 'required|phone|maxlength:50',
        'email' => 'required|email|maxlength:100',
        'country' => 'required|maxlength:60|alphabets',
        'security_question' => 'required|maxlength:200',
        'security_answer' => 'required|maxlength:100',
        'password' => 'required|maxlength:20',
        'password_confirm' => 'required|equalsToField:password|maxlength:20',
            ], $cleanedUserInputMap);

    
    if (empty($validationResult)) {
        $validationResult = $form_validate([
            'display_picture' => 'filerequired|filemaxmegabytes:2|filemimetypes:image/jpeg,image/png|imagedimensions:min_width=100,min_height=100'
                ], ['display_picture' => $_FILES['display_picture']]);

        if (empty($validationResult)) {
            mysqli_autocommit($con, false);

//I used boolval to clean the user_permissions array
            $userPermission = array_reduce($_POST['user_permissions'], function($userPermission, $permission) {
                return UserPermissions::grantUserPermission($permission, $userPermission);
            }, 0);

            unset($cleanedUserInputMap['password_confirm']);
            
            $cleanedUserInputMap['email'] = strtolower($cleanedUserInputMap['email']);
            $cleanedUserInputMap['password'] = password_hash($cleanedUserInputMap['password'], PASSWORD_BCRYPT);

            $displayPictureFileName = $cleanedUserInputMap['email'] . '.' . str_replace('jpeg', 'jpg', substr(getimagesize($_FILES["display_picture"]["tmp_name"])["mime"], 6));

            $fields = array_keys($cleanedUserInputMap);
            $fields[] = 'user_permissions';
            $fields[] = 'date_joined';
            $fields[] = 'display_picture_file_name';
            $values = array_map(function($value) {
                return '"' . $value . '"';
            }, array_values($cleanedUserInputMap));
            $values[] = $userPermission;
            $values[] = "'" . (new DateTime())->format(DateTime::ISO8601) . "'";
            $values[] = "'" . $displayPictureFileName . "'";
            $insertResult = runSimpleInsertQuery($con, "admins", $fields, $values);
            if (!$insertResult['err']['code']) {
                if (move_uploaded_file($_FILES["display_picture"]["tmp_name"], '../img/admins/' . $displayPictureFileName)) {
                    if (mysqli_commit($con)) {
                        //Log it
                        mysqli_autocommit($con, true);
                        require_once 'save_admin_actions.php';
                        
                        saveAdminActions($con, 'register_employee', $_SESSION['email'], $cleanedUserInputMap['email']);
                    } else {
                        //Nt commited
                        $operationStatus['err'] = true;
                        $operationStatus['type'] = 'regNtCommittedError';
                        $operationStatus['message'] = 'New admin registration not commited into DB';
                    }
                } else {
                    $operationStatus['err'] = true;
                    $operationStatus['type'] = 'imageUploadError';
                    $operationStatus['message'] = 'There was an problem uploading your display picture, please retry';
                }
            } else {
                $operationStatus['err'] = true;
                $operationStatus['type'] = 'insertError';

                switch ($insertResult['err']['code']) {
                    case '1062':
                        $error = $insertResult['err']['error'];
                        $error[strlen($error) - 1] = ' ';
                        $duplicateField = rtrim(substr($error, strrpos($error, "'") + 1));
                        $operationStatus['message'] = $cleanedUserInputMap[$duplicateField] . ' has been used to register another user, please try another ' . str_replace('_', ' ', $duplicateField);
                        break;
                    default:
                        $operationStatus['message'] = 'Something went wrong, please retry';
                        break;
                }
            }
        } else {
            $operationStatus['err'] = true;
            $operationStatus = array_merge($validationResult, ['type' => 'validationError']);
        }
    } else {
        $operationStatus['err'] = true;
        $operationStatus = array_merge($validationResult, ['type' => 'validationError']);
    }
}

$securityQuestions = runSimpleFetchQuery($con, ['question'], 'security_questions', [], [], [], '', '', '')['result'];

require_once './views/register_employee.php';
