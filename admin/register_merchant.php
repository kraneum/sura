<?php
require_once '../php/user_permissions.php';

session_name('UICIMA');
//Warning: This functionality must be blocked, we'll only create merchants in-house!!!
//exit('Its nothing personal, its just business');
session_start();

if (!isset($_SESSION['id']) || !UserPermissions::hasPermission('CREATE',+$_SESSION['user_permissions'])) {
    header("Location: login.php");
}

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';

$operationStatus = ['err' => false];

if (isset($_POST['btn_register'])) {
    require_once '../php/form_validate.php';

    // clean user inputs to prevent sql injections
    $cleanedUserInputMap = array_map(function($value) {
        return htmlspecialchars(strip_tags(trim($_POST[$value])));
    }, ['name' => 'name', 'write_up' => 'write_up', 'contact_phone' => 'contact_phone', 'contact_email' => 'contact_email', 'rc_number' => 'rc_number', 'operating_country' => 'operating_country', 'head_office_address' => 'head_office_address']);

    $validationResult = $form_validate([
        'display_picture' => 'filerequired|filemaxmegabytes:2|filemimetypes:image/jpeg,image/png|imagedimensions:min_width=100,min_height=100'
            ], ['display_picture' => $_FILES['display_picture']]);

    if (empty($validationResult)) {
        $cleanedUserInputMap['contact_email'] = strtolower($cleanedUserInputMap['contact_email']);
        $displayPictureFileName = $cleanedUserInputMap['contact_email'] . '.' . str_replace('jpeg', 'jpg', substr(getimagesize($_FILES["display_picture"]["tmp_name"])["mime"], 6));
        if (move_uploaded_file($_FILES["display_picture"]["tmp_name"], '../img/merchants/' . $displayPictureFileName)) {
            $validationResult = $form_validate([
                'name' => 'required|maxlength:100',
                'write_up' => 'required|maxlength:200',
                'contact_phone' => 'required|phone|maxlength:50',
                'contact_email' => 'required|email|maxlength:100',
                'rc_number' => 'required|maxlength:100',
                'operating_country' => 'required|maxlength:60|alphabets',
                'head_office_address' => 'required|maxlength:100'
                    ], $cleanedUserInputMap);

            if (empty($validationResult)) {
                $con = makeConnection();
                $fields = array_keys($cleanedUserInputMap);
                $fields[] = 'date_joined';
                $fields[] = 'display_picture_file_name';
                $values = array_map(function($value) {
                    return '"' . $value . '"';
                }, array_values($cleanedUserInputMap));
                $values[] = "'" . (new DateTime())->format(DateTime::ISO8601) . "'";
                $values[] = "'" . $displayPictureFileName . "'";
                $insertResult = runSimpleInsertQuery($con, "merchants", $fields, $values);
                if (!$insertResult['err']['code']) {
                    require_once 'save_admin_actions.php';
                    saveAdminActions($con, 'register_merchant', $_SESSION['email'], $cleanedUserInputMap['name'], null);
                } else {
                    $operationStatus['err'] = true;
                    $operationStatus['type'] = 'insertError';

                    switch ($insertResult['err']['code']) {
                        case '1062':
                            $error = $insertResult['err']['error'];
                            $error[strlen($error) - 1] = ' ';
                            $duplicateField = rtrim(substr($error, strrpos($error, "'") + 1));
                            $operationStatus['message'] = $cleanedUserInputMap[$duplicateField] . ' is already registered, please try another ' . str_replace('_', ' ', $duplicateField) . ' or login if you have registered';
                            break;
                        default:
                            $operationStatus['message'] = 'Something went wrong, please retry';
                            break;
                    }
                }
            } else {
                $operationStatus['err'] = true;
                $operationStatus = array_merge($validationResult, ['type' => 'validationError']);
            }
        } else {
            $operationStatus['err'] = true;
            $operationStatus['type'] = 'imageUploadError';
            $operationStatus['message'] = 'There was an problem uploading your display picture, please retry';
        }
    } else {
        $operationStatus['err'] = true;
        $operationStatus = array_merge($validationResult, ['type' => 'validationError']);
    }
}

require_once './views/register_merchant.php';
