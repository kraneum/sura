<?php

//Warning: This functionality must be blocked, we'll only create superadmins in-house!!!
//exit('Its nothing personal, its just business');
session_name('UICIMA');
session_start();

require_once '../php/user_permissions.php';

if (!isset($_SESSION['id']) || !UserPermissions::hasPermission('CREATE', +$_SESSION['user_permissions'])) {
    header("Location: login.php?id=1");
}

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';

$operationStatus = ['err' => false];

$con = makeConnection();

if (isset($_POST['btn_register'])) {
    require_once '../php/form_validate.php';

    // clean user inputs to prevent sql injections
    $cleanedUserInputMap = array_map(function($value) {
        return htmlspecialchars(strip_tags(trim($_POST[$value])));
    }, ['merchant_name' => 'merchant_name', 'merchant_write_up' => 'merchant_write_up', 'merchant_phone' => 'merchant_phone', 'merchant_email' => 'merchant_email', 'merchant_rc_number' => 'merchant_rc_number', 'merchant_operating_country' => 'merchant_operating_country', 'merchant_head_office_address' => 'merchant_head_office_address', 'admin_first_name' => 'admin_first_name', 'admin_middle_name' => 'admin_middle_name', 'admin_last_name' => 'admin_last_name', 'admin_dob' => 'admin_dob', 'admin_phone' => 'admin_phone', 'admin_email' => 'admin_email', 'admin_country' => 'admin_country', 'admin_security_question' => 'admin_security_question', 'admin_security_answer' => 'admin_security_answer', 'admin_password' => 'admin_password', 'admin_password_confirm' => 'admin_password_confirm']);

    $validationResult = $form_validate([
        'merchant_display_picture' => 'filerequired|filemaxmegabytes:2|filemimetypes:image/jpeg,image/png|imagedimensions:min_width=100,min_height=100'
            ], ['merchant_display_picture' => $_FILES['merchant_display_picture'],
        'admin_display_picture' => 'filerequired|filemaxmegabytes:2|filemimetypes:image/jpeg,image/png|imagedimensions:min_width=100,min_height=100'
            ], ['admin_display_picture' => $_FILES['admin_display_picture']]);

    if (empty($validationResult)) {
        $cleanedUserInputMap['admin_email'] = strtolower($cleanedUserInputMap['admin_email']);
        $cleanedUserInputMap['merchant_email'] = strtolower($cleanedUserInputMap['merchant_email']);

        $merchantDisplayPictureFileName = $cleanedUserInputMap['merchant_email'] . '.' . str_replace('jpeg', 'jpg', substr(getimagesize($_FILES["merchant_display_picture"]["tmp_name"])["mime"], 6));
        $adminDisplayPictureFileName = $cleanedUserInputMap['admin_email'] . '.' . str_replace('jpeg', 'jpg', substr(getimagesize($_FILES["admin_display_picture"]["tmp_name"])["mime"], 6));
        if (move_uploaded_file($_FILES["merchant_display_picture"]["tmp_name"], '../img/merchants/' . $merchantDisplayPictureFileName) && move_uploaded_file($_FILES["admin_display_picture"]["tmp_name"], '../img/employees/' . $adminDisplayPictureFileName)) {
            $validationResult = $form_validate([
                'merchant_name' => 'required|maxlength:100',
                'merchant_write_up' => 'required|maxlength:200',
                'merchant_phone' => 'required|phone|maxlength:50',
                'merchant_email' => 'required|email|maxlength:100',
                'merchant_rc_number' => 'required|maxlength:100',
                'merchant_operating_country' => 'required|maxlength:60|alphabets',
                'merchant_head_office_address' => 'required|maxlength:100',
                'admin_first_name' => 'required|maxlength:100',
                'admin_middle_name' => 'maxlength:100',
                'admin_last_name' => 'required|maxlength:100',
                'admin_dob' => 'required|dob|maxlength:10',
                'admin_phone' => 'required|phone|maxlength:50',
                'admin_email' => 'required|email|maxlength:100',
                'admin_country' => 'required|maxlength:60|alphabets',
                'admin_security_question' => 'required|maxlength:200',
                'admin_security_answer' => 'required|maxlength:100',
                'admin_password' => 'required|maxlength:20',
                'admin_password_confirm' => 'required|equalsToField:admin_password|maxlength:20'
                    ], $cleanedUserInputMap);

            if (empty($validationResult)) {
                mysqli_autocommit($con, false);

                $merchantCleanedUserInputMap = [];
                $merchant_tbl = [
                    'merchant_name' => 'name',
                    'merchant_write_up' => 'write_up',
                    'merchant_phone' => 'contact_phone',
                    'merchant_email' => 'contact_email',
                    'merchant_rc_number' => 'rc_number',
                    'merchant_operating_country' => 'operating_country',
                    'merchant_head_office_address' => 'head_office_address'
                ];

                foreach ($merchant_tbl as $merchant_tbl_k => $merchant_tbl_v) {
                    $merchantCleanedUserInputMap[$merchant_tbl_v] = $cleanedUserInputMap[$merchant_tbl_k];
                }

                $requiredDocumentsList = '{"doc1":0,"doc2":0,"doc3":0,"doc4":0}';

                $fields = array_keys($merchantCleanedUserInputMap);
                $fields[] = 'date_joined';
                $fields[] = 'display_picture';
                $fields[] = 'approved';
                $fields[] = 'deleted';
                $fields[] = 'documents';
                $values = array_map(function($value) {
                    return '"' . $value . '"';
                }, array_values($merchantCleanedUserInputMap));
                $values[] = "'" . (new DateTime())->format(DateTime::ISO8601) . "'";
                $values[] = "'" . $merchantDisplayPictureFileName . "'";
                $values[] = 0;
                $values[] = 0;
                $values[] = "'" . $requiredDocumentsList . "'";
                $merchantInsertResult = runSimpleInsertQuery($con, "merchants", $fields, $values);
                if (!$merchantInsertResult['err']['code']) {
                    require_once '../php/user_permissions.php';

                    $adminCleanedUserInputMap = [];
                    $admin_tbl = [
                        'admin_first_name' => 'first_name',
                        'admin_middle_name' => 'middle_name',
                        'admin_last_name' => 'last_name',
                        'admin_dob' => 'dob',
                        'admin_phone' => 'phone',
                        'admin_email' => 'email',
                        'admin_country' => 'country',
                        'admin_security_question' => 'security_question',
                        'admin_security_answer' => 'security_answer',
                        'admin_password' => 'password'
                    ];

                    foreach ($admin_tbl as $admin_tbl_k => $admin_tbl_v) {
                        $adminCleanedUserInputMap[$admin_tbl_v] = $cleanedUserInputMap[$admin_tbl_k];
                    }

                    $fields = array_keys($adminCleanedUserInputMap);
                    $fields[] = 'user_permissions';
                    $fields[] = 'user_type_id';
                    $fields[] = 'company_id';
                    $fields[] = 'date_joined';
                    $fields[] = 'display_picture_file_name';

                    $values = array_map(function($value) {
                        return '"' . $value . '"';
                    }, array_values($adminCleanedUserInputMap));
                    $values[] = UserPermissions::grantSuperuserPermission();
                    $values[] = 1;
                    $values[] = $merchantInsertResult['insertedId'];
                    $values[] = "'" . (new DateTime())->format(DateTime::ISO8601) . "'";
                    $values[] = "'" . $adminDisplayPictureFileName . "'";
                    $adminInsertResult = runSimpleInsertQuery($con, "employees", $fields, $values);
                    if (!$adminInsertResult['err']['code']) {
                        if (mkdir('../merchant_docs/' . $merchantInsertResult['insertedId']) && mysqli_commit($con)) {
                            require_once 'save_admin_actions.php';

                            saveAdminActions($con, 'register_merchant', $cleanedUserInputMap['admin_email'], $cleanedUserInputMap['merchant_name']);

                            $_SESSION['id'] = $adminInsertResult['insertedId'];
                            $_SESSION['email'] = $cleanedUserInputMap['admin_email'];
                            $_SESSION['company_id'] = $merchantInsertResult['insertedId'];
                            $_SESSION['user_permissions'] = UserPermissions::grantSuperuserPermission();
                            $_SESSION['display_picture_file_name'] = $adminDisplayPictureFileName;
                            $_SESSION['last_login'] = (new DateTime())->format(DateTime::ISO8601);
                            $_SESSION['name'] = $cleanedUserInputMap['merchant_name'];
                            $_SESSION['approved'] = 0;
                            $_SESSION['documents'] = json_decode($requiredDocumentsList, JSON_OBJECT_AS_ARRAY);

                            header("Location: documents.php");
                        } else {
                            //Nt commited
                            $operationStatus['err'] = true;
                            $operationStatus['type'] = 'regNtCommittedError';
                            $operationStatus['message'] = 'New merchant registration not commited into DB';
                        }
                    } else {
                        $operationStatus['err'] = true;
                        $operationStatus['type'] = 'insertError';

                        switch ($adminInsertResult['err']['code']) {
                            case '1062':
                                $error = $adminInsertResult['err']['error'];
                                $error[strlen($error) - 1] = ' ';
                                $duplicateField = rtrim(substr($error, strrpos($error, "'") + 1));
                                $operationStatus['message'] = $cleanedUserInputMap[$duplicateField] . ' has been used to register another user, please try another ' . str_replace('_', ' ', $duplicateField);
                                break;
                            default:echo $adminInsertResult['err']['error'];
                                $operationStatus['message'] = 'Something went wrong, please retry';
                                break;
                        }
                    }
                } else {
                    $operationStatus['err'] = true;
                    $operationStatus['type'] = 'insertError';

                    switch ($merchantInsertResult['err']['code']) {
                        case '1062':
                            $error = $merchantInsertResult['err']['error'];
                            $error[strlen($error) - 1] = ' ';
                            $duplicateField = rtrim(substr($error, strrpos($error, "'") + 1));
                            $operationStatus['message'] = $cleanedUserInputMap[$duplicateField] . ' is already registered, please try another ' . str_replace('_', ' ', $duplicateField) . ' or login if you have registered';
                            break;
                        default:echo $merchantInsertResult['err']['error'];
                            $operationStatus['message'] = 'Something went wrong, please retry';
                            break;
                    }
                }
            } else {
                $operationStatus['err'] = true;
                $operationStatus = array_merge($validationResult, ['type' => 'validationError']);
            }
        } else {
            $operationStatus['err'] = true;
            $operationStatus['type'] = 'imageUploadError';
            $operationStatus['message'] = 'There was an problem uploading your display picture, please retry';
        }
    } else {
        $operationStatus['err'] = true;
        $operationStatus = array_merge($validationResult, ['type' => 'validationError']);
    }
}

$securityQuestions = runSimpleFetchQuery($con, ['question'], 'security_questions', [], [], [], '', '', '')['result'];

require_once './views/register_merchant_superadmin.php';
