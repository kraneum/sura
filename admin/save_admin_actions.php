<?php

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';

function saveAdminActions($con, $action, $admin, $user, $changes = null) {
  
    $dateTimeZone = (new DateTime())->format(DateTime::ISO8601);
    $saveIntoAdminActivities = runSimpleInsertQuery($con, 'admins_activities', ['admin', 'entity', 'summary', 'details', 'datetimezone'], ['"' . $admin . '"', '"' . $user . '"', '"' . $action . '"', "'" . json_encode($changes) . "'", '"' . $dateTimeZone . '"']);

    if (!$saveIntoAdminActivities['err']['code']) {
        //runSimpleInsertQuery($con, 'merchants_recent_activity', ['activity_summary', 'datetimezone', 'activity_type', 'activity_id'], ["'" . json_encode(['admin' => $admin, 'user' => $user, 'summary' => $action, 'details' => $changes]) . "'", '"' . $dateTimeZone . '"', '"ADMIN_ACTIONS"', $saveIntoAdminActivities['insertedId']]);
    } else {
        //return $saveIntoAdminActivities['err'];
    }
}
