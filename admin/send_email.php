<?php

session_name('UICIMA');
session_start();

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';

require_once '../php/user_permissions.php';

if ((!isset($_SESSION['id']) || !UserPermissions::hasPermission('UPDATE', +$_SESSION['user_permissions']))) {
    echo '{"status" : "false", "err":"NO_PERMISSION", "message":"You do not have the permission to perform this operation"}';
} else {

    $con = makeConnection();

    autoCommit($con, false);

    if ($con) {
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if (!$_POST) {
            autoCommit($con, true);
            disconnectConnection($con);
            echo "{'status' : 'false', 'err':'VALIDATION_ERROR', 'message':'A validation error occurred'}";
            exit();
        }

        $rec_email = $_POST['id'];
        $entity = $_POST['entity'];
        $subject = $_POST['subject'];
        $body = $_POST['mail'];

        $selfInfo = runSimpleFetchQuery($con, ['id', 'email'], 'admins', ['id'], ['='], [$_SESSION['id']], '', '', 1)['result'];
        if (empty($selfInfo)) {
            autoCommit($con, true);
            disconnectConnection($con);
            echo "{'status':false, 'err':'NO_SUCH_ADMIN', 'message':'Enter correct admin login details'}";
            exit();
        }
        $self_email = $selfInfo[0]['email'];

        $flag = true;
        $date = (new DateTime())->format(DateTime::ISO8601);
        
        $ent = null;
        if ($entity == 'users') {
            $ent = 'user';
        } else if ($entity == 'pension') {
            $ent = 'pension manager';
        } else {
            $ent = $entity;
        }

        $res = null;

        do {

            $res = runSimpleInsertQuery($con, 'admins_activities', ['admin', 'entity', 'summary', 'details', 'datetimezone'], ["'$self_email'", "'$ent'", "'sent_email'", "'$rec_email'", "'$date'"]);
            if ($res['err']['code']) {
                $flag = false;
                break;
            }
        } while (false);

        if ($flag) {
            commit($con);
            $response['status'] = true;
        } else {
            rollback($con);
            $response['status'] = false;
            $response['err'] = 'FAILURE';
            $response['message'] = 'Error sending notification';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
        $response['message'] = 'Error connecting into the database';
    }

    autoCommit($con, true);

    disconnectConnection($con);

    echo json_encode($response, JSON_PRETTY_PRINT);
    
    if ($flag) {
        require 'send_mail.php';
        send_mail($rec_email, $subject, $body);
    }
    
}