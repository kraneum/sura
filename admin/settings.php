<?php
session_name('UICIMA');
session_start();

if (!isset($_SESSION['id'])) {
    header("Location: login.php?goto=" . urlencode($_SERVER['REQUEST_URI']));
}

require_once './fragments/header.php';
require_once '../php/curl.php';

$health_insurers = getData('get_all_health_insurers');
$general_insurers = getData('get_all_general_insurers');
?>

<link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
<link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>

<div id="home-main" class="container">

    <div style="margin-bottom: 30px;">
        <h4><strong><i class="fa fa-gear"></i> System Settings</strong></h4>
        <small>&emsp;&emsp;You can change the default system settings here</small>
    </div>

    <ul class="nav nav-tabs tabs-material">
        <li class="active"><a href="#policies" data-toggle="tab">Basic Policies</a></li>
        <li><a href="#others" data-toggle="tab">Other Settings</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="policies" style="background-color: white; padding: 3%; margin-bottom: 10px;">

            <div id="primary">

                <div class="row">
                    <button id="publishPolicy" class="btn btn-success btn-sm pull-right"> Create New Policy</button>
                </div>

                <div class="table-responsive">
                    <table class="table ">

                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Type</th>
                                <th class="text-center">Title</th>
                                <th class="text-center">Description</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Tenure</th>
                            </tr>
                        </thead>

                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>

            <div id="secondary"></div>
        </div>
        <div class="tab-pane fade" id="others">

            <div class="panel-group panel-accordion" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="base_insurers_heading">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#base_insurers" aria-expanded="false" aria-controls="base_insurers" class="collapsed">
                                Basic Product Insurers
                            </a>
                        </h4>
                    </div>
                    <div id="base_insurers" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="base_insurers_heading" aria-expanded="false">
                        <div class="panel-body">

                            <form id="baseInsurersForm" class="form-horizontal">
                                <div class="form-group">
                                    <label for="health_insurer" class="col-sm-2 control-label">Health Insurer</label>
                                    <div class="col-sm-4">
                                        <select class="form-control" name="health_insurer" id="health_insurer">
                                            <?php
                                            $health_insurers_count = count($health_insurers);
                                            for ($i = 0; $i < $health_insurers_count - 1; $i++) {
                                                $selected = $health_insurers[$i]['id'] == $health_insurers[$health_insurers_count - 1]['base_insurer_id'] ? 'selected' : '';
                                                echo '<option value="' . $health_insurers[$i]['id'] . '" ' . $selected . ' >' . $health_insurers[$i]['name'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="general_insurer"  class="col-sm-2 control-label">General Insurer</label>
                                    <div class="col-sm-4">
                                        <select class="form-control" name="general_insurer" id="general_insurer">
                                            <?php
                                            $general_insurers_count = count($general_insurers);
                                            for ($i = 0; $i < $general_insurers_count - 1; $i++) {
                                                $selected = $general_insurers[$i]['id'] == $general_insurers[$general_insurers_count - 1]['base_insurer_id'] ? 'selected' : '';
                                                echo '<option value="' . $general_insurers[$i]['id'] . '" ' . $selected . ' >' . $general_insurers[$i]['name'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="smsConfigPanel">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#smsConfig" aria-expanded="false" aria-controls="collapseTwo">
                                SMS Price Configuration
                            </a>
                        </h4>
                    </div>
                    <div id="smsConfig" class="panel-collapse collapse" role="tabpanel" aria-labelledby="smsConfigPanel" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <form id="smsPriceForm" class="form-horizontal">
                                <div class="form-group">
                                    <label for="sms_price"  class="col-sm-2 control-label">SMS Price in (in naira)</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" type="text" id="sms_price" name="price" value="" data-parsley-required data-parsley-number/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="paymentCommissionPanel">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#paymentCommission" aria-expanded="false" aria-controls="collapseTwo">
                                Payment Commission Configuration
                            </a>
                        </h4>
                    </div>
                    <div id="paymentCommission" class="panel-collapse collapse" role="tabpanel" aria-labelledby="paymentCommissionPanel" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <form id="paymentCommissionForm" class="form-horizontal">
                                <div class="form-group">
                                    <label for="pay_commission"  class="col-sm-2 control-label">Payment Commission (in &#37;)</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" type="text" id="pay_commission" name="commission" value="" data-parsley-required data-parsley-number/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <div id="info"></div>

</div>

<!-------------- Modal ---------------------->
<div class="modal fade" id="deactivateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Confirm Deactivate</h4>
            </div>
            <div class="modal-body">
                <p>You are about to <strong>de-activate</strong> this policy. Having read the <a href="#">Terms</a> and <a href="#">Conditions</a>, do you wish to continue?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-rounded box-shadow--2dp btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" id="modalDeactivateBtn" class="btn btn-danger btn-rounded box-shadow--2dp" data-policyid="">De-activate</button>
            </div>
        </div>
    </div>
</div>

<!-------------- Modal ---------------------->
<div class="modal fade" id="activateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Confirm activate</h4>
            </div>
            <div class="modal-body">
                <p>You are about to <strong>activate</strong> this policy. Having read the <a href="#">Terms</a> and <a href="#">Conditions</a>, do you wish to continue?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-rounded box-shadow--2dp btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" id="modalActivateBtn" class="btn btn-success btn-rounded box-shadow--2dp" data-policyid="">Activate</button>
            </div>
        </div>
    </div>
</div>

<?php
require_once './fragments/footer.php';
?>

<script src="../js/jquery.bootcomplete.js" type="text/javascript"></script>
<script src="../js/working_alert.js"  type="text/javascript"></script>
<script src="views/js/admin_verification.js" type="text/javascript"></script>
<script src="views/js/settings.js" type="text/javascript" ></script>

</body>
</html>
