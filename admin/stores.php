<?php
session_name('UICIMA');
session_start();

if (!isset($_SESSION['id'])) {
    header("Location: login.php?goto=" . urlencode($_SERVER['REQUEST_URI']));
}

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';
require_once '../php/curl.php';
require_once './fragments/header.php';
?>
<link href="../css/bootcomplete.css" rel="stylesheet" type="text/css"/>
<link href="../css/simplePagination.css" rel="stylesheet" type="text/css"/>

<link href="../css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
<link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>

<div id="home-main" class="container">
    
    <h2 class="text-center"><strong>STORES</strong></h2>

    <div id="primary">
        
        <div class="row" style="margin: 1% 0;">
            
            <div class="col-sm-2">
                <button id="add_store" class="btn btn-primary btn-sm"><i class="fa fa-plus btn-icon-left"></i>Add Store</button>
            </div>
            <div class="col-sm-2">
                <a href="export.php?entity=ecommerce" target="_blank" id="export" class="btn btn-sm btn-default">Export Stores</a>
            </div>
            <div class="input-group col-sm-offset-10 col-sm-2">
                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
                <input id="search" type="text" class="form-control" placeholder="Search" aria-describedby="basic-addon1">
            </div>
        </div>

        <div class="table-responsive">

            <table class="table table-hover ">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>About</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Account No.</th>
                        <th>Balance</th>
                        <th>Date Joined</th>
                        <th>Approved</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>


    <div id="secondary" style="display: none"></div>

    <div id="pagination" style="display: none"></div>

    <div id="info"></div> <!-- for feedback purposes ...-->

</div>

<?php
require_once './fragments/footer.php';
?>
<script src="../js/jquery.simplePagination.js" type="text/javascript"></script>
<script src="../js/moment.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/working_alert.js"></script>
<script src="../js/parsley.min.js" type="text/javascript"></script>
<script src="views/js/admin_verification.js" type="text/javascript"></script>
<script src="views/js/_globals.js" type="text/javascript"></script>
<script type="text/javascript" src="views/js/stores.js"></script>

