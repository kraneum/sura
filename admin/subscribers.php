<?php
session_name('UICIMA');
session_start();

if (!isset($_SESSION['id'])) {
    header("Location: login.php?goto=" . urlencode($_SERVER['REQUEST_URI']));
}

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';
require_once '../php/curl.php';
require_once './fragments/header.php';
?>
<link href="../css/bootcomplete.css" rel="stylesheet" type="text/css"/>
<link href="../css/simplePagination.css" rel="stylesheet" type="text/css"/>
<link href="../css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
<link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>

<div id="home-main" class="container">
    <div class="row" style="margin-bottom: 30px;">
        <div class="col-sm-10">
            <h4><strong><i class="fa fa-users"></i> View Subscribers</strong></h4>
            <small>&emsp;&emsp;You can also approved and validate them</small>
        </div>
        <div class="col-sm-2">
            <a href="usersgen.php">View generated users</a>
        </div>
    </div>

    <div id="primary">

        <div class="row" style="margin: 1% 0;">
            <div class="col-sm-2">
                <a href="export.php?entity=users" target="_blank" id="export" class="btn btn-sm btn-success">Export Users</a>
            </div>
            <div class="input-group col-sm-offset-10 col-sm-2">
                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
                <input id="search" type="text" class="form-control" placeholder="Search" aria-describedby="basic-addon1">
            </div>
        </div>

        <div class="table-responsive">

            <table class="table table-hover ">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>Last Name</th>
                        <th>Gender</th>
                        <th>Date of birth</th>
                        <th>Phone</th>
                        <!--<th>Date Joined</th>-->
                        <th>Last Login</th>
                        <!--<th>Health Insurer</th>-->
                        <th>Health Points</th>
                        <th>General Points</th>
                        <!--<th>Pension Points</th>-->
                        <th>Approved</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>


    <div id="secondary" style="display: none"></div>

    <div id="pagination" style="display: none"></div>

    <div id="info"></div> <!-- for feedback purposes ...-->

    <!-------------------------------- Modal --------------------------------->

    <div class="modal fade" id="disburseModal" tabindex="-1" role="dialog" aria-labelledby="disburseModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Disburse SMS credit</h4>
                </div>
                <form id="disburseForm">
                    <div class="modal-body">
                        <!--<div id="sms_info" style="display: none;"></div>-->
                        <input type="hidden" name="insurerid" value=""/> <!-- ... assigned dynamically-->
                        <div class="row" style="margin-top: 5%;">
                            <label for="amount" class="col-sm-3 col-sm-offset-2" style="padding: 6px;">Enter amount: </label>
                            <div class="col-sm-5">
                                <input type="text" id="amount" name="amount" required data-parsley-type="number"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"style="margin-top: 5%;">
                        <button type="submit" class="btn btn-primary">Disburse</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-------------------------------- Details Modal --------------------------------->

    <div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title text-center">User details</h4>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>

</div>

<?php
require_once './fragments/footer.php';
?>
<script src="../js/jquery.simplePagination.js" type="text/javascript"></script>
<script src="../js/moment.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/working_alert.js"></script>
<script src="../js/parsley.min.js" type="text/javascript"></script>
<script src="views/js/admin_verification.js" type="text/javascript"></script>
<script src="views/js/_globals.js" type="text/javascript"></script>
<script type="text/javascript" src="views/js/users.js"></script>

</body>
</html>
