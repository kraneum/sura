<?php
session_name('UICIMA');
session_start();

require_once '../php/user_permissions.php';

if ((!isset($_SESSION['id']) || !UserPermissions::isSuperuserPermission(+$_SESSION['user_permissions']))) {
    exit();
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = json_decode(file_get_contents("php://input"), JSON_OBJECT_AS_ARRAY);

    if (isset($data['id']) && isset($data['opr']) && isset($data['email'])) {
        require_once '../php/sura_config.php';
        require_once '../php/sura_functions.php';

        $con = makeConnection();

        $suspendUserQueryResult = runSuspendAdminQuery($con, +$data['id'], UserPermissions::grantSuperuserPermission(), +$data['opr']);

        if (!$suspendUserQueryResult['err']['code']) {
            if ($suspendUserQueryResult['result']) {
                require_once '../php/kickout_admin.php';
                require_once 'save_admin_actions.php';

                kickoutAdmin($con, +$data['id']);
                saveAdminActions($con, 'suspend', $_SESSION['email'], $data['email'], ['suspend' => +$data['opr']]);

                echo '{"status" : "SUCCESS"}';
            } else {
                echo '{"status" : "EMPLOYEENOTFOUNDORINSUFFICIENTPRIVILEDGESORNOCHANGES"}';
            }
        } else {
            echo '{"status" : "DBERROR"}';
        }
    }
}
