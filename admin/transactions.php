<?php
session_name('UICIMA');
session_start();

if (!isset($_SESSION['id'])) {
    header("Location: login.php?goto=" . urlencode($_SERVER['REQUEST_URI']));
}

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';
require_once '../php/curl.php';
require_once './fragments/header.php';

?>
<link href="../css/bootcomplete.css" rel="stylesheet" type="text/css"/>
<link href="../css/simplePagination.css" rel="stylesheet" type="text/css"/>
<link href="../css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>

<div id="home-main" class="container">
    <div style="margin-bottom: 30px;">
        <h4><strong><i class="fa fa-exchange"></i> View all Transactions</strong></h4>
        <small>&emsp;&emsp;You can also filter based on various parameters</small>
    </div>

    <ul class="nav nav-tabs tabs-material">
        <li id="users" data-id="users" class="active"><a href="#listing" data-toggle="tab">Users</a></li>
        <li id="agents" data-id="agents"><a href="#listing" data-toggle="tab">Agents</a></li>
        <li id="insurers" data-id="insurers"><a href="#listing" data-toggle="tab">Insurers</a></li>
        <li id="insurers_sms" data-id="insurers_sms"><a href="#sms_listing" data-toggle="tab">Insurers (sms)</a></li>
        <li id="pensions" data-id="pensions"><a href="#listing" data-toggle="tab">Pensions</a></li>
        <li id="merchants" data-id="merchants"><a href="#listing" data-toggle="tab">Merchants</a></li>
        <li id="ecommerce" data-id="ecommerce"><a href="#listing" data-toggle="tab">E-commerce</a></li>
    </ul>
    <div class="tab-content">
        
        <div class="tab-pane fade active in" id="listing">
            <div class="panel-group panel-accordion" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                Filter
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <form id="transFilter">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="transtype">Transaction type </label>
                                        <select class="form-control" id="transtype" name="transtype">
                                            <option selected value=""></option>
                                            <option value="1">Load Points</option>
                                            <option value="2">Buy Policy</option>
                                            <option value="3">Renew Policy</option>
                                            <option value="4">User-to-User Point Transfer</option>
                                            <option value="7">Agent-to-User Point Transfer</option>
                                            <option value="9">Make e-Commerce Payment</option>
                                            <option value="10">Pension Top-up</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <label for="from">From  </label>
                                        <input class="form-control" type="text" id="from">
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="to">To </label>
                                        <input class="form-control" type="text" id="to"/>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="startDate">Date range </label>
                                        <div class="input-group input-daterange">
                                            <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="startDate" name="startDate" value="" data-date-end-date="0d">
                                            <div class="input-group-addon">to</div>
                                            <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="endDate" name="endDate" value="" data-date-end-date="0d">
                                        </div>
                                    </div>
                                    <div class="col-sm-2" style="margin-top: 25px">
                                        <button id="filterSubmit" type="submit" class="btn btn-sm btn-success btn-rounded"  name="apply">
                                            <i class="fa fa-filter btn-icon-left btn-rounded"></i> Apply</button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-hover ">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Ref</th>
                            <th>Points</th>
                            <th>Date</th>
                            <th>Type</th>
                            <th>From</th>
                            <th>To</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!--populated via javascript-->
                    </tbody>
                </table>
            </div>

            <div id="pagination" ></div>

        </div>
        
        <div class="tab-pane fade" id="sms_listing">
            <div class="panel-group panel-accordion" id="accordion_sms" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion_sms" href="#collapseOne_sms" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                Filter
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne_sms" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <form id="transFilter_sms">
                                <div class="row">
                                    
                                    <div class="col-sm-4">
                                        <label for="to_sms">To </label>
                                        <input class="form-control" type="text" id="to_sms"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="startDate_sms">Date range </label>
                                        <div class="input-group input-daterange">
                                            <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="startDate_sms" name="startDate_sms" value="" data-date-end-date="0d">
                                            <div class="input-group-addon">to</div>
                                            <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="endDate_sms" name="endDate_sms" value="" data-date-end-date="0d">
                                        </div>
                                    </div>
                                    <div class="col-sm-2" style="margin-top: 25px">
                                        <button id="filterSubmit_sms" type="submit" class="btn btn-sm btn-success btn-rounded"  name="apply_sms">
                                            <i class="fa fa-filter btn-icon-left btn-rounded"></i> Apply</button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-hover ">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Ref</th>
                            <th>Points</th>
                            <th>Date</th>
                            <th>To</th>
                            <th>Previous Balance</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!--populated via javascript-->
                    </tbody>
                </table>
            </div>

            <div id="pagination" ></div>

        </div>

    </div>

</div>

<?php
require_once './fragments/footer.php';
?>
<script src="../js/jquery.bootcomplete.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/working_alert.js"></script>
<script src="../js/moment.min.js" type="text/javascript"></script>
<script src="../js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="../js/jquery.simplePagination.js" type="text/javascript"></script>
<script type="text/javascript" src="views/js/transactions.js"></script>

</body>
</html>