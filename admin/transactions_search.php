<?php

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';

$con = makeConnection();

$res = null;

if ($con) {

    $_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
    if (!$_GET) {
        echo "{'status' => false, 'err' => 'VALIDATION_ERROR', 'message' => 'A validation error occurred'}";
    }

    $queryString = $_GET['query'];
    $entity = $_GET['entity'];
    $spec = $_GET['spec'];
    $limit = 10;
    ////////////////////////////////////////////////////////

    if ($entity == 'users') {
        if ($spec == 'from') {
            $res = runTransactionsSearch_user_from($con, $queryString, $limit)['result'];
        } else {
            $res = runTransactionsSearch_user_to($con, $queryString, $limit)['result'];
        }
    } else if ($entity == 'agents') {
        if ($spec == 'from') {
            $res = runTransactionsSearch_agent_from($con, $queryString, $limit)['result'];
        } else {
            $res = runTransactionsSearch_agent_to($con, $queryString, $limit)['result'];
        }
    } else if ($entity == 'insurers') {
        if ($spec == 'from') {
            $res = runTransactionsSearch_IPS_from($con, $queryString, $limit)['result'];
        } else {
            $res = runTransactionsSearch_insurer_to($con, $queryString, $limit)['result'];
        }
    } else if ($entity == 'insurers_sms') {
        $res = runTransactionsSearch_insurer_to($con, $queryString, $limit)['result'];
    } else if ($entity == 'pensions') {
        if ($spec == 'from') {
            $res = runTransactionsSearch_IPS_from($con, $queryString, $limit)['result'];
        } else {
            $res = runTransactionsSearch_pension_to($con, $queryString, $limit)['result'];
        }
    } else if ($entity == 'ecommerce') {
        if ($spec == 'from') {
            $res = runTransactionsSearch_IPS_from($con, $queryString, $limit)['result'];
        } else {
            $res = runTransactionsSearch_ecommerce_to($con, $queryString, $limit)['result'];
        }
    } else if ($entity == 'merchants') {
        if ($spec == 'from') {
            $res = runTransactionsSearch_merchant_from($con, $queryString, $limit)['result'];
        } else {
            $res = runTransactionsSearch_merchant_to($con, $queryString, $limit)['result'];
        }
    }

    /////////////////////////////////////////////////////////
} else {
    $response['status'] = false;
    $response['err'] = 'DATABASE_CONNECT_ERROR';
    $response['message'] = 'Error connecting into the database';
}

autoCommit($con, true);

disconnectConnection($con);

echo json_encode($res, JSON_PRETTY_PRINT);
