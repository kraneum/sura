<?php

session_name('UICIMA');
session_start();

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';
require_once '../php/user_permissions.php';


if ((!isset($_SESSION['id']) || !UserPermissions::hasPermission('UPDATE', +$_SESSION['user_permissions']))) {
    echo '{"status" : "false", "err":"NO_PERMISSION", "message":"You do not have the permission to perform this operation"}';
} else {
    $con = makeConnection();
    autoCommit($con, false);

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                echo json_encode(['status' => false, 'info' => 'VALIDATION_ERROR', 'message' => 'A validation error occurred']);
                disconnectConnection($con);
                exit();
            }

            $health_insurer_id = $_POST['health_insurer'];
            $general_insurer_id = $_POST['general_insurer'];

            /////////////////////////////////////////////

            $selfInfo = runSimpleFetchQuery($con, ['id', 'email'], 'admins', ['id'], ['='], [$_SESSION['id']], '', '', 1)['result'];
            if (empty($selfInfo)) {
                disconnectConnection($con);
                echo json_encode(['status' => false, 'info' => 'NO_SUCH_ADMIN', 'message' => 'Enter correct admin login details']);
                exit();
            }
            $self_email = $selfInfo[0]['email'];

            /////////////////////////////////////////////

            $flag = true;
            $date = (new DateTime())->format(DateTime::ISO8601);

            do {

                $res = runSimpleUpdateQuery($con, 'insurer_leads', ['health_insurer_id', 'general_insurer_id'], ["'$health_insurer_id'", "'$general_insurer_id'"], ['id'], ['='], ['1']);
                if ($res['err']['code']) {
                    $flag = false;
                    break;
                }

                $res = runSimpleInsertQuery($con, 'admins_activities', ['admin', 'summary', 'datetimezone'], ["'$self_email'", "'base_insurers_update'", "'$date'"]);
                if ($res['err']['code']) {
                    $flag = false;
                    break;
                }
            } while (false);

            /////////////////////////////////////////////

            if ($flag) {
                commit($con);
                $response['status'] = true;
            } else {
                rollback($con);
                $response['status'] = false;
                $response['info'] = 'ERROR_OCCURRED';
            }
        } else {
            $response['status'] = false;
            $response['info'] = 'WRONG_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['info'] = 'DATABASE_CONNECT_ERROR';
    }

    autoCommit($con, true);

    disconnectConnection($con);

    echo json_encode($response);
}