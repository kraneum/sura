<?php
session_name('UICIMA');
session_start();

if (!isset($_SESSION['id'])) {
    header("Location: login.php?goto=" . urlencode($_SERVER['REQUEST_URI']));
}

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';
require_once '../php/curl.php';
require_once './fragments/header.php';
?>
<link href="../css/simplePagination.css" rel="stylesheet" type="text/css"/>

<div id="home-main" class="container">
    <div class="row" style="margin-bottom: 30px;">
        <div class="col-sm-10">
            <h4><strong><i class="fa fa-users"></i> View Auto-generated List</strong></h4>
            <small>&emsp;&emsp;You can also download them</small>
        </div>
    </div>

<!--    <div class="row" style="margin: 1% 0;">
        <div class="col-sm-offset-10 col-sm-2">
            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
            <input id="search" type="text" class="form-control" placeholder="Search" aria-describedby="basic-addon1">
        </div>
    </div>-->


    <div id="primary" style="margin-top: 20px">
        <div class="">
            <div id="standard-table">
                <div class="table-responsive">
                    <table class="table">

                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Type</th>
                                <th class="text-center">List</th>
                                <th class="text-center">Date</th>
                                <th class="text-center">Processed</th>
                                <th class="text-center"></th>
                            </tr>
                        </thead>

                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <div id="pagination" style="display: none"></div>
        </div>
    </div>

</div>

<?php
require_once './fragments/footer.php';
?>
<script src="../js/jquery.simplePagination.js" type="text/javascript"></script>
<script src="../js/moment.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/working_alert.js"></script>
<script src="views/js/admin_verification.js" type="text/javascript"></script>
<script src="views/js/_globals.js" type="text/javascript"></script>
<script type="text/javascript" src="views/js/usersgen.js"></script>

</body>
</html>
