<?php

session_name('UICIMA');
session_start();

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';


$con = makeConnection();

if ($con) {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if (!$_POST) {
            echo "{'status' : false, 'err' : 'VALIDATION_ERROR', 'message' : 'A validation error occurred'}";
        }
        $password = $_POST['password'];

        ////////////////////////////////////////////

        $self_password = $_SESSION['password'];

        $flag = false;

        ////////////////////////////////////////////////

        if (password_verify($password, $self_password)) {
            $flag = true;
        }

        ////////////////////////////////////////////////

        if ($flag) {
            $response['status'] = true;
            $response['message'] = 'Admin verification successful';
        } else {
            $response['status'] = false;
            $response['err'] = 'FAILURE';
            $response['message'] = 'Admin verification failed';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'WRONG_REQUEST_TYPE';
        $response['message'] = 'The request was sent with a wrong type';
    }
} else {
    $response['status'] = false;
    $response['err'] = 'DATABASE_CONNECT_ERROR';
    $response['message'] = 'Error connecting to database';
}


disconnectConnection($con);

echo json_encode($response, JSON_PRETTY_PRINT);

