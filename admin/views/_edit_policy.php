<?php
session_start();
require_once '../../php/curl.php';

$id = $_POST['policyID'];

$types = getData('get_payment_structures');
$policy = getData('get_uic_policies', $id, $id);
$policyID = $policy[0]['id'];
$type = $policy[0]['type'];
$desc = $policy[0]['description'];
$title = $policy[0]['title'];
$price = $policy[0]['price'];
$tenure = $policy[0]['tenure'];
?>

<form id ="editPolicyForm" method="post" action="#">
    <input type="hidden" name="policyid" id="policyid" value="<?php echo $policyID ?>"/>

        <div class="form-group">
            <label for="type" class="control-label">Insurance type </label>
            <p><?php echo $type; ?></p>
        </div>

    <div class="form-group">
        <label for="title" class="control-label"> Title * </label>
        <input class="form-control" id="title" name="title"  type="text" placeholder="Enter company name" value="<?php echo $title ?>" data-parsley-required/>
    </div>

    <div class="form-group">
        <label for="desc" class="control-label">Description (not more than 100 words) * </label>
        <textarea class="form-control" id="desc" name="desc"  type="text" placeholder="Enter description" size="200" data-parsley-required><?php echo $desc ?></textarea>
    </div>

    <div class="form-group">
        <label for="extlink" class="control-label">More Info Link  * </label>
        <input class="form-control" id="extlink" name="extlink" value="http://www.google.com" type="url" placeholder="e.g http://www.google.com" data-parsley-required data-parsley-type="url" data-parsley-type-message="This is not a valid URL"/>
    </div>

    <div class="form-group">
        <label for="price" class="control-label">Price (in LP)* </label>
        <input class="form-control" id="price" name="price"  type="text" placeholder="Enter policy price" value="<?php echo $price ?>" data-parsley-required data-parsley-digits-message="This value should be digits" data-parsley-type="digits"/>
    </div>

    <div class="form-group">
        <label for="tenure">Tenure (in months) * </label>
        <select class="form-control" id="tenure" name="tenure"  data-parsley-required>
            <option disabled>Select Tenure</option>
            <?php
            for ($i = 0; $i < count($types); $i++) {
                $selected = $types[$i]['id'] == $tenure ? 'selected' : '';
                echo '<option value="' . $types[$i]['id'] . '" ' . $selected . '>' . ucfirst($types[$i]['name']) . '</option>';
            }
            ?>
        </select>
    </div>



    <div class="row">
        <button id="policy-back" class="btn btn-primary btn-rounded formButtons"  name="policy-back" style="margin-left: 15px;"><i class="fa fa-arrow-left"></i> BACK</button>
        <button id="policyEdit" type="submit" class="btn btn-success btn-rounded formButtons pull-right"  name="policyEdit" style="margin-right: 15px;">SAVE</button>
    </div>
    <br>
    <div id="license" class="text-muted text-center">By clicking Sign up, you agree to our<a href="#"> Terms and Conditions</a> and <a href="#"> Privacy statement </a></div>
</form>

<script type="text/javascript">
    $('#editPolicyForm').parsley();
</script>
