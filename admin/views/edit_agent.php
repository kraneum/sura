<?php
require_once '../../php/curl.php';

$agent = getData('get_agent_by_id', $_POST['agentid'])[0];

//print_r($agent);
?>
<link href="../css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
<link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>


<div id="home-main" class="container">
    <span id="validationMessage"></span>

    <form id ="regForm" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $agent ? $agent['id'] : '' ?>">
        <div class="form-group">
            <label for="fname">First Name * </label>
            <input class="form-control" id="fname" name="fname" value="<?= $agent ? $agent['f_name'] : '' ?>" type="text" placeholder="Enter first name"  data-parsley-required/>
        </div>

        <div class="form-group">
            <label for="midname">Middle Name </label>
            <input  class="form-control" id="midname" name="mname" value="<?= $agent ? $agent['m_name'] : '' ?>"  type="text" placeholder="Enter middle name"/>
        </div>

        <div class="form-group">
            <label for="lname"> Last Name * </label>
            <input class="form-control" id="lname" name="lname" value="<?= $agent ? $agent['l_name'] : '' ?>" type="text" placeholder="Enter last name"  data-parsley-required/>
        </div>

        <div class="form-group">
            <label for="gender">Gender * </label>
            <select class="form-control" id="gender" name="gender"  data-parsley-required>
                <option value="MALE" <?= strcasecmp($agent['gender'], 'male') == 0 ? 'selected' : '' ?> >Male</option>
                <option value="FEMALE" <?= strcasecmp($agent['gender'], 'female') == 0 ? 'selected' : '' ?> >Female</option>
            </select>
        </div>

        <div id="dob-group" class="form-group">
            <label for="dob">Date of Birth * </label>
            <div class="input-group">
                <input class="form-control" type="text" id="dob" name="dob" value="<?= $agent ? $agent['dob'] : '' ?>" placeholder="Enter date of birth"  data-parsley-required />
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>


        <div class="form-group">
            <label for="phone">Phone * </label>
            <input class="form-control" id="phone" name="phone" value="<?= $agent ? $agent['phone'] : '' ?>" placeholder="Enter phone number" data-parsley-required data-parsley-minlength="11" data-parsley-maxlength="11"  data-parsley-type="digits" data-parsley-minlength-message="This value should be 11 digits" data-parsley-maxlength-message="This value should be 11 digits" data-parsley-type-message="This value should contain digits only"/>
        </div>

        <div class="form-group">
            <label for="email">Email * </label>
            <input class="form-control" id="email" name="email" value="<?= $agent ? $agent['email'] : '' ?>"  type="email" placeholder="Enter email"/>
        </div>

        <div class="form-group">
            <label for="display_picture">Profile Picture * </label>
            <input class="form-control" id="display_picture" name="pic"  type="file" placeholder="Choose file" accept="image/jpeg, image/png" data-parsley-filemaxmegabytes="2" data-parsley-trigger="change" data-parsley-dimensions="true" data-parsley-dimensions-options='\'{"min_width": "100","min_height": "100"}\'' data-parsley-filemimetypes="image/jpeg, image/png"/>
        </div>

        <div class="form-group"> 

            <button id="btn_register" type="submit" class="btn btn-success formButtons"  name="btn_register"><i class="fa fa-save btn-icon-left"></i>Save</button>
        </div>
    </form>

    <button id="back" class="btn btn-primary"><i class="fa fa-arrow-left btn-icon-left"></i>Back</button>
</div>

<script src="../js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="../js/parsley-laraextras.min.js" type="text/javascript"></script>
<script src="../js/parsley_file_validators.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {

        $("input[name=dob]").datepicker({
            format: "dd/mm/yyyy",
            clearBtn: true,
            orientation: "auto",
            autoclose: true
        });

        $('#regForm').parsley();

        var goBack = function () {
            $('div#secondary').fadeOut(500, function () {
                $('div#primary').fadeIn(500);
                $('#pagination').show();
            });
            $(this).empty();
        };

        $('#back').on('click', function () {
            goBack();
        });

        $('#regForm').on('submit', function (event) {
            working_alert.start();

            $.ajax({
                type: "POST",
                url: "edit_agent.php",
                data: new FormData(document.getElementById('regForm')),
                dataType: 'JSON',
                /*** Options to tell JQuery not to process data or worry about content-type ****/
                cache: false,
                contentType: false,
                processData: false,
                /****************************************/
                success: function (data) {
                    var info = '';
                    if (data['status']) {
                        info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                                'Update Successful</div>';
                        let row = '#tr-' + $('input[name=id]').val();
                        $(row + ' #fname').html($('input[name=fname]').val());
                        $(row + ' #mname').html($('input[name=mname]').val());
                        $(row + ' #lname').html($('input[name=lname]').val());
                        $(row + ' #gender').html($("select[name=gender] option:selected").val());
                        $(row + ' #nname').html($('input[name=lname]').val());
                        $(row + ' #phone').html($('input[name=phone]').val());
                        $(row + ' #email').html($('input[name=email]').val());
                        $(row + ' #dob').html($('input[name=dob]').val());
                    } else {
                        if (data['err'] === 'FAILURE') {
                            info = '<div class="alert alert-dismissible alert-warning text-center btn-rounded">' +
                                    '<strong>Update Failed!</strong> Please try again with different entry' +
                                    '</div>';
                        } else if (data['err'] === 'VALIDATION_ERROR') {
                            info = '<div class="alert alert-dismissible alert-danger text-center btn-rounded">' +
                                    '<strong>' + data['errVal']['message'] + '</strong>' +
                                    '</div>';
                            $('#' + data['errVal']['field']).removeClass('parsley-success');
                            $('#' + data['errVal']['field']).addClass('parsley-error');
                        } else if (data['err'] === 'UPLOAD_ERROR') {
                            info = '<div class="alert alert-dismissible alert-danger text-center btn-rounded">' +
                                    '<strong>File upload failed</strong>. Try again later' +
                                    '</div>';
                            $("#logo").val('');
                        } else if (data['err'] === 'FILE_ERROR') {
                            info = '<div class="alert alert-dismissible alert-danger text-center btn-rounded">' +
                                    '<strong>Please upload a valid file</strong>' +
                                    '</div>';
                            $("#logo").val('');
                        } else if (data['err'] === 'ERORR_OCCURRED') {
                            info = '<div class="alert alert-dismissible alert-danger text-center btn-rounded">' +
                                    '<strong>Error Occurred!</strong> Please try again later' +
                                    '</div>';
                        }
                    }
                    working_alert.finish();
                    $("html, body").animate({scrollTop: 0}, "fast");
                    $('#info').html(info).stop().slideDown().delay(3000).slideUp();
                    goBack();

                }
            });

            $("html, body").animate({scollTop: 0}, "fast");

            event.preventDefault();
        });

    });
</script>

