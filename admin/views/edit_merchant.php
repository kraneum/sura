<?php
require_once '../../php/curl.php';

$merchant = getData('get_merchant_by_id', $_POST['merchantid'])[0];

//print_r($merchant);
?>

<!--<link href="../css/jquery-ui.min_datepicker.css" rel="stylesheet" type="text/css"/>-->
<link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
<link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>

<div id="home-main" class="container">
    <span id="validationMessage"></span>

    <form id ="regForm" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $merchant ? $merchant['id'] : '' ?>">
        <div class="form-group">
            <label for="name">Merchant Name * </label>
            <input class="form-control" id="name" name="name" type="text" value="<?= $merchant ? $merchant['name'] : '' ?>" placeholder="Enter merchant\'s name" maxlength="100" required/>
        </div>

        <div class="form-group">
            <label for="write_up">Description *</label>
            <textarea class="form-control" rows="5" id="write_up" name="desc" maxlength="200" placeholder="About merchant" required><?= ($merchant ? $merchant['write_up'] : '') ?></textarea>
        </div>

        <div class="form-group">
            <label for="phone">Phone * </label>
            <input class="form-control" id="contact_phone" required name="phone" placeholder="Enter the contact phone" value="<?= ($merchant ? $merchant['contact_phone'] : '') ?>" data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$" maxlength="11"/>
        </div>

        <div class="form-group">
            <label for="email">Email * </label>
            <input class="form-control" id="contact_email" required name="email" placeholder="Enter contact email"  type="email" value="<?= ($merchant ? $merchant['contact_email'] : '') ?>" maxlength="100"/>
        </div>

        <div class="form-group">
            <label for="fname">RC number * </label>
            <input class="form-control" id="rc_number" name="rc" required type="text" placeholder="Enter merchant\'s CAC registration number" value="<?= ($merchant ? $merchant['rc_number'] : '') ?>" maxlength="20"/>
        </div>

        <div class="form-group">
            <label for="display_picture">Logo * </label>
            <input class="form-control" id="display_picture" name="logo"  type="file" placeholder="Choose file" accept="image/jpeg, image/png" data-parsley-filemaxmegabytes="2" data-parsley-trigger="change" data-parsley-dimensions="true" data-parsley-dimensions-options='\'{"min_width": "100","min_height": "100"}\'' data-parsley-filemimetypes="image/jpeg, image/png"/>
        </div>

        <div class="form-group"> 

            <button id="btn_register" type="submit" class="btn btn-success formButtons"  name="btn_register"><i class="fa fa-save btn-icon-left"></i>Save</button>
        </div>
    </form>

    <button id="back" class="btn btn-primary"><i class="fa fa-arrow-left btn-icon-left"></i>Back</button>
</div>

<script src="../js/parsley-laraextras.min.js" type="text/javascript"></script>
<script src="../js/parsley_file_validators.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {

        $('#regForm').parsley();

        var goBack = function () {
            $('div#secondary').fadeOut(500, function () {
                $('div#primary').fadeIn(500);
                $('#pagination').show();
            });
            $(this).empty();
        };

        $('#back').on('click', function () {
            goBack();
        });

        $('#regForm').on('submit', function (event) {
            working_alert.start();

            $.ajax({
                type: "POST",
                url: "edit_merchant.php",
                data: new FormData(document.getElementById('regForm')),
                dataType: 'JSON',
                /*** Options to tell JQuery not to process data or worry about content-type ****/
                cache: false,
                contentType: false,
                processData: false,
                /****************************************/
                success: function (data) {
                    var info = '';
                    if (data['status']) {
                        info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                                'Update Successful</div>';
                        let row = '#tr-' + $('input[name=id]').val();
                        $(row + ' #name').html($('input[name=name]').val());
                        $(row + ' #desc').html($('textarea[name=desc]').val().substr(0, 20)+'...');
                        $(row + ' #phone').html($('input[name=phone]').val());
                        $(row + ' #email').html($('input[name=email]').val());
                        $(row + ' #rc').html($('input[name=rc]').val());
                        $(row + ' #acc').html($('input[name=account_no]').val());
                    } else {
                        if (data['err'] === 'FAILURE') {
                            info = '<div class="alert alert-dismissible alert-warning text-center btn-rounded">' +
                                    '<strong>Update Failed!</strong> Please try again with different entry' +
                                    '</div>';
                        } else if (data['err'] === 'VALIDATION_ERROR') {
                            info = '<div class="alert alert-dismissible alert-danger text-center btn-rounded">' +
                                    '<strong>' + data['errVal']['message'] + '</strong>' +
                                    '</div>';
                            $('#' + data['errVal']['field']).removeClass('parsley-success');
                            $('#' + data['errVal']['field']).addClass('parsley-error');
                        } else if (data['err'] === 'UPLOAD_ERROR') {
                            info = '<div class="alert alert-dismissible alert-danger text-center btn-rounded">' +
                                    '<strong>File upload failed</strong>. Try again later' +
                                    '</div>';
                            $("#logo").val('');
                        } else if (data['err'] === 'FILE_ERROR') {
                            info = '<div class="alert alert-dismissible alert-danger text-center btn-rounded">' +
                                    '<strong>Please upload a valid file</strong>' +
                                    '</div>';
                            $("#logo").val('');
                        } else if (data['err'] === 'ERORR_OCCURRED') {
                            info = '<div class="alert alert-dismissible alert-danger text-center btn-rounded">' +
                                    '<strong>Error Occurred!</strong> Please try again later' +
                                    '</div>';
                        }
                    }
                    working_alert.finish();
                    $("html, body").animate({scrollTop: 0}, "fast");
                    $('#info').html(info).stop().slideDown().delay(3000).slideUp();
                    goBack();

                }
            });

            $("html, body").animate({scollTop: 0}, "fast");

            event.preventDefault();
        });

    });
</script>

