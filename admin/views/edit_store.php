<?php
require_once '../../php/curl.php';

$store = null;

if ($_POST) {
    $store = getData('get_store_by_id', $_POST['storeid'])[0];
}
?>

<!--<link href="../css/jquery-ui.min_datepicker.css" rel="stylesheet" type="text/css"/>-->
<link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
<link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>

<div id="home-main" class="container">
    <span id="validationMessage"></span>

    <form id ="regForm" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $store ? $store['id'] : 'null' ?>">
        <div class="form-group">
            <label for="name">Store Name * </label>
            <input class="form-control" id="name" name="name" type="text" value="<?= $store ? $store['name'] : '' ?>" placeholder="Enter store's name" maxlength="100" required/>
        </div>

        <div class="form-group">
            <label for="write_up">Description *</label>
            <textarea class="form-control" rows="5" id="desc" name="desc" maxlength="200" placeholder="About store" required><?= ($store ? $store['write_up'] : '') ?></textarea>
        </div>

        <div class="form-group">
            <label for="phone">Phone * </label>
            <input class="form-control" id="phone" required name="phone" placeholder="Enter phone number" value="<?= ($store ? $store['phone'] : '') ?>" data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$" maxlength="50"/>
        </div>

        <div class="form-group">
            <label for="email">Email * </label>
            <input class="form-control" id="email" required name="email" placeholder="Enter email"  type="email" value="<?= ($store ? $store['email'] : '') ?>" maxlength="100"/>
        </div>

        <div class="form-group">
            <label for="account_no">Account Number * </label>
            <input class="form-control" id="account_no" name="account_no" placeholder="Enter account number"  type="text" value="<?= ($store ? $store['account_no'] : '') ?>" maxlength="10"/>
        </div>

        <div class="form-group"> 

            <button id="btn_register" type="submit" class="btn btn-success formButtons"  name="btn_register"><i class="fa fa-save btn-icon-left"></i>Save</button>
        </div>
    </form>

    <button id="back" class="btn btn-primary"><i class="fa fa-arrow-left btn-icon-left"></i>Back</button>
</div>

<script type="text/javascript">
    $(function () {

        $('#regForm').parsley();

        var goBack = function () {
            $('div#secondary').fadeOut(500, function () {
                $('div#primary').fadeIn(500);
                $('#pagination').show();
                fetchStores(10,0);
            });
            $(this).empty();
        };

        $('#back').on('click', function () {
            goBack();
        });

        $('#regForm').on('submit', function (event) {
            working_alert.start();

            $.ajax({
                type: "POST",
                url: "edit_store.php",
                data: new FormData(document.getElementById('regForm')),
                dataType: 'JSON',
                /*** Options to tell JQuery not to process data or worry about content-type ****/
                cache: false,
                contentType: false,
                processData: false,
                /****************************************/
                success: function (data) {
                    var info = '';
                    if (data['status']) {
                        info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                                'Update Successful</div>';
                        let row = '#tr-' + $('input[name=id]').val();
                        $(row + ' #name').html($('input[name=name]').val());
                        $(row + ' #desc').html($('textarea[name=desc]').val().substr(0, 20) + '...');
                        $(row + ' #phone').html($('input[name=phone]').val());
                        $(row + ' #email').html($('input[name=email]').val());
                        $(row + ' #acc').html($('input[name=account_no]').val());
                    } else {
                        if (data['err'] === 'FAILURE') {
                            info = '<div class="alert alert-dismissible alert-warning text-center btn-rounded">' +
                                    '<strong>Update Failed!</strong> Please try again' +
                                    '</div>';
                        } else if (data['err'] === 'VALIDATION_ERROR') {
                            info = '<div class="alert alert-dismissible alert-danger text-center btn-rounded">' +
                                    '<strong>' + data['errVal']['message'] + '</strong>' +
                                    '</div>';
                            $('#' + data['errVal']['field']).removeClass('parsley-success');
                            $('#' + data['errVal']['field']).addClass('parsley-error');
                        } else if (data['err'] === 'ERORR_OCCURRED') {
                            info = '<div class="alert alert-dismissible alert-danger text-center btn-rounded">' +
                                    '<strong>Error Occurred!</strong> Please try again later' +
                                    '</div>';
                        }
                    }
                    working_alert.finish();
                    $("html, body").animate({scrollTop: 0}, "fast");
                    $('#info').html(info).stop().slideDown().delay(3000).slideUp();
                    goBack();

                }
            });

            $("html, body").animate({scollTop: 0}, "fast");

            event.preventDefault();
        });

    });
</script>

