<?php
require_once './fragments/header.php';
require_once '../php/user_permissions.php';
?>
<link href="../css/jquery.onoff.css" rel="stylesheet" type="text/css"/>

<div id="home-main" class="container">
    <div style="margin-bottom: 30px;">
        <h4><strong><i class="fa fa-user-plus"></i> View and Create Admins</strong></h4>
        <small>&emsp;&emsp;Functionality to control access also included</small>
    </div>
    
    <div class="row">
        <div id="employeesInfoContainer" class="col-xs-12">
            <?php
            if (!$employees['err']) {
                if (count($employees['res'])) {
                    echo '<div style="margin-bottom: 20px"><a style="width:135px" href="register_employee.php" class="btn btn-xs btn-default">Add Admin</a></div><div class="table-responsive"><table class="table table-hover table-responsive">
                <thead>
                <tr>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Logout</th>
                        <th>Suspend</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>' .
                    array_reduce($employees['res'], function($carry, $item) use (&$employees) {
                        return $carry . '<tr ' . ($employees['isSuperuserPermission'] = UserPermissions::isSuperuserPermission(+$item['user_permissions']) ? 'class="alert-info" title="Super Admin"' : '' ) . '><th class="employeeCt" scope="row">'.++$employees['ct'].'</th><td>' . $item['first_name'] . '</td><td>' . $item['last_name'] . '</td><td id="eM-'.$item['id'].'">' . $item['email'] . '</td><td>'.(!$employees['isSuperuserPermission'] ? '<input id="ko-'.$item['id'].'" type="checkbox" class="form-check-input"/>' : '' ).'</td><td>'.(!$employees['isSuperuserPermission'] ? '<input id="spd-'.$item['id'].'" type="checkbox" class="form-check-input" ' . ($item['suspended'] ? 'checked="checked"/>' : '') : '').'</td><td>'.'<a href="edit_employee.php?id='.$item['id'].'" class="btn btn-success"><span class="glyphicon glyphicon-pencil" title="edit" aria-hidden="true"></span></a>'.'</td><td>'.(!$employees['isSuperuserPermission'] ? '<button type="button" id="del'.'-'.$item['id'].'" class="btn btn-danger" title="delete" aria-label="Left Align"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>' : '').'</td></tr>';
                    }, '')
                    . '</tbody>
            </table></div>';
                } else {
                    //use bootstrap alert info to display this message
                    echo '<div class="alert alert-warning">No employees registered</div>';
                }
            } else {
                //use bootstrap alert error to display this message
                echo '<div class="alert alert-danger">Problem displaying employees</div>';
            }
            ?>

        </div>
    </div>
</div>
<div id="workingAlert" style="display:none; position:fixed;width:100px;z-index:990;top:0;left: 50%;box-shadow:0 2px 4px rgba(0,0,0,0.2);height:30px;border-color: #f0c36d;text-align: center;background-color: #f9edbe;color:#333;font-weight:bold;">Working...</div>

<div style="margin-top: 6%"></div>
<?php
require_once './fragments/footer.php';
?>
<!-- touch capability has an buggy behaviour of not checking the checkbox when the pointer is dragged too quickly
<script src="../js/pointertouch.js" type="text/javascript"></script>-->
<script src="../js/onoff.js" type="text/javascript"></script>
<script type="text/javascript">
    $('input[type=checkbox]').onoff();
</script>
<script src="../js/working_alert.js" type="text/javascript"></script>
<script src="./views/js/employees.js" type="text/javascript"></script>

</body>
</html>