<?php
require_once './fragments/header_h.php';
require '../php/curl.php';

$insurers_count = getData('get_insurers_count');
$pension_managers_count = getData('get_pension_managers_count');
$subscribers_count = getData('get_subscribers_count');

$total_points_bought_merchants = getData('get_total_points_bought_merchants');
$total_points_bought_users = getData('get_total_points_bought_users');

date_default_timezone_set('Africa/Lagos');
?>

<link href="../css/timeline-custom.css" rel="stylesheet" type="text/css"/>
<link href="../css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>


<div id="home-main" class="container">
    <div id="north" class="row">
        <div class="col-sm-3">
            <div id="north-sidebar">
                <span class="span-title"> Overview </span>
                <hr/>
                <div class="box-shadow--2dp admin_b">
                    <div class="admininfo">
                        <div class="notice notice-success">Merchants registered: <strong><?= $runRegisteredMerchantsCt ?></strong></div>
                        <div class="notice notice-success">Merchants not approved: <strong><?= $runUnapprovedMerchantsCt ?></strong></div>
                        <div class="notice notice-success">Merchants registered this month: <strong><?= $runRegisterdMerchantsThisMnthCt ?></strong></div><br/>

                        <div class="notice notice-primary">Insurers registered: <strong><?= $insurers_count['total'] ?></strong></div>
                        <div class="notice notice-primary">Insurers validated: <strong><?= $insurers_count['validated'] ?></strong></div>
                        <div class="notice notice-primary">Insurers approved: <strong><?= $insurers_count['approved'] ?></strong></div><br/>

                        <div class="notice notice-warning">Pension managers registered: <strong><?= $pension_managers_count['total'] ?></strong></div>
                        <div class="notice notice-warning">Pension managers validated: <strong><?= $pension_managers_count['validated'] ?></strong></div>
                        <div class="notice notice-warning">Pension managers approved: <strong><?= $pension_managers_count['approved'] ?></strong></div><br/>

                        <div class="notice notice-info">Subscribers registered: <strong><?= $subscribers_count['total'] ?></strong></div>
                        <div class="notice notice-info">Subscribers active: <strong><?= $subscribers_count['active'] ?></strong></div>
                    </div>
                </div>
            </div>
            <!--<div id="south">
                <div id="south-sidebar">
                    Redemptions
                    <span class="pull-right"><i class="fa fa-gear"></i></span>
                    <hr/>
                    <div class="box-shadow--2dp"></div>
                </div>
            </div>-->
        </div>
        <div id="north-content" class="col-sm-8 col-sm-offset-1">
            <span class="span-title"> Sign-up summary</span>

            <hr/>
            <div class="box-shadow--2dp content-white">
                <ul class="nav nav-tabs">
                    <li><a href="#summary" data-toggle="tab">Summary</i></a></li>
                    <li><a href="#points-chart" data-toggle="tab">iPoint Purchase <i class="fa fa-pie-chart"></i></a></li>
                    <li class="active"><a href="#policies-chart" data-toggle="tab">Policies <i class="fa fa-line-chart"></i></a></li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade" id="summary">
                        <div id="table">

                            <div class="lead">Amount of Points bought by merchants: &#8358;<span class="points_bought"><?= $total_points_bought_merchants['total_amount'] ?></span></div>

                            <div class="lead">Amount of Policies bought by Subscribers: &#8358;<span class="policies_bought"><?= $total_points_bought_users['total_amount'] ?></span></div>


                            <div id="pagination" class="center-block"></div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="points-chart">
                        <p id="barchart">
                        <div id="info"></div>
                        <div class="panel-group panel-accordion" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                            Filter
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                    <div class="panel-body">
                                        <form id="pieChartFilter">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label for="date">Date range </label>
                                                    <div class="input-group input-daterange">
                                                        <input type="text" class="form-control" id="startDate_a" name="startDate" value="" data-date-end-date="0d">
                                                        <div class="input-group-addon">to</div>
                                                        <input type="text" class="form-control" id="endDate_a" name="endDate" value="" data-date-end-date="0d">
                                                    </div>
                                                </div>

                                                <div class="col-sm-offset-1 col-sm-2" style="margin-top: 30px">
                                                    <button id="filterSubmit" type="submit" class="btn btn-sm btn-success btn-rounded"  name="btn-register">
                                                        <i class="fa fa-filter btn-icon-left btn-rounded"></i> Apply</button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <canvas id="myPieChart" width="200" height="70"></canvas>
                        </p>
                    </div>

                    <div class="tab-pane fade active in" id="policies-chart">
                        <p id="linechart">
                        <div id="info"></div>
                        <div class="panel-group panel-accordion" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                            Filter
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                    <div class="panel-body">
                                        <form id="lineChartFilter">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label for="date">Date range </label>
                                                    <div class="input-group input-daterange">
                                                        <input type="text" class="form-control" id="startDate_l" name="startDate_l" value="" data-date-end-date="0d">
                                                        <div class="input-group-addon">to</div>
                                                        <input type="text" class="form-control" id="endDate_l" name="endDate_l" value="" data-date-end-date="0d">
                                                    </div>
                                                </div>

                                                <div class="col-sm-offset-1 col-sm-2" style="margin-top: 30px">
                                                    <button id="filterSubmit" type="submit" class="btn btn-sm btn-success btn-rounded"  name="btn-register">
                                                        <i class="fa fa-filter btn-icon-left btn-rounded"></i> Apply</button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <canvas id="myLineChart" width="200" height="70"></canvas>
                        </p>
                    </div>

                </div>



            </div>

            <div style="margin-top: 5%">
                <span class="span-title">Recent Activity</span>
                <!-- Modal -->
                <hr/>
                <div class="content-white  box-shadow--2dp" id="recentActivitiesContainer" style="background-color: white; padding: 20px;"></div>
            </div>
        </div>
    </div>



</div>

<?php
require_once './fragments/footer_f.php';
?>


<script src="../js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="../js/working_alert.js" type="text/javascript"></script><script src="./views/js/home.js" type="text/javascript"></script>

</body>
</html>
