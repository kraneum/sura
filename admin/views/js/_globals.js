    var limit = 10;
    
    var re_eval = false; // to check whether to repopulate users' table
    
    var searchInput = $('input[id="search"]');
    
    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    }());
    
    var options = {// for bootstrap switch...
        size: "small",
        handleWidth: "20",
        onText: "<i class=\"fa fa-check-circle\"></i>",
        offText: "<i class=\"fa fa-times\"></i>",
        onColor: 'success',
        offColor: 'danger',
        animate: true,
    };