
var callback = null;

var target = null;

var reverseSwitch = function (_target) {
    if (_target !== null) {
        _target.bootstrapSwitch('toggleState', true);
        target = null;
    }
};

function displayVerifyModal() {
    var verifyAdminModal = `<div class="modal fade" id="verifyAdminModal" tabindex="-1" role="dialog" aria-labelledby="verifyAdminModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Admin Verification</h4>
                </div>
                <form id="verifyAdminForm">
                    <div class="modal-body">
                        <div class="row" style="margin-top: 3%;">
                            <div class="form-group">
                                <label for="password" class="col-sm-3 control-label">Enter Password</label>
                                <div class="col-sm-8">
                                    <input class="form-control" id="password" name="password" type="password" value="" required>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 3%; margin-right: 5%">
                            <button type="submit" id="verifySubmit" class="btn btn-success pull-right" style="margin-right:10px">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>`;
    $('body').append(verifyAdminModal);
    $('#verifyAdminModal').modal('toggle');
}

$('body').on('submit', '#verifyAdminForm', function (event) {
    $('#verifySubmit').addClass('disabled');
    $.post('verify_admin.php', $(this).serialize(), function (data) {

        if (data['status']) {
            callback();
            target = null;
        } else {
            let info = '';
            if (data['err'] === 'VALIDATION_ERROR') {
                info = '<div class="alert alert-dismissible alert-danger text-center">' +
                        'Validation error occurred</div>';
            } else if (data['err'] === 'FAILURE') {
                info = '<div class="alert alert-dismissible alert-danger text-center">' +
                        '<strong>Incorrect Password</div>';
            }
            reverseSwitch(target);
            $('#info').html(info).stop().slideDown().delay(3000).slideUp();
        }
        $('#verifyAdminModal').modal('toggle').find('input').val('');
        $('#verifySubmit').removeClass('disabled');
    }, 'JSON').fail(function (data) {
        $('#sms_info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                '<strong>Oops!</strong> Shit happened. Please try again later.</div>')
                .slideDown().delay(3000).slideUp();
        $('#verifySubmit').removeClass('disabled');
    });
    event.preventDefault();
});

$('body').on('hidden.bs.modal', '#verifyAdminModal', function (e) {
    reverseSwitch(target);
});