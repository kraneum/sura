/*jslint browser: true*/
/*global $ alert working_alert */

$(document).ready(function () {

    $('body').on('switchChange.bootstrapSwitch', 'input[name="approved"]', function (event, state) {
        let _this = $(this);
        target = $(this);

        callback = function () {
            working_alert.start();
            var formData = {
                'agentid': _this.data('agentid'),
                'state': state
            };
            let name = _this.data('agentname');
            let button = _this;

            $.post('approve_agent.php', formData, function (data) {
                data = JSON.parse(data);
                var info = '';
                if (data['status']) {
                    let type = state ? ' approved' : ' disapproved';
                    info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                            name + type + ' successfully</div>';
                    button.closest('tr').stop().fadeOut(500, function () {
                        button.closest('tr').toggleClass('alert-danger');
                        button.closest('tr').stop().fadeIn(500);

                        let counter = +$('#unapprvd_merchants_ct').html();
                        if (state) {
                            $('#unapprvd_merchants_ct').html(--counter);
                            counter === 0 ? $('#unapprvd_merchants_ct').hide() : '';

                        } else {
                            $('#unapprvd_merchants_ct').html(++counter);
                            $('#unapprvd_merchants_ct').show();
                        }
                    });
                } else {
                    if (data['err'] === 'VALIDATION_ERROR') {
                        info = '<div class="alert alert-dismissible alert-danger text-center">' +
                                'Validation error occurred</div>';
                    } else if (data['err'] === 'ERROR_OCCURRED') {
                        info = '<div class="alert alert-dismissible alert-danger text-center">' +
                                '<strong>Error Occurred!</strong> Please try again later</div>';
                    }
                }
                $('#info').html(info).stop().slideDown().delay(3000).slideUp();
                working_alert.finish();

            }).fail(function (data) {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<strong>Oops!</strong> Shit happened. Please try again later.</div>')
                        .slideDown().delay(3000).slideUp();
            });

            $("html, body").animate({scrollTop: 0}, "fast");
        };

        displayVerifyModal();

        event.preventDefault();
    });

    $('body').on('click', 'button.editBtn', function () {
        let _this = $(this);

        callback = function () {

            working_alert.start();

            $('#pagination').hide();

            var formData = {
                'agentid': _this.data('agentid')
            };

            $('div#primary').fadeOut(500);
            $('div#secondary').load("views/edit_agent.php", formData, function () {
                $(this).fadeIn(500);
                working_alert.finish();
            });
        };

        displayVerifyModal();
    }); // for editing an agent

    searchInput.on('keyup', function () {
        let value = $(this).val();
        delay(function () {
            fetchAgentByName(value, 0);
        }, 500);
    });

    $('input#search').on('input', function (e) {
        if ($(this).val().length < 3) {
            if (re_eval) {
                fetchAgents(limit, 0);
            }
        }

    }); // on value. length < 3, repopulate the agents' table

    ///////////////////////////////////////////////////

    fetchPageCount(); // to populate agent table on load

    ///////////////////////////////////////////////////

    function fetchPageCount(collection) {
        let api = '../php/suracommands.php?command=get_agents_count';

        $.getJSON(api, function (response) {

            $('#pagination').pagination({
                items: response.total,
                itemsOnPage: limit,
                cssStyle: 'light-theme',
                edges: 2,
                onPageClick: function (pageNumber) {
                    fetchAgents(limit, limit * (pageNumber - 1), searchInput.val());
                }
            });

            fetchAgents(limit, 0);
        });
    }

    function fetchAgents(limit, offset, spec = '') {
        working_alert.start();

        re_eval = false;

        let api = '../php/suracommands.php?command=get_all_agents&limit=' + limit + '&offset=' + offset + '&spec=' + spec;

        $.getJSON(api, function (data) {
            displayAgents(data);

            working_alert.finish();

            $("[name='validated']").bootstrapSwitch(options);
            $("[name='approved']").bootstrapSwitch(options);

        });
    }

    function fetchAgentByName(spec, offset) {
        working_alert.start();

        let api = '../php/suracommands.php?command=get_all_agents&limit=' + limit + '&offset=' + offset + '&spec=' + spec;

        $.getJSON(api, function (data) {

            displayAgents(data);

            working_alert.finish();

            $("[name='approved']").bootstrapSwitch(options);
        });
    }


    function displayAgents(data) {
        let content = '';

        data.forEach(function (agent) {
            content += '<tr id="tr-' + agent.id + '"  class="' + (agent.active === 'FALSE' ? 'alert-danger' : "") + '" >' +
                    '<td>' + agent.id + '</td>' +
                    '<td id="fname">' + agent.f_name + '</td>' +
                    '<td id="mname">' + agent.m_name + '</td>' +
                    '<td id="lname">' + agent.l_name + '</td>' +
                    '<td id="gender">' + agent.gender + '</td>' +
                    '<td id="dob">' + agent.dob + '</td>' +
                    '<td id="email">' + agent.email + '</td>' +
                    '<td id="phone">' + agent.phone + '</td>' +
                    '<td>' + moment(agent.date_joined).format('ddd, MMM Do YYYY, h:mm A') + '</td>' +
                    '<td>' + moment(agent.last_login).format('ddd, MMM Do YYYY, h:mm A') + '</td>' +
                    '<td>' + (agent.points ? agent.points : '0') + '</td>' +
                    '<td>' +
                    '<input name="approved" type="checkbox" ' + (agent.active === 'TRUE' ? 'checked' : "") + ' ' +
                    'data-agentid="' + agent.id + '" data-agentname="' + agent.f_name + ' ' + agent.l_name + '"/> &nbsp;' +
                    '</td>' +
                    '<td>' +
                    '<button class="btn btn-success editBtn" title="Edit" data-agentid="' + agent.id + '">' +
                    '<i class="fa fa-edit"></i></button>' +
                    '</td>' +
                    '</tr>';
        });

        $('tbody').html(content);
        $('#pagination').show();
    }

});