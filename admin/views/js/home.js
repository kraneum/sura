'use strict';

//Stuff to export
var employees = {};

$(function () {
    //Keep file variables inside here
    var config = {
        recentActivitiesContainer: $('#recentActivitiesContainer'),
        recentActivityRowPrefix: 'rAR',
        recentActivityColPrefix: 'rAC',
        loadRecentActivitiesDownBtnId: 'loadRecentActivitiesDownBtn',
        loadRecentActivitiesDownId: 'loadRecentActivitiesDown',
        //activityIconColors: ['bg-primary', 'bg-secondary', 'bg-success', 'bg-info', 'bg-warning', 'bg-danger']
    };

    var globvars = {
        loadRecentActivityBusy: false,
        firstRecentActivityId: null,
        lastRecentActivityId: null,
        currentDateRow: null,
        //activityLastIconColor: -1,
        recentActivityIsLoaded: false
    };

    /**********************************************************************************************************************************/
    //Main
    /**********************************************************************************************************************************/
    setTimeout(function () {
        loadRecentActivity();
    }, 0);

    /**********************************************************************************************************************************/
    //Bind event handlers
    /**********************************************************************************************************************************/
    config.recentActivitiesContainer.on('click', '#' + config.loadRecentActivitiesDownBtnId, function () {
        loadDownRecentActivity();
    });

    /****************************************************************************************************************************/
    //Private functions
    /****************************************************************************************************************************/
    var loadRecentActivity = function () {
        fetchAndDisplayRecentActivity();
    };
    var loadUpRecentActivity = function () {
        fetchAndDisplayRecentActivity(globvars.firstRecentActivityId, 1);
    };
    var loadDownRecentActivity = function () {
        fetchAndDisplayRecentActivity(globvars.lastRecentActivityId, 0);
    };
    var displayRecentActivity = function (recentActivities, direction) {
        if (recentActivities.length) {
            !globvars.recentActivityIsLoaded && (config.recentActivitiesContainer.html(''), globvars.recentActivityIsLoaded = true);

            var activityString, dateObj, meridian, hr, min, displayHTML = '', lastDateRow, dateRow, activityRowBeginIndex, activityRowEndIndex, activityRowBeginString, activityRowEndString, activityRowId, activityColId, months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];

            $('#' + config.loadRecentActivitiesDownId).remove();

            for (var idx = 0, recentActivity, recentActivitiesCt = recentActivities.length; idx < recentActivitiesCt; ++idx) {
                recentActivity = recentActivities[idx];

                dateObj = new Date(recentActivity.datetimezone);

                dateRow = dateObj.toDateString().replace(new RegExp(' ', 'g'), '_');

                activityRowId = config.recentActivityRowPrefix + dateRow;
                activityColId = config.recentActivityColPrefix + dateRow;

                activityRowBeginString = '<div class="row" id="' + activityRowId + '"><div class="col-xs-1" style="padding-right: 0;"><div id="date" class="text-center pull-right" style="padding-top: 10px;"><span class="date_mobile" style="font-size: 16px; letter-spacing: 1px;">' + dateObj.getFullYear() + '</span><br><span style="font-size: 12px; letter-spacing: 1px;">' + months[dateObj.getMonth()] + ' ' + dateObj.getDate() + '</span></div></div><div class="col-xs-11"><div id="' + activityColId + '" class="timeline-centered">';
                activityRowEndString = '</div></div></div>';

                meridian = dateObj.getHours() > 12 ? (hr = dateObj.getHours() - 12) && 'pm' : (hr = dateObj.getHours() || 1) && 'am';
                min = dateObj.getMinutes();
                min < 10 && (min = '0' + min);

                activityString = '<article class="timeline-entry"><div class="timeline-entry-inner"><div class="timeline-icon bg-info"><i class="entypo-feather"></i></div><div class="timeline-label"><h2>' + recentActivity.summary.toUpperCase().replace(new RegExp('_', 'g'), ' ') + '</h2><p>' + display_activity_summary[recentActivity.summary](recentActivity, hr + ':' + min + meridian) + '</p></div></div></article>';

                if (document.getElementById(activityRowId)) {
                    !direction ? $('#' + activityColId).append(activityString) : $('#' + activityColId).prepend(activityString);
                } else if (lastDateRow === dateRow) {
                    if (!direction) {
                        activityRowEndIndex = displayHTML.lastIndexOf(activityRowEndString);
                        displayHTML = displayHTML.substring(0, activityRowEndIndex) + activityString + activityRowEndString;
                    } else {
                        activityRowBeginIndex = displayHTML.indexOf(activityRowBeginString);
                        displayHTML = displayHTML.substring(0, activityRowBeginIndex + activityRowBeginString.length) + activityString + displayHTML.slice(activityRowBeginIndex + activityRowBeginString.length);
                    }
                } else {
                    displayHTML += activityRowBeginString + activityString + activityRowEndString;
                }

                lastDateRow = dateRow;
            }

            displayHTML.length && !direction ? config.recentActivitiesContainer.append(displayHTML) : config.recentActivitiesContainer.prepend(displayHTML);
            $('#' + activityColId).append('<div class="timeline-centered" id="' + config.loadRecentActivitiesDownId + '"><article class="timeline-entry begin"><div class="timeline-entry-inner"><div id="' + config.loadRecentActivitiesDownBtnId + '" class="timeline-icon" style="-webkit-transform: rotate(-90deg); -moz-transform: rotate(-90deg);background-color:blue;cursor:pointer;"><i class="entypo-flight fa fa-plus" style="position: relative; top:-9px;color:#f5f5f6;"></i></div></div></article></div>');
        } else {
            !config.recentActivitiesContainer.html().length && config.recentActivitiesContainer.html('<div class="alert alert-info">No recent activity</div>');
        }
    };

    var fetchAndDisplayRecentActivity = function (id, direction) {
        if (globvars.loadRecentActivityBusy) {
            return;
        }

        globvars.loadRecentActivityBusy = true;
        working_alert.start();

        $.ajax({
            type: "POST",
            url: "get_recent_activity.php",
            data: JSON.stringify({id: id, direction: direction}),
            success: function (data, textStatus, jqXHR) {
                if (textStatus === 'success') {
                    if (!data.err) {
                        var firstLoad = !globvars.lastRecentActivityId;

                        data.result.length && (id ? !direction ? globvars.lastRecentActivityId = data.result[data.result.length - 1].id : globvars.firstRecentActivityId = data.result[0].id : globvars.firstRecentActivityId = data.result[0].id, globvars.lastRecentActivityId = data.result[data.result.length - 1].id);

                        displayRecentActivity(data.result, direction);
                        //Scroll to the bottom of the page
                        !firstLoad && window.scrollTo(0, document.body.scrollHeight);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {

            },
            complete: function () {
                globvars.loadRecentActivityBusy = false;
                working_alert.finish();
            },
            dataType: 'JSON'
        });
    };

    var display_activity_summary = {
        validate_insurer: function (activity_summary, time) {
            return activity_summary.admin + ' validated <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, approve_insurer: function (activity_summary, time) {
            return activity_summary.admin + ' approved <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, invalidate_insurer: function (activity_summary, time) {
            return activity_summary.admin + ' invalidated <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, disapprove_insurer: function (activity_summary, time) {
            return activity_summary.admin + ' disapproved <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, edit_insurer: function (activity_summary, time) {
            return activity_summary.admin + ' edited <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, validate_pension: function (activity_summary, time) {
            return activity_summary.admin + ' validated <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, approve_pension: function (activity_summary, time) {
            return activity_summary.admin + ' approved <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, invalidate_pension: function (activity_summary, time) {
            return activity_summary.admin + ' invalidated <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, disapprove_pension: function (activity_summary, time) {
            return activity_summary.admin + ' disapproved <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, edit_pension: function (activity_summary, time) {
            return activity_summary.admin + ' edited <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, approve_agent: function (activity_summary, time) {
            return activity_summary.admin + ' approved <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, disapprove_agent: function (activity_summary, time) {
            return activity_summary.admin + ' disapproved <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, edit_agent: function (activity_summary, time) {
            return activity_summary.admin + ' edited <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, approve_user: function (activity_summary, time) {
            return activity_summary.admin + ' approved <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, disapprove_user: function (activity_summary, time) {
            return activity_summary.admin + ' disapproved <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, edit_user: function (activity_summary, time) {
            return activity_summary.admin + ' edited <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, approve_store: function (activity_summary, time) {
            return activity_summary.admin + ' approved <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, disapprove_store: function (activity_summary, time) {
            return activity_summary.admin + ' disapproved <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, edit_store: function (activity_summary, time) {
            return activity_summary.admin + ' edited <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, register_store: function (activity_summary, time) {
            return activity_summary.admin + ' registered <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, validate_merchant: function (activity_summary, time) {
            return activity_summary.admin + ' validated <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, approve_merchant: function (activity_summary, time) {
            return activity_summary.admin + ' approved <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, invalidate_merchant: function (activity_summary, time) {
            return activity_summary.admin + ' invalidated <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, disapprove_merchant: function (activity_summary, time) {
            return activity_summary.admin + ' disapproved <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, edit_merchant: function (activity_summary, time) {
            return activity_summary.admin + ' edited <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, disbursed_sms_credit: function (activity_summary, time) {
            return activity_summary.admin + ' disbursed ' + activity_summary.details + ' SMS credit to <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, approve_name: function (activity_summary, time) {
            return activity_summary.admin + ' approved ' + activity_summary.details + ' as the new name of <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, register_merchant: function (activity_summary, time) {
            return activity_summary.admin + ' registered merchant ' + activity_summary.entity + ' at ' + time;
        }, register_employee: function (activity_summary, time) {
            return activity_summary.admin + ' registered admin ' + activity_summary.entity + ' at ' + time;
        }, delete_merchant: function (activity_summary, time) {
            return activity_summary.admin + ' deleted merchant ' + activity_summary.entity + ' at ' + time;
        }, sent_notification: function (activity_summary, time) {
            return activity_summary.admin + ' sent notification(s) to ' + (activity_summary.entity).toUpperCase() + ' (ID: ' + activity_summary.details + ') at ' + time;
        }, sent_email: function (activity_summary, time) {
            return activity_summary.admin + ' sent a mail to ' + (activity_summary.entity).toUpperCase() + ' (' + activity_summary.details + ') at ' + time;
        }, base_insurers_update: function (activity_summary, time) {
            return activity_summary.admin + ' updated the base insurers <strong>' + activity_summary.entity + '</strong> at ' + time;
        }, create_policy: function (activity_summary, time) {
            return activity_summary.admin + ' created a policy titled "<strong>' + activity_summary.entity + '"</strong> at ' + time;
        }, edit_policy: function (activity_summary, time) {
            return activity_summary.admin + ' edited the policy titled "<strong>' + activity_summary.entity + '"</strong> at ' + time;
        }, activate_basic_policy: function (activity_summary, time) {
            return activity_summary.admin + ' activated the policy titled "<strong>' + activity_summary.entity + '</strong>" at ' + time;
        }, deactivate_basic_policy: function (activity_summary, time) {
            return activity_summary.admin + ' de-activated the policy titled "<strong>' + activity_summary.entity + '"</strong> at ' + time;
        }, delete_employee: function (activity_summary, time) {
            return activity_summary.admin + ' deleted user ' + activity_summary.entity + ' at ' + time;
        }, sms_price_change: function (activity_summary, time) {
            return activity_summary.admin + ' changed the SMS Price to &#8358;' + activity_summary.details + ' at ' + time;
        }, payment_commission: function (activity_summary, time) {
            return activity_summary.admin + ' changed the payment commission to ' + activity_summary.details + 'k at ' + time;
        }, logout: function (activity_summary, time) {
            return activity_summary.admin + ' loggedout user ' + activity_summary.entity + ' at ' + time;
        }, suspend: function (activity_summary, time) {
            return activity_summary.admin + ' suspended user ' + activity_summary.entity + ' at ' + time;
        }, approve: function (activity_summary, time) {
            var details = JSON.parse(activity_summary.details);
            return activity_summary.admin + ' approved merchant ' + activity_summary.entity + ' at ' + time;
        }, disapprove: function (activity_summary, time) {
            return activity_summary.admin + ' disapproved merchant ' + activity_summary.entity + ' at ' + time;
        }, approve_document: function (activity_summary, time) {
            var details = JSON.parse(activity_summary.details);
            return activity_summary.admin + ' approved the <a title="click to download this document" target="_blank" href="get_doc_file.php?i=' + details.merchant_id + '&n=' + details.document_name + '">' + details.document_name + '</a> of merchant ' + activity_summary.entity + ' at ' + time;
        }, reject_document: function (activity_summary, time) {
            var details = JSON.parse(activity_summary.details);
            return activity_summary.admin + ' rejected the <a title="click to download this document" target="_blank" href="get_doc_file.php?i=' + details.merchant_id + '&n=' + details.document_name + '">' + details.document_name + '</a> of merchant ' + activity_summary.entity + ' at ' + time;
        }, edit_employee: function (activity_summary, time) {
            var op = activity_summary.admin, details = JSON.parse(activity_summary.details), f_name_trans = {'display_picture_file_name': 'profile_picture'};

            op += ' edited user ' + activity_summary.entity + '.<br/>Changes: ';

            for (var detail in details) {
                f_name_trans[detail] && (detail = f_name_trans[detail]);
                op += '<br/>' + detail.replace(new RegExp('_', 'g'), ' ') + (detail !== 'user_permissions' ? (details[detail] ? ' to ' + details[detail] : '') : (function () {
                    var perms = {'VIEW': 1, 'CREATE': 2, 'UPDATE': 4, 'DELETE': 8}, newPerms = '';
                    for (var perm in perms) {
                        (details[detail] & perms[perm]) && (newPerms += (newPerms.length ? ', ' : '') + perm.toLowerCase());
                    }
                    return ' to ' + newPerms;
                })());
            }

            return op += '.<br>At ' + time;
//        }, edit_merchant: function (activity_summary, time) {
//            var op = activity_summary.admin, details = JSON.parse(activity_summary.details), f_name_trans = {'display_picture_file_name': 'logo'};
//
//            op += ' edited user ' + activity_summary.entity + '.<br/>Changes: ';
//
//            for (var detail in details) {
//                f_name_trans[detail] && (detail = f_name_trans[detail]);
//                op += '<br/>' + detail.replace(new RegExp('_', 'g'), ' ') + (detail !== 'user_permissions' ? (details[detail] ? ' to ' + details[detail] : '') : (function () {
//                    var perms = {'VIEW': 1, 'CREATE': 2, 'UPDATE': 4, 'DELETE': 8}, newPerms = '';
//                    for (var perm in perms) {
//                        (details[detail] & perms[perm]) && (newPerms += (newPerms.length ? ', ' : '') + perm.toLowerCase());
//                    }
//                    return ' to ' + newPerms;
//                })());
//            }
//
//            return op += '.<br>At ' + time;
        }
    };

    /////////////////////////////////////////////////////////////////////////////

    var myPieChart = null;
    var myLineChart = null;

    $('.tab-content').find('.input-group input').datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true
    });

    drawPieChart(null);
    drawLineChart(null);

    $('body').on('submit', '#pieChartFilter', function (event) {
        drawPieChart($('#pieChartFilter').serialize());
        event.preventDefault();
    });
    
    $('body').on('submit', '#lineChartFilter', function (event) {
        drawLineChart($('#lineChartFilter').serialize());
        event.preventDefault();
    });

    function toMonth($num, $type) {
        switch ($num) {
            case '1':
                return $type === 'long' ? 'January' : 'Jan';
            case '2':
                return $type === 'long' ? 'February' : 'Feb';
            case '3':
                return $type === 'long' ? 'March' : 'Mar';
            case '4':
                return $type === 'long' ? 'April' : 'Apr';
            case '5':
                return $type === 'long' ? 'May' : 'May';
            case '6':
                return $type === 'long' ? 'June' : 'Jun';
            case '7':
                return $type === 'long' ? 'July' : 'Jul';
            case '8':
                return $type === 'long' ? 'August' : 'Aug';
            case '9':
                return $type === 'long' ? 'September' : 'Sep';
            case '10':
                return $type === 'long' ? 'October' : 'Oct';
            case '11':
                return $type === 'long' ? 'November' : 'Nov';
            case '12':
                return $type === 'long' ? 'December' : 'Dec';
            default:
                return "Incorrect Month Value";
        }
    }

    function getSummaryData() {
        $('.tab-pane.active').append(spinner.el);
        $('.tab-content').addClass('overlay');

        var content = '';
        var count;

        var api = '../php/suracommands.php?command=get_pie_chart_data_insurer&web';
        $.get(api, '', function (data) {
            data = JSON.parse(data);
            count = data[data.length - 1]['count'];
            for (var i = 0; i < data.length - 1; i++) {
                var title = data[i]['title'];
                var num_users = data[i]['num_users'];

                content += '<tr>';
                content += '<td class="text-center">' + (i + 1) + '</td>';
                content += '<td class="text-center">' + title + '</td>';
                content += '<td class="text-center">' + num_users + '</td>';
                content += '</tr>';
            }
            content += '<tr>';
            content += '<td class="text-center"></td>';
            content += '<td class="text-center text-italic" style="font-weight: bold">Total</td>';
            content += '<td class="text-center text-italic">' + count + '</td>';
            content += '</tr>';
        }).done(function () {
            $(spinner.el).remove();
            $('.tab-content').removeClass('overlay');
            $('#table').show();
            $('tbody').html(content);
            $("#num_range_b").ionRangeSlider({
                type: "double",
                grid: true,
                min: 0,
                max: count,
                from: 0,
                to: 0
            });
            $("#num_range_l").ionRangeSlider({
                type: "double",
                grid: true,
                min: 0,
                max: count,
                from: 0,
                to: 0
            });
            drawPieChart(null);
            drawLineChart(null);
        });
    }

    function drawPieChart(filterData) {
        if (myPieChart !== null) {
            myPieChart.destroy();
        }

        var dataset = {
            labels: [],
            datasets: [
                {
                    label: "TeamA Score",
                    data: [],
                    backgroundColor: [],
                    borderWidth: [1, 1, 1, 1, 1]
                }
            ]
        };

        var api = '../php/suracommands.php?command=get_pie_chart_data_admin';
        $.getJSON(api, filterData, function (data) {
//            count = data[data.length - 1]['count'];
            for (var i = 0; i < data.length - 1; i++) {
                dataset.labels.push(data[i]['name']);
                dataset.datasets[0].data.push(data[i]['points']);
                dataset.datasets[0].backgroundColor.push(getRandomColor());
            }
        }).done(function () {
            var ctx = document.getElementById("myPieChart");
            myPieChart = new Chart(ctx, {
                type: 'pie',
                data: dataset,
                options: {
                    responsive: true,
                    cutoutPercentage: 10,
                    title: {
                        display: true,
                        text: 'Amount of point purchases per merchant from inception',
                        position: 'bottom',
                        fullWidth: true,
                        fontSize: 14,
                        fontColor: '#1c4b70'
                    },
                    legend: {
                        display: true,
                        position: "right",
                        labels: {
                            fontColor: "#333",
                            fontSize: 16
                        }
                    }
                }
            });
        });
    }

    function drawLineChart(filterData) {
        if (myLineChart !== null) {
            myLineChart.destroy();
        }
        var dataset = [];

        var api = '../php/suracommands.php?command=get_line_chart_data_admin';
        $.getJSON(api, filterData, function (data) {
//            var count = data[data.length - 1]['total_signups'];
//            console.log(count);
            for (var i = 0; i < data.length; i++) {
                var tempLine = {
                    label: null,
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    backgroundColor: null
                };
                var purchases = data[i]['purchases'];
                tempLine.label = data[i]['name'];
//                if (signups.length > 0) {
                for (var j = 0; j < purchases.length; j++) {
                    var month_val = new Date(purchases[j]['date_transacted']).getMonth();
                    tempLine.data[month_val] += parseInt(purchases[j]['point_worth']);
                    tempLine.backgroundColor = getRandomColor();
                }
//                }
                dataset.push(tempLine);
            }

        }).done(function () {
            var ctx = document.getElementById("myLineChart");
            myLineChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                    datasets: dataset
                },
                options: {
                    scales: {
                        yAxes: [{
//                                stacked: true,
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    },
                    title: {
                        display: true,
                        text: 'Amount generated from monthly user signups per Insurance Company',
                        position: 'bottom',
                        fullWidth: true,
                        fontSize: 14,
                        fontColor: '#1c4b70'
                    },
                    legend: {
                        display: true,
                        position: 'bottom',
                        labels: {
                            fontColor: 'rgb(255, 99, 132)'
                        }
                    },
                    responsiveAnimationDuration: 500
                }
            });
        });
    }

    function getRandomColor() {
//        var letters = '0123456789ABCDEF'.split('');
//        var color = '#';
//        for (var i = 0; i < 6; i++) {
//            color += letters[Math.floor(Math.random() * 16)];
//        }

        var color = 'rgba(';
        for (var i = 0; i < 3; i++) {
            i > 0 ? color += ',' : '';
            color += Math.floor(Math.random() * 255);
        }
        color += ',0.5)';
        return color;
    }

});