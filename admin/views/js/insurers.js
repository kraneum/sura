/*jslint browser: true*/
/*global $ alert working_alert */

$(document).ready(function () {

    $('body').on('switchChange.bootstrapSwitch', 'input[name="validated"]', function (event, state) {
        let _this = $(this);
        target = $(this);

        callback = function () {
            working_alert.start();
            var formData = {
                'insurerid': _this.data('insurerid'),
                'state': state
            };
            let name = _this.data('insurername');
            let button = _this;

            $.post('validate_insurer.php', formData, function (data) {
                data = JSON.parse(data);
                var info = '';
                if (data['status']) {
                    let type = state ? ' validated' : ' invalidated';
                    info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                            name + type + ' successfully</div>';
                    button.closest('tr').stop().fadeOut(500, function () {
                        button.closest('tr').toggleClass('alert-warning');
                        button.closest('tr').stop().fadeIn(500);
                    });

                    let counter = +$('#unapprvd_merchants_ct').html();
                    if (state) {
                        $('#unapprvd_merchants_ct').html(--counter);
                        counter === 0 ? $('#unapprvd_merchants_ct').hide() : '';

                    } else {
                        $('#unapprvd_merchants_ct').html(++counter);
                        $('#unapprvd_merchants_ct').show();
                    }
                } else {
                    if (data['err'] === 'VALIDATION_ERROR') {
                        info = '<div class="alert alert-dismissible alert-danger text-center">' +
                                'Validation error occurred</div>';
                    } else if (data['err'] === 'ERROR_OCCURRED') {
                        info = '<div class="alert alert-dismissible alert-danger text-center">' +
                                '<strong>Error Occurred!</strong> Please try again later</div>';
                    }
                }
                $('#info').html(info).stop().slideDown().delay(3000).slideUp();
                working_alert.finish();

            }).fail(function (data) {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<strong>Oops!</strong> Shit happened. Please try again later.</div>')
                        .slideDown().delay(3000).slideUp();
            });

            $("html, body").animate({scrollTop: 0}, "fast");
        };

        displayVerifyModal();

        event.preventDefault();
    }); // for validating or invalidating an insurer

    $('body').on('switchChange.bootstrapSwitch', 'input[name="approved"]', function (event, state) {
        let _this = $(this);
        target = $(this);
        callback = function () {
            working_alert.start();
            var formData = {
                'insurerid': _this.data('insurerid'),
                'state': state
            };
            let name = _this.data('insurername');
            let button = _this;

            $.post('approve_insurer.php', formData, function (data) {
                data = JSON.parse(data);
                var info = '';
                if (data['status']) {
                    let type = state ? ' approved' : ' disapproved';
                    info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                            name + type + ' successfully</div>';
                    button.closest('tr').stop().fadeOut(500, function () {
                        button.closest('tr').toggleClass('alert-danger');
                        button.closest('tr').stop().fadeIn(500);

                        let counter = +$('#unapprvd_merchants_ct').html();
                        if (state) {
                            $('#unapprvd_merchants_ct').html(--counter);
                            counter === 0 ? $('#unapprvd_merchants_ct').hide() : '';

                        } else {
                            $('#unapprvd_merchants_ct').html(++counter);
                            $('#unapprvd_merchants_ct').show();
                        }
                    });
                } else {
                    if (data['err'] === 'VALIDATION_ERROR') {
                        info = '<div class="alert alert-dismissible alert-danger text-center">' +
                                'Validation error occurred</div>';
                    } else if (data['err'] === 'ERROR_OCCURRED') {
                        info = '<div class="alert alert-dismissible alert-danger text-center">' +
                                '<strong>Error Occurred!</strong> Please try again later</div>';
                    }
                }
                $('#info').html(info).stop().slideDown().delay(3000).slideUp();
                working_alert.finish();

            }).fail(function (data) {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<strong>Oops!</strong> Shit happened. Please try again later.</div>')
                        .slideDown().delay(3000).slideUp();
            });

            $("html, body").animate({scrollTop: 0}, "fast");
        };

        displayVerifyModal();

        event.preventDefault();
    }); // for approving or disapproving an insurer

    $('body').on('click', 'button.editBtn', function () {
        let _this = $(this);

        callback = function () {
            working_alert.start();

            $('#pagination').hide();

            var formData = {
                'insurerid': _this.data('insurerid')
            };

            $('div#primary').fadeOut(500);
            $('div#secondary').load("views/edit_insurer.php", formData, function () {
                $(this).fadeIn(500);
                working_alert.finish();
            });
        };

        displayVerifyModal();
    }); // for editing an insurer

    $('body').on('click', 'td.name_change', function () {
        let _this = $(this);

        callback = function () {
            _this.addClass('name-change-active');

            let insurerid = _this.closest('tr').data('insurerid');
            let current_name = _this.html();
            let changed_name = _this.data('namechange');

            let nameChangeModal = `<div class="modal fade" id="nameChangeModal" tabindex="-1" role="dialog" aria-labelledby="nameChangeModalLabel">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Approve Name Change</h4>
                            </div>
                            <form id="nameChangeForm">
                                <div class="modal-body">
                                    <input type="hidden" name="entityid" value="` + insurerid + `"/>
                                    <input type="hidden" name="entity" value="insurer"/>
                                    <div class="row" style="margin-top: 3%;">
                                        <div class="form-group">
                                            <label for="current_name" class="col-sm-3 control-label">Current Name</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" id="current_name" name="current_name" type="text" readonly value="` + current_name + `">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 3%;">
                                        <div class="form-group">
                                            <label for="changed_name" class="col-sm-3 control-label">Proposed Name</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" id="changed_name" name="changed_name" type="text" readonly value="` + changed_name + `">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer"style="margin-top: 5%;">
                                    <button type="submit" class="btn btn-success">Approve</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>`;

            $('body').append(nameChangeModal);
            $('#nameChangeModal').modal('toggle');
        };

        displayVerifyModal();
    });

    $('body').on('hidden.bs.modal', '#nameChangeModal', function (e) {
        $('td#name').removeClass('name-change-active');
        $('#nameChangeModal').remove();
    });

    $('body').on('submit', '#nameChangeForm', function (event) {

        let formData = $(this).serialize();

        $.post('approve_name.php', formData, function (data) {
            data = JSON.parse(data);
            var info = '';
            if (data['status']) {
                info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                        'New name Approved Succesfully</div>';
                $('td#name.name_change').html($('input[name=changed_name]').val()).removeClass('name_change');
            } else {
                if (data['err'] === 'VALIDATION_ERROR') {
                    info = '<div class="alert alert-dismissible alert-danger text-center">' +
                            'Validation error occurred</div>';
                } else if (data['err'] === 'ERROR_OCCURRED') {
                    info = '<div class="alert alert-dismissible alert-danger text-center">' +
                            '<strong>Error Occurred!</strong> Please try again later</div>';
                }
            }
            $('#info').html(info).stop().slideDown().delay(3000).slideUp();
            $('#nameChangeModal').modal('toggle');

        }).fail(function (data) {
            $('#sms_info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                    '<strong>Oops!</strong> Shit happened. Please try again later.</div>')
                    .slideDown().delay(3000).slideUp();
        });

        $("html, body").animate({scrollTop: 0}, "fast");

        event.preventDefault();
    }); // for approving proposed name

    $('body').on('click', '.disburseBtn', function () {
        let _this = $(this);

        callback = function () {
            $("#disburseForm input[name=insurerid]").val(_this.data('insurerid'));
            $('#disburseModal').modal('toggle');
        };

        displayVerifyModal();
    }); // for displaying the sms disburse form

    $("#disburseForm").on('submit', function (event) {

        let formData = $(this).serialize();

        let insurerid = $(this).find('input[name=insurerid]').val();
        let amount = $(this).find('#amount').val();

        $.post('disburse_sms_credit.php', formData, function (data) {
            var info = '';
            if (data['status']) {
                info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                        'SMS credit disbursed successfully</div>';
                $('#tr-' + insurerid).find('td#sms_wallet').html(data['sms_wallet']);
            } else {
                if (data['err'] === 'VALIDATION_ERROR') {
                    info = '<div class="alert alert-dismissible alert-danger text-center">' +
                            'Validation error occurred</div>';
                } else if (data['err'] === 'ERROR_OCCURRED') {
                    info = '<div class="alert alert-dismissible alert-danger text-center">' +
                            '<strong>Error Occurred!</strong> Please try again later</div>';
                } else if (data['err'] === 'NO_PENDING_TRANSACTIONS') {
                    info = '<div class="alert alert-dismissible alert-danger text-center">' +
                            '<strong>Operation failed!</strong> There are no pending transactions</div>';
                } else if (data['err'] === 'INVALID_TRANSACTION') {
                    info = '<div class="alert alert-dismissible alert-danger text-center">' +
                            '<strong>Operation failed!</strong> Please confirm last pending transaction</div>';
                }
            }
            $('#info').html(info).stop().slideDown().delay(3000).slideUp();
            $('#disburseModal').modal('toggle');

        }, 'JSON').fail(function (data) {
            $('#sms_info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                    '<strong>Oops!</strong> Shit happened. Please try again later.</div>')
                    .slideDown().delay(3000).slideUp();
        });

        $(this).find('input').val('');
        $("html, body").animate({scrollTop: 0}, "fast");

        event.preventDefault();
    }); // for disbursing sms points

    searchInput.on('keyup', function () {
        let value = $(this).val();
        delay(function () {
            fetchInsurerByName(value, 0);
        }, 500);
    });

    $('input#search').on('input', function (e) {
        if ($(this).val().length < 3) {
            if (re_eval) {
                fetchInsurers(limit, 0);
            }
        }

    }); // on value. length < 3, repopulate the insurers' table

    ////////////////////////////////////////////////////////

    fetchPageCount(); // to populate insurer table on load

    ////////////////////////////////////////////////////////

    function fetchPageCount(collection) {
        let api = '../php/suracommands.php?command=get_insurers_count';

        $.getJSON(api, function (response) {

            $('#pagination').pagination({
                items: response.total,
                itemsOnPage: limit,
                cssStyle: 'light-theme',
                edges: 2,
                onPageClick: function (pageNumber) {
                    fetchInsurers(limit, limit * (pageNumber - 1), searchInput.val());
                }
            });

            fetchInsurers(limit, 0);
        });
    }

    function fetchInsurers(limit, offset, spec = '') {
        working_alert.start();

        re_eval = false;

        let api = '../php/suracommands.php?command=get_all_insurers&limit=' + limit + '&offset=' + offset + '&spec=' + spec;

        $.getJSON(api, function (data) {
            displayInsurers(data);

            working_alert.finish();

            $("[name='validated']").bootstrapSwitch(options);
            $("[name='approved']").bootstrapSwitch(options);

        });
    }

    function fetchInsurerByName(spec, offset) {
        working_alert.start();

        let api = '../php/suracommands.php?command=get_all_insurers&limit=' + limit + '&offset=' + offset + '&spec=' + spec;

        $.getJSON(api, function (data) {

            displayInsurers(data);

            working_alert.finish();

            $("[name='approved']").bootstrapSwitch(options);
            $("[name='validated']").bootstrapSwitch(options);
        });
    }

    function displayInsurers(data) {
        let content = '';

        data.forEach(function (insurer) {
            content += '<tr id="tr-' + insurer.id + '" data-insurerid = "' + insurer.id + '"  class="' + (insurer.approved === 'FALSE' ? 'alert-danger' : "") + ' ' + (insurer.validated === 'FALSE' ? 'alert-warning' : "") + '" >' +
                    '<td>' + insurer.id + '</td>';

            if (!insurer.name_change) {
                content += '<td id="name">' + insurer.name + '</td>';
            } else {
                content += '<td id="name" class="name_change" data-namechange ="' + insurer.name_change + '">' + insurer.name + '</td>';
            }

            content += '<td id="desc">' + (insurer.write_up < 20 ? insurer.write_up : insurer.write_up.substr(0, 20) + '...') + '</td>' +
                    '<td id="rc">' + insurer.rc_number + '</td>' +
                    '<td id="email">' + insurer.email + '</td>' +
                    '<td id="phone">' + insurer.phone + '</td>' +
                    '<td>' + insurer.type_name + '</td>' +
                    '<td id="acc">' + insurer.account_no + '</td>' +
                    '<td>' + insurer.redeemable_points + '</td>' +
                    '<td id="sms_wallet">' + insurer.sms_wallet + '</td>' +
                    '<td>' +
                    `<div class="btn-group">
                        <a href="javascript:;" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-folder-open"></i></a>
                        <ul class="dropdown-menu">`;
            insurer.docs.forEach(function (doc) {
                content += '<li><a href="../insurer_docs/docs_id_' + insurer.id + '/' + doc + '" target="_blank">' + doc + '</a></li>';
            });
            content += '<li class="divider"></li>';
            insurer.name_docs.forEach(function (name_doc) {
                content += '<li><a href="../insurer_docs/docs_id_' + insurer.id + '/Change of Name Docs/' + name_doc + '" target="_blank">' + name_doc + '</a></li>';
            });
            content += `</ul>
                      </div>` +
                    '</td>' +
                    '<td>' +
                    '<div>' +
                    '<input name="validated" type="checkbox" ' + (insurer.validated === 'TRUE' ? 'checked' : "") + ' ' +
                    'data-insurerid="' + insurer.id + '" data-insurername="' + insurer.name + '"/>' +
                    '</div>' +
                    '</td>' +
                    '<td>' +
                    '<input name="approved" type="checkbox" ' + (insurer.approved === 'TRUE' ? 'checked' : "") + ' ' +
                    'data-insurerid="' + insurer.id + '" data-insurername="' + insurer.name + '"/>' +
                    '</td>' +
                    '<td>' +
                    '<button class="btn btn-success editBtn" title="Edit" data-insurerid="' + insurer.id + '">' +
                    '<i class="fa fa-edit"></i></button> &nbsp;' +
                    '</td>' +
                    '<td>' +
                    '<button class="btn btn-success disburseBtn" title="Disburse SMS" ' +
                    'data-insurerid="' + insurer.id + '">' +
                    '<i class="fa fa-gg"></i></button>' +
                    '</td>' +
                    '</tr>';
        });

        $('tbody').html(content);
        $('#pagination').show();
    }


});