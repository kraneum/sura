/*jslint browser: true*/
/*global $ alert working_alert */

$(document).ready(function () {

    $('body').on('switchChange.bootstrapSwitch', 'input[name="validated"]', function (event, state) {
        let _this = $(this);
        target = $(this);

        callback = function () {
            working_alert.start();
            var formData = {
                'merchantid': _this.data('merchantid'),
                'state': state
            };
            let name = _this.data('merchantname');
            let button = _this;

            $.post('validate_merchant.php', formData, function (data) {
                data = JSON.parse(data);
                var info = '';
                if (data['status']) {
                    let type = state ? ' validated' : ' invalidated';
                    info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                            name + type + ' successfully</div>';
                    button.closest('tr').stop().fadeOut(500, function () {
                        button.closest('tr').toggleClass('alert-warning');
                        button.closest('tr').stop().fadeIn(500);
                    });

                } else {
                    if (data['err'] === 'VALIDATION_ERROR') {
                        info = '<div class="alert alert-dismissible alert-danger text-center">' +
                                'Validation error occurred</div>';
                    } else if (data['err'] === 'ERROR_OCCURRED') {
                        info = '<div class="alert alert-dismissible alert-danger text-center">' +
                                '<strong>Error Occurred!</strong> Please try again later</div>';
                    }
                }
                $('#info').html(info).stop().slideDown().delay(3000).slideUp();
                working_alert.finish();

            }).fail(function (data) {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<strong>Oops!</strong> Shit happened. Please try again later.</div>')
                        .slideDown().delay(3000).slideUp();
            });

            $("html, body").animate({scrollTop: 0}, "fast");
        };

        displayVerifyModal();

        event.preventDefault();
    }); // for validating or invalidating an merchant

    $('body').on('switchChange.bootstrapSwitch', 'input[name="approved"]', function (event, state) {
        let _this = $(this);
        target = $(this);

        callback = function () {

            working_alert.start();
            var formData = {
                'merchantid': _this.data('merchantid'),
                'state': state
            };
            let name = _this.data('merchantname');
            let button = _this;

            $.post('approve_merchant.php', formData, function (data) {
                data = JSON.parse(data);
                var info = '';
                if (data['status']) {
                    let type = state ? ' approved' : ' disapproved';
                    info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                            name + type + ' successfully</div>';
                    button.closest('tr').stop().fadeOut(500, function () {
                        button.closest('tr').toggleClass('alert-danger');
                        button.closest('tr').stop().fadeIn(500);

                        let counter = +$('#unapprvd_merchants_ct').html();
                        if (state) {
                            $('#unapprvd_merchants_ct').html(--counter);
                            counter === 0 ? $('#unapprvd_merchants_ct').hide() : '';

                        } else {
                            $('#unapprvd_merchants_ct').html(++counter);
                            $('#unapprvd_merchants_ct').show();
                        }
                    });
                } else {
                    if (data['err'] === 'VALIDATION_ERROR') {
                        info = '<div class="alert alert-dismissible alert-danger text-center">' +
                                'Validation error occurred</div>';
                    } else if (data['err'] === 'ERROR_OCCURRED') {
                        info = '<div class="alert alert-dismissible alert-danger text-center">' +
                                '<strong>Error Occurred!</strong> Please try again later</div>';
                    }
                }
                $('#info').html(info).stop().slideDown().delay(3000).slideUp();
                working_alert.finish();

            }).fail(function (data) {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<strong>Oops!</strong> Shit happened. Please try again later.</div>')
                        .slideDown().delay(3000).slideUp();
            });

            $("html, body").animate({scrollTop: 0}, "fast");
        };

        displayVerifyModal();

        event.preventDefault();
    }); // for approving or disapproving an merchant

    $('body').on('click', 'button.editBtn', function () {
        let _this = $(this);
        callback = function () {

            working_alert.start();

            $('#pagination').hide();

            var formData = {
                'merchantid': _this.data('merchantid')
            };

            $('div#primary').fadeOut(500);
            $('div#secondary').load("views/edit_merchant.php", formData, function () {
                $(this).fadeIn(500);
                working_alert.finish();
            });
        };

        displayVerifyModal();
    }); // for editing an merchant

    $('body').on('click', 'td.name_change', function () {
        let _this = $(this);

        callback = function () {
            _this.addClass('name-change-active');

            let merchantid = _this.closest('tr').data('merchantid');
            let current_name = _this.html();
            let changed_name = _this.data('namechange');

            let nameChangeModal = `<div class="modal fade" id="nameChangeModal" tabindex="-1" role="dialog" aria-labelledby="nameChangeModalLabel">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Approve Name Change</h4>
                            </div>
                            <form id="nameChangeForm">
                                <div class="modal-body">
                                    <input type="hidden" name="entityid" value="` + merchantid + `"/>
                                    <input type="hidden" name="entity" value="merchants"/>
                                    <div class="row" style="margin-top: 3%;">
                                        <div class="form-group">
                                            <label for="current_name" class="col-sm-3 control-label">Current Name</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" id="current_name" name="current_name" type="text" readonly value="` + current_name + `">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 3%;">
                                        <div class="form-group">
                                            <label for="changed_name" class="col-sm-3 control-label">Proposed Name</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" id="changed_name" name="changed_name" type="text" readonly value="` + changed_name + `">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer"style="margin-top: 5%;">
                                    <button type="submit" class="btn btn-success">Approve</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>`;

            $('.body').append(nameChangeModal);
            $('#nameChangeModal').modal('toggle');
        };

        displayVerifyModal();

    });

    $('body').on('hidden.bs.modal', '#nameChangeModal', function (e) {
        $('td#name').removeClass('name-change-active');
        $('#nameChangeModal').remove();
    });

    $('body').on('submit', '#nameChangeForm', function (event) {

        let formData = $(this).serialize();

        $.post('approve_name.php', formData, function (data) {
            data = JSON.parse(data);
            var info = '';
            if (data['status']) {
                info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                        'New name Approved Succesfully</div>';
                $('td#name.name_change').html($('input[name=changed_name]').val()).removeClass('name_change');
            } else {
                if (data['err'] === 'VALIDATION_ERROR') {
                    info = '<div class="alert alert-dismissible alert-danger text-center">' +
                            'Validation error occurred</div>';
                } else if (data['err'] === 'ERROR_OCCURRED') {
                    info = '<div class="alert alert-dismissible alert-danger text-center">' +
                            '<strong>Error Occurred!</strong> Please try again later</div>';
                }
            }
            $('#info').html(info).stop().slideDown().delay(3000).slideUp();
            $('#nameChangeModal').modal('toggle');

        }).fail(function (data) {
            $('#sms_info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                    '<strong>Oops!</strong> Shit happened. Please try again later.</div>')
                    .slideDown().delay(3000).slideUp();
        });

        $("html, body").animate({scrollTop: 0}, "fast");

        event.preventDefault();
    }); // for approving proposed name

    searchInput.on('keyup', function () {
        let value = $(this).val();
        delay(function () {
            fetchMerchantByName(value, 0);
        }, 500);
    });

    $('input#search').on('input', function (e) {
        if ($(this).val().length < 3) {
            if (re_eval) {
                fetchMerchants(limit, 0);
            }
        }

    }); // on value. length < 3, repopulate the merchants' table

    ////////////////////////////////////////////////////////

    fetchPageCount(); // to populate merchant table on load
    
    ////////////////////////////////////////////////////////

    function fetchPageCount(collection) {
        let api = '../php/suracommands.php?command=get_merchants_count';

        $.getJSON(api, function (response) {

            $('#pagination').pagination({
                items: response.total,
                itemsOnPage: limit,
                cssStyle: 'light-theme',
                edges: 2,
                onPageClick: function (pageNumber) {
                    fetchMerchants(limit, limit * (pageNumber - 1), searchInput.val());
                }
            });

            fetchMerchants(limit, 0);
        });
    }

    function fetchMerchants(limit, offset, spec = '') {
        working_alert.start();

        re_eval = false;

        let api = '../php/suracommands.php?command=get_all_merchants&limit=' + limit + '&offset=' + offset + '&spec=' + spec;

        $.getJSON(api, function (data) {
            displayMerchants(data);

            working_alert.finish();

            $("[name='validated']").bootstrapSwitch(options);
            $("[name='approved']").bootstrapSwitch(options);

        });
    }

    function fetchMerchantByName(spec, offset) {
        working_alert.start();

        let api = '../php/suracommands.php?command=get_all_merchants&limit=' + limit + '&offset=' + offset + '&spec=' + spec;

        $.getJSON(api, function (data) {

            displayMerchants(data);

            working_alert.finish();

            $("[name='approved']").bootstrapSwitch(options);
            $("[name='validated']").bootstrapSwitch(options);
        });
    }

    function displayMerchants(data) {
        let content = '';

        data.forEach(function (merchant) {
            content += '<tr id="tr-' + merchant.id + '" data-merchantid = "' + merchant.id + '"  class="' + (merchant.deleted === '1' ? 'alert-danger' : "") + ' ' + (merchant.approved === '0' ? 'alert-warning' : "") + '" >' +
                    '<td>' + merchant.id + '</td>';

            if (!merchant.name_change) {
                content += '<td id="name">' + merchant.name + '</td>';
            } else {
                content += '<td id="name" class="name_change" data-namechange ="' + merchant.name_change + '">' + merchant.name + '</td>';
            }

            content += '<td id="desc">' + (merchant.write_up.length < 20 ? merchant.write_up : merchant.write_up.substr(0, 20) + '...') + '</td>' +
                    '<td id="rc">' + merchant.rc_number + '</td>' +
                    '<td id="email">' + merchant.contact_email + '</td>' +
                    '<td id="phone">' + merchant.contact_phone + '</td>' +
                    '<td id="acc">' + merchant.point_balance + '</td>' +
                    '<td>' +
                    `<div class="btn-group">
                        <a href="javascript:;" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-folder-open"></i></a>
                        <ul class="dropdown-menu">`;
            merchant.docs.forEach(function (doc) {
                content += '<li><a href="../merchant_docs/docs_id_' + merchant.id + '/' + doc + '" target="_blank">' + doc + '</a></li>';
            });
            content += '<li class="divider"></li>';
            merchant.name_docs.forEach(function (name_doc) {
                content += '<li><a href="../merchant_docs/docs_id_' + merchant.id + '/Change of Name Docs/' + name_doc + '" target="_blank">' + name_doc + '</a></li>';
            });
            content += `</ul>
                      </div>` +
                    '</td>' +
                    '<td>' +
                    '<div>' +
                    '<input name="validated" type="checkbox" ' + (merchant.approved === '1' ? 'checked' : "") + ' ' +
                    'data-merchantid="' + merchant.id + '" data-merchantname="' + merchant.name + '"/>' +
                    '</div>' +
                    '</td>' +
                    '<td>' +
                    '<input name="approved" type="checkbox" ' + (merchant.deleted === '0' ? 'checked' : "") + ' ' +
                    'data-merchantid="' + merchant.id + '" data-merchantname="' + merchant.name + '"/>' +
                    '</td>' +
                    '<td>' +
                    '<button class="btn btn-success editBtn" title="Edit" data-merchantid="' + merchant.id + '">' +
                    '<i class="fa fa-edit"></i></button>' +
                    '</td>' +
                    '</tr>';
        });

        $('tbody').html(content);
        $('#pagination').show();
    }


});