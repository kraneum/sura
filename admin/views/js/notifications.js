/*jslint browser: true*/
/*global $ alert working_alert, working_alert, moment */

$(document).ready(function () {

    var paginationOptions = {
        items: 10,
        itemsOnPage: limit,
        cssStyle: 'light-theme',
        edges: 0,
        displayedPages: 1,
        onPageClick: function (pageNumber) {
            fetchQueryData($('#searchFilter').serialize() + '&limit=' + limit + '&offset=' + limit * (pageNumber - 1),
                    $("#entity option:selected").val());
        }
    };
    $('#pagination').pagination(paginationOptions);

    var searchOption = {// for the autocomplete plugin
        url: 'name_search.php',
        idFieldName: 'search_name',
        method: 'GET',
        dataParams: {entity: $("#entity option:selected").val()},
        minLength: 3
    };
    $('#search').bootcomplete(searchOption);

    $('#notifForm').parsley();
    $('#mailForm').parsley();


    ///////////////////////////////////////////////////

    function formReset() {
        $('#notifForm textarea').val('');
        $('#mailForm textarea').val('');
        $('#mailForm input[name=subject]').val('');
        $('#notifModal .modal-header h4 span').empty();
        $('#mailModal .modal-header h4 span').empty();
    }

    ///////////////////////////////////////////////////

    $('#search').on('input', function () {
        $(this).prev().val('');
    });

    $('#entity').change(function () {
        searchOption.dataParams.entity = $(this).val();
    });

    $('#searchFilter').submit(function (event) {
        let entity = $("#entity option:selected").val();

        $('#notifForm input[name=entity]').val(entity);
        $('#mailForm input[name=entity]').val(entity);
        $('#pagination').pagination(paginationOptions);
        fetchQueryData($(this).serialize(), entity);

        event.preventDefault();
    });

    $('body').on('click', '.direct-notif', function () {
        let _this = $(this);
        callback = function () {
            $('#notifModal .modal-header h4.modal-title').html('Send Notification to ' + _this.data('name'));
            $('#notifForm input[name=id]').val(_this.data('id'));
            formReset();

            $('#notifModal').modal('toggle');
        };

        displayVerifyModal();
    });

    $('body').on('click', '.direct-mail', function () {
        let _this = $(this);
        callback = function () {
            $('#mailModal .modal-header h4.modal-title').html('Send Mail to ' + _this.data('email'));
            $('#mailForm input[name=id]').val(_this.data('email'));
            formReset();

            $('#mailModal').modal('toggle');
        };

        displayVerifyModal();
    });

    $('body').on('click', '#notifyAll', function () {
        callback = function () {
            $('#notifModal .modal-header h4.modal-title').html('Send Notification to ' + $("#entity option:selected").html());
            $('#notifForm input[name=id]').val('0');
            formReset();

            $('#notifModal').modal('toggle');
        };

        displayVerifyModal();
    });

    $('body').on('submit', '#notifForm', function (event) {
        sendNotification($(this).serialize());
        event.preventDefault();
    });

    $('body').on('submit', '#mailForm', function (event) {
        sendMail($(this).serialize());
        event.preventDefault();
    });

    //////////////////////////////////////////////////

    var limit = 10;

    function sendNotification(formData) {
        working_alert.start();
        $.post('send_notification.php', formData, function (data) {
            let info = null;
            data = JSON.parse(data);

            if (data['status']) {
                info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                        'Notification(s) sent successfully</div>';

            } else {
                info = '<div class="alert alert-dismissible alert-danger text-center">' +
                        'Error sending notification</div>';
            }
            $('#info').html(info).stop().slideDown().delay(3000).slideUp();
            $('#notifModal').modal('toggle');
            working_alert.finish();
        });
    }

    function sendMail(formData) {
        working_alert.start();
        $.post('send_email.php', formData, function (data) {
            let info = null;
            data = JSON.parse(data);

            if (data['status']) {
                info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                        'Mail(s) sent successfully</div>';

            } else {
                info = '<div class="alert alert-dismissible alert-danger text-center">' +
                        'Error sending mail</div>';
            }
            $('#info').html(info).stop().slideDown().delay(3000).slideUp();
            $('#mailModal').modal('toggle');
            working_alert.finish();
        });
    }

    function fetchQueryData(filter = null, entity) {
        working_alert.start();

        $.getJSON('query_search.php', filter + '&limit=' + limit, function (data) {

            if (data.length) {
                $('#pagination').removeClass('hide');
                $('#mailAll').removeClass('disabled');
                $('#notifyAll').removeClass('disabled');
                if (data.length < limit) {
                    $('#pagination li:last-child').addClass('disabled');
                    $('#pagination li:last-child a ').remove();
                    $('#pagination li:last-child').html('<span class="current next">Next</span>');
                }
                displayQueryData(data, entity);
            } else {
                $('#mailAll').addClass('disabled');
                $('#notifyAll').addClass('disabled');
                $('#pagination li:last-child').addClass('disabled');
                $('#pagination li:last-child a ').remove();
                $('#pagination li:last-child').html('<span class="current next">Next</span>');
            }

            working_alert.finish();

        });
    }

    function displayQueryData(data, entity) {
        let content = '';

        if (entity === 'users' || entity === 'agent') {
            data.forEach(function (packet) {
                content += '<tr>' +
                        '<td>' + packet.id + '</td>' +
                        '<td>' + (packet.f_name + ' ' + packet.l_name) + '</td>' +
                        '<td>' + packet.email + '</td>' +
                        '<td>' + packet.phone + '</td>' +
                        '<td><button title="Send mail" class="btn direct-mail" data-id="' + packet.id + '" data-email = "' + packet.email + '" ><i class="fa fa-envelope"></i></button></td>' +
                        '<td><button title="Send notification" class="btn direct-notif" data-id="' + packet.id + '" data-name = "' + (packet.f_name + ' ' + packet.l_name) + '" ><i class="fa fa-bell"></i></button></td>' +
                        '</tr>';

            });
        } else {
            data.forEach(function (packet) {
                content += '<tr>' +
                        '<td>' + packet.id + '</td>' +
                        '<td>' + packet.name + '</td>' +
                        '<td>' + packet.email + '</td>' +
                        '<td>' + packet.phone + '</td>' +
                        '<td><button title="Send mail" class="btn direct-mail" data-id="' + packet.id + '" data-email = "' + packet.email + '" ><i class="fa fa-envelope"></i></button></td>';

                entity !== 'e_commerce' ? content += '<td><button title="Send notification" class="btn direct-notif" data-id="' + packet.id + '" data-name = "' + packet.name + '" ><i class="fa fa-bell"></i></button></td>' : '';

                content += '</tr>';

            });
        }

        $('tbody').html(content);

        $('#pagination').show();
    }

});