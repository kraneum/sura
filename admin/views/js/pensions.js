/*jslint browser: true*/
/*global $ alert working_alert */

$(document).ready(function () {

    $('body').on('switchChange.bootstrapSwitch', 'input[name="validated"]', function (event, state) {
        let _this = $(this);
        target = $(this);

        callback = function () {
            working_alert.start();
            var formData = {
                'pensionid': _this.data('pensionid'),
                'state': state
            };
            let name = _this.data('pensionname');
            let button = _this;

            $.post('validate_pension.php', formData, function (data) {
                data = JSON.parse(data);
                var info = '';
                if (data['status']) {
                    let type = state ? ' validated' : ' invalidated';
                    info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                            name + type + ' successfully</div>';
                    button.closest('tr').stop().fadeOut(500, function () {
                        button.closest('tr').toggleClass('alert-warning');
                        button.closest('tr').stop().fadeIn(500);
                    });

                    let counter = +$('#unapprvd_merchants_ct').html();
                    if (state) {
                        $('#unapprvd_merchants_ct').html(--counter);
                        counter === 0 ? $('#unapprvd_merchants_ct').hide() : '';

                    } else {
                        $('#unapprvd_merchants_ct').html(++counter);
                        $('#unapprvd_merchants_ct').show();
                    }
                } else {
                    if (data['err'] === 'VALIDATION_ERROR') {
                        info = '<div class="alert alert-dismissible alert-danger text-center">' +
                                'Validation error occurred</div>';
                    } else if (data['err'] === 'ERROR_OCCURRED') {
                        info = '<div class="alert alert-dismissible alert-danger text-center">' +
                                '<strong>Error Occurred!</strong> Please try again later</div>';
                    }
                }
                $('#info').html(info).stop().slideDown().delay(3000).slideUp();
                working_alert.finish();

            }).fail(function (data) {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<strong>Oops!</strong> Shit happened. Please try again later.</div>')
                        .slideDown().delay(3000).slideUp();
            });

            $("html, body").animate({scrollTop: 0}, "fast");
        };

        displayVerifyModal();

        event.preventDefault();
    });

    $('body').on('switchChange.bootstrapSwitch', 'input[name="approved"]', function (event, state) {
        let _this = $(this);
        target = $(this);

        callback = function () {

            working_alert.start();
            var formData = {
                'pensionid': _this.data('pensionid'),
                'state': state
            };
            let name = _this.data('pensionname');
            let button = _this;

            $.post('approve_pension.php', formData, function (data) {
                data = JSON.parse(data);
                var info = '';
                if (data['status']) {
                    let type = state ? ' approved' : ' disapproved';
                    info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                            name + type + ' successfully</div>';
                    button.closest('tr').stop().fadeOut(500, function () {
                        button.closest('tr').toggleClass('alert-danger');
                        button.closest('tr').stop().fadeIn(500);

                        let counter = +$('#unapprvd_merchants_ct').html();
                        if (state) {
                            $('#unapprvd_merchants_ct').html(--counter);
                            counter === 0 ? $('#unapprvd_merchants_ct').hide() : '';

                        } else {
                            $('#unapprvd_merchants_ct').html(++counter);
                            $('#unapprvd_merchants_ct').show();
                        }
                    });
                } else {
                    if (data['err'] === 'VALIDATION_ERROR') {
                        info = '<div class="alert alert-dismissible alert-danger text-center">' +
                                'Validation error occurred</div>';
                    } else if (data['err'] === 'ERROR_OCCURRED') {
                        info = '<div class="alert alert-dismissible alert-danger text-center">' +
                                '<strong>Error Occurred!</strong> Please try again later</div>';
                    }
                }
                $('#info').html(info).stop().slideDown().delay(3000).slideUp();
                working_alert.finish();

            }).fail(function (data) {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<strong>Oops!</strong> Shit happened. Please try again later.</div>')
                        .slideDown().delay(3000).slideUp();
            });

            $("html, body").animate({scrollTop: 0}, "fast");
        };

        displayVerifyModal();

        event.preventDefault();
    });

    $('body').on('click', 'button.editBtn', function () {
        let _this = $(this);

        callback = function () {
            working_alert.start();

            $('#pagination').hide();

            var formData = {
                'pensionid': _this.data('pensionid')
            };

            $('div#primary').fadeOut(500);
            $('div#secondary').load("views/edit_pension.php", formData, function () {
                $(this).fadeIn(500);
                working_alert.finish();
            });
        };

        displayVerifyModal();
    }); // for editing an pension

    $('body').on('click', 'td.name_change', function () {
        let _this = $(this);

        callback = function () {
            _this.addClass('name-change-active');

            let pensionid = _this.closest('tr').data('pensionid');
            let current_name = _this.html();
            let changed_name = _this.data('namechange');

            let nameChangeModal = `<div class="modal fade" id="nameChangeModal" tabindex="-1" role="dialog" aria-labelledby="nameChangeModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Approve Name Change</h4>
                </div>
                <form id="nameChangeForm">
                    <div class="modal-body">
                        <input type="hidden" name="entityid" value="` + pensionid + `"/>
                        <input type="hidden" name="entity" value="pension"/>
                        <div class="row" style="margin-top: 3%;">
                            <div class="form-group">
                                <label for="current_name" class="col-sm-3 control-label">Current Name</label>
                                <div class="col-sm-9">
                                    <input class="form-control" id="current_name" name="current_name" type="text" readonly value="` + current_name + `">
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 3%;">
                            <div class="form-group">
                                <label for="changed_name" class="col-sm-3 control-label">Proposed Name</label>
                                <div class="col-sm-9">
                                    <input class="form-control" id="changed_name" name="changed_name" type="text" readonly value="` + changed_name + `">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"style="margin-top: 5%;">
                        <button type="submit" class="btn btn-success">Approve</button>
                    </div>
                </form>
            </div>
        </div>
    </div>`;

            $('.body').append(nameChangeModal);
            $('#nameChangeModal').modal('toggle');
        };

        displayVerifyModal();

    });

    $('body').on('hidden.bs.modal', '#nameChangeModal', function (e) {
        $('td#name').removeClass('name-change-active');
        $('#nameChangeModal').remove();
    });

    $('body').on('submit', '#nameChangeForm', function (event) {

        let formData = $(this).serialize();

        $.post('approve_name.php', formData, function (data) {
            data = JSON.parse(data);
            var info = '';
            if (data['status']) {
                info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                        'New name Approved Succesfully</div>';
                $('td#name.name-change-active').html($('input[name=changed_name]').val()).removeClass('name_change');
            } else {
                if (data['err'] === 'VALIDATION_ERROR') {
                    info = '<div class="alert alert-dismissible alert-danger text-center">' +
                            'Validation error occurred</div>';
                } else if (data['err'] === 'ERROR_OCCURRED') {
                    info = '<div class="alert alert-dismissible alert-danger text-center">' +
                            '<strong>Error Occurred!</strong> Please try again later</div>';
                }
            }
            $('#info').html(info).stop().slideDown().delay(3000).slideUp();
            $('#nameChangeModal').modal('toggle');

        }).fail(function (data) {
            $('#sms_info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                    '<strong>Oops!</strong> Shit happened. Please try again later.</div>')
                    .slideDown().delay(3000).slideUp();
        });

        $("html, body").animate({scrollTop: 0}, "fast");

        event.preventDefault();
    }); // for approving proposed name

    searchInput.on('keyup', function () {
        let value = $(this).val();
        delay(function () {
            fetchPensionManagerByName(value, 0);
        }, 500);
    });

    $('input#search').on('input', function (e) {
        if ($(this).val().length < 3) {
            if (re_eval) {
                fetchPensionManagers(limit, 0);
            }
        }

    }); // on value. length < 3, repopulate the pensions' table

    ///////////////////////////////////////////////////

    fetchPageCount(); // to populate pension table on load

    ///////////////////////////////////////////////////

    function fetchPageCount(collection) {
        let api = '../php/suracommands.php?command=get_pension_managers_count';

        $.getJSON(api, function (response) {

            $('#pagination').pagination({
                items: response.total,
                itemsOnPage: limit,
                cssStyle: 'light-theme',
                edges: 2,
                onPageClick: function (pageNumber) {
                    fetchPensionManagers(limit, limit * (pageNumber - 1), searchInput.val());
                }
            });

            fetchPensionManagers(limit, 0);
        });
    }

    function fetchPensionManagers(limit, offset, spec = '') {
        working_alert.start();

        re_eval = false;

        let api = '../php/suracommands.php?command=get_all_pension_managers&limit=' + limit + '&offset=' + offset + '&spec=' + spec;

        $.getJSON(api, function (data) {
            displayPensionManager(data);

            working_alert.finish();

            $("[name='validated']").bootstrapSwitch(options);
            $("[name='approved']").bootstrapSwitch(options);

        });
    }

    function fetchPensionManagerByName(spec, offset) {
        working_alert.start();

        let api = '../php/suracommands.php?command=get_all_pension_managers&limit=' + limit + '&offset=' + offset + '&spec=' + spec;

        $.getJSON(api, function (data) {

            displayPensionManager(data);

            working_alert.finish();

            $("[name='approved']").bootstrapSwitch(options);
            $("[name='validated']").bootstrapSwitch(options);
        });
    }

    function displayPensionManager(data) {
        let content = '';

        data.forEach(function (pension) {
            content += '<tr id="tr-' + pension.id + '" data-pensionid = "' + pension.id + '"  class="' + (pension.approved === 'FALSE' ? 'alert-danger' : "") + ' ' + (pension.validated === 'FALSE' ? 'alert-warning' : "") + '" >' +
                    '<td>' + pension.id + '</td>';
            if (!pension.name_change) {
                content += '<td id="name">' + pension.name + '</td>';
            } else {
                content += '<td id="name" class="name_change" data-namechange ="' + pension.name_change + '">' + pension.name + '</td>';
            }
            content += '<td id="desc">' + (pension.write_up < 20 ? pension.write_up : pension.write_up.substr(0, 20) + '...') + '</td>' +
                    '<td id="rc">' + pension.rc_number + '</td>' +
                    '<td id="email">' + pension.email + '</td>' +
                    '<td id="phone">' + pension.phone + '</td>' +
                    '<td id="acc">' + pension.account_no + '</td>' +
                    '<td>' + pension.redeemable_points + '</td>' +
                    '<td>' +
                    `<div class="btn-group">
                        <a href="javascript:;" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-folder-open"></i></a>
                        <ul class="dropdown-menu">`;
            pension.docs.forEach(function (doc) {
                content += '<li><a href="../pension_docs/docs_id_' + pension.id + '/' + doc + '" target="_blank">' + doc + '</a></li>';
            });
            content += '<li class="divider"></li>';
            pension.name_docs.forEach(function (name_doc) {
                content += '<li><a href="../pension_docs/docs_id_' + pension.id + '/Change of Name Docs/' + name_doc + '" target="_blank">' + name_doc + '</a></li>';
            });
            content += `</ul>
                      </div>` +
                    '</td>' +
                    '<td>' +
                    '<div>' +
                    '<input name="validated" type="checkbox" ' + (pension.validated === 'TRUE' ? 'checked' : "") + ' ' +
                    'data-pensionid="' + pension.id + '" data-pensionname="' + pension.name + '"/>' +
                    '</div>' +
                    '</td>' +
                    '<td>' +
                    '<input name="approved" type="checkbox" ' + (pension.approved === 'TRUE' ? 'checked' : "") + ' ' +
                    'data-pensionid="' + pension.id + '" data-pensionname="' + pension.name + '"/>' +
                    '</td>' +
                    '<td>' +
                    '<button class="btn btn-success editBtn" title="Edit" data-pensionid="' + pension.id + '">' +
                    '<i class="fa fa-edit"></i></button>' +
                    '</td>' +
                    '</tr>';
        });

        $('tbody').html(content);
        $('#pagination').show();
    }

});