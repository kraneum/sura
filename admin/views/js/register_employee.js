'use strict';

//Stuff to export
var register_employee = {};

$(function () {
    //Keep global variables inside here
    var config = {
        pageNavLocId: 'pageNavLoc',
        navMenuId: 'navMenu',
        userPermissionsChkbx: 'user_permissions[]',
        regFormId: 'regForm'
    };

    var globvars = {};

    /**********************************************************************************************************************************/
    //Main
    /**********************************************************************************************************************************/
//    document.getElementById(config.pageNavLocId).innerHTML = '<li><a href="./employees.php">ADMINS</a></li><li class="active">CREATE ADMIN</li>';
            $('#' + config.navMenuId).find('li > a').each(function () {         if ($(this).html() === 'ADMINS') {             $(this).append('<span class="sr-only">(current)</span>').addClass('active');
            return false;
        }
    });
  
    /**********************************************************************************************************************************/
    //Bind event handlers
    /**********************************************************************************************************************************/
    $('#' + config.regFormId)/*.on('click', 'input[name="' + config.userPermissionsChkbx + '"][value="ALL"]', function () {
     var checked = $(this).prop('checked');
     $('input[name="' + config.userPermissionsChkbx + '"][value!="ALL"]').prop({'checked': checked, 'disabled': checked});
     })*/.parsley();
    $("#dob").datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        orientation: "auto right",
        autoclose: true
    });

    /****************************************************************************************************************************/
    //Private functions
    /****************************************************************************************************************************/

});