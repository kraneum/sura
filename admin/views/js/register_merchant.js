'use strict';

//Stuff to export
var register_employee = {};

$(function () {
    //Keep global variables inside here
    var config = {
        pageNavLocId: 'pageNavLoc',
        navMenuId: 'navMenu',
        userPermissionsChkbx: 'user_permissions[]',
        regFormId: 'regForm'
    };

    var globvars = {};

    /**********************************************************************************************************************************/
    //Main
    /**********************************************************************************************************************************/
    document.getElementById('pageNavLoc').innerHTML = '<li><a href="./merchants.php">MERCHANTS</a></li><li class="active">CREATE MERCHANT</li>';
    $('#navMenu').find('li > a').each(function () {
        if ($(this).html() === 'MERCHANTS') {
            $(this).append('<span class="sr-only">(current)</span>').addClass('active');
            return false;
        }
    });
   


    /**********************************************************************************************************************************/
    //Bind event handlers
    /**********************************************************************************************************************************/
    $('#' + config.regFormId)/*.on('click', 'input[name="' + config.userPermissionsChkbx + '"][value="ALL"]', function () {
        var checked = $(this).prop('checked');
        $('input[name="' + config.userPermissionsChkbx + '"][value!="ALL"]').prop({'checked': checked, 'disabled': checked});
    })*/.parsley();
    $("#dob").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+00",
        dateFormat: "dd/mm/yy"
    });

    /****************************************************************************************************************************/
    //Private functions
    /****************************************************************************************************************************/

});