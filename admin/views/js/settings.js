/*jslint browser: true*/
/*global $, alert, working_alert */

$(document).ready(function () {
    
    $('#smsPriceForm').parsley();
    
    ///////////////////////////////////////////////////

    $('body').on('submit', '#baseInsurersForm', function (e) {
        e.preventDefault();
        var _this = $(this);

        callback = function () {
            working_alert.start();

            $.post("update_base_insurer.php", $(_this).serialize(), function (data) {
                var info = '';
                if (data.status) {
                    info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                            'Base Insurers Updated successfully</div>';
                } else {
                    if (data.info === 'ERROR_OCCURRED') {
                        info = '<div class="alert alert-dismissible alert-error text-center btn-rounded">' +
                                'Internal Error occurred</div>';
                    } else if (data.info === 'VALIDATION_ERROR') {
                        info = '<div class="alert alert-dismissible alert-warning text-center btn-rounded">' +
                                'Validation Error occurred</div>';
                    }
                }

                $('#info').html(info).stop().slideDown().delay(3000).slideUp();
                working_alert.finish();
            }, 'JSON');
        };

        displayVerifyModal();
    });
    
    $('body').on('submit', '#smsPriceForm', function (e) {
        e.preventDefault();
        var _this = $(this);

        callback = function () {
            working_alert.start();

            $.post("update_sms_price.php", $(_this).serialize(), function (data) {
                var info = '';
                if (data.status) {
                    info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                            'SMS Price Updated successfully</div>';
                } else {
                    if (data.info === 'ERROR_OCCURRED') {
                        info = '<div class="alert alert-dismissible alert-error text-center btn-rounded">' +
                                'Internal Error occurred</div>';
                    } else if (data.info === 'VALIDATION_ERROR') {
                        info = '<div class="alert alert-dismissible alert-warning text-center btn-rounded">' +
                                'Validation Error occurred</div>';
                    }
                }

                $('#info').html(info).stop().slideDown().delay(3000).slideUp();
                working_alert.finish();
            }, 'JSON');
        };

        displayVerifyModal();
    }); 
    
    $('body').on('submit', '#paymentCommissionForm', function (e) {
        e.preventDefault();
        var _this = $(this);

        callback = function () {
            working_alert.start();

            $.post("update_payment_commission.php", $(_this).serialize(), function (data) {
                var info = '';
                if (data.status) {
                    info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                            'Payment Commission Updated successfully</div>';
                } else {
                    if (data.info === 'ERROR_OCCURRED') {
                        info = '<div class="alert alert-dismissible alert-error text-center btn-rounded">' +
                                'Internal Error occurred</div>';
                    } else if (data.info === 'VALIDATION_ERROR') {
                        info = '<div class="alert alert-dismissible alert-warning text-center btn-rounded">' +
                                'Validation Error occurred</div>';
                    }
                }

                $('#info').html(info).stop().slideDown().delay(3000).slideUp();
                working_alert.finish();
            }, 'JSON');
        };

        displayVerifyModal();
    }); 

    ///////////////////////////////////////////////////

    $('body').on('click', '#policy-back', function () {
        $('#secondary').empty();
        $('#primary').fadeIn('slow');
    });

    $('body').on('click', '#editBtn', function () {
        var _this = $(this);

        callback = function () {
            $('#info').empty();
            working_alert.start();
            var formData = {
                'policyID': _this.data('policyid')
            };
            $('#secondary').load("views/_edit_policy.php", formData, function () {
                $('#primary').hide();
                working_alert.finish();
            });

        };

        displayVerifyModal();
    });
    $('body').on('submit', '#editPolicyForm', function (event) {
        working_alert.start();
        var formData = {
            'policyID': $("input[name=id]").val(),
            'title': $('input[name=title]').val(),
            'desc': $('textarea[name=desc]').val(),
            'extlink': $('input[name=extlink]').val(),
            'price': $('input[name=price]').val(),
            'tenure': $("#tenure option:selected").val()
        };
        $.post('../php/suracommands.php?command=update_uic_policy_info', $(this).serialize(), function (data) {
            var info = '';
            if (data['status'] === true) {
                info = '<div class="alert alert-success text-center">' +
                        '<strong>Insurance policy deactivate successfully</strong></div>';
                $('#secondary').empty();
                getPolicies();
                $('#primary').fadeIn('slow');
            } else {
                if (data['info'] === 'VALIDATION_ERROR') {
                    info = '<div class="alert alert-danger text-center">' +
                            'Fill in appropiate values for all fields'
                            + '</div>';
                } else if (data['info'] === 'NO_CHANGES') {
                    info = '<div class="alert alert-warning text-center">' +
                            '<strong>Update Failed!</strong> Kindly edit a field and try again' +
                            '</div>';
                } else if (data['info'] === 'FAILURE') {
                    info = '<div class="alert alert-danger text-center">' +
                            '<strong>Error Occured!</strong> Try again later' +
                            '</div>';
                } else if (data['info'] === 'ERORR_OCCURRED') {
                    $info = '<div class="alert alert-danger text-center">' +
                            '<strong>Error Occurred!</strong> Please try again later' +
                            '</div>';
                }
            }
            $('#info').html(info).stop().slideDown().delay(3000).slideUp();
            $("html, body").animate({scrollTop: 0}, "fast");

            working_alert.finish();
        }, 'JSON');
        event.preventDefault();
    });

    $('body').on("click", ".deactivateBtn", function () {
        $("#deactivateModal #modalDeactivateBtn").data('policyid', $(this).data('policyid'));
    });
    $('body').on("click", ".activateBtn", function () {
        $("#activateModal #modalActivateBtn").data('policyid', $(this).data('policyid'));
    });
    $('body').on('click', '#modalDeactivateBtn', function () {
        var _this = $(this);

        callback = function () {
            $('#deactivateModal').modal('hide');
            working_alert.start();
            var formData = {
                'policyid': _this.data('policyid')
            };

            $.post('../php/suracommands.php?command=deactivate_uic_policy_by_id', formData, function (data) {
                data = JSON.parse(data);
                var info = '';
                if (data['status'] === true) {
                    info = '<div class="alert alert-success text-center">' +
                            '<strong>Insurance policy deactivated successfully</strong></div>';
                    getPolicies();
                    $('#primary').fadeIn('slow');
                } else {
                    if (data['info'] === 'ERORR_OCCURRED') {
                        $info = '<div class="alert alert-danger text-center">' +
                                '<strong>Error Occurred!</strong> Please try again later' +
                                '</div>';
                    }
                }
                working_alert.finish();
                $('#info').html(info).stop().slideDown().delay(3000).slideUp();
                $("html, body").animate({scrollTop: 0}, "fast");
            });
        };

        displayVerifyModal();
    });
    $('body').on('click', '#modalActivateBtn', function () {
        var _this = $(this);

        callback = function () {
            $('#activateModal').modal('hide');
            working_alert.start();
            var formData = {
                'policyid': _this.data('policyid')
            };

            $.post('../php/suracommands.php?command=activate_uic_policy_by_id', formData, function (data) {
                data = JSON.parse(data);
                var info = '';
                if (data['status'] === true) {
                    info = '<div class="alert alert-success text-center">' +
                            '<strong>Insurance policy activated successfully</strong></div>';
                    getPolicies();
                    $('#primary').fadeIn('slow');
                } else {
                    if (data['info'] === 'ERORR_OCCURRED') {
                        $info = '<div class="alert alert-danger text-center">' +
                                '<strong>Error Occurred!</strong> Please try again later' +
                                '</div>';
                    }
                }
                working_alert.finish();
                $('#info').html(info).stop().slideDown().delay(3000).slideUp();
                $("html, body").animate({scrollTop: 0}, "fast");
            });
        };

        displayVerifyModal();
    });

    $('body').on('click', '#publishPolicy', function () { // publish policy 
        callback = function () {

            working_alert.start();
            $('#secondary').load("views/_add_new_policy.php", function () {
                $('#primary').hide();
                working_alert.finish();
            });
        };

        displayVerifyModal();
    });
    $('body').on('submit', '#newPolicyForm', function (event) {
        working_alert.start();

        $.post('../php/suracommands.php?command=publish_uic_policy', $(this).serialize(), function (data) {
            var info = '';
            if (data['status'] === true) {
                info = '<div class="alert alert-success text-center">' +
                        '<strong>Insurance policy created successfully</strong></div>';
                $('#secondary').empty();
                getPolicies();
                $('#primary').fadeIn('slow');
            } else {
                if (data['info'] === 'VALIDATION_ERROR') {
                    info = '<div class="alert alert-danger text-center">' +
                            'Fill in appropiate values for all fields'
                            + '</div>';
                } else if (data['info'] === 'NO_CHANGES') {
                    info = '<div class="alert alert-warning text-center">' +
                            '<strong>Update Failed!</strong> Kindly edit a field and try again' +
                            '</div>';
                } else if (data['info'] === 'FAILURE') {
                    info = '<div class="alert alert-danger text-center">' +
                            '<strong>Error Occured!</strong> Try again later' +
                            '</div>';
                } else if (data['info'] === 'ERORR_OCCURRED') {
                    $info = '<div class="alert alert-danger text-center">' +
                            '<strong>Error Occurred!</strong> Please try again later' +
                            '</div>';
                }
            }
            $('#info').html(info).stop().slideDown().delay(3000).slideUp();
            $("html, body").animate({scrollTop: 0}, "fast");

            working_alert.finish();

        }, 'JSON');
        event.preventDefault();
    });

    /////////////////////////////////////////////////////////

    function getPolicies() {
        var content = '';
        working_alert.start();
        $.get('../php/suracommands.php?command=get_uic_policies', function (data) {
            var policyInfo = JSON.parse(data);
            for (var i = 0; i < policyInfo.length; i++) {
                var id = i + 1;
                var policyid = policyInfo[i]['id'];
                var type = policyInfo[i]['type'];
                var desc = policyInfo[i]['description'];
                var title = policyInfo[i]['title'];
                var price = policyInfo[i]['price'];
                var tenure = policyInfo[i]['duration'];
                var active = policyInfo[i]['active'];
                var editBtn = '<button id="editBtn" title="Edit" class ="btn btn-circular box-shadow--2dp btn-success"  data-policyid="' + policyid + '"><i class="fa fa-pencil"></i></button>';
                var activeBtn = '';
                if (active === 'FALSE') {
                    activeBtn = '<button data-toggle="modal" title="Inactive" data-target="#activateModal"  class ="btn box-shadow--2dp btn-circular btn-danger activateBtn"  data-policyid="' + policyid + '"><i class="fa fa-remove"></i></button>';
                } else {
                    activeBtn = '<button data-toggle="modal" title="Active" data-target="#deactivateModal"  class ="btn box-shadow--2dp btn-circular btn-success deactivateBtn"  data-policyid="' + policyid + '"><i class="fa fa-check"></i></button>';
                }
                content += '<tr>';
                content += '<td class="text-center">' + policyid + '</td>';
                content += '<td class="text-center">' + type + '</td>';
                content += '<td class="text-center">' + title + '</td>';
                content += '<td class="text-center">' + desc + '</td>';
                content += '<td class="text-center">' + price + '</td>';
                content += '<td class="text-center">' + tenure + ' month(s)</td>';

                content += '<td>' + activeBtn + '</td>';
                content += '<td>' + editBtn + '</td>';

                content += '</tr>';
            }
        }).done(function () {
            working_alert.finish();
            $('#primary tbody').html(content);

        });
    }


    ////////////////////////////////////////////////////////

    getPolicies();
});