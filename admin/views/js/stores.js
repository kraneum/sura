/*jslint browser: true*/
/*global $ alert working_alert */

$(document).ready(function () {

    $('body').on('switchChange.bootstrapSwitch', 'input[name="approved"]', function (event, state) {
        let _this = $(this);
        target = $(this);

        callback = function () {
            working_alert.start();
            var formData = {
                'storeid': _this.data('storeid'),
                'state': state
            };
            let name = _this.data('storename');
            let button = _this;

            $.post('approve_store.php', formData, function (data) {
                data = JSON.parse(data);
                var info = '';
                if (data['status']) {
                    let type = state ? ' approved' : ' disapproved';
                    info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                            name + type + ' successfully</div>';
                    button.closest('tr').stop().fadeOut(500, function () {
                        button.closest('tr').toggleClass('alert-danger');
                        button.closest('tr').stop().fadeIn(500);

                        let counter = +$('#unapprvd_merchants_ct').html();
                        if (state) {
                            $('#unapprvd_merchants_ct').html(--counter);
                            counter === 0 ? $('#unapprvd_merchants_ct').hide() : '';

                        } else {
                            $('#unapprvd_merchants_ct').html(++counter);
                            $('#unapprvd_merchants_ct').show();
                        }
                    });
                } else {
                    if (data['err'] === 'VALIDATION_ERROR') {
                        info = '<div class="alert alert-dismissible alert-danger text-center">' +
                                'Validation error occurred</div>';
                    } else if (data['err'] === 'ERROR_OCCURRED') {
                        info = '<div class="alert alert-dismissible alert-danger text-center">' +
                                '<strong>Error Occurred!</strong> Please try again later</div>';
                    }
                }
                $('#info').html(info).stop().slideDown().delay(3000).slideUp();
                working_alert.finish();

            }).fail(function (data) {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<strong>Oops!</strong> Shit happened. Please try again later.</div>')
                        .slideDown().delay(3000).slideUp();
            });

            $("html, body").animate({scrollTop: 0}, "fast");
        };

        displayVerifyModal();

        event.preventDefault();
    });

    $('body').on('click', 'button.editBtn', function () {
        let _this = $(this);
        callback = function () {

            working_alert.start();

            $('#pagination').hide();

            var formData = {
                'storeid': _this.data('storeid')
            };

            $('div#primary').fadeOut(500);
            $('div#secondary').load("views/edit_store.php", formData, function () {
                $(this).fadeIn(500);
                working_alert.finish();
            });
        };

        displayVerifyModal();
    }); // for editing an store

    $('input[id="search"]').on('keyup', function () {
        let value = $(this).val();
        delay(function () {
            fetchStoreByName(value, 0);
        }, 500);
    });

    searchInput.on('input', function (e) {
        if ($(this).val().length < 3) {
            if (re_eval) {
                fetchStores(limit, 0);
            }
        }

    }); // on value. length < 3, repopulate the stores' table

    $('body').on('click', '#add_store', function () {

        callback = function () {

            working_alert.start();

            $('#pagination').hide();

            $('div#primary').fadeOut(500);
            $('div#secondary').load("views/edit_store.php", function () {
                $(this).fadeIn(500);
                working_alert.finish();
            });
        };

        displayVerifyModal();
    });

    ///////////////////////////////////////////////////

    fetchPageCount(); // to populate store table on load
    
    ///////////////////////////////////////////////////

    function fetchPageCount(collection) {
        let api = '../php/suracommands.php?command=get_stores_count';

        $.getJSON(api, function (response) {

            $('#pagination').pagination({
                items: response.total,
                itemsOnPage: limit,
                cssStyle: 'light-theme',
                edges: 2,
                onPageClick: function (pageNumber) {
                    fetchStores(limit, limit * (pageNumber - 1), searchInput.val());
                }
            });

            fetchStores(limit, 0);
        });
    }

    function fetchStores(limit, offset, spec = '') {
        working_alert.start();

        re_eval = false;

        let api = '../php/suracommands.php?command=get_all_stores&limit=' + limit + '&offset=' + offset + '&spec=' + spec;

        $.getJSON(api, function (data) {
            displayStores(data);

            working_alert.finish();

            $("[name='validated']").bootstrapSwitch(options);
            $("[name='approved']").bootstrapSwitch(options);

        });
    }

    function fetchStoreByName(spec, offset) {
        working_alert.start();

        let api = '../php/suracommands.php?command=get_all_stores&limit=' + limit + '&offset=' + offset + '&spec=' + spec;

        $.getJSON(api, function (data) {

            displayStores(data);

            working_alert.finish();

            $("[name='approved']").bootstrapSwitch(options);
        });
    }

    function displayStores(data) {
        let content = '';

        data.forEach(function (store) {
            content += '<tr id="tr-' + store.id + '"  class="' + (store.approved === 'FALSE' ? 'alert-danger' : "") + ' ' + (store.validated === 'FALSE' ? 'alert-warning' : "") + '" >' +
                    '<td>' + store.id + '</td>' +
                    '<td id="name">' + store.name + '</td>' +
                    '<td id="desc">' + (store.write_up.length < 20 ? store.write_up : (store.write_up.substr(0, 20) + '...')) + '</td>' +
                    '<td id="email">' + store.email + '</td>' +
                    '<td id="phone">' + store.phone + '</td>' +
                    '<td id="acc">' + (store.account_no ? store.account_no : '---') + '</td>' +
                    '<td>' + store.points + '</td>' +
                    '<td>' + moment(store.date_joined).format('ddd, MMM Do YYYY, h:mm A') + '</td>' +
                    '<td>' +
                    '<input name="approved" type="checkbox" ' + (store.active === 'TRUE' ? 'checked' : "") + ' ' +
                    'data-storeid="' + store.id + '" data-storename="' + store.name + '"/>' +
                    '</td>' +
                    '<td>' +
                    '<button class="btn btn-success editBtn" title="Edit" data-storeid="' + store.id + '">' +
                    '<i class="fa fa-edit"></i></button>' +
                    '</td>' +
                    '</tr>';
        });

        $('tbody').html(content);
        $('#pagination').show();
    }

});