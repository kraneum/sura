/*jslint browser: true*/
/*global $ alert working_alert, working_alert, moment */

$(document).ready(function () {

    $('.input-daterange input').each(function () {
        $(this).datepicker({
            clearBtn: true,
            autoclose: true
        });
    });
    var paginationOptions = {
        items: 5000,
        itemsOnPage: limit,
        cssStyle: 'light-theme',
        edges: 0,
        displayedPages: 1,
        onPageClick: function (pageNumber) {
            fetchTransactions($('#transFilter').serialize() + '&limit=' + limit + '&offset=' + limit * (pageNumber - 1));
        }
    };
    $('#pagination').pagination(paginationOptions);

    var fromOption = {// for the autocomplete plugin
        url: 'transactions_search.php',
        idFieldName: 'from-id',
        method: 'GET',
        dataParams: {entity: $('ul.nav-tabs li.active').data('id'), spec: 'from'},
        minLength: 3
    };
    var toOption = {// for the autocomplete plugin
        url: 'transactions_search.php',
        idFieldName: 'to-id',
        method: 'GET',
        dataParams: {entity: $('ul.nav-tabs li.active').data('id'), spec: 'to'},
        minLength: 3
    };

    $('#from').bootcomplete(fromOption);
    $('#to').bootcomplete(toOption);

    $('#to_sms').bootcomplete({// for the autocomplete plugin
        url: 'transactions_search.php',
        idFieldName: 'to_sms-id',
        method: 'GET',
        dataParams: {entity: $('ul.nav-tabs li.active').data('id'), spec: 'to'},
        minLength: 3
    });

    ///////////////////////////////////////////////////

    function toTitleCase(str) {
        return str.replace(/\w\S*/g, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }

    function formReset() {
        $('#transFilter input').val('');
        $('#transFilter_sms input').val('');
        $('#transFilter select').val('');
    }

    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    }());

    ///////////////////////////////////////////////////

    $('body').on('submit', '#transFilter', function (event) {
        $('#pagination').pagination(paginationOptions);
        fetchTransactions($('#transFilter').serialize() + '&limit=' + limit);
        $('.bc-wrapper > input').val('');
        event.preventDefault();
    });

    $('body').on('submit', '#transFilter_sms', function (event) {
        $('#pagination').pagination(paginationOptions);
        fetchTransactions($('#transFilter_sms').serialize() + '&limit=' + limit);
        event.preventDefault();
    });


    $('body').on('click', 'ul.nav-tabs #users', function () {
        var data_id = $(this).data('id');
        formReset();

        setTimeout(function () { //so as to wait for the click event to complete
            $('div.tab-content form select#transtype').empty().append(`<option selected value=""></option>
                                            <option value="1">Load Points</option>
                                            <option value="2">Buy Policy</option>
                                            <option value="3">Renew Policy</option>
                                            <option value="4">User-to-User Point Transfer</option>
                                            <option value="7">Agent-to-User Point Transfer</option>
                                            <option value="9">Make e-Commerce Payment</option>
                                            <option value="10">Pension Top-up</option>`);
            fetchTransactions($('#transFilter').serialize() + '&limit=' + limit);
            $('#pagination').pagination(paginationOptions);

            fromOption.dataParams.entity = data_id;
            $('#from').bootcomplete(fromOption);
            toOption.dataParams.entity = data_id;
            $('#to').bootcomplete(toOption);
        }, 250);
    });

    $('body').on('click', 'ul.nav-tabs #agents', function () {
        var data_id = $(this).data('id');
        formReset();

        setTimeout(function () {
            $('div.tab-content form select#transtype').empty().append(`<option selected value=""></option>
                                            <option value="7">Agent-to-User Point Transfer</option>
                                            <option value="6">Agent-to-Agent Point Transfer</option>
                                            <option value="5">Merchant-to-Agent Point Transfer</option>`);
            fetchTransactions($('#transFilter').serialize() + '&limit=' + limit);
            $('#pagination').pagination(paginationOptions);

            fromOption.dataParams.entity = data_id;
            $('#from').bootcomplete(fromOption);
            toOption.dataParams.entity = data_id;
            $('#to').bootcomplete(toOption);
        }, 250);
    });

    $('body').on('click', 'ul.nav-tabs #insurers', function () {
        var data_id = $(this).data('id');
        formReset();

        setTimeout(function () {
            $('div.tab-content form select#transtype').empty().append(`<option selected value=""></option>
                                            <option value="2">Policy Bought</option>
                                            <option value="3">Policy Renewed</option>`);
            fetchTransactions($('#transFilter').serialize() + '&limit=' + limit);

            $('#pagination').pagination(paginationOptions);

            fromOption.dataParams.entity = data_id;
            $('#from').bootcomplete(fromOption);
            toOption.dataParams.entity = data_id;
            $('#to').bootcomplete(toOption);
        }, 250);
    });

    $('body').on('click', 'ul.nav-tabs #insurers_sms', function () {
        formReset();

        setTimeout(function () {
            fetchTransactions($('#transFilter_sms').serialize() + '&limit=' + limit);

            $('#pagination').pagination(paginationOptions);

        }, 250);
    });

    $('body').on('click', 'ul.nav-tabs #pensions', function () {
        var data_id = $(this).data('id');
        formReset();

        setTimeout(function () {
            $('div.tab-content form select#transtype').empty().append(`<option selected value=""></option>
                                            <option value="10">Pension Top-up</option>`);
            fetchTransactions($('#transFilter').serialize() + '&limit=' + limit);

            $('#pagination').pagination(paginationOptions);
            fromOption.dataParams.entity = data_id;
            $('#from').bootcomplete(fromOption);
            toOption.dataParams.entity = data_id;
            $('#to').bootcomplete(toOption);
        }, 250);
    });

    $('body').on('click', 'ul.nav-tabs #ecommerce', function () {
        var data_id = $(this).data('id');
        formReset();

        setTimeout(function () {
            $('div.tab-content form select#transtype').empty().append(`<option selected value=""></option>
                                            <option value="9">Made Payment</option>`);
            fetchTransactions($('#transFilter').serialize() + '&limit=' + limit);

            $('#pagination').pagination(paginationOptions);
            fromOption.dataParams.entity = data_id;
            $('#from').bootcomplete(fromOption);
            toOption.dataParams.entity = data_id;
            $('#to').bootcomplete(toOption);
        }, 250);
    });

    $('body').on('click', 'ul.nav-tabs #merchants', function () {
        var data_id = $(this).data('id');
        formReset();

        setTimeout(function () {
            $('div.tab-content form select#transtype').empty().append(`<option selected value=""></option>
                                            <option value="11">Point Purcase</option>
                                            <option value="5">Merchant to Agent Transfer</option>`);
            fetchTransactions($('#transFilter').serialize() + '&limit=' + limit);

            $('#pagination').pagination(paginationOptions);
            fromOption.dataParams.entity = data_id;
            $('#from').bootcomplete(fromOption);
            toOption.dataParams.entity = data_id;
            $('#to').bootcomplete(toOption);
        }, 250);
    });

    $('body').on('input', 'input#from, input#to, input#to_sms', function () {
        if ($(this).val().length === 0) {
            $(this).prev().val('');
            runFetchTransaction();
        }
    });
    
    $('body').on('change', 'input[name=from-id], input[name=to-id], input[name=to_sms-id]', function () {
        runFetchTransaction();
    });
    
    $('body').on('change', '#transtype', function () {
        runFetchTransaction();
    });
    
    $('body').on('change', '.input-daterange input', function () {
        if ($(this).val().length >= 10 || $(this).val().length === 0) {
            runFetchTransaction();
        }
    });



    //////////////////////////////////////////////////

    var limit = 10;

    fetchTransactions($('#transFilter').serialize() + '&limit=' + limit); // to populate user table on load

    function runFetchTransaction() {
        $('#pagination').pagination(paginationOptions);
        fetchTransactions($('#transFilter').serialize() + '&limit=' + limit);
    }

    function fetchTransactions(filter = null) {
        working_alert.start();

        var entity = $('ul.nav-tabs li.active').data('id');
        var api = null;

        if (entity === 'users') {
            api = '../php/suracommands.php?command=get_transactions_user';
        } else if (entity === 'agents') {
            api = '../php/suracommands.php?command=get_transactions_agent';
        } else if (entity === 'insurers') {
            api = '../php/suracommands.php?command=get_transactions_insurer';
        } else if (entity === 'insurers_sms') {
            api = '../php/suracommands.php?command=get_transactions_insurer_sms';
        } else if (entity === 'pensions') {
            api = '../php/suracommands.php?command=get_transactions_pension';
        } else if (entity === 'ecommerce') {
            api = '../php/suracommands.php?command=get_transactions_ecommerce';
        } else if (entity === 'merchants') {
            api = '../php/suracommands.php?command=get_transactions_merchant';
        }

        $.getJSON(api, filter, function (data) {

            if (data.length) {
                if (data.length < limit) {
                    $('#pagination li:last-child').addClass('disabled');
                    $('#pagination li:last-child a ').remove();
                    $('#pagination li:last-child').html('<span class="current next">Next</span>');
                }
                displayTransactions(data, entity);
            } else {
                $('#pagination li:last-child').addClass('disabled');
                $('#pagination li:last-child a ').remove();
                $('#pagination li:last-child').html('<span class="current next">Next</span>');
            }

            working_alert.finish();

        });
    }

    function displayTransactions(data, entity) {
        let content = '';

        if (entity === 'insurers_sms') {
            data.forEach(function (transaction) {
                content += '<tr>' +
                        '<td>' + transaction.id + '</td>' +
                        '<td>' + transaction.reference + '</td>' +
                        '<td>' + transaction.points + '</td>' +
                        '<td>' + moment(transaction.date).format('ddd, MMM Do YYYY, h:mm A') + '</td>' +
                        '<td>' + transaction.insurer_name + '</td>' +
                        '<td>' + transaction.previous_balance + '</td>' +
                        '</tr>';
            });

            $('#sms_listing tbody').html(content);
        } else {
            data.forEach(function (transaction) {
                content += '<tr>' +
                        '<td>' + transaction.id + '</td>' +
                        '<td>' + transaction.reference + '</td>' +
                        '<td>' + transaction.points + '</td>' +
                        '<td>' + moment(transaction.date).format('ddd, MMM Do YYYY, h:mm A') + '</td>' +
                        '<td>' + toTitleCase((transaction.type).replace(/_/g, ' ')) + '</td>';

                if (transaction.type_id === '1' || transaction.type_id === '5') {
                    content += '<td>' + transaction.from + '</td>' +
                            '<td>' + transaction.to[0].phone +
                            ' <small><em>(' + transaction.to[0].id + ':' + transaction.to[0].f_name + ' ' + transaction.to[0].l_name + ')</em></small></td>' +
                            '</tr>';
                } else if (transaction.type_id === '2' || transaction.type_id === '3' || transaction.type_id === '9' || transaction.type_id === '10') {
                    content += '<td>' + transaction.from[0].phone +
                            ' <small><em>(' + transaction.from[0].id + ':' + transaction.from[0].f_name + ' ' + transaction.from[0].l_name + ')</em></small></td>' +
                            '<td>' + transaction.to + '</td>';
                    '</tr>';
                } else if (transaction.type_id === '4' || transaction.type_id === '6' || transaction.type_id === '7') {
                    content += '<td>' + transaction.from[0].phone +
                            ' <small><em>(' + transaction.from[0].id + ':' + transaction.from[0].f_name + ' ' + transaction.from[0].l_name + ')</em></small></td>' +
                            '<td>' + transaction.to[0].phone +
                            ' <small><em>(' + transaction.to[0].id + ':' + transaction.to[0].f_name + ' ' + transaction.to[0].l_name + ')</em></small></td>' +
                            '</tr>';
                } else if (transaction.type_id === '11') {
                    content += '<td>' + transaction.name + '</td>' +
                            '<td>---</td>' +
                            '</tr>';
                }
            });

            $('#listing tbody').html(content);
        }


        $('#pagination').show();
    }

});