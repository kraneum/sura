/*jslint browser: true*/
/*global $ alert working_alert */

$(document).ready(function () {

    $('body').on('switchChange.bootstrapSwitch', 'input[name="approved"]', function (event, state) {
        let _this = $(this);

        callback = function () {
            working_alert.start();
            var formData = {
                'userid': _this.data('userid'),
                'state': state
            };
            let name = _this.data('username');
            let button = _this;

            $.post('approve_user.php', formData, function (data) {
                data = JSON.parse(data);
                var info = '';
                if (data['status']) {
                    let type = state ? ' approved' : ' disapproved';
                    info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                            name + type + ' successfully</div>';
                    button.closest('tr').stop().fadeOut(500, function () {
                        button.closest('tr').toggleClass('alert-danger');
                        button.closest('tr').stop().fadeIn(500);

                        let counter = +$('#unapprvd_merchants_ct').html();
                        if (state) {
                            $('#unapprvd_merchants_ct').html(--counter);
                            counter === 0 ? $('#unapprvd_merchants_ct').hide() : '';

                        } else {
                            $('#unapprvd_merchants_ct').html(++counter);
                            $('#unapprvd_merchants_ct').show();
                        }
                    });
                } else {
                    if (data['err'] === 'VALIDATION_ERROR') {
                        info = '<div class="alert alert-dismissible alert-danger text-center">' +
                                'Validation error occurred</div>';
                    } else if (data['err'] === 'ERROR_OCCURRED') {
                        info = '<div class="alert alert-dismissible alert-danger text-center">' +
                                '<strong>Error Occurred!</strong> Please try again later</div>';
                    }
                }
                $('#info').html(info).stop().slideDown().delay(3000).slideUp();
                working_alert.finish();

            }).fail(function (data) {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<strong>Oops!</strong> Shit happened. Please try again later.</div>')
                        .slideDown().delay(3000).slideUp();
            });

            $("html, body").animate({scrollTop: 0}, "fast");
        };

        displayVerifyModal();

        event.preventDefault();
    });

    $('body').on('click', 'button.editBtn', function () {
        let _this = $(this);

        callback = function () {

            working_alert.start();

            $('#pagination').hide();

            var formData = {
                'userid': _this.data('userid')
            };

            $('div#primary').fadeOut(500);
            $('div#secondary').load("views/edit_user.php", formData, function () {
                $(this).fadeIn(500);
                working_alert.finish();
            });
        };

        displayVerifyModal();
    }); // for editing an user

    $('body').on('click', 'button.viewPolicies', function () {
        let _this = $(this);

//        callback = function () {
            working_alert.start();

            $('#pagination').hide();

            var formData = {
                'userid': _this.data('userid')
            };

            $('div#primary').fadeOut(500);
            $('div#secondary').load("views/view_policies.php", formData, function () {
                $(this).fadeIn(500);
                working_alert.finish();
            });
//        };

//        displayVerifyModal();
    }); // for editing an user

    searchInput.on('keyup', function () {
        let value = $(this).val();
        delay(function () {
            fetchUserByName(value, 0);
        }, 500);
    });

    $('body').on('click', '.detailsBtn', function () {
        let user = $(this).data('user');
        let detailsModal = $('#detailsModal');
        let content = `<table class="table ">
                    <tr>
                        <td>Date Joined: </td>
                        <td>` + moment(user.date_joined).format('ddd, MMM Do YYYY, h:mm A') + `</td>
                    </tr>
                    <tr>
                        <td>Health Insurer: </td>
                        <td>` + user.email + `</td>
                    </tr>
                </table>`;

        detailsModal.find('.modal-body').html(content);
        detailsModal.modal('show');
    });

    ///////////////////////////////////////////////////

    fetchPageCount(); // to populate user table on load
    
    //////////////////////////////////////////////////

    function fetchPageCount(collection) {
        let api = '../php/suracommands.php?command=get_users_count';

        $.getJSON(api, function (response) {

            $('#pagination').pagination({
                items: response.total,
                itemsOnPage: limit,
                cssStyle: 'light-theme',
                edges: 2,
                onPageClick: function (pageNumber) {
                    fetchUsers(limit, limit * (pageNumber - 1), searchInput.val());
                }
            });

            fetchUsers(limit, 0);
        });
    }

    function fetchUsers(limit, offset, spec = '') {
        working_alert.start();

        re_eval = false;

        let api = '../php/suracommands.php?command=get_all_users&limit=' + limit + '&offset=' + offset+ '&spec=' + spec;

        $.getJSON(api, function (data) {
            displayUsers(data);

            working_alert.finish();

            $("[name='validated']").bootstrapSwitch(options);
            $("[name='approved']").bootstrapSwitch(options);

        });
    }

    function fetchUserByName(spec, offset) {
        working_alert.start();

        let api = '../php/suracommands.php?command=get_all_users&limit=' + limit + '&offset=' + offset + '&spec=' + spec;

        $.getJSON(api, function (data) {

            displayUsers(data);

            working_alert.finish();

            $("[name='approved']").bootstrapSwitch(options);
        });
    }

    function displayUsers(data) {
        let content = '';

        data.forEach(function (user) {
//            console.log(JSON.stringify(user));
            content += '<tr id="tr-' + user.id + '"  class="' + (user.active === 'FALSE' ? 'alert-danger' : "") + '" >' +
                    '<td>' + user.id + '</td>' +
                    '<td id="fname">' + user.f_name + '</td>' +
                    '<td id="mname">' + user.m_name + '</td>' +
                    '<td id="lname">' + user.l_name + '</td>' +
                    '<td id="gender">' + user.gender + '</td>' +
                    '<td id="dob">' + user.dob + '</td>' +
                    '<td id="phone">' + user.phone + '</td>' +
//                    '<td>' + moment(user.date_joined).format('ddd, MMM Do YYYY, h:mm A') + '</td>' +
                    '<td>' + moment(user.last_login).format('ddd, MMM Do YYYY, h:mm A') + '</td>' +
//                    '<td>' + user.health_insurer + '</td>' +
                    '<td>' + user.health_point_balance + '</td>' +
                    '<td>' + user.general_point_balance + '</td>' +
//                    '<td>' + (user.pension_points ? user.pension_points : '0') + '</td>' +
                    '<td>' +
                    '<input name="approved" type="checkbox" ' + (user.active === 'TRUE' ? 'checked' : "") + ' ' +
                    'data-userid="' + user.id + '" data-username="' + user.f_name + ' ' + user.l_name + '"/>' +
                    '</td>' +
                    '<td>' +
                    '<button class="btn btn-success detailsBtn" title="View other details" data-user=\'' + JSON.stringify(user) + '\'>' +
                    '<i class="fa fa-binoculars"></i></button>' +
                    '</td>' +
                    '<td>' +
                    '<button class="btn btn-success editBtn" title="Edit" data-userid="' + user.id + '">' +
                    '<i class="fa fa-edit"></i></button>' +
                    '</td>' +
                    '<td>' +
                    '<button class="btn btn-success viewPolicies" title="View Policies" data-userid="' + user.id + '">' +
                    '<i class="fa fa-shield"></i></button>' +
                    '</td>' +
                    '</tr>';
        });

        $('tbody').html(content);
        $('#pagination').show();
    }

});