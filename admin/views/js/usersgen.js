/*jslint browser: true*/
/*global $ alert working_alert */

$(document).ready(function () {

    searchInput.on('keyup', function () {
        let value = $(this).val();
        delay(function () {
            fetchUserByName(value, 0);
        }, 500);
    });

    ///////////////////////////////////////////////////

    fetchPageCount(); // to populate user table on load

    //////////////////////////////////////////////////
    var limit = 20;
    
    function fetchPageCount(collection) {
        let api = '../php/suracommands.php?command=get_generated_users_count';

        $.getJSON(api, function (response) {

            $('#pagination').pagination({
                items: response,
                itemsOnPage: limit,
                cssStyle: 'light-theme',
                edges: 2,
                onPageClick: function (pageNumber) {
                    fetchGeneratedUsers(limit, limit * (pageNumber - 1));
                }
            });

            fetchGeneratedUsers(limit, 0);
        });
    }

    function fetchGeneratedUsers(limit, offset) {
        working_alert.start();

        re_eval = false;

        let api = '../php/suracommands.php?command=get_generated_users&type_id=&limit=' + limit + '&offset=' + offset;

        $.getJSON(api, function (data) {
            displayUsers(data);

            working_alert.finish();
        });
    }

    function fetchGeneratedUsersByName(spec, offset) {
        working_alert.start();

        let api = '../php/suracommands.php?command=get_all_users&limit=' + limit + '&offset=' + offset + '&spec=' + spec;

        $.getJSON(api, function (data) {

            displayUsers(data);

            working_alert.finish();

            $("[name='approved']").bootstrapSwitch(options);
        });
    }

    function displayUsers(data) {
        var content = '';
        var sn = 0;

        if (true) {
            data.forEach(function (user) {

                sn += 1;
                var list_name = user['list_name'];
                var date = user['date_created'];
                var type = user['type'];

                content += '<tr class="span-title">';
                content += '<td class = "text-center">' + sn + '</td>';
                content += '<td class = "text-center">' + type + '</td>';
                content += '<td class = "text-center">' + list_name + '</td>';
                content += '<td class = "text-center">' + new Date(date).toLocaleDateString("en-US", {year: 'numeric', month: 'long', day: 'numeric'}) + '</td>';
                content += '<td class = "text-center">' + (user['status'] == 'TRUE' ? '<i class="fa fa-check" style="color: green"></i>' : '<i class="fa fa-times" style="color: red"></i>') + '</td>';
                content += '<td class = "text-center"><a href="../generated_users/' + type.toString().toLowerCase() + '/' + list_name + '.csv" download>Download</a></td>';
                content += '</tr>';
            });
        } else {
            content += '<div class="text-center"><h3>No generated users at this time</h3><br/>Check back at a later time<div>';
        }



        $('tbody').html(content);
        $('#pagination').show();
    }

});