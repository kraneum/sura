<!DOCTYPE html>
<html>
    <head>
        <title>Admin-iInsurance</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="icon" type="image/png" href="../img/favicon.ico">
        <link rel="apple-touch-icon" href="../img/favicon.ico">

        <link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
        <link href="../css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style_login.css" rel="stylesheet" type="text/css"/>
        
        <style type="text/css">
            .alert-dismissable .close, .alert-dismissible .close {
                top: -10px;
            }

            @font-face {
                font-family: 'Avenir';
                src: url('../fonts/AvenirLTStd-Book.woff2') format('woff2'), /* Super Modern Browsers */
                    url('../fonts/AvenirLTStd-Book.woff') format('woff'), /* Pretty Modern Browsers */
                    url('../fonts/AvenirLTStd-Book.ttf') format('truetype'); /* Safari, Android, iOS */
            }
            body {
                font-family: "Avenir", sans-serif;
            }
        </style>

    </head>
    <body>

<div class="container">

<div class="form">
    
  <div class="login-img"><img src="../img/logos/uic2.png" class="img-responsive" /></div>
    <div id="validationMessage"></div><!--Leave Req URI-->
    <form id="regForm" method="post" action="<?php echo htmlspecialchars($_SERVER['REQUEST_URI']); ?>">
    <div class="form-group">
    <input id="email" name="email" type="email" placeholder="Enter email" value="lai-labode@uici.com" data-parsley-required class="input_" />
    <input class="input_" maxlength="12" id="password" name="password" type="password" placeholder="Enter password" value="aaaa" data-parsley-required />
    <a href="../php/forgotpasswrd.php?entity=user" id="lnkforget">Forgot?</a>

</div>
    <div id="license">By clicking Sign In, you agree to our<a href=""> License Agreement</a></div>
    <button id="btn_login" class="btn btn-success signinbtn" type="submit" name="btn_login">Sign In</button>
    
    
  </form>
  
</div>


</div>

<script src="../js/jquery-3.1.1.js" type="text/javascript"></script>
<script src="../js/parsley.min.js" type="text/javascript"></script>
<script src="../js/bootstrap.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function () {

$('#regForm').parsley();
});

<?php 
if (isset($_POST['btn_login'])) {
    echo'document.getElementById("validationMessage").innerHTML = ' .
    ($operationStatus['err'] === false ? '"<div class=\"alert alert-success\">Login successful</div>"' : '"<div class=\"alert alert-danger\">' . $operationStatus['message'] . '</div>";' . ($operationStatus['type'] === 'validationError' ? 'document.getElementById("' . $operationStatus['field'] . '").style.borderColor="red"' : ''));
}
?>
</script>

</body>
</html>