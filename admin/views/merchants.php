<?php

//Bugs:
//When another admin, nt the superadmin, clicks on the edit button on the merchant page, he/she gets redirected to the home page!!!

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';
require_once '../php/curl.php';
require_once './fragments/header.php';
?>

<link href="../css/bootcomplete.css" rel="stylesheet" type="text/css"/>
<link href="../css/simplePagination.css" rel="stylesheet" type="text/css"/>
<link href="../css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
<link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>

<div id="home-main" class="container">
    <div style="margin-bottom: 30px;">
        <h4><strong><i class="fa fa-building"></i> View Merchants</strong></h4>
        <small>&emsp;&emsp;You can also approve and validate them</small>
    </div>

    <div id="primary">

        <div class="row" style="margin: 1% 0;">
            <div class="col-sm-2">
                <a href="export.php?entity=merchants" target="_blank" id="export" class="btn btn-sm btn-success">Export Merchants</a>
            </div>
            <div class="input-group col-sm-offset-10 col-sm-2">
                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
                <input id="search" type="text" class="form-control" placeholder="Search" aria-describedby="basic-addon1">
            </div>
        </div>

        <div class="table-responsive">

            <table class="table table-hover ">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>About</th>
                        <th>RC Number</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Balance</th>
                        <th>Docs</th>
                        <th>Validated</th>
                        <th>Approved</th>

                    </tr>
                </thead>
                <tbody id="base">
                    <!--populated via javascript-->
                </tbody>
            </table>
        </div>        
    </div>

    <div id="secondary" style="display: none"></div>

    <div id="pagination" style="display: none"></div>

    <div id="info"></div> <!-- for feedback purposes ...-->


</div>

<?php
require_once './fragments/footer.php';
?>

<script type="text/javascript" src="../js/working_alert.js"></script>
<script src="../js/jquery.simplePagination.js" type="text/javascript"></script>
<script src="../js/parsley.min.js" type="text/javascript"></script>
<script src="views/js/admin_verification.js" type="text/javascript"></script>
<script src="views/js/_globals.js" type="text/javascript"></script>
<script type="text/javascript" src="views/js/merchants.js"></script>

</body>
</html>