<?php
require_once '../../php/curl.php';
require_once('../../php/date_functions.php');

$policies = getData('get_user_policies_by_id', $_POST['userid']);

//print_r($policies);
?>

<div id="home-main" class="container">
    <div class="row">

        <?php
        if ($policies) {
            $count = count($policies);
            for ($i = 0; $i < $count; $i++) {
                $policy = $policies[$i];
                if ($i % 4 == 0 && $i != 0) {
                    echo '</div>';
                    echo '<div class="row">';
                }
                ?>
                <div class="col-sm-3">            
                    <div class="card">

                        <div class="card-content">
                            <?=
                            $policy['active'] == 'TRUE' ?
                                    '<span class="label label-success label-ribbon-right">Active</span>' :
                                    '<span class="label label-danger label-ribbon-right">Inactive</span>'
                            ?>
                            <div class="card-header"><strong><?= $policy['title'] ?></strong></div>
                            <div class="card-meta"><small><?= $policy['insurer'] ? $policy['insurer'] : 'UICI' ?></small></div>
                            <div class="card-description"><?= $policy['description'] ?></div>
                        </div>
                        <div class="card-extra card-content">
                            <span>Next Payment Schedule: <?= !empty($policy['payment_schedule'][0]['date_to_charge']) ? dateFormatFine($policy['payment_schedule'][0]['date_to_charge'], 'long') : '' ?></span>
                        </div>
                    </div>
                </div>
                <?php
            }
            echo '</div>';
        } else {
            echo 'No policy signups<br><br>';
        }
        ?>

        <div class="row">
            <button id="back" class="btn btn-success"><i class="fa fa-arrow-left btn-icon-left"></i>Back</button>
        </div>

    </div>

    <script type="text/javascript">
        $(function () {

            var goBack = function () {
                $('div#secondary').fadeOut(500, function () {
                    $('div#primary').fadeIn(500);
                    $('#pagination').show();
                });
                $(this).empty();
            };

            $('#back').on('click', function () {
                goBack();
            });

        });
    </script>

