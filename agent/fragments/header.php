<?php
if (!isset($_SESSION)) {
    session_start();
}
if (empty($_SESSION['entity']) || $_SESSION['entity_type'] !== 'AGENT') {
    session_destroy();
    header("Location: login.php");
}

require_once('../php/date_functions.php');
require_once('../php/curl.php');

$agent_info = $_SESSION['entity'];
$id = $agent_info['id'];
$loyalty_points = $agent_info['points'];
$profile_pic = $agent_info['profile_pic'];
$fileName = ucfirst(basename($_SERVER['PHP_SELF'], ".php"));
if ($fileName === 'Policydetails' || $fileName === 'Addnewpolicy') {
    $fileName = 'Policies';
}
$notifCount = 0;
if (empty($_SESSION['notifCount']) && $_SESSION) {
    $notifCount = intval(getData("get_notification_count_agent", $id)['count']);
    $_SESSION['notifCount'] = $notifCount;
} else {
    $notifCount = intval($_SESSION['notifCount']);
}

$last_transfer_type = null;
$data = getData("get_transfers_by_agent_id", $id, '1');
if (!isset($_SESSION['last_transfer'])) {
    $last_transfer_type = count($data)?$data[0]['trans_type']:'';
    $_SESSION['last_transfer_type'] = $last_transfer_type;
} else {
    $last_transfer_type = $_SESSION['last_transfer_type'];
}

$arrow_symbol = null;
if (!empty($last_transfer_type)) {

    if ($loyalty_points > 0) {
        if ($last_transfer_type === 'credit') {
            $arrow_symbol = '<span class="glyphicon glyphicon-triangle-top" style="color: #33ff00"></span>';
        } else if ($last_transfer_type === 'debit') {
            $arrow_symbol = '<span class="glyphicon glyphicon-triangle-bottom" style="color: #ff0000"></span>';
        }
    }
}
?>

<!DOCTYPE html>

<html>
    <head>
        <title>iInsurance</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="icon" type="image/png" href="../img/favicon.ico">
        <link rel="apple-touch-icon" href="../img/favicon.ico">

        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <link href="../css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/timeline.css" rel="stylesheet" type="text/css"/>
        <link href="../css/simplePagination.css" rel="stylesheet" type="text/css"/>
        <link href="../css/animate.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style_a.css" rel="stylesheet" type="text/css"/>
        <link href="css/custom.css" rel="stylesheet" type="text/css"/>
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.min.js" type="text/javascript"></script>-->
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.php"><img src="../img/logos/uic2.png" alt="uic logo"/></a>
                </div>

                <div class="collapse navbar-collapse navbar-right" id="bs-navbar-collapse">

                    <ul class="nav navbar-nav">
                        <li><a href="home.php">DASHBOARD</a></li>
                        <li><a href="points.php">TRANSACTIONS</a></li>

                        <li class="dropdown dropdown-hover">
                            <a class="dropdown-toggle" data-toggle="dropdown"  role="button" aria-expanded="false">
                                <?php
                                if ($notifCount > 0) {
                                    echo '<div id="notifCount" class="animated swing infinite ">' . $notifCount . '</div>';
                                }
                                ?>
                                <i class="fa fa-bell-o"></i>
                            </a>
                            <ul class = "dropdown-menu" data-notifcount = "<?php echo $notifCount; ?>" data-notifids="" role = "menu">
                                <li id = "notif_header" class = "text-bold">Notifications <i class = "fa fa-spinner fa-pulse pull-right"></i></li>
                            </ul>
                        </li>

                        <li><a href="settings.php" title="Settings"><img id = "header-pic" class = "img-responsive img-circle" width = "55px" height = "55px" src = "../img/agents/<?php echo $profile_pic ?>" alt = "pic"/></a></li>

                    </ul>

                </div>
            </div>
        </nav>

        <div id="minibar">
            <span><strong>Balance</strong>: &nbsp;
                <span id="caret"><?php echo $arrow_symbol; ?></span>
                <span id="lp"> <?php echo number_format($loyalty_points) ?> </span> Points
            </span>
            <span id="logout" class="pull-right">Logout</span>
        </div>

        <div id="breadcrumb-display">
            <span class="active pull-left"><a href="home.php">Dashboard</a></span>
            <span class="active pull-left"><?php echo $fileName == 'Home' ? '' : '&nbsp;/&nbsp;' . $fileName ?></span>
            <span class="small pull-right">
                <?php
                if (!empty($agent_info['last_login'])) {
                    echo 'YOUR LAST LOGIN WAS ' . dateFormatFine($agent_info['last_login'], 'long');
                } else {
                    echo 'WELCOME, ' . $agent_info['f_name'];
                }
                ?> 

            </span>
        </div>
