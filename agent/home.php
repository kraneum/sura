<?php
require_once './fragments/header.php';

?>
<div id="home-main" class="container">
    <div id="north" class="row">
        <div id="north-sidebar" class="col-sm-3">
            Recent Transfers
            <hr/>
            <div class="box-shadow--2dp content-white">
                <?php
                $transferInfo = getData('get_transfers_by_agent_id', $id, 7);
                if (!empty($transferInfo)) {
                    for ($i = 0; $i < count($transferInfo); $i++) {
                        $type_id = $transferInfo[$i]['type_id'];
                        $ref = $transferInfo[$i]['ref'];
                        $trans_type = $transferInfo[$i]['trans_type'];

                        if ($type_id == 5) {
                            $ref = 'Merchant name - ' . $ref;
                        } else if ($type_id == 6) {
                            $ref = 'Agent ID - ' . $ref;
                        } else if ($type_id == 7) {
                            $ref = 'User ID - ' . $ref;
                        }
                        $points = $transferInfo[$i]['point_worth'];
                        $_date = (new DateTime($transferInfo[$i]['date_transacted']))->format('j/n/Y');
                        $str = explode('/', $_date);
                        $date = $str[0] . ' ' . toMonth($str[1]) . ', ' . $str[2];
                        $tType = null;
                        if ($type_id == '5') {
                            $tType = '<span style="color:green;">CREDIT</span>';
                        } else if ($type_id == '6') {
                            if ($trans_type == 'credit') {
                                $tType = '<span style="color:green;">CREDIT</span>';
                            } else {
                                $tType = '<span style="color:red;">DEBIT</span>';
                            }
                        } else if ($type_id == '7') {
                            $tType = '<span style="color:red;">DEBIT</span>';
                        }

                        echo '<span style="font-size:14px;"><strong>' . $ref . '</strong>' . '<span class="pull-right"><strong>' . $points . ' LP</strong></span></span><br>';
                        echo '<span style="font-size:11px;">' . 'Transaction type: ' . $tType . '</span><br>';
                        echo '<span class="text-muted">' . $date . '</span><br><br>';
                    }
                } else {
                    echo '<p style="padding: 50px;" class="text-center text-muted text-italic">No Transaction Performed</p>';
                }
                ?>
            </div>
        </div>

        <div id="south-content" class="col-sm-8 col-sm-offset-1">
            Recent Activity
            <hr/>
            <div id="recent_activity" class="box-shadow--2dp content-white">
                
            </div>
        </div>
    </div>
</div>

<?php
include './fragments/footer.php';
?>




<script src="../js/jquery-3.1.1.js" type="text/javascript"></script>
<script src="../js/jquery-ui.js" type="text/javascript"></script>
<script src="../js/parsley.min.js" type="text/javascript"></script>
<script src="../js/bootstrap.js" type="text/javascript"></script>
<script src="../js/spin.min.js" type="text/javascript"></script>
<script src="../js/spinner-config.js" type="text/javascript"></script>
<script src="../js/script_a.js" type="text/javascript"></script>

</body>
</html>
