<?php

if (isset($_SESSION['verify_otp'])) {
    session_destroy();
}
if (isset($_SESSION['entity'])) {
    header('Location: ../home.php');
}

session_start();

?>
<!DOCTYPE html>

<html>
    <head>
        <title>iInsurance</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="icon" type="image/png" href="../img/favicon.ico"/>
        <link rel="apple-touch-icon" href="../img/favicon.ico"/>

        <link href="../css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
        <link href="../css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>

        <style type="text/css" >
            .modal-dialog {
                margin-top: 10%;
            }
            .modal-content {
                border-radius: 20px; 
                padding: 0;
            }
            .modal-content .row {
                padding: 0;
                margin: 0;
            }
            #left {
                padding: 0;
                margin-top: 15px;
                margin-left: 30px;
                margin-bottom: 0;
                margin-right: 0;
                padding-right: 30px;

            }
            .modal-body {
                padding: 0;
                margin-left: 5px;
            }
            .modal-header {
                border-bottom: none;
                padding: 0;
            }
            .alert {
                margin-bottom: 10px;
            }
            #loginForm {
                margin-top: 10px;
            }
            #right {
                border-left: solid 1px #dadada;
            }
            #right > div {
                margin: 35% auto ;
            }
            #right #login-content div:nth-child(1),
            #right #login-content div:nth-child(3){
                font-size: 14px;
                font-weight: bold;
                color: #666666;
            }
            #right #login-content div:nth-child(2) a{
                font-size: 30px;
                font-weight: bold;
                color: #265a88;
            }
            h4 {
                color: #666666;
                letter-spacing: 1px;
                font-size: 24px;
            }
            label {
                font-size: 17px;
                color: #77777a;
                font-weight: normal;
                letter-spacing: 0.3px;
            }

            #loginForm  input.form-control {
                border-radius: 2px;
                border-left: none;
            }
            #regForm input.form-control {
                border-radius: 2px;
            }
            form .form-group:first-of-type {
                margin-left: 0;
            }
            #main #loginForm i {
                font-size: 1.2em;
                color: #dadada;
            }
            #main #loginForm .input-group-addon {
                border-right: none;
                background-color: white;
            }
            div.form-group:nth-child(2) {
                margin-bottom: 7px;
            }
            #forgotten {
                font-size: 11px;
                margin-top: 5px;
                margin-right: 10px;
            }
            .checkbox-s {
                margin-top: 20px;
                margin-bottom: 20px;
            }
            .checkbox-s label {
                color: #265a88;
                font-size: 14px;
                padding-left: 25px;
                letter-spacing: 0.7px;
            }
            .btn {
                border-radius: 0;
                font-size: 16px;
                letter-spacing: 1px;
            }
            div.form-group:nth-child(6) {
                margin-bottom: 7px;
            }
            div#license {
                font-size: 10px;
            }
            .box-shadow--4dp {
                box-shadow: 0 4px 5px 0 rgba(0, 0, 0, .14), 0 1px 10px 0 rgba(0, 0, 0, .12), 0 2px 4px -1px rgba(0, 0, 0, .2);
            }
            #error {
                padding-top: 5px;
                padding-bottom: 5px;
            }
            .ddd {
                z-index: 100000;
            }
            @media screen 
            and (max-width: 360px) 
            and (orientation: landscape) {
                .modal-dialog {
                    width: 200px;
                }
            }
            @media screen and (min-width: 481px) {
                .modal-dialog {
                    width: 600px; 
                }
            }
            @media screen and (min-width: 769px) {
                .modal-dialog {
                    width: 900px; /* New width for default modal */
                }

            }
            @media screen and (min-width: 992px) {
                .col-md-7 {
                    width: 54.99%;
                }
            }
        </style>
    </head>

    <body class="container" 
          style="margin-top: 20px; background-image: url('../img/bg_image4.jpg');
          background-repeat: no-repeat;
          background-position: top center;
          background-position-x: 0;
          background-position-y: 0;
          background-size: auto;">


        <div id="main">

            <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="row">
                            <div id="left" class="col-md-7">
                                <div class="modal-header">
                                    <h4><strong>Sign In</strong></h4>
                                </div>
                                <div id="info"></div>

                                <div class="modal-body">
                                    <div>
                                        <form id="loginForm" method="post" action="#">

                                            <div class="form-group">
                                                <label for="id">Email/Phone Number * </label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                                    <input class="form-control" id="id" name="id" value="modernbabbagee@gmail.com" type="text" data-parsley-required/>
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <label for="password">Password * </label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                                    <input class="form-control" id="password" name="password" value="aaaa" type="password" data-parsley-required/>
                                                </div>
                                            </div>
                                            <div id="forgotten" class="pull-right"><a href="../php/forgotpassword.php?entity=agent">I forgot my user ID or password</a></div>
                                            <div class="clearfix"></div>

                                            <div class="checkbox-s">
                                                <label>
                                                    <input id="rememberME" name="rememberME" value="true" type="checkbox"> Remember me
                                                </label>
                                            </div>

                                            <div class="form-group box-shadow--4dp" style="margin-top: 20px;"> 
                                                <button type="submit" class="btn btn-primary btn-block btn-lg "  name="btn-login">LOG IN</button>
                                            </div>
                                            <div id="license">By clicking Sign In, you agree to our<a href=""> License Agreement</a></div>
                                        </form>


                                    </div>

                                </div>

                            </div>
                            <div id="right" class="col-md-5 text-center pull-right">
                                <div>
                                    <a href="http://uiclimited.com"><img src="../img/logos/uic_login.png"/></a>
                                    <div id="login-content">
                                        <div>Don't have an account ?</div>
                                        <div><a href="#" id="newuser">SIGN UP NOW</a></div>
                                        <div>and become a UICI agent</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <script src="../js/jquery-3.1.1.js" type="text/javascript"></script>
        <script src="../js/parsley.min.js" type="text/javascript"></script>
        <script src="../js/bootstrap.js" type="text/javascript"></script>
        <script src="../js/bootstrap-datepicker.min.js" type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function () {

                $('.modal').modal({
                    backdrop: 'static',
                    keyboard: false
                });

                $('#loginForm').parsley();

                $('body').on('submit', '#loginForm', (function (event) {
                    $('#btn-login').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
                    $('#info').empty();
                    var formData = {
                        'id': $('input[name=id]').val(),
                        'password': $('input[name=password]').val()
                    };
                    $.post('../php/suracommands.php?command=validate_agent_credentials', formData, function (data) {
                        data = JSON.parse(data);

                        $('#btn-login').html('LOG IN');
                        if (data['status']) {
                            if (data['err'] === 'UNAPPROVED') {
                                window.location.href = "../php/reverify.php";
                            } else {
                                window.location.href = "home.php";
                            }
                        } else {
                            if (data['err'] === 'INVALID') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>Invalid login credentials</strong>'
                                        + ' </div>');
                                $('input[name=username]').addClass('parsley-error');
                                $('input[name=password]').addClass('parsley-error');
                            } else if (data['err'] === 'NO_USERNAME') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong">Please enter username</strong>'
                                        + ' </div>');
                                $('input[name=username]').addClass('parsley-error');
                            } else if (data['err'] === 'NO_PASSWORD') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center" >'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>Please enter password</strong>'
                                        + ' </div>');
                                $('input[name=password]').addClass('parsley-error');
                            } else if (data['err'] === 'DATABASE_CONNECT_ERROR') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>Login failed</strong>. Please try again later'
                                        + ' </div>');
                            }
                        }
                    });

                    event.preventDefault();
                }));

                $('body').on('submit', '#regForm', (function (event) {
                    $('#info').empty();

                    var formData = {
                        'fname': $('input[name=fname]').val(),
                        'midname': $('input[name=midname]').val(),
                        'lname': $('input[name=lname]').val(),
                        'gender': $("#gender option:selected").val(),
                        'dob': $('input[name=dob]').val(),
                        'phone': $('input[name=phone]').val(),
                        'email': $('input[name=email]').val(),
                        'question': $("#question option:selected").text(),
                        'answer': $('input[name=answer]').val(),
                        'password': $('input[name=password_r]').val(),
                        'method': $('input[name=verify]:checked').val()
                    };
                    $.post('../php/suracommands.php?command=register_agent', formData, function (data) {
                        data = JSON.parse(data);

                        if (data['status']) {
                            if (data['msg'] === 'EMAIL_VERIFY') {
                                window.location.href = "../php/checkmail.php";
                            }

                            if (data['msg'] === 'OTP_VERIFY') {
                                window.location.href = "../php/veripage.php";
                            }
                        } else {
                            if (data['err'] === 'VALIDATION_ERROR') {
                                var obj = data['errVal']['field'];
                                var error = data['errVal']['message'];
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center" >'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>' + error + '</strong></div>');
                                $('#' + obj).addClass('parsley-error');
                                $("html, body, #left").animate({scrollTop: 0}, "fast");
                            } else if (data['err'] === 'DUPLICATE_ERROR') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center" >'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>' + data['errColumn'] + ' already in use</strong></div>');
                                $('#regForm #' + data['errColumn'].toString().toLowerCase()).addClass('parsley-error');
                                $("html, body, #left").animate({scrollTop: 0}, "fast");
                            } else if (data['err'] === 'REGISTRATION_ERROR') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center" >'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>Registration failed. Please try again</strong></div>');
                                $("html, body, #left").animate({scrollTop: 0}, "fast");
                            } else if (data['err'] === 'DATABASE_CONNECT_ERROR') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center" >'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>Internal error occured</strong></div>');
                                $("html, body, #left").animate({scrollTop: 0}, "fast");
                            }
                        }
                    });
                    event.preventDefault();
                }));

                $('#newuser').click(function () {
                    $('.alert').css("display", "none");
                    $('#info').empty();
                    $('.modal-dialog').css("margin-top", "30px");
                    $('#left').css("overflow-y", "scroll");
                    $('#left').css("max-height", "700px");
                    $('#right').css("height", "720px");
                    $('#right').css("background-color", "#181818");
                    $('#right > div').css("margin-top", "85%");
                    $('#right > div').empty().append(
                            '<div id="reg-content">' +
                            '<div style="color: white"><strong>GET YOU POLICIES SIGNED UP WITH</strong></div>' +
                            '<img src="../img/logos/uic_login.png"/>' +
                            '</div>'
                            );
                    $('.modal-header').hide();
                    $('#loginForm').hide();

                    $('.modal-body > div').load("views/_new_agent.php");

                    return false;
                });

                $('body').on('click', '#formLogin', function () {
//                    $('#regForm').hide();
//                    $('.modal-header').show();
//                    $('#loginForm').show();
                    window.location.href = "login.php";
                    return false;
                });


            });
        </script>
    </body>
</html>

