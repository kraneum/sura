<?php
require './fragments/header.php';
include_once './views/_transfer_points.php';
?>


<div id="points-main" class="container box-shadow--2dp">
    <div><span class="lead">Transferred Points</span>
        <div class="pull-right" style="width: 10%; ">
            <a id="pointsButton" type = "button" class = "btn btn-primary btn-sm btn-rounded box-shadow--2dp" data-toggle = "modal" data-target = "#transferModal">
                Points Transfer
            </a>
        </div>

        <div class="clearfix"></div>

        <div id ="content" style="margin-top: 20px;">
            <div id="table" style="display: none">
                <div class="table-responsive">
                    <table class="table table-striped">

                        <thead>
                            <tr>
                                <th class="text-center">Date</th>
                                <th class="text-center">Entity</th>
                                <th class="text-center">Credit/Debit</th>
                                <th class="text-center">Balance</th>
                            </tr>
                        </thead>

                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div id="pagination" class="center-block"></div>
            </div>
        </div>

    </div>
</div>

<?php
include './fragments/footer.php';
?>

<script src="../js/jquery-3.1.1.js" type="text/javascript"></script>
<script src="../js/bootstrap.js" type="text/javascript"></script>
<script src="../js/parsley.min.js" type="text/javascript"></script>
<script src="../js/jquery.simplePagination.js" type="text/javascript"></script>
<script src="../js/spin.min.js" type="text/javascript"></script>
<script src="../js/spinner-config.js" type="text/javascript"></script>
<script src="../js/script_a.js" type="text/javascript"></script>
</body>
</html>