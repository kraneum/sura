<?php
require_once './fragments/header.php';

$fname = $agent_info['f_name'];
$lname = $agent_info['l_name'];
$mname = $agent_info['m_name'];
$dob = $agent_info['dob'];
$phone = $agent_info['phone'];
$email = $agent_info['email'];
$gender = $agent_info['gender'];
$url = $agent_info['ref_id'];
$date = (new DateTime($agent_info['date_joined']))->format('j/n/Y');
?>

<div id="settings-main" class="container">
    <div class="col-sm-3" style="background: white; padding: 0;">
        <div id="settings-profile" class="text-center" style="padding-top: 20px;">

            <p><img id="profile-pic" src="../img/agents/<?php echo $profile_pic ?>" alt="profile pic" width="130px" height="130px" class="img-responsive img-circle center-block" style="border: solid 2.5px #cccccc "/></p>
            <p><span id = "name" class="lead"><?php echo $fname . ' ' . $lname ?></span></p>
            <p><span>Member since <?php echo $date ?></span></p>
            <p style="margin-top: 5px;"><button type="button" id="dp-upload" class=" btn-sm btn-primary btn-rounded" name="dp_upload" style="padding: 4px; font-size: 10px">Change Picture</button></p>


        </div>
        <div id="settings-nav">
            <p id="account-nav"><strong>Account</strong> <img src="img/icons/left_arrow_grey.png" class="pull-right"></p>
            <!--<p id="referral-nav"><strong>Referrals</strong> <img src="img/icons/left_arrow_grey.png" class="pull-right"></p>-->
            <p id="setnot-nav"><strong>Settings & Notifications</strong> <img src="img/icons/left_arrow_grey.png" class="pull-right"></p>
            <p id="help-nav"><strong>Help</strong> <img src="img/icons/left_arrow_grey.png" class="pull-right"></p>
        </div>
        <a href ="php/logout.php"><div id="settings-logout" class="text-center"><strong>LOG OUT</strong></div></a>

    </div>

    <div id="content" class="container col-sm-8" style="background: white; margin-left: 20px;">
        <div id="info"></div>
        <div id="profile">
            <form id ="profileForm" enctype="multipart/form-data">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3" for="fname"><span class="pull-right">First Name</span> </label>
                        <input class="col-sm-5" id="fname" name="fname" type="text" value="<?php echo $fname ?>" data-parsley-required />
                    </div>
                    <div class="col-sm-4"></div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3" for="lname"><span class="pull-right"> Last Name</span> </label>
                        <input class="col-sm-5" id="lname" name="lname"  type="text" value="<?php echo $lname ?>" data-parsley-required />
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3" for="gender"><span class="pull-right"> Gender * </span></label>
                        
                            <?php
                            if (empty($gender)) {
                                echo '<select class="col-sm-5" id="gender" name="gender"  data-parsley-required>
                                    <option disabled selected>Select gender</option>
                        <option value="MALE">Male</option>
                        <option value="FEMALE">Female</option>';
                            } else {
                                echo '<select disabled class="col-sm-5" id="gender" name="gender"  data-parsley-required>'
                                . '<option selected value="' . strtoupper($gender) . '">' . strtoupper($gender) . '</option>';
                            }
                            ?>

                        </select>
                    </div>
                    <div class="col-sm-4"></div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3" for="dob"><span class="pull-right">Date of Birth</span> </label>
                        <input class="col-sm-5" id="dob" name="dob" value="<?php echo $dob ?>" data-parsley-required />
                    </div>
                    <div class="col-sm-4"></div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3" for="email"><span class="pull-right"> Email </span> </label>
                        <input class="col-sm-5" id="email" name="email" value="<?php echo $email ?>" data-parsley-required data-parsley-type="email" />
                    </div>
                    <div class="col-sm-4"></div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3" for="phone"><span class="pull-right">Phone Number</span> </label>
                        <input class="col-sm-5" id="phone" name="phone" value="<?php echo $phone ?>" data-parsley-required data-parsley-type="digits" data-parsley-minlength="9" data-parsley-maxlength="11" data-parsley-type-message="This value must be digits only" data-parsley-minlength-message="This value should be between 9 and 11 digits" data-parsley-maxlength-message="This value should be between 9 and 11 digits"/>
                    </div>
                    <div class="col-sm-4"></div>
                </div>



                <div class="row">
                    <div class="col-sm-3"></div>
                    <span class="col-sm-5" style="margin-bottom: 8px"><strong>Account Resets</strong></span>
                    <div class="col-sm-4"></div>
                </div>

                <div class="row"> 
                    <div class="col-sm-3"></div>
                    <span class="col-sm-5" style="width: 46.5%">
                        <button type="button" id="password-reset" class=" btn btn-primary settings-btn box-shadow--2dp btn-rounded btn-block" name="password_reset" ><strong>RESET PASSWORD</strong></button>
                    </span>
                    <div class="col-sm-4"></div>
                </div> 

                <div class="row"> 
                    <div class="col-sm-3"></div>
                    <span class="col-sm-5" style="width: 46.5%">
                        <button type="button" id="sq-reset" class=" btn btn-primary settings-btn box-shadow--2dp btn-rounded btn-block" name="sq_reset"><strong>RESET SECURITY QUESTION</strong></button>
                    </span>
                    <div class="col-sm-4" style="width: 25.5%">
                        <button id="save" type="submit" class="btn btn-primary settings-btn box-shadow--2dp btn-rounded pull-right" name="save"><strong>SAVE</strong></button>
                    </div>
                </div>

            </form>
        </div>
        <div id="sub-content"></div>
    </div>

</div>

</div>

<?php
include './fragments/footer.php';
?>



<script src="../js/jquery-3.1.1.js" type="text/javascript"></script>
<script src="../js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="../js/parsley.min.js" type="text/javascript"></script>
<script src="../js/bootstrap.js" type="text/javascript"></script>
<script src="../js/spin.min.js" type="text/javascript"></script>
<script src="../js/spinner-config.js" type="text/javascript"></script>
<script src="../js/script_a.js" type="text/javascript"></script>

</body>
</html>