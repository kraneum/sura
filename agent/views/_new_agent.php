<?php
require '../../php/curl.php';

$data = getData('get_security_questions');
?>

<form id ="regForm" method="post" action="#">

    <div class="form-group">
        <label for="fname">First Name * </label>
        <input class="form-control" id="fname" name="fname" value="Grace" type="text" placeholder="Enter first name"  data-parsley-required/>
    </div>

    <div class="form-group">
        <label for="midname">Middle Name </label>
        <input  class="form-control" id="midname" name="midname" value="Peace"  type="text" placeholder="Enter middle name"/>
    </div>

    <div class="form-group">
        <label for="lname"> Last Name * </label>
        <input class="form-control" id="lname" name="lname" value="Kings" type="text" placeholder="Enter last name"  data-parsley-required/>
    </div>

    <div class="form-group">
        <label for="gender">Gender * </label>
        <select class="form-control" id="gender" name="gender"  data-parsley-required>
            <option disabled selected>Select gender</option>
            <option value="male">Male</option>
            <option value="female">Female</option>
        </select>
    </div>

    <div id="dob-group" class="form-group">
        <label for="dob">Date of Birth * </label>
        <div class="input-group">
            <input class="form-control" id="dob" name="dob" value="19/02/1990" placeholder="Enter date of birth"  data-parsley-required />
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
        </div>
    </div>


    <div class="form-group">
        <label for="phone">Phone * </label>
        <input class="form-control" id="phone" name="phone" value="07032741109" placeholder="Enter phone number" data-parsley-required data-parsley-minlength="11" data-parsley-maxlength="11"  data-parsley-type="digits" data-parsley-minlength-message="This value should be 11 digits" data-parsley-maxlength-message="This value should be 11 digits" data-parsley-type-message="This value should contain digits only"/>
    </div>

    <div class="form-group">
        <label for="email">Email </label>
        <input class="form-control" id="email" name="email" value="modernbabbage@gmail.com"  type="email" placeholder="Enter email"/>
    </div>
    
    <div class="form-group">
        <label for="question">Security question * </label>
        <select class="form-control" id="question" name="question"  data-parsley-required>
            <option disabled selected>Select security question</option>
            <?php
            for ($i = 0; $i < count($data); $i++) {
                echo '<option value = "'  .$data[$i]['question'].'">' . $data[$i]['question'] . '</option>';
            }
            ?>
        </select>
    </div>

    <div class="form-group">
        <label for="answer">Security Answer * </label>
        <input class="form-control" id="answer" name="answer" value="UK"  type="text" placeholder="Enter security answer"  data-parsley-required/>
    </div>

    <div class="form-group">
        <label for="password">Password * </label>
        <input class="form-control" id="password" name="password_r" value="aaaa"  type="password" placeholder="Enter password"  data-parsley-required/>
    </div>

    <div class="form-group">
        <label for="password_confirm">Confirm Password * </label>
        <input class="form-control" id="password_confirm" name="password_confirm" value="aaaa" type="password" placeholder="Enter password again" data-parsley-equalto="#password" data-parsley-equalto-message="Password mismatch" data-parsley-required/>
    </div>
    
    <div class="form-group">
        <label for="password_confirm">Verify Registration * </label>
        <div class="radio-s">
            <input id="r2" name="verify" value="sms" type="radio" checked data-parsley-required/>
            <label for="r2" style="font-size: 16px">By SMS</label>
        </div>
        <div class="radio-s">
            <input id="r1" name="verify" value="email" type="radio"  data-parsley-required/>
            <label for="r1" style="font-size: 16px">By E-mail</label>
        </div>
        
    </div>

    <div class="form-group">
        <div class="row">
            <button id="formSignup" type="submit" class="btn btn-primary formButtons col-sm-5"  name="btn-register"><i class="fa fa-lock btn-icon-left"></i> SIGN UP</button>
            <a id="formLogin" href="#" class="btn btn-primary formButtons col-sm-offset-1 col-sm-6"  name="btn-login">LOG IN</a>
        </div>
    </div>
    <div id="license">By clicking Sign up, you agree to our<a href="#"> Terms of Use</a> and <a href="#"> Privacy statement </a></div>
</form>

<script type="text/javascript">

    $("#dob").datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        orientation: "auto right",
        autoclose: true
    });
    $('#regForm').parsley();
    $('#dob').parsley().on('field:error', function () {
        $('#dob').next().remove();
        $('#dob-group').append('<ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">This value is required.</li></ul>');
    });
    $('#dob').parsley().on('field:success', function () {
        $('#dob-group > ul').remove();
    });
</script>
