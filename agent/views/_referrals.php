<?php
require '../php/suracommands/get_agent_referrals.php';
$ref = get_agent_referrals();
?>
<div>
    <p class="lead">Referrals</p>
    <ul>
        <?php
        if (count($ref) > 0) {
            for ($i = 0; $i < count($ref); $i++) {
                $name = $ref[$i]['name'];
                $rc = $ref[$i]['rc_number'];
                echo '<li>' . $name . ' <span class="smaller">(' . $rc . ')</span></li>';
            }
        } else {
            echo '<p>No referrals yet</p>';
        }
        ?>
    </ul>
</div>