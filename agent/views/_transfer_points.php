
<!--Modal -->
<div class = "modal fade" id = "transferModal" tabindex = "-1" role = "dialog" aria-labelledby = "pointsLabel" aria-hidden = "true">
    <div class = "modal-dialog">
        <div class = "modal-content">
            <div class = "modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class = "modal-title pull-left" id = "pointsLabel">Transfer Points</h4>
            </div>

            <div class = "modal-body">
                <div id="info"></div>

                <ul class="nav nav-tabs tabs-material">
                    <li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">To User</a></li>
                    <li class=""><a href="#tab2" data-toggle="tab" aria-expanded="true">To Agent</a></li>
                </ul>
                <div class="tab-content" >
                    <div class="tab-pane fade active in" id="tab1" style="max-height: 400px; overflow-y: auto; overflow-x: hidden" >
                        <div class="row" style="margin-right: 5px"><button id="add-num" class="btn btn-primary btn-rounded btn-xs pull-right">Add Number</button></div>
                        <form id="transferAgent2UserForm">
                            <div class="row">
                                <div class = "col-sm-offset-1 col-sm-6">
                                    <label for = "phone1" class = "control-label">Phone Number: </label>
                                    <input type="text" name="phone[]" class="form-control" id="phone1" placeholder="Enter phone number" data-parsley-required data-parsley-minlength="11" data-parsley-maxlength="11"  data-parsley-type="digits" data-parsley-minlength-message="This value should be 11 digits" data-parsley-maxlength-message="This value should be 11 digits" data-parsley-type-message="This value should contain digits only"/>
                                </div>
                                <div class = "col-sm-offset-1 col-sm-4">
                                    <label for = "amount1" class = "control-label">Amount: </label>
                                    <input type = "text" class = "form-control" name="amount[]" id = "amount1" placeholder="Enter LP amount" data-parsley-required data-parsley-type="digits" data-parsley-type-message="This value should contain digits only"/>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <span class="col-md-6 small text-muted">By clicking "Top Up", you agree to our <span style="text-decoration: underline">Terms</span> and <span style="text-decoration: underline">Privacy Policy</span></span>
                                <div class="col-md-6 text-center ">
                                    <span class="small text-muted"> Users count: <span id="users-count">1</span></span>
                                    <input id="transfer2UserBtn" name="transfer2UserBtn" type="submit" class="btn btn-success btn-rounded box-shadow--2dp pull-right" value="Transfer"/>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="tab-pane fade" id="tab2">
                        <form id="transferAgent2AgentForm" class="form-horizontal">
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class = "form-group col-sm-8">
                                    <label for = "id" class = "control-label">Agent Phone Number: </label>
                                    <input type="text" name="phone" class="form-control" id="id" placeholder="Enter phone number" ddata-parsley-required data-parsley-minlength="11" data-parsley-maxlength="11"  data-parsley-type="digits" data-parsley-minlength-message="This value should be 11 digits" data-parsley-maxlength-message="This value should be 11 digits" data-parsley-type-message="This value should contain digits only"/>
                                </div><div class="col-sm-2"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class = "form-group col-sm-8">
                                    <label for = "amountt" class = "control-label">Amount: </label>
                                    <input type = "text" class = "form-control" name="amountt" id = "amountt" placeholder="Enter LP amount" data-parsley-required data-parsley-type="digits" data-parsley-type-message="This value should contain digits only"/>
                                </div>
                                <div class="col-sm-2"></div>
                            </div>

                            <div class="row">
                                <span class="col-md-6 small text-muted">By clicking "Top Up", you agree to our <a href = "#" >Terms</a> and <a href = "#" >Privacy Policy</a></span>
                                <div class="col-md-6 text-right">
                                    <input id="transfer2AgentBtn" name="transfer2AgentBtn" type="submit" class="btn btn-success btn-rounded box-shadow--2dp" value="Transfer"/>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>

            </div> <!--Modal body -->
        </div>
    </div>
</div>



