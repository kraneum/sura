<div id="profile-pic">
    <form id="profilePicForm" enctype="multipart/form-data">
        <div class="row">
            <div class="col-sm-2"></div>
            <label class="col-sm-2" for="pic">Profile Pic <span class="text-muted text-italic">(JPEG / PNG format)</span> </label>
            <input class="settings-input col-sm-6" id="pic" name="pic"  type="file" accept="image/jpeg, image/png" data-parsley-filemaxmegabytes="4" data-parsley-trigger="change" data-parsley-dimensions="true" data-parsley-dimensions-options='{"min_width": "100", "max_width": "100"}' data-parsley-filemimetypes="image/jpeg, image/png" data-parsley-required/>
            <div class="col-sm-2">
                <button id="pic_save" type="submit" class="btn btn-primary btn-xs box-shadow--2dp settings-btn btn-rounded" style="letter-spacing: 2px; padding: 7px; margin-top: 5px;" name="pic_save"><strong>SAVE</strong></button>
            </div>
        </div>
    </form>
</div>
