
<form id="transferAgent2UserForm" class="form-horizontal">

    <div class="row">
        <div class="col-sm-2"></div>
        <div class = "form-group col-sm-8">
            <label for = "phone" class = "control-label">Phone Number: </label>
            <input type="text" name="phone" class="form-control" id="phone" placeholder="Enter phone number" data-parsley-required data-parsley-minlength="11" data-parsley-maxlength="11"  data-parsley-type="digits" data-parsley-minlength-message="This value should be 11 digits" data-parsley-maxlength-message="This value should be 11 digits" data-parsley-type-message="This value should contain digits only"/>
        </div><div class="col-sm-2"></div>
    </div>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class = "form-group col-sm-8">
            <label for = "amount" class = "control-label">Amount: </label>
            <input type = "text" class = "form-control" name="amount" id = "amount" placeholder="Enter LP amount" data-parsley-required data-parsley-type="digits" data-parsley-type-message="This value should contain digits only"/>
        </div>
        <div class="col-sm-2"></div>
    </div>

    <div class="row">
        <span class="col-md-6 small text-muted">By clicking "Top Up", you agree to our <span style="text-decoration: underline">Terms</span> and <span style="text-decoration: underline">Privacy Policy</span></span>
        <div class="col-md-6 text-right">
            <input id="transferPointsBtn" name="transferPointsBtn" type="submit" class="btn btn-success btn-rounded box-shadow--2dp" value="Transfer"/>
        </div>
    </div>
</form>