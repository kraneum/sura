<?php
if (isset($_SESSION['id'])) {
    header('Location: insert.php');
}

session_start();
?>
<!DOCTYPE html>

<html>
    <head>
        <title>Customer Service</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="img/favicon_fint.png" />
        <link rel="icon" type="image/png" href="img/favicon_fint.png">
        <link rel="apple-touch-icon" href="img/favicon_fint.png">


        <link href="css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="css/animate.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/cc_style.css" rel="stylesheet" type="text/css"/>
        <link href="css/parsley.css" rel="stylesheet" type="text/css"/>
        <link href="css/parsley_cc_styles.css" rel="stylesheet" type="text/css"/>

    </head>

    <body class="container" style="background-color: #030725">

        <div id="main-login">

            <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: 4%;">
                <div class="modal-dialog">
                    <div class="modal-content" style="background-color: #f2f4f4">
                        <div class="modal-header" style="border-bottom: none">
                            <img src="img/fint.png" alt="" class="center-block"/>
                        </div>

                        <div class="modal-body" style="padding-left: 15%; padding-right: 15%;">
                            <div class="alert alert-dismissible alert-danger text-center"
                                 style="display: none; margin-top: 10px; border-radius: 50px;">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong id="info"></strong>
                            </div>
                            <form id="loginForm" method="post" action="#">

                                <div class="form-group">
                                    <input class="form-control form-input form-input-login" id="username" name="username" value="chiwete" type="text" data-parsley-required  style="margin-bottom: 10%;"/>
                                </div>

                                <div class="form-group">
                                    <input class="form-control form-input form-input-login" id="password" name="password" value="aaaa" type="password" data-parsley-required/>
                                </div>


                                <div class="form-group box-shadow--4dp" style="margin-top: 20px;"> 
                                    <button id="btn-login" type="submit" class="btn center-block" name="btn-login">
                                        <i class="fa fa-key animated infinite swing"></i></button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>




        <div>
            <?php
            include './footer.php';
            ?>
        </div>

        <script src="js/jquery-3.1.1.js" type="text/javascript"></script>
        <script src="js/parsley.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function () {

                $('.modal').modal({
                    backdrop: 'static',
                    keyboard: false
                });

                $('#loginForm').parsley();

                $('body').on('submit', '#loginForm', (function (event) {
//                    $('#btn-login').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
                    $('#btn-login > i').removeClass('swing infinite');
                    $('#btn-login > i').addClass('hinge');
                    $('#info').empty();
                    var formData = {
                        'username': $('input[name=username]').val(),
                        'password': $('input[name=password]').val()
                    };
                    $.post('./php/suracommands.php?command=validate_cc_credentials', formData, function (data) {
                        data = JSON.parse(data);

                        if (data['status']) {
                            window.location.href = 'insert.php';
                        } else {
                            $('.alert').css("display", "block");
                            $('#btn-login > i').removeClass('hinge');
                        $('#btn-login > i').addClass('swing infinite');
                            if (data['err'] === 'INVALID') {
                                $('#info').append('Incorrect login credentials');
                                $('input[name=username]').addClass('parsley-error');
                                $('input[name=password]').addClass('parsley-error');
                            } else if (data['err'] === 'DATABASE_CONNECT_ERROR') {
                                $('#info').append('Internal error occurred');
                            }
                        }

                    });

                    event.preventDefault();
                }));






            });
        </script>
    </body>
</html>

