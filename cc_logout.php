<?php

if (!isset($_SESSION['id'])) {
    header("Location: cc_login.php");
} else {
    session_unset();
    session_destroy();
    header("Location: cc_login.php");
    exit;
}
