<?php
require './header.php';
?>


<div id="contact-main" class="container box-shadow--2dp">
    <div class="row" style="margin-top:4%">
        <div class="col-sm-3"></div>
        <div class="col-sm-6"><div id="info"></div></div>
        <div class="col-sm-3"></div>
    </div>

    <form id="contactForm">

        <div class="row" style="margin-top:2%">
            <div class="col-sm-2"></div>
            <div class="col-sm-4">
                <label for="type">Mail Type * </label>
                <select class="form-control" id="type" name="type"  data-parsley-required>
                    <option value="SUPPORT" selected>SUPPORT</option>
                </select>
            </div>
            <div class="col-sm-6"></div>
        </div>

        <div class="row" style="margin-top:2%">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <label for="message">Message (not more than 1000 characters) * </label>
                <textarea class="form-control" id="message" name="message" type="text" placeholder="Enter your message" size="1000" rows="10" data-parsley-required></textarea>
            </div>
            <div class="col-sm-2"></div>
        </div>

        <div class="row" style="margin-top: 2%;margin-bottom: 4%">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <button id="contactSubmit" type="submit" class="btn btn-primary pull-right btn-rounded box-shadow--2dp"  name="btn-contact"><i class="fa fa-send-o btn-icon-left btn-rounded"></i> SUBMIT</button>
            </div>
            <div class="col-sm-3"></div>
        </div>

    </form>
</div>

<?php
include './footer.php';
?>

<script src="js/jquery-3.1.1.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
<script src="js/parsley.min.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
</body>
</html>