<?php
if (!isset($_SESSION)) {
    session_start();
}

if (empty($_SESSION['entity']) || $_SESSION['entity_type'] !== 'USER') {
    session_destroy();
    header("Location: login.php");
}

require_once('./php/date_functions.php');
require_once('./php/curl.php');
require_once('./php/curl_cms.php');

$user_info = $_SESSION['entity'];
$id = $user_info['id'];
$health_loyalty_points = intval($user_info['health_point_balance']);
$general_loyalty_points = intval($user_info['general_point_balance']);

//$loyalty_points = $user_info['point_balance'];
//$pension_points = $user_info['pension_points'];
//$dedicated_points = 0;
//if (!(empty($user_info['dedicated_points']) && $user_info['dedicated_points'] == 0)) {
//    $dedicated_points = $user_info['dedicated_points'];
//}
//$pension_points = $user_info['pension_points'];
//$dedicated_points = 0;
//if (!(empty($user_info['dedicated_points']) && $user_info['dedicated_points'] == 0)) {
//    $dedicated_points = $user_info['dedicated_points'];
//}

$profile_pic = $user_info['profile_pic'];
$num_signups = getData('get_user_signups_count', $id);
$fileName = ucfirst(basename($_SERVER['PHP_SELF'], ".php"));
if ($fileName === 'Policydetails' || $fileName === 'Addnewpolicy') {
    $fileName = 'Policies';
}
$notifCount = null;
if (empty($_SESSION['notifCount'])) {
    $notifCount = getData("get_notification_count_user", $id)['count'];
    $_SESSION['notifCount'] = $notifCount;
} else {
    $notifCount = $_SESSION['notifCount'];
}

$last_transaction_type = null;
if (!isset($_SESSION['last_transaction'])) {
    $data = getData("get_my_transactions", $id, '1');
    $last_transaction_type = count($data) ? $data[0]['trans_type'] : '';
    $_SESSION['last_transaction'] = $last_transaction_type;
} else {
    $last_transaction_type = $_SESSION['last_transaction'];
}

$arrow_symbol = null;
if (!empty($last_transaction_type)) {

    if (($health_loyalty_points + $general_loyalty_points) > 0) {
        if ($last_transaction_type === 'credit') {
            $arrow_symbol = '<span class="fa fa-caret-up" style="color: #33ff00"></span>';
        } else if ($last_transaction_type === 'debit') {
            $arrow_symbol = '<span class="fa fa-caret-down" style="color: #ff0000"></span>';
        }
    }
}
?>
<!DOCTYPE html>

<html>
    <head>
        <title>iInsurance</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="img/favicon.ico" />
        <link rel="icon" type="image/png" href="img/favicon.ico">
        <link rel="apple-touch-icon" href="img/favicon.ico">

        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <link href="css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="css/parsley.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/timeline.css" rel="stylesheet" type="text/css"/>
        <link href="css/animate.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/simplePagination.css" rel="stylesheet" type="text/css"/>
        <link href="css/formstyles.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="css/jquery-ui.css">
        <link href="css/custom.css" rel="stylesheet" type="text/css"/>
        <script src="js/sweetalert.min.js"></script>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">
            body {
                background-color: #e0e0e0;
                font-size: 12px;
                color: #696969;
            }
            .swal2-popup{margin-top: 50px !important;}
        </style>
    </head>
    <body >
        <div class="body_">
            <div class="container">
                <div class="row" >
                    <div class="col-lg-6"><a class="navbar-brand" href="home.php" style="float: left;"><img src="img/logos/uic2.png" height="30px" /></a></div>

                    <div class="col-lg-6" style="padding-right: 0px;">
                        <ul class="rightc">
                            <li class="dropdown dropdown-hover nav-icon">
                                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <?php
                                    if ($notifCount > 0) {
                                        echo '<div id="notifCount" class="animated swing infinite "><span>' . $notifCount . '</span></div>';
                                    }
                                    ?>
                                    <i class="fa fa-bell-o fa-2x"></i>
                                </a>

                                <ul class = "dropdown-menu notif_body" data-notifcount = "<?= $notifCount; ?>" data-notifids="" role = "menu">
                                    <li id = "notif_header" class = "text-bold text-center">Notifications <i class = "fa fa-spinner fa-pulse  pull-right"></i></li>
                                </ul>
                            </li>
                            <li class="nav-icon">
                                <a class="logout_">
                                    <i class="fa fa-power-off fa-2x"></i>
                                </a>
                            </li>
                            <li id="pic_settings">
                                <a href="settings.php" title="Settings"> 
                                    <img id = "header-pic" style="background-color: #CCCCCC;" class = "img-responsive img-circle" width = "55px" height = "55px" src = "img/users/<?php echo $profile_pic ?>" alt = "pic"/>
                                </a>
                            </li>      
                        </ul>

                    </div>

                </div>
            </div>
        </div>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="home.php">DASHBOARD</a></li>
                        <li><a href="policies.php">POLICIES</a></li>
                        <li><a href="transactions.php">TRANSACTIONS</a></li>
                        <li><a href="points.php">TRANSFERS</a></li>
                        <li><a href="javascript:;">FAQs</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <?php
                            if (!empty($user_info['last_login'])) {
                                echo 'Last Login: ' . date("d-m-y", strtotime($user_info['last_login']));
                            } else {
                                echo 'Welcome, ';
                                echo empty($user_info['f_name']) == false ? $user_info['f_name'] : $user_info['phone'];
                            }
                            ?> </li>
                    </ul>
                </div>
            </div>
        </nav>
