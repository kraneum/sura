<?php
require_once './header_h.php';
include_once './_add_points.php';
include_once './_transfer_points.php';
?>

<div id="home-main" class="container">
    <div id="north" class="row">

        <div id="north-sidebar" class="col-sm-3">

            <div class="box-shadow--2dp content-live " style="margin-bottom: 20px; margin-left: 0px;">
                <center>
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#HB" class="tab-points">Health Balance</a></li>
                        <li><a data-toggle="tab" href="#GB" class="tab-points">General Balance</a></li>
                    </ul>
                </center>

                <div class="tab-content">

                    <div id="policies" class="tab-pane fade">
                        <center>
                            <h1 class="text-center center-block"></h1>
                        </center>
                        <div class="text-center" style="margin: 10% 5%">
                            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#policyModal">Add Policies</button>
                        </div>
                    </div>
                    <div id="HB" class="tab-pane fade in active">
                        <center style="margin-top: 2em;">
                            <h1 id="hlp" class="text-center center-block"><?php echo number_format($health_loyalty_points); ?> </h1>
                        </center>

                        <div class="text-center" style="margin: 16% 5%">
                            <div class="bs-divider"><strong>iPoints</strong></div>
                            <div class="btn-group">
                                <center>
                                    <button type="button" id="pointsButton" data-toggle = "modal" data-target = "#transferModal" class="btn btn-primary btn-sm">Transfer</button>
                                    <button type="button" id="pointsButton" class="btn btn-success btn-sm" data-toggle = "modal" data-target = "#pointsModal">Buy/Redeem</button>
                                </center>
                            </div> 
                        </div>

                    </div>
                    <div id="GB" class="tab-pane fade">
                        <center style="margin-top: 2em;">
                            <h1 id="glp" class="text-center center-block" ><?php echo number_format($general_loyalty_points); ?> </h1>
                        </center>

                        <div class="text-center" style="margin: 16% 5%">
                            <div class="bs-divider"><strong>iPoints</strong></div>
                            <center>
                                <div class="btn-group">
                                    <button type="button" id="pointsButton" data-toggle = "modal" data-target = "#transferModal"
                                            class="btn btn-primary btn-sm">Transfer</button>
                                    <button type="button" id="pointsButton" class="btn btn-success btn-sm" data-toggle = "modal"
                                            data-target = "#pointsModal">Buy/Redeem</button>
                                </div> 
                            </center>
                        </div>

                    </div>

                </div>
            </div>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="policyModal" tabindex="-1" role="dialog" aria-labelledby="policyLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div id="add-policy" class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">ADD POLICY</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row text-center">
                            <div class="col-sm-6">
                                <a href="addnewpolicy.php?type=general" class="btn btn-success">
                                    <i class="fa fa-car fa-4x"></i></a>

                                <p class="caption" style="padding-top: 10px">GENERAL</p>
                            </div>

                            <div class="col-sm-6">
                                <a href="addnewpolicy.php?type=health" class="btn btn-success" style="border-radius: 3px">
                                    <img src="img/icons/health.png" alt="health logo" height="55px" width="60px"></a>

                                <p class="caption" style="padding-top: 10px">HEALTH</p>
                            </div>


                            <!--                            <div class="thumbnail col-sm-2 home-modal">
                                                            <p>
                                                                <a href="addnewpolicy.php?type=life"><img src="img/icons/life.png" alt=""/></a>
                                                            </p>
                                                            <p class="caption">LIFE</p>
                                                        </div>-->

                        </div>
                        <!--                        <div>
                                                    <a class="btn btn-block btn-primary" href="">Next</a>
                                                </div>-->
                    </div>
                </div>
            </div>
        </div>
        <div id="north-content" class="col-sm-8 col-sm-offset-1">
            <span class="span-title">RECENT ACTIVE POLICIES (<?php echo $num_signups['active'] ?>) <a class="pull-right" style="background-color: #72be58; color: #ffffff" data-toggle="modal" data-target="#policyModal">ADD POLICY <strong style="font-size: 14px;">+</strong></a> </span>

            <hr/>
            <div class="box-shadow--2dp content-white">
                <?php
                $policies = getData('get_user_policies_by_id', $id)['active'];
                $count = 0;
                if (!empty($policies)) {
                    echo '<div class="table-responsive" style="padding: 2px 35px;">';
                    echo '<table class="table" ><tbody>';
                    for ($i = count($policies) - 1; $i >= 0; $i--) {
                        if ($count == 4) {
                            break;
                        }
                        $policyID = $policies[$i]['policy_id'];
                        $signup_active = $policies[$i]['active'];
                        $insurer = $policies[$i]['insurer'];
                        $logo_url = $policies[$i]['insurer_logo_url'];
                        echo '<tr style="font-weight:bold;">';
                        echo '<td class=""><img src="img/insurers/' . $logo_url . '" alt="..." class="img-responsive" style="max-height:40px;max-width:50px;width:50px;height:40px;"></td>';
                        echo '<td class="pull-left" style="margin-top:15px;">' . $insurer . '</td>';

                        echo '<td class="pull-right" style="margin-top:15px;">', ($signup_active == 'TRUE' ? '<span class="text text-success" >Active</span>' : '<span class="text text-danger">Inactive</span>'), '</td>';

                        echo '</tr></tbody>';
                        $count++;
                    }
                    echo '</table>';
                    echo '</div>';
                } else {
                    echo '<p style="padding: 50px;" class="text-center text-muted text-italic">No Policies Signed up for</p>';
                }
                ?>
            </div>
        </div>
    </div>

    <div id="south" class="row">
        <div id="south-sidebar" class="col-sm-3">
            <span class="span-title">RECENT TRANSACTIONS</span>
            <hr/>
            <div class="box-shadow--2dp content-white" style="font-weight: bold; font-size: 12px;">
                <?php
                $transactionInfo = getData('get_my_transactions', $id, '6');

                if (!empty($transactionInfo)) {
                    for ($i = 0; $i < count($transactionInfo); $i++) {
                        $type_id = $transactionInfo[$i]['type_id'];
                        $trans_type = $transactionInfo[$i]['trans_type'];
                        $ref = $transactionInfo[$i]['ref'];
                        $points = $transactionInfo[$i]['point_worth'];
                        $_date = (new DateTime($transactionInfo[$i]['date_transacted']))->format('j/n/Y');
                        $str = explode('/', $_date);
                        $date = $str[0] . ' ' . toMonth($str[1]) . ', ' . $str[2];
                        $tType = null;
                        if ($type_id == '1') {
                            $tType = '<span class="text text-success">iPOINTS TOP UP</span>';
                        } else if ($type_id == '2') {
                            $tType = '<span class="text text-danger">POLICY PURCHASE</span>';
                        } else if ($type_id == '3') {
                            $tType = '<span class="text text-danger">POLICY RENEWAL</span>';
                        } else if ($type_id == '4' && $trans_type == 'credit') {
                            $tType = '<span class="text text-success">iPOINTS TRANSFER</span>';
                        } else if ($type_id == '4' && $trans_type == 'debit') {
                            $tType = '<span class="text text-danger">iPOINTS TRANSFER</span>';
                        } else if ($type_id == '7') {
                            $tType = '<span class="text text-success">iPOINTS TRANSFER</span>';
                        } else if ($type_id == '9') {
                            $tType = '<span class="text text-danger">MADE PAYMENT</span>';
                        } else if ($type_id == '10') {
                            $tType = '<span class="text text-warning">PENSION TOP UP</span>';
                        } else if ($type_id == '14') {
                            $tType = '<span class="text text-success" >iPOINTS TOP UP</span>';
                        }

                        echo '<span ><strong>' . $ref . '</strong></span><br>';
                        echo '<span class="ttype" >' . $tType . '</span><br>';
                        echo '<span ><strong>' . $points . ' iPoints</strong></span><span class="pull-right">' . date("d-m-y", strtotime($date)) . '</span><br><br>';
                        echo '<hr>';
                    }
                } else {
                    echo '<p style="padding: 50px;" class="text-center text-muted text-italic">No Transaction Performed</p>';
                }
                ?>
            </div>
        </div>
        <div id="south-content" class="col-sm-8 col-sm-offset-1">
            <span class="span-title">RECENT ACTIVITY</span>
            <hr/>
            <div id="recent_activity" class="box-shadow--2dp content-white">

            </div>
        </div>
    </div>
</div>

<?php
include './footer_f.php';
?>

<script src="js/jquery-3.1.1.js" type="text/javascript"></script>
<script src="js/jquery-ui.js" type="text/javascript"></script>
<script src="js/parsley.min.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
<script src="js/spin.min.js" type="text/javascript"></script>
<script src="js/spinner-config.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>

</body>
</html>
