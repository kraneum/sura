<?php
session_start();

if (!isset($_SESSION['id'])) {
    header('Location: cc_login.php');
}
?>
<!DOCTYPE html>

<html>
    <head>
        <title>Customer Service</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="img/favicon_fint.png" />
        <link rel="icon" type="image/png" href="img/favicon_fint.png">
        <link rel="apple-touch-icon" href="img/favicon_fint.png">

        <link href="css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="css/animate.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/cc_style.css" rel="stylesheet" type="text/css"/>
        <link href="css/parsley.css" rel="stylesheet" type="text/css"/>
        <link href="css/parsley_cc_styles.css" rel="stylesheet" type="text/css"/>

    </head>

    <body id="main" class="container" >
        <div class="row">
            <div id="alert-me" class="col-sm-offset-2 col-sm-8">
                <div class="alert alert-dismissible text-center center-block "
                     style="display: none; border-radius: 50px;max-width: 400px">
<!--                    <button type="button" class="close" data-dismiss="alert">&times;</button>-->
                    <strong id="info"></strong>
                </div>
            </div>
            <div style="margin-right: 20px">
                <a href="cc_logout.php" class="btn btn-danger btn-sm pull-right btn-rounded"><i class="fa fa-2x fa-unlock-alt"></i></a>
            </div>
        </div>
        <br/>
        <form id ="insertForm" class="form-horizontal">

            <div class="form-group">
                <label for="name" class="col-sm-offset-1 col-sm-1"><i class="fa fa-2x fa-user"></i></label>
                <div class="col-sm-8">
                    <input class="form-control form-input" id="name" name="name" type="text" value="Micheal" data-parsley-required/>
                </div>
            </div>

            <div class="form-group">
                <label for="phone" class="col-sm-offset-1 col-sm-1"><i class="fa fa-2x fa-phone"></i></label>
                <div class="col-sm-8">
                    <input class="form-control form-input" id="phone" name="phone"  value="09057183476" data-parsley-required data-parsley-minlength="11" data-parsley-maxlength="11"  data-parsley-type="digits" data-parsley-minlength-message="This value should be 11 digits" data-parsley-maxlength-message="This value should be 11 digits" data-parsley-type-message="This value should contain digits only"/>
                </div>
            </div>

            <div class="form-group">
                <label for="location" class="col-sm-offset-1 col-sm-1"><i class="fa fa-2x fa-map-marker"></i></label>
                <div class="col-sm-8">
                    <input class="form-control form-input" id="location" name="location" value="Surulere"  type="text" data-parsley-required />
                </div>
            </div>

            <div class="form-group">
                <label for="desc" class="col-sm-offset-1 col-sm-1"><i class="fa fa-2x fa-pencil"></i></label>
                <div class="col-sm-8">
                    <textarea class="form-control form-input" id="message" name="message"  type="text" size="200"  data-parsley-required data-parsley-minlength="4" data-parsley-minlength-message="Character length should be minimum of 4"></textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="question" class="col-sm-offset-1 col-sm-1"><i class="fa fa-2x fa-tag"></i></label>
                <div class="col-sm-8">
                    <select class="form-control form-input" id="type" name="type"  data-parsley-required>
                        <option disabled selected>Select Type</option>
                        <option value="INFO">INFO</option>
                        <option value="SUPPORT">SUPPORT</option>
                        <option value="TECHNICAL">TECHNICAL</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <button id="btn-insert" type="submit" class="btn btn-rounded btn-primary center-block"  name="btn-insert"><i class="fa fa-2x fa-pencil-square-o"></i></button>
                </div>
            </div>
        </form>

        <div>
            <?php
            include './footer.php';
            ?>
        </div>


        <script src="js/jquery-3.1.1.js" type="text/javascript"></script>
        <script src="js/parsley.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function () {

                $('#insertForm').parsley();

                $('body').on('submit', '#insertForm', (function (event) {
                    $('#btn-insert').addClass('disabled');
                    $('#btn-insert i').removeClass("fa-pencil-square-o");
                    $('#btn-insert i').addClass("fa-spinner fa-pulse fa-fw");

                    $('#info').empty();

                    var formData = {
                        'name': $('input[name=name]').val(),
                        'phone': $('input[name=phone]').val(),
                        'location': $('input[name=location]').val(),
                        'message': $('textarea[name=message]').val(),
                        'type': $("#type option:selected").val(),
                    };
                    $.post('./php/suracommands.php?command=cc_insert', formData, function (data) {
                        data = JSON.parse(data);

                        $('#btn-insert').removeClass('disabled');
                        $('#btn-insert i').addClass("fa-pencil-square-o");
                        $('#btn-insert i').removeClass("fa-spinner fa-pulse fa-fw");

                        if (data['status']) {
                            $('.alert').removeClass('alert-danger');
                            $('.alert').addClass('alert-success');
                            $('#info').html('Insert successful');
                            $('.alert').slideDown().delay(3000).slideUp();
                            $('#type').val('');
                            $('input').val('');
                            $('textarea').val('');
                            
                        } else {
                            $('.alert').removeClass('alert-success');
                            $('.alert').addClass('alert-danger');
                            if (data['err'] === 'VALIDATION_ERROR') {
                                $('#info').html('Validation error');
                            } else if (data['err'] === 'REGISTRATION_ERROR') {
                                $('#info').html('Data Insert Failed. Please try again...');
                            } else if (data['err'] === 'DATABASE_CONNECT_ERROR') {
                                $('#info').html('Internal error occurred');
                            }
                            $('.alert').slideDown().delay(3000).slideUp();
                        }
                    });
                    $("html, body").animate({scrollTop: 0}, "fast");
                    event.preventDefault();
                }));
            });
        </script>
    </body>
</html>

