<?php
if (!isset($_SESSION)) {
    session_start();
}
if (empty($_SESSION['entity']) || $_SESSION['entity_type'] !== 'INSURER') {
    session_destroy();
    header("Location: login.php");
}

require_once('../php/date_functions.php');
require_once('../php/curl.php');

$insurer_info = $_SESSION['entity'];
$id = $insurer_info['id'];
$name = $insurer_info['name'];
$phone = $insurer_info['phone'];
$email = $insurer_info['email'];
$points = getData('get_insurer_points_by_id', $id)['points'];
$type_id = $insurer_info['insurance_type'];
$desc = $insurer_info['write_up'];
$name_change = $insurer_info['name_change'];
$logo = $insurer_info['logo_url'];
$date_joined = $insurer_info['date_joined'];
$sms_wallet = intval($insurer_info['sms_wallet']);
$isValidated = $_SESSION['entity']['validated'];
$activePoliciesCount = count(getData('get_active_policies_by_insurer', $id));
$is_lead = $insurer_info['is_lead'];

$fileName = ucfirst(basename($_SERVER['PHP_SELF'], ".php"));
if ($fileName === 'Policydetails' || $fileName === 'Addnewpolicy') {
    $fileName = 'Policies';
}
if ($fileName != 'Settings') {

    if ($isValidated == 'FALSE' || $isValidated == 'PENDING') {
        header("Location: settings.php");
    }
}
if (empty($_SESSION['notifCount'])) {
    $notifCount = getData("get_notification_count_insurer", $id)['count'];
    $_SESSION['notifCount'] = $notifCount;
} else {
    $notifCount = $_SESSION['notifCount'];
}
?>
<!DOCTYPE html>


<html>
    <head>
        <title>iInsurance</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="icon" type="image/png" href="../img/favicon.ico">
        <link rel="apple-touch-icon" href="../img/favicon.ico">

        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

        <link href="../css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/timeline.css" rel="stylesheet" type="text/css"/>
        <link href="../css/animate.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/simplePagination.css" rel="stylesheet" type="text/css"/>
        <link href="../css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
        <link href="../css/ion.rangeSlider.skinFlat.css" rel="stylesheet" type="text/css"/>
        <link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>
        <link href="../css/custom.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style_i.css" rel="stylesheet" type="text/css"/>
        <script src="../js/Chart.bundle.min.js" type="text/javascript"></script>
        <script src="../js/sweetalert.min.js" type="text/javascript"></script>
        <style type="text/css">
            body {
                background-color: #e0e0e0;
                font-size: 12px;
                color: #696969;
            }
            .swal2-popup{margin-top: 50px !important;}
        </style>
    </head>
    <body>
        <div class="body_">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6"><a class="navbar-brand" href="home.php" style="float: left;"><img src="../img/logos/uic2.png" height="35px" /></a></div>

                    <div class="col-lg-6" style="padding-right: 0px;" >
                        <ul class="rightc">
                            <li class="dropdown dropdown-hover nav-icon">
                                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <?php
                                    if ($isValidated == 'TRUE') {
                                        if ($notifCount > 0) {
                                            echo '<div id="notifCount" class="animated swing infinite "><span>' . $notifCount . '</span></div>';
                                        }
                                    }
                                    ?>
                                    <i class="fa fa-bell-o fa-2x"></i>
                                </a>

                                <ul class = "dropdown-menu notif_body" data-notifcount = "<?php echo $notifCount; ?>" data-notifids="" role = "menu">
                                    <li id = "notif_header" class = "text-bold text-center">Notifications <i class = "fa fa-spinner fa-pulse  pull-right"></i></li>
                                </ul>
                            </li>
                            <li class="nav-icon">
                                <a class="logout_"><i class="fa fa-power-off fa-2x"></i></a>
                            </li>
                            <li>
                                <a href="settings.php" title="Settings">
                                    <img id = "header-pic" style="margin-top: 10px;" class = "img-responsive img-circle" width = "40px" height = "40px" src = "../img/insurers/<?php echo $logo ?>" alt = "pic"/>
                                </a>
                            </li>      
                        </ul>

                    </div>

                </div>
            </div>
        </div>
        <nav class="navbar navbar-default" >
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <?php
                        if ($isValidated == 'TRUE') {
                            echo '<li><a href="home.php">DASHBOARD</a></li>';
                            echo '<li><a href = "policies.php">POLICIES</a></li>';
                            echo '<li><a href = "marketing.php">MARKETING</a></li>';
                            echo $is_lead == true ? '<li><a href = "usergen.php">GENERATED USERS</a></li>' : '';
                        }
                        ?>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <?php
                            if (!empty($insurer_info['last_login'])) {
                                echo 'Last Login: ' . date("d-m-y", strtotime($insurer_info['last_login']));
                            } else {
                                echo 'Welcome, ' . $insurer_info['name'];
                            }
                            ?> </li>
                    </ul>
                </div>
            </div>
        </nav>
