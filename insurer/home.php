<?php
require_once './fragments/header_h.php';

$data = getData('get_insurer_policies_count_by_id', $id);
$total = $data['total'];
$active = $data['active'];
$inactive = $data['Inactive'];
?>
<div class="mainbody">
    <div id="home-main" class="container ">
        <div id="north" class="row">
            <div id="north-sidebar" class="col-sm-3">

                <div class="" style="margin-top: 30px;">
                    <div class="notice notice-success">
                        <strong>Redeemable iPoints:</strong>&nbsp;<span style="font-weight: bold;"><?= $points ?></span>  iPoints &nbsp;
                        <a class="text-info" id="point_info" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-content="This is the ipoints payment and for policy purchase and renewal"><i class="fa fa-question-circle"></i></a>
                    </div>
                    <div class="notice notice-success">
                        <strong>SMS Units:</strong>&nbsp;<span style="font-weight: bold;"> <?= $sms_wallet ?></span> iPoints &nbsp;
                        <a class="text-info" id="wallet_info" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-content="This is used for sending marketing texts to users"><i class="fa fa-question-circle"></i></a>
                    </div>

                </div>


            </div>

            <div id="north-content" class="col-sm-8 col-sm-offset-1">

                <span class="span-title">Sign-up summary</span>

                <hr/>
                <div class="box-shadow--2dp content-white">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#summary" data-toggle="tab" class="insurer_tab">Summary</i></a></li>
                        <li><a href="#policies-chart" data-toggle="tab" class="insurer_tab">Info-chart <i class="fa fa-pie-chart"></i></a></li>
                        <li><a href="#timeline-chart" data-toggle="tab" class="insurer_tab">Time-line chart <i class="fa fa-line-chart"></i></a></li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="summary">
                            <div id="table" style="display: none">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="text-center">s/n</th>
                                                <th class="text-center">Title</th>
                                                <th class="text-center">Number of sign-ups</th>
                                            </tr>
                                        </thead>

                                        <tbody class="summary_tb">

                                        </tbody>
                                    </table>
                                </div>
                                <div id="pagination" class="center-block_i"></div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="policies-chart">
                            <p id="barchart">
                            <div id="info"></div>
                            <div class="panel-group panel-accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                                Filter
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <form id="pieChartFilter">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <label for="gender">Gender </label>
                                                        <select class="form-control" id="gender_b" name="gender_b">
                                                            <option selected value=""></option>
                                                            <option value="MALE">Male</option>
                                                            <option value="FEMALE">Female</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <label for="date">Date range </label>
                                                        <div class="input-group input-daterange">
                                                            <input type="text" class="form-control" id="startDate_b" name="startDate_b" value="" data-date-end-date="0d">
                                                            <div class="input-group-addon">to</div>
                                                            <input type="text" class="form-control" id="endDate_b" name="endDate_b" value="" data-date-end-date="0d">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label for="active">Active </label>
                                                        <select class="form-control" id="active_b" name="active_b">
                                                            <option selected value=""></option>
                                                            <option value="TRUE">True</option>
                                                            <option value="FALSE">False</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top: 10px">
                                                    <div class="col-sm-9">
                                                        <label for="num_range">Number of signups </label>
                                                        <input type="text" id="num_range_b" name="num_range_b" value=""/>
                                                    </div>
                                                    <div class="col-sm-offset-1 col-sm-2" style="margin-top: 30px">
                                                        <button id="filterSubmit" type="submit" class="btn btn-sm btn-success btn-rounded"  name="btn-register">
                                                            <i class="fa fa-filter btn-icon-left btn-rounded"></i> Apply</button>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <canvas id="myPieChart" width="200" height="70"></canvas>
                            </p>
                        </div>

                        <div class="tab-pane fade" id="timeline-chart">
                            <p id="linechart">
                            <div id="info"></div>
                            <div class="panel-group panel-accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                                Filter
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <form id="lineChartFilter">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <label for="gender">Gender </label>
                                                        <select class="form-control" id="gender_l" name="gender_l">
                                                            <option selected value=""></option>
                                                            <option value="MALE">Male</option>
                                                            <option value="FEMALE">Female</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <label for="date">Date range </label>
                                                        <div class="input-group input-daterange">
                                                            <input type="text" class="form-control" id="startDate_l" name="startDate_l" value="" data-date-end-date="0d">
                                                            <div class="input-group-addon">to</div>
                                                            <input type="text" class="form-control" id="endDate_l" name="endDate_l" value="" data-date-end-date="0d">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label for="active">Active </label>
                                                        <select class="form-control" id="active_l" name="active_l">
                                                            <option selected value=""></option>
                                                            <option value="TRUE">True</option>
                                                            <option value="FALSE">False</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top: 10px">
                                                    <div class="col-sm-9">
                                                        <label for="num_range">Number of signups </label>
                                                        <input type="text" id="num_range_l" name="num_range_l" value=""/>
                                                    </div>
                                                    <div class="col-sm-offset-1 col-sm-2" style="margin-top: 30px">
                                                        <button id="filterSubmit" type="submit" class="btn btn-sm btn-success btn-rounded"  name="btn-register">
                                                            <i class="fa fa-filter btn-icon-left btn-rounded"></i> Apply</button>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <canvas id="myLineChart" width="200" height="70"></canvas>
                            </p>
                        </div>

                    </div>

                </div>
            </div>
        </div>


        <div id="south" class="row">
            <div class="col-sm-3">
                <span class="span-title">Live Overview</span>
                <hr/>
                <div class="box-shadow--2dp content-white">
                    <span class="text-center center-block_i" >

                        <h4 class="text-center center-block_i" ><i class="fa fa-check" style="color: #33ff00"></i> <?php echo $total ?> Policies created</h4>
                    </span>
                    <br>
                    <span class="text-center center-block_i" >

                        <h4 class="text-center center-block_i" ><i class="fa fa-plus" style="color: #33ff00"></i> <?php echo $active ?> Active Policies</h4>
                    </span>
                    <br>
                    <span class="text-center center-block_i" >

                        <h4 class="text-center center-block_i" ><i class="fa fa-minus" style="color: #ff0000"></i> <?php echo $inactive ?> In-active Policies</h4>
                    </span>
                </div>
            </div>
            <div id="south-content" class="col-sm-8 col-sm-offset-1">
                <span class="span-title"> Recent Activity</span>
                <hr/>
                <div id="recent_activity" class="box-shadow--2dp content-white">

                </div>
            </div>


        </div>
    </div>

</div>

<?php require_once './fragments/footer_f.php'; ?>

<script src="../js/jquery-3.1.1.js" type="text/javascript"></script>
<script src="../js/jquery-ui.js" type="text/javascript"></script>
<script src="../js/parsley.min.js" type="text/javascript"></script>
<script src="../js/bootstrap.js" type="text/javascript"></script>
<script src="../js/ion.rangeSlider.js" type="text/javascript"></script>
<script src="../js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="../js/spin.min.js" type="text/javascript"></script>
<script src="../js/spinner-config.js" type="text/javascript"></script>
<script src="../js/script_i.js" type="text/javascript"></script>

</body>
</html>
