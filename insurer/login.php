<?php
if (isset($_SESSION['verify'])) {
    session_destroy();
}

if (isset($_SESSION['entity'])) {
    header('Location: home.php');
}

require '../php/curl.php';

$sq = getData('get_security_questions');
$types = getData('get_insurance_types_by_category', 0);
?>

<!DOCTYPE html>
<html>
    <head>
        <title>iInsurance</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="icon" type="image/png" href="../img/favicon.ico">
        <link rel="apple-touch-icon" href="../img/favicon.ico">

        <link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
        <link href="../css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style_login.css" rel="stylesheet" type="text/css"/>
        <style type="text/css">
            .alert-dismissable .close, .alert-dismissible .close {
                top: -10px;
            }

            @font-face {
                font-family: 'Avenir';
                src: url('../fonts/AvenirLTStd-Book.woff2') format('woff2'), /* Super Modern Browsers */
                    url('../fonts/AvenirLTStd-Book.woff') format('woff'), /* Pretty Modern Browsers */
                    url('../fonts/AvenirLTStd-Book.ttf') format('truetype'); /* Safari, Android, iOS */
            }
            body {
                font-family: "Avenir", sans-serif;
            }
        </style>

    </head>
    <body>

        <div class="container">

            <div class="form">

                <div class="login-img text-center"><img src="../img/logos/uic2.png" height="40px" /></div>
                <div id="info"></div>
                <form class="register-form" id ="regForm" method="post" action="#">
                    <span style="font-weight: bold; font-size: 18px; margin: 20px 0px;">Registration</span>
                    <?php include 'views/_new_insurer.php'; ?>
                </form>
                <form class="login-form" id="loginForm" method="post" action="#">
                    <div class="form-group">
                        <input type="email" id="email_log_in" name="email_log_in" value="arinze.salvation@kraneum.com" 
                               class="input_" placeholder="Email Address" data-parsley-required data-parsley-type="email"/>
                        <input type="password" name="password_log_in"  id="password_log_in" class="input_"
                               value="aaaa" maxlength="12" data-parsley-required  placeholder="Password"/>
                        <a href="../php/forgotpasswrd.php?entity=user" id="lnkforget">Forgot?</a>

                    </div>
                    <div id="license" style="margin-bottom: 20px;">By clicking Log In, you agree to our<a href=""> License Agreement</a></div>
                    <button id="btn-login" class="btn btn-success signinbtn" type="submit" name="btn-login">LOG IN</button>

                </form>

            </div>
            <p class="text-center">Don't have an account? <a href="register.php" style="color: #000000; font-weight: bold;">Sign Up</a></p>
        </div>

        <script src="../js/jquery-3.1.1.js" type="text/javascript"></script>
        <script src="../js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="../js/parsley.min.js" type="text/javascript"></script>
        <script src="../js/bootstrap.js" type="text/javascript"></script>

        <script type="text/javascript">
            $('.message a').click(function () {
                $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
            });
        </script>

        <!-- Login Form -->
        <script type="text/javascript">
            $(document).ready(function () {

                $('#loginForm').parsley();
                $('#regForm').parsley();

                $('body').on('submit', '#loginForm', (function (event) {
                    event.preventDefault();
                    $('#btn-login').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
                    $('#info').empty();

                    var formData = {
                        'email': $('input[name=email_log_in]').val(),
                        'password': $('input[name=password_log_in]').val()
                    };
                    $.post('../php/suracommands.php?command=validate_insurer_credentials', formData, function (data) {
                        data = JSON.parse(data);

                        if (data['status']) {
                            if (data['err'] === 'UNAPPROVED') {
                                window.location.href = "../php/reverify.php";
                            } else {
                                window.location.href = "home.php";
                            }
                        } else {
                            $('#btn-login').html('LOG IN');
                            if (data['err'] === 'INVALID') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>Invalid login credentials</strong>'
                                        + ' </div>');
                                $('input[name=email_log_in]').addClass('parsley-error');
                                $('input[name=password_log_in]').addClass('parsley-error');
                            } else if (data['err'] === 'NO_USERNAME') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong">Please enter username</strong>'
                                        + ' </div>');
                                $('input[name=email_log_in]').addClass('parsley-error');
                            } else if (data['err'] === 'NO_PASSWORD') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center" >'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>Please enter password</strong>'
                                        + ' </div>');
                                $('input[name=password_log_in]').addClass('parsley-error');
                            } else if (data['err'] === 'DATABASE_CONNECT_ERROR') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>Login failed</strong>. Please try again later'
                                        + ' </div>');
                            }
                            setTimeout(function () {
                                $('#info').slideUp().empty().slideDown();
                            }, 4000);
                        }
                    });

                    
                }));


            });

        </script>
    </body>
</html>

