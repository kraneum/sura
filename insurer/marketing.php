<?php
require './fragments/header.php';

$pay_rate = getData('get_uic_sms_rate')['sms_rate'];
?>

<div id="query-main" class="container">
    <div class="row">
        <div class="col-sm-10">
            <h4><strong><i class="fa fa-calendar"></i> Market your policies</strong></h4>
            <small>&emsp;&emsp;You can buy SMS credit here and send targeted messages</small>
        </div>
        <div class="col-sm-2 pull-right">
            <strong>SMS Units:</strong> <span id = "sms_wallet"><?= $sms_wallet ?></span> Units
        </div>
    </div>

    <div class="row">
        <div id="sms_info" style="margin-left: 20%; margin-right: 20%; padding-top: 20px"></div>
        <div class="col-sm-3" style="padding: 20px">
            <form id="userSearchFilter" style="margin: 3%">
                <div class="row">
                    <label for="gender">Gender </label>
                    <select class="form-control" id="gender" name="gender">
                        <option selected value=""></option>
                        <option value="MALE">Male</option>
                        <option value="FEMALE">Female</option>
                    </select>
                </div><br/>

                <div class="row">
                    <label for="active">Active </label>
                    <select class="form-control" id="active" name="active">
                        <option value="TRUE" selected>True</option>
                        <option value="FALSE">False</option>
                    </select>
                </div><br/>
                <div class="row" >
                    <label for="points_range">Number of points </label>
                    <input type="text" id="points_range" name="points_range" value=""/>
                </div><br/>
                <div class="row">

                    <button id="filterSubmit" type="submit" class="btn btn-xs btn-success btn-rounded pull-right" style="margin-top: 20px" name="btn-register">
                        <i class="fa fa-filter btn-icon-left btn-rounded"></i> Query</button>
                </div>

            </form>
        </div>

        <div id="query_display" class="col-sm-offset-1 col-sm-8" style="margin: 3%; padding-bottom: 10px;">
            <div class="row"  style="background-color: white; padding: 3%; border: #e4e3e3 solid 2px; overflow-y:auto;">

            </div>
            <br/>
            <div id="pagination" class="center-block"></div>

            <div class="row">
                <button id="buy-credit" type="button" class="btn btn-xs btn-default pull-left btn-rounded" 
                        name="buy-credit" data-toggle = "modal" data-target = "#smsModal">Buy SMS Credit</button>
                <button id="messageAll" type="submit" class="btn btn-xs btn-default btn-rounded pull-right disabled"
                        name="btn-message-all" data-toggle = "modal" data-target = "#messageModal" data-userids="">
                    <i class="fa fa-envelope-o btn-icon-left btn-rounded"></i>Message All</button>
            </div>

        </div>

    </div>


    <!---- Modal ---->
    <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-userids = "">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Send Message</h4>
                </div>

                <form id="messageForm">
                    <div class="modal-body">
                        <div id="info"></div>
                        <textarea id="notifMessage" name="notifMessage" rows="5" cols="65" maxlength="100" data-parsley-required data-parsley-minlength="4" data-parsley-maxlength="100" data-parsley-maxlength-message="This value should be less than or equal 100 characters" data-parsley-minlength-message="This value should be at least 4 characters"></textarea>

                    </div>
                    <div class="modal-footer">
                        <button id="notifSend" type="submit" class="btn btn-sm btn-success btn-rounded pull-right" style="margin-top: 20px" name="btn-notif-send">
                            <i class="fa fa-envelope-o btn-icon-left btn-rounded"></i>Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!---- SMS Modal ---->
    <div class="modal fade" id="smsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-userids = "">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Buy SMS Credit</h4>
                </div>


                <div class="modal-body">
                    <form id="buySMSForm">
                        <script src="https://js.paystack.co/v1/inline.js"></script>
                        <input type="hidden" id="pay_id" value="<?= $id ?>"/>
                        <input type="hidden" id="pay_rate" value="<?= $pay_rate ?>"/>
                        <div class="row">
                            <div class = "form-group col-md-6">
                                <label for = "pay_email" class = "control-label">Email</label>
                                <input class="form-control" readonly id="pay_email" name="pay_email" value="<?= $email ?>"/>
                            </div>

                            <div class = "col-md-6 text-right">
                                <img src = "../img/logos/mastercard.jpg" alt = "" width="25px" height="17px"/>
                                <img src = "../img/logos/visa.jpg" alt = "" width="25px" height="17px"/>
                                <img src = "../img/logos/verve.png" alt = "" width="35px" height="17px"/>
                                <img src = "../img/logos/discover.jpg" alt = "" width="25px" height="17px"/>
                            </div>
                        </div>
                        <div class = "row">
                            <div class = "form-group col-md-6">
                                <label for = "pay_points" class = "control-label">Number of SMS Points</label>
                                <input  class="form-control" id="pay_points" name="pay_points" value="" data-parsley-required data-parsley-type="digits"/>
                                <span class="help-block text-success" >Charging amount: &#8358;<span>0</span></span>
                            </div>


                        </div>

                        <div class="row">
                            <span class="col-md-6 small text-muted">By clicking "Top Up", you agree to our <a href = "#" >Terms</a> and <a href = "#" >Privacy Policy</a></span>
                            <div class="col-md-6 text-right">
                                <button type="submit" class="btn btn-lg btn-success" >Top Up</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>

<div id="sms_info" style="position: absolute;
     bottom: 1%;
     right: 2%;
     display: none;"></div>

<?php
include './fragments/footer.php';
?>

<script src="../js/jquery-3.1.1.js" type="text/javascript"></script>
<script src="../js/bootstrap.js" type="text/javascript"></script>
<script src="../js/parsley.min.js" type="text/javascript"></script>
<script src="../js/ion.rangeSlider.js" type="text/javascript"></script>
<script src="../js/jquery.simplePagination.js" type="text/javascript"></script>
<script src="../js/spin.min.js" type="text/javascript"></script>
<script src="../js/spinner-config.js" type="text/javascript"></script>
<script src="../js/moment.min.js" type="text/javascript"></script>
<script src="../js/script_i.js" type="text/javascript"></script>

</body>
</html>
