<?php
require_once './fragments/header.php';
?>


<div id="policies-main" class="container">
    <h4><strong><i class="fa fa-calendar"></i> View all policies</strong></h4>
    <small>&emsp;&emsp;You can view all policies and create new ones</small>
    <div style="margin-top: 18px; margin-bottom: 3em; ">
        <!-- Redeemed Points -->
        <div class="col-sm-6 lead"></div>
        <div class="col-sm-6">
            <button id="publishPolicy" class="btn btn-defuult btn-rounded box-shadow--2dp btn-xs pull-right" style="margin-right: 15px">Publish new policy</button>
        </div>

    </div>

    <div>
        <div class="row container">
            <div id="info" style="margin-top: 10px; margin-left: 20%; margin-right: 20%;"></div>

            <div id="content">
                <div id="primary">
                    <div class="bs-divider insure_title"><i class="fa fa-star smaller"></i> Standard Policies <i class="fa fa-star smaller"></i></div>
                    <div class="">
                        <div id="standard-table">
                            <div class="table-responsive">
                                <table class="table">

                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Title</th>
                                            <th class="text-center">Description</th>
                                            <th class="text-center">Price</th>
                                            <th class="text-center">Tenure</th>
                                        </tr>
                                    </thead>

                                    <tbody>

                                        <?php
                                        $policies = getData('get_uic_policies_by_type', $type_id);

                                        for ($i = 0; $i < count($policies); $i++) {
                                            $id = $i + 1;
                                            ;
                                            $title = $policies[$i]['title'];
                                            $desc = $policies[$i]['description'];
                                            $price = $policies[$i]['price'];
                                            $tenure = $policies[$i]['duration'];

                                            echo '<tr class="span-title">';
                                            echo '<td class = "text-center">' . $id . '</td>';
                                            echo '<td class = "text-center">' . $title . '</td>';
                                            echo '<td class = "text-center">' . $desc . '</td>';
                                            echo '<td class = "text-center">' . $price . '</td>';
                                            echo '<td class = "text-center">' . $tenure . ' month(s)</td>';
                                            echo '</tr>';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="bs-divider insure_title" style="padding-top: 30px;">Custom Policies</div>
                    <div id="sub-content">
                        <div id="custom-table" style="display: none">
                            <div class="table-responsive">
                                <table class="table">

                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Title</th>
                                            <th class="text-center">Description</th>
                                            <th class="text-center">Price</th>
                                            <th class="text-center">Tenure</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>

                                    <tbody class="span-title">

                                    </tbody>
                                </table>
                            </div>
                            <div id="pagination" class="center-block"></div>
                        </div>
                    </div>
                </div>
                <div id="secondary"></div>
            </div>


            <!-------------- Modal ---------------------->
            <div class="modal fade" id="continue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Confirm Delete</h4>
                        </div>
                        <div class="modal-body">
                            <p>You are about to <strong>de-activate</strong> this policy. Having read the <a href="#">Terms</a> and <a href="#">Conditions</a>, do you wish to continue?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-rounded box-shadow--2dp btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" id="modalDeactivateBtn" class="btn btn-danger btn-rounded box-shadow--2dp" data-policyid="">De-activate</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include './fragments/footer.php';
?>

<script src="../js/jquery-3.1.1.js" type="text/javascript"></script>
<script src="../js/bootstrap.js" type="text/javascript"></script>
<script src="../js/parsley.min.js" type="text/javascript"></script>
<script src="../js/jquery.simplePagination.js" type="text/javascript"></script>
<script src="../js/spin.min.js" type="text/javascript"></script>
<script src="../js/spinner-config.js" type="text/javascript"></script>
<script src="../js/script_i.js" type="text/javascript"></script>
</body>
</html>