<?php
require '../php/curl.php';

$sq = getData('get_security_questions');
$types = getData('get_insurance_types_by_category', 0);
?>

<!DOCTYPE html>
<html>
    <head>
        <title>iInsurance</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="icon" type="image/png" href="../img/favicon.ico">
        <link rel="apple-touch-icon" href="../img/favicon.ico">

        <link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
        <link href="../css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style_login.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">
            @font-face {
                font-family: 'Avenir';
                src: url('../fonts/AvenirLTStd-Book.woff2') format('woff2'), /* Super Modern Browsers */
                    url('../fonts/AvenirLTStd-Book.woff') format('woff'), /* Pretty Modern Browsers */
                    url('../fonts/AvenirLTStd-Book.ttf') format('truetype'); /* Safari, Android, iOS */
            }
            body {
                font-family: "Avenir", sans-serif;
            }
        </style>

    </head>
    <body>

        <div class="container">

            <div class="form">

                <div class="login-img"><img src="../img/logos/uic2.png" class="img-responsive" /></div>
                <div class="alert alert-danger alert-dismissable" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong id="info"></strong>
                </div>

                <form id ="regForm" method="post" action="#">

                    <div class="form-group">
                        <label class="label_" for="name"> Name * </label>
                        <input class="input_" id="name" name="name" value="Grace Insurance"  type="text" placeholder="Enter company name"  data-parsley-required/>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="desc">Description (not more than 200 words) * </label>
                        <textarea class="input_" id="desc" name="desc"  type="text" placeholder="Enter description" size="200"  data-parsley-required></textarea>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="type">Insurance type * </label>
                        <select class="input_" id="type" name="type"  data-parsley-required>
                            <option disabled selected>Select Insurance type</option>
                            <?php
                            for ($i = 0; $i < count($types); $i++) {
                                echo '<option value="' . $types[$i]['id'] . '">' . $types[$i]['name'] . '</option>';
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="phone">Phone * </label>
                        <input class="input_" id="phone" name="phone" value="09057183473" placeholder="Enter phone number" data-parsley-required data-parsley-minlength="11" data-parsley-maxlength="11"  data-parsley-type="digits" data-parsley-minlength-message="This value should be 11 digits" data-parsley-maxlength-message="This value should be 11 digits" data-parsley-type-message="This value should contain digits only" data-parsley-remote="insurer_phone_available.php?phone={value}" data-parsley-trigger="keyup" data-parsley-remote-message="Phone number already in use" data-parsley-debounce="500" />
                    </div>

                    <div class="form-group">
                        <label class="label_" for="email">Email * </label>
                        <input class="input_" id="email" name="email" value="modernbabbage@gmail.com"  type="email" placeholder="Enter email"  data-parsley-required   data-parsley-remote="insurer_email_available.php?email={value}" data-parsley-trigger="keyup" data-parsley-remote-message="Email already in use" data-parsley-debounce="500" />
                    </div>

                    <div class="form-group">
                        <label class="label_" for="rc">RC Number * </label>
                        <input class="input_" id="rc" name="rc" value="95478654" type="text" placeholder="Enter RC number" data-parsley-required/>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="extlink">Corporate Web Address  * </label>
                        <input class="input_" id="extlink" name="extlink" value="http://www.google.com" type="url" placeholder="e.g http://www.google.com " data-parsley-required data-parsley-type="url" data-parsley-type-message="This is not a valid URL"/>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="question">Security question * </label>
                        <select class="input_" id="question" name="question"  data-parsley-required>
                            <option disabled selected>Select security question</option>
                            <?php
                            for ($i = 0; $i < count($sq); $i++) {
                                echo '<option value = "' . $sq[$i]['question'] . '">' . $sq[$i]['question'] . '</option>';
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="answer">Security Answer * </label>
                        <input class="input_" id="answer" name="answer" value="Italy"  type="text" placeholder="Enter security answer"  data-parsley-required/>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="password">Password * </label>
                        <input class="input_" id="password" name="password_r" value="zeus"  type="password" placeholder="Enter password"  data-parsley-required/>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="password_confirm">Confirm Password * </label>
                        <input class="input_" id="password_confirm" name="password_confirm" value="zeus"  type="password" placeholder="Enter password again" data-parsley-equalto="#password" data-parsley-equalto-message="Password mismatch" data-parsley-required/>
                    </div>

                    <div id="license" style="margin-bottom: 20px;">By clicking Sign up, you agree to our<a href="#"> Terms of Use</a> and <a href="#"> Privacy statement </a></div>
                    <div class="form-group">
                        <div class="row">
                            <button id="formSignup" type="submit" class="btn btn-success signinbtn"  name="btn-register">SIGN UP</button>
                        </div>
                    </div>

                </form>

            </div>
            <p class="message text-center" style="margin-bottom: 5%">Already Registered? <a href="login.php" style="color: #000000; font-weight: bold;">Sign In</a></p>
        </div>
        <script src="../js/jquery-3.1.1.js" type="text/javascript"></script>
        <script src="../js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="../js/parsley.min.js" type="text/javascript"></script>
        <script src="../js/bootstrap.js" type="text/javascript"></script>

        <!-- Registration Form -->
        <script type="text/javascript">
            $(document).ready(function () {

                $('#regForm').parsley();

                $('body').on('submit', '#regForm', (function (event) {
                    $('#formSignup').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
                    $('#info').empty();

                    var formData = {
                        'name': $('input[name=name]').val(),
                        'desc': $('textarea[name=desc]').val(),
                        'type': $("#type option:selected").val(),
                        'rc': $('input[name=rc]').val(),
                        'extlink': $('input[name=extlink]').val(),
                        'phone': $('input[name=phone]').val(),
                        'email': $('input[name=email]').val(),
                        'question': $("#question option:selected").text(),
                        'answer': $('input[name=answer]').val(),
                        'password': $('input[name=password_r]').val()
                    };
                    $.post('../php/suracommands.php?command=register_insurer', formData, function (data) {
                        data = JSON.parse(data);

                        $('#formSignup').html('<i class="fa fa-lock btn-icon-left"></i> SIGN UP');
                        if (data['status']) {
                            window.location.href = "../php/checkmail_i.php";
                        } else {
                            if (data['err'] === 'VALIDATION_ERROR') {
                                var obj = data['errVal']['field'];
                                var error = data['errVal']['message'];
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center" >'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>' + error + '</strong></div>');
                                $('#' + obj).addClass('parsley-error');
                                $("html, body, #left").animate({scrollTop: 0}, "fast");
                            } else if (data['err'] === 'DUPLICATE_ERROR') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center" >'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>' + data['errColumn'] + ' already in use</strong></div>');
                                $('#regForm #' + data['errColumn'].toString().toLowerCase()).addClass('parsley-error');
                                $("html, body, #left").animate({scrollTop: 0}, "fast");
                            } else if (data['err'] === 'REGISTRATION_ERROR') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center" >'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>Registration failed. Please try again</strong></div>');
                                $("html, body, #left").animate({scrollTop: 0}, "fast");
                            } else if (data['err'] === 'DATABASE_CONNECT_ERROR') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center" >'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>Internal error occured</strong></div>');
                                $("html, body, #left").animate({scrollTop: 0}, "fast");
                            }
                        }
                    });
                    event.preventDefault();
                }));


            });

        </script>
    </body>
</html>