<?php
if (!isset($_SESSION)) {
    session_start();
}

if (empty($_SESSION['entity']['is_lead'])) {
    header('Location: home.php');
}

require_once './fragments/header.php';
require_once('../php/curl.php');
?>


<div id="policies-main" class="container">
    <h4><strong><i class="fa fa-users"></i> View all generated users</strong></h4>
    <small>&emsp;&emsp;You can also download and re-upload the generated users file</small>

    <div>
        <div class="row container">
            <div id="info" style="margin-top: 20px; margin-left: 20%; margin-right: 20%;"></div>

            <div id="content">
                <div id="primary" style="margin-top: 20px">
                    <div class="">
                        <div id="standard-table">
                            <?php
                            $gen_users = getData('get_generated_users', $type_id);
                            $sn = 0;
                            if (count($gen_users)) {
                                ?>
                                <div class="table-responsive">
                                    <table class="table">

                                        <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th class="text-center">List</th>
                                                <th class="text-center">Date</th>
                                                <th class="text-center"></th>
                                                <th class="text-center"></th>
                                                <th class="text-center"></th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php
                                            foreach ($gen_users as $user) {
                                                $sn += 1;
                                                $id = $user['id'];
                                                $list_name = $user['list_name'];
                                                $date = $user['date_created'];

                                                echo '<tr class="span-title">';
                                                echo '<td class = "text-center">' . $sn . '</td>';
                                                echo '<td class = "text-center">' . $list_name . '</td>';
                                                echo '<td class = "text-center">' . date_format(date_create($date), 'd F, Y') . '</td>';
                                                echo '<td class = "text-center"><a href="../generated_users/health/' . $list_name . '.csv" download>Download</a></td>';
                                                echo '<td class = "text-center">';
                                                echo $user['status'] == 'TRUE' ? '<i class="fa fa-check" style="color: green"></i>' : '<button  type="button" class="btn btn-primary btn-xs uploadModalTrigger" data-toggle="modal" data-target="#uploadModal" data-rowid="' . $id . '">Upload</button></td>';
                                                echo '</tr>';
                                            }
                                        } else {
                                            echo '<div class="text-center"><h3>No generated users at this time</h3><br/>Check back at a later time<div>';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


                <!-------------- Modal ---------------------->
                <div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Upload</h4>
                            </div>

                            <div class="modal-body">
                                <form id="uploadUsersForm" data-rowid="" enctype="multipart/form-data" style="margin-bottom: 20px">
                                    <input type='hiddden' name="rowid" value="" style="display: none"/>
                                    <input type='hiddden' name="typeid" value="<?= $type_id ?>" style="display: none"/>
                                    <div class="form-group">
                                        <label for="list">File input</label>
                                        <input type = "file" class = "form-control" id="list" name="list" placeholder="Enter number of iPoints" data-parsley-required data-parsley-trigger="change" accept=".csv" data-parsley-filemimetypes="text/csv, application/csv, application/vnd.ms-excel, text/x-csv"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="remarks">Remarks</label>
                                        <textarea class="form-control" id="remarks" name="remarks" placeholder="Enter additional remarks here"></textarea>
                                    </div>
                                    <div class="row">
                                        <button type="submit" class="btn btn-default pull-right" style="margin-right: 10px">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        include './fragments/footer.php';
        ?>

        <script src="../js/jquery-3.1.1.js" type="text/javascript"></script>
        <script src="../js/bootstrap.js" type="text/javascript"></script>
        <script src="../js/parsley.min.js" type="text/javascript"></script>
        <script src="../js/parsley-laraextras.min.js" type="text/javascript"></script>
        <script src="../js/parsley_file_validators.js" type="text/javascript"></script>
        <script src="../js/jquery.simplePagination.js" type="text/javascript"></script>
        <script src="../js/spin.min.js" type="text/javascript"></script>
        <script src="../js/spinner-config.js" type="text/javascript"></script>
        <script src="../js/script_i.js" type="text/javascript"></script>
        </body>
        </html>