<?php
session_start();
require '../../php/curl.php';

$type_id = $_SESSION['entity']['insurance_type'];
?>

<button id="policy-back" class="btn btn-xs btn-default btn-rounded box-shadow--2dp formButtons"
        name="policy-back" style="margin-bottom: 2em;"><i class="fa fa-arrow-left"></i> BACK</button>

<div style="margin-left: 10%; margin-right: 10%;">
    <form id ="newPolicyForm"  data-parsley-validate>

        <input type="hidden" name="type" value="<?= $type_id ?>"/>

        <div class="form-group">
            <label for="title"> Title * </label>
            <input class="form-control" id="title" name="title"  type="text" placeholder="Enter title"  data-parsley-required/>
        </div>

        <div class="form-group">
            <label for="desc">Description <span class="small text-italic" style="font-weight: normal;">(not more than 100 words)</span> * </label>
            <textarea class="form-control" id="desc" name="desc"  type="text" placeholder="Enter description" size="200"  data-parsley-required></textarea>
        </div>

        <div class="form-group">
            <label for="extlink">More Info Link  * </label>
            <input class="form-control" id="extlink" name="extlink" value="http://www.google.com" type="url"
                   placeholder="e.g http://www.google.com " data-parsley-required data-parsley-type="url" data-parsley-type-message="This is not a valid URL"/>
        </div>

        <div class="form-group">
            <label for="price">Price <span class="small text-italic" style="font-weight: normal;">(in iPoints)</span> * </label>
            <input class="form-control" id="price" name="price"  type="text" placeholder="Enter policy price" 
                   data-parsley-required data-parsley-type-message="This value should be digits" data-parsley-type="digits"/>
        </div>

        <div class="form-group">
            <label for="tenure">Tenure * </label>
            <select class="form-control" id="tenure" name="tenure"  data-parsley-required>
                <option disabled selected>Select Tenure</option>
                <?php
                $types = getData('get_payment_structures');
                for ($i = 0; $i < count($types); $i++) {
                    echo '<option value="' . $types[$i]['id'] . '">' . ucfirst($types[$i]['name']) . '</option>';
                }
                ?>
            </select>
        </div>

        <div class="row">
            <button id="policySave" type="submit" class="btn btn-xs btn-success btn-rounded box-shadow--2dp formButtons pull-right"
                    name="policySave" style="margin-right: 15px;">PUBLISH</button>
        </div>
        <div id="license" class="text-muted">By clicking Publish, you agree to our
            <a href="#"> Terms and Conditions</a> and <a href="#"> Privacy statement </a></div>
    </form>
</div>

<script type="text/javascript">
    $('#newPolicyForm').parsley();
</script>
