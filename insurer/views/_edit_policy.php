<?php
session_start();
require_once '../../php/curl.php';

$id = $_POST['policyID'];
$insurerID = $_SESSION['entity']['id'];

$types = getData('get_payment_structures');
$policy = getData('get_insurer_policy_by_id', $insurerID, $id);
$policyID = $policy[0]['id'];
//$type = $policy[0]['type_info'][0]['name'];
$desc = $policy[0]['description'];
$title = $policy[0]['title'];
$price = $policy[0]['price'];
$tenure = $policy[0]['tenure'];
?>
<button id="policy-back" class="btn btn-xs btn-default btn-rounded box-shadow--2dp formButtons"
        name="policy-back" style="margin-bottom: 2em;"><i class="fa fa-arrow-left"></i> BACK</button>

<div style="margin-left: 10%; margin-right: 10%;">
    <form id ="editPolicyForm" data-parsley-validate>
        <input type="hidden" name="id" id="policyid" value="<?php echo $policyID ?>"/>

        <div class="form-group">
            <label for="title"> Title * </label>
            <input class="form-control" id="title" name="title"  type="text" placeholder="Enter company name" value="<?php echo $title ?>" data-parsley-required/>
        </div>

        <div class="form-group">
            <label for="desc">Description (not more than 100 words) * </label>
            <textarea class="form-control" id="desc" name="desc"  type="text" placeholder="Enter description" size="200" data-parsley-required><?php echo $desc ?></textarea>
        </div>

        <div class="form-group">
            <label for="extlink">More Info Link  * </label>
            <input class="form-control" id="extlink" name="extlink" value="http://www.google.com" type="url" placeholder="e.g http://www.google.com" data-parsley-required data-parsley-type="url" data-parsley-type-message="This is not a valid URL"/>
        </div>

        <div class="form-group">
            <label for="price">Price <span class="small text-italic" style="font-weight: normal;">(in iPoints)</span> * </label>
            <input class="form-control" id="price" name="price"  type="text" placeholder="Enter policy price in iPoints" value="<?php echo $price ?>" data-parsley-required data-parsley-digits-message="This value should be digits" parsley-type="digits"/>
        </div>

        <div class="form-group">
            <label for="tenure">Tenure (in months) * </label>
            <select class="form-control" id="tenure" name="tenure"  data-parsley-required>
                <option disabled selected>Select Tenure</option>
                <?php
                for ($i = 0; $i < count($types); $i++) {
                    $selected = $types[$i]['id'] == $tenure ? 'selected' : '';
                    echo '<option value="' . $types[$i]['id'] . '" ' . $selected . '>' . ucfirst($types[$i]['name']) . '</option>';
                }
                ?>
            </select>
        </div>

        <div class="row">
            <button id="policyEdit" type="submit" class="btn btn-xs btn-success btn-rounded formButtons pull-right"  
                    name="policyEdit" style="margin-right: 15px;">SAVE</button>
        </div>
        <div id="license" class="text-muted">By clicking Save, you agree to our<a href="#"> Terms and Conditions</a> and <a href="#"> Privacy statement </a></div>
    </form>
</div>

<!--<script type="text/javascript">
    $('#editPolicyForm').parsley();
</script>-->
