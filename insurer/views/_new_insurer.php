    <div class="form-group">
        <label class="label_" for="name"> Name * </label>
        <input class="input_" id="name" name="name" value="Grace Insurance"  type="text" placeholder="Enter company name"  data-parsley-required/>
    </div>

    <div class="form-group">
        <label class="label_" for="desc">Description (not more than 200 words) * </label>
        <textarea class="input_" id="desc" name="desc"  type="text" placeholder="Enter description" size="200"  data-parsley-required></textarea>
    </div>
    
    <div class="form-group">
        <label class="label_" for="type">Insurance type * </label>
        <select class="input_" id="type" name="type"  data-parsley-required>
            <option disabled selected>Select Insurance type</option>
            <?php
            for ($i = 0; $i < count($types); $i++) {
                echo '<option value="'.$types[$i]['id'].'">'.$types[$i]['name'].'</option>';
            }
            ?>
        </select>
    </div>

    <div class="form-group">
        <label class="label_" for="phone">Phone * </label>
        <input class="input_" id="phone" name="phone" value="09057183473" placeholder="Enter phone number" data-parsley-required data-parsley-minlength="11" data-parsley-maxlength="11"  data-parsley-type="digits" data-parsley-minlength-message="This value should be 11 digits" data-parsley-maxlength-message="This value should be 11 digits" data-parsley-type-message="This value should contain digits only"/>
    </div>

    <div class="form-group">
        <label class="label_" for="email">Email * </label>
        <input class="input_" id="email" name="email" value="modernbabbage@gmail.com"  type="email" placeholder="Enter email"  data-parsley-required/>
    </div>

    <div class="form-group">
        <label class="label_" for="rc">RC Number * </label>
        <input class="input_" id="rc" name="rc" value="95478654" type="text" placeholder="Enter RC number" data-parsley-required/>
    </div>
    
    <div class="form-group">
        <label class="label_" for="extlink">Corporate Web Address  * </label>
        <input class="input_" id="extlink" name="extlink" value="http://www.google.com" type="url" placeholder="e.g http://www.google.com " data-parsley-required data-parsley-type="url" data-parsley-type-message="This is not a valid URL"/>
    </div>

    <div class="form-group">
        <label class="label_" for="question">Security question * </label>
        <select class="input_" id="question" name="question"  data-parsley-required>
            <option disabled selected>Select security question</option>
            <?php
            for ($i = 0; $i < count($sq); $i++) {
                echo '<option value = "' . $sq[$i]['question'] . '">' . $sq[$i]['question'] . '</option>';
            }
            ?>
        </select>
    </div>

    <div class="form-group">
        <label class="label_" for="answer">Security Answer * </label>
        <input class="input_" id="answer" name="answer" value="Italy"  type="text" placeholder="Enter security answer"  data-parsley-required/>
    </div>

    <div class="form-group">
        <label class="label_" for="password">Password * </label>
        <input class="input_" id="password" name="password_r" value="zeus"  type="password" placeholder="Enter password"  data-parsley-required/>
    </div>

    <div class="form-group">
        <label class="label_" for="password_confirm">Confirm Password * </label>
        <input class="input_" id="password_confirm" name="password_confirm" value="zeus"  type="password" placeholder="Enter password again" data-parsley-equalto="#password" data-parsley-equalto-message="Password mismatch" data-parsley-required/>
    </div>
     <div id="license">By clicking Sign up, you agree to our<a href="#"> Terms of Use</a> and <a href="#"> Privacy statement </a></div>
    <div class="form-group">
        <div class="row">
            <button id="formSignup" type="submit" class="btn btn-success signinbtn"  name="btn-register"><i class="fa fa-lock btn-icon-left"></i> SIGN UP</button>
        </div>   
    </div>
<p class="message">Already registered? <a href="#" class="text-bold">Sign In</a></p>

