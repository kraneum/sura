<?php
session_start();

$isValidated = $_SESSION['entity']['validated'];
?>
<div id="upload" style="margin: 2% 5% 2%">
    <form id="uploadLogoForm" enctype="multipart/form-data">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <div class="form-group">
                    <label for="logo">Company Logo <span class="text-muted text-italic">(JPEG / PNG format)</span> * </label>
                    <input class="settings-input" id="logo" name="logo"  type="file" accept="image/jpeg, image/png" data-parsley-filemaxmegabytes="2" data-parsley-trigger="change" data-parsley-dimensions="true" data-parsley-dimensions-options='{"min_width": "100", "max_width": "100"}' data-parsley-filemimetypes="image/jpeg, image/png" data-parsley-required/>
                </div>
            </div>
            <div class="col-sm-2">
                <button id="logo_save" type="submit" class="btn btn-success btn-xs box-shadow--2dp settings-btn btn-rounded" style="letter-spacing: 2px; padding: 7px; margin-top: 30px;" name="logo_save"><strong>SAVE</strong></button>
            </div>
        </div>
    </form>


    <div class="bs-divider">Upload Documents <span class="text-muted text-italic">(JPEG / PNG / PDF / Word format)</span></div>

    <div id="upload-doc">
        <?php
        if ($isValidated != 'TRUE') {
            echo'<form id="uploadDocumentsForm" enctype="multipart/form-data" style="margin-bottom: 20px">
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <label>Documents</label>
                        <input class="settings-input" name="docs[]" type="file" accept="image/jpeg, image/png, application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document" data-parsley-required data-parsley-trigger="change" data-parsley-filemimetypes="image/jpeg, image/png, application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document"/>
                    </div>
                </div>
                <div class="col-sm-2"></div>
            </div>

            <div class="row" id="doc_buttons" style="margin-top: 4%">
                <div class="col-sm-offset-2 col-sm-2">
                    <!--           <button id="settings-back" type="button" class="btn btn-primary box-shadow--2dp settings-btn btn-rounded btn-lg" name="back"><i class="fa fa-arrow-left"></i></button>-->
                    <button id="new_doc" type="button" class="btn btn-success btn-rounded btn-sm" name="back"><i class="fa fa-plus"></i> Add</button>
                </div>
                <div class="col-sm-6"></div>
                <div class="col-sm-2">';
            if ($isValidated == 'FALSE') {
                echo '<button id = "documents_save" type = "submit" class = "btn btn-success box-shadow--2dp settings-btn btn-rounded" name = "documents_save"><strong>SAVE</strong></button>';
            }
            echo '</div>
            </div>
        </form>';
        } else {
            echo '<img class="img-responsive center-block" src="../img/approved.png" alt="Approved" style="padding:13%; padding-left:40%; opacity:.5"/>';
        }
        ?>
    </div>
</div>