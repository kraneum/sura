<?php
if (isset($_SESSION['verify'])) {
    session_destroy();
}

if (isset($_SESSION['entity'])) {
    header('Location: home.php');
}

require '../php/curl.php';

$sq = getData('get_security_questions');
$types = getData('get_insurance_types_by_category', 0);
?>

<!DOCTYPE html>
<html>
    <head>
        <title>iInsurance</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="icon" type="image/png" href="../img/favicon.ico">
        <link rel="apple-touch-icon" href="../img/favicon.ico">

        <link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
        <link href="../css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style_login.css" rel="stylesheet" type="text/css"/>

    </head>
    <body>

        <div class="container">

            <div class="form">

                <div class="login-img"><img src="../img/logos/uic2.png" class="img-responsive" /></div>
                <div class="alert alert-danger alert-dismissable" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong id="info"></strong>
                </div>
                <form class="register-form" id ="regForm" method="post" action="#">
                    <span style="font-weight: bold; font-size: 18px; margin: 20px 0px;">Registration</span>
<?php include 'views/_new_insurer.php'; ?>
                </form>
                <form class="login-form" id="loginForm" method="post" action="#">
                    <div class="form-group">
                        <input type="text" id="email" name="email" value="modernbabbagee@gmail.com" class="input_" placeholder="Email Address"/>
                        <input type="password" placeholder="Password" id="password" class="input_" name="password" value="aaaa" maxlength="12" data-parsley-required />
                        <a href="../php/forgotpasswrd.php?entity=user" id="lnkforget">Forgot?</a>

                    </div>
                    <div id="license">By clicking Sign In, you agree to our<a href=""> License Agreement</a></div>
                    <button id="btn-login" class="btn btn-success signinbtn" type="submit" name="btn-login">Sign In</button>
                    <p class="message">Don't have an account? <a href="#">Sign Up</a></p>

                </form>

            </div>

        </div>

        <script src="../js/jquery-3.1.1.js" type="text/javascript"></script>
        <script src="../js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="../js/parsley.min.js" type="text/javascript"></script>
        <script src="../js/bootstrap.js" type="text/javascript"></script>

        <script type="text/javascript">
            $('.message a').click(function () {
                $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
            });
        </script>

        <!-- Login and Registration Form -->
        <script type="text/javascript">
            $(document).ready(function () {

                $('#loginForm').parsley();
                $('#regForm').parsley();

                $('body').on('submit', '#loginForm', (function (event) {
                    $('#btn-login').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
                    $('#info').empty();

                    var formData = {
                        'email': $('input[name=email]').val(),
                        'password': $('input[name=password]').val()
                    };
                    $.post('../php/suracommands.php?command=validate_insurer_credentials', formData, function (data) {
                        data = JSON.parse(data);

                        $('#btn-login').html('LOG IN');
                        if (data['status']) {
                            if (data['err'] === 'UNAPPROVED') {
                                window.location.href = "../php/reverify.php";
                            } else {
                                window.location.href = "home.php";
                            }
                        } else {
                            if (data['err'] === 'INVALID') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>Invalid login credentials</strong>'
                                        + ' </div>');
                                $('input[name=email]').addClass('parsley-error');
                                $('input[name=password]').addClass('parsley-error');
                            } else if (data['err'] === 'NO_USERNAME') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong">Please enter username</strong>'
                                        + ' </div>');
                                $('input[name=email]').addClass('parsley-error');
                            } else if (data['err'] === 'NO_PASSWORD') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center" >'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>Please enter password</strong>'
                                        + ' </div>');
                                $('input[name=password]').addClass('parsley-error');
                            } else if (data['err'] === 'DATABASE_CONNECT_ERROR') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>Login failed</strong>. Please try again later'
                                        + ' </div>');
                            }
                        }
                    });

                    event.preventDefault();
                }));

                $('body').on('submit', '#regForm', (function (event) {
                    $('#formSignup').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
                    $('#info').empty();

                    var formData = {
                        'name': $('input[name=name]').val(),
                        'desc': $('textarea[name=desc]').val(),
                        'type': $("#type option:selected").val(),
                        'rc': $('input[name=rc]').val(),
                        'extlink': $('input[name=extlink]').val(),
                        'phone': $('input[name=phone]').val(),
                        'email': $('input[name=email]').val(),
                        'question': $("#question option:selected").text(),
                        'answer': $('input[name=answer]').val(),
                        'password': $('input[name=password_r]').val()
                    };
                    $.post('../php/suracommands.php?command=register_insurer', formData, function (data) {
                        data = JSON.parse(data);

                        $('#formSignup').html('<i class="fa fa-lock btn-icon-left"></i> SIGN UP');
                        if (data['status']) {
                            window.location.href = "../php/checkmail.php";
                        } else {
                            if (data['err'] === 'VALIDATION_ERROR') {
                                var obj = data['errVal']['field'];
                                var error = data['errVal']['message'];
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center" >'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>' + error + '</strong></div>');
                                $('#' + obj).addClass('parsley-error');
                                $("html, body, #left").animate({scrollTop: 0}, "fast");
                            } else if (data['err'] === 'DUPLICATE_ERROR') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center" >'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>' + data['errColumn'] + ' already in use</strong></div>');
                                $('#regForm #' + data['errColumn'].toString().toLowerCase()).addClass('parsley-error');
                                $("html, body, #left").animate({scrollTop: 0}, "fast");
                            } else if (data['err'] === 'REGISTRATION_ERROR') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center" >'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>Registration failed. Please try again</strong></div>');
                                $("html, body, #left").animate({scrollTop: 0}, "fast");
                            } else if (data['err'] === 'DATABASE_CONNECT_ERROR') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center" >'
                                        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
                                        + '<strong>Internal error occured</strong></div>');
                                $("html, body, #left").animate({scrollTop: 0}, "fast");
                            }
                        }
                    });
                    event.preventDefault();
                }));
            });

        </script>
    </body>
</html>

