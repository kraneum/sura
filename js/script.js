/*jslint browser: true*/
/*global $ alert */
$(document).ready(function () {
    //**************  Variables  ******************//

    var totalItems = null;
    var NoOfItemsOnDisplay = null;
    var limit = null;
    var offset = null;
    var isItemRemaining = false;
    var isEmpty = true; // to check whether there are elements at all...
    $urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results !== null) {
            return results[1] || 0;
        }
        return null;
    };
    //********** ********************//

    //************ Add policy links *********************//

    $('body').on('click', '.policy-link', function () {
        $('#secondaryView').hide();
        $('#policiesView').show();
        $('#pagination').show();
        $('#policies-back').remove();
    });
    $('#generalLink').click(function () {
        $('#info').empty();
        $('#policiesView').data('typeid', $(this).data('typeid'));
        changePolicyLinks($(this));
        $('#policiesView').append(spinner.el);
        $('#policiesView').addClass('overlay');
        getTotalItems();
    });

    $('#healthLink').click(function () {
        $('#info').empty();
        $('#policiesView').data('typeid', $(this).data('typeid'));
        changePolicyLinks($(this));
        $('#policiesView').append(spinner.el);
        $('#policiesView').addClass('overlay');
        getTotalItems();
    });

    //************ For pagination *********************//

    var filenameWithExtension = location.pathname.split('/').slice(-1)[0];
    var filename = filenameWithExtension.split(".")[0];

    //****** For notifications *****************//

    $(".dropdown-toggle").hover(function () {
        $('#notifCount').removeClass('animated');
        $('#notifCount').removeClass('swing');
        $('#notifCount').removeClass('infinite');
    }, function () {
        $('#notifCount').addClass('animated');
        $('#notifCount').addClass('swing');
        $('#notifCount').addClass('infinite');
    });
    if ($('.dropdown-menu').data('notifcount') === 0) {
        getNotifications(false);
    } else {
        getNotifications(true);
    }
    $('#redeemPinBtn').prop('disabled', true); //disables Top Up button in redeem pin form

    //*********** For Pop-over *********//

    $('#dp_info').popover();
    $('#pp_info').popover();


    //*********** Tab Active Selection *********//
    if (filename === 'home') {
        $('ul.navbar-nav > li:nth-child(1)').addClass('active');

        limit = 5;
        offset = 0;

        getRecentActivity(limit, offset);
    } else if (filename === 'policies') {

        $('ul.navbar-nav > li:nth-child(2)').addClass('active');
    } else if (filename === 'addnewpolicy') {
        NoOfItemsOnDisplay = 5;
        $('ul.navbar-nav > li:nth-child(2)').addClass('active');
        var policy = $urlParam('type');
        if (policy === 'general') {
            $("#generalLink").trigger("click");
        } else if (policy === 'health' || policy === null) {
            $("#healthLink").trigger("click");
        }
    } else if (filename === 'policydetails') {
        $('ul.navbar-nav > li:nth-child(2)').addClass('active');
        NoOfItemsOnDisplay = 5;
        getTotalItems();
    } else if (filename === 'transactions') {
        $('ul.navbar-nav > li:nth-child(3)').addClass('active');
        NoOfItemsOnDisplay = 6;
        getTotalItems();
    } else if (filename === 'points') {
        $('ul.navbar-nav > li:nth-child(4)').addClass('active');
        $("#transferForm").parsley();
        $("#buyPointsForm").parsley();
        NoOfItemsOnDisplay = 6;
        getTotalItems();
    } else if (filename === 'settings') {
        changeSettingsNavLink($('#account-nav'));
        $('#lga').select2();
        $("#dob").datepicker({
            format: "dd/mm/yyyy",
            clearBtn: true,
            orientation: "auto left",
            autoclose: true
        });
        $('#profileForm').parsley();
        $('#image-cropper').cropit();

        var lga = $('#profileForm #lga').data('lga');
//        $("#profileForm #lga option[value="+lga+"]").attr('selected', 'selected');
        $("#profileForm #lga").val(lga).trigger('change');
    } else if (filename === 'contact') {
        $('ul.navbar-nav > li:nth-child(6)').addClass('active');
        $('#contactForm').parsley();

    }

    //****************** Event Handlers *****************//

    $('.logout_').click(function () {
        swal({
            title: "Are you sure?",
            text: 'Clicking "OK" will logout you out of the system',
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function (result) {
            if (result) {
                $.getJSON('./php/suracommands.php?command=logout_user&web', function (data) {

                    if (data === 'SUCCESS') {
                        window.location = "login.php";
                    } else {
                        alert('Internal error occurred');
                    }
                });
            }
        });
    });
    $('body').on('click', '#notif_footer', function () {
        $('.dropdown-menu > li .fa-spinner').show();

        var formData = {
            'notifIDs': $('.dropdown-menu').data('notifids')
        };
        $.post('./php/suracommands.php?command=update_notifications_user&web', formData, function (data) {
            data = JSON.parse(data);

            if (data['info'] === 'SUCCESS') {
                $('.dropdown-menu > li .fa-spinner').hide();
                var notifCount = data['notifCount'];
                if (notifCount === 0) {
                    $('#notifCount').remove();
                } else {
                    $('#notifCount').html(notifCount);
                }
                getNotifications(true);
            } else {
                console.log('Failure');
            }
        });
    });
    $('input:radio[id="pin"]').click(function () {
        $('#cardSection').hide();
        $('#pinSection').fadeIn('slow');
        $('#redeemPinForm').parsley();
    });
    $('input:radio[id="card"]').click(function () {
        $('#pinSection').hide();
        $('#cardSection').fadeIn('slow');
    });
    $('#pointsModal').on('show.bs.modal', function () { // to hide the card section when pointsModal loads
        if ($('#pinCode').length) {
            if (filename === 'points' || filename === 'settings' || filename === 'home') {
                $('input:radio[id="pin"]').prop("checked", true);
                $('input:radio[id="pin"]').trigger('click');
            } else {
                $('input:radio[id="card"]').prop("checked", true);
                $('input:radio[id="card"]').trigger('click');
            }
            $('#pinCode').val('');
            $('#tick-img').css("display", "none");
        }
        $('#infoPoints').empty();
    });
    $('#transferModal').on('show.bs.modal', function () { // to hide the card section when pointsModal loads
        $('input[type=text]').val('');
        $('#info').empty();
    });


    $('body').on('click', '.details-link', function () {
        $('#info').empty();
        $('#logo').attr('src', $(this).data('logo'));
        $('#company').html($(this).data('name'));
        var more_info = $(this).data('more-info');
        var company_desc = $(this).data('company-desc');
        $('#company-more-info').html('More Info:</span> <a target="_blank" href="' + more_info + '">' + more_info + '</a>');

        $('#desc').html(company_desc);
        $('#email').html($(this).data('email'));
        $('#phone').html($(this).data('phone'));
        $('#title').html($(this).data('title'));
        $('#description').html($(this).data('description'));
        $('#pric').html($(this).data('price') + ' iPoints');
        $('#tenure').html($(this).data('tenure') + ' months');
        $('#policy-more-info').html('More info: <a target="_blank" href="' + $(this).data('policy-more-info') + '">' + $(this).data('policy-more-info') + '</a>');
    });
    $('body').on('click', '.insurer', function () {
        var insurerid = $(this).data('insurerid');
//        var policyid = $('#secondaryView').data('policyid');
//        $('#continue').data('policyid', policyid);
        $('#continue').data('insurerid', insurerid);
//        alert(policyid+ ' ' + insurerid);
        $('#confirmPurchaseModal').modal();
    });
    $('body').on('click', '.add-policy', function () { // to set confirm button policy id
        $('#continue').data('policyid', $(this).data('policyid'));
        $('#price').html($(this).data('policyprice'));
        $('#continue').data('policytype', 'CUSTOM');
        $('#confirmPurchaseModal').modal();
    });
    $('body').on('click', '#continue', function () { // add policy prompt

        $('.modal').modal('hide');
        if (filename === 'policydetails') {
            renewPolicy($(this).data('signupid'));
        } else if (filename === 'addnewpolicy') {
//            if ($('#continue').data('policytype') === 'STANDARD') {
//                console.log($('#continue').data('policyid') + ' ' + $('#continue').data('insurerid') + ' ' + $('#continue').data('policytype'));
//                addPolicy($('#continue').data('policyid'), $('#continue').data('insurerid'));
//            } else {
//                console.log($('#continue').data('policyid') + ' ' + $('#continue').data('policytype'));
            addPolicy($(this).data('policyid'), null);
//            }
        }
    });
    $('body').on('click', '__.add-policy', function () { // to set confirm button policy id
        $('#continue').data('policyid', $(this).data('policyid'));
        $('#price').html($(this).data('policyprice'));
//        $('#secondaryView').data('policyid', $(this).data('policyid'));
        if ($(this).data('policytype') === 'standard') {
            $('#info').empty();
            $('#content-header').html('Select Company to apply for Policy');
            $('#content-header').prepend('<a id="policies-back" class="btn btn-success btn-sm" style="border-radius:12px; margin-right:20px;"><i class="fa fa-arrow-left"></i></a>');
            $('#continue').data('policytype', 'STANDARD');
            $('#policiesView').hide();
            $('#pagination').hide();
            $('#secondaryView').empty().show();
            $('#secondaryView').append(spinner.el);
            $('#secondaryView').addClass('overlay');
            var type = $('#policiesView').data('typeid');
            var content = '<div><div class="row">';
            if (type === 1) {
                $.get('php/suracommands.php?command=get_health_insurer_by_user&web',
                        function (data) {
                            data = JSON.parse(data);
                            var id = data['insurer'][0]['id'];
                            var name = data['insurer'][0]['name'];
                            var logo = data['insurer'][0]['logo_url'];

                            content += '<div class="thumbnail insurer" style="padding: 10px" data-insurerid = "' + id + '">';
                            content += '<img src="img/logos/' + logo + '" alt="..." class="img-responsive" style="max-height=100%;max-width=100%;width=auto;height=auto;">';
                            content += '<div class="caption text-center">';
                            content += '<span>' + name + '</span>';
                            content += '</div></div>';
                            content += '</div>';
                            content += '</div>';

                        }).done(function () {
                    $(spinner.el).remove();
                    $('#secondaryView').removeClass('overlay');
                    $('#secondaryView').append(content);
                });
            } else {
                $.get('php/suracommands.php?command=get_insurers_by_type&type=' + type,
                        function (data) {
                            data = JSON.parse(data);
                            for (var i = 0; i < data.length; i++) {
                                if (i % 6 === 0 && i !== 0) {
                                    content += '</div>';
                                    content += '<div class="row">';
                                }
                                var id = data[i]['id'];
                                var name = data[i]['name'];
                                var logo = data[i]['logo_url'];
                                content += '<div class="col-sm-3 col-md-3 col-lg-3">';
                                content += '<div class="thumbnail insurer" style="padding: 10px" data-insurerid = "' + id + '">';
                                content += '<img src="img/insurers/' + logo + '" alt="..." class="img-responsive" style="max-height=100%;max-width=100%;width=auto;height=auto;">';
                                content += '<div class="caption text-center">';
                                content += '<span>' + name + '</span>';
                                content += '</div></div>';
                                content += '</div>';
                                content += '</div>';
                                content += '</div>';
                            }
                        }).done(function () {
                    $(spinner.el).remove();
                    $('#secondaryView').removeClass('overlay');
                    $('#secondaryView').append(content);
                });
            }
        } else {
            $('#continue').data('policytype', 'CUSTOM');
            $('#confirmPurchaseModal').modal();
        }
    });
    $('body').on('click', '__#continue', function () { // add policy prompt

        $('.modal').modal('hide');
        if (filename === 'policydetails') {
            renewPolicy($(this).data('signupid'));
        }
        if (filename === 'addnewpolicy') {
            if ($('#continue').data('policytype') === 'STANDARD') {
                console.log($('#continue').data('policyid') + ' ' + $('#continue').data('insurerid') + ' ' + $('#continue').data('policytype'));
                addPolicy($('#continue').data('policyid'), $('#continue').data('insurerid'));
            } else {
                console.log($('#continue').data('policyid') + ' ' + $('#continue').data('policytype'));
                addPolicy($(this).data('policyid'), null);
            }
        }
    });
    $('body').on('keyup', '#pinCode', (function (e) {
        if ($(this).val().length === 16) {
            $('#tick-img').css("display", "block");
            $('#redeemPinBtn').prop('disabled', false);
        } else {
            $('#tick-img').css("display", "none");
            $('#redeemPinBtn').prop('disabled', true);
        }
    }));
    $('body').on('submit', '#redeemPinForm', (function (event) {
        var formData = {
            'pinCode': $('input[name=pinCode]').val()
        };
        $.post('./php/suracommands.php?command=redeem_pin&web', formData, function (data) {
            data = JSON.parse(data);
            if (data['info'] === 'SUCCESS') {
                $('#infoPoints').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Pin code redeemed succesfully</div>');
                setTimeout(function () {
                    $('#infoPoints').empty();
                }, 5000);
                $('#hlp').html(data['health_points']);
                $('#glp').html(data['general_points']);
                $('.caret-pos').html($('<span class="fa fa-caret-up" style="color: #33ff00"></span>'));
                if (filename === "points") {
                    getTotalItems();
                } else if (filename === "settings") {
                    $('#lp-settings > #points').html(data['points']);
                    $('#lp-settings > #arrow').empty.append('<span class="glyphicon glyphicon-triangle-top" style="color: #33ff00"></span>');
                }
            } else if (data['info'] === 'ALREADY_REDEEMED') {
                $('#infoPoints').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Pin already used! </strong> Try with a different Pin' +
                        '</div>');
            } else if (data['info'] === 'ERROR') {
                $('#infoPoints').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error occured! </strong> Please try again later' +
                        '</div>');
            } else if (data['info'] === 'FAILURE') {
                $('#infoPoints').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Invalid Pin!</strong>' +
                        '</div>');
            }
        }).fail(function (data) {});
        event.preventDefault();
    }));
    $('body').on('submit', '#transferForm', (function (event) {
        var formData = {
            'phone': $('input[name=phone]').val(),
            'amount': $('input[name=amount]').val()
        };
        $.post('./php/suracommands.php?command=transfer_points_user_to_user&web', $(this).serialize(), function (data) {
            data = JSON.parse(data);
            if (data['info'] === 'SUCCESS') {
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Points transferred succesfully</div>');
                setTimeout(function () {
                    $('#info').empty();
                }, 5000);
                data['wallet'] === 'health' ? $('#hlp').html(data['points']) : $('#glp').html(data['points']);
                $('#caret').html('<span class="glyphicon glyphicon-triangle-bottom" style="color: #ff0000"></span>');
            } else if (data['info'] === 'INSUFFICIENT_POINTS') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Points Tranfer Failed!</strong> due to insufficient iPoints' +
                        '</div>');
            } else if (data['info'] === 'ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error occured! </strong> Please try again later' +
                        '</div>');
            } else if (data['info'] === 'NUMBER_NOT_RESOLVED') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>This user doesn\'t exist</strong>' +
                        '</div>');
            } else if (data['info'] === 'SELF_TRANSFER') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Invalid transfer operation</strong>' +
                        '</div>');
            } else if (data['info'] === 'INVALID_AMOUNT') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Invalid amount!</strong>. Enter amount less than ' + $('#lp').html() +
                        '</div>');
            } else if (data['info'] === 'NO_BASIC_POLICY') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'You cannot make a transfer without having a basic policy from UICI</div>');
            }

        }).fail(function (data) {});
        event.preventDefault();
    }));
    $('body').on('submit', '#profileForm', (function (event) {
        $('#info').empty();
        var formData = {
            'fname': $('input[name=fname]').val(),
            'lname': $('input[name=lname]').val(),
            'dob': $('input[name=dob]').val(),
            'gender': $("#gender option:selected").val(),
            'email': $('input[name=email]').val(),
            'phone': $('input[name=phone]').val(),
            'address': $('input[name=address]').val(),
            'local_gov': $('select[name=lga]').val(),
            'kin_name': $('input[name=kin_name]').val(),
            'kin_rel': $('input[name=kin_rel]').val(),
            'kin_phone': $('input[name=kin_phone]').val()
        };
        $.post('./php/suracommands.php?command=update_user_profile&web', formData, function (data) {
            data = JSON.parse(data);
            if (data['info'] === 'SUCCESS') {
                var fname = $('input[name=fname]').val();
                var lname = $('input[name=lname]').val();
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Update Successful</div>');
                $("#name").html(fname + ' ' + lname);
                $("html, body").animate({scrollTop: 0}, "fast");
            } else if (data['info'] === 'VALIDATION_ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Fill in appropiate values for all fields'
                        + '</div>');
                $("html, body, #left").animate({scrollTop: 0}, "fast");
            } else if (data['info'] === 'NO_EDIT') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Update Failed!</strong> Kindly edit a field and try again' +
                        '</div>');
                $("html, body").animate({scrollTop: 0}, "fast");
            } else if (data['info'] === 'DUPLICATE_ENTRY') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Email / Phone number already exists' +
                        '</div>');
                $("html, body").animate({scrollTop: 0}, "fast");
            } else if (data['info'] === 'FAILURE') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occured!</strong> Try again later' +
                        '</div>');
                $("html, body").animate({scrollTop: 0}, "fast");
            }

            setTimeout(function () {
                $('#info').fadeOut().empty().fadeIn();
            }, 4000);
        }).fail(function (data) {});
        event.preventDefault();
    }));
    $('body').on('submit', '#resetPasswordForm', (function (event) {
        $('#info').empty();
        var formData = {
            'oldPassword': $('input[name=oldPassword]').val(),
            'newPassword': $('input[name=newPassword]').val()
        };
        $.post('./php/suracommands.php?command=update_user_password&web', formData, function (data) {
            data = JSON.parse(data);
            if (data['info'] === 'SUCCESS') {
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Update Successful</div>');
                $('input[name=oldPassword]').val('');
                $('input[name=newPassword]').val('');
                $('input[name=confirmPassword]').val('');
            } else if (data['info'] === 'FAILURE') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Update Failed!</strong> Please try again later' +
                        '</div>');
            } else if (data['info'] === 'INCORRECT_PASSWORD') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Incorrect Password! </strong> Enter correct password or reset password' +
                        '</div>');
                $('input[name=oldPassword]').val('');
                $('input[name=newPassword]').val('');
                $('input[name=confirmPassword]').val('');
            } else if (data['info'] === 'NO_CHANGES') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<strong>Update Failed! </strong> Kindly enter a different password' +
                        '</div>');
                $('input[name=newPassword]').val('');
//                $('input[name=newPassword]').addClass('input.parsley-error');
                $('input[name=confirmPassword]').val('');
//                $('input[name=confirmPassword]').addClass('input.parsley-error');
            } else if (data['info'] === 'VALIDATION_ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Fill in all fields with appropiate values' +
                        '</div>');
            } else if (data['info'] === 'ERORR_OCCURRED') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>ERORR_OCCURRED!</strong> Please try again later' +
                        '</div>');
            }

            $("html, body").animate({scrollTop: 0}, "fast");
        }).fail(function (data) {});

        event.preventDefault();
    }));
    $('body').on('submit', '#resetSQForm', (function (event) {
        $('#info').empty();
        var formData = {
            'question': $('#question').val(),
            'answer': $('input[name=answer]').val()
        };
        $.post('./php/suracommands.php?command=update_user_security_question&web', formData, function (data) {
            data = JSON.parse(data);
            if (data['info'] === 'SUCCESS') {
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Update Successful</div>');
                $("html, body").animate({scrollTop: 0}, "fast");
            } else if (data['info'] === 'FAILURE') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Update Failed!</strong> Please try again with different entry' +
                        '</div>');
                $("html, body").animate({scrollTop: 0}, "fast");
            } else if (data['info'] === 'DATABASE_ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Update Failed!</strong> Please try again with different entry' +
                        '</div>');
                $("html, body").animate({scrollTop: 0}, "fast");
            } else if (data['info'] === 'VALIDATION_ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Fill all fields with appropiate values' +
                        '</div>');
                $("html, body").animate({scrollTop: 0}, "fast");
            } else if (data['info'] === 'ERORR_OCCURRED') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<strong>ERORR_OCCURRED!</strong> Please try again later' +
                        '</div>');
            }

            $("html, body").animate({scrollTop: 0}, "fast");
        }).fail(function (data) {});
        event.preventDefault();
    }));
    $('body').on('submit', '#profilePicForm', function (event) {
        $('#info').empty();

        $.ajax({
            type: "POST",
            url: "php/suracommands.php?command=upload_profile_pic_user&web",
            data: new FormData(document.getElementById('profilePicForm')),
            dataType: 'JSON',
            /*** Options to tell JQuery not to process data or worry about content-type ****/
            cache: false,
            contentType: false,
            processData: false,
            /****************************************/
            success: function (data) {
                if (data['info'] === 'SUCCESS') {
                    $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            'Update Successful</div>');
                    $('#profile-pic').attr('src', 'img/users/' + data.path + '?' + Date.now());
                    $('#header-pic').attr('src', 'img/users/' + data.path + '?' + Date.now());
                } else if (data['info'] === 'FAILURE') {
                    $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Update Failed!</strong> Please try again with different entry' +
                            '</div>');
                } else if (data['info'] === 'VALIDATION_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>' + data['errVal']['message'] + '</strong>' +
                            '</div>');
                    $('#' + data['errVal']['field']).removeClass('parsley-success');
                    $('#' + data['errVal']['field']).addClass('parsley-error');
                } else if (data['info'] === 'UPLOAD_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>File upload failed</strong>. Try again later' +
                            '</div>');
                } else if (data['info'] === 'FILE_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Please upload a valid file</strong>' +
                            '</div>');
                } else if (data['info'] === 'ERORR_OCCURRED') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>ERORR_OCCURRED!</strong> Please try again later' +
                            '</div>');
                }
                $("html, body").animate({scrollTop: 0}, "fast");
            }
        });

        event.preventDefault();
    });
    $('body').on('submit', '__#pensionTopupForm', (function (event) {
        var formData = {
            'amount': $('input[name=amount]').val()
        };
        $.post('./php/suracommands.php?command=topup_pension&web', formData, function (data) {
            data = JSON.parse(data);
            if (data['info'] === 'SUCCESS') {
                $('#infoTopup').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Pension Top-up succesful</div>').slideDown().delay(3000).slideUp();

                $('#lp').html(data['points']);
                $('#dp').html(data['dedicated_points']);
                $('#pp').html(data['pension_points']);
                $('#caret').html('<span class="glyphicon glyphicon-triangle-bottom" style="color: #ff0000"></span>');
                $('#lp-settings > #points').html(data['points']);
                $('#lp-settings > #arrow').html('<span class="glyphicon glyphicon-triangle-bottom" style="color: #ff0000"></span>');
            } else if (data['info'] === 'ERROR') {
                $('#infoTopup').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error occured! </strong> Please try again later' +
                        '</div>');
            } else if (data['info'] === 'FAILURE') {
                $('#infoTopup').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Invalid Pin!</strong>' +
                        '</div>');
            }
        }).fail(function (data) {});
        event.preventDefault();
    }));
    $('body').on('click', '.request-payment', function () {
        var referralid = $(this).data('referralid');
        $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                '<strong>Ooops!</strong> Still in consideration' +
                '</div>');

//        $.getJSON('php/suracommands.php?command=add_pension_signup&id=' + pensionerID + '&web', function (data) {
//            
//        });
    });


    /* $('body').on('submit', '#contactForm', function (event) {
     $('#info').empty();
     $('form i.fa').removeClass('fa-send-o');
     $('form i.fa').addClass('fa-spinner');
     $('form i.fa').addClass('fa-pulse');
     $('form button span').html('On the FLY...');
     var formData = {
     'type': $("#type option:selected").text(),
     'message': $('textarea[name=message]').val(),
     'entity': 'USER'
     };
     $.post('./php/suracommands.php?command=send_message&web', formData, function (data) {
     data = JSON.parse(data);
     $('form i.fa').addClass('fa-send-o');
     $('form i.fa').removeClass('fa-spinner');
     $('form i.fa').removeClass('fa-pulse');
     $('form button span').html('SEND');
     if (data['info'] === 'SUCCESS') {
     $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
     '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
     'Message sent succesfully</div>');
     setTimeout(function () {
     $('#info').empty();
     }, 5000);
     $('textarea[name=message]').val('');
     } else if (data['info'] === 'ERROR_OCCURRED') {
     $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
     '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
     '<strong>Error occured! </strong> Please try again later' +
     '</div>');
     } else if (data['info'] === 'VALIDATION_ERROR') {
     $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
     '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
     '<strong>Message more than 1000 characters</strong>' +
     '</div>');
     }
     
     }).fail(function (data) {});
     event.preventDefault();
     }); */
    $("#dp-upload").click(function () {
        $('#info').empty();
        $('#profile').hide();
        $('#sub-content').empty().load('_upload_profile_pic.php', function () {
            $('#profilePicForm').parsley();
        });
        $("html, body").animate({scrollTop: 0}, "fast");
    });
    $("#account-nav").click(function () {
        $('#info').empty();
        changeSettingsNavLink($(this));
        $('#sub-content').hide();
        $('#profile').show();
    });

    $("#pension-nav").click(function () {
        $('#info').empty();
        $('#profile').hide();
        $('#sub-content').append(spinner.el);
        $('#sub-content').addClass('overlay');
        changeSettingsNavLink($(this));
        var content = '';
        $.get('php/suracommands.php?command=get_my_pension&web', function (data) {
            data = JSON.parse(data);
            if (data.length > 0) {
                $('#subcontent-header').html('Pension Manager');
                var pensionerInfo = data[0]['pension_manager'][0];
                var name = pensionerInfo['name'];
                var desc = pensionerInfo['write_up'];
                var link = pensionerInfo['more_info_link'];
                var logo = pensionerInfo['logo_url'];
                var phone = pensionerInfo['phone'];
                var email = pensionerInfo['email'];

                content += '<img class="img-responsive center-block" width="200" height="200" src="img/pensions/' + logo + '" /><br/><br/>';
                content += '<strong>Name:</strong> ' + name + '<br>';
                content += '<strong>Description:</strong> ' + desc + ' <a href="' + link + '" target="_blank" class="small">' + link + '</a><br>';
                content += '<strong>Phone:</strong> ' + phone + '<br>';
                content += '<strong>E-mail:</strong> ' + email + '<br><br>';
                content += '<button id="topup-pension" class="btn btn-success" data-toggle = "modal" data-target = "#topupModal"> Top-up Pension</button>';
            } else {
                $('#subcontent-header').html('Choose a pension manager');
                $.get('php/suracommands.php?command=get_all_pension_managers', function (data) {
                    data = JSON.parse(data);
                    if (data.length > 0) {
                        content += '<div class="row">';
                        for (var i = 0; i < data.length; i++) {
                            if (i % 4 === 0 && i !== 0) {
                                content += '</div>';
                                content += '<div class="row">';
                            }
                            var pensionerInfo = data[i];
                            var pensionerID = pensionerInfo['id'];
                            var name = pensionerInfo['name'];
                            var logo = pensionerInfo['logo_url'];
                            content += '<div class="col-sm-3 col-md-3 col-lg-3 pensioner" data-id="' + pensionerID + '" style="cursor:pointer">';
                            content += '<div class="thumbnail" data-id="' + pensionerID + '">';
                            content += '<img src="img/logos/' + logo + '" alt="..." class="img-responsive" data-id="' + pensionerID + '">';
                            content += '<div class="caption text-center" data-id="' + pensionerID + '">';
                            content += '<span data-id="' + pensionerID + '">' + name + '</span>';
                            content += '</div></div>';
                            content += '</div>';
                        }
                    }
                }).done(function () {
                    $(spinner.el).remove();
                    $('#sub-content').removeClass('overlay');
                    $('#sub-content > #subcontent-body').html(content);
                });
            }
        }).done(function () {
            $(spinner.el).remove();
            $('#sub-content').removeClass('overlay');
            $('#sub-content > #subcontent-body').html(content);
        });
    });
    $('body').on('click', '.pensioner', function (e) {
        var pensionerID = $(this).data('id');
        $.get('php/suracommands.php?command=get_pensioner_details_by_id&id=' + pensionerID, function (data) {
            data = JSON.parse(data);

            data = data[0];

            $('#detailsModal .modal-body #logo').attr('src', 'img/pensions/' + data['logo_url']);
            $('#detailsModal .modal-body #name').html(data['name']);
            $('#detailsModal .modal-body #email').html(data['email']);
            $('#detailsModal .modal-body #phone').html(data['phone']);
            $('#detailsModal .modal-body #desc').html(data['write_up'] + ' '
                    + '<a class="small" href="' + data['more_info_link'] + '" target="_blank">' + data['more_info_link'] + '</a>');
            $('#add-manager').data('id', pensionerID);
        }).done(function () {
            $('#detailsModal').modal();
        });
    });
    $('body').on('click', '#add-manager', function (e) {
        var pensionerID = $(this).data('id');
        $.get('php/suracommands.php?command=add_pension_signup&id=' + pensionerID + '&web', function (data) {
            data = JSON.parse(data);
            $('#detailsModal').toggle();
            $(".modal-backdrop").remove();
        }).done(function () {
            $('#subcontent-info').html('<div class="alert alert-dismissible alert-success text-center">' +
                    'Pension Manager added successfully</div>').slideDown().delay(3000).slideUp();
            $('#pension-nav').trigger('click');
        });
    });

    $("#referrals-nav").click(function () {
        changeSettingsNavLink($(this));
        $('#info').empty();
        $('#profile').hide();
        $('#sub-content').show().append(spinner.el);
        $('#sub-content').addClass('overlay');
        $('#subcontent-header').html('Referrals');
        $('#subcontent-body').html(`<div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#Ref. ID</th>
                                <th>Merchant Name</th>
                                <th>Date Referred</th>
                                <th>Paid</th>
                                <th>Date Paid</th>
                                <th>Invoice Number</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>`);

        var content = '';
        $.getJSON('php/suracommands.php?command=get_my_referrals&entity=user&web', function (data) {

            for (var i = 0; i < data.length; i++) {
                content += '<tr>' +
                        '<td>' + data[i]['id'] + '</td>' +
                        '<td>' + data[i]['merchant_name'] + '</td>' +
                        '<td>' + moment(data[i]['date_referred']).format('MMM Do YYYY, h:mm A') + '</td>' +
                        '<td>' + data[i]['paid'] + '</td>';

                if (data[i]['paid'] === 'FALSE') {
                    content += '<td>-</td>' +
                            '<td>-</td>' +
                            '<td><button class="btn btn-xs btn-success request-payment" data-referralid="' + data[i]['id'] + '">Request Payment</button></td>';
                } else {
                    content += '<td>' + moment(data[i]['date_paid']).format('MMM Do YYYY, h:mm A') + '</td>' +
                            '<td>' + data[i]['invoice_no'] + '</td><td></td>';
                }

                content += '</tr>';
            }
        }).done(function () {
            $(spinner.el).remove();
            $('#sub-content').removeClass('overlay');
            $('#sub-content > #subcontent-body tbody').html(content);
        });
    });
    $("#faq-nav").click(function () {
        $('#info').empty();
        $('#sub-content').show();
        $('#subcontent-body').empty();
        changeSettingsNavLink($(this));
        $('#profile').hide();
        $('#subcontent-header').empty().append('Coming Soon!!!');
    });
    $("#help-nav").click(function () {
        $('#info').empty();
        $('#sub-content').show();
        $('#subcontent-body').empty();
        changeSettingsNavLink($(this));
        $('#profile').hide();
        $('#subcontent-header').empty().append('Coming Soon!!!');
    });
    $("#password-reset").click(function () {
        var content = '<div class="reset" style="padding-left: 60px;">' +
                '<form id="resetPasswordForm">' +
                '<div class="row">' +
                '<div class="col-sm-2"></div>' +
                '<div class="col-sm-7">' +
                '<div class="row">' +
                '<label class="col-sm-4" for="oldPassword"><span class="pull-right">Old Password</span> </label>' +
                '<input class="col-sm-8" id="oldPassword" name="oldPassword" type="password" data-parsley-required />' +
                '</div>' +
                '<div class="row">' +
                '<label class="col-sm-4" for="newPassword"><span class="pull-right">New Password</span> </label>' +
                '<input class="col-sm-8" id="newPassword" name="newPassword" type="password" ' +
                'data-parsley-required data-parsley-minlength="4"/>' +
                '</div>' +
                '<div class="row">' +
                '<label class="col-sm-4" for="confirmPassword"><span class="pull-right">Confirm Password</span> </label>' +
                '<input class="col-sm-8" id="confirmPassword" name="confirmPassword" type="password"' +
                'data-parsley-required data-parsley-equalto="#newPassword" data-parsley-equalto-message="Password mismatch" />' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-sm-2">' +
                '<button id="settings-back" type="button" class="btn btn-success btn-rounded settings-btn box-shadow--2dp btn-lg" name="back"><i class="fa fa-arrow-left"></i></button>' +
                '</div>' +
                '<div class="col-sm-8"></div>' +
                '<div class="col-sm-2">' +
                '<button id="reset_password_save" type="submit" class="btn btn-success btn-rounded settings-btn box-shadow--2dp" name="save"><strong>SAVE</strong></button>' +
                '</div>' +
                '</div>' +
                '</form>' +
                '</div>';
        $('#profile').hide();
        $('#sub-content').empty().html(content);
        $('#resetPasswordForm').parsley();
    });
    $("#sq-reset").click(function () {
        var content = '<div class="reset" style="padding-left: 60px;">' +
                '<form id="resetSQForm">' +
                '<div class="row">' +
                '<div class="col-sm-2"></div>' +
                '<div class="col-sm-7">' +
                '<div class="row">' +
                '<label class="col-sm-4" for="question"><span class="pull-right">Security Question</span> </label>' +
                '<select class="col-sm-8" id="question" name="question"  data-parsley-required>' +
                '<option disabled selected>Select security question</option>';
        $.get('php/suracommands.php?command=get_security_questions', function (data) {
            data = JSON.parse(data);
            for (var i = 0; i < data.length; i++)
                content += "<option value = '" + data[i]['question'] + "'>" + data[i]['question'] + '</option>';
            content += '</select>' +
                    '</div>' +
                    '<div class="row">' +
                    '<label class="col-sm-4" for="answer"><span class="pull-right">Security Answer</span> </label>' +
                    '<input class="col-sm-8" id="answer" name="answer" type="text" data-parsley-required/>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="row">' +
                    '<div class="col-sm-2">' +
                    '<button id="settings-back" type="button" class="btn btn-success btn-rounded settings-btn box-shadow--2dp btn-lg" name="back"><i class="fa fa-arrow-left"></i></button>' +
                    '</div>' +
                    '<div class="col-sm-8"></div>' +
                    '<div class="col-sm-2">' +
                    '<button id="reset_password_save" type="submit" class="btn btn-success btn-rounded settings-btn box-shadow--2dp" name="save"><strong>SAVE</strong></button>' +
                    '</div>' +
                    '</div>' +
                    '</form>' +
                    '</div>';
            $('#profile').hide();
            $('#sub-content').empty().html(content);
            $('#resetSQForm').parsley();
        });
    });
    $('body').on('click', '#settings-back', function () {
        $('#info').empty();
        $('#sub-content').empty();
        $('#profile').show();
    });
    $('body').on('click', '#load-new', function () {
        if (isItemRemaining === true) {
            offset += limit;
            getRecentActivity(limit, offset);
            isEmpty = false;
        }
    });
    $('body').on('click', '#policies-back', function () {
        $(this).remove();
        $('#secondaryView').empty();
        $('#policiesView').fadeIn('slow');
        $('#pagination').fadeIn('slow');
        $('#content-header').html('Select Policy');
    });

    //********** Miscellaneous ***********************//

    function changeSettingsNavLink(e) {
        e.css("background-color", "#72be58");
        e.css("color", "white");
        e.find("img").attr("src", "img/icons/left_arrow_white.png");
        e.hover(function () {
            e.css("background-color", "#72be58");
        },
                function () {
                    e.css("background-color", "#72be58");
                });
        var temp = e.index();
        $('#settings-nav p').each(function (i) {
            if (i !== temp) {
                $(this).css("background-color", "white");
                $(this).css("color", "black");
                $(this).find("img").attr("src", "img/icons/left_arrow_grey.png");
                $(this).hover(function () {
                    $(this).css("background-color", "#dadada");
                },
                        function () {
                            $(this).css("background-color", "white");
                        });
            }
        });
    }

    function changePolicyLinks(e) {
        e.find('span').css("background", "#003366");
        $('#policy-link-icon').children('div').each(function (i) {
            if (i !== e.index()) {
                $(this).find('span').css("background", "#419641");
            }
        });
    }

    ////////////////////////////////////////////////////

    function toMonth($num, $type) {
        switch ($num) {
            case '1':
                return $type === 'long' ? 'January' : 'Jan';
            case '2':
                return $type === 'long' ? 'February' : 'Feb';
            case '3':
                return $type === 'long' ? 'March' : 'Mar';
            case '4':
                return $type === 'long' ? 'April' : 'Apr';
            case '5':
                return $type === 'long' ? 'May' : 'May';
            case '6':
                return $type === 'long' ? 'June' : 'Jun';
            case '7':
                return $type === 'long' ? 'July' : 'Jul';
            case '8':
                return $type === 'long' ? 'August' : 'Aug';
            case '9':
                return $type === 'long' ? 'September' : 'Sep';
            case '10':
                return $type === 'long' ? 'October' : 'Oct';
            case '11':
                return $type === 'long' ? 'November' : 'Nov';
            case '12':
                return $type === 'long' ? 'December' : 'Dec';
            default:
                return "Incorrect Month Value";
        }
    }

    function getNotifications(flag) {
        var content = '';
        var notifIDs = '';
        if (flag === true) {
            $.get('php/suracommands.php?command=get_notifications_user&limit=10&web', function (data) {
                var notifs = JSON.parse(data);
                var notifCount = notifs.length;

                if (notifCount > 0) {
                    for (var i = 0; i < notifCount; i++) {
                        i === 0 ? '' : notifIDs += ',';
                        notifIDs += notifs[i]['id'];
                        content += '<li class="divider notif-temp"></li>';
                        content += '<li class="text-center notif-temp">' + notifs[i]['message'] + '</li>';
                    }

                    content += '<li class="divider notif-temp" id="last_divider"></li>';
                    content += '<li id="notif_footer" class="text-center center-block notif-temp">Mark as Read</li>';
                } else {
                    content += '<li class="divider"> </li>';
                    content += '<li id="no_new_notif" class="text-center">No new notifications</li>';
                }

            }).done(function () {
                $('.dropdown-menu > li .fa-spinner').hide();
                $('.notif-temp').remove();
                $('.dropdown-menu').append(content);
                $('.dropdown-menu').data('notifids', notifIDs);
                $('.dropdown-menu #notif_header').css('padding-bottom', '0');
                $('.dropdown-menu .divider:nth-child(2)').css('margin-top', '5px');
            });
        } else {
            content += '<li class="divider"> </li>';
            content += '<li id="no_new_notif" class="text-center">No new notifications</li>';
            $('.dropdown-menu > li .fa-spinner').hide();
            $('.dropdown-menu').append(content);
            $('.dropdown-menu #notif_header').css('padding-bottom', '0');
            $('.dropdown-menu .divider:nth-child(2)').css('margin-top', '5px');
        }

    }

    function getRecentActivity(limit, offset) {
        var content = '';

        $('#south-content > div').append(spinner.el);
        $('#south-content > div').addClass('overlay');
        $.get('php/suracommands.php?command=get_recent_activity_user&limit=' + limit + '&offset=' + offset + '&web', function (data) {
            var activityInfo = JSON.parse(data);
            var newDateDay = null;
            var newDateMonth = null;
            var newDateYear = null;

            if (activityInfo.length > 0) {

                isItemRemaining = true;

                for (var i = 0; i < activityInfo.length; i++) {
                    var ref = activityInfo[i]['ref'] !== null ? activityInfo[i]['ref'][0] : '';
                    var _date = new Date(activityInfo[i]['date']);
                    var dateDay = _date.getDate().toString();
                    var dateMonth = toMonth((_date.getMonth() + 1).toString());
                    var dateYear = _date.getFullYear().toString();
                    var action = activityInfo[i]['action'];
                    var iconColor = null;
                    var heading = null;
                    var info = null; // used for setting the content to be display for various transaction activities..
                    if (action === 'BUY_POLICY') {
                        heading = 'POLICY PURCHASE';
                        iconColor = 'bg-warning';
                        info = 'You signed up for an Insurance package titled ' + ref['title'] + ' worth ' + ref['price'] + ' iPoints';
                    } else if (action === 'RENEW_POLICY') {
                        heading = 'POLICY RENEWAL';
                        iconColor = 'bg-warning';
                        info = 'You renewed an Insurance package titled ' + ref['title'] + ' worth ' + ref['price'] + ' iPoints';
                    } else if (action === 'LOAD_PIN') {
                        heading = 'PIN LOAD';
                        iconColor = 'bg-success';
                        info = 'You redeemed a pin from ' + ref['merchant'] + ' worth ' + ref['point_worth'] + ' iPoints';
                    } else if (action === 'TRANSFER_POINTS') {
                        heading = 'POINTS TRANSFER';
                        iconColor = 'bg-secondary';
                        info = 'You transferred ' + ref['point_worth'] + ' iPoints to ' + ref['user'];
                    } else if (action === 'RECEIVED_POINTS_USER') {
                        heading = 'RECEIVED POINTS FROM USER';
                        iconColor = 'bg-success';
                        info = 'You received ' + ref['point_worth'] + ' iPoints from ' + ref['user'];
                    } else if (action === 'RECEIVED_POINTS_AGENT') {
                        heading = 'RECEIVED POINTS FROM AGENT';
                        iconColor = 'bg-success';
                        info = 'You received ' + ref['point_worth'] + ' iPoints from ' + ref['agent'];
                    } else if (action === 'EDIT_PROFILE') {
                        heading = 'PROFILE EDIT';
                        iconColor = 'bg-info';
                        info = 'You edited your profile information';
                    } else if (action === 'CHANGE_PASSWORD') {
                        heading = 'PASSWORD CHANGE';
                        iconColor = 'bg-info';
                        info = 'You changed your password details';
                    } else if (action === 'CHANGE_SEC_QUE') {
                        heading = 'SECURITY QUESTION CHANGE';
                        iconColor = 'bg-info';
                        info = 'You edited your security question information';
                    } else if (action === 'CHANGE_PROFILE_PIC') {
                        heading = 'PROFILE PICTURE CHANGE';
                        iconColor = 'bg-info';
                        info = 'You changed your profile picture';
                    } else if (action === 'SEND_MESSAGE') {
                        heading = 'MESSAGE SENT';
                        iconColor = 'bg-info';
                        info = 'You sent a message to the administrator. Awaiting response...';
                    } else if (action === 'PAYMENT_OVERDUE') {
                        heading = 'PAYMENT OVERDUE';
                        iconColor = 'bg-warning';
                        info = 'Your insurance policy will be suspended due a default in payment. Pay up to renew your policy';
                    } else if (action === 'MAKE_PAYMENT') {
                        heading = 'MAKE PAYMENT';
                        iconColor = 'bg-warning';
                        info = 'A payment of ' + ref['point_worth'] + ' iPoints was made to ' + ref['store'];
                    } else if (action === 'PENSION_SIGNUP') {
                        heading = 'PENSION SIGNUP';
                        iconColor = 'bg-success';
                        info = "You signed up with a pension manager named " + ref['name'];
                    } else if (action === 'PENSION_TOPUP') {
                        heading = 'PENSION TOPUP';
                        iconColor = 'bg-secondary';
                        info = "You topped up your pension by " + ref['point_worth'] + ' iPoints';
                    } else if (action === 'ADMIN_DISAPPROVE') {
                        heading = 'ADMIN BAN';
                        iconColor = 'bg-danger';
                        info = "You were banned from accessing the platform by UICI administrator";
                    } else if (action === 'ADMIN_APPROVE') {
                        heading = 'ADMIN UNBAN';
                        iconColor = 'bg-info';
                        info = "You were un-banned from accessing the platform by UICI administrator";
                    } else if (action === 'ADMIN_EDIT') {
                        heading = 'ADMIN EDIT';
                        iconColor = 'bg-info';
                        info = "Your profile was edited by UICI administrator";
                    } else if (action === 'RECEIVED_POINTS_MERCHANT') {
                        heading = 'RECEIVED POINTS FROM MERCHANT';
                        iconColor = 'bg-success';
                        info = 'You received ' + ref['point_worth'] + ' iPoints from ' + ref['merchant'];
                    } else if (action === 'BUY_POINTS') {
                        heading = 'iPOINT PURCHASE';
                        iconColor = 'bg-success';
                        info = 'You bought iPoints worth ' + ref['point_worth'] + ' iPoints';
                    } else if (action === 'BASIC_POLICY_SUB') {
                        heading = 'BASIC POLICY SUBSCRIBTION';
                        iconColor = 'bg-success';
                        info = 'You have been marked for subscribtion to the basic insurance package. Your account will be updated in a few days';
                    }

                    if (dateDay !== newDateDay || dateMonth !== newDateMonth || dateYear !== newDateYear) {
                        if (i !== 0) {
                            content += '</div>' +
                                    '</div>' +
                                    '</div>';
                        }
                        newDateDay = dateDay;
                        newDateMonth = dateMonth;
                        newDateYear = dateYear;
                        content += '<div class="row">'
                                + '<div class="col-xs-1">' +
                                '<div id="date" class="text-center pull-right">' +
                                '<span id="month">' + dateYear + '</span>' +
                                '<br>' +
                                '<span id="day">' + dateMonth + ' ' + dateDay + '</span>' +
                                '</div>' +
                                '</div>' +
                                '<div class="col-xs-11">' +
                                '<div class="timeline-centered">';
                    }
                    content += '<article class="timeline-entry">' +
                            '<div class="timeline-entry-inner">' +
                            '<div class="timeline-icon ' + iconColor + '"></div>' +
                            '<div class="timeline-label ">' +
                            '<h2 style="font-weight:500; font-weight:bold;">' + heading + '</h2>' +
                            '<p>' + info + '</p>' +
                            '</div>' +
                            '</div>' +
                            '</article>';
                }

                content += '<article id="plus" class="timeline-entry begin">' +
                        '<div class="timeline-entry-inner">' +
                        '<div id="load-new" class="timeline-icon" style="-webkit-transform: rotate(-90deg); -moz-transform: rotate(-90deg); cursor:pointer; ">' +
                        '<i class="fa fa-plus"></i>' +
                        '</div>' +
                        ' </div>' +
                        '</article>';
                content += '</div>' +
                        '</div>' +
                        '</div>';
            } else {
                isItemRemaining = false;
                if (isEmpty) {
                    content = '<p style="padding: 50px;" class="text-center text-muted text-italic">No Recent Activity</p>';
                }
            }
        }).done(function () {
            $(spinner.el).remove();
            $('#south-content > div').removeClass('overlay');
            if (isItemRemaining || isEmpty) {
                $('#south-content > div').empty().append(content);
            }
        });
    }

    function getTransactions(limit, offset) {

        var content = '';
        $('#content').append(spinner.el);
        $('#content').addClass('overlay');
        $.get('php/suracommands.php?command=get_my_transactions&limit=' + limit + '&offset=' + offset + '&web', function (data) {
            var transactionInfo = JSON.parse(data);
            if (transactionInfo.length > 0) {
                isItemRemaining = true;
                if ($('#table').is(":hidden")) {
                    $('#no-transactions').remove();
                    $('#table').show();
                }
                for (var i = 0; i < transactionInfo.length; i++) {
                    var trans_type = transactionInfo[i]['trans_type'];
                    var img = null;
                    var balance = null;
                    if (trans_type === 'credit') {
                        img = 'text-success';
                        balance = transactionInfo[i]['previous_balance_to'];
                    } else {
                        img = 'text-danger';
                        balance = transactionInfo[i]['previous_balance_from'];
                    }

                    var ref = transactionInfo[i]['ref'];
                    var points = transactionInfo[i]['point_worth'];
                    var _date = new Date(transactionInfo[i]['date_transacted']);
                    var date = _date.getDate() + ' - ' + toMonth((_date.getMonth() + 1).toString()) + ' - ' + _date.getFullYear();
                    content += '<tr style="font-weight:bold; ">';
                    content += '<td class="text-center">' + date + '</td>';
                    content += '<td class="text-center">' + ref + '</td>';
                    content += '<td class="text-center' + ' ' + img + '">' + points + '</td>';
                    content += '<td class="text-center">' + balance + '</td>';
                    content += '</tr>';
                }
            } else {
                content += '<p id="no-transactions" style="padding: 50px;" class="text-center text-muted text-italic">No Transactions made</p>';
                isItemRemaining = false;
            }
        }).done(function () {
            if ($('#table').is(":visible")) {
                if (isItemRemaining === true) {
                    $(spinner.el).remove();
                    $('#content').removeClass('overlay');
                    $('tbody').html(content);
                }
            } else {
                $(spinner.el).remove();
                $('#content').removeClass('overlay');
                $('#content').append(content);
            }
        });
    }

    function getPoints(limit, offset) {
        var content = '';
        $('#content').append(spinner.el);
        $('#content').addClass('overlay');
        $.get('php/suracommands.php?command=get_redeemed_points&limit=' + limit + '&offset=' + offset + '&web', function (data) {
            var pointsInfo = JSON.parse(data);
            if (pointsInfo.length > 0) {
                isItemRemaining = true;
                if ($('#table').is(":hidden")) {
                    $('#no-points').remove();
                    $('#table').show();
                }
                for (var i = 0; i < pointsInfo.length; i++) {
                    var _date = new Date(pointsInfo[i]['redeemed_date']);
                    var date = _date.getDate() + ' - ' + toMonth((_date.getMonth() + 1).toString()) + ' - ' + _date.getFullYear();
                    var merchant = pointsInfo[i]['merchant'];
                    var points = pointsInfo[i]['ussd_code_id'][0]['point_worth'];
                    content += '<tr style="font-weight:bold;">';
                    content += '<td class="text-center">' + date + '</td>';
                    content += '<td class="text-center">' + merchant + '</td>';
                    content += '<td class="text-center">' + points + '</td>';
                    content += '</tr>';
                }
            } else {
                content += '<p id="no-points" style="padding: 50px;" class="text-center text-muted text-italic">No Points Redeemed</p>';
                isItemRemaining = false;
            }
        }).done(function () {
            if ($('#table').is(":visible")) {
                if (isItemRemaining === true) {
                    $(spinner.el).remove();
                    $('#content').removeClass('overlay');
                    $('tbody').html(content);
                }
            } else {
                $(spinner.el).remove();
                $('#content').removeClass('overlay');
                $('#content').append(content);
            }
        });
    }

    function getPayments(limit, offset, signupID) {
        var content = '';
        $('#content').append(spinner.el);
        $('#content').addClass('overlay');
        $.get('php/suracommands.php?command=get_my_payments&signupid=' + signupID + '&limit=' + limit + '&offset=' + offset + '&web', function (data) {
            var payment = JSON.parse(data);
            if (payment.length > 0) {
                isItemRemaining = true;
                if ($('#table').is(":hidden")) {
                    $('#no-payments').remove();
                    $('#table').show();
                }
                for (var i = 0; i < payment.length; i++) {
                    var _date = new Date(payment[i]['date_to_charge']);
                    var date = _date.getDate() + ' - ' + toMonth((_date.getMonth() + 1).toString()) + ' - ' + _date.getFullYear();
                    var amount = payment[i]['amount'];
                    var ref = payment[i]['ref'];
                    content += '<tr>';
                    content += '<td class="text-center">' + date + '</td>';
                    content += '<td class="text-center">' + amount + ' iPoints</td>';
                    content += '<td class="text-center">' + ref + '</td>';
                    content += '</tr>';
                }
            } else {
                content += '<p id="no-payments" style="padding: 50px;" class="text-center text-muted text-italic">No Renewals made</p>';
                isItemRemaining = false;
            }
        }).done(function () {
            if ($('#table').is(":visible")) {
                if (isItemRemaining === true) {
                    $(spinner.el).remove();
                    $('#content').removeClass('overlay');
                    $('tbody').html(content);
                }
            } else {
                $(spinner.el).remove();
                $('#content').removeClass('overlay');
                $('#content').append(content);
            }
        });
    }

    function getPolicies(limit, offset, typeID) {
        var content = '';
        $.get('php/suracommands.php?command=get_policies_by_category&type_id=' + typeID + '&limit=' + limit + '&offset=' + offset,
                function (data) {
                    data = JSON.parse(data);
                    for (var i = 0; i < data.length; i++) {
                        var policyInfo = data[i];
                        var id = policyInfo['id'];
                        var title = policyInfo['title'];
                        var price = policyInfo['price'];
                        var policy_more_info = policyInfo['more_info_link'];
                        var description = policyInfo['description'];
                        var tenure = policyInfo['tenure_info'][0]['in_months'];
                        var name = null;
                        var logo = null;
                        var email = null;
                        var phone = null;
                        var desc = null;
                        var more_info = null;
                        var policy_type = null;
                        if (policyInfo['insurer_id'] !== '0') {
                            var insurerInfo = policyInfo['insurer'][0];
                            name = insurerInfo['name'];
                            logo = insurerInfo['logo_url'];
                            email = insurerInfo['email'];
                            phone = insurerInfo['phone'];
                            more_info = insurerInfo['more_info_link'];
                            desc = insurerInfo['write_up'];
                            policy_type = 'custom';
//                            
                        } else {
                            name = 'UICI';
                            logo = 'uic.png';
                            email = '-';
                            phone = '-';
                            desc = '-';
                            policy_type = 'standard';
                        }

                        content += '<div class="panel panel-default" >' +
                                '<div class="panel-body">' +
                                '<div class="row">' +
                                '<div class="col-sm-4">' +
                                title +
                                '</div>' +
                                '<div id = "insurer_info" class="col-sm-3">' +
                                "<img id=\"\" width=\"40px\" height=\"60px\" src=\"img/insurers/" + logo + "\"  alt=\"img\" class=\"img-responsive img-rounded center-block\" style=\"display:inline; \"/>" + ' ' + name + '</div>' +
                                '<div class="col-sm-2">' +
                                price + ' iPoints / ' + tenure + ' months' +
                                '</div>' +
                                '<div class="col-sm-1">' +
                                '<a class="details-link small" data-logo="img/insurers/' + logo + '" data-title="' + title + '" data-name="' + name + '" data-description="' + description + '" data-company-desc="' + desc + '" data-more-info="' + more_info + '" data-policy-more-info="' + policy_more_info + '" data-price="' + price + '" data-tenure="' + tenure + '" data-email="' + email + '" data-phone="' + phone + '" data-toggle="modal" data-target="#detailsModal">View Details</a>' +
                                '</div>' +
                                '<div class="col-sm-2">' +
                                '<a data-policyid="' + id + '" data-policyprice="' + price + '" data-policytype="' + policy_type + '" class="btn btn-success btn-sm add-policy pull-right box-shadow--2dp" style="border-radius:7px;">Add Policy</a>' +
                                '</div>' +
                                '</div>' +
                                '</div></div>';
                    }
                }).done(function () {
            $(spinner.el).remove();
            $('#policiesView').removeClass('overlay');
            $('#policiesView').html(content);
        });
    }

    function getTotalItems() {
        var _signupid = null; // for policydetails...
        var _typeid = null; // for addnewpolicy...
        var api = '';
        if (filename === 'policydetails') {
            _signupid = $('#name').data('signupid');
            api = 'php/suracommands.php?command=get_my_payments&signupid=' + _signupid + '&web';
        } else if (filename === 'transactions') {
            api = 'php/suracommands.php?command=get_my_transactions&web';
        } else if (filename === 'points') {
            api = 'php/suracommands.php?command=get_redeemed_points&web';
        } else if (filename === 'addnewpolicy') {
            _typeid = $('#policiesView').data('typeid');
            api = 'php/suracommands.php?command=get_policies_by_category&id=' + _typeid;
        }
        $.get(api, function (data) {
            var info = JSON.parse(data);
            totalItems = _signupid === null ? info.length : info.length - 1; // cos of initial offset
        }).done(function () {
            $('#pagination').pagination({
                items: totalItems,
                itemsOnPage: NoOfItemsOnDisplay,
                cssStyle: 'light-theme',
                edges: 2,
                onPageClick: function (pageNumber) {
                    if (filename === 'transactions')
                        getTransactions(NoOfItemsOnDisplay, NoOfItemsOnDisplay * (pageNumber - 1));
                    else if (filename === 'points')
                        getPoints(NoOfItemsOnDisplay, NoOfItemsOnDisplay * (pageNumber - 1));
                    else if (filename === 'policydetails')
                        getPayments(NoOfItemsOnDisplay, NoOfItemsOnDisplay * (pageNumber - 1), _signupid);
                    else if (filename === 'addnewpolicy')
                        getPolicies(NoOfItemsOnDisplay, NoOfItemsOnDisplay * (pageNumber - 1), _typeid);
                }
            });
            if (filename === 'transactions')
                getTransactions(NoOfItemsOnDisplay, 0);
            else if (filename === 'points')
                getPoints(NoOfItemsOnDisplay, 0);
            else if (filename === 'policydetails')
                getPayments(NoOfItemsOnDisplay, 1, _signupid);
            else if (filename === 'addnewpolicy')
                getPolicies(NoOfItemsOnDisplay, 0, _typeid);
        });
    }

    function addPolicy(policyID, insurerID) {
        var api = null;
        if (insurerID) {
            api = './php/suracommands.php?command=add_user_signup&policyid = ' + policyID + '&insurerid=' + insurerID + '&web';
        } else {
            api = './php/suracommands.php?command=add_user_signup&policyid = ' + policyID + '&web';
        }
        $.post(api, function (data) {
            data = JSON.parse(data);
            if (data['info'] === 'SUCCESS') {
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'You successfuly signed for a(n) ' + data['policy_type'] + ' Insurance</div>');
                if (data['policy_type'] === 'Health') {
                    $('#hlp').html(data['points']);
                } else {
                    $('#glp').html(data['points']);
                }
                $('.caret-pos').html('<span class="fa fa-caret-down" style="color: #ff0000"></span>');
            } else if (data['info'] === 'FAILURE') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Failed due to <strong>Insufficient Points</strong>. <a href="#" data-toggle = "modal" data-target = "#pointsModal">Buy more iPoints</a> and try again</div>');
            } else if (data['info'] === 'ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error occured.</strong> Please try again later</div>');
            } else if (data['info'] === 'USER_DETAILS_INCOMPLETE') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Failed due to incomplete user details. Please update user profile in <a href="settings.php" class="alert-link">Settings</a></div>');
            } else if (data['info'] === 'NO_BASIC_POLICY') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'You cannot purchased a custom policy without having a basic policy from UICI</div>');
            }
        }).fail(function (data) {

        });
    }

    function renewPolicy(signupID) {
        var formData = {
            'signupID': signupID
        };
        $.post('./php/suracommands.php?command=renew_policy&web', formData, function (data) {
            data = JSON.parse(data);
            if (data['info'] === 'SUCCESS') {
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Policy renewed successfully</div>');
                if (data['policy_type'] === 'health') {
                    $('#hlp').html(data['points']);
                } else {
                    $('#glp').html(data['points']);
                }
                var _date = new Date(data['newdate']);
                var date = _date.getDate() + ' ' + toMonth((_date.getMonth() + 1).toString(), 'long') + ', ' + _date.getFullYear();
                $('#next-pay-day').html(date);
                getTotalItems('get_my_payments');
            } else if (data['info'] === 'FAILURE') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Failed due to <strong>Insufficient Points</strong>. <a href="#" data-toggle = "modal" data-target = "#pointsModal">Buy more iPoints</a> and try again</div>');
            } else if (data['info'] === 'ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>An error occurred</strong>. Please try again later</div>');
            }
        }).fail(function (data) {});
    }

    //*****************     Buy point with card    ***********************//



    var pay_rate = parseInt($("input[id=pay_rate]").val());

    $('body').on('input', 'input[name=pay_amt]', function (event) {
        var amt = parseInt($(this).val());

        $(this).next().children('span').html(amt + (amt * pay_rate / 100));
    });

    $('#pointsModal').on('submit', '#buyPointsForm', function (e) {
        e.preventDefault();

        if (parseInt($('input[name=pay_amt]').val()) > 0) {
            $('#pointsModal').modal('hide');
            payWithPaystack();
        }
    });

    function payWithPaystack() {
        var amt = parseInt($("input[name=pay_amt]").val());
        var commission = amt * (pay_rate / 100);
        var totalAmt = (amt + commission) * 100;
        var ref = 'UICI-' + $("input[id=pay_id]").val() + moment().unix();

        var payload = {
            'ref': ref,
            'amount': amt
        };

        var handler = PaystackPop.setup({
            key: 'pk_test_bb835543a20ab7491632daf4aecffde31b029f40',
            email: $("input[name=pay_email]").val(),
            amount: totalAmt,
            ref: ref,
            callback: function (response) {
                if (response.reference) {
                    $.post('php/suracommands.php?command=buy_points_card&web', payload, function (data) {
                        var info = '';
                        if (data.status) {
                            info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                                    'Points Purchased successfully</div>';
                            $('#hlp').html(data.health_points);
                            $('#glp').html(data.general_points);
                        } else {
                            if (data.info === 'ERROR') {
                                info = '<div class="alert alert-dismissible alert-danger text-center btn-rounded">' +
                                        'Points Purchase failed</div>';
                            }
                        }

                        $('#pay_info').html(info).stop().slideDown().delay(3000).slideUp();
                        working_alert.finish();
                    }, 'JSON').fail(function () {});
                } else {
                    alert('naah');
                }
            },
            onClose: function () {}
        });
        handler.openIframe();
    }

});

/************ Called from HTML **************/

function deletePolicy(signupID) {
    var formData = {
        'meta': 'delete_policy',
        'signupID': signupID
    };
    $.post('php/custom.php', formData, function (data) {
        if (data === 'success')
            window.location = "http://localhost/iinsurance/policies.php";
    }).fail(function () {

    });
}
