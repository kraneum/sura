/*jslint browser: true*/
/*global $, Spinner, alert */
$(document).ready(function () {
    /**************  Variables  ******************/
    var NoOfItemsOnDisplay = null;
    var limit = null;
    var offset = null;
    var totalItems = null;
    var isItemRemaining = true;
    var isEmpty = true; // to check whether there are elements at all...
    var newNumberCount = 1; // for point transfer to users...

    /************ For pagination *********************/
    var filenameWithExtension = location.pathname.split('/').slice(-1)[0];
    var filename = filenameWithExtension.split(".")[0];

    /****** For notifications *****************/
    $(".dropdown-toggle").hover(function () {
        $('#notifCount').removeClass('animated');
        $('#notifCount').removeClass('swing');
        $('#notifCount').removeClass('infinite');
    }, function () {
        $('#notifCount').addClass('animated');
        $('#notifCount').addClass('swing');
        $('#notifCount').addClass('infinite');
    });
    if ($('.dropdown-menu').data('notifcount') === 0) {
        getNotifications(false);
    } else {
        getNotifications(true);
    }

    /*********** Tab Active Selection *********/
    if (filename === 'home') {
        $('ul.navbar-nav > li:nth-child(1)').addClass('active');
        limit = 5;
        offset = 0;
        getRecentActivity(limit, offset);
//        drawChart();
    } else if (filename === 'points') {
        $('ul.navbar-nav > li:nth-child(2)').addClass('active');
        $('#transferAgent2AgentForm').parsley();
        $('#transferAgent2UserForm').parsley();
        NoOfItemsOnDisplay = 10;
        getTotalItems('get_transfers_by_agent_id');
    } else if (filename === 'settings') {
        changeSettingsNavLink($('#account-nav'));
        $('#profileForm').parsley();
        $('#dob').datepicker();
    } else if (filename === 'contact') {
        $('ul.navbar-nav > li:nth-child(4)').addClass('active');
        $('#contactForm').parsley();
    }

    /****************** Policies Event Handlers *****************/


    /****************** Settings Event Handlers *****************/
    $('#logout').click(function () {
        $.get('../php/suracommands.php?command=logout_agent&web', function (data) {
            data = JSON.parse(data);

            if (data === 'SUCCESS') {
                window.location = "login.php";
            } else {
                alert('Internal error occurred');
            }
        });
    });
    $('body').on('click', '#notif_footer', function () {
        $('.dropdown-menu > li .fa-spinner').show();

        var formData = {
            'notifIDs': $('.dropdown-menu').data('notifids')
        };
        $.post('../php/suracommands.php?command=update_notifications_agent&web', formData, function (data) {
            data = JSON.parse(data);

            if (data['info'] === 'SUCCESS') {
                $('.dropdown-menu > li .fa-spinner').hide();
                var notifCount = data['notifCount'];
                if (notifCount === 0) {
                    $('#notifCount').remove();
                } else {
                    $('#notifCount').html(notifCount);
                }
                getNotifications(true);
            } else {
                console.log('Failure');
            }
        });
    });
    $('body').on('submit', '#transferAgent2AgentForm', function (e) {
        $('#info').empty();

        var formData = {
            'phone': $('#transferAgent2AgentForm input[name=phone]').val(),
            'amount': $('#transferAgent2AgentForm input[name=amountt]').val()
        };
        $.post('../php/suracommands.php?command=transfer_points_agent_to_agent&web', formData, function (data) {
            data = JSON.parse(data);

            if (data['info'] === 'SUCCESS') {
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Points transferred succesfully</div>');
                setTimeout(function () {
                    $('#info').empty();
                }, 5000);
                $('#lp').html(data['points']);
                $('#caret').html('<span class="glyphicon glyphicon-triangle-bottom" style="color: #ff0000"></span>');
                getTotalItems('get_transfers_by_agent_id');
            } else if (data['info'] === 'INSUFFICIENT_POINTS') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Points Tranfer Failed!</strong> due to insufficient points' +
                        '</div>');
            } else if (data['info'] === 'ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error occured! </strong> Please try again later' +
                        '</div>');
            } else if (data['info'] === 'ID_NOT_RESOLVED') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Agent does not exist</strong>' +
                        '</div>');
            } else if (data['info'] === 'SELF_TRANSFER') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Invalid transfer operation</strong>' +
                        '</div>');
            } else if (data['info'] === 'INVALID_AMOUNT') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Invalid amount!</strong>. Enter amount less than ' + $('#lp').html() +
                        '</div>');
            } else if (data['info'] === 'ERORR_OCCURRED') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occurred!</strong> Please try again later' +
                        '</div>');
            }
            $("html, body").animate({scrollTop: 0}, "fast");
        }).fail(function () {});
        e.preventDefault();
    });
    $('body').on('submit', '#transferAgent2UserForm', function (e) {
        $('#info').empty();
        console.log($('#transferAgent2UserForm').serialize());

        $.post('../php/suracommands.php?command=transfer_points_agent_to_user&web', $('#transferAgent2UserForm').serialize(), function (data) {
            data = JSON.parse(data);

            if (data['info'] === 'SUCCESS') {
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Points transferred succesfully</div>');
                setTimeout(function () {
                    $('#info').empty();
                }, 5000);
                $('#lp').html(data['points']);
                $('#caret').html('<span class="glyphicon glyphicon-triangle-bottom" style="color: #ff0000"></span>');
                getTotalItems('get_transfers_by_agent_id');
            } else if (data['info'] === 'INSUFFICIENT_POINTS') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Points Tranfer Failed!</strong> due to insufficient points' +
                        '</div>');
            } else if (data['info'] === 'ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error occured! </strong> Please try again later' +
                        '</div>');
            } else if (data['info'] === 'NUMBER_NOT_RESOLVED') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>User does not exist</strong>' +
                        '</div>');
                $('#transferAgent2UserForm > div:nth-child(' + data['extra'] + ') input').addClass('parsley-error');
            } else if (data['info'] === 'DUPLICATE_NUMBERS') {
                var keys = Object.keys(data['extra']);
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>' + keys.length + '</strong> Duplicate Number(s) Entered' +
                        '</div>');
                for (var i = 0; i < keys.length; i++) {
                    $('#transferAgent2UserForm > div:nth-child(' + (+keys[i] + 1) + ') input').addClass('parsley-error');
                }

            } else if (data['info'] === 'INVALID_AMOUNT') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Invalid amount!</strong>. Enter amount less than ' + $('#lp').html() +
                        '</div>');
            } else if (data['info'] === 'ERORR_OCCURRED') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occurred!</strong> Please try again later' +
                        '</div>');
            }
            $("html, body").animate({scrollTop: 0}, "fast");
        }).fail(function () {});
        e.preventDefault();
    });
    $('body').on('click', '#add-num', function () {
        $('#info').empty();

        if (newNumberCount < 10) {
            newNumberCount++;
            $('#transferAgent2UserForm br').before('<div class="row" style="margin-top: 10px">\n\
<div class = "col-sm-offset-1 col-sm-6">\n\
<label for = "phone' + newNumberCount + '" class = "control-label">Phone Number: </label>\n\
<input type="text" name="phone[]" class="form-control" id="phone' + newNumberCount + '" placeholder="Enter phone number" data-parsley-required data-parsley-minlength="11" data-parsley-maxlength="11"  data-parsley-type="digits" data-parsley-minlength-message="This value should be 11 digits" data-parsley-maxlength-message="This value should be 11 digits" data-parsley-type-message="This value should contain digits only"/></div>\n\
<div class = "col-sm-offset-1 col-sm-4">\n\
<label for = "amount' + newNumberCount + '" class = "control-label">Amount: </label>\n\
<input type = "text" class = "form-control " name="amount[]" id = "amount' + newNumberCount + '" placeholder="Enter LP amount" data-parsley-required data-parsley-type="digits" data-parsley-type-message="This value should contain digits only"/>\n\
</div>\n\
<div class = "col-sm-1">\n\
<label></label>\n\
<button class="btn btn-danger btn-rounded text-center remove-num" style="margin-top: 12px; height: 20px; width: 20px; padding: 5px; line-height: 7px;">&times;</button>\n\
</div>\n\
</div>');
            $('#users-count').html(newNumberCount);
        } else {
            $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                    '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                    'Maximum of 10 numbers at a time</div>');
        }
    });
    $('body').on('click', '.remove-num', function () {
        newNumberCount--;
        $(this).parent().parent().fadeOut(500, function() {
            $(this).remove();
        });
        $('#users-count').html(newNumberCount);
    });

    $('body').on('submit', '#profileForm', function (event) {
        $('#info').empty();

        var formData = {
            'fname': $('input[name=fname]').val(),
            'lname': $('input[name=lname]').val(),
            'dob': $('input[name=dob]').val(),
            'gender': $("#gender option:selected").val(),
            'email': $('input[name=email]').val(),
            'phone': $('input[name=phone]').val()
        };
        $.post('../php/suracommands.php?command=update_agent_profile&web', formData, function (data) {
            data = JSON.parse(data);
            if (data['info'] === 'SUCCESS') {
                var fname = $('input[name=fname]').val();
                var lname = $('input[name=lname]').val();
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Update Successful</div>');
                $("#name").html(fname + ' ' + lname);
                $("html, body").animate({scrollTop: 0}, "fast");
            } else if (data['info'] === 'VALIDATION_ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Fill in appropiate values for all fields'
                        + '</div>');
            } else if (data['info'] === 'NO_EDIT') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Update Failed!</strong> Kindly edit a field and try again' +
                        '</div>');
            } else if (data['info'] === 'FAILURE') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occured!</strong> Try again later' +
                        '</div>');
            } else if (data['info'] === 'ERORR_OCCURRED') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occurred!</strong> Please try again later' +
                        '</div>');
            }
            $("html, body").animate({scrollTop: 0}, "fast");
        }).fail(function (data) {});
        event.preventDefault();
    });
    $('body').on('submit', '#resetPasswordForm', function (event) {
        $('#info').empty();
        var formData = {
            'oldPassword': $('input[name=oldPassword]').val(),
            'newPassword': $('input[name=newPassword]').val()
        };
        $.post('../php/suracommands.php?command=update_agent_password&web', formData, function (data) {
            data = JSON.parse(data);
            if (data['info'] === 'SUCCESS') {
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Update Successful</div>');
                $('input[name=oldPassword]').val('');
                $('input[name=newPassword]').val('');
                $('input[name=confirmPassword]').val('');
            } else if (data['info'] === 'FAILURE') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Update Failed!</strong> Please try again later' +
                        '</div>');
            } else if (data['info'] === 'INCORRECT_PASSWORD') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<strong>Incorrect Password! </strong> Enter correct password or reset password' +
                        '</div>');
                $('input[name=oldPassword]').val('');
                $('input[name=newPassword]').val('');
                $('input[name=confirmPassword]').val('');
            } else if (data['info'] === 'NO_CHANGES') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Update Failed! </strong> Kindly enter a different password' +
                        '</div>');
                $('input[name=newPassword]').val('');
                $('input[name=confirmPassword]').val('');
            } else if (data['info'] === 'VALIDATION_ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Fill in all fields with appropiate values' +
                        '</div>');
            } else if (data['info'] === 'ERORR_OCCURRED') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occurred!</strong> Please try again later' +
                        '</div>');
            }
            $("html, body").animate({scrollTop: 0}, "fast");
        }).fail(function (data) {});
        event.preventDefault();
    });
    $('body').on('submit', '#resetSQForm', function (event) {
        $('#info').empty();
        var formData = {
            'question': $('#question').val(),
            'answer': $('input[name=answer]').val()
        };
        $.post('../php/suracommands.php?command=update_agent_security_question&web', formData, function (data) {
            data = JSON.parse(data);
            if (data['info'] === 'SUCCESS') {
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        'Update Successful</div>');
                $('input[name=answer]').val('');
            } else if (data['info'] === 'FAILURE') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<strong>Update Failed!</strong> Please try again with different entry' +
                        '</div>');
            } else if (data['info'] === 'VALIDATION_ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        'Fill all fields with appropiate values' +
                        '</div>');
            } else if (data['info'] === 'ERORR_OCCURRED') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occurred!</strong> Please try again later' +
                        '</div>');
            }
            $("html, body").animate({scrollTop: 0}, "fast");
        }).fail(function (data) {});
        event.preventDefault();
    });
    $('body').on('submit', '#profilePicForm', function (event) {
        $('#info').empty();

        $.ajax({
            type: "POST",
            url: "../php/suracommands.php?command=upload_profile_pic_agent&web",
            data: new FormData(document.getElementById('profilePicForm')),
            dataType: 'JSON',
            /*** Options to tell JQuery not to process data or worry about content-type ****/
            cache: false,
            contentType: false,
            processData: false,
            /****************************************/
            success: function (data) {
                if (data['info'] === 'SUCCESS') {
                    $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            'Update Successful</div>');
                    $('#profile-pic').attr('src', '../img/agents/' + data.path + '?' + Date.now());
                    $('#header-pic').attr('src', '../img/agents/' + data.path + '?' + Date.now());
                } else if (data['info'] === 'FAILURE') {
                    $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Update Failed!</strong> Please try again with different entry' +
                            '</div>');
                } else if (data['info'] === 'VALIDATION_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>' + data['errVal']['message'] + '</strong>' +
                            '</div>');
                    $('#' + data['errVal']['field']).removeClass('parsley-success');
                    $('#' + data['errVal']['field']).addClass('parsley-error');
                } else if (data['info'] === 'UPLOAD_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>File upload failed</strong>. Try again later' +
                            '</div>');
                } else if (data['info'] === 'FILE_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Please upload a valid file</strong>' +
                            '</div>');
                } else if (data['info'] === 'ERORR_OCCURRED') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Error Occurred!</strong> Please try again later' +
                            '</div>');
                }
                $("html, body").animate({scrollTop: 0}, "fast");
            }
        });

        event.preventDefault();
    });
    $('body').on('submit', '#contactForm', function (event) {
        $('#info').empty();
        $('form i.fa').removeClass('fa-send-o');
        $('form i.fa').addClass('fa-spinner');
        $('form i.fa').addClass('fa-pulse');
        $('form button span').html('On the FLY...');
        var formData = {
            'type': $("#type option:selected").text(),
            'message': $('textarea[name=message]').val(),
            'entity': 'AGENT'
        };
        $.post('../php/suracommands.php?command=send_message&web', formData, function (data) {
            data = JSON.parse(data);
            $('form i.fa').addClass('fa-send-o');
            $('form i.fa').removeClass('fa-spinner');
            $('form i.fa').removeClass('fa-pulse');
            $('form button span').html('SEND');
            if (data['info'] === 'SUCCESS') {
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Message sent succesfully</div>');
                setTimeout(function () {
                    $('#info').empty();
                }, 5000);
                $('textarea[name=message]').val('');
            } else if (data['info'] === 'ERROR_OCCURRED') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error occured! </strong> Please try again later' +
                        '</div>');
            } else if (data['info'] === 'VALIDATION_ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Message more than 1000 characters</strong>' +
                        '</div>');
            }

        }).fail(function (data) {});
        event.preventDefault();
    });
    $("#dp-upload").click(function () {
        $('#info').empty();
        $('#profile').hide();
        $('#sub-content').empty().load('views/_upload_profile_pic.php', function () {
            $('#profilePicForm').parsley();
        });
        $("html, body").animate({scrollTop: 0}, "fast");
    });
    $("#account-nav").click(function () {
        $('#info').empty();
        changeSettingsNavLink($(this));
        $('#sub-content').empty();
        $('#profile').show();
        $("html, body").animate({scrollTop: 0}, "fast");
    });
    $("#referral-nav").click(function () {
        $('#info').empty();
        changeSettingsNavLink($(this));
        $('#profile').hide();
        $('#sub-content').empty().load('views/_referrals.php');
        $("html, body").animate({scrollTop: 0}, "fast");
    });
    $("#setnot-nav").click(function () {
        $('#info').empty();
        changeSettingsNavLink($(this));
        $('#profile').hide();
        $('#sub-content').empty().append('Coming Soon!!!');
        $("html, body").animate({scrollTop: 0}, "fast");
    });
    $("#help-nav").click(function () {
        $('#info').empty();
        changeSettingsNavLink($(this));
        $('#profile').hide();
        $('#sub-content').empty().append('Coming Soon!!!');
        $("html, body").animate({scrollTop: 0}, "fast");
    });
    $("#password-reset").click(function () {
        $('#info').empty();
        var content = '<div class="reset">' +
                '<form id="resetPasswordForm">' +
                '<div class="row">' +
                '<div class="col-sm-2"></div>' +
                '<div class="col-sm-7">' +
                '<div class="row">' +
                '<label class="col-sm-4" for="oldPassword"><span class="pull-right">Old Password</span> </label>' +
                '<input class="col-sm-8" id="oldPassword" name="oldPassword" type="password" data-parsley-required />' +
                '</div>' +
                '<div class="row">' +
                '<label class="col-sm-4" for="newPassword"><span class="pull-right">New Password</span> </label>' +
                '<input class="col-sm-8" id="newPassword" name="newPassword" type="password" ' +
                'data-parsley-required data-parsley-minlength="4"/>' +
                '</div>' +
                '<div class="row">' +
                '<label class="col-sm-4" for="confirmPassword"><span class="pull-right">Confirm Password</span> </label>' +
                '<input class="col-sm-8" id="confirmPassword" name="confirmPassword" type="password"' +
                'data-parsley-required data-parsley-equalto="#newPassword" data-parsley-equalto-message="Password mismatch" />' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-sm-2">' +
                '<button id="settings-back" type="button" class="btn btn-primary settings-btn btn-rounded box-shadow--2dp btn-lg" name="back"><i class="fa fa-arrow-left"></i></button>' +
                '</div>' +
                '<div class="col-sm-8"></div>' +
                '<div class="col-sm-2">' +
                '<button id="reset_password_save" type="submit" class="btn btn-primary settings-btn btn-rounded box-shadow--2dp btn-lg" name="save"><strong>SAVE</strong></button>' +
                '</div>' +
                '</div>' +
                '</form>' +
                '</div>';
        $('#profile').hide();
        $('#sub-content').empty().html(content);
        $('#resetPasswordForm').parsley();
    });
    $("#sq-reset").click(function () {
        var content = '<div class="reset">' +
                '<form id="resetSQForm">' +
                '<div class="row">' +
                '<div class="col-sm-2"></div>' +
                '<div class="col-sm-7">' +
                '<div class="row">' +
                '<label class="col-sm-4" for="question"><span class="pull-right">Security Question</span> </label>' +
                '<select class="col-sm-8" id="question" name="question"  data-parsley-required>' +
                '<option disabled selected>Select security question</option>';
        $.get('../php/suracommands.php?command=get_security_questions', function (data) {
            data = JSON.parse(data);
            for (var i = 0; i < data.length; i++)
                content += "<option value = '" + data[i]['question'] + "'>" + data[i]['question'] + '</option>';
            content += '</select>' +
                    '</div>' +
                    '<div class="row">' +
                    '<label class="col-sm-4" for="answer"><span class="pull-right">Security Answer</span> </label>' +
                    '<input class="col-sm-8" id="answer" name="answer" type="text" data-parsley-required/>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="row">' +
                    '<div class="col-sm-2">' +
                    '<button id="settings-back" type="button" class="btn btn-primary settings-btn btn-rounded box-shadow--2dp btn-lg" name="back"><i class="fa fa-arrow-left"></i></button>' +
                    '</div>' +
                    '<div class="col-sm-8"></div>' +
                    '<div class="col-sm-2">' +
                    '<button id="reset_password_save" type="submit" class="btn btn-primary settings-btn btn-rounded box-shadow--2dp btn-lg" name="save"><strong>SAVE</strong></button>' +
                    '</div>' +
                    '</div>' +
                    '</form>' +
                    '</div>';
            $('#profile').hide();
            $('#sub-content').empty().html(content);
            $('#resetSQForm').parsley();
        });
    });
    $('body').on('click', '#settings-back', function () {
        $('#info').empty();
        $('#sub-content').empty();
        $('#profile').show();
    });
    $('body').on('click', '#load-new', function () {
        if (isItemRemaining === true) {
            offset += limit;
            getRecentActivity(limit, offset);
            isEmpty = false;
        }
    });
    /********** Miscellaneous ***********************/

    function changeSettingsNavLink(e) {
        e.css("background-color", "#265a88");
        e.css("color", "white");
        e.find("img").attr("src", "../img/icons/left_arrow_white.png");
        e.hover(function () {
            e.css("background-color", "#265a88");
        },
                function () {
                    e.css("background-color", "#265a88");
                });
        var temp = e.index();
        $('#settings-nav p').each(function (i) {
            if (i !== temp) {
                $(this).css("background-color", "white");
                $(this).css("color", "black");
                $(this).find("img").attr("src", "../img/icons/left_arrow_grey.png");
                $(this).hover(function () {
                    $(this).css("background-color", "#dadada");
                },
                        function () {
                            $(this).css("background-color", "white");
                        });
            }
        });
    }

    /*************************************/

    function toMonth($num, $type) {
        switch ($num) {
            case '1':
                return $type === 'long' ? 'January' : 'Jan';
            case '2':
                return $type === 'long' ? 'February' : 'Feb';
            case '3':
                return $type === 'long' ? 'March' : 'Mar';
            case '4':
                return $type === 'long' ? 'April' : 'Apr';
            case '5':
                return $type === 'long' ? 'May' : 'May';
            case '6':
                return $type === 'long' ? 'June' : 'Jun';
            case '7':
                return $type === 'long' ? 'July' : 'Jul';
            case '8':
                return $type === 'long' ? 'August' : 'Aug';
            case '9':
                return $type === 'long' ? 'September' : 'Sep';
            case '10':
                return $type === 'long' ? 'October' : 'Oct';
            case '11':
                return $type === 'long' ? 'November' : 'Nov';
            case '12':
                return $type === 'long' ? 'December' : 'Dec';
            default:
                return "Incorrect Month Value";
        }
    }

    function getNotifications(flag) {
        var content = '';
        var notifIDs = '';
        if (flag === true) {
            $.get('../php/suracommands.php?command=get_notifications_agent&limit=10&web', function (data) {
                var notifs = JSON.parse(data);
                var notifCount = notifs.length;

                if (notifCount > 0) {
                    for (var i = 0; i < notifCount; i++) {
                        i === 0 ? '' : notifIDs += ',';
                        notifIDs += notifs[i]['id'];
                        content += '<li class="divider notif-temp"></li>';
                        content += '<li class="text-center notif-temp">' + notifs[i]['message'] + '</li>';
                    }

                    content += '<li class="divider notif-temp" id="last_divider"></li>';
                    content += '<li id="notif_footer" class="text-center notif-temp">Mark as Read</li>';
                } else {
                    content += '<li class="divider"> </li>';
                    content += '<li id="no_new_notif" class="text-center">No new notifications</li>';
                }

            }).done(function () {
                $('.dropdown-menu > li .fa-spinner').hide();
                $('.notif-temp').remove();
                $('.dropdown-menu').append(content);
                $('.dropdown-menu').data('notifids', notifIDs);
                $('.dropdown-menu #notif_header').css('padding-bottom', '0');
                $('.dropdown-menu .divider:nth-child(2)').css('margin-top', '5px');
            });
        } else {
            content += '<li class="divider"> </li>';
            content += '<li id="no_new_notif" class="text-center">No new notifications</li>';
            $('.dropdown-menu > li .fa-spinner').hide();
            $('.dropdown-menu').append(content);
            $('.dropdown-menu #notif_header').css('padding-bottom', '0');
            $('.dropdown-menu .divider:nth-child(2)').css('margin-top', '5px');
        }

    }

    function getRecentActivity(limit, offset) {
        var content = '';

        $('#south-content > div').append(spinner.el);
        $('#south-content > div').addClass('overlay');
        $.get('../php/suracommands.php?command=get_recent_activity_agent&limit=' + limit + '&offset=' + offset + '&web', function (data) {
            var logInfo = JSON.parse(data);
            var newDateDay = null;
            var newDateMonth = null;
            var newDateYear = null;

            if (logInfo.length > 0) {
                isItemRemaining = true;

                for (var i = 0; i < logInfo.length; i++) {
                    var ref = logInfo[i]['ref'] !== null ? logInfo[i]['ref'][0] : '';
                    var _date = new Date(logInfo[i]['date']);
                    var dateDay = _date.getDate().toString();
                    var dateMonth = toMonth((_date.getMonth() + 1).toString());
                    var dateYear = _date.getFullYear().toString();
                    var action = logInfo[i]['action'];
                    var iconColor = null;
                    var heading = null;
                    var info = null;
                    if (action === 'TRANSFER_POINTS_USER') {
                        heading = 'POINTS TRANSFER TO USER';
                        iconColor = 'bg-secondary';
                        info = 'You transferred ' + ref['point_worth'] + ' Loyalty Points to ' + ref['user'];
                    } else if (action === 'TRANSFER_POINTS_AGENT') {
                        heading = 'POINTS TRANSFER TO AGENT';
                        iconColor = 'bg-secondary';
                        info = 'You transferred ' + ref['point_worth'] + ' Loyalty Points to ' + ref['agent'];
                    } else if (action === 'RECEIVED_POINTS_MERCHANT') {
                        heading = 'RECEIVED POINTS FROM MERCHANT';
                        iconColor = 'bg-success';
                        info = 'You received ' + ref['point_worth'] + ' Loyalty Points from ' + ref['merchant'];
                    } else if (action === 'RECEIVED_POINTS_AGENT') {
                        heading = 'RECEIVED POINTS FROM AGENT';
                        iconColor = 'bg-success';
                        info = 'You received ' + ref['point_worth'] + ' Loyalty Points from ' + ref['agent'];
                    } else if (action === 'EDIT_PROFILE') {
                        heading = 'PROFILE EDIT';
                        iconColor = 'bg-info';
                        info = 'You edited your profile information';
                    } else if (action === 'CHANGE_PASSWORD') {
                        heading = 'PASSWORD CHANGE';
                        iconColor = 'bg-info';
                        info = 'You changed your password details';
                    } else if (action === 'CHANGE_SEC_QUE') {
                        heading = 'SECURITY QUESTION CHANGE';
                        iconColor = 'bg-info';
                        info = 'You edited your security question information';
                    } else if (action === 'CHANGE_PROFILE_PIC') {
                        heading = 'PROFILE PICTURE CHANGE';
                        iconColor = 'bg-info';
                        info = 'You changed your profile picture';
                    } else if (action === 'SEND_MESSAGE') {
                        heading = 'MESSAGE SENT';
                        iconColor = 'bg-info';
                        info = 'You sent a message to the administrator. Awaiting response...';
                    } else if (action === 'ADMIN_DISAPPROVE') {
                        heading = 'ADMIN BAN';
                        iconColor = 'bg-danger';
                        info = "You were banned from accessing the platform by UICI administrator";
                    } else if (action === 'ADMIN_APPROVE') {
                        heading = 'ADMIN UNBAN';
                        iconColor = 'bg-info';
                        info = "You were un-banned from accessing the platform by UICI administrator";
                    } else if (action === 'ADMIN_EDIT') {
                        heading = 'ADMIN EDIT';
                        iconColor = 'bg-info';
                        info = "Your profile was edited by UICI administrator";
                    } 

                    if (dateDay !== newDateDay || dateMonth !== newDateMonth || dateYear !== newDateYear) {
                        if (i !== 0) {
                            content += '</div>' +
                                    '</div>' +
                                    '</div>';
                        }
                        newDateDay = dateDay;
                        newDateMonth = dateMonth;
                        newDateYear = dateYear;
                        content += '<div class="row">'
                                + '<div class="col-xs-1">' +
                                '<div id="date" class="text-center pull-right">' +
                                '<span id="month">' + dateYear + '</span>' +
                                '<br>' +
                                '<span id="month">' + dateMonth + '</span>' +
                                '<br>' +
                                '<span id="day">' + dateDay + '</span>' +
                                '</div>' +
                                '</div>' +
                                '<div class="col-xs-11">' +
                                '<div class="timeline-centered">';
                    }
                    content += '<article class="timeline-entry">' +
                            '<div class="timeline-entry-inner">' +
                            '<div class="timeline-icon ' + iconColor + '"></div>' +
                            '<div class="timeline-label">' +
                            '<h2>' + heading + '</h2>' +
                            '<p>' + info + '</p>' +
                            '</div>' +
                            '</div>' +
                            '</article>';
                }

                content += '<article id="plus" class="timeline-entry begin">' +
                        '<div class="timeline-entry-inner">' +
                        '<div id="load-new" class="timeline-icon" style="-webkit-transform: rotate(-90deg); -moz-transform: rotate(-90deg); cursor:pointer;">' +
                        '<i class="fa fa-plus"></i>' +
                        '</div>' +
                        ' </div>' +
                        '</article>';
                content += '</div>' +
                        '</div>' +
                        '</div>';
            } else {
                isItemRemaining = false;
                if (isEmpty) {
                    content = '<p style="padding: 50px;" class="text-center text-muted text-italic">No Recent Activity</p>';
                }
            }
        }).done(function () {
            $(spinner.el).remove();
            $('#south-content > div').removeClass('overlay');
            if (isItemRemaining || isEmpty) {
                $('#south-content > div').empty().append(content);
            }
        });
    }

    function getPoints(limit, offset) {
        var content = '';
        $('#content').append(spinner.el);
        $('#content').addClass('overlay');
        $.get('../php/suracommands.php?command=get_transfers_by_agent_id&limit=' + limit + '&offset=' + offset + '&web', function (data) {
            var transferInfo = JSON.parse(data);

            if (transferInfo.length > 0) {
                isItemRemaining = true;
                if ($('#table').is(":hidden")) {
                    $('#no-points').remove();
                    $('#table').show();
                }
                for (var i = 0; i < transferInfo.length; i++) {
                    var _date = new Date(transferInfo[i]['date_transacted']);
                    var date = _date.getDate() + ' - ' + toMonth((_date.getMonth() + 1).toString()) + ' - ' + _date.getFullYear();
                    var ref = transferInfo[i]['ref'];
                    var points = transferInfo[i]['point_worth'];
                    var balance = null;
                    var trans_type = transferInfo[i]['trans_type'];
                    var sign = trans_type === 'credit' ? '+' : '-';
                    var img = null;
                    if (sign === '+') {
                        img = '<span class="glyphicon glyphicon-triangle-top" style="color: #33ff00; font-size:0.7em;"></span>';
                        balance = transferInfo[i]['previous_balance_to'];
                    } else {
                        img = '<span class="glyphicon glyphicon-triangle-bottom" style="color: #ff0000; font-size:0.7em;"></span>';
                        balance = transferInfo[i]['previous_balance_from'];
                    }

                    content += '<tr>';
                    content += '<td class="text-center">' + date + '</td>';
                    content += '<td class="text-center">' + ref + '</td>';
                    content += '<td class="text-center">' + img + ' ' + points + '</td>';
                    content += '<td class="text-center">' + balance + '</td>';
                    content += '</tr>';
                }
            } else {
                content += '<p id="no-points" style="padding: 50px;" class="text-center text-muted text-italic">No Points Redeemed</p>';
                isItemRemaining = false;
            }
        }).done(function () {
            if ($('#table').is(":visible")) {
                if (isItemRemaining === true) {
                    $(spinner.el).remove();
                    $('#content').removeClass('overlay');
                    $('tbody').html(content);
                }
            } else {
                $(spinner.el).remove();
                $('#content').removeClass('overlay');
                $('#content').append(content);
            }
        });
    }

    function getTotalItems(command) {
        var api = '../php/suracommands.php?command=' + command + '&web';

        $.get(api, function (data) {
            var info = JSON.parse(data);
            totalItems = info.length;
        }).done(function () {
            $('#pagination').pagination({
                items: totalItems,
                itemsOnPage: NoOfItemsOnDisplay,
                cssStyle: 'light-theme',
                edges: 4,
                onPageClick: function (pageNumber) {
                    if (filename === 'points') {
                        getPoints(NoOfItemsOnDisplay, NoOfItemsOnDisplay * (pageNumber - 1));
                    }
                }
            });
            if (filename === 'points') {
                getPoints(NoOfItemsOnDisplay, 0);
            }

        });
    }

    function drawChart() {
        var dataset = [];
        var labels = [];
        var count;

        $('#north-content > div').append(spinner.el);
        $('#north-content > div').addClass('overlay');
        $.get('../php/suracommands.php?command=get_insurer_policies_and_signups', function (data) {
            data = JSON.parse(data);
            count = data[data.length - 1]['count'];
            for (var i = 0; i < data.length - 1; i++) {
                labels.push(data[i]['title']);
                dataset.push(data[i]['no_of_signups']);
            }
        }).done(function () {
            $(spinner.el).remove();
            $('#north-content > div').removeClass('overlay');
            $('#north-content > span').append(' (' + count + ')');
            var ctx = $('#myChart');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: labels,
                    datasets: [
                        {
                            type: 'bar',
                            label: 'Bar Component',
                            data: dataset,
                            backgroundColor: 'rgba(18,168,233,0.5)'
                        },
                        {
                            type: 'line',
                            label: 'Line Component',
                            data: dataset,
                            backgroundColor: 'rgba(231,166,51,0.5)',
                            borderColor: 'rgba(209,66,105,0.5)',
                            borderWidth: 2,
//                            stepped: true
                        }
                    ]
                },
                options: {
                    title: {
                        display: true,
                        text: 'Number of user signups per policy',
                        position: 'bottom',
                        fullWidth: true,
                        fontSize: 14,
                        fontColor: '#1c4b70'
                    },
                    legend: {
                        display: false
                    },
                    responsiveAnimationDuration: 1000,

                }
            });
        });
    }

});

