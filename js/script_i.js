/*jslint browser: true*/
/*global $, Spinner*/
$(document).ready(function () {
    /**************  Variables  ******************/
    var NoOfItemsOnDisplay = null;
    var limit = null;
    var offset = null;
    var totalItems = null;
    var isItemRemaining = true;
    var isEmpty = true; // to check whether there are elements at all...
    var myPieChart = null;
    var myLineChart = null;

    /************ For pagination *********************/
    var filenameWithExtension = location.pathname.split('/').slice(-1)[0];
    var filename = filenameWithExtension.split(".")[0];

    /****** For notifications *****************/
    $(".dropdown-toggle").hover(function () {
        $('#notifCount').removeClass('animated');
        $('#notifCount').removeClass('swing');
        $('#notifCount').removeClass('infinite');
    }, function () {
        $('#notifCount').addClass('animated');
        $('#notifCount').addClass('swing');
        $('#notifCount').addClass('infinite');
    });
    if ($('.dropdown-menu').data('notifcount') === 0) {
        getNotifications(false);
    } else {
        getNotifications(true);
    }

    /*********** For Pop-over *********/
    $('#wallet_info').popover();
    $('#point_info').popover();

    /*********** Tab Active Selection *********/
    if (filename === 'home') {
        $('ul.navbar-nav > li:nth-child(1)').addClass('active');
        limit = 5;
        offset = 0;

        getRecentActivity(limit, offset);
        getSummaryData();
        $('.input-daterange input').each(function () {
            $(this).datepicker({
                format: "dd/mm/yyyy",
                clearBtn: true,
                autoclose: true
            });
        });

    } else if (filename === 'policies') {
        $('ul.navbar-nav > li:nth-child(2)').addClass('active');
        NoOfItemsOnDisplay = 5;
        getTotalItems('get_insurer_policies');
    } else if (filename === 'marketing') {
        $('ul.navbar-nav > li:nth-child(3)').addClass('active');
        $('#userSearchFilter').parsley();
        $('#messageForm').parsley();
        $('#buySMSForm').parsley();
        NoOfItemsOnDisplay = 10;
        $("#points_range").ionRangeSlider({
            grid: true,
            min: 0,
            max: 10000,
            step: 50
        });
    } else if (filename === 'settings') {
        changeSettingsNavLink($('#account-nav'));
        $('#profileForm').parsley();
        $('#changeNameForm').parsley();
    } else if (filename === 'usergen') {
        $('ul.navbar-nav > li:nth-child(4)').addClass('active');
        $('#uploadUsersForm').parsley();
    }

    /****************** Policies Event Handlers *****************/
    $('body').on('submit', '#pieChartFilter', function (event) {
        $('#info').empty();
        var formData = {
            'gender_b': $("#gender_b option:selected").val(),
            'startDate_b': $('#startDate_b').val(),
            'endDate_b': $('#endDate_b').val(),
            'active_b': $("#active_b option:selected").val(),
            'for_b': $('#num_range_b').data('from'),
            'end_b': $('#num_range_b').data('to')
        };

        console.log(JSON.stringify(formData) + "--- " + $('#pieChartFilter').serialize());
        drawPieChart($('#pieChartFilter').serialize());
        event.preventDefault();
    });
    $('body').on('submit', '#lineChartFilter', function (event) {
        $('#info').empty();
        var formData = {
            'gender_l': $("#gender_l option:selected").val(),
            'startDate_l': $('#startDate_l').val(),
            'endDate_l': $('#endDate_l').val(),
            'active_l': $("#active_l option:selected").val(),
            'for_l': $('#num_range_l').data('from'),
            'end_l': $('#num_range_l').data('to')
        };

        console.log(JSON.stringify(formData) + "--- " + $('#lineChartFilter').serialize());
        drawLineChart($('#lineChartFilter').serialize());
        event.preventDefault();
    });
    $('body').on('submit', '#userSearchFilter', function (event) {
//        $('#query_display > div').empty();
        getTotalItems($('#userSearchFilter').serialize());
        event.preventDefault();
    });
    $('body').on('click', '#query_display .row > table .fa-envelope-o', function (event) {
        $('.modal #info').empty();
        $('#notifMessage').val('');
        $('.modal').data('userids', $(this).data('userid'));
        $('#messageModal').modal();
        event.preventDefault();
    });
    $('body').on('click', '#messageAll', function (event) {
        $('.modal #info').empty();
        $('#notifMessage').val('');
        $('.modal').data('userids', $(this).data('userids'));
        event.preventDefault();
    });
    $('body').on('submit', '#messageForm', (function (event) {
        $('#info').empty();
        $('#notifSend').addClass('disabled');
        var formData = {
            'message': $('textarea[name=notifMessage]').val(),
            'userids': $('.modal').data('userids')
        };
        $.post('../php/suracommands.php?command=send_notification&web', formData, function (data) {
            console.log(data);
            data = JSON.parse(data);
            $('#notifSend').removeClass('disabled');
            if (data['info'] === 'SUCCESS') {
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Notification sent Successful</div>');
                $('#sms_wallet').html(data['points']);
            } else if (data['info'] === 'VALIDATION_ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Notification message more than 200 characters' +
                        '</div>');
            } else if (data['info'] === 'ERORR_OCCURRED') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occurred!</strong> Please try again later' +
                        '</div>');
            }
            $("html, body").animate({scrollTop: 0}, "fast");
        }).fail(function (data) {});
        event.preventDefault();
    }));
    $('body').on('click', '#policy-back', function () {
        $('#info').empty();
        $('#secondary').empty();
        $('#primary').fadeIn('slow');
        $('#publishPolicy').show();
        getTotalItems('get_insurer_policies');
    });
    $('body').on('click', '#publishPolicy', function () { // publish policy 
        $('#info').empty();
        $('#no-policies').remove();
        $(this).hide();
        $('#content').append(spinner.el);
        $('#content').addClass('overlay');
        $('#secondary').load("views/_add_new_policy.php", function () {
            $('#primary').hide();
            $(spinner.el).remove();
            $('#content').removeClass('overlay');
        });
    });
    $('body').on('click', '#editBtn', function (event) {
        $('#info').empty();
        $('#publishPolicy').hide();
        $('#content').append(spinner.el);
        $('#content').addClass('overlay');
        var formData = {
            'policyID': $(this).data('policyid')
        };
        $('#secondary').load("views/_edit_policy.php", formData, function () {
            $('#primary').hide();
            $(spinner.el).remove();
            $('#content').removeClass('overlay');
        });
    });
    $('body').on("click", "#deactivateBtn", function () {
        $('#info').empty();
        var policyid = $(this).data('policyid');
        $("#continue #modalDeactivateBtn").data('policyid', policyid);
    });
    $('body').on('click', '#modalDeactivateBtn', function (event) {
        $('.modal').modal('hide');
        var formData = {
            'policyID': $(this).data('policyid')
        };
        $.post('../php/suracommands.php?command=deactivate_policy&web', formData, function (data) {
            data = JSON.parse(data);
            if (data['info'] === 'SUCCESS') {
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>DE-ACTIVATE SUCCESSFUL</strong></div>');
                getTotalItems('get_insurer_policies');
            } else if (data['info'] === 'FAILURE') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occured!</strong> Try again later' +
                        '</div>');
            } else if (data['info'] === 'NO_CHANGES') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Update Failed!</strong> Kindly edit a field and try again' +
                        '</div>');
            } else if (data['info'] === 'ERORR_OCCURRED') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occurred!</strong> Please try again later' +
                        '</div>');
            }
            $("html, body").animate({scrollTop: 0}, "fast");
        }).fail(function (data) {});
    });
    $('body').on('submit', '#editPolicyForm', (function (event) {
        $('#info').empty();
        $('#content').append(spinner.el);
        $('#content').addClass('overlay');
        var formData = {
            'policyID': $("input[name=id]").val(),
            'title': $('input[name=title]').val(),
            'desc': $('textarea[name=desc]').val(),
            'extlink': $('input[name=extlink]').val(),
            'price': $('input[name=price]').val(),
            'tenure': $("#tenure option:selected").val()
        };
        $.post('../php/suracommands.php?command=update_policy_info&web', formData, function (data) {
            data = JSON.parse(data);
            if (data['info'] === 'SUCCESS') {
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>UPDATE SUCCESSFUL</strong></div>');
                setTimeout(function () {
                    $('#info').empty();
                }, 5000);
                $('#secondary').empty();
                getTotalItems('get_insurer_policies');
                $('#content > div:nth-child(1)').fadeIn('slow');
            } else if (data['info'] === 'VALIDATION_ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        data['errVal']['message']
                        + '</div>');
            } else if (data['info'] === 'NO_CHANGES') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Update Failed!</strong> Kindly edit a field and try again' +
                        '</div>');
            } else if (data['info'] === 'FAILURE') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occured!</strong> Try again later' +
                        '</div>');
            } else if (data['info'] === 'ERORR_OCCURRED') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occurred!</strong> Please try again later' +
                        '</div>');
            }
            $("html, body").animate({scrollTop: 0}, "fast");
        }).done(function (data) {
            $(spinner.el).remove();
            $('#content').removeClass('overlay');
        });
        event.preventDefault();
    }));
    $('body').on('submit', '#newPolicyForm', (function (event) {
        $('#info').empty();
        $('#content').append(spinner.el);
        $('#content').addClass('overlay');
        $('#publishPolicy').show();

        $.post('../php/suracommands.php?command=publish_new_policy&web', $(this).serialize(), function (data) {
            data = JSON.parse(data);
            if (data['info'] === 'SUCCESS') {
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Publish Successful</strong></div>');
                setTimeout(function () {
                    $('#info').empty();
                }, 5000);
                $('#secondary').empty();
                $('#primary').fadeIn('slow');
                getTotalItems('get_insurer_policies');
            } else if (data['info'] === 'VALIDATION_ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Fill in appropiate values for all fields</strong>'
                        + '</div>');
            } else if (data['info'] === 'FAILURE') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occured!</strong> Try again later' +
                        '</div>');
            } else if (data['info'] === 'ERORR_OCCURRED') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occurred!</strong> Please try again later' +
                        '</div>');
            }
            $("html, body").animate({scrollTop: 0}, "fast");
        }).done(function () {
            $(spinner.el).remove();
            $('#content').removeClass('overlay');
        });
        event.preventDefault();
    }));

    /****************** Settings Event Handlers *****************/
    $('.logout_').click(function () {
        swal({
            title: "Are you sure?",
            text: 'Clicking "OK" will logout you out of the system',
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function (result) {
            if (result) {
                $.getJSON('../php/suracommands.php?command=logout_insurer&web', function (data) {

                    if (data === 'SUCCESS') {
                        window.location = "login.php";
                    } else {
                        alert('Internal error occurred');
                    }
                });
            }
        });
    });
    $('body').on('click', '#notif_footer', function () {
        $('.dropdown-menu > li .fa-spinner').show();

        var formData = {
            'notifIDs': $('.dropdown-menu').data('notifids')
        };
        $.post('../php/suracommands.php?command=update_notifications_insurer&web', formData, function (data) {
            data = JSON.parse(data);

            if (data['info'] === 'SUCCESS') {
                $('.dropdown-menu > li .fa-spinner').hide();
                var notifCount = data['notifCount'];
                if (notifCount === 0) {
                    $('#notifCount').remove();
                } else {
                    $('#notifCount').html(notifCount);
                }
                getNotifications(true);
            } else {
                console.log('Failure');
            }
        });
    });
    $('body').on('submit', '#changeNameForm', (function (event) {
        $('#info').empty();

        $.ajax({
            type: "POST",
            url: "../php/suracommands.php?command=update_insurer_name&web",
            data: new FormData(document.getElementById('changeNameForm')),
            dataType: 'JSON',
            /*** Options to tell JQuery not to process data or worry about content-type ****/
            cache: false,
            contentType: false,
            processData: false,
            /****************************************/
            success: function (data) {
                if (data['info'] === 'SUCCESS') {
                    $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            'Update Successful</div>').slideDown().delay(3000).slideUp();
                    $("#name_change").html('<h4 class="col-sm-5">PENDING APPROVAL...</h4>');
                    setTimeout(function () {
                        $('#info').slideUp().empty().slideDown();
                    }, 4000);
                } else if (data['info'] === 'FAILURE') {
                    $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Update Failed!</strong> Please try again with different entry' +
                            '</div>');
                } else if (data['info'] === 'VALIDATION_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>' + data['errVal']['message'] + '</strong>' +
                            '</div>');
                    $('#' + data['errVal']['field']).removeClass('parsley-success');
                    $('#' + data['errVal']['field']).addClass('parsley-error');
                } else if (data['info'] === 'UPLOAD_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>File upload failed</strong>. Try again later' +
                            '</div>');
                    $("#name_doc").val('');
                } else if (data['info'] === 'FILE_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Please upload a valid file</strong>' +
                            '</div>');
                    $("#name_doc").val('');
                } else if (data['info'] === 'ERORR_OCCURRED') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Error Occurred!</strong> Please try again later' +
                            '</div>');
                }
                $("html, body").animate({scrollTop: 0}, "fast");
            }
        });

        event.preventDefault();
    }));
    $('body').on('submit', '#profileForm', (function (event) {
        $('#info').empty();

        $.ajax({
            type: "POST",
            url: "../php/suracommands.php?command=update_insurer_profile&web",
            data: new FormData(document.getElementById('profileForm')),
            dataType: 'JSON',
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            if (data['info'] === 'SUCCESS') {
                var name = $('input[name=name]').val();
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Update Successful</div>');
                $("#name").html(name);
                $("#ins_name").html(name);
            } else if (data['info'] === 'VALIDATION_ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Fill in appropiate values for all fields'
                        + '</div>');
            } else if (data['info'] === 'NO_EDIT') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Update Failed!</strong> Kindly edit a field and try again' +
                        '</div>');
            } else if (data['info'] === 'FAILURE') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occured!</strong> Try again later' +
                        '</div>');
            } else if (data['info'] === 'ERORR_OCCURRED') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occurred!</strong> Please try again later' +
                        '</div>');
            }
            $("html, body").animate({scrollTop: 0}, "fast");
            setTimeout(function () {
                $('#info').slideUp().empty().slideDown();
            }, 4000);
        }).fail(function (data) {});
        event.preventDefault();
    }));
    $('body').on('submit', '#resetPasswordForm', (function (event) {
        $('#info').empty();
        var formData = {
            'oldPassword': $('input[name=oldPassword]').val(),
            'newPassword': $('input[name=newPassword]').val()
        };
        $.post('../php/suracommands.php?command=update_insurer_password&web', formData, function (data) {
            data = JSON.parse(data);
            if (data['info'] === 'SUCCESS') {
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Update Successful</div>');
            } else if (data['info'] === 'FAILURE') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Update Failed!</strong> Please try again later' +
                        '</div>');
            } else if (data['info'] === 'INCORRECT_PASSWORD') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<strong>Incorrect Password! </strong> Enter correct password or reset password' +
                        '</div>');
                $('input[name=oldPassword]').val('');
                $('input[name=newPassword]').val('');
                $('input[name=confirmPassword]').val('');
            } else if (data['info'] === 'NO_CHANGES') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Update Failed! </strong> Kindly enter a different password' +
                        '</div>');
                $('input[name=newPassword]').val('');
                $('input[name=confirmPassword]').val('');
            } else if (data['info'] === 'VALIDATION_ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Fill in all fields with appropiate values' +
                        '</div>');
            } else if (data['info'] === 'ERORR_OCCURRED') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occurred!</strong> Please try again later' +
                        '</div>');
            }
            $("html, body").animate({scrollTop: 0}, "fast");
        }).fail(function (data) {});
        event.preventDefault();
    }));
    $('body').on('submit', '#resetSQForm', (function (event) {
        $('#info').empty();
        var formData = {
            'question': $('#question').val(),
            'answer': $('input[name=answer]').val()
        };
        $.post('../php/suracommands.php?command=update_insurer_security_question&web', formData, function (data) {
            data = JSON.parse(data);
            if (data['info'] === 'SUCCESS') {
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        'Update Successful</div>');
            } else if (data['info'] === 'FAILURE') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<strong>Update Failed!</strong> Please try again with different entry' +
                        '</div>');
            } else if (data['info'] === 'VALIDATION_ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        'Fill all fields with appropiate values' +
                        '</div>');
            } else if (data['info'] === 'ERORR_OCCURRED') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occurred!</strong> Please try again later' +
                        '</div>');
            }
            $("html, body").animate({scrollTop: 0}, "fast");
        }).fail(function (data) {});
        event.preventDefault();
    }));
    $('body').on('submit', '#uploadDocumentsForm', (function (event) {
        event.preventDefault();
        $('#info').empty();
        $('#documents_save').html('<i class="fa fa-spinner fa-pulse fa-fw"></i> SAVING...');

        $.ajax({
            type: "POST",
            url: "../php/suracommands.php?command=upload_insurer_documents&web",
            data: new FormData(document.getElementById('uploadDocumentsForm')),
            /*** Options to tell JQuery not to process data or worry about content-type ****/
            cache: false,
            contentType: false,
            processData: false,
            /****************************************/
            success: function (data) {
                $('#documents_save').html('<strong>SAVE</strong>');
                if (data['info'] === 'SUCCESS') {
                    $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            'UPLOAD Successful</div>');
                    $('.badge').css('background-color', 'orange');
                    $('.badge').html('PENDING');
                    $('#documents_save').remove();
                    $('#uploadDocumentsForm .settings-input').val('');
                    setTimeout(function () {
                        $('#info').slideUp().empty().slideDown();
                    }, 4000);
                } else if (data['info'] === 'FAILURE') {
                    $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Update Failed!</strong> Please try again with different entry' +
                            '</div>');
                } else if (data['info'] === 'VALIDATION_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>' + data['errVal']['message'] + '</strong>' +
                            '</div>');
                    $('#' + data['errVal']['field']).removeClass('parsley-success');
                    $('#' + data['errVal']['field']).addClass('parsley-error');
                } else if (data['info'] === 'UPLOAD_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>File upload failed</strong>. Try again later' +
                            '</div>');
                } else if (data['info'] === 'FILE_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Please upload a valid file</strong>' +
                            '</div>');
                } else if (data['info'] === 'ERORR_OCCURRED') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Error Occurred!</strong> Please try again later' +
                            '</div>');
                }
                $("html, body").animate({scrollTop: 0}, "fast");
            }
        });


    }));
    $('body').on('click', '.uploadModalTrigger', function () {
        var rowid = $(this).data('rowid');
        $('#uploadUsersForm input[name=rowid]').val(rowid);
        $('.uploadModalTrigger.active').removeClass('active');
        $(this).addClass('active');
    });
    $('body').on('submit', '#uploadUsersForm', (function (event) {
        event.preventDefault();
        $(this).find('button[type=submit]').attr('disabled', 'disabled');

        $.ajax({
            type: "POST",
            url: "../php/suracommands.php?command=upload_insurer_subscribers&web",
            data: new FormData(document.getElementById('uploadUsersForm')),
            /*** Options to tell JQuery not to process data or worry about content-type ****/
            cache: false,
            contentType: false,
            processData: false,
            /****************************************/
            success: function (data) {
                data = JSON.parse(data);
                $('#uploadUsersForm button[type=submit]').attr('disabled', false);
                $('#uploadModal').modal('toggle');
                if (data['status']) {
                    $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            'Upload Successful</div>');
                    $('.uploadModalTrigger.active').parent().html('<i class="fa fa-check" style="color: green"></i>');
                    
                } else {
                    if (data['err'] === 'FAILURE') {
                        $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                '<strong>Update Failed!</strong> Please try again with different entry' +
                                '</div>');
                    } else if (data['err'] === 'VALIDATION_ERROR') {
                        $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                '<strong>' + data['desc']['message'] + '</strong>' +
                                '</div>');
                    } else if (data['err'] === 'UPLOAD_ERROR') {
                        $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                '<strong>File upload failed</strong>. Try again later' +
                                '</div>');
                    } else if (data['err'] === 'FILE_ERROR') {
                        $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                '<strong>Please upload a valid file</strong>' +
                                '</div>');
                    } else if (data['err'] === 'ERORR_OCCURRED') {
                        $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                '<strong>Error Occurred!</strong> Please try again later' +
                                '</div>');
                    }
                }
                $("html, body").animate({scrollTop: 0}, "fast");
            }
        });


    }));
    $('body').on('submit', '#uploadLogoForm', (function (event) {
        $('#info').empty();

        $.ajax({
            type: "POST",
            url: "../php/suracommands.php?command=upload_insurer_logo&web",
            data: new FormData(document.getElementById('uploadLogoForm')),
            dataType: 'JSON',
            /*** Options to tell JQuery not to process data or worry about content-type ****/
            cache: false,
            contentType: false,
            processData: false,
            /****************************************/
            success: function (data) {
                if (data['info'] === 'SUCCESS') {
                    $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            'Update Successful</div>');
                    $('#settings-logo').attr('src', '../img/insurers/' + data.path + '?' + Date.now());
                    $('#header-pic').attr('src', '../img/insurers/' + data.path + '?' + Date.now());
                    setTimeout(function () {
                        $('#info').slideUp().empty().slideDown();
                    }, 4000);
                } else if (data['info'] === 'FAILURE') {
                    $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Update Failed!</strong> Please try again with different entry' +
                            '</div>');
                } else if (data['info'] === 'VALIDATION_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>' + data['errVal']['message'] + '</strong>' +
                            '</div>');
                    $('#' + data['errVal']['field']).removeClass('parsley-success');
                    $('#' + data['errVal']['field']).addClass('parsley-error');
                } else if (data['info'] === 'UPLOAD_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>File upload failed</strong>. Try again later' +
                            '</div>');
                } else if (data['info'] === 'FILE_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Please upload a valid file</strong>' +
                            '</div>');
                } else if (data['info'] === 'ERORR_OCCURRED') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Error Occurred!</strong> Please try again later' +
                            '</div>');
                }
                $("html, body").animate({scrollTop: 0}, "fast");
                setTimeout(function () {
                    $('#info').slideUp().empty().slideDown();
                }, 4000);
            }
        });

        event.preventDefault();
    }));
    $('body').on('submit', '#contactForm', function (event) {
        $('#info').empty();
        $('form i.fa').removeClass('fa-send-o');
        $('form i.fa').addClass('fa-spinner');
        $('form i.fa').addClass('fa-pulse');
        $('form button span').html('On the FLY...');
        var formData = {
            'type': $("#type option:selected").text(),
            'message': $('textarea[name=message]').val(),
            'entity': 'INSURER'
        };
        $.post('../php/suracommands.php?command=send_message&web', formData, function (data) {
            data = JSON.parse(data);
            $('form i.fa').addClass('fa-send-o');
            $('form i.fa').removeClass('fa-spinner');
            $('form i.fa').removeClass('fa-pulse');
            $('form button span').html('SEND');
            if (data['info'] === 'SUCCESS') {
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Message sent succesfully</div>');
                setTimeout(function () {
                    $('#info').empty();
                }, 5000);
                $('textarea[name=message]').val('');
            } else if (data['info'] === 'ERROR_OCCURRED') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error occured! </strong> Please try again later' +
                        '</div>');
            } else if (data['info'] === 'VALIDATION_ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Message more than 1000 characters</strong>' +
                        '</div>');
            }

        }).fail(function (data) {});
        event.preventDefault();
    });
    $('body').on('click', '#new_doc', function (event) {
        $('#uploadDocumentsForm > div.row:last-child').
                before('<div class="row">' +
                        '<div class="col-sm-2"></div>' +
                        '<div class="col-sm-8">' +
                        '<div class="form-group">' +
//                        '<label>Documents</label>'+
                        '<input class="settings-input" name="docs[]" type="file" accept="image/jpeg, image/png, application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document" data-parsley-required data-parsley-trigger="change" data-parsley-filemimetypes="image/jpeg, image/png, application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document"/>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-sm-2" style="margin-top: 10px">' +
                        '<button type="button" class="btn btn-danger btn-rounded btn-sm remove_doc"><i class="fa fa-times"></i></button>' +
                        '</div>' +
                        '</div>');
        event.preventDefault();
    });
    $('body').on('click', '.remove_doc', function (event) {
        $(this).parent().parent().remove()
        event.preventDefault();
    });
    $("#account-nav").click(function () {
        changeSettingsNavLink($(this));
        $('#sub-content').empty();
        $('#profile').show();
    });
    $("#uploaddoc-nav").click(function () {
        changeSettingsNavLink($(this));
        $('#profile').append(spinner.el);
        $('#profile').addClass('overlay');

        $('#sub-content').load('views/_upload_docs.php', function () {
            $('#profile').hide();
            $('#uploadLogoForm').add('#uploadDocumentsForm').parsley();
            $(spinner.el).remove();
            $('#profile').removeClass('overlay');
        });

    });
    $("#setnot-nav").click(function () {
        $('#info').empty();
        changeSettingsNavLink($(this));
        $('#profile').hide();
        $('#sub-content').empty().append('Coming Soon!!!');
    });
    $("#help-nav").click(function () {
        $('#info').empty();
        changeSettingsNavLink($(this));
        $('#profile').hide();
        $('#sub-content').empty().append('Coming Soon!!!');
    });
    $("#password-reset").click(function () {
        $('#info').empty();
        var content = '<div class="reset">' +
                '<form id="resetPasswordForm">' +
                '<div class="row">' +
                '<div class="col-sm-2"></div>' +
                '<div class="col-sm-7">' +
                '<div class="row">' +
                '<label class="col-sm-4" for="oldPassword"><span class="pull-right">Old Password</span> </label>' +
                '<input class="col-sm-8 settings-input" id="oldPassword" name="oldPassword" type="password" data-parsley-required />' +
                '</div>' +
                '<div class="row">' +
                '<label class="col-sm-4" for="newPassword"><span class="pull-right">New Password</span> </label>' +
                '<input class="col-sm-8 settings-input" id="newPassword" name="newPassword" type="password" ' +
                'data-parsley-required data-parsley-minlength="4"/>' +
                '</div>' +
                '<div class="row">' +
                '<label class="col-sm-4" for="confirmPassword"><span class="pull-right">Confirm Password</span> </label>' +
                '<input class="col-sm-8 settings-input" id="confirmPassword" name="confirmPassword" type="password"' +
                'data-parsley-required data-parsley-equalto="#newPassword" data-parsley-equalto-message="Password mismatch" />' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-sm-2">' +
                '<button id="settings-back" type="button" class="btn btn-primary box-shadow--2dp settings-btn btn-rounded btn-lg" name="back"><i class="fa fa-arrow-left"></i></button>' +
                '</div>' +
                '<div class="col-sm-8"></div>' +
                '<div class="col-sm-2">' +
                '<button id="reset_password_save" type="submit" class="btn btn-primary box-shadow--2dp settings-btn btn-rounded" name="save"><strong>SAVE</strong></button>' +
                '</div>' +
                '</div>' +
                '</form>' +
                '</div>';
        $('#profile').hide();
        $('#sub-content').empty().html(content);
        $('#resetPasswordForm').parsley();
    });
    $("#sq-reset").click(function () {
        $('#info').empty();
        var content = '<div class="reset">' +
                '<form id="resetSQForm">' +
                '<div class="row">' +
                '<div class="col-sm-2"></div>' +
                '<div class="col-sm-7">' +
                '<div class="row">' +
                '<label class="col-sm-4" for="question"><span class="pull-right">Security Question</span> </label>' +
                '<select class="col-sm-8" id="question" name="question"  data-parsley-required>' +
                '<option disabled selected>Select security question</option>';
        $.get('../php/suracommands.php?command=get_security_questions', function (data) {
            data = JSON.parse(data);
            for (var i = 0; i < data.length; i++)
                content += "<option value = '" + data[i]['question'] + "'>" + data[i]['question'] + '</option>';
            content += '</select>' +
                    '</div>' +
                    '<div class="row">' +
                    '<label class="col-sm-4" for="answer"><span class="pull-right">Security Answer</span> </label>' +
                    '<input class="col-sm-8 settings-input" id="answer" name="answer" type="text" data-parsley-required/>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="row">' +
                    '<div class="col-sm-2">' +
                    '<button id="settings-back" type="button" class="btn btn-primary box-shadow--2dp settings-btn btn-rounded btn-lg" name="back"><i class="fa fa-arrow-left"></i></button>' +
                    '</div>' +
                    '<div class="col-sm-8"></div>' +
                    '<div class="col-sm-2">' +
                    '<button id="reset_password_save" type="submit" class="btn btn-primary box-shadow--2dp settings-btn btn-rounded" name="save"><strong>SAVE</strong></button>' +
                    '</div>' +
                    '</div>' +
                    '</form>' +
                    '</div>';
            $('#profile').hide();
            $('#sub-content').empty().html(content);
            $('#resetSQForm').parsley();
        });
    });
    $('body').on('click', '#settings-back', function () {
        $('#info').empty();
        $('#sub-content').empty();
        $('#profile').fadeIn('slow');
    });
    $('body').on('click', '#load-new', function () {
        if (isItemRemaining === true) {
            offset += limit;
            getRecentActivity(limit, offset);
            isEmpty = false;
        }
    });
    /********** Miscellaneous ***********************/

    function changeSettingsNavLink(e) {
        e.css("background-color", "#72be58");
        e.css("color", "white");
        e.find("img").attr("src", "../img/icons/left_arrow_white.png");
        e.hover(function () {
            e.css("background-color", "#72be58");
        },
                function () {
                    e.css("background-color", "#72be58");
                });
        var temp = e.index();
        $('#settings-nav p').each(function (i) {
            if (i !== temp) {
                $(this).css("background-color", "white");
                $(this).css("color", "black");
                $(this).find("img").attr("src", "../img/icons/left_arrow_grey.png");
                $(this).hover(function () {
                    $(this).css("background-color", "#dadada");
                },
                        function () {
                            $(this).css("background-color", "white");
                        });
            }
        });
    }

    /*************************************/

    function toMonth($num, $type) {
        switch ($num) {
            case '1':
                return $type === 'long' ? 'January' : 'Jan';
            case '2':
                return $type === 'long' ? 'February' : 'Feb';
            case '3':
                return $type === 'long' ? 'March' : 'Mar';
            case '4':
                return $type === 'long' ? 'April' : 'Apr';
            case '5':
                return $type === 'long' ? 'May' : 'May';
            case '6':
                return $type === 'long' ? 'June' : 'Jun';
            case '7':
                return $type === 'long' ? 'July' : 'Jul';
            case '8':
                return $type === 'long' ? 'August' : 'Aug';
            case '9':
                return $type === 'long' ? 'September' : 'Sep';
            case '10':
                return $type === 'long' ? 'October' : 'Oct';
            case '11':
                return $type === 'long' ? 'November' : 'Nov';
            case '12':
                return $type === 'long' ? 'December' : 'Dec';
            default:
                return "Incorrect Month Value";
        }
    }

    function displayQueryData(info, limit, offset) {
        var content = '';

        $.get('../php/suracommands.php?command=query_users&' + info + '&limit=' + limit + '&offset=' + offset, function (data) {
            var data = JSON.parse(data);

            if (data.length > 0) {
                content = '<table class="table table-striped">' +
                        '<thead>' +
                        '<tr><th>Full Name</th>' +
                        '<th>General iPoints</th>' +
                        '<th>Health iPoints</th>' +
                        '<th></th></tr>' +
                        '</thead>' +
                        '<tbody>';
                $('#pagination').show();
                var userids = '';

                for (var i = 0; i < data.length; i++) {
                    var name = data[i]['f_name'] + ' ' + data[i]['l_name'];
                    if (i === 0) {
                        userids += data[i]['id'];
                    } else {
                        userids += (',' + data[i]['id']);
                    }
                    var element = name;
                    var notif = '<i class="fa fa-envelope-o" style="pointer: cursor" data-userid = "' + data[i]['id'] + '"></i>';

                    content += '<tr>' +
                            '<td>' + element + '</td>' +
                            '<td>' + data[i]['general_point_balance'] + '</td>' +
                            '<td>' + data[i]['health_point_balance'] + '</td>' +
                            '<td>' + notif + '</td>' +
                            '</tr>';
                }
                content += '</tbody>' +
                        '</table>';
                $('#query_display > div:nth-child(1)').html(content);
                $('#messageAll').data('userids', userids);
                $('#messageAll').removeClass('disabled');
            } else {
                $('#pagination').hide();
                content += '<span class="text-muted">No query data...</span>';
                $('#query_display > div').html(content);
            }
        });
    }

    function getNotifications(flag) {
        var content = '';
        var notifIDs = '';
        if (flag === true) {
            $.get('../php/suracommands.php?command=get_notifications_insurer&limit=10&web', function (data) {
                var notifs = JSON.parse(data);
                var notifCount = notifs.length;

                if (notifCount > 0) {
                    for (var i = 0; i < notifCount; i++) {
                        i === 0 ? '' : notifIDs += ',';
                        notifIDs += notifs[i]['id'];
                        content += '<li class="divider notif-temp"></li>';
                        content += '<li class="text-center notif-temp">' + notifs[i]['message'] + '</li>';
                    }

                    content += '<li class="divider notif-temp" id="last_divider"></li>';
                    content += '<li id="notif_footer" class="text-center notif-temp"><strong>Mark as Read</strong></li>';
                } else {
                    content += '<li class="divider"> </li>';
                    content += '<li id="no_new_notif" class="text-center">No new notifications</li>';
                }

            }).done(function () {
                $('.dropdown-menu > li .fa-spinner').hide();
                $('.notif-temp').remove();
                $('.dropdown-menu').append(content);
                $('.dropdown-menu').data('notifids', notifIDs);
                $('.dropdown-menu #notif_header').css('padding-bottom', '0');
                $('.dropdown-menu .divider:nth-child(2)').css('margin-top', '5px');
            });
        } else {
            content += '<li class="divider"> </li>';
            content += '<li id="no_new_notif" class="text-center">No new notifications</li>';
            $('.dropdown-menu > li .fa-spinner').hide();
            $('.dropdown-menu').append(content);
            $('.dropdown-menu #notif_header').css('padding-bottom', '0');
            $('.dropdown-menu .divider:nth-child(2)').css('margin-top', '5px');
        }

    }

    function getRecentActivity(limit, offset) {
        var content = '';
        $('#south-content > div').append(spinner.el);
        $('#south-content > div').addClass('overlay');

        $.get('../php/suracommands.php?command=get_recent_activity_insurer&limit=' + limit + '&offset=' + offset + '&web', function (data) {
            var logInfo = JSON.parse(data);
            var newDateDay = null;
            var newDateMonth = null;
            var newDateYear = null;

            if (logInfo.length > 0) {
                isItemRemaining = true;

                for (var i = 0; i < logInfo.length; i++) {
                    var ref = logInfo[i]['ref'] !== null ? logInfo[i]['ref'][0] : '';
                    var _date = new Date(logInfo[i]['date']);
                    var dateDay = _date.getDate().toString();
                    var dateMonth = toMonth((_date.getMonth() + 1).toString());
                    var dateYear = _date.getFullYear().toString();
                    var action = logInfo[i]['action'];
                    var iconColor = null;
                    var heading = null;
                    var info = null;

                    if (action === 'CREATE_POLICY') {
                        heading = 'POLICY CREATED';
                        iconColor = 'bg-success';
                        info = "You created a policy titled '" + ref['title'] + "' worth " + ref['price'] + ' points';
                    } else if (action === 'EDIT_POLICY') {
                        heading = 'POLICY EDITED';
                        iconColor = 'bg-warning';
                        info = "You edited a policy titled '" + ref['title'] + "'";
                    } else if (action === 'DEACTIVATE_POLICY') {
                        heading = 'POLICY DEACTIVATED';
                        iconColor = 'bg-danger';
                        info = "You deactivated a policy titled '" + ref['title'] + "'";
                    } else if (action === 'EDIT_COMPANY_NAME') {
                        heading = 'CHANGE OF NAME REQUEST';
                        iconColor = 'bg-info';
                        info = 'You requested for a change of company name';
                    } else if (action === 'EDIT_PROFILE') {
                        heading = 'PROFILE EDIT';
                        iconColor = 'bg-info';
                        info = 'You edited your profile information';
                    } else if (action === 'CHANGE_PASSWORD') {
                        heading = 'PASSWORD CHANGE';
                        iconColor = 'bg-info';
                        info = 'You changed the password details';
                    } else if (action === 'CHANGE_SEC_QUE') {
                        heading = 'SECURITY QUESTION CHANGE';
                        iconColor = 'bg-info';
                        info = 'You edited the security question information';
                    } else if (action === 'CHANGE_LOGO') {
                        heading = 'LOGO CHANGE';
                        iconColor = 'bg-info';
                        info = 'You changed the company logo';
                    } else if (action === 'NAMEDOC_VALIDATED') {
                        heading = 'CHANGE OF NAME APPROVED';
                        iconColor = 'bg-info';
                        info = 'The change of company name request has been approved';
                    } else if (action === 'UPLOAD_DOCUMENTS') {
                        heading = 'OFFICIAL DOCUMENTS UPLOAD';
                        iconColor = 'bg-info';
                        info = 'The official company documents has been uploaded';
                    } else if (action === 'DOCUMENTS_VALIDATED') {
                        heading = 'OFFICIAL DOCUMENTS VALIDATED';
                        iconColor = 'bg-info';
                        info = 'The official company documents has been validated';
                    } else if (action === 'SEND_MESSAGE') {
                        heading = 'MESSAGE SENT';
                        iconColor = 'bg-info';
                        info = 'You sent a message to the administrator. Awaiting response...';
                    } else if (action === 'SENT_NOTIFICATION') {
                        heading = 'SENT NOTIFICATION';
                        iconColor = 'bg-info';
                        info = 'You sent notifcations to user(s).';
                    } else if (action === 'USER_SIGNUP') {
                        heading = 'POLICY PURCHASE';
                        iconColor = 'bg-success';
                        info = "A user purchased '" + ref['title'] + "' worth " + ref['price'] + ' points';
                    } else if (action === 'POLICY_RENEW') {
                        heading = 'POLICY RENEW';
                        iconColor = 'bg-success';
                        info = "A user renewed '" + ref['title'] + "' policy worth " + ref['price'] + ' points';
                    } else if (action === 'BOUGHT_SMS_POINTS') {
                        heading = 'BOUGHT SMS POINTS';
                        iconColor = 'bg-info';
                        info = "You bought '" + logInfo[i]['info'] + "' point-worth of SMS";
                    } else if (action === 'ADMIN_DISAPPROVE') {
                        heading = 'ADMIN BAN';
                        iconColor = 'bg-danger';
                        info = "You were banned from accessing the platform by UICI administrator";
                    } else if (action === 'ADMIN_APPROVE') {
                        heading = 'ADMIN UNBAN';
                        iconColor = 'bg-info';
                        info = "You were un-banned from accessing the platform by UICI administrator";
                    } else if (action === 'ADMIN_EDIT') {
                        heading = 'ADMIN EDIT';
                        iconColor = 'bg-info';
                        info = "Your profile was edited by UICI administrator";
                    } else if (action === 'ADMIN_VALIDATE') {
                        heading = 'ADMIN VALIDATE';
                        iconColor = 'bg-info';
                        info = "Your documents and account has been validated by UICI administrator";
                    } else if (action === 'ADMIN_INVALIDATE') {
                        heading = 'ADMIN INVALIDATE';
                        iconColor = 'bg-danger';
                        info = "Your account has been invalidated by UICI administrator";
                    } else if (action === 'ADMIN_NAME_APPROVE') {
                        heading = 'ADMIN NAME CHANGE';
                        iconColor = 'bg-info';
                        info = "Your proposed name have been approved by UICI administrator";
                    } else if (action === 'UPLOAD_SUBSCRIBERS') {
                        heading = 'RE-UPLOADED ENROLLED SUBSCRIBERS';
                        iconColor = 'bg-info';
                        info = "You have successfully downloaded and made an upload of enrolled subscribers to the system. Updates will be made shortly.";
                    }

                    if (dateDay !== newDateDay || dateMonth !== newDateMonth || dateYear !== newDateYear) {
                        if (i !== 0) {
                            content += '</div>' +
                                    '</div>' +
                                    '</div>';
                        }
                        newDateDay = dateDay;
                        newDateMonth = dateMonth;
                        newDateYear = dateYear;
                        content += '<div class="row">'
                                + '<div class="col-xs-1">' +
                                '<div id="date" class="text-center pull-right">' +
                                '<span id="month">' + dateYear + '</span>' +
                                '<br>' +
                                '<span id="day">' + dateMonth + ' ' + dateDay + '</span>' +
                                '</div>' +
                                '</div>' +
                                '<div class="col-xs-11">' +
                                '<div class="timeline-centered">';
                    }
                    content += '<article class="timeline-entry">' +
                            '<div class="timeline-entry-inner">' +
                            '<div class="timeline-icon ' + iconColor + '"></div>' +
                            '<div class="timeline-label">' +
                            '<h2>' + heading + '</h2>' +
                            '<p>' + info + '</p>' +
                            '</div>' +
                            '</div>' +
                            '</article>';
                }


                content += '<article id="plus" class="timeline-entry begin">' +
                        '<div class="timeline-entry-inner">' +
                        '<div id="load-new" class="timeline-icon" style="-webkit-transform: rotate(-90deg); -moz-transform: rotate(-90deg); cursor:pointer;">' +
                        '<i class="fa fa-plus"></i>' +
                        '</div>' +
                        ' </div>' +
                        '</article>';
                content += '</div>' +
                        '</div>' +
                        '</div>';


            } else {
                isItemRemaining = false;
                if (isEmpty) {
                    content = '<p style="padding: 50px;" class="text-center text-muted text-italic">No Recent Activity</p>';
                }
            }
        }).done(function () {
            $(spinner.el).remove();
            $('#south-content > div').removeClass('overlay');
            if (isItemRemaining || isEmpty) {
                $('#south-content > div').empty().append(content);
            }
        });
    }

    function getPolicies(limit, offset) {

        var content = '';
        $('#sub-content').append(spinner.el);
        $('#sub-content').addClass('overlay');
        $.get('../php/suracommands.php?command=get_insurer_policies&limit=' + limit + '&offset=' + offset + '&web', function (data) {
            var policyInfo = JSON.parse(data);
            if (policyInfo.length > 0) {
                isItemRemaining = true;
                if ($('#custom-table').is(":hidden")) {
                    $('#no-policies').remove();
                    $('#custom-table').show();
                }
                for (var i = 0; i < policyInfo.length; i++) {
                    var id = i + 1;
                    var policyid = policyInfo[i]['id'];
                    var desc = policyInfo[i]['description'];
                    var title = policyInfo[i]['title'];
                    var price = policyInfo[i]['price'];
                    var tenure = policyInfo[i]['tenure_info'][0]['in_months'];
                    var editBtn = '<button id="editBtn" title="Edit" class ="btn btn-circular box-shadow--2dp btn-info"  data-policyid="' + policyid + '"><i class="fa fa-pencil"></i></button>';
                    var deleteBtn = '<button data-toggle="modal" title="De-activate" data-target="#continue" id="deactivateBtn" class ="btn box-shadow--2dp btn-circular btn-danger"  data-policyid="' + policyid + '"><i class="fa fa-remove"></i></button>';
                    content += '<tr>';
                    content += '<td class="text-center">' + id + '</td>';
                    content += '<td class="text-center">' + title + '</td>';
                    content += '<td class="text-center">' + desc + '</td>';
                    content += '<td class="text-center">' + price + '</td>';
                    content += '<td class="text-center">' + tenure + ' month(s)</td>';
                    content += '<td>' + editBtn + '</td>';
                    content += '<td>' + deleteBtn + '</td>';
                    content += '</tr>';
                }
            } else {
                $('#custom-table').hide();
                content += '<p id="no-policies" style="padding: 50px;" class="text-center text-muted text-italic">No Policies published</p>';
                isItemRemaining = false;
            }
        }).done(function () {
            if ($('#custom-table').is(":visible")) {
                $(spinner.el).remove();
                $('#sub-content').removeClass('overlay');
                $('#custom-table tbody').html(content);
            } else {
                $(spinner.el).remove();
                $('#sub-content').removeClass('overlay');
                $('#sub-content').append(content);
            }
        });
    }

    function getTotalItems(info) {
        var api = '';
        if (filename === 'policies')
            api = '../php/suracommands.php?command=' + info + '&web';
        else if (filename === 'marketing')
            api = '../php/suracommands.php?command=query_users&' + info;

        $.get(api, function (data) {
            var info = JSON.parse(data);
            totalItems = info.length;
        }).done(function () {
            $('#pagination').pagination({
                items: totalItems,
                itemsOnPage: NoOfItemsOnDisplay,
                cssStyle: 'light-theme',
                edges: 4,
                onPageClick: function (pageNumber) {
                    if (filename === 'policies') {
                        getPolicies(NoOfItemsOnDisplay, NoOfItemsOnDisplay * (pageNumber - 1));
                    } else if (filename === 'marketing')
                        displayQueryData(info, NoOfItemsOnDisplay, NoOfItemsOnDisplay * (pageNumber - 1));
                }
            });
            if (filename === 'policies') {
                getPolicies(NoOfItemsOnDisplay, 0);
            } else if (filename === 'marketing')
                displayQueryData(info, NoOfItemsOnDisplay, 0);

        });
    }

    function getSummaryData() {
        $('.tab-pane.active').append(spinner.el);
        $('.tab-content').addClass('overlay');

        var content = '';
        var count;

        var api = '../php/suracommands.php?command=get_pie_chart_data_insurer&web';
        $.get(api, '', function (data) {
            data = JSON.parse(data);
            count = data[data.length - 1]['count'];
            for (var i = 0; i < data.length - 1; i++) {
                var title = data[i]['title'];
                var num_users = data[i]['num_users'];

                content += '<tr style="font-weight:bold; border-bottom:1px solid #e0e0e0;">';
                content += '<td class="text-center">' + (i + 1) + '</td>';
                content += '<td class="text-center">' + title + '</td>';
                content += '<td class="text-center">' + num_users + '</td>';
                content += '</tr>';
            }
            content += '<tr style="font-weight:bold; ">';
            content += '<td class="text-center"></td>';
            content += '<td class="text-center text-italic" style="font-weight: bold">Total</td>';
            content += '<td class="text-center text-italic" >' + count + '</td>';
            content += '</tr>';
        }).done(function () {
            $(spinner.el).remove();
            $('.tab-content').removeClass('overlay');
            $('#table').show();
            $('tbody').html(content);
            $("#num_range_b").ionRangeSlider({
                type: "double",
                grid: true,
                min: 0,
                max: count,
                from: 0,
                to: 0
            });
            $("#num_range_l").ionRangeSlider({
                type: "double",
                grid: true,
                min: 0,
                max: count,
                from: 0,
                to: 0
            });
            drawPieChart(null);
            drawLineChart(null);
        });
    }

    function drawPieChart(filterData) {
        if (myPieChart !== null) {
            myPieChart.destroy();
        }

        var dataset = {
            labels: [],
            datasets: [
                {
                    label: "TeamA Score",
                    data: [],
                    backgroundColor: [],
                    borderWidth: [1, 1, 1, 1, 1]
                }
            ]
        };

        var api = '../php/suracommands.php?command=get_pie_chart_data_insurer&web';
        $.get(api, filterData, function (data) {
            data = JSON.parse(data);
            count = data[data.length - 1]['count'];
            for (var i = 0; i < data.length - 1; i++) {
                dataset.labels.push(data[i]['title']);
                dataset.datasets[0].data.push(data[i]['num_users']);
                dataset.datasets[0].backgroundColor.push(getRandomColor());
            }
        }).done(function () {
            var ctx = document.getElementById("myPieChart");
            myPieChart = new Chart(ctx, {
                type: 'pie',
                data: dataset,
                options: {
                    responsive: true,
                    cutoutPercentage: 30,
                    title: {
                        display: true,
                        text: 'Number of monthly user signups per policy',
                        position: 'bottom',
                        fullWidth: true,
                        fontSize: 14,
                        fontColor: '#1c4b70'
                    },
                    legend: {
                        display: true,
                        position: "right",
                        labels: {
                            fontColor: "#333",
                            fontSize: 16
                        }
                    }
                }
            });
        });
    }

    function drawLineChart(filterData) {
        if (myLineChart !== null) {
            myLineChart.destroy();
        }
        var dataset = [];

        var api = '../php/suracommands.php?command=get_line_chart_data_insurer&web';
        $.get(api, filterData, function (data) {
            data = JSON.parse(data);
            var count = data[data.length - 1]['total_signups'];
//            console.log(count);
            for (var i = 0; i < data.length - 1; i++) {
                var tempLine = {
                    label: null,
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    backgroundColor: null
                };
                var signups = data[i]['signups'];
                tempLine.label = data[i]['title'];
                for (var j = 0; j < signups.length; j++) {
                    var month_val = new Date(signups[j]['date_signed']).getMonth();
                    tempLine.data[month_val]++;
                    tempLine.backgroundColor = getRandomColor();
                }
                dataset.push(tempLine);
            }

        }).done(function () {
            var ctx = document.getElementById("myLineChart");
            myLineChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                    datasets: dataset
                },
                options: {
                    scales: {
                        yAxes: [{
//                                stacked: true,
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    },
                    title: {
                        display: true,
                        text: 'Number of monthly user signups per policy',
                        position: 'bottom',
                        fullWidth: true,
                        fontSize: 14,
                        fontColor: '#1c4b70'
                    },
                    legend: {
                        display: true,
                        position: 'bottom',
                        labels: {
                            fontColor: 'rgb(255, 99, 132)'
                        }
                    },
                    responsiveAnimationDuration: 500
                }
            });
        });
    }

    function getRandomColor() {
//        var letters = '0123456789ABCDEF'.split('');
//        var color = '#';
//        for (var i = 0; i < 6; i++) {
//            color += letters[Math.floor(Math.random() * 16)];
//        }

        var color = 'rgba(';
        for (var i = 0; i < 3; i++) {
            i > 0 ? color += ',' : '';
            color += Math.floor(Math.random() * 255);
        }
        color += ',0.5)';
        return color;
    }

    /////////////////////////////////////////////////////

    $("#buySMSForm").parsley();

    var pay_rate = parseFloat($("input[id=pay_rate]").val());

    $('body').on('input', 'input[name=pay_points]', function (event) {
        var amt = parseFloat($(this).val());
        $(this).next().children('span').html(amt * pay_rate);
    });

    $('body').on('submit', '#buySMSForm', function (e) {
        e.preventDefault();

        if (parseFloat($('input[name=pay_points]').val()) > 0) {
            $('#smsModal').modal('hide');
            payWithPaystack();
        }
    });

    function payWithPaystack() {
        var points = parseFloat($("input[name=pay_points]").val());
        var amt = points * pay_rate * 100;
        var ref = 'UICI-' + $("input[id=pay_id]").val() + moment().unix();

        var payload = {
            'ref': ref,
            'amount': points
        };

        var handler = PaystackPop.setup({
            key: 'pk_test_bb835543a20ab7491632daf4aecffde31b029f40',
            email: $("input[name=pay_email]").val(),
            amount: amt,
            ref: ref,
            callback: function (response) {
                if (response.reference) {
                    $.post('../php/suracommands.php?command=buy_sms_points_card&web', payload, function (data) {
                        var info = '';
                        if (data.status) {
                            info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                                    'SMS Points Purchased successfully</div>';
                            $('#sms_wallet').html(data.points);
                        } else {
                            if (data.info === 'ERROR') {
                                info = '<div class="alert alert-dismissible alert-danger text-center btn-rounded">' +
                                        'SMS Points Purchase failed</div>';
                            }
                        }

                        $('#sms_info').html(info).stop().slideDown().delay(3000).slideUp();
                    }, 'JSON').fail(function () {});
                } else {
                    alert('naah');
                }
            },
            onClose: function () {}
        });
        handler.openIframe();
    }


});



