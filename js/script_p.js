/*jslint browser: true*/
/*global $, Spinner*/
$(document).ready(function () {
    /**************  Variables  ******************/
    var NoOfItemsOnDisplay = null;
    var limit = null;
    var offset = null;
    var totalItems = null;
    var isItemRemaining = true;
    var isEmpty = true; // to check whether there are elements at all...
    var myPieChart = null;
    var myLineChart = null;

    /************ For pagination *********************/
    var filenameWithExtension = location.pathname.split('/').slice(-1)[0];
    var filename = filenameWithExtension.split(".")[0];

    /****** For notifications *****************/
    $(".dropdown-toggle").hover(function () {
        $('#notifCount').removeClass('animated');
        $('#notifCount').removeClass('swing');
        $('#notifCount').removeClass('infinite');
    }, function () {
        $('#notifCount').addClass('animated');
        $('#notifCount').addClass('swing');
        $('#notifCount').addClass('infinite');
    });
    if ($('.dropdown-menu').data('notifcount') === 0) {
        getNotifications(false);
    } else {
        getNotifications(true);
    }

    /*********** For Pop-over *********/
    $('#point_info').popover();

    /*********** Tab Active Selection *********/
    if (filename === 'home') {
        $('ul.navbar-nav > li:nth-child(1)').addClass('active');
        limit = 5;
        offset = 0;

        getRecentActivity(limit, offset);
        drawLineChart(null);
        $('.input-daterange input').each(function () {
            $(this).datepicker({
                format: "dd/mm/yyyy",
                clearBtn: true,
                autoclose: true
            });
        });

    } else if (filename === 'settings') {
        changeSettingsNavLink($('#account-nav'));
        $('#profileForm').parsley();
        $('#changeNameForm').parsley();
    }

    /****************** Policies Event Handlers *****************/

    $('body').on('submit', '#lineChartFilter', function (event) {
        $('#info').empty();
        var formData = {
            'gender_l': $("#gender_l option:selected").val(),
            'startDate_l': $('#startDate_l').val(),
            'endDate_l': $('#endDate_l').val(),
            'active_l': $("#active_l option:selected").val(),
        };

//        console.log(JSON.stringify(formData) + "--- " + $('#lineChartFilter').serialize());
        drawLineChart($('#lineChartFilter').serialize());
        event.preventDefault();
    });
    $('body').on('click', '#query_display .row > div .fa-envelope-o', function (event) {
        $('.modal #info').empty();
        $('#notifMessage').val('');
        $('.modal').data('userids', $(this).data('userid'));
        $('#messageModal').modal();
        event.preventDefault();
    });
    $('body').on('click', '#messageAll', function (event) {
        $('.modal #info').empty();
        $('#notifMessage').val('');
        $('.modal').data('userids', $(this).data('userids'));
        event.preventDefault();
    });
    $('body').on('submit', '#buySMSForm', function (event) {
        $('#btn_buy_sms').addClass('disabled');

        var formData = {
            'amount': $('input[name=amount]').val()
        };
        $.post('../php/suracommands.php?command=buy_sms_credit&web', formData, function (data) {
            console.log(data);
            data = JSON.parse(data);
            $('#btn_buy_sms').removeClass('disabled');
            if (data['info'] === 'SUCCESS') {
                $('#sms_info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'SMS bought successful</div>');
                $('#sms_wallet').html(data['points']);
            } else if (data['info'] === 'VALIDATION_ERROR') {
                $('#sms_info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Notification message more than 200 characters' +
                        '</div>');
            } else if (data['info'] === 'ERORR_OCCURRED') {
                $('#sms_info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occurred!</strong> Please try again later' +
                        '</div>');
            } else if (data['info'] === 'INSUFFICIENT_POINTS') {
                $('#sms_info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Insufficient Points</strong>. Buy more points' +
                        '</div>');
            }
            $("html, body").animate({scrollTop: 0}, "fast");
        }).fail(function (data) {});

        event.preventDefault();
    });
    $('body').on('submit', '#messageForm', function (event) {
        $('#info').empty();
        $('#notifSend').addClass('disabled');
        var formData = {
            'message': $('textarea[name=notifMessage]').val(),
            'userids': $('.modal').data('userids')
        };
        $.post('../php/suracommands.php?command=send_notification&web', formData, function (data) {
            console.log(data);
            data = JSON.parse(data);
            $('#notifSend').removeClass('disabled');
            if (data['info'] === 'SUCCESS') {
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Notification sent Successful</div>');
                $('#sms_wallet').html(data['points']);
            } else if (data['info'] === 'VALIDATION_ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Notification message more than 200 characters' +
                        '</div>');
            } else if (data['info'] === 'ERORR_OCCURRED') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occurred!</strong> Please try again later' +
                        '</div>');
            }
            $("html, body").animate({scrollTop: 0}, "fast");
        }).fail(function (data) {});
        event.preventDefault();
    });

    /****************** Settings Event Handlers *****************/
    $('.logout').click(function () {
        $.get('../php/suracommands.php?command=logout_pension&web', function (data) {
            data = JSON.parse(data);

            if (data === 'SUCCESS') {
                window.location = "login.php";
            } else {
                alert('Internal error occurred');
            }
        });
    });
    $('body').on('click', '#notif_footer', function () {
        $('.dropdown-menu > li .fa-spinner').show();

        var formData = {
            'notifIDs': $('.dropdown-menu').data('notifids')
        };
        $.post('../php/suracommands.php?command=update_notifications_pension&web', formData, function (data) {
            data = JSON.parse(data);

            if (data['info'] === 'SUCCESS') {
                $('.dropdown-menu > li .fa-spinner').hide();
                var notifCount = data['notifCount'];
                if (notifCount === 0) {
                    $('#notifCount').remove();
                } else {
                    $('#notifCount').html(notifCount);
                }
                getNotifications(true);
            } else {
                console.log('Failure');
            }
        });
    });
    $('body').on('submit', '#changeNameForm', function (event) {
        $('#info').empty();

        $.ajax({
            type: "POST",
            url: "../php/suracommands.php?command=update_pension_name&web",
            data: new FormData(document.getElementById('changeNameForm')),
            dataType: 'JSON',
            /*** Options to tell JQuery not to process data or worry about content-type ****/
            cache: false,
            contentType: false,
            processData: false,
            /****************************************/
            success: function (data) {
                if (data['info'] === 'SUCCESS') {
                    $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            'Update Successful</div>');
                    $("#name_change").html('<h4 class="col-sm-5">PENDING APPROVAL...</h4>');

                    setTimeout(function () {
                        $('#info').empty();
                    }, 5000);
                } else if (data['info'] === 'FAILURE') {
                    $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Update Failed!</strong> Please try again with different entry' +
                            '</div>');
                } else if (data['info'] === 'VALIDATION_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>' + data['errVal']['message'] + '</strong>' +
                            '</div>');
                    $('#' + data['errVal']['field']).removeClass('parsley-success');
                    $('#' + data['errVal']['field']).addClass('parsley-error');
                } else if (data['info'] === 'UPLOAD_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>File upload failed</strong>. Try again later' +
                            '</div>');
                    $("#name_doc").val('');
                } else if (data['info'] === 'FILE_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Please upload a valid file</strong>' +
                            '</div>');
                    $("#name_doc").val('');
                } else if (data['info'] === 'ERORR_OCCURRED') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Error Occurred!</strong> Please try again later' +
                            '</div>');
                }
                $("html, body").animate({scrollTop: 0}, "fast");
            }
        });

        event.preventDefault();
    });
    $('body').on('submit', '#profileForm', function (event) {
        $('#info').empty();

        $.ajax({
            type: "POST",
            url: "../php/suracommands.php?command=update_pension_profile&web",
            data: new FormData(document.getElementById('profileForm')),
            dataType: 'JSON',
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            if (data['info'] === 'SUCCESS') {
                var name = $('input[name=name]').val();
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Update Successful</div>');
                $("#name").html(name);
                $("#ins_name").html(name);
            } else if (data['info'] === 'VALIDATION_ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Fill in appropiate values for all fields'
                        + '</div>');
            } else if (data['info'] === 'NO_EDIT') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Update Failed!</strong> Kindly edit a field and try again' +
                        '</div>');
            } else if (data['info'] === 'FAILURE') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occured!</strong> Try again later' +
                        '</div>');
            } else if (data['info'] === 'ERORR_OCCURRED') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occurred!</strong> Please try again later' +
                        '</div>');
            }
            $("html, body").animate({scrollTop: 0}, "fast");
        }).fail(function (data) {});
        event.preventDefault();
    });
    $('body').on('submit', '#resetPasswordForm', function (event) {
        $('#info').empty();
        var formData = {
            'oldPassword': $('input[name=oldPassword]').val(),
            'newPassword': $('input[name=newPassword]').val()
        };
        $.post('../php/suracommands.php?command=update_pension_password&web', formData, function (data) {
            data = JSON.parse(data);
            if (data['info'] === 'SUCCESS') {
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Update Successful</div>');
            } else if (data['info'] === 'FAILURE') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Update Failed!</strong> Please try again later' +
                        '</div>');
            } else if (data['info'] === 'INCORRECT_PASSWORD') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<strong>Incorrect Password! </strong> Enter correct password or reset password' +
                        '</div>');
                $('input[name=oldPassword]').val('');
                $('input[name=newPassword]').val('');
                $('input[name=confirmPassword]').val('');
            } else if (data['info'] === 'NO_CHANGES') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Update Failed! </strong> Kindly enter a different password' +
                        '</div>');
                $('input[name=newPassword]').val('');
                $('input[name=confirmPassword]').val('');
            } else if (data['info'] === 'VALIDATION_ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        'Fill in all fields with appropiate values' +
                        '</div>');
            } else if (data['info'] === 'ERORR_OCCURRED') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occurred!</strong> Please try again later' +
                        '</div>');
            }
            $("html, body").animate({scrollTop: 0}, "fast");
        }).fail(function (data) {});
        event.preventDefault();
    });
    $('body').on('submit', '#resetSQForm', function (event) {
        $('#info').empty();
        var formData = {
            'question': $('#question').val(),
            'answer': $('input[name=answer]').val()
        };
        $.post('../php/suracommands.php?command=update_pension_security_question&web', formData, function (data) {
            data = JSON.parse(data);
            if (data['info'] === 'SUCCESS') {
                $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                        'Update Successful</div>');
            } else if (data['info'] === 'FAILURE') {
                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<strong>Update Failed!</strong> Please try again with different entry' +
                        '</div>');
            } else if (data['info'] === 'VALIDATION_ERROR') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        'Fill all fields with appropiate values' +
                        '</div>');
            } else if (data['info'] === 'ERORR_OCCURRED') {
                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occurred!</strong> Please try again later' +
                        '</div>');
            }
            $("html, body").animate({scrollTop: 0}, "fast");
        }).fail(function (data) {});
        event.preventDefault();
    });
    $('body').on('submit', '#uploadDocumentsForm', function (event) {
        $('#info').empty();
        $('#documents_save').html('<i class="fa fa-spinner fa-pulse fa-fw"></i> SAVING...');

        $.ajax({
            type: "POST",
            url: "../php/suracommands.php?command=upload_pension_documents&web",
            data: new FormData(document.getElementById('uploadDocumentsForm')),
            dataType: 'JSON',
            /*** Options to tell JQuery not to process data or worry about content-type ****/
            cache: false,
            contentType: false,
            processData: false,
            /****************************************/
            success: function (data) {
                $('#documents_save').html('<strong>SAVE</strong>');
                if (data['info'] === 'SUCCESS') {
                    $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            'UPLOAD Successful</div>');
                    $('.badge').css('background-color', 'orange');
                    $('.badge').html('PENDING');
                    $('#documents_save').remove();
                    $('#uploadDocumentsForm .settings-input').val('');
                } else if (data['info'] === 'FAILURE') {
                    $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Update Failed!</strong> Please try again with different entry' +
                            '</div>');
                } else if (data['info'] === 'VALIDATION_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>' + data['errVal']['message'] + '</strong>' +
                            '</div>');
                    $('#' + data['errVal']['field']).removeClass('parsley-success');
                    $('#' + data['errVal']['field']).addClass('parsley-error');
                } else if (data['info'] === 'UPLOAD_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>File upload failed</strong>. Try again later' +
                            '</div>');
                } else if (data['info'] === 'FILE_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Please upload a valid file</strong>' +
                            '</div>');
                } else if (data['info'] === 'ERORR_OCCURRED') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Error Occurred!</strong> Please try again later' +
                            '</div>');
                }
                $("html, body").animate({scrollTop: 0}, "fast");
            }
        });

        event.preventDefault();
    });
    $('body').on('submit', '#uploadLogoForm', function (event) {
        $('#info').empty();

        $.ajax({
            type: "POST",
            url: "../php/suracommands.php?command=upload_pension_logo&web",
            data: new FormData(document.getElementById('uploadLogoForm')),
            dataType: 'JSON',
            /*** Options to tell JQuery not to process data or worry about content-type ****/
            cache: false,
            contentType: false,
            processData: false,
            /****************************************/
            success: function (data) {
                if (data['info'] === 'SUCCESS') {
                    $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            'Update Successful</div>');
                    $('#settings-logo').attr('src', '../img/logos/' + data.path + '?' + Date.now());
                    $('#header-logo').attr('src', '../img/logos/' + data.path + '?' + Date.now());
                    $('input[name=logo]').val('');
                } else if (data['info'] === 'FAILURE') {
                    $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Update Failed!</strong> Please try again with different entry' +
                            '</div>');
                } else if (data['info'] === 'VALIDATION_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>' + data['errVal']['message'] + '</strong>' +
                            '</div>');
                    $('#' + data['errVal']['field']).removeClass('parsley-success');
                    $('#' + data['errVal']['field']).addClass('parsley-error');
                } else if (data['info'] === 'UPLOAD_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>File upload failed</strong>. Try again later' +
                            '</div>');
                } else if (data['info'] === 'FILE_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Please upload a valid file</strong>' +
                            '</div>');
                } else if (data['info'] === 'ERORR_OCCURRED') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Error Occurred!</strong> Please try again later' +
                            '</div>');
                }
                $("html, body").animate({scrollTop: 0}, "fast");
            }
        });

        event.preventDefault();
    });
    $('body').on('click', '#new_doc', function (event) {
        $('#uploadDocumentsForm > div.row:last-child').
                before('<div class="row">' +
                        '<div class="col-sm-2"></div>' +
                        '<div class="col-sm-8">' +
                        '<div class="form-group">' +
//                        '<label>Documents</label>'+
                        '<input class="settings-input" name="docs[]" type="file" accept="image/jpeg, image/png, application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document" data-parsley-required data-parsley-trigger="change" data-parsley-filemimetypes="image/jpeg, image/png, application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document"/>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-sm-2" style="margin-top: 10px">' +
                        '<button type="button" class="btn btn-danger btn-rounded btn-sm remove_doc"><i class="fa fa-times"></i></button>' +
                        '</div>' +
                        '</div>');
        event.preventDefault();
    });
    $('body').on('click', '.remove_doc', function (event) {
        $(this).parent().parent().remove()
        event.preventDefault();
    });

    $("#account-nav").click(function () {
        changeSettingsNavLink($(this));
        $('#sub-content').empty();
        $('#profile').show();
    });
    $("#uploaddoc-nav").click(function () {
        changeSettingsNavLink($(this));
        $('#profile').append(spinner.el);
        $('#profile').addClass('overlay');
        $('#uploadDocumentsForm').parsley();
        $('#sub-content').load('views/_upload_docs.php', function () {
            $('#profile').hide();
            $('#uploadLogoForm').add('#uploadDocumentsForm').parsley();
            $(spinner.el).remove();
            $('#profile').removeClass('overlay');
        });

    });
    $("#setnot-nav").click(function () {
        $('#info').empty();
        changeSettingsNavLink($(this));
        $('#profile').hide();
        $('#sub-content').empty().append('Coming Soon!!!');
    });
    $("#help-nav").click(function () {
        $('#info').empty();
        changeSettingsNavLink($(this));
        $('#profile').hide();
        $('#sub-content').empty().append('Coming Soon!!!');
    });
    $("#password-reset").click(function () {
        $('#info').empty();
        var content = '<div class="reset">' +
                '<form id="resetPasswordForm">' +
                '<div class="row">' +
                '<div class="col-sm-2"></div>' +
                '<div class="col-sm-7">' +
                '<div class="row">' +
                '<label class="col-sm-4" for="oldPassword"><span class="pull-right">Old Password</span> </label>' +
                '<input class="col-sm-8 settings-input" id="oldPassword" name="oldPassword" type="password" data-parsley-required />' +
                '</div>' +
                '<div class="row">' +
                '<label class="col-sm-4" for="newPassword"><span class="pull-right">New Password</span> </label>' +
                '<input class="col-sm-8 settings-input" id="newPassword" name="newPassword" type="password" ' +
                'data-parsley-required data-parsley-minlength="4"/>' +
                '</div>' +
                '<div class="row">' +
                '<label class="col-sm-4" for="confirmPassword"><span class="pull-right">Confirm Password</span> </label>' +
                '<input class="col-sm-8 settings-input" id="confirmPassword" name="confirmPassword" type="password"' +
                'data-parsley-required data-parsley-equalto="#newPassword" data-parsley-equalto-message="Password mismatch" />' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-sm-2">' +
                '<button id="settings-back" type="button" class="btn btn-primary box-shadow--2dp settings-btn btn-rounded btn-lg" name="back"><i class="fa fa-arrow-left"></i></button>' +
                '</div>' +
                '<div class="col-sm-8"></div>' +
                '<div class="col-sm-2">' +
                '<button id="reset_password_save" type="submit" class="btn btn-primary box-shadow--2dp settings-btn btn-rounded" name="save"><strong>SAVE</strong></button>' +
                '</div>' +
                '</div>' +
                '</form>' +
                '</div>';
        $('#profile').hide();
        $('#sub-content').empty().html(content);
        $('#resetPasswordForm').parsley();
    });
    $("#sq-reset").click(function () {
        $('#info').empty();
        var content = '<div class="reset">' +
                '<form id="resetSQForm">' +
                '<div class="row">' +
                '<div class="col-sm-2"></div>' +
                '<div class="col-sm-7">' +
                '<div class="row">' +
                '<label class="col-sm-4" for="question"><span class="pull-right">Security Question</span> </label>' +
                '<select class="col-sm-8" id="question" name="question"  data-parsley-required>' +
                '<option disabled selected>Select security question</option>';
        $.get('../php/suracommands.php?command=get_security_questions', function (data) {
            data = JSON.parse(data);
            for (var i = 0; i < data.length; i++)
                content += "<option value = '" + data[i]['question'] + "'>" + data[i]['question'] + '</option>';
            content += '</select>' +
                    '</div>' +
                    '<div class="row">' +
                    '<label class="col-sm-4" for="answer"><span class="pull-right">Security Answer</span> </label>' +
                    '<input class="col-sm-8 settings-input" id="answer" name="answer" type="text" data-parsley-required/>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="row">' +
                    '<div class="col-sm-2">' +
                    '<button id="settings-back" type="button" class="btn btn-primary box-shadow--2dp settings-btn btn-rounded btn-lg" name="back"><i class="fa fa-arrow-left"></i></button>' +
                    '</div>' +
                    '<div class="col-sm-8"></div>' +
                    '<div class="col-sm-2">' +
                    '<button id="reset_password_save" type="submit" class="btn btn-primary box-shadow--2dp settings-btn btn-rounded" name="save"><strong>SAVE</strong></button>' +
                    '</div>' +
                    '</div>' +
                    '</form>' +
                    '</div>';
            $('#profile').hide();
            $('#sub-content').empty().html(content);
            $('#resetSQForm').parsley();
        });
    });
    $('body').on('click', '#settings-back', function () {
        $('#info').empty();
        $('#sub-content').empty();
        $('#profile').fadeIn('slow');
    });
    $('body').on('click', '#load-new', function () {
        if (isItemRemaining === true) {
            offset += limit;
            getRecentActivity(limit, offset);
            isEmpty = false;
        }
    });
    /********** Miscellaneous ***********************/

    function changeSettingsNavLink(e) {
        e.css("background-color", "#265a88");
        e.css("color", "white");
        e.find("img").attr("src", "../img/icons/left_arrow_white.png");
        e.hover(function () {
            e.css("background-color", "#265a88");
        },
                function () {
                    e.css("background-color", "#265a88");
                });
        var temp = e.index();
        $('#settings-nav p').each(function (i) {
            if (i !== temp) {
                $(this).css("background-color", "white");
                $(this).css("color", "black");
                $(this).find("img").attr("src", "../img/icons/left_arrow_grey.png");
                $(this).hover(function () {
                    $(this).css("background-color", "#dadada");
                },
                        function () {
                            $(this).css("background-color", "white");
                        });
            }
        });
    }

    /*************************************/

    function toMonth($num, $type) {
        switch ($num) {
            case '1':
                return $type === 'long' ? 'January' : 'Jan';
            case '2':
                return $type === 'long' ? 'February' : 'Feb';
            case '3':
                return $type === 'long' ? 'March' : 'Mar';
            case '4':
                return $type === 'long' ? 'April' : 'Apr';
            case '5':
                return $type === 'long' ? 'May' : 'May';
            case '6':
                return $type === 'long' ? 'June' : 'Jun';
            case '7':
                return $type === 'long' ? 'July' : 'Jul';
            case '8':
                return $type === 'long' ? 'August' : 'Aug';
            case '9':
                return $type === 'long' ? 'September' : 'Sep';
            case '10':
                return $type === 'long' ? 'October' : 'Oct';
            case '11':
                return $type === 'long' ? 'November' : 'Nov';
            case '12':
                return $type === 'long' ? 'December' : 'Dec';
            default:
                return "Incorrect Month Value";
        }
    }

    function getNotifications(flag) {
        var content = '';
        var notifIDs = '';
        if (flag === true) {
            $.get('../php/suracommands.php?command=get_notifications_pension&limit=10&web', function (data) {
                var notifs = JSON.parse(data);
                var notifCount = notifs.length;

                if (notifCount > 0) {
                    for (var i = 0; i < notifCount; i++) {
                        i === 0 ? '' : notifIDs += ',';
                        notifIDs += notifs[i]['id'];
                        content += '<li class="divider notif-temp"></li>';
                        content += '<li class="text-center notif-temp">' + notifs[i]['message'] + '</li>';
                    }

                    content += '<li class="divider notif-temp" id="last_divider"></li>';
                    content += '<li id="notif_footer" class="text-center notif-temp">Mark as Read</li>';
                } else {
                    content += '<li class="divider"> </li>';
                    content += '<li id="no_new_notif" class="text-center">No new notifications</li>';
                }

            }).done(function () {
                $('.dropdown-menu > li .fa-spinner').hide();
                $('.notif-temp').remove();
                $('.dropdown-menu').append(content);
                $('.dropdown-menu').data('notifids', notifIDs);
                $('.dropdown-menu #notif_header').css('padding-bottom', '0');
                $('.dropdown-menu .divider:nth-child(2)').css('margin-top', '5px');
            });
        } else {
            content += '<li class="divider"> </li>';
            content += '<li id="no_new_notif" class="text-center">No new notifications</li>';
            $('.dropdown-menu > li .fa-spinner').hide();
            $('.dropdown-menu').append(content);
            $('.dropdown-menu #notif_header').css('padding-bottom', '0');
            $('.dropdown-menu .divider:nth-child(2)').css('margin-top', '5px');
        }

    }

    function getRecentActivity(limit, offset) {
        var content = '';
        $('#south-content > div').append(spinner.el);
        $('#south-content > div').addClass('overlay');

        $.get('../php/suracommands.php?command=get_recent_activity_pension&limit=' + limit + '&offset=' + offset + '&web', function (data) {
            var logInfo = JSON.parse(data);
            var newDateDay = null;
            var newDateMonth = null;
            var newDateYear = null;

            if (logInfo.length > 0) {
                isItemRemaining = true;

                for (var i = 0; i < logInfo.length; i++) {
                    var ref = logInfo[i]['ref'] !== null ? logInfo[i]['ref'][0] : '';
                    var _date = new Date(logInfo[i]['date']);
                    var dateDay = _date.getDate().toString();
                    var dateMonth = toMonth((_date.getMonth() + 1).toString());
                    var dateYear = _date.getFullYear().toString();
                    var action = logInfo[i]['action'];
                    var iconColor = null;
                    var heading = null;
                    var info = null;

                    if (action === 'EDIT_COMPANY_NAME') {
                        heading = 'CHANGE OF NAME REQUEST';
                        iconColor = 'bg-info';
                        info = 'You requested for a change of company name';
                    } else if (action === 'EDIT_PROFILE') {
                        heading = 'PROFILE EDIT';
                        iconColor = 'bg-info';
                        info = 'You edited your profile information';
                    } else if (action === 'CHANGE_PASSWORD') {
                        heading = 'PASSWORD CHANGE';
                        iconColor = 'bg-info';
                        info = 'You changed the password details';
                    } else if (action === 'CHANGE_SEC_QUE') {
                        heading = 'SECURITY QUESTION CHANGE';
                        iconColor = 'bg-info';
                        info = 'You edited the security question information';
                    } else if (action === 'CHANGE_LOGO') {
                        heading = 'LOGO CHANGE';
                        iconColor = 'bg-info';
                        info = 'You changed the company logo';
                    } else if (action === 'NAMEDOC_VALIDATED') {
                        heading = 'CHANGE OF NAME APPROVED';
                        iconColor = 'bg-info';
                        info = 'The change of company name request has been approved';
                    } else if (action === 'UPLOAD_DOCUMENTS') {
                        heading = 'OFFICIAL DOCUMENTS UPLOAD';
                        iconColor = 'bg-info';
                        info = 'The official company documents has been uploaded';
                    } else if (action === 'DOCUMENTS_VALIDATED') {
                        heading = 'OFFICIAL DOCUMENTS VALIDATED';
                        iconColor = 'bg-info';
                        info = 'The official company documents has been validated';
                    } else if (action === 'PENSION_SIGNUP') {
                        heading = 'USER SIGNUP';
                        iconColor = 'bg-secondary';
                        info = "A user '" + ref['f_name'] + ' ' + ref['l_name'] + "' (" + ref['phone'] + ") signed up";
                    } else if (action === 'PENSION_TOPUP') {
                        heading = 'PENSION TOPUP';
                        iconColor = 'bg-success';
                        info = "A user, " + ref['user'][0]['f_name'] + ' ' + ref['user'][0]['l_name'] + " (" + ref['user'][0]['phone'] + "), topped up pension by " + ref['point_worth'] + ' points';
                    }  else if (action === 'ADMIN_DISAPPROVE') {
                        heading = 'ADMIN BAN';
                        iconColor = 'bg-danger';
                        info = "You were banned from accessing the platform by UICI administrator";
                    } else if (action === 'ADMIN_APPROVE') {
                        heading = 'ADMIN UNBAN';
                        iconColor = 'bg-info';
                        info = "You were un-banned from accessing the platform by UICI administrator";
                    } else if (action === 'ADMIN_EDIT') {
                        heading = 'ADMIN EDIT';
                        iconColor = 'bg-info';
                        info = "Your profile was edited by UICI administrator";
                    } else if (action === 'ADMIN_VALIDATE') {
                        heading = 'ADMIN VALIDATE';
                        iconColor = 'bg-info';
                        info = "Your documents and account has been validated by UICI administrator";
                    } else if (action === 'ADMIN_INVALIDATE') {
                        heading = 'ADMIN INVALIDATE';
                        iconColor = 'bg-danger';
                        info = "Your account has been invalidated by UICI administrator";
                    } else if (action === 'ADMIN_NAME_APPROVE') {
                        heading = 'ADMIN NAME CHANGE';
                        iconColor = 'bg-info';
                        info = "Your proposed name have been approved by UICI administrator";
                    } 

                    if (dateDay !== newDateDay || dateMonth !== newDateMonth || dateYear !== newDateYear) {
                        if (i !== 0) {
                            content += '</div>' +
                                    '</div>' +
                                    '</div>';
                        }
                        newDateDay = dateDay;
                        newDateMonth = dateMonth;
                        newDateYear = dateYear;
                        content += '<div class="row">'
                                + '<div class="col-xs-1">' +
                                '<div id="date" class="text-center pull-right">' +
                                '<span id="month">' + dateYear + '</span>' +
                                '<br>' +
                                '<span id="month">' + dateMonth + '</span>' +
                                '<br>' +
                                '<span id="day">' + dateDay + '</span>' +
                                '</div>' +
                                '</div>' +
                                '<div class="col-xs-11">' +
                                '<div class="timeline-centered">';
                    }
                    content += '<article class="timeline-entry">' +
                            '<div class="timeline-entry-inner">' +
                            '<div class="timeline-icon ' + iconColor + '"></div>' +
                            '<div class="timeline-label">' +
                            '<h2>' + heading + '</h2>' +
                            '<p>' + info + '</p>' +
                            '</div>' +
                            '</div>' +
                            '</article>';
                }


                content += '<article id="plus" class="timeline-entry begin">' +
                        '<div class="timeline-entry-inner">' +
                        '<div id="load-new" class="timeline-icon" style="-webkit-transform: rotate(-90deg); -moz-transform: rotate(-90deg); cursor:pointer;">' +
                        '<i class="fa fa-plus"></i>' +
                        '</div>' +
                        ' </div>' +
                        '</article>';
                content += '</div>' +
                        '</div>' +
                        '</div>';


            } else {
                isItemRemaining = false;
                if (isEmpty) {
                    content = '<p style="padding: 50px;" class="text-center text-muted text-italic">No Recent Activity</p>';
                }
            }
        }).done(function () {
            $(spinner.el).remove();
            $('#south-content > div').removeClass('overlay');
            if (isItemRemaining || isEmpty) {
                $('#south-content > div').empty().append(content);
            }
        });
    }

    function getTotalItems(info) {
        var api = '';
        if (filename === 'policies')
            api = '../php/suracommands.php?command=' + info + '&web';

        $.get(api, function (data) {
            var info = JSON.parse(data);
            totalItems = info.length;
        }).done(function () {
            $('#pagination').pagination({
                items: totalItems,
                itemsOnPage: NoOfItemsOnDisplay,
                cssStyle: 'light-theme',
                edges: 4,
                onPageClick: function (pageNumber) {
                    if (filename === 'policies') {
                        getPolicies(NoOfItemsOnDisplay, NoOfItemsOnDisplay * (pageNumber - 1));
                    } else if (filename === 'query')
                        displayQueryData(info, NoOfItemsOnDisplay, NoOfItemsOnDisplay * (pageNumber - 1));
                }
            });
            if (filename === 'policies') {
                getPolicies(NoOfItemsOnDisplay, 0);
            }

        });
    }

    function drawLineChart(filterData) {
        $('#north-content > .content-white').append(spinner.el);
        $('#north-content > .content-white').addClass('overlay');

        if (myLineChart !== null) {
            myLineChart.destroy();
        }
        var dataset = [];
        var count = null;
        console.log(filterData);
        var api = '../php/suracommands.php?command=get_line_chart_data_pension&web';
        $.get(api, filterData, function (data) {
            data = JSON.parse(data);
            count = data[0]['total_signups'];

            var tempLine = {
                label: null,
                data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                backgroundColor: null
            };
            var signups = data[0]['signups'];
            tempLine.label = 'No of sign-ups';
            for (var i = 0; i < signups.length; i++) {
                var month_val = new Date(signups[i]['date_signed']).getMonth();
                tempLine.data[month_val]++;
                tempLine.backgroundColor = getRandomColor();
            }
            dataset.push(tempLine);

        }).done(function () {
            $("#num_range_l").ionRangeSlider({
                type: "double",
                grid: true,
                min: 0,
                max: count,
                from: 0,
                to: 0
            });
            /////////////////
            var ctx = document.getElementById("myLineChart");
            myLineChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                    datasets: dataset
                },
                options: {
                    scales: {
                        yAxes: [{
//                                stacked: true,
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    },
                    title: {
                        display: true,
                        text: 'Number of monthly user signups per policy',
                        position: 'bottom',
                        fullWidth: true,
                        fontSize: 14,
                        fontColor: '#1c4b70'
                    },
                    legend: {
                        display: false,
                    },
                    responsiveAnimationDuration: 500
                }
            });
            $(spinner.el).remove();
            $('#north-content > .content-white').removeClass('overlay');
        });
    }

    function getRandomColor() {
        var color = 'rgba(';
        for (var i = 0; i < 3; i++) {
            i > 0 ? color += ',' : '';
            color += Math.floor(Math.random() * 255);
        }
        color += ',0.5)';
        return color;
    }

});



