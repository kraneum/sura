var opts = {
        lines: 11 // The number of lines to draw
        , length: 15 // The length of each line
        , width: 2 // The line thickness
        , radius: 25 // The radius of the inner circle
        , scale: 1.5 // Scales overall size of the spinner
        , corners: 1 // Corner roundness (0..1)
        , color: '#12a8e9' // #rgb or #rrggbb or array of colors
        , opacity: 0.1 // Opacity of the lines
        , rotate: 0 // The rotation offset
        , direction: 1 // 1: clockwise, -1: counterclockwise
        , speed: 1 // Rounds per second
        , trail: 40 // Afterglow percentage
        , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
        , className: 'spinner' // The CSS class to assign to the spinner
        , top: '50%' // Top position relative to parent
        , left: '50%' // Left position relative to parent
        , shadow: false // Whether to render a shadow
        , hwaccel: false // Whether to use hardware acceleration
        , position: 'absolute' // Element positioning
    };
    var spinner = typeof Spinner !== 'undefined' && new Spinner(opts).spin();

