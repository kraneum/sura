'use strict';

//exports
var working_alert = {};

(function () {

    var globvars = {
        ongoingCt: 0
    };

    working_alert.start = function () {
        if (!$('#workingAlert').length) {
            $('body').append('<div id="workingAlert" style="position: fixed; width: 100px; z-index: 990; top: 0px; left: 50%; box-shadow: rgba(0, 0, 0, 0.2) 0px 2px 4px; height: 30px; border-color: rgb(240, 195, 109); text-align: center; background-color: rgb(249, 237, 190); color: rgb(51, 51, 51); font-weight: bold; display: none;padding-top:5px;">Working...</div>');
        }
        globvars.ongoingCt++ || $('#workingAlert').show();
    };
    working_alert.finish = function () {
        --globvars.ongoingCt || $('#workingAlert').fadeOut();
    };
})();