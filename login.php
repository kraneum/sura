<?php
if (isset($_SESSION['verify_otp'])) {
    session_destroy();
}
if (isset($_SESSION['entity'])) {
    header('Location: home.php');
}

session_start();
?>

<!DOCTYPE html>

<html>
    <head>
        <title>iInsurance</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="img/favicon.ico" />
        <link rel="icon" type="image/png" href="img/favicon.ico">
        <link rel="apple-touch-icon" href="img/favicon.ico">

        <link href="css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/parsley.css" rel="stylesheet" type="text/css"/>
        <link href="css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/formstyles.css" rel="stylesheet" type="text/css"/>
        <link href="css/style_login.css" rel="stylesheet" type="text/css"/>
        <style type="text/css">

            @font-face {
                font-family: 'Avenir';
                src: url('fonts/AvenirLTStd-Book.woff2') format('woff2'), /* Super Modern Browsers */
                    url('fonts/AvenirLTStd-Book.woff') format('woff'), /* Pretty Modern Browsers */
                    url('fonts/AvenirLTStd-Book.ttf') format('truetype'); /* Safari, Android, iOS */
            }
            body {
                font-family: "Avenir", sans-serif;
            }
        </style>

    </head>
    <body>

        <div class="container">

            <div class="form">

                <div class="login-img"><img src="img/logos/uic2.png" class="img-responsive" /></div>
                <div class="alert alert-danger alert-dismissable" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong id="info"></strong>
                </div>

                <form class="login-form" id="loginForm" method="post" action="#">
                    <div class="form-group">
                        <input type="text" id="id" name="id" value="09057183473" class="input_" placeholder="Email Address / Phone Number" data-parsley-required/>
                        <input type="password" placeholder="Password" id="password" class="input_" name="password" value="aaaa" maxlength="12" data-parsley-required />
                        <a href="php/forgotpasswrd.php?entity=user" id="lnkforget">Forgot? </a>

                    </div>
                    <div id="license" style="margin-bottom: 20px;">By clicking Sign In, you agree to our<a href=""> License Agreement</a></div>
                    <button class="btn btn-success signinbtn" type="submit" name="btn-login">Sign In</button>

                </form>



            </div>
            <p class="text-center">Don't have an account? <a href="register.php" style="color: #000000; font-weight: bold;">Sign Up</a></p>

        </div>

        <script src="js/jquery-3.1.1.js" type="text/javascript"></script>
        <script src="js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="js/parsley.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>

        <!-- Login Form -->
        <script type="text/javascript">
            $(document).ready(function () {
                $('#loginForm').parsley();

                $('body').on('submit', '#loginForm', (function (event) {
                    $('#btn-login').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
                    $('#info').empty();
                    $('.alert').hide();
                    var formData = {
                        'id': $('input[name=id]').val(),
                        'password': $('input[name=password]').val()
                    };
                    $.post('./php/suracommands.php?command=validate_user_credentials', formData, function (data) {
                        data = JSON.parse(data);

                        if (data['status']) {
                            if (data['err'] === 'UNAPPROVED') {
                                window.location.href = "php/reverify.php";
                            } else {
                                window.location.href = "home.php";
                            }
                        } else {
                            $('#btn-login').html('LOG IN');
                            $('.alert').css("display", "block");
                            if (data['err'] === 'INVALID') {
                                $('#info').append('Incorrect login credentials');
                                $('input[name=id]').addClass('parsley-error');
                                $('input[name=password]').addClass('parsley-error');
                            } else if (data['err'] === 'NO_EMAIL') {
                                $('#info').append('Please enter username');
                                $('input[name=id]').addClass('parsley-error');
                            } else if (data['err'] === 'NO_PASSWORD') {
                                $('#info').append('Please enter password');
                                $('input[name=password]').addClass('parsley-error');
                            } else if (data['err'] === 'DATABASE_CONNECT_ERROR') {
                                $('#info').append('Internal error occurred');
                            }
                        }
                    });

                    event.preventDefault();
                }));

            });
        </script>
    </body>
</html>

