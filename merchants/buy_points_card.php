<?php

session_start();

require_once('../php/sura_config.php');
require_once('../php/sura_functions.php');
//require_once 'send_sms.php';

$con = makeConnection_merchant();

if ($con) {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if (!$_POST) {
            disconnectConnection($con);
            echo json_encode(['status' => false, 'err' => 'INVALID_DETAILS']);
            exit();
        }

        $id = $_SESSION['id'];
        $merchant_id = $_SESSION['company_id'];
        $ref = $_POST['ref'];
        $amount = $_POST['amount'];


        // retrieves merchant info...
        $merchantInfo = runSimpleFetchQuery($con, ['id', 'name', 'point_balance'], 'merchants', ['id'], ['='], ["'$merchant_id'"], '', '', '')['result'];
        if (empty($merchantInfo)) {
            disconnectConnection($con);
            echo json_encode(['status' => false, 'info' => 'MERCHANT_ID_NOT_RESOLVED']);
            exit();
        }
        $merchant_name = $merchantInfo[0]['name'];
        $point_balance = $merchantInfo[0]['point_balance'];

        // retrieves employee info...
        $selfInfo = runSimpleFetchQuery($con, ['id', 'email'], 'employees', ['id'], ['='], [$id], '', '', '')['result'];
        if (empty($selfInfo)) {
            disconnectConnection($con);
            echo json_encode(['status' => false, 'info' => 'EMPLOYEE_ID_NOT_RESOLVED']);
            exit();
        }

        /////////////////////////////////////////////////////

        autoCommit($con, false);
        $flag = true;
        $date = (new DateTime())->format(DateTime::ISO8601);

        $new_point_balance = $point_balance + $amount;

        do {
            $result = runSimpleUpdateQuery($con, 'merchants', ['point_balance'], ["'$new_point_balance'"], ['id'], ['='], ["'$merchant_id'"]);
            if ($result['err']['code']) {
                $flag = false;
                break;
            }

            $result = runSimpleInsertQuery($con, 'point_purchases', ['reference', 'point_amount', 'previous_balance', 'date_bought', 'merchant_id', 'type_id'], ["'$ref'", "'$amount'", "'$point_balance'", "'$date'", "'$merchant_id'", '11']);
            if ($result['err']['code']) {
                $flag = false;
                break;
            }
            $insertedID = $result['insertedId'];

            $result = runSimpleInsertQuery($con, 'merchants_recent_activity', ['activity_summary', 'datetimezone', 'activity_type', 'activity_id', 'merchant_id'], ["'" . json_encode(['user' => $_SESSION['email'], 'points' => $amount]) . "'", "'$date'", '"POINTS_PURCHASE"', "'$insertedID'", $merchant_id]);
            if ($result['err']['code']) {
                $flag = false;
            }
        } while (false);

        ///////////////////////////////////////////////

        if ($flag) {
            commit($con);
            $message['status'] = true;
            $message['info'] = 'SUCCESS';
            $message['points'] = $new_point_balance;
            $_SESSION['point_balance'] = $new_point_balance;
//            require_once '../php/sms/sms_sender.php';
//            sendSms($rec_name, 'You were transferred ' . $amount . ' points from a Merchant' . ' - ' . $merchant_name, "$rec_number");
        } else {
            rollback($con);
            $message['status'] = false;
            $message['info'] = 'ERROR';
        }
    } else {
        $message['status'] = false;
        $message['err'] = 'WRONG_REQUEST_TYPE';
    }
} else {
    $message['status'] = false;
    $message['err'] = 'DATABASE_CONNECT_ERROR';
}

autoCommit($con, true);

disconnectConnection($con);

echo json_encode($message, JSON_PRETTY_PRINT);
