<?php
exit('');
session_start();

if (!isset($_SESSION['id'])) {
    exit();
}
//To prevent sql injection
//htmlspecialchars(strip_tags(trim())));
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (count($data = json_decode(file_get_contents("php://input"), JSON_OBJECT_AS_ARRAY))) {
        $cleanedUserInputMap = array_filter(
                array_map(function($value) {
                    return htmlspecialchars(strip_tags(trim($value)));
                }, $data), function($value, $key){
                    //Remove unwanted or injected inputs
                    return strlen($value) && in_array($key, ['old_password','password', 'password_confirm']);
                }, ARRAY_FILTER_USE_BOTH);
                
        if (count($cleanedUserInputMap)) {
            require_once '../php/form_validate.php';
            $response = [];

            $validationResult = $form_validate([
                'old_password' => 'required|maxlength:20',
                'password' => 'required|maxlength:20',
                'password_confirm' => 'required|equalsToField:password|maxlength:20'
            ], $cleanedUserInputMap);

            if (empty($validationResult)) {
                require_once '../php/sura_config.php';
                require_once '../php/sura_functions.php';
                
                //BCYRPT THESE PASSWORDS
                $old_password = $cleanedUserInputMap['old_password'];
                $password = $cleanedUserInputMap['password'];

                $updatePasswdResult = runUpdatePasswdQuery(makeConnection_merchant(), $_SESSION['id'], $old_password, $password);

                if (!$updatePasswdResult['err']['code']) {
                    if ($updatePasswdResult['result']) {
                        $response['result'] = true;
                        $response['err'] = null;
                    } else {
                        $response['result'] = null;
                        $response['err'] = ['error' => 'NOCHANGES', 'msg' => 'No changes'];
                    }
                } else {
                    $response['result'] = null;
                    $response['err'] = ['error' => 'DB', 'msg' => 'An error occured, please try again'];
                }
            } else {
                $response['result'] = null;
                $response['err'] = ['error' => 'VALIDATION', 'msg' => $validationResult];
            }

            echo json_encode($response);
        }
    }
}