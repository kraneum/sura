<?php
exit('');
session_start();

if (!isset($_SESSION['id'])) {
    exit();
}
//To prevent sql injection
//htmlspecialchars(strip_tags(trim()));
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (count($data = json_decode(file_get_contents("php://input"), JSON_OBJECT_AS_ARRAY))) {
        $cleanedUserInputMap = array_filter(
                array_map(function($value) {
                    return htmlspecialchars(strip_tags((trim($value))));
                }, $data), function($value, $key) {
            return strlen($value) && in_array($key, ['security_question', 'security_answer']);
        }, ARRAY_FILTER_USE_BOTH);

        if (count($cleanedUserInputMap)) {
            require_once '../php/form_validate.php';
            $response = [];

            $validationResult = $form_validate([
                'security_question' => 'required|maxlength:200',
                'security_answer' => 'required|maxlength:100'
            ], $cleanedUserInputMap);

            if (empty($validationResult)) {
                require_once '../php/sura_config.php';
                require_once '../php/sura_functions.php';

                $columns = array_keys($cleanedUserInputMap);
                $values = array_map(function($value) {
                    return '"' . $value . '"';
                }, array_values($cleanedUserInputMap));

                $updateEmployeeResult = runSimpleUpdateQuery(makeConnection_merchant(), 'employees', $columns, $values, ['id'], ['='], [$_SESSION['id']], 1);

                if (!$updateEmployeeResult['err']['code']) {
                    if ($updateEmployeeResult['result']) {
                        $response['result'] = true;
                        $response['err'] = null;
                    } else {
                        $response['result'] = null;
                        $response['err'] = ['error' => 'NOCHANGES', 'msg' => 'No changes'];
                    }
                } else {
                    $response['result'] = null;
                    $response['err'] = ['error' => 'DB', 'msg' => 'An error occured, please try again'];
                }
            } else {
                $response['result'] = null;
                $response['err'] = ['error' => 'VALIDATION', 'msg' => $validationResult];
            }

            echo json_encode($response);
        }
    }
}