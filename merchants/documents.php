<?php
session_start();

if (!isset($_SESSION['id'])) {
    header("Location: login.php?goto=" . urlencode($_SERVER['REQUEST_URI']));
}

if (!empty($_SESSION['approved'])) {
    header("Location: " . $_SERVER['HTTP_REFERER']);
}

require_once './views/documents.php';
?>