<?php

session_start();

if (!isset($_SESSION['id']) || $_SESSION['approved']) {
    exit();
}

//To prevent sql injection
//htmlspecialchars(strip_tags(trim()));
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    require_once '../php/form_validate.php';
    require_once '../php/sura_config.php';
    require_once '../php/sura_functions.php';

    $response = ['err' => null, 'result' => false];
    $company_id = filter_var($_SESSION['company_id'], FILTER_VALIDATE_INT, array('min_range' => 0));

    $con = makeConnection_merchant();

    $merchantAprvdDetailsResult = runSimpleFetchQuery($con, ['approved', 'documents'], 'merchants', ['id'], ['='], [$company_id], '', '', 1);
//0-not uploaded,1-approved,2-not approved/pending,3-rejected
    if (!$merchantAprvdDetailsResult['err']['code']) {
        if (!$merchantAprvdDetailsResult['result'][0]['approved']) {
            $documentsMap = [];
            $emptyDocs = 0;
            $documentsApprvdDetails = json_decode($merchantAprvdDetailsResult['result'][0]['documents'], JSON_OBJECT_AS_ARRAY);
            foreach (array_filter($documentsApprvdDetails, function($stat) {
                return !$stat || +$stat === 3;
            }) as $documnt => $aprvdStat) {
                ($_FILES[$documnt]['size'] && $documentsMap[$documnt] = $_FILES[$documnt]) || ( ++$emptyDocs);
            }

            if (!empty($documentsMap)) {
                $validationResult = $form_validate(array_map(function() {
                            return 'filemaxmegabytes:8|filemimetypes:image/jpeg,image/jpg,image/png,application/pdf,application/x-pdf,application/acrobat,applications/vnd.pdf,text/pdf,text/x-pdf';
                        }, $documentsMap)
                        , $documentsMap);

                if (empty($validationResult)) {
                    require_once '../php/mime_content_type.php';

                    $mimeTransTbl = ["image/jpeg" => 'jpg', "image/jpg" => 'jpg', "image/png" => 'png', "application/pdf" => 'pdf', "application/x-pdf" => 'pdf', "application/acrobat" => 'pdf', "applications/vnd.pdf" => 'pdf', "text/pdf" => 'pdf', "text/x-pdf" => 'pdf'];
                    $dir = '../merchant_docs/docs_id_' . $company_id . '/';
                    if (!file_exists($dir)) {
                        mkdir($dir, 0777, true);
                    }
                    $ls = scandir($dir);
                    if (!$u_array_walk($documentsMap, function($document, $name)use($mimeTransTbl, $ls, $dir) {
                                foreach ($ls as $lsdoc) {
                                    if (!strncmp($lsdoc, $name, strlen($name))) {
                                        unlink($dir . $lsdoc);
                                        break;
                                    }
                                }
                                if (move_uploaded_file($document["tmp_name"], $dir . $name . '.' . $mimeTransTbl[mime_content_type($document["tmp_name"])])) {
                                    return false;
                                } else {
                                    return true;
                                }
                            })) {
                        $newDocumentsApprvdDetails = array_replace($documentsApprvdDetails, array_map(function() {
                                    //0-not uploaded,1-approved,2-not approved/pending,3-rejected
                                    return 2;
                                }, $documentsMap));

                        $updateEmployeeResult = runSimpleUpdateQuery($con, 'merchants', ['documents'], ["'" . json_encode($newDocumentsApprvdDetails) . "'"], ['id'], ['='], [$company_id], 1);

                        if (!$updateEmployeeResult['err']['code']) {
                            $_SESSION['documents'] = $newDocumentsApprvdDetails;
                            $response['result'] = ['docs' => array_keys($documentsMap), 'emptyDocs' => $emptyDocs];
                            $response['err'] = null;
                        } else {
                            $response['result'] = null;
                            $response['err'] = ['error' => 'DB', 'msg' => 'An error occured, please try again'];
                        }
                    } else {
                        $response['result'] = null;
                        $response['err'] = ['error' => 'IMAGEUPLOADERROR', 'msg' => 'There was an problem uploading your display picture, please retry'];
                    }
                } else {
                    $response['result'] = null;
                    $response['err'] = ['error' => 'VALIDATION', 'msg' => $validationResult];
                }
            } else {
                $response['result'] = null;
                $response['err'] = ['error' => 'NODOCUMENTS', 'msg' => 'No document(s) were sent'];
            }
        } else {
            $_SESSION['approved'] = 1;
            unset($_SESSION['documents']);

            $response['result'] = null;
            $response['err'] = ['error' => 'ALREADYAPPROVED', 'msg' => 'Merchant already approved. <a href="javascript:location.reload();">Click here</a> to refresh page'];
        }
    } else {
        $response['result'] = null;
        $response['err'] = ['error' => 'DB', 'msg' => 'An error occured, please try again'];
    }

    echo json_encode($response);
}