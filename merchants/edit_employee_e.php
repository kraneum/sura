<?php
exit('');
session_start();

if (!isset($_SESSION['id'])) {
    exit();
}
//To prevent sql injection
//htmlspecialchars(strip_tags(trim()));
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    require_once '../php/form_validate.php';
    
    $cleanedUserInputMap = array_filter(
            array_map(function($value) {
                return isset($_POST[$value]) ? htmlspecialchars(strip_tags(trim($_POST[$value]))) : '';
            }, ['first_name' => 'first_name', 'middle_name' => 'middle_name', 'last_name' => 'last_name', 'dob' => 'dob', 'phone' => 'phone', 'country' => 'country']), function($value) {
        return strlen($value);
    });
    
    $response = ['err' => null, 'result' => false];
    
    if (isset($_FILES["display_picture"]["tmp_name"])) {
        $validationResult = $form_validate([
            'display_picture' => 'filemaxmegabytes:2|filemimetypes:image/jpeg,image/png|imagedimensions:min_width=100,min_height=100'
                ], ['display_picture' => $_FILES['display_picture']]);

        if (empty($validationResult)) {
            $displayPictureFileName = $_SESSION['email'] . '.' . str_replace('jpeg', 'jpg', substr(getimagesize($_FILES["display_picture"]["tmp_name"])["mime"], 6));

            if (move_uploaded_file($_FILES["display_picture"]["tmp_name"], '../img/employees/' . $displayPictureFileName)) {
                //edit the editables
                $_SESSION['display_picture_file_name'] = $cleanedUserInputMap['display_picture_file_name'] = $displayPictureFileName;
                $response['display_picture_changed'] = $cleanedUserInputMap['display_picture_file_name'];
            } else {
                $response['result'] = null;
                $response['err'] = ['error' => 'IMAGEUPLOADERROR', 'msg' => 'There was an problem uploading your display picture, please retry'];
            }
        } else {
            $response['result'] = null;
            $response['err'] = ['error' => 'VALIDATION', 'msg' => $validationResult];
        }
    }

    if (!$response['err'] && count($cleanedUserInputMap)) {
        require_once '../php/form_validate.php';

        $validationResult = $form_validate([
            'first_name' => 'maxlength:100',
            'middle_name' => 'maxlength:100',
            'last_name' => 'maxlength:100',
            'dob' => 'dob|maxlength:10',
            'phone' => 'phone|maxlength:50',
            'country' => 'length:2|alphabets'
                ], $cleanedUserInputMap);

        if (empty($validationResult)) {
            require_once '../php/sura_config.php';
            require_once '../php/sura_functions.php';

            $columns = array_keys($cleanedUserInputMap);
            $values = array_map(function($value) {
                return '"' . $value . '"';
            }, array_values($cleanedUserInputMap));

            $updateEmployeeResult = runSimpleUpdateQuery(makeConnection_merchant(), 'employees', $columns, $values, ['id', 'user_type_id'], ['=', '='], [+$_SESSION['id'], 1], 1);

            if (!$updateEmployeeResult['err']['code']) {
                if ($updateEmployeeResult['result'] || isset($_FILES["display_picture"]["tmp_name"])) {
                    $response['result'] = true;
                    $response['err'] = null;

                    isset($cleanedUserInputMap['email']) && $_SESSION['email'] = $cleanedUserInputMap['email'];
                    isset($cleanedUserInputMap['user_permissions']) && $_SESSION['user_permissions'] = $cleanedUserInputMap['user_permissions'];
                } else {
                    $response['result'] = null;
                    $response['err'] = ['error' => 'NOCHANGES', 'msg' => 'No changes'];
                }
            } else {
                $response['result'] = null;
                $response['err'] = ['error' => 'DB', 'msg' => 'An error occured, please try again'];
            }
        } else {
            $response['result'] = null;
            $response['err'] = ['error' => 'VALIDATION', 'msg' => $validationResult];
        }
    }

    echo json_encode($response);
}