<?php

session_start();

require_once '../php/user_permissions.php';

if ((!isset($_SESSION['id']) || !UserPermissions::isSuperuserPermission(+$_SESSION['user_permissions']))&& !empty($_SESSION['approved'])) {
    exit();
}

if (isset($_GET['phone'])) {
    require_once '../php/sura_config.php';
    require_once '../php/sura_functions.php';

    $emailAvailableResult = runSimpleFetchQuery(makeConnection(), ['phone'], 'employees', ['phone'], ['='], ['"' . strtolower(htmlspecialchars(strip_tags(trim($_GET['phone'])))) . '"'], '', '', 1);
    
    if ($emailAvailableResult['err']['code'] || $emailAvailableResult['result']) {
        header("HTTP/1.0 400 Bad request");
    }
}