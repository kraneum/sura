<?php

$merchant_id = $_SESSION['company_id'];
$email = $_SESSION['email'];

$pay_rate = getData('get_uic_pay_commission')['pay_commission'];
?>
<!--Modal -->
<div class = "modal fade" id = "pointsModal" tabindex = "-1" role = "dialog" aria-labelledby = "pointsLabel" aria-hidden = "true">
    <div class = "modal-dialog">
        <div class = "modal-content">
            <div class = "modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class = "modal-title pull-left" id = "pointsLabel">Buy iPoints</h4>
            </div>

            <div class = "modal-body">
                <div id="cardSection">
                    <form id="buyPointsForm">
                        <script src="https://js.paystack.co/v1/inline.js"></script>
                        <input type="hidden" id="pay_id" value="<?= $merchant_id?>"/>
                        <input type="hidden" id="pay_rate" value="<?= $pay_rate?>"/>
                        <div class="row">
                            <div class = "form-group col-md-6">
                                <label for = "pay_email" class = "control-label">Email</label>
                                <input class="form-control" readonly id="pay_email" name="pay_email" value="<?= $email ?>"/>
                            </div>
                            
                            <div class = "col-md-6 text-right">
                                <img src = "../img/logos/mastercard.jpg" alt = "" width="30px" height="17px"/>
                                <img src = "../img/logos/visa.jpg" alt = "" width="30px" height="17px"/>
                                <img src = "../img/logos/verve.png" alt = "" width="45px" height="17px"/>
                            </div>
                        </div>
                        <div class = "row">
                            <div class = "form-group col-md-6">
                                <label for = "pay_amt" class = "control-label">Number of iPoints</label>
                                <input  class="form-control" id="pay_amt" name="pay_amt" value="" data-parsley-required data-parsley-type="digits"/>
                                <span class="help-block text-success" >Charging amount: &#8358;<span>0</span></span>
                            </div>

                            
                        </div>

                        <div class="row">
                            <span class="col-md-6 small text-muted">By clicking "Top Up", you agree to our <a href = "#" >Terms</a> and <a href = "#" >Privacy Policy</a></span>
                            <div class="col-md-6 text-right">
                                <button type="submit" class="btn btn-lg btn-success" >Top Up</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>





