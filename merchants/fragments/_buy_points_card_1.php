
<!--Modal -->
<div class = "modal fade" id = "pointsModal" tabindex = "-1" role = "dialog" aria-labelledby = "pointsLabel" aria-hidden = "true">
    <div class = "modal-dialog">
        <div class = "modal-content">
            <div class = "modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class = "modal-title pull-left" id = "pointsLabel">Buy Points</h4>
            </div>

            <div class = "modal-body">
                <div id="infoModal"></div>

                <div id="cardSection">
                    <form>
                        <div class = "row">
                            <div class = "form-group col-md-6">
                                <label for = "amt" class = "control-label">Point Deposit Amount</label>
                                <div class = "input-group">
                                    <span class = "input-group-addon">&#8358;</span>
                                    <input type = "number" class = "form-control" id = "amt">
                                </div>
                            </div>

                            <div class = "col-md-6 text-right">
                                <img src = "../img/logos/mastercard.jpg" alt = ""/>
                                <img src = "../img/logos/visa.jpg" alt = ""/>
                                <img src = "../img/logos/verve.png" alt = ""/>
                                <img src = "../img/logos/discover.jpg" alt = ""/>
                            </div>
                        </div>
                        <div class = "row">
                            <div class = "form-group col-md-12">
                                <label for = "ccNum" class = "control-label">Card Number</label>
                                <input type = "number" class = "form-control" id = "ccNum">
                            </div>
                        </div>
                        <div class = "row">
                            <div class = "form-group col-md-5">
                                <label for = "secCode" class = "control-label">Security Code</label>
                                <input type = "password" class = "form-control" id = "SecCode">
                            </div>
                            <div class = "form-group col-md-offset-1 col-md-5">
                                <label for = "cvv" class = "control-label">CVV</label>
                                <input type = "number" class = "form-control" id = "cvv">
                            </div>
                        </div>
                        <div class = "row">
                            <div class = "form-group col-md-5">
                                <label for = "exMonth" class = "control-label">Expiration Month</label>
                                <select class = "form-control" id = "select">
                                    <option disabled selected></option>
                                    <?php
                                    $num = 28;

                                    for ($i = 1; $i <= 12; $i++) {
                                        $monthName = jdmonthname($num, 3);
                                        echo "<option value=\"$i\">$monthName</option>";
                                        $num += 28;
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-5">
                                <label for="exYear" class="control-label">Expiration Year</label>
                                <select class="form-control" id="select">
                                    <option disabled selected></option>
                                    <?php
                                    $current = date("Y");
                                    for ($i = $current; $i <= $current + 20; $i++) {
                                        echo "<option value=\"$i\">$i</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <div class="row">
                            <span class="col-md-6 small text-muted">By clicking "Top Up", you agree to our <a href = "#" >Terms</a> and <a href = "#" >Privacy Policy</a></span>
                            <div class="col-md-6 text-right">
                                <a class="btn btn-lg btn-primary">Top Up</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



