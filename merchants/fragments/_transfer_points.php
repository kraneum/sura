
<!--Modal -->
<div class = "modal fade" id = "transferModal" tabindex = "-1" role = "dialog" aria-labelledby = "pointsLabel" aria-hidden = "true">
    <div class = "modal-dialog">
        <div class = "modal-content">
            <div class = "modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class = "modal-title pull-left" id = "pointsLabel">Transfer Points</h4>
            </div>

            <div class = "modal-body">
                <div id="infoModal"></div>


                <ul class="nav nav-tabs tabs-material">
                    <li class="active"><a href="#agent" data-toggle="tab">to Agent</a></li>
                    <li><a href="#user" data-toggle="tab">to User</a></li>
                    <li><a href="#merchant" data-toggle="tab">to Merchant</a></li>
                    <li class="pull-right"><a href="#many" data-toggle="tab">to Many</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="agent">
                        <form id="transferM2AForm">
                            <h4 class="text-muted text-center">Enter Agent Number and Amount</h4>
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class = "form-group col-sm-8">
                                    <label for = "phone" class = "control-label">Agent Number: </label>
                                    <input type="text" name="phone" class="form-control" id="phone" placeholder="Enter phone number" maxlength="11" data-parsley-required data-parsley-minlength="11" data-parsley-maxlength="11"  data-parsley-type="digits" data-parsley-minlength-message="This value should be 11 digits" data-parsley-maxlength-message="This value should be 11 digits" data-parsley-type-message="This value should contain digits only"/>
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class = "form-group col-sm-8">
                                    <label for = "amount" class = "control-label">Amount: </label>
                                    <input type = "text" class = "form-control" name="amount" placeholder="Enter number of iPoints" data-parsley-required data-parsley-type="digits" data-parsley-type-message="This value should contain digits only"/>
                                </div>
                                <div class="col-sm-2"></div>
                            </div>

                            <div class="row">
                                <span class="col-md-6 small text-muted">By clicking "Top Up", you agree to our <a href = "#" >Terms</a> and <a href = "#" >Privacy Policy</a></span>
                                <div class="col-md-6 text-right">
                                    <input id="transfer2AgentBtn" name="transfer2AgentBtn" type="submit" class="btn btn-success btn-rounded box-shadow--2dp" value="Transfer"/>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="user">
                        <form id="transferM2UForm">
                            <h4 class="text-muted text-center">Enter User Phone Number and Amount</h4>
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class = "form-group col-sm-8">
                                    <label for = "phone1" class = "control-label">Phone Number: </label>
                                    <input type="text" name="phone" class="form-control" id="phone1" placeholder="Enter phone number" maxlength="11" data-parsley-required data-parsley-minlength="11" data-parsley-maxlength="11"  data-parsley-type="digits" data-parsley-minlength-message="This value should be 11 digits" data-parsley-maxlength-message="This value should be 11 digits" data-parsley-type-message="This value should contain digits only"/>
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class = "form-group col-sm-8">
                                    <label for = "amount1" class = "control-label">Amount: </label>
                                    <input type = "text" class = "form-control" name="amount" id = "amount1" placeholder="Enter number of iPoints" data-parsley-required data-parsley-type="digits" data-parsley-type-message="This value should contain digits only"/>
                                </div>
                                <div class="col-sm-2"></div>
                            </div>

                            <div class="row">
                                <span class="col-md-6 small text-muted">By clicking "Top Up", you agree to our <a href = "#" >Terms</a> and <a href = "#" >Privacy Policy</a></span>
                                <div class="col-md-6 text-right">
                                    <input id="transfer2AgentBtn" name="transfer2UserBtn" type="submit" class="btn btn-success btn-rounded box-shadow--2dp" value="Transfer"/>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="merchant">
                        <form id="transferM2MForm">
                            <h4 class="text-muted text-center">Enter Merchant Email and Amount</h4>
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class = "form-group col-sm-8">
                                    <label for = "email2" class = "control-label">Email: </label>
                                    <input type="email" class="form-control" id="email2" name="email" placeholder="Enter email" required />
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class = "form-group col-sm-8">
                                    <label for = "amount2" class = "control-label">Amount: </label>
                                    <input type = "text" class = "form-control" id="amount2" name="amount" placeholder="Enter number of iPoints" required data-parsley-type="digits" data-parsley-type-message="This value should contain digits only"/>
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <div class="row">
                                <span class="col-md-6 small text-muted">By clicking "Top Up", you agree to our <a href = "#" >Terms</a> and <a href = "#" >Privacy Policy</a></span>
                                <div class="col-md-6 text-right">
                                    <input id="transfer2AgentBtn" name="transfer2AgentBtn" type="submit" class="btn btn-success btn-rounded box-shadow--2dp" value="Transfer"/>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="tab-pane fade" id="many">
                        <form id="transferM2ManyForm">
                            <!--<h4 class="text-muted text-center">Enter Merchant Email and Amount</h4>-->
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class = "form-group col-sm-8">
                                    <label for = "entity" class = "control-label">Entity: </label>
                                    <select class="form-control" id="entity" name="entity" required>
                                        <!--<option disabled selected></option>-->
                                        <option selected value="USER">Users</option>
                                        <!--<option value="agents">Agents</option>-->
                                    </select>
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class = "form-group col-sm-8">
                                    <label for = "list" class = "control-label">Select file (in .csv format): </label>
                                    <input type = "file" class = "form-control" id="list" name="list" placeholder="Enter number of iPoints" required data-parsley-required data-parsley-trigger="change" accept=".csv" data-parsley-filemimetypes="text/csv, application/csv, application/vnd.ms-excel, text/x-csv"/>
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <div class="row">
                                <span class="col-md-6 small text-muted">By clicking "Top Up", you agree to our <a href = "#" >Terms</a> and <a href = "#" >Privacy Policy</a></span>
                                <div class="col-md-6 text-right">
                                    <input id="transfer2ManyBtn" name="transfer2ManyBtn" type="submit" class="btn btn-success btn-rounded box-shadow--2dp" value="Transfer"/>
                                </div>
                            </div>

                            <div id="insufficientFundsModal"></div>
                            <div id="invalidNumbersModal"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





