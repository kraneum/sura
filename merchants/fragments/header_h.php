<?php
require_once '../php/user_permissions.php';
require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';
require_once('../php/curl.php');
$pageFileName = strrchr($_SERVER['PHP_SELF'], "/");

if ($_SESSION['approved']) {
    $pointBalance = $_SESSION['point_balance'];
}
if (empty($_SESSION['notifCount'])) {
    $notifCount = getData("get_notification_count_merchant", $_SESSION['company_id'])['count'];
    $_SESSION['notifCount'] = $notifCount;
} else {
    $notifCount = $_SESSION['notifCount'];
}
?>
<!DOCTYPE html>

<html>
    <head>
        <title><?php echo $_SESSION['name']; ?>-iInsurance</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="../img/favicon.png" />
        <link rel="icon" type="image/png" href="../img/favicon.png">
        <link rel="apple-touch-icon" href="../img/favicon.png">

        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

        <link href="../css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="../css/animate.min.css" rel="stylesheet" type="text/css"/>

        <link href="../css/custom.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style_e.css" rel="stylesheet" type="text/css"/>
        <script src="../js/sweetalert.min.js" type="text/javascript"></script>

        <style type="text/css">
            body {
                font-family: Avenir;
                background-color: #e0e0e0;
                font-weight: 200;
                font-size: 14px;
                color: #696969;
                /*font-family: 'Poppins', sans-serif;*/
            }
            .swal2-popup{margin-top: 50px !important;}
            table tr td {
                padding: 25px 0px !important;
            }
        </style>


    </head>
    <body>
        <div class="body_">
            <div class="container">
                <div class="row" >
                    <div class="col-lg-6"><a class="navbar-brand" href="home.php" style="float: left;"><img src="../img/logos/uic2.png" height="35px" /></a></div>

                    <div class="col-lg-6" style="padding-right: 0px;">
                        <ul class="rightc">

                            <li class="dropdown dropdown-hover nav-icon">
                                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <?php
                                    if ($notifCount > 0) {
                                        echo '<div id="notifCount" class="animated swing infinite "><span>' . $notifCount . '</span></div>';
                                    }
                                    ?>
                                    <i class="fa fa-bell-o fa-2x"></i>
                                </a>

                                <ul id="notifications" class = "dropdown-menu notif_body" data-notifcount =  "<?php echo $notifCount; ?>" data-notifids="" role = "menu">
                                    <li id = "notif_header" class = "text-bold text-center">Notifications <i class = "fa fa-spinner fa-pulse  pull-right"></i></li>
                                </ul>
                            </li>
                            <li class="nav-icon">
                                <a id="logout">
                                    <i class="fa fa-power-off fa-2x"></i>
                                </a>
                            </li>
                            <li class="dropdown dropdown-hover">
                                <?php
                                $pageName = '';
                                $approved = !empty($_SESSION['approved']);

                                if (!$approved) {
                                    $needToUpld = 0;
                                    foreach ($_SESSION['documents'] as $value) {
                                        switch ($value) {
                                            //0-not uploaded,1-approved,2-not approved/pending,3-rejected
                                            case 0:
                                            case 3:
                                                ++$needToUpld;
                                        }
                                    }
//                                    echo $needToUpld ? '<div  id="profileNeedToUpldCt">' . $needToUpld . '</div>' : '';
                                }
                                ?>
                                <a <?php echo!$approved ? 'href="documents.php"' : ''; ?>>
                                    <img id="profileDpImg" class="img-responsive img-circle" width="40px" height="40px" src="../img/employees/<?php echo $_SESSION['display_picture_file_name']; ?>" alt="" style="margin-top: 10px"/>
                                </a>

                            </li>      
                        </ul>

                    </div>

                </div>
            </div>
        </div>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav" id="navMenu"> 
                        <?php
                        echo $approved ? array_reduce([['home.php', 'DASHBOARD', 'DUMMY'], ['employees.php', 'ADMINS', 'SUPER'], ['transactions.php', 'TRANSACTIONS', 'DUMMY'], ['view_ussd.php', 'PIN CODES', 'DUMMY']], function($carry, $item) use ($pageFileName, &$pageName) {
                                    ('/' . $item[0] === $pageFileName) && ($pageName = $item[1]);
                                    return $carry . ((UserPermissions::hasPermission($item[2], intval($_SESSION['user_permissions'])) ? '<li><a' . ('/' . $item[0] === $pageFileName ? ' style="border-bottom:2px solid #72be58; color:#72be58;"' : '') . ' href="' . $item[0] . '">' . $item[1] . ('/' . $item[0] === $pageFileName ? '<span class="sr-only">(current)</span>' : '') . '</a></li>' : ''));
                                }, '') : '';
                        ?>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li >
                            Last Login: <?php echo $_SESSION['last_login'] ? date("d-m-y", strtotime($_SESSION['last_login'])) : 'Never' ?>      
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
