<?php
session_start();

if (!isset($_SESSION['id'])) {
    header("Location: login.php?goto=".urlencode($_SERVER['REQUEST_URI']));
}
!$_SESSION['approved'] && exit('');

//To prevent sql injection
 //htmlspecialchars(strip_tags(trim()));
require_once './views/generate_ussd.php';
?>