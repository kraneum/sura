<?php

session_start();

if (!isset($_SESSION['id'])) {
    exit();
}
!$_SESSION['approved'] && exit('');
//To prevent sql injection
//htmlspecialchars(strip_tags(trim()));
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    require_once '../php/form_validate.php';
    $response = [];
    $data = json_decode(file_get_contents("php://input"), JSON_OBJECT_AS_ARRAY);

    $cleanedUserInputMap = array_filter(
            array_map(function($value) {
                return htmlspecialchars(strip_tags((trim($value))));
            }, $data), function($value, $key) {
        return strlen($value) && in_array($key, ['id', 'direction']);
    }, ARRAY_FILTER_USE_BOTH);

    $validationResult = $form_validate([
        'id' => 'digits',
        'direction' => 'digits'
            ], $cleanedUserInputMap);

    if (empty($validationResult)) {
        require_once '../php/sura_config.php';
        require_once '../php/sura_functions.php';

        $columns = ['merchant_id'];
        $values = [$_SESSION['company_id']];
        $operators = ['='];
        $limit = isset($cleanedUserInputMap['id']) ? 3 : 5;
        $order = empty($cleanedUserInputMap['direction']) ? 'DESC' : 'ASC';

        if (isset($cleanedUserInputMap['id'])) {
            $columns[] = 'id';
            $operators[] = $cleanedUserInputMap['direction'] ? '>' : '<';
            $values[] = $cleanedUserInputMap['id'];
        }

        $fetchRecentActivityQueryResult = runSimpleFetchQuery(makeConnection_merchant(), ['id', 'activity_summary', 'datetimezone', 'activity_type'], 'merchants_recent_activity', $columns, $operators, $values, '', 'id ' . $order, $limit);

        if (!$fetchRecentActivityQueryResult['err']['code']) {
            $response['result'] = $fetchRecentActivityQueryResult['result'];
            $response['err'] = null;
        } else {
            $response['result'] = null;
            $response['err'] = ['error' => 'DB', 'msg' => 'An error occured, please try again'];
        }
    } else {
        $response['result'] = null;
        $response['err'] = ['error' => 'VALIDATION', 'msg' => $validationResult];
    }

    echo json_encode($response);
}