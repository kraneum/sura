<?php

session_start();

if (!isset($_SESSION['id'])) {
    header("Location: login.php?goto=" . urlencode($_SERVER['REQUEST_URI']));
}
!$_SESSION['approved'] && exit('');
//To prevent sql injection
//htmlspecialchars(strip_tags(trim()));
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    require_once '../php/form_validate.php';
    $response = [];

//Pass the format and the search criteria of ussd codes and get the downloaded file containing ussd codes
    $cleanedUserInputMap = array_filter(
            array_map(function($value) {
                return htmlspecialchars(strip_tags((trim($value))));
            }, $_GET), function($value, $key) {
        return strlen($value) && in_array($key, ['criteria', 'format', 'searchValue']);
    }, ARRAY_FILTER_USE_BOTH);

    $validationResult = $form_validate([
        'searchValue' => 'required',
        'criteria' => 'required|in:purchase_id',
        'format' => 'required|in:csv'
            ], $cleanedUserInputMap);

    if (empty($validationResult)) {
        require_once '../php/sura_config.php';
        require_once '../php/sura_functions.php';

        $fetchUssdCodesResult = runSimpleFetchQuery(makeConnection_merchant(), ['id', 'code', 'point_worth'], 'ussd_codes', ['merchant_id', $cleanedUserInputMap['criteria']], ['=', '='], [$_SESSION['company_id'], $cleanedUserInputMap['searchValue']], '', '', '');

        if (!$fetchUssdCodesResult['err']['code']) {
            $ussdCodes = $fetchUssdCodesResult['result'];
            $output = "serial,point worth,pin";

            if (!empty($ussdCodes)) {

                switch ($cleanedUserInputMap['format']) {
                    case 'csv':
                        foreach ($ussdCodes as $ussdCode) {
                            $output .= "\r\n" . str_pad($ussdCode['id'], 10, "0", STR_PAD_LEFT) . "," . $ussdCode['point_worth'] . ",'" . $ussdCode['code'] . "'";
                        }
                        break;
                }

                header('Content-Type: application/csv');
                header('Content-disposition: attachment; filename=uic_pins_' . $cleanedUserInputMap['searchValue'] . '.csv');
                header('Content-Length: ' . strlen($output));
                exit($output);
            } else {
                $response['result'] = null;
                $response['err'] = null;
            }
        } else {
            $response['result'] = null;
            $response['err'] = ['error' => 'DB', 'msg' => 'An error occured, please try again'];
        }
    } else {
        $response['result'] = null;
        $response['err'] = ['error' => 'VALIDATION', 'msg' => $validationResult];
    }

    echo json_encode($response);
}