<?php
session_start();

if (!isset($_SESSION['id'])) {
    header("Location: login.php?goto=".urlencode($_SERVER['REQUEST_URI']));
}
!$_SESSION['approved'] && exit('');

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';

$company_id = $_SESSION['company_id'];
$con = makeConnection_merchant();

$merchantQueryResult = runSimpleFetchQuery($con, ['name','write_up','contact_phone','contact_email', 'date_joined','rc_number','operating_country','display_picture','head_office_address'], 'merchants', ['id'], ['='], [$company_id], '', '', 1);

if(!$merchantQueryResult['err']['code']){
    $merchant = $merchantQueryResult['result'][0];
    
    //$last5recentActivties = runSimpleFetchQuery($con, ['activity_summary','datetimezone'], 'merchants_recent_activity', ['merchant_id'], ['='], [$company_id], '', 'id DESC', 5)['result'];
    
    $last5redemptions = runFetchLast5Redeemptions($con, $company_id)['result'];

    //$unredeemedUssdCt = runFetchUnredeemedUssdCt($con, $company_id)['result'];
   
    require_once './views/home.php';
}
?>


     