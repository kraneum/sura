<?php

session_start();

require_once '../php/user_permissions.php';

if ((!isset($_SESSION['id']) || !UserPermissions::isSuperuserPermission(+$_SESSION['user_permissions']))) {
    exit();
}
!$_SESSION['approved'] && exit('');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = json_decode(file_get_contents("php://input"), JSON_OBJECT_AS_ARRAY);
    if (isset($data['id']) && isset($data['email'])) {
        require_once '../php/kickout_employee.php';
        require_once 'save_admin_actions.php';

        $con = makeConnection_merchant();

        saveAdminActions($con, 'logout', $_SESSION['email'], htmlspecialchars(strip_tags(trim($data['email']))));

        echo json_encode($kickoutEmployee($con, htmlspecialchars(strip_tags(trim($data['id']))), +$_SESSION['company_id']));
    }
}
