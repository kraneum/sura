<?php

session_start();

if (isset($_SESSION['id']) && !isset($_GET['id'])) {
    header("Location: home.php");
}

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';
require_once '../php/form_validate.php';

$operationStatus = ['err' => false];

if (isset($_POST['btn_login'])) {
    // clean user inputs to prevent sql injections
    $cleanedUserInputMap = array_map(function($value) {
        return filter_var($_POST[$value], FILTER_SANITIZE_STRING);
    }, ['email' => 'email', 'password' => 'password']);

    $validationResult = $form_validate([
        'email' => 'required',
        'password' => 'required',
            ], $cleanedUserInputMap);

    if (empty($validationResult)) {
        $cleanedUserInputMap['email'] = strtolower($cleanedUserInputMap['email']);
        $password = $cleanedUserInputMap['password'];
        unset($cleanedUserInputMap['password']);

        //$cleanedUserInputMap['password'] = hash('sha256', $cleanedUserInputMap['password']); // password hashing using SHA256
        $fields = array_keys($cleanedUserInputMap);
        $fields[] = 'user_type_id';
        $fields[] = 'deleted';
        $values = array_map(function($value) {
            return '"' . $value . '"';
        }, array_values($cleanedUserInputMap));
        $values[] = 1; //Verify that its a merchant
        $values[] = 0;


        $con = makeConnection();
        $isValid = false;

        $queryResult = runSimpleFetchQuery($con, ['id', 'password', 'company_id', 'user_permissions', 'suspended', 'display_picture_file_name', 'last_login'], 'employees', $fields, ['=', '=', '='], $values, null, null, 1);
        !empty($queryResult['result']) && $isValid = password_verify($password, $queryResult['result'][0]['password']);

        if ($isValid) {
            unset($queryResult['result'][0]['password']);
            $employeeInfo = $queryResult['result'][0];
//            print_r($employeeInfo);
            if (+$employeeInfo['suspended'] === 0) {
                //Update encrypt sessionId and last login time to DB
                $updateLogin = runSimpleUpdateQuery($con, 'employees', ['session_id'], ["'" . session_id() . "'"], ['id'], ['='], [$employeeInfo['id']], 1);

                if (!$updateLogin['err']['code']) {
                    $_SESSION['id'] = $employeeInfo['id'];
                    $_SESSION['email'] = $cleanedUserInputMap['email'];
                    $_SESSION['entity']['id'] = $employeeInfo['company_id']; // for accessing suracommands...
                    $_SESSION['company_id'] = $employeeInfo['company_id'];
                    $_SESSION['user_permissions'] = $employeeInfo['user_permissions'];
                    $_SESSION['display_picture_file_name'] = $employeeInfo['display_picture_file_name'];
                    $_SESSION['last_login'] = $employeeInfo['last_login'];

                    $query0Result = runSimpleFetchQuery($con, ['name', 'point_balance', 'approved', 'documents'], 'merchants', ['id'], ['='], [$employeeInfo['company_id']], null, null, 1);
                    $_SESSION['name'] = $query0Result['result'][0]['name'];
                    $_SESSION['point_balance'] = $query0Result['result'][0]['point_balance'];
                    //Session documents exists only when merchant is nt approved
                    if (($_SESSION['approved'] = $query0Result['result'][0]['approved'])) {
                        header("Location: home.php");
                    } else {
                        $_SESSION['documents'] = json_decode($query0Result['result'][0]['documents'], JSON_OBJECT_AS_ARRAY);
                        header("Location: documents.php");
                    }
                } else {
                    $operationStatus['err'] = true;
                    $operationStatus['type'] = 'updateError';

                    $operationStatus['message'] = 'Something went wrong, please retry';
                }
            } else {
                $operationStatus['err'] = true;
                $operationStatus['type'] = 'employeeSuspendedError';

                $operationStatus['message'] = 'Your account is suspended, please contact your administrator';
            }
        } else if (!$queryResult['err']['code']) {
            $operationStatus['err'] = true;
            $operationStatus['type'] = 'employeeNotFoundError';

            $operationStatus['message'] = 'Email/password combination is incorrect';
        } else {
            $operationStatus['err'] = true;
            $operationStatus['type'] = 'queryError';

            $operationStatus['message'] = 'Something went wrong, please retry';
        }
    } else {
        $operationStatus['err'] = true;
        $operationStatus = array_merge($validationResult, ['type' => 'validationError']);
    }
}

require_once './views/login.php';
