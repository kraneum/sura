<?php

session_start();

require_once '../php/user_permissions.php';

if ((!isset($_SESSION['id']) || !UserPermissions::isSuperuserPermission(+$_SESSION['user_permissions']))&& !empty($_SESSION['approved'])) {
    exit();
}

if (isset($_GET['rc_number'])) {
    require_once '../php/sura_config.php';
    require_once '../php/sura_functions.php';

    $emailAvailableResult = runSimpleFetchQuery(makeConnection(), ['rc_number'], 'merchants', ['rc_number'], ['='], ['"' . strtolower(htmlspecialchars(strip_tags(trim($_GET['rc_number'])))) . '"'], '', '', 1);
    
    if ($emailAvailableResult['err']['code'] || $emailAvailableResult['result']) {
        header("HTTP/1.0 400 Bad request");
    }
}