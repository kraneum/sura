<?php

session_start();

if (!isset($_SESSION['id'])) {
    exit();
}
!$_SESSION['approved'] && exit('');
//To prevent sql injection
//htmlspecialchars(strip_tags(trim()));
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (count($data = json_decode(file_get_contents("php://input"), JSON_OBJECT_AS_ARRAY))) {
        $cleanedUserInputMap = array_filter(
                array_map(function($value) {
                    return htmlspecialchars(strip_tags((trim($value))));
                }, $data), function($value, $key) {
            return strlen($value) && in_array($key, ['depositAmount', 'cardNumber', 'cvv', 'securityCode', 'expirationMonth', 'expirationYear']);
        }, ARRAY_FILTER_USE_BOTH);

        if (count($cleanedUserInputMap)) {
            require_once '../php/form_validate.php';
            $response = [];

            $validationResult = $form_validate([//Give the digits a max_length
                'depositAmount' => 'required|numeric',
                'cardNumber' => 'required|digits',
                'cvv' => 'required|digits',
                'securityCode' => 'required|digits',
                'expirationMonth' => 'required|numeric|month',
                'expirationYear' => 'required|numeric|gte:' . date('Y'),
                    ], $cleanedUserInputMap);

            if (empty($validationResult)) {
                require_once '../php/sura_config.php';
                require_once '../php/sura_functions.php';

                $con = makeConnection();
                $pointsToAdd = $cleanedUserInputMap['depositAmount'];
                $merchantId = $_SESSION['company_id'];
                $datetimezone = '"' . (new DateTime())->format(DateTime::ISO8601) . '"';
                
                $ref = 'UICI-' . $merchantId . round(microtime(true) * 1000);

                mysqli_autocommit($con, false);

                //Call the payment platform
                //check if success was returned
                //if success update points in merchants and make an entry in point purchases and make an entry into recent activities
                $addMerchantsPointsResult = runAddMerchantsPoints($con, $merchantId, $pointsToAdd);
                if (!$addMerchantsPointsResult['err']['code']) {
                    if ($addMerchantsPointsResult['result']) {
                        //make an entry in point purchases
                        $makeMerchantPointsEntryResult = runSimpleInsertQuery($con, 'point_purchases', ['reference' ,'point_amount', 'date_bought', 'merchant_id'], ["'$ref'", $pointsToAdd, $datetimezone, $merchantId]);

                        if (!$makeMerchantPointsEntryResult['err']['code']) {
                            if ($makeMerchantPointsEntryResult['result']) {
                                date_default_timezone_set('Africa/Lagos');

                                //make an entry in recent activities
                                $makeRecentActivitiesEntryResult = runSimpleInsertQuery($con, 'merchants_recent_activity', ['activity_summary', 'datetimezone', 'activity_type', 'activity_id', 'merchant_id'], ["'" . /*$_SESSION['email'] . ' purchased ' . $pointsToAdd . ' worth of points at ' . date('g:ia T')*/json_encode(['user'=>$_SESSION['email'], 'points'=>$pointsToAdd]) . "'", $datetimezone, '"POINT_PURCHASE"', $makeMerchantPointsEntryResult['insertedId'], $merchantId]);

                                if (!$makeRecentActivitiesEntryResult['err']['code']) {
                                    if ($makeRecentActivitiesEntryResult['result']) {
                                        //Success
                                        if (mysqli_commit($con)) {
                                            $response['result'] = true;
                                            $response['err'] = null;
                                        } else {
                                            //Nt commited
                                            $response['result'] = null;
                                            $response['err'] = ['error' => 'NOTCOMMITED', 'msg' => 'Changes not commited into DB'];
                                        }
                                    } else {
                                        //Nt inserted
                                        $response['result'] = null;
                                        $response['err'] = ['error' => 'GENERIC', 'msg' => 'A problem is preventing the point purchases from being totally successful'];
                                    }
                                } else {
                                    $response['result'] = null;
                                    $response['err'] = ['error' => 'DB', 'msg' => 'An error occured, please try again'];
                                }
                            } else {
                                //Nt inserted
                                $response['result'] = null;
                                $response['err'] = ['error' => 'GENERIC', 'msg' => 'A problem is preventing the point purchases from being totally successful'];
                            }
                        } else {
                            $response['result'] = null;
                            $response['err'] = ['error' => 'DB', 'msg' => 'An error occured, please try again'];
                        }
                    } else {
                        //Merchant Maybe nt found
                        $response['result'] = null;
                        $response['err'] = ['error' => 'NOTFOUND', 'msg' => 'Merchant not found'];
                    }
                } else {
                    $response['result'] = null;
                    $response['err'] = ['error' => 'DB', 'msg' => 'An error occured, please try again'];
                }
            } else {
                $response['result'] = null;
                $response['err'] = ['error' => 'VALIDATION', 'msg' => $validationResult];
            }

            echo json_encode($response);
        }
    }
}