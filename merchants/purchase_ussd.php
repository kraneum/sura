<?php

session_start();

if (!isset($_SESSION['id'])) {
    exit();
}
!$_SESSION['approved'] && exit('');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (count($data = json_decode(file_get_contents("php://input"), JSON_OBJECT_AS_ARRAY))) {
        $cleanedUserInputMap = array_filter(
                array_map(function($value) {
                    return htmlspecialchars(strip_tags((trim($value))));
                }, $data), function($value, $key) {
            return strlen($value) && in_array($key, ['5', '10', '20', '50', '100', '200', '500', '1000', 'orderCards']);
        }, ARRAY_FILTER_USE_BOTH);

        if (count($cleanedUserInputMap)) {
            require_once '../php/form_validate.php';
            $response = ['err' => null];

            $validationResult = $form_validate([//Give the digits a max_length
                '5' => 'digits',
                '10' => 'digits',
                '20' => 'digits',
                '50' => 'digits',
                '100' => 'digits',
                '200' => 'digits',
                '1000' => 'digits',
                'orderCards' => 'digits'
                    ], $cleanedUserInputMap);

            if (empty($validationResult)) {
                $totalValue = 0;
                $ussdCode2Gen = 0;
                //$ussdPurchaseSummary = '';
                $orderCards = +isset($cleanedUserInputMap['orderCards']);

                unset($cleanedUserInputMap['orderCards']);

                foreach ($cleanedUserInputMap as $ussdValue => $ussdAmount) {
                    //$ussdPurchaseSummary .= ' ' . $ussdAmount . ' PIN codes of ' . $ussdValue . ' value,';
                    $totalValue += ($ussdValue * $ussdAmount);
                    $ussdCode2Gen += $ussdAmount;
                }

                $maxCodes2Gen = 40320;

                if ($ussdCode2Gen <= $maxCodes2Gen) {
                    require_once '../php/sura_config.php';
                    require_once '../php/sura_functions.php';

                    $merchantId = $_SESSION['company_id'];
                    $datetimezone = '"' . (new DateTime())->format(DateTime::ISO8601) . '"';

                    //$ussdPurchaseSummary[count($ussdPurchaseSummary) - 1] = ' ';
                    $con = makeConnection_merchant();
                    mysqli_autocommit($con, false);

                    $subtractMerchantsPointsResult = runSubtractMerchantPointsQuery($con, $merchantId, $totalValue);
                    if (!$subtractMerchantsPointsResult['err']['code']) {
                        if ($subtractMerchantsPointsResult['result']) {
                            //make an entry in ussd purchases
                            $makeMerchantPointsEntryResult = runSimpleInsertQuery($con, 'ussd_purchases', ['total_value', 'codes_purchased', 'datetimezone', 'order_cards', 'merchant_id'], [$totalValue, $ussdCode2Gen, $datetimezone, $orderCards, $merchantId]);

                            if (!$makeMerchantPointsEntryResult['err']['code']) {
                                if ($makeMerchantPointsEntryResult['result']) {
                                    require_once 'ussd_codes.php';

                                    //date_default_timezone_set('Africa/Lagos');
                                    $purchaseId = mysqli_insert_id($con);
                                    $ussdPurchaseInsertId = $purchaseId - 1;

                                    //insert and replace ussd_codes duplicates
                                    try {
                                        //Fine tune this value depending on the maximum ussdcodes that can be created at a stretch, 37000 currently
                                        $ussdCodeLength = 8;
                                        $modifierLength = 16 - $ussdCodeLength;
                                        $maxCodesGen = 30000; //mysql limit
                                        $codesGenerated = 0;
                                        $codes2GenNow = $ussdCode2Gen <= $maxCodesGen ? $ussdCode2Gen : $maxCodesGen;
                                        $generationModifier = substr(str_pad(strlen($ussdPurchaseInsertId) < $modifierLength + 1 ? $ussdPurchaseInsertId : $ussdPurchaseInsertId - (strval($ussdPurchaseInsertId)[0] * pow(10, strlen($ussdPurchaseInsertId) - 1)), $modifierLength, '0', STR_PAD_LEFT), -$modifierLength);
                                        $ussdCodes = [];

                                        do {
                                            $codesGenerated += $codes2GenNow;
                                            $ussdCodes = array_merge($ussdCodes, UssdCodes::generate($codes2GenNow, $ussdCodeLength, $generationModifier));
                                            $runInsertUSSDCodesResult = runInsertUSSDCodes($con, $ussdCodes, $cleanedUserInputMap, $purchaseId, $merchantId);
                                        } while (!$runInsertUSSDCodesResult['err']['code'] && $codesGenerated < $ussdCode2Gen && $codes2GenNow = $ussdCode2Gen - $codesGenerated);

                                        if (!$runInsertUSSDCodesResult['err']['code']) {
                                            if ($runInsertUSSDCodesResult['result']) {
                                                if ($orderCards) {
                                                    $makeCardOrdersEntryResult = runSimpleInsertQuery($con, 'card_orders', ['purchase_id', 'merchant_id'], [$purchaseId, $merchantId]);

                                                    if ($makeCardOrdersEntryResult['err']['code']) {
                                                        $response['result'] = null;
                                                        $response['err'] = ['error' => 'DB', 'msg' => 'An error occured, please try again1'];
                                                    }
                                                }

                                                if (!$response['err']) {
                                                    //make an entry in recent activities
                                                    $makeRecentActivitiesEntryResult = runSimpleInsertQuery($con, 'merchants_recent_activity', ['activity_summary', 'datetimezone', 'activity_type', 'activity_id', 'merchant_id'], ["'" . /* $_SESSION['email'] . ' purchased' . $ussdPurchaseSummary . ' of total value ' . $totalValue . ' at ' . date('g:ia T') */ json_encode(['user' => $_SESSION['email'], 'totalValue' => $totalValue, 'totalPins' => $ussdCode2Gen, 'details' => $cleanedUserInputMap, 'purchaseId' => $makeMerchantPointsEntryResult['insertedId'], 'orderCards' => $orderCards]) . "'", $datetimezone, '"PINS_GENERATED"', $makeMerchantPointsEntryResult['insertedId'], $merchantId]);
//                                                    print_r($makeRecentActivitiesEntryResult);
                                                    if (!$makeRecentActivitiesEntryResult['err']['code']) {
                                                        if ($makeRecentActivitiesEntryResult['result']) {
                                                            //Success
                                                            if (mysqli_commit($con)) {
                                                                $response['result'] = $makeMerchantPointsEntryResult['insertedId'];
                                                                $response['err'] = null;
                                                            } else {
                                                                //Nt commited
                                                                $response['result'] = $makeRecentActivitiesEntryResult;
                                                                $response['err'] = ['error' => 'NOTCOMMITED', 'msg' => 'Changes not commited into DB'];
                                                            }
                                                        } else {
                                                            //Nt inserted
                                                            $response['result'] = null;
                                                            $response['err'] = ['error' => 'GENERIC', 'msg' => 'A problem is preventing the ussdcodes generation from being totally successful'];
                                                        }
                                                    } else {
                                                        $response['result'] = null;
                                                        $response['err'] = ['error' => 'DB', 'msg' => 'An error occured, please try again2'];
                                                    }
                                                }
                                            } else {
                                                //Nt inserted
                                                $response['result'] = null;
                                                $response['err'] = ['error' => 'GENERIC', 'msg' => 'A problem is preventing the ussdcodes generation from being successful'];
                                            }
                                        } else {
                                            $response['result'] = null;
                                            $response['err'] = ['error' => 'DB', 'msg' => 'An error occured, please try again3'];
                                        }
                                    } catch (Exception $e) {
                                        $response['result'] = null;
                                        $response['err'] = ['error' => 'USSDCODESGENERROR', 'msg' => 'Problem with ussdcodes, try spliting the ussdcodes generation into batches'];
                                    }
                                } else {
                                    //Nt inserted
                                    $response['result'] = null;
                                    $response['err'] = ['error' => 'GENERIC', 'msg' => 'A problem is preventing the ussdcodes generation from being totally successful'];
                                }
                            } else {
                                $response['result'] = null;
                                $response['err'] = ['error' => 'DB', 'msg' => 'An error occured, please try again4'];
                            }
                        } else {
                            //Insufficient points
                            $response['result'] = null;
                            $response['err'] = ['error' => 'INSUFFICIENT_POINTS', 'msg' => 'You dont have sufficient points to make this purchase, please topup more points'];
                        }
                    } else {
                        $response['result'] = null;
                        $response['err'] = ['error' => 'DB', 'msg' => 'An error occured, please try again5'];
                    }
                } else {
                    //validation error
                    $response['result'] = null;
                    $response['err'] = ['error' => 'MAX_PIN_GENERATION_EXCEEDED', 'msg' => 'You can\'t generate more than ' . $maxCodes2Gen . ' pins at a time, please split your pin generation into batches'];
                }
            } else {
                $response['result'] = null;
                $response['err'] = ['error' => 'VALIDATION', 'msg' => $validationResult];
            }

            echo json_encode($response);
        }
    }
}