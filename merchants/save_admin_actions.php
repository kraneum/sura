<?php

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';

function saveAdminActions($con, $action, $admin, $user, $changes = null) {
    $dateTimeZone = (new DateTime())->format(DateTime::ISO8601);
    $merchantId = $_SESSION['company_id'];

    $saveIntoAdminActivities = runSimpleInsertQuery($con, 'admin_activities', ['admin', 'user', 'summary', 'details', 'datetimezone', 'merchant_id'], ['"' . $admin . '"', '"' . $user . '"', '"' . $action . '"', "'" . json_encode($changes) . "'", '"' . $dateTimeZone . '"', $merchantId]);

    if (!$saveIntoAdminActivities['err']['code']) {
        /*$saveIntoMerchantsRecentActivity = */runSimpleInsertQuery($con, 'merchants_recent_activity', ['activity_summary', 'datetimezone', 'activity_type', 'activity_id', 'merchant_id'], ["'" . json_encode(['admin' => $admin, 'user' => $user, 'summary' => $action, 'details' => $changes]) . "'", '"' . $dateTimeZone . '"', '"ADMIN_ACTIONS"', $saveIntoAdminActivities['insertedId'], $merchantId]);

        /* if ($saveIntoMerchantsRecentActivity['err']['code']) {
          //throw new Exception($saveIntoMerchantsRecentActivity['err']['error']);
          //return $saveIntoMerchantsRecentActivity['err'];
          } */
    } else {
        //return $saveIntoAdminActivities['err'];
    }
}
