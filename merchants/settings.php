<?php
exit('');

session_start();

if (!isset($_SESSION['id'])) {
    header("Location: login.php?goto=".urlencode($_SERVER['REQUEST_URI']));
}

require_once '../php/sura_config.php';
require_once '../php/sura_functions.php';

//To prevent sql injection
$id = +htmlspecialchars(strip_tags(trim($_SESSION['id'])));

$con = makeConnection_merchant();
$fetchEmployeeResult = runSimpleFetchQuery($con, ['first_name', 'middle_name', 'last_name', 'dob', 'phone', 'country', 'security_question', 'security_answer'], 'employees', ['id'], ['='], [+$id], '', '', 1);
    if (!$fetchEmployeeResult['err']['code']) {
       $employee = $fetchEmployeeResult['result'][0];
       $securityQuestions = runSimpleFetchQuery($con, ['question'], 'security_questions', [], [], [], '', '', '')['result'];
       require_once './views/settings.php';
    }


?>