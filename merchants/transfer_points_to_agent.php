<?php

session_start();

require_once('../php/sura_config.php');
require_once('../php/sura_functions.php');
//require_once 'send_sms.php';

$con = makeConnection_merchant();

autoCommit($con, false);

if ($con) {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if (!$_POST) {
            return ['status' => false, 'err' => 'INVALID_DETAILS'];
        }

        $id = $_SESSION['id'];
        $merchant_id = $_SESSION['company_id'];
        $phone = $_POST['phone'];
        $amount = $_POST['amount'];
        $date = (new DateTime())->format(DateTime::ISO8601);

        // retrieves merchant info...
        $merchantInfo = runSimpleFetchQuery($con, ['id', 'name', 'point_balance'], 'merchants', ['id'], ['='], ["'$merchant_id'"], '', '', '')['result'];
        if (empty($merchantInfo)) {
            autoCommit($con, true);
            return ['status' => false, 'info' => 'MERCHANT_ID_NOT_RESOLVED'];
        }
        $merchant_points = intval($merchantInfo[0]['point_balance']);
        $merchant_name = $merchantInfo[0]['name'];

        // retrieves employee info...
        $selfInfo = runSimpleFetchQuery($con, ['id', 'phone'], 'employees', ['id'], ['='], [$id], '', '', '')['result'];
        if (empty($selfInfo)) {
            autoCommit($con, true);
            return ['status' => false, 'info' => 'EMPLOYEE_ID_NOT_RESOLVED'];
        }

        // retrieves agent info...
        $recipientInfo = runSimpleFetchQuery($con, ['id', 'f_name', 'email', 'phone', 'points'], 'agent', ['phone'], ['='], ["'$phone'"], '', '', '')['result'];
        if (empty($recipientInfo)) {
            autoCommit($con, true);
            return ['status' => false, 'info' => 'ID_NOT_RESOLVED'];
        }
        $agentid = $recipientInfo[0]['id'];
        $rec_points = intval($recipientInfo[0]['points']);
        $rec_name = $recipientInfo[0]['f_name'];
        $rec_email = $recipientInfo[0]['email'];
        $rec_number = $recipientInfo[0]['phone'];

        if ($amount <= 0) {
            autoCommit($con, true);
            $message['info'] = 'INVALID_AMOUNT';
        } else if ($amount > $merchant_points) {
            autoCommit($con, true);
            $message['info'] = 'INSUFFICIENT_POINTS';
        } else {
            $new_rec_points = $rec_points + $amount;
            $new_merchant_points = $merchant_points - $amount;

            $flag = true;

            require 'transfer_points_to_agent_.php';

            if ($flag) {
                commit($con);
                $message['status'] = true;
                $message['info'] = 'SUCCESS';
                $message['points'] = number_format($new_merchant_points);
                require_once '../php/sms/sms_sender.php';
                sendSms($rec_name, 'You were transferred ' . $amount . ' points from a Merchant' . ' - ' . $merchant_name, "$rec_number");
            } else {
                rollback($con);
                $message['status'] = false;
                $message['info'] = 'ERROR';
            }
        }
    } else {
        $message['status'] = false;
        $message['err'] = 'WRONG_REQUEST_TYPE';
    }
} else {
    $message['status'] = false;
    $message['err'] = 'DATABASE_CONNECT_ERROR';
}

autoCommit($con, true);

disconnectConnection($con);

echo json_encode($message, JSON_PRETTY_PRINT);
