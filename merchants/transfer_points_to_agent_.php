<?php

$ref = 'UICI-'.$merchant_id.round(microtime(true) * 1000);

do {
    $result = runSimpleUpdateQuery($con, 'merchants', ['point_balance'], ["$new_merchant_points"], ['id'], ['='], [$merchant_id]);
    if ($result['err']['code']) {
        $flag = false;
        break;
    }

    $result = runSimpleUpdateQuery($con, 'agent', ['points'], ["$new_rec_points"], ['id'], ['='], ["'$agentid'"]);
    if ($result['err']['code']) {
        $flag = false;
        break;
    }

    $result = runSimpleInsertQuery($con, 'point_transfers', ['reference', 'point_worth', 'date_transacted', 'type_id', 'currency_id', 'from_whom', 'to_who', 'previous_balance_from', 'previous_balance_to'], ["'$ref'", "'$amount'", "'$date'", 5, 1, "'$id'", "'$agentid'", "'$new_merchant_points'", "'$new_rec_points'"]);
    if ($result['err']['code']) {
        $flag = false;
        break;
    }
    $insertedID = $result['insertedId'];

    $result = runSimpleInsertQuery($con, 'agent_log', ['action', 'info', 'date', 'agent_id'], ["'RECEIVED_POINTS_MERCHANT'", "'$insertedID'", "'$date'", $agentid]);
    if ($result['err']['code']) {
        $flag = false;
        break;
    }

    $result = runSimpleInsertQuery($con, 'merchants_recent_activity', ['activity_summary', 'datetimezone', 'activity_type', 'activity_id', 'merchant_id'], ["'" . json_encode(['agent_name' => $rec_name, 'agent_email' => $rec_email, 'points' => $amount]) . "'", "'$date'", '"POINTS_TRANSFERRED_TO_AGENT"', "'$insertedID'", $merchant_id]);
    if ($result['err']['code']) {
        $flag = false;
    }
} while (false);

