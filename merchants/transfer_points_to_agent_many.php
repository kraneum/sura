<?php

session_start();

require_once('../php/sura_config.php');
require_once('../php/sura_functions.php');
require_once('../php/file_extras.php');
require_once 'send_sms.php';

$con = makeConnection_merchant();
autoCommit($con, false);

if ($con) {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $id = $_SESSION['id'];
        $merchant_id = $_SESSION['company_id'];
        $date = (new DateTime())->format(DateTime::ISO8601);


        // retrieves employee info...
        $selfInfo = runSimpleFetchQuery($con, ['id', 'phone'], 'employees', ['id'], ['='], [$id], '', '', '')['result'];
        if (empty($selfInfo)) {
            echo json_encode(['status' => false, 'err' => 'EMPLOYEE_ID_NOT_RESOLVED']);
        }

        $invalid_numbers = '';
        $valid_numbers = '';
        $flag = true;
        $invalid_numbers_flag = false;
        $insufficient_funds_flag = false;

        if ($_FILES['list']['error'] == UPLOAD_ERR_OK && is_uploaded_file($_FILES['list']['tmp_name'])) {
            $list = fopen($_FILES['list']['tmp_name'], "r") or die("Unable to open file!");

            while (!feof($list)) {
                $row = explode(",", fgetss($list));
                $phone = filter_var($row[0], FILTER_SANITIZE_NUMBER_INT);
                $amount = intval(filter_var($row[1], FILTER_SANITIZE_NUMBER_INT));
                
                if (empty($phone) || empty($amount)) {
                    $invalid_numbers_flag = true;
                    $invalid_numbers .= ($phone . ', ');
                    continue;
                }

                // retrieves merchant info...
                $merchantInfo = runSimpleFetchQuery($con, ['id', 'name', 'point_balance'], 'merchants', ['id'], ['='], ["'$merchant_id'"], '', '', '')['result'];
                if (empty($merchantInfo)) {
                    echo json_encode(['status' => false, 'info' => 'MERCHANT_ID_NOT_RESOLVED']);
                }
                $merchant_points = intval($merchantInfo[0]['point_balance']);
                $merchant_name = $merchantInfo[0]['name'];


                // retrieves agent info...
                $recipientInfo = runSimpleFetchQuery($con, ['id', 'f_name', 'email', 'phone', 'points'], 'agent', ['phone'], ['='], ["'$phone'"], '', '', '')['result'];
                if (empty($recipientInfo)) {
                    $invalid_numbers_flag = true;
                    $invalid_numbers .= ($phone . '');
                    continue;
                } else {
                    $valid_numbers .= ($phone . '');
                }
                $agentid = $recipientInfo[0]['id'];
                $rec_points = intval($recipientInfo[0]['points']);
                $rec_name = $recipientInfo[0]['f_name'];
                $rec_email = $recipientInfo[0]['email'];
                $rec_number = $recipientInfo[0]['phone'];

                if ($amount <= 0) {
                    $invalid_numbers_flag = true;
                    $invalid_numbers .= ($phone . '');
                    continue;
                } else if ($amount > $merchant_points) {
                    $insufficient_funds_flag = true;
                    break;
                } else {

                    $new_rec_points = $rec_points + $amount;
                    $new_merchant_points = $merchant_points - $amount;

                    require 'transfer_points_to_agent_.php';

                    if ($flag) {
                        commit($con);
                        send_sms($rec_name, 'Hi, You were transferred ' . $amount . ' points from a Merchant' . ' - ' . $merchant_name, "'$rec_number'");
                    } else {
                        rollback($con);
                    }
                }
            }
            fclose($list);

            if ($flag) {
                $message['status'] = true;

                if ($insufficient_funds_flag) {
                    $message['info'] = 'SUCCESS_WITH_INSUFFICIENT_POINTS';
                    $message['valid_numbers'] = $valid_numbers;
                }
                if ($invalid_numbers_flag) {
                    $message['info'] = 'SUCCESS_WITH_INVALID_NUMBERS';
                    $message['invalid_numbers'] = $invalid_numbers;
                }

                $message['points'] = number_format($new_merchant_points);
            } else {
                $message['status'] = false;
                $message['info'] = 'ERROR';
            }
        }
    } else {
        $message['status'] = false;
        $message['err'] = 'WRONG_REQUEST_TYPE';
    }
} else {
    $message['status'] = false;
    $message['err'] = 'DATABASE_CONNECT_ERROR';
}

autoCommit($con, true);

disconnectConnection($con);

echo json_encode($message, JSON_PRETTY_PRINT);
