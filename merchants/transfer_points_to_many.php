<?php
header('Content-type:application/json;charset=utf-8');
session_start();

require_once('../php/sura_config.php');
require_once('../php/sura_functions.php');
require_once('../php/form_validate.php');

$con = makeConnection_merchant();

if ($con) {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $id = null;
        $merchant_id = null;
        $merchant_point_balance = null;
        $date = (new DateTime())->format(DateTime::ISO8601);

        if (isset($_SESSION['id']) && !empty($_SESSION['id'])) {
            $id = $_SESSION['id'];
            $merchant_id = $_SESSION['company_id'];
            $id = runSimpleFetchQuery($con, ['id'], 'employees', ['id'], ['='], [$id], '', '', '')['result'];
            $merchant_point_balance = runSimpleFetchQuery($con, ['point_balance'], 'merchants', ['id'], ['='], ["'$merchant_id'"], '', '', '')['result'];
            $merchant_point_balance = doubleval($merchant_point_balance);
        } else {
            $employee_email = $_POST['employee_email'];
            $employee_password = $_POST['employee_password'];
            $company_email = $_POST['company_email'];
            $merchant_info = runSimpleFetchQuery($con, ['id', 'point_balance'], 'merchants', ['contact_email'], ['='], ["'$company_email'"], '', '', '')['result'];
            $merchant_id = $merchant_info[0]['id'];
            $merchant_point_balance = doubleval($merchant_info[0]['point_balance']);
            $id = runSimpleFetchQuery($con, ['id'], 'employees', ['email', 'password', 'company_id'], ['=', '=', '='], ["'$employee_email'", "'$employee_password'", $merchant_id], '', '', '')['result'];
        }

        /////////////////////

        if (empty($id)) {
            echo json_encode(['status' => false, 'err' => 'EMPLOYEE_ID_NOT_RESOLVED']);
            disconnectConnection($con);
            exit();
        }
        if (empty($merchant_id)) {
            echo json_encode(['status' => false, 'err' => 'MERCHANT_ID_NOT_RESOLVED']);
            disconnectConnection($con);
            exit();
        }

        $rec_entity = 'user';
        $list = $_FILES['list'];
        $validationResult = $form_validate([
            'list' => 'filerequired|filemimetypes:text/csv,text/plain,application/csv,application/vnd.ms-excel,text/x-csv,text/comma-separated-values,application/csv,application/excel,application/vnd.msexcel,text/anytext'
                ], ['list' => $list]);

        if (!empty($validationResult)) {
            $message['err'] = 'VALIDATION_ERROR';
            $message['errVal'] = $validationResult;
        } else if ($list['size'] <= 0 || !$list['error'] == UPLOAD_ERR_OK || !is_uploaded_file($list['tmp_name'])) {
            $message['err'] = 'FILE_ERROR';
        } else {
            $_temp = str_getcsv(file_get_contents($list['tmp_name']), "\n");
            $arraylist = [];
            foreach ($_temp as $t) {
                $arraylist[] = explode(',', $t);
            }
            $count_arraylist = count($arraylist);
            if ($count_arraylist > 0) {
                $amount = 0;

                foreach ($arraylist as $row) {
                    if (is_numeric($row[1])) {
                        $amount += intval($row[1]);
                    } else {
                        echo json_encode(['status' => false, 'err' => 'AMOUNT_FORMAT_ERROR']);
                        disconnectConnection($con);
                        exit();
                    }
                }
                if ($amount > $merchant_point_balance) {
                    echo json_encode(['status' => false, 'err' => 'INSUFFICIENT_POINTS']);
                    disconnectConnection($con);
                    exit();
                }
            }

            $uploaddir = '../merchant_transfer_files/' . strtolower($rec_entity) . 's/';
            $list_name = date('Ymd') . '_' . $merchant_id . '_' . md5(pathinfo(basename($list["name"]), PATHINFO_FILENAME) . microtime());
            $uploadfile = $uploaddir . $list_name . '.' . pathinfo(basename($list["name"]), PATHINFO_EXTENSION);

            if (move_uploaded_file($list["tmp_name"], $uploadfile)) {
                $flag = true;
                $date = (new DateTime())->format(DateTime::ISO8601);

                $res = runSimpleInsertQuery($con, 'merchant_transfer_jobs', ['merchant_id', 'employee_id', 'rec_entity', 'list_name'], [$merchant_id, $id, "'$rec_entity'", "'$list_name'"]);
                if ($res['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }

                if ($flag) {
                    commit($con);
                    $message['status'] = true;
                    $message['message'] = 'SUCCESS';
                } else {
                    rollback($con);
                    $message['status'] = false;
                    $message['err'] = 'FAILURE';
                }
            } else {
                $message['err'] = 'UPLOAD_ERROR';
            }
        }
    } else {
        $message['status'] = false;
        $message['err'] = 'WRONG_REQUEST_TYPE';
    }
} else {
    $message['status'] = false;
    $message['err'] = 'DATABASE_CONNECT_ERROR';
}

autoCommit($con, true);

disconnectConnection($con);

echo json_encode($message, JSON_PRETTY_PRINT);
