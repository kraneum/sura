<?php

session_start();

require_once('../php/sura_config.php');
require_once('../php/sura_functions.php');

$con = makeConnection_merchant();

autoCommit($con, false);

if ($con) {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if (!$_POST) {
            return ['status' => false, 'err' => 'INVALID_DETAILS'];
        }

        $id = $_SESSION['id'];
        $merchant_id = $_SESSION['company_id'];
        $rec_email = $_POST['email'];
        $amount = $_POST['amount'];
        $date = (new DateTime())->format(DateTime::ISO8601);

        // retrieves receipient merchant info...
        $recipientInfo = runSimpleFetchQuery($con, ['id', 'name', 'point_balance'], 'merchants', ['contact_email'], ['='], ["'$rec_email'"], '', '', '')['result'];
        if (empty($recipientInfo)) {
            autoCommit($con, true);
            return ['status' => false, 'info' => 'ID_NOT_RESOLVED'];
        }
        $rec_id = $recipientInfo[0]['id'];
        $rec_points = intval($recipientInfo[0]['point_balance']);
        $rec_name = $recipientInfo[0]['name'];

        // retrieves merchant info...
        $merchantInfo = runSimpleFetchQuery($con, ['id', 'name', 'point_balance'], 'merchants', ['id'], ['='], ["'$merchant_id'"], '', '', '')['result'];
        if (empty($merchantInfo)) {
            autoCommit($con, true);
            return ['status' => false, 'info' => 'MERCHANT_ID_NOT_RESOLVED'];
        }
        $merchant_points = intval($merchantInfo[0]['point_balance']);
        $merchant_name = $merchantInfo[0]['name'];

        // retrieves employee info...
        $selfInfo = runSimpleFetchQuery($con, ['id', 'phone'], 'employees', ['id'], ['='], [$id], '', '', '')['result'];
        if (empty($selfInfo)) {
            autoCommit($con, true);
            return ['status' => false, 'info' => 'EMPLOYEE_ID_NOT_RESOLVED'];
        }

        if ($amount <= 0) {
            autoCommit($con, true);
            $message['info'] = 'INVALID_AMOUNT';
        } else if ($amount > $merchant_points) {
            autoCommit($con, true);
            $message['info'] = 'INSUFFICIENT_POINTS';
        } else {
            $new_rec_points = $rec_points + $amount;
            $new_merchant_points = $merchant_points - $amount;

            $flag = true;

            $ref = 'UICI-' . $insurer_id . round(microtime(true) * 1000);

            do {
                $result = runSimpleUpdateQuery($con, 'merchants', ['point_balance'], ["$new_merchant_points"], ['id'], ['='], [$merchant_id]);
                if ($result['err']['code']) {
                    $flag = false;
                    break;
                }

                $result = runSimpleUpdateQuery($con, 'merchants', ['point_balance'], ["$new_rec_points"], ['id'], ['='], ["'$rec_id'"]);
                if ($result['err']['code']) {
                    $flag = false;
                    break;
                }

                $result = runSimpleInsertQuery($con, 'point_transfers', ['reference', 'point_worth', 'date_transacted', 'type_id', 'currency_id', 'from_whom', 'to_who', 'previous_balance_from', 'previous_balance_to'], ["'$ref'", "'$amount'", "'$date'", 13, 1, "'$id'", "'$rec_id'", "'$new_merchant_points'", "'$new_rec_points'"]);
                if ($result['err']['code']) {
                    $flag = false;
                    break;
                }
                $insertedID = $result['insertedId'];

                $result = runSimpleInsertQuery($con, 'merchants_recent_activity', ['activity_summary', 'datetimezone', 'activity_type', 'activity_id', 'merchant_id'], ["'" . json_encode(['merchant_name' => $rec_name, 'merchant_email' => $rec_email, 'points' => $amount]) . "'", "'$date'", '"POINTS_TRANSFERRED_FROM_MERCHANT"', "'$insertedID'", $rec_id]);
                if ($result['err']['code']) {
                    $flag = false;
                    break;
                }

                $result = runSimpleInsertQuery($con, 'merchants_recent_activity', ['activity_summary', 'datetimezone', 'activity_type', 'activity_id', 'merchant_id'], ["'" . json_encode(['merchant_name' => $rec_name, 'merchant_email' => $rec_email, 'points' => $amount]) . "'", "'$date'", '"POINTS_TRANSFERRED_TO_MERCHANT"', "'$insertedID'", $merchant_id]);
                if ($result['err']['code']) {
                    $flag = false;
                }
            } while (false);

            if ($flag) {
                commit($con);
                $message['status'] = true;
                $message['info'] = 'SUCCESS';
                $message['points'] = number_format($new_merchant_points);
                require_once './send_mail.php';
                send_mail( $rec_email, 'iPoints Topup', 'Points have been transferred to your UICi Account from' . $merchant_name);
            } else {
                rollback($con);
                $message['status'] = false;
                $message['info'] = 'ERROR';
            }
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'WRONG_REQUEST_TYPE';
    }
} else {
    $response['status'] = false;
    $response['err'] = 'DATABASE_CONNECT_ERROR';
}

autoCommit($con, true);

disconnectConnection($con);

echo json_encode($message, JSON_PRETTY_PRINT);
