<?php

session_start();

require_once('../php/sura_config.php');
require_once('../php/sura_functions.php');
//require_once 'fragments/sms/sms_sender.php';

$con = makeConnection_merchant();

if ($con) {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if (!$_POST) {
            echo json_encode(['status' => false, 'err' => 'INVALID_DETAILS']);
            exit();
        }

        $id = $_SESSION['id'];
        $merchant_id = $_SESSION['company_id'];
        $phone = $_POST['phone'];
        $amount = $_POST['amount'];
        $date = (new DateTime())->format(DateTime::ISO8601);

        // retrieves user info...
        $recipientInfo = runSimpleFetchQuery($con, ['id', 'f_name', 'email', 'phone', 'health_point_balance', 'general_point_balance'], 'users', ['phone'], ['='], ["'$phone'"], '', '', '')['result'];
        if (empty($recipientInfo)) {
            echo json_encode(['status' => false, 'err' => 'ID_NOT_RESOLVED']);
            exit();
        }
        $userid = $recipientInfo[0]['id'];
        $rec_health_points = intval($recipientInfo[0]['health_point_balance']);
        $rec_general_points = intval($recipientInfo[0]['general_point_balance']);
        $rec_name = $recipientInfo[0]['f_name'];
        $rec_email = $recipientInfo[0]['email'];
        $rec_number = $recipientInfo[0]['phone'];

        // retrieves merchant info...
        $merchantInfo = runSimpleFetchQuery($con, ['id', 'name', 'point_balance'], 'merchants', ['id'], ['='], ["'$merchant_id'"], '', '', '')['result'];
        if (empty($merchantInfo)) {
            echo json_encode(['status' => false, 'err' => 'MERCHANT_ID_NOT_RESOLVED']);
            exit();
        }
        $merchant_points = intval($merchantInfo[0]['point_balance']);
        $merchant_name = $merchantInfo[0]['name'];

        // retrieves employee info...
        $selfInfo = runSimpleFetchQuery($con, ['id', 'phone'], 'employees', ['id'], ['='], [$id], '', '', '')['result'];
        if (empty($selfInfo)) {
            echo json_encode(['status' => false, 'err' => 'EMPLOYEE_ID_NOT_RESOLVED']);
            exit();
        }

        if ($amount <= 0) {
            $message['err'] = 'INVALID_AMOUNT';
        } else if ($amount > $merchant_points) {
            $message['err'] = 'INSUFFICIENT_POINTS';
        } else {
            $new_rec_health_points = $rec_health_points + ceil($amount / 2);
            $new_rec_general_points = $rec_general_points + floor($amount / 2);

            $new_merchant_points = $merchant_points - $amount;

            $flag = true;
            autoCommit($con, false);

            require 'transfer_points_to_user_.php';

            if ($flag) {
                commit($con);
                $message['status'] = true;
                $message['points'] = number_format($new_merchant_points);

                require 'send_sms.php';
                send_sms($rec_name, 'You were transferred ' . $amount . ' points from Merchant - ' . $merchant_name, "'$rec_number'", 234);
            } else {
                rollback($con);
                $message['status'] = false;
                $message['err'] = 'ERROR';
            }
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'WRONG_REQUEST_TYPE';
    }
} else {
    $response['status'] = false;
    $response['err'] = 'DATABASE_CONNECT_ERROR';
}

autoCommit($con, true);

disconnectConnection($con);

echo json_encode($message, JSON_PRETTY_PRINT);
