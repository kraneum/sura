<?php

$ref = 'UICI-' . $merchant_id . round(microtime(true) * 1000);

do {
    $result = runSimpleUpdateQuery($con, 'merchants', ['point_balance'], ["$new_merchant_points"], ['id'], ['='], [$merchant_id]);
    if ($result['err']['code']) {
        $flag = false;
        break;
    }

    $result = runSimpleUpdateQuery($con, 'users', ['health_point_balance', 'general_point_balance'], ["$new_rec_health_points", "$new_rec_general_points"], ['id'], ['='], ["'$userid'"]);
    if ($result['err']['code']) {
        $flag = false;
        break;
    }

    $new_rec_points = $new_rec_health_points + $new_rec_general_points;
    $result = runSimpleInsertQuery($con, 'point_transfers', ['reference', 'point_worth', 'date_transacted', 'type_id', 'currency_id', 'from_whom', 'to_who', 'previous_balance_from', 'previous_balance_to'], ["'$ref'", "'$amount'", "'$date'", 12, 1, "'$merchant_id'", "'$phone'", "'$new_merchant_points'", "'$new_rec_points'"]);
    if ($result['err']['code']) {
        $flag = false;
        break;
    }
    $insertedID = $result['insertedId'];

    $result = runSimpleInsertQuery($con, 'user_log', ['action', 'info', 'date', 'user_id'], ["'RECEIVED_POINTS_MERCHANT'", "'$insertedID'", "'$date'", $userid]);
    if ($result['err']['code']) {
        $flag = false;
        break;
    }

    $result = runSimpleInsertQuery($con, 'merchants_recent_activity', ['activity_summary', 'datetimezone', 'activity_type', 'activity_id', 'merchant_id'], ["'" . json_encode(['user_name' => $rec_name, 'user_email' => $rec_email, 'points' => $amount]) . "'", "'$date'", '"POINTS_TRANSFERRED_TO_USER"', "'$insertedID'", $merchant_id]);
    if ($result['err']['code']) {
        $flag = false;
    }
} while (false);
