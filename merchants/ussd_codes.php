<?php

class UssdCodes {

    const charset = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    const sanitizeMap = ['o' => '0', 'l' => '1', 'i' => '1', 'L' => '1', 'I' => '1', 's' => '5', 'z' => '5', 'b' => '6', 'P' => '9', 'p' => '9'];

    public static function load($ussdCode) {
        $ussdCode = self::santize($ussdCode);
        
        
    }

    public static function santize($ussdCode) {
        //When santizing pins convert o to 0, l or i to 1, s to 5 etc
        $ussdCodeCt = strlen($ussdCode);
        for($idx = 0; $idx < $ussdCodeCt; ++$idx){
            in_array($ussdCode[$idx], self::sanitizeMap) && ($ussdCode[$idx] = self::sanitizeMap[$ussdCode[$idx]]);
        }
    }

    public static function generate($ussdCode2Gen, $ussdCodeLength, $modifier) {
        $charsetLength = count(self::charset);
        $generateUssdCodes = [];
        $ussdCode;
        $generateUssdCodesCt;
        $ussdCode2GenHere = $ussdCode2Gen;

        $php_memory_limit = ini_get('memory_limit');
        $php_max_exec_time = ini_get('max_execution_time');
        //$start = microtime(true);
        ini_set('max_execution_time', -1);
        ini_set('memory_limit', '90G'); //Fine tune this value

        do {
            for ($ct = 0; $ct < $ussdCode2GenHere; ++$ct) {
                $ussdCode = $modifier;
                for ($ct0 = 0; $ct0 < $ussdCodeLength; ++$ct0) {
                    $ussdCode .= self::charset[mt_rand(0, $charsetLength - 1)];
                }

                $generateUssdCodes[] = $ussdCode;
            }
        } while (($generateUssdCodesCt = count($generateUssdCodes = array_unique($generateUssdCodes, SORT_STRING))) < $ussdCode2Gen && $ussdCode2GenHere = ($ussdCode2Gen - $generateUssdCodesCt));

        ini_set('MAX_EXECUTION_TIME', $php_max_exec_time);
        ini_set('memory_limit', $php_memory_limit);
        //echo (microtime(true) - $start) . '<br/>';
        //echo 'Ussd codes: ' . count($generateUssdCodes);

        return $generateUssdCodes;
    }
}
