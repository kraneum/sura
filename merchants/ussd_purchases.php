<?php

session_start();

if (!isset($_SESSION['id'])) {
    exit();
}
!$_SESSION['approved'] && exit('');
//To prevent sql injection
//htmlspecialchars(strip_tags(trim()));
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    require_once '../php/form_validate.php';
    $response = ['err' => null];
    $data = json_decode(file_get_contents("php://input"), JSON_OBJECT_AS_ARRAY);
//use mysqli offset
    $cleanedUserInputMap = array_filter(
            array_map(function($value) {
                return htmlspecialchars(strip_tags((trim($value))));
            }, $data), function($value, $key) {
        return strlen($value) && in_array($key, ['page']);
    }, ARRAY_FILTER_USE_BOTH);

    $validationResult = $form_validate([
        'page' => 'required|digits'
            ], $cleanedUserInputMap);

    if (empty($validationResult)) {
        require_once '../php/sura_config.php';
        require_once '../php/sura_functions.php';

        $con = makeConnection();

        if (+$cleanedUserInputMap['page'] === 1) {
            $runFetchUssdPurchasesCtResult = runFetchUssdPurchasesCt($con, $_SESSION['company_id']);

            if (!$runFetchUssdPurchasesCtResult['err']['code']) {
                $response['result']['ct'] = $runFetchUssdPurchasesCtResult['result'];
            } else {
                $response['result'] = null;
                $response['err'] = ['error' => 'DB', 'msg' => 'An error occured, please try again'];
            }
        }

        if (!$response['err']) {
            $ussdPurchasesResult = ussdPurchases($con, $_SESSION['company_id'], $cleanedUserInputMap['page']);

            if (!$ussdPurchasesResult['err']['code']) {
                $response['result']['rows'] = $ussdPurchasesResult['result'];
            } else {
                $response['result'] = null;
                $response['err'] = ['error' => 'DB', 'msg' => 'An error occured, please try again'];
            }
        }

        //Use table joins to get the purchases summary for the ussd purchases from the merchant_recent_activities
//Use join, make activity_id index-
    } else {
        $response['result'] = null;
        $response['err'] = ['error' => 'VALIDATION', 'msg' => $validationResult];
    }

    echo json_encode($response);
}