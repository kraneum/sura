<?php
require_once './fragments/header.php';
?>

<link href="../css/jquery-ui.min_datepicker.css" rel="stylesheet" type="text/css"/>
<div id="home-main" class="container">
    <h2 class="text-center">Please submit required documents</h2>
    <div class="text-center"><small style="color: red">(must be in pdf or jpeg format)</small></div><br>
    <div class="row">
        <div id='documentsFormContainer' class="col-sm-8">
            <div id="documentsMsg"></div>
            <form id="documentsForm" method="post" action="documents_e.php" enctype="multipart/form-data">
                <?php
                $thrSthToUpload = false;
                $documentsDetails = '<div class="list-group">';
                //0-not uploaded,1-approved,2-not approved/pending,3-rejected
                $documntContextInfo = [['list-group-item-info', 'Not uploaded'], ['list-group-item-success', 'Approved'], ['list-group-item-warning', 'Pending'], ['list-group-item-danger', 'Rejected, Please review and re-upload']];
                foreach ($_SESSION['documents'] as $docmnt => $apprvdStat) {
                    $documentsDetails .= '<a href="#" id="' . $docmnt . '-detail" class="list-group-item ' . $documntContextInfo[$apprvdStat][0] . '">' . str_replace('_', ' ', $docmnt) . '<span id="' . $docmnt . '-badge" class="badge">' . $documntContextInfo[$apprvdStat][1] . '</span></a>';
                    if (!$apprvdStat || +$apprvdStat === 3) {
                        $thrSthToUpload = true;
                        $title_of_doc = 'CAC Documents';
//                        if ($docmnt == 'doc1') {
//                            $title_of_doc = 'CAC Documents';
//                        } else if ($docmnt == 'doc2') {
//                            $title_of_doc = 'Some document';
//                        } else if ($docmnt == 'doc3') {
//                            $title_of_doc = 'Some other document';
//                        } else if ($docmnt == 'doc4') {
//                            $title_of_doc = 'Last document';
//                        }
                        echo '<div id="' . $docmnt . '" class="form-group"><label for="' . $docmnt . '">' . $title_of_doc . '</label><input class="form-control" name="' . $docmnt . '"  type="file" data-parsley-filemaxmegabytes="8" data-parsley-trigger="change"  accept="image/jpeg, image/jpg,image/png, application/pdf, application/x-pdf, application/acrobat, applications/vnd.pdf, text/pdf, text/x-pdf" data-parsley-filemimetypes="image/jpeg, image/jpg,image/png, application/pdf,application/x-pdf,application/acrobat,applications/vnd.pdf,text/pdf,text/x-pdf"/></div>';
                    }
                }
                $documentsDetails .= '</div>';

                if ($thrSthToUpload) {
                    echo '<div id="documentsSave" class="form-group"><button id="documentsSave" type="submit" class="btn btn-sm btn-primary" name="documentsSave">Save</button></div>';
                }
                ?>
            </form>
        </div>
        <div id='documentsDetailsContainer' class="col-sm-4 <?php echo $thrSthToUpload ? 'pull-right' : '' ?>">
            <h3>Documents detail</h3>
            <div><?php echo $documentsDetails; ?></div>
        </div>
    </div>
</div>
<?php
require_once './fragments/footer.php';
?>
<script src="../js/parsley-file-validators.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
//        document.getElementById('pageNavLoc').innerHTML = '<li class="active">DOCUMENTS</li>';
        $('#documentsForm').parsley().on('form:submit', function () {
            //window.scrollTo(0, 0);
            document.getElementById('documentsMsg').innerHTML = 'Uploading document(s)...';
            if (FormData) {
                $.ajax({
                    type: "POST",
                    url: "documents_e.php",
                    data: new FormData(document.getElementById("documentsForm")),
                    dataType: 'JSON',
                    /*** Options to tell JQuery not to process data or worry about content-type ****/
                    cache: false,
                    contentType: false,
                    processData: false,
                    /****************************************/
                    success: function (data) {
                        if (!data.err) {
                            if (!data.result.emptyDocs) {
                                document.getElementById('documentsForm').remove();
                                document.getElementById('documentsDetailsContainer').classList.remove('pull-right');
//                                document.getElementById('profileNeedToUpldCt').innerHTML = '';
//                                document.getElementById('profileNeedToUpldCt').style.display = 'none';
                            } else {
                                data.result.docs.forEach(function (doc) {
                                    document.getElementById(doc).remove();
                                });
//                                document.getElementById('profileNeedToUpldCt').innerHTML = data.result.emptyDocs;
                            }

                            data.result.docs.forEach(function (doc) {
                                var docId = doc + '-detail';

                                document.getElementById(docId).classList.remove('list-group-item-info');
                                document.getElementById(docId).classList.remove('list-group-item-danger');
                                document.getElementById(docId).classList.add('list-group-item-warning');

                                document.getElementById(doc + '-badge').innerHTML = 'Pending';
                            });

                            document.getElementById('documentsMsg').innerHTML = '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Documents uploaded.</div>';
                        } else {
                            var errMsg;

                            switch (data.err.error) {
                                case 'NODOCUMENTS':
                                    errMsg = data.err.msg;
                                    break;
                                case 'VALIDATION':
                                    errMsg = data.err.msg.message;
                                    document.getElementById(data.err.msg.field).classList.add('parsley-error');
                                    break;
                                case 'IMAGEUPLOADERROR':
                                    errMsg = data.err.msg;
                                    break;
                                case 'ALREADYAPPROVED':
                                    errMsg = data.err.msg;
                                    break;
                                case 'DB':
                                    errMsg = data.err.msg;
                                    break;
                                default:
                                    break;
                            }

                            document.getElementById('documentsMsg').innerHTML = '<div class="alert alert-warning alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + errMsg + '</div>';
                        }
                    }, error: function () {
                        document.getElementById('documentsMsg').innerHTML = '<div class="alert alert-warning alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Problem uploading documents, please try again.</div>';
                    }, complete: function () {

                    }
                });

                return false;
            }
        });

        $('#logout').click(function () {
            swal({
                title: "Are you sure?",
                text: 'Clicking "OK" will log you out of the system',
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(function (result) {
                if (result) {
                    window.location = "logout.php";
                }
            });
        });
    });
</script>
</body>
</html>