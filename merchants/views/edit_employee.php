<?php
require_once './fragments/header.php';
?>
<link href="../css/jquery-ui.min_datepicker.css" rel="stylesheet" type="text/css"/>
<div id="home-main" class="container">
    <span id="validationMessage"></span>
    <?php
    echo '<form id ="regForm" method="post" action="' . htmlspecialchars($_SERVER['PHP_SELF']) . '" enctype="multipart/form-data">
            <input type="hidden" name="id" value="' . $id . '"><input type="hidden" name="employee" value="' . htmlspecialchars(serialize($employee)) . '">
            <div class="form-group">
                <label for="name">First Name </label>
                <input class="form-control" id="first_name" name="first_name" type="text" placeholder="' . ($employee ? $employee['first_name'] : 'Enter employee\'s first name') . '" maxlength="100"/>
            </div>

            <div class="form-group">
                <label for="write_up">Middle Name</label>
                <input class="form-control" id="middle_name" name="middle_name" placeholder="' . ($employee ? $employee['middle_name'] : 'Enter employee\'s middle name') . '" maxlength="100"/>
            </div>
            
            <div class="form-group">
                <label for="name">Last Name </label>
                <input class="form-control" id="last_name" name="last_name" type="text" placeholder="' . ($employee ? $employee['last_name'] : 'Enter employee\'s last name') . '" maxlength="100"/>
            </div>
            
            <div class="form-group">
                <label for="phone">Date of birth </label>
                <div class="input-group">
                    <input class="form-control" id="dob" name="dob" placeholder="' . ($employee ? $employee['dob'] : 'Enter the date of birth') . '" data-date-format="MM/DD/YYYY" maxlength="10"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>            
            </div>

            <div class="form-group">
                <label for="phone">Phone </label>
                <input class="form-control" type="phone" id="phone" name="phone" placeholder="' . ($employee ? $employee['phone'] : 'Enter the phone number') . '" data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$" maxlength="50"/>
            </div>

            <div class="form-group">
                <label for="country">Nationality </label>
                <select class="form-control" id="country" name="country">
' . array_reduce(["Afghanistan","Aland Islands","Albania","Algeria","American Samoa","Andorra","Angola","Anguilla","Antarctica","Antigua and Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bonaire, Saint Eustatius and Saba ","Bosnia and Herzegovina","Botswana","Bouvet Island","Brazil","British Indian Ocean Territory","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central African Republic","Chad","Chile","China","Christmas Island","Cocos Islands","Colombia","Comoros","Cook Islands","Costa Rica","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Democratic Republic of the Congo","Denmark","Djibouti","Dominica","Dominican Republic","East Timor","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Guiana","French Polynesia","French Southern Territories","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guadeloupe","Guam","Guatemala","Guernsey","Guinea","Guinea-Bissau","Guyana","Haiti","Heard Island and McDonald Islands","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Ivory Coast","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macao","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Martinique","Mauritania","Mauritius","Mayotte","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauru","Nepal","Netherlands","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Niue","Norfolk Island","North Korea","Northern Mariana Islands","Norway","Oman","Pakistan","Palau","Palestinian Territory","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Pitcairn","Poland","Portugal","Puerto Rico","Qatar","Republic of the Congo","Reunion","Romania","Russia","Rwanda","Saint Barthelemy","Saint Helena","Saint Kitts and Nevis","Saint Lucia","Saint Martin","Saint Pierre and Miquelon","Saint Vincent and the Grenadines","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Sint Maarten","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Georgia and the South Sandwich Islands","South Korea","South Sudan","Spain","Sri Lanka","Sudan","Suriname","Svalbard and Jan Mayen","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Togo","Tokelau","Tonga","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Turks and Caicos Islands","Tuvalu","U.S. Virgin Islands","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States","United States Minor Outlying Islands","Uruguay","Uzbekistan","Vanuatu","Vatican","Venezuela","Vietnam","Wallis and Futuna","Western Sahara","Yemen","Zambia","Zimbabwe"], function($carry, $item)use($employee) {
        return $carry . '<option' . ($employee['country'] === $item ? ' selected' : '') . ' value="' . $item . '">' . $item . '</option>';
    }, '') . '<select>
            </div>
            ' . (empty($employee['SUPER']) ? '
            <div class="form-group">
                <label for="">User permissions</label>
                <div class="checkbox">
                    <label><input name="user_permissions[]" type="checkbox" ' . (!empty($employee['VIEW']) ? 'checked="checked"' : '') . ' value="VIEW">view</label>
                </div>
                <div class="checkbox">
                    <label><input name="user_permissions[]" type="checkbox" ' . (!empty($employee['CREATE']) ? 'checked="checked"' : '') . ' value="CREATE">create</label>
                </div>
                <div class="checkbox">
                    <label><input name="user_permissions[]" type="checkbox" ' . (!empty($employee['UPDATE']) ? 'checked="checked"' : '') . ' value="UPDATE">update</label>
                </div>
                <div class="checkbox">
                    <label><input name="user_permissions[]" type="checkbox" ' . (!empty($employee['DELETE']) ? 'checked="checked"' : '') . ' value="DELETE">delete</label>
                </div>
            </div>
' : '') . '
            <div class="form-group">
                <label for="password">Password </label>
                <input class="form-control" id="password" name="password"  type="password" placeholder="" maxlength="20"/>
            </div>
            
            <div class="form-group">
                <label for="password_confirm">Password confirm</label>
                <input class="form-control" id="password_confirm" name="password_confirm"  type="password" placeholder="" maxlength="20" data-parsley-equalto="#password" data-parsley-equalto-message="Password mismatch"/>
            </div>
            
            <div class="form-group">
                <label for="country">Security question</label>
                <select class="form-control" id="security_question" name="security_question">
' . array_reduce($employee['security_questions'], function($carry, $item)use($employee) {
        return $carry . '<option' . ($employee['security_question'] === $item ? ' selected' : '') . ' value="' . $item . '">' . $item . '</option>';
    }, '') . '            <select>
            </div>
            
            <div class="form-group">
                <label for="security_answer">Security answer</label>
                <input class="form-control" id="security_answer" name="security_answer"  type="text" placeholder="' . ($employee ? $employee['security_answer'] : 'Type security answer') . '" maxlength="100"/>
            </div>
            
            <div class="form-group">
                <label for="display_picture">Display picture * </label>
                <input class="form-control" id="display_picture" name="display_picture"  type="file" placeholder="Choose file" accept="image/jpeg, image/png" data-parsley-filemaxmegabytes="2" data-parsley-trigger="change" data-parsley-dimensions="true" data-parsley-dimensions-options=\'{"min_width": "100","min_height": "100"}\' data-parsley-filemimetypes="image/jpeg, image/png"/>
            </div>

            <div class="form-group"> 
                <a href="employees.php" class="btn btn-primary"><i class="fa fa-arrow-left btn-icon-left"></i>Back</a>
                <button id="btn_register" type="submit" class="btn btn-success formButtons"  name="btn_register"><i class="fa fa-save btn-icon-left"></i>Save</button>
            </div>
        </form>';
    ?>
</div>
<?php
require_once './fragments/footer.php';
?>
<script src="../js/jquery-ui.min_datepicker.js" type="text/javascript"></script>
<script src="../js/parsley-laraextras.min.js" type="text/javascript"></script>
<script src="../js/parsley-file-validators.js" type="text/javascript"></script>
<script src="./views/js/edit_employee.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        Parsley.addMessages('en', {
            dimensions: 'The display picture dimensions should be a minimum of 100px by 100px'
        });
    });
<?php 
if (isset($_POST['btn_register'])) {
    echo (!empty($operationStatus['display_picture_changed']) ? 'document.getElementById("profileDpImg").src = document.getElementById("profileDpImg").src.substring(0,document.getElementById("profileDpImg").src.lastIndexOf(\'/\'))+"/'.$employee['display_picture_file_name'].'"+"?"+Date.now();' : ''). 'document.getElementById("validationMessage").innerHTML = ' .
    ($operationStatus['err'] === false ? '"<center><strong>Admin successfully edited</strong></center>"' : '"' . $operationStatus['message'] . '";' . ($operationStatus['type'] === 'validationError' ? 'document.getElementById("' . $operationStatus['field'] . '").style.borderColor="red"' : ''));
}
?>

    $(document).ready(function () {
        $('#regForm').parsley();
    });
</script>
</body>
</html>

