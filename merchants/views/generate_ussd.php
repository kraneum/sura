<?php
require_once './fragments/header.php';
?>
<div id="home-main" class="container">
    <div style="margin-bottom: 30px;" class="row">
        <div class="col-sm-6">
            <h4><strong><i class="fa fa-exchange"></i> Generate Pin codes</strong></h4>
            <small>&emsp;&emsp;You can also generate them in various pin values</small>
        </div>
        <div class="col-sm-6">
            <span class="pull-right">Point Balance: <strong id="pointBalance"><?php echo $pointBalance ?></strong> iPoints</span>
        </div>
    </div>
    <div id="ussdFormInfo" class="text-center" style="margin-left: 20%; margin-right: 20%;"></div>
    <form class="form-style" id="ussdValueGenerateForm">
        <div id="ussdFieldsContainer" style="margin-bottom: 5%"></div>
        <div class="row">
            <div class="col-xs-6">This sale: <i id="thisSale">0</i></div>
            <div class="col-xs-6">Point balance after this sale: <i id="pointBalAftThisSale"><?php echo $pointBalance; ?></i></div>
        </div>
        <div>
            <div class="row">
                <div class="col-xs-12 small text-muted">By clicking "Generate", you agree to our <span style="text-decoration: underline">Terms</span> and <span style="text-decoration: underline">Privacy Policy</span></div>
            </div>
            <div class="row" style="margin-top: 20px;">
                <div class="col-xs-6 pull-right" style="padding-right:120px">
                    <span class="switch">
                        <input id="cards" type="checkbox" name="cards" value="1">
                        <label for="cards">Order scratch cards</label>
                    </span>
                    <a href="view_ussd.php" class="btn btn-primary"><i class="fa fa-arrow-left btn-icon-left"></i>Back</a>
                    <input id="generate" type="submit" value="Generate" class="btn btn-lg btn-success">
                </div>
            </div>
        </div>
    </form>
</div>
<div style="margin-top: 11.6%;"></div>
<?php
require_once './fragments/footer.php';
?>
<script src="../js/to_fixed.js" type="text/javascript"></script>
<script src="./views/js/ussd_value_generate_form.js" type="text/javascript"></script>
<script src="./views/js/generate_ussd.js" type="text/javascript"></script>