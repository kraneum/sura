<?php
require_once './fragments/header_h.php';
require_once './fragments/_buy_points_card.php';

date_default_timezone_set('Africa/Lagos');
?>

<script src="../js/working_alert.js" type="text/javascript"></script>
<script src="./views/js/home.js" type="text/javascript"></script>

<link href="../css/timeline-custom.css" rel="stylesheet" type="text/css"/>

<div id="home-main" class="container">
    <div id="north" class="row">
        <div class="col-sm-3">
            <div id="north-sidebar">
                <div class="notice notice-success text-center">
                    <strong>iPoint Balance:</strong>&nbsp;<span id="pointBalance" style="font-weight: bold;"><?php echo $pointBalance; ?> </span>  iPoints &nbsp;
                    <a id="pointsButton" type = "button" class = "btn btn-success btn-sm" data-toggle = "modal" data-target = "#pointsModal"
                       style="margin-top: 10px;">
                        Buy more iPoints
                    </a>
                </div>
    <!--                <span class="span-title"> Overview</span>-->
                <hr/>
                <div class="box-shadow--2dp s-sidebar">
                    <img class="center-block" src=<?php echo '"../img/merchants/' . $merchant['display_picture'] . '"'; ?> 
                         alt=<?php echo '"' . $merchant['name'] . 'logo"'; ?> style="width:100%">
                    <div>
                        <h2 class="text-center"><?php echo $merchant['name']; ?>
                            <small style="font-size: 12px"> RC: <?php echo $merchant['rc_number']; ?></small></h2>
                        <h6><?php echo $merchant['write_up']; ?></h6>
                        <!--<h6>Signup date: <?php echo (new DateTime($merchant['date_joined']))->format('jS F, Y'); ?></h6>-->
                        <h6><?php // echo $merchant['contact_phone'];       ?></h6>
                        <h6><?php // echo $merchant['contact_email'];       ?></h6>
                        <!--<h6>Office address: <?php echo $merchant['head_office_address']; ?></h6>-->
                    </div>
                </div>
            </div>
            <div id="south">
                <div id="south-sidebar">
                    <span class="span-title">RECENT REDEMPTIONS</span>
                    <hr/>
                    <div class="box-shadow--2dp s-sidebar">
                        <?php
                        echo array_reduce($last5redemptions, function($carry, $item) {
                            return $carry . '<span style="font-size:14px;"><strong>' . ($item['code']) . '</strong><span class="pull-right"><strong>' . ($item['point_worth']) . ' POINTS</strong></span></span><br><span style="font-size:18px;"><span class="text-muted">' . date("d-m-y", strtotime($item['redeemed_date'])) . '</span></span><br><br>';
                        }, '')
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="north-content" class="col-sm-8 col-sm-offset-1">
            <span class="span-title">RECENT ACTIVITY</span>
            <!-- Modal -->
            <hr/>
            <div class="box-shadow--2dp content-white" id="recentActivitiesContainer"></div>
        </div>
    </div>

    <div id="workingAlert" style="display:none; position:fixed;width:100px;z-index:990;top:0;left: 50%;box-shadow:0 2px 4px rgba(0,0,0,0.2);height:30px;border-color: #f0c36d;text-align: center;background-color: #f9edbe;color:#333;font-weight:bold;">Working...</div>
</div>

<?php require_once './fragments/footer_f.php'; ?>

<script src="../js/working_alert.js" type="text/javascript"></script>
<script src="./views/js/home.js" type="text/javascript"></script>
<script src="./views/js/buy_points.js" type="text/javascript"></script>

</body>
</html>

