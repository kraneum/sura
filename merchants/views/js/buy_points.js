/*jslint browser: true*/
/*global $ alert working_alert moment, moment */

$(document).ready(function () {

    $("#buyPointsForm").parsley();

    var pay_rate = parseInt($("input[id=pay_rate]").val());

    $('body').on('input', 'input[name=pay_amt]', function (event) {
        var amt = parseInt($(this).val());
        $(this).next().children('span').html(amt + (amt * pay_rate / 100));
    });

    $('#pointsModal').on('submit', '#buyPointsForm', function (e) {
        e.preventDefault();

        if (parseInt($('input[name=pay_amt]').val()) > 0) {
            $('#pointsModal').modal('hide');
            payWithPaystack();
        }
    });

    function payWithPaystack() {
        var amt = parseInt($("input[name=pay_amt]").val());
        var commission = amt * (pay_rate / 100);
        var totalAmt = (amt + commission) * 100;
        var ref = 'UICI-' + $("input[id=pay_id]").val() + moment().unix();

        var payload = {
            'ref': ref,
            'amount': amt
        };

        var handler = PaystackPop.setup({
            key: 'pk_test_bb835543a20ab7491632daf4aecffde31b029f40',
            email: $("input[name=pay_email]").val(),
            amount: totalAmt,
            ref: ref,
            callback: function (response) {
                working_alert.start();
                if (response.reference) {
                    $.post('buy_points_card.php', payload, function (data) {
                        var info = '';
                        if (data.status) {
                            info = '<div class="alert alert-dismissible alert-success text-center btn-rounded">' +
                                    'Points Purchased successfully</div>';
                            $('#pointBalance').html(data.points);
                        } else {
                            if (data.info === 'ERROR') {
                                info = '<div class="alert alert-dismissible alert-danger text-center btn-rounded">' +
                                        'Points Purchase failed</div>';
                            }
                        }

                        $('#info').html(info).stop().slideDown().delay(3000).slideUp();
                        working_alert.finish();
                    }, 'JSON').fail(function () {});
                } else {
                    alert('naah');
                }
            },
            onClose: function () {}
        });
        handler.openIframe();
    }

});