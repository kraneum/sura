'use strict';

//Stuff to export
var employees = {};

$(function () {

    /****************** Logout Event Handlers *****************/
    $('#logout').click(function () {
        swal({
            title: "Are you sure?",
            text: 'Clicking "OK" will log you out of the system',
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function (result) {
            if (result) {
                window.location = "logout.php";
            }
        });
    });
    //Keep global variables inside here
    var config = {
        suspendIdPrefix: 'spd',
        kickOutIdPrefix: 'ko',
        deleteIdPrefix: 'del',
        employeesInfoContainer: $('#employeesInfoContainer'),
        employeeCtClass: 'employeeCt',
        emailTdIdPrefix: 'eM'
    };

    var globvars = {
        suspendBusy: false,
        kickoutBusy: false,
        delBusy: false
    };

    /**********************************************************************************************************************************/
    //Bind event handlers
    /**********************************************************************************************************************************/
    config.employeesInfoContainer.on('click', "[id |= '" + config.suspendIdPrefix + "']", function () {
        globvars.suspendBusy || suspend($(this));
    }).on('click', "[id |= '" + config.kickOutIdPrefix + "']", function () {
        globvars.kickoutBusy || kickout($(this));
    }).on('click', "[id |= '" + config.deleteIdPrefix + "']", function () {
        confirm('Are you sure you want to delete this admin?') && (globvars.delBusy || del($(this)));
    });

    /****************************************************************************************************************************/
    //Private functions
    /****************************************************************************************************************************/
    var kickout = function (element) {
        element.prop('disabled', true);
        globvars.kickoutBusy = true;
        working_alert.start();
        //Optimistic operation, always assumed successful, unless ntwk fails
        var id = element.prop('id').split('-')[1];

        $.ajax({
            type: "POST",
            url: "kickout_employee.php",
            data: JSON.stringify({id: id, email: $('#' + config.emailTdIdPrefix + '-' + id).html()}),
            success: function (data, textStatus, jqXHR) {
                if (textStatus === 'success') {
                    switch (data['status']) {
                        case 'SUCCESS':
                            //Success
                            break;
                        case 'SESSIONNOTFOUND':
                        case 'CANNOTKICKOUTANOTHERCOMPANYEMPLOYEE':
                        case 'INSUFFICIENTPRIVILEDGESTOKICKOUT':
                        case 'DBERROR':
                            //Failed
                            break;
                        default:
                            //No response
                            break;
                    }
                } else {
                    //Fail
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {

            },
            complete: function () {
                setTimeout(function () {
                    element.prop({disabled: false, checked: false});
                }, 200);
                globvars.kickoutBusy = false;
                working_alert.finish();
            },
            dataType: 'JSON'
        });
    };

    var suspend = function (element) {
        element.prop('disabled', true);
        globvars.suspendBusy = true;
        working_alert.start();

        var checked = element.prop('checked'), id = element.prop('id').split('-')[1];

        $.ajax({
            type: "POST",
            url: "suspend_employee.php",
            data: JSON.stringify({opr: +checked, id: id, email: $('#' + config.emailTdIdPrefix + '-' + id).html()}),
            success: function (data, textStatus, jqXHR) {
                if (textStatus === 'success') {
                    switch (data['status']) {
                        case 'SUCCESS':
                            //Success
                            break;
                        case 'EMPLOYEENOTFOUNDORINSUFFICIENTPRIVILEDGESORNOCHANGES':
                        case 'DBERROR':
                            //Failed
                            setTimeout(function () {
                                element.prop('checked', !checked);
                            }, 500);
                            break;
                        default:
                            //No response
                            setTimeout(function () {
                                element.prop('checked', !checked);
                            }, 500);
                            break;
                    }
                } else {
                    //Failed
                    setTimeout(function () {
                        element.prop('checked', !checked);
                    }, 500);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                setTimeout(function () {
                    element.prop('checked', !checked);
                }, 500);
            },
            complete: function () {
                setTimeout(function () {
                    element.prop('disabled', false);
                    globvars.suspendBusy = false;
                }, 500);
                working_alert.finish();
            },
            dataType: 'JSON'
        });
    };

    var del = function (element) {
        element.prop('disabled', true);
        globvars.delBusy = true;
        working_alert.start();

        var id = element.prop('id').split('-')[1];

        $.ajax({
            type: "POST",
            url: "delete_employee.php",
            data: JSON.stringify({id: id, email: $('#' + config.emailTdIdPrefix + '-' + id).html()}),
            success: function (data, textStatus, jqXHR) {
                if (textStatus === 'success') {
                    switch (data['status']) {
                        case 'SUCCESS':
                            //Success
                            element.parents('tr').first().remove();
                            $('.' + config.employeeCtClass).each(function (index, element) {
                                $(element).html(index + 1);
                            });
                            break;
                        case 'EMPLOYEENOTFOUNDORINSUFFICIENTPRIVILEDGESORNOCHANGES':
                        case 'DBERROR':
                            //Failed
                            break;
                        default:
                            //No response
                            break;
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {

            },
            complete: function () {
                setTimeout(function () {
                    element.prop('disabled', false);
                    globvars.delBusy = false;
                }, 500);
                working_alert.finish();
            },
            dataType: 'JSON'
        });


    };

//    $('#register_employee').click(function (e) {
//        e.preventDefault();
//    });

});