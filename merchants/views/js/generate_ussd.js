'use strict';

//Stuff to export
var generate_ussd = {};

$(function () {

    /****************** Logout Event Handlers *****************/
    $('#logout').click(function () {
        swal({
            title: "Are you sure?",
            text: 'Clicking "OK" will log you out of the system',
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function (result) {
            if (result) {
                window.location = "logout.php";
            }
        });
    });
    //Keep global variables inside here
    var config = {
        pageNavLocId: 'pageNavLoc',
        navMenuId: 'navMenu',
        pointBalanceId: 'pointBalance',
        ussdValueGenerateFormId: 'ussdValueGenerateForm',
        ussdFormInfo: $('#ussdFormInfo')
    };

    var globvars = {};

    /**********************************************************************************************************************************/
    //Main
    /**********************************************************************************************************************************/
//    document.getElementById(config.pageNavLocId).innerHTML = '<li><a href="./view_ussd.php">PIN CODES</a></li><li class="active">GENERATE CODES</li>';
    $('#' + config.navMenuId).find('li > a').each(function () {
        if ($(this).html() === 'PIN CODES') {
            $(this).append('<span class="sr-only">(current)</span>').addClass('active');
            return false;
        }
    });

    /**********************************************************************************************************************************/
    //Bind event handlers
    /**********************************************************************************************************************************/
    //To block the form from submiting and reloading the page
    $('#' + config.ussdValueGenerateFormId).submit(function (event) {
        event.preventDefault();
    });
    $('#' + config.ussdValueGenerateFormId).parsley().on('form:success', function () {
        generateUSSD();
    });

    /****************************************************************************************************************************/
    //Private functions
    /****************************************************************************************************************************/
    var generateUSSD = function () {
        var ussdPurchase = ussd_value_generate_form.ussdPurchase();

        if (Object.keys(ussdPurchase).length > 1) {
            config.ussdFormInfo.html('');


            if (!ussd_value_generate_form.pointIsInsufficient()) {
                var maxUssdCodes = 40320;

                if (ussd_value_generate_form.getTotalNumOfUssdCodes() <= maxUssdCodes) {
                    ussd_value_generate_form.freeze();

                    $.ajax({
                        type: "POST",
                        url: "purchase_ussd.php",
                        data: JSON.stringify(ussd_value_generate_form.ussdPurchase()),
                        success: function (data, textStatus, jqXHR) {
                            if (textStatus === 'success') {
                                if (!data.err) {
                                    //create target _blank anchor link and click on the anchor link
                                    /*var element = document.createElement('a');
                                     element.setAttribute('href', './get_ussd_file.php?criteria=purchase_id&format=csv&searchValue=' + data.result);
                                     element.setAttribute('target', '_blank');
                                     
                                     element.style.display = 'none';
                                     document.body.appendChild(element);
                                     
                                     element.click();
                                     
                                     document.body.removeChild(element);*/
                                    window.open('get_ussd_file.php?criteria=purchase_id&format=csv&searchValue=' + data.result);

                                    document.getElementById(config.pointBalanceId).innerHTML = ussd_value_generate_form.getPointBalAftThisSale();
                                    ussd_value_generate_form.reset();
                                    config.ussdFormInfo.html('<div class="alert alert-success">Success!</div>');
                                } else {
                                    var errMsg;

                                    switch (data['err']['error']) {
                                        case 'VALIDATION':
                                            errMsg = data['err']['msg']['message'];
                                            break;
                                        case 'USSDCODESGENERROR':
                                            errMsg = 'Problem with ussdcodes, try spliting the ussdcodes generation into batches';
                                            break;
                                        case 'INSUFFICIENT_POINTS':
                                            errMsg = 'You dont have sufficient points to make this purchase, please topup more points';
                                            break;
                                        case 'MAX_PIN_GENERATION_EXCEEDED':
                                            errMsg = data['err']['msg'];
                                            break;
                                        case 'DB':
                                        case 'GENERIC':
                                        case 'NOTFOUND':
                                        case 'NOTCOMMITED':
                                            errMsg = 'A problem is preventing your purchase from completing';
                                            break;
                                        default:
                                            errMsg = 'Pls try again';
                                            break;
                                    }
                                    config.ussdFormInfo.html('<div class="alert alert-warning"><strong>Problem! </strong>' + errMsg + '</div>');
                                }
                            } else {
                                config.ussdFormInfo.html('<div class="alert alert-danger">Pls try again</div>');
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            config.ussdFormInfo.html('<div class="alert alert-danger">Pls try again</div>');
                        },
                        complete: function () {
                            ussd_value_generate_form.free();
                        },
                        dataType: 'JSON'
                    });
                } else {
                    config.ussdFormInfo.html('<div class="alert alert-warning"><strong>Max Pins Exceeded!</strong> You can\'t generate more than ' + maxUssdCodes + ' pins at a time, please split your pin generation into batches</div>');
                }
            } else {
                config.ussdFormInfo.html('<div class="alert alert-warning"><strong>Insufficient points!</strong> You need extra ' + toFixed(Math.abs(ussd_value_generate_form.getPointBalAftThisSale())) + ' Points to make this puchase</div>');
            }
        }
    };

    /****************************************************************************************************************************/
    //Private classes
    /****************************************************************************************************************************/

});