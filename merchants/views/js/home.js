
//Stuff to export
var employees = {};

$(function () {

    /****************** Logout Event Handlers *****************/
    $('#logout').click(function () {
        swal({
            title: "Are you sure?",
            text: 'Clicking "OK" will log you out of the system',
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function (result) {
            if (result) {
                window.location = "logout.php";
            }
        });
    });
    //Keep file variables inside here
    var config = {
        recentActivitiesContainer: $('#recentActivitiesContainer'),
        recentActivityRowPrefix: 'rAR',
        recentActivityColPrefix: 'rAC',
        loadRecentActivitiesDownBtnId: 'loadRecentActivitiesDownBtn',
        loadRecentActivitiesDownId: 'loadRecentActivitiesDown',
        //activityIconColors: ['bg-primary', 'bg-secondary', 'bg-success', 'bg-info', 'bg-warning', 'bg-danger']
    };

    var globvars = {
        loadRecentActivityBusy: false,
        firstRecentActivityId: null,
        lastRecentActivityId: null,
        currentDateRow: null,
        //activityLastIconColor: -1,
        recentActivityIsLoaded: false
    };

    /**********************************************************************************************************************************/
    //Main
    /**********************************************************************************************************************************/
    setTimeout(function () {
        loadRecentActivity();
    }, 0);

    /**********************************************************************************************************************************/
    //Bind event handlers
    /**********************************************************************************************************************************/
    config.recentActivitiesContainer.on('click', '#' + config.loadRecentActivitiesDownBtnId, function () {
        loadDownRecentActivity();
    });

    /****************************************************************************************************************************/
    //Private functions
    /****************************************************************************************************************************/
    var loadRecentActivity = function () {
        fetchAndDisplayRecentActivity();
    };
    var loadUpRecentActivity = function () {
        fetchAndDisplayRecentActivity(globvars.firstRecentActivityId, 1);
    };
    var loadDownRecentActivity = function () {
        fetchAndDisplayRecentActivity(globvars.lastRecentActivityId, 0);
    };
    var displayRecentActivity = function (recentActivities, direction) {
        if (recentActivities.length) {
            !globvars.recentActivityIsLoaded && (config.recentActivitiesContainer.html(''), globvars.recentActivityIsLoaded = true);

            var activityString, dateObj, meridian, hr, min, displayHTML = '', lastDateRow, dateRow, activityRowBeginIndex, activityRowEndIndex, activityRowBeginString, activityRowEndString, activityRowId, activityColId, months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];

            $('#' + config.loadRecentActivitiesDownId).remove();

            for (var idx = 0, recentActivity, recentActivitiesCt = recentActivities.length; idx < recentActivitiesCt; ++idx) {
                recentActivity = recentActivities[idx];

                dateObj = new Date(recentActivity.datetimezone);

                dateRow = dateObj.toDateString().replace(new RegExp(' ', 'g'), '_');

                activityRowId = config.recentActivityRowPrefix + dateRow;
                activityColId = config.recentActivityColPrefix + dateRow;

                activityRowBeginString = '<div class="row" id="' + activityRowId + '"><div class="col-xs-1" style="padding-right: 0;"><div id="date" class="text-center pull-right" style="padding-top: 10px;"><span style="font-size: 20px; letter-spacing: 1px;">' + dateObj.getFullYear() + '</span><br><span style="font-size: 12px; letter-spacing: 1px;">' + months[dateObj.getMonth()] +' '+ dateObj.getDate() + '</span></div></div><div class="col-xs-11"><div id="' + activityColId + '" class="timeline-centered">';
                activityRowEndString = '</div></div></div>';

                meridian = dateObj.getHours() > 12 ? (hr = dateObj.getHours() - 12) && 'pm' : (hr = dateObj.getHours() || 1) && 'am';
                min = dateObj.getMinutes();
                min < 10 && (min = '0' + min);

                activityString = '<article class="timeline-entry"><div class="timeline-entry-inner"><div class="timeline-icon ' + (function () {
                    switch (recentActivity.activity_type) {
                        case 'ADMIN_ACTIONS':
                            return 'bg-info';
                        case 'PINS_GENERATED':
                            return 'bg-secondary';
                        case 'POINTS_PURCHASE':
                            return 'bg-success';
                        case 'POINTS_TRANSFERRED':
                            return 'bg-secondary';
                    }
                })() + '"><i class="entypo-feather"></i></div><div class="timeline-label"><h2 style="font-weight: bold;">' + recentActivity.activity_type.replace(new RegExp('_', 'g'), ' ') + '</h2><p>' + display_activity_summary[recentActivity.activity_type](JSON.parse(recentActivity.activity_summary), hr + ':' + min + meridian) + '</p></div></div></article>';

                if (document.getElementById(activityRowId)) {
                    !direction ? $('#' + activityColId).append(activityString) : $('#' + activityColId).prepend(activityString);
                } else if (lastDateRow === dateRow) {
                    if (!direction) {
                        activityRowEndIndex = displayHTML.lastIndexOf(activityRowEndString);
                        displayHTML = displayHTML.substring(0, activityRowEndIndex) + activityString + activityRowEndString;
                    } else {
                        activityRowBeginIndex = displayHTML.indexOf(activityRowBeginString);
                        displayHTML = displayHTML.substring(0, activityRowBeginIndex + activityRowBeginString.length) + activityString + displayHTML.slice(activityRowBeginIndex + activityRowBeginString.length);
                    }
                } else {
                    displayHTML += activityRowBeginString + activityString + activityRowEndString;
                }

                lastDateRow = dateRow;
            }

            displayHTML.length && !direction ? config.recentActivitiesContainer.append(displayHTML) : config.recentActivitiesContainer.prepend(displayHTML);
            $('#' + activityColId).append('<div class="timeline-centered" id="' + config.loadRecentActivitiesDownId + '"><article class="timeline-entry begin"><div class="timeline-entry-inner"><div id="' + config.loadRecentActivitiesDownBtnId + '" class="timeline-icon" style="-webkit-transform: rotate(-90deg); -moz-transform: rotate(-90deg);cursor:pointer;"><i class="entypo-flight fa fa-plus" style="position: relative; top: -20px; color: grey"></i></div></div></article></div>');
                        
        } else {
            !config.recentActivitiesContainer.html().length && config.recentActivitiesContainer.html('<div class="alert alert-info">No recent activity</div>');
        }
    };

    var fetchAndDisplayRecentActivity = function (id, direction) {
        if (globvars.loadRecentActivityBusy) {
            return;
        }

        globvars.loadRecentActivityBusy = true;
        working_alert.start();

        $.ajax({
            type: "POST",
            url: "get_recent_activity.php",
            data: JSON.stringify({id: id, direction: direction}),
            success: function (data, textStatus, jqXHR) {
                if (textStatus === 'success') {
                    if (!data.err) {
                        var firstLoad = !globvars.lastRecentActivityId;

                        data.result.length && (id ? !direction ? globvars.lastRecentActivityId = data.result[data.result.length - 1].id : globvars.firstRecentActivityId = data.result[0].id : globvars.firstRecentActivityId = data.result[0].id, globvars.lastRecentActivityId = data.result[data.result.length - 1].id);

                        displayRecentActivity(data.result, direction);
                        //Scroll to the bottom of the page
                        !firstLoad && window.scrollTo(0, document.body.scrollHeight);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {

            },
            complete: function () {
                globvars.loadRecentActivityBusy = false;
                working_alert.finish();
            },
            dataType: 'JSON'
        });
    };

    var display_activity_summary = {
        PINS_GENERATED: function (activity_summary, time) {
            var details = activity_summary.details;

            return activity_summary.user + ' generated ' + Object.keys(details).reduce(function (carry, key, index) {
                return carry + (index ? ', ' : '') + details[key] + ' pin codes of ' + key + ' value';
            }, '') + '. Total pins generated ' + activity_summary.totalPins + ' pins of total value ' + activity_summary.totalValue + ' iPoints. ' + (!activity_summary.orderCards ? 'Generated' : 'Ordered') + ' at ' + time + '<br/><a target="_blank" href="get_ussd_file.php?criteria=purchase_id&format=csv&searchValue=' + activity_summary.purchaseId + '">Download</a>';
        }, POINTS_PURCHASE: function (activity_summary, time) {
            return activity_summary.user + ' purchased ' + activity_summary.points + ' iPoints at ' + time;
        }, ADMIN_ACTIONS: function (activity_summary, time) {
            var op = activity_summary.admin;

            switch (activity_summary.summary) {
                case 'logout':
                    op += ' loggedout user ' + activity_summary.user;
                    break;
                case 'delete':
                    op += ' deleted user ' + activity_summary.user;
                    break;
                case 'suspend':
                    op += ' ' + (!+activity_summary.details.suspend ? 'un' : '') + 'suspended user ' + activity_summary.user;
                    break;
                case 'edit':
                    op += ' edited user ' + activity_summary.user + '.<br/>Changes: ';

                    for (var detail in activity_summary.details) {
                        op += '<br/>' + detail.replace(new RegExp('_', 'g'), ' ') + (detail !== 'user_permissions' ? (activity_summary.details[detail] ? ' to ' + activity_summary.details[detail] : '') : (function () {
                            var perms = {'VIEW': 1, 'CREATE': 2, 'UPDATE': 4, 'DELETE': 8}, newPerms = '';
                            for (var perm in perms) {
                                (activity_summary.details[detail] & perms[perm]) && (newPerms += (newPerms.length ? ', ' : '') + perm.toLowerCase());
                            }
                            return ' to ' + newPerms;
                        })());
                    }
                    break;
                case 'register':
                    op += ' created user ' + activity_summary.user;
                    break;
            }

            return op += '.<br>At ' + time;
        }, POINTS_TRANSFERRED_TO_AGENT: function (activity_summary, time) {
            return 'Agent ' + activity_summary.agent_name + ' (' + activity_summary.agent_email + ')' + ' was transferred ' + activity_summary.points + ' iPoints at ' + time;
        }, POINTS_TRANSFERRED_TO_USER: function (activity_summary, time) {
            return 'User ' + activity_summary.user_name + ' (' + activity_summary.user_email + ')' + ' was transferred ' + activity_summary.points + ' iPoints at ' + time;
        }, POINTS_TRANSFERRED_TO_MERCHANT: function (activity_summary, time) {
            return 'Merchant ' + activity_summary.merchant_name + ' (' + activity_summary.merchant_email + ')' + ' was transferred ' + activity_summary.points + ' iPoints at ' + time;
        }, UICI_ACTIONS: function (activity_summary, time) {
            var op = activity_summary.admin;

            switch (activity_summary.summary) {

                case 'admin_invalidate':
                    op += ' invalidated your account';
                    break;
                case 'admin_validate':
                    op += ' validated your account';
                    break;
                case 'admin_approve':
                    op += ' approved your account';
                    break;
                case 'admin_disapprove':
                    op += ' disapproved your account';
                    break;
            }

            return op += '.<br>At ' + time;
        }
    };

    /****************************************************************************************************************************/
    //Private classes
    /****************************************************************************************************************************/


});