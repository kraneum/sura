/********* Notifications *******************/
if ($('#notif-icon #notifications.dropdown-menu').data('notifcount') === 0) {
    getNotifications(false);
} else {
    getNotifications(true);
}

$('body').on('click', '#notif_footer', function () {
    $('#notifications.dropdown-menu > li .fa-spinner').show();

    var formData = {
        'notifIDs': $('#notifications.dropdown-menu').data('notifids')
    };
    $.post('../php/suracommands.php?command=update_notifications_merchant&web', formData, function (data) {
        data = JSON.parse(data);

        if (data['info'] === 'SUCCESS') {
            $('#notifications.dropdown-menu > li .fa-spinner').hide();
            var notifCount = data['notifCount'];
            if (notifCount === 0) {
                $('#notifCount').remove();
            } else {
                $('#notifCount').html(notifCount);
            }
            getNotifications(true);
        } else {
            console.log('Failure');
        }
    });
});

function getNotifications(flag) {
    var content = '';
    var notifIDs = '';
    if (flag === true) {
        $.get('../php/suracommands.php?command=get_notifications_merchant&limit=10&web', function (data) {
            var notifs = JSON.parse(data);
            var notifCount = notifs.length;

            if (notifCount > 0) {
                for (var i = 0; i < notifCount; i++) {
                    i === 0 ? '' : notifIDs += ',';
                    notifIDs += notifs[i]['id'];
                    content += '<li class="divider notif-temp"></li>';
                    content += '<li class="text-center notif-temp">' + notifs[i]['message'] + '</li>';
                }

                content += '<li class="divider notif-temp" id="last_divider" style="margin-bottom: 0"></li>';
                content += '<li id="notif_footer" class="text-center notif-temp" style="margin: 0"><strong>Mark as Read</strong></li>';
            } else {
                content += '<li class="divider"> </li>';
                content += '<li id="no_new_notif" class="text-center">No new notifications</li>';
            }

        }).done(function () {
            $('#notifications.dropdown-menu > li .fa-spinner').hide();
            $('.notif-temp').remove();
            $('#notifications.dropdown-menu').append(content);
            $('#notifications.dropdown-menu').data('notifids', notifIDs);
            $('#notifications.dropdown-menu .divider:nth-child(2)').css('margin-top', '5px');
        });
    } else {
        content += '<li class="divider"> </li>';
        content += '<li id="no_new_notif" class="text-center">No new notifications</li>';
        $('#notif-icon #notifications.dropdown-menu > li .fa-spinner').hide();
        $('#notifications.dropdown-menu').append(content);
        $('#notifications.dropdown-menu #notif_header').css('padding-bottom', '0');
        $('#notifications.dropdown-menu .divider:nth-child(2)').css('margin-top', '5px');
    }

}