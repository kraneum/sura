'use strict';

//Stuff to export
var register_employee = {};

$(function () {

    /****************** Logout Event Handlers *****************/
    $('#logout').click(function () {
        swal({
            title: "Are you sure?",
            text: 'Clicking "OK" will log you out of the system',
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function (result) {
            if (result) {
                window.location = "logout.php";
            }
        });
    });
    //Keep global variables inside here
    var config = {
        pageNavLocId: 'pageNavLoc',
        navMenuId: 'navMenu',
        userPermissionsChkbx: 'user_permissions[]',
        regFormId: 'regForm'
    };

    var globvars = {};

    /**********************************************************************************************************************************/
    //Main
    /**********************************************************************************************************************************/
    document.getElementById(config.pageNavLocId).innerHTML = '<li><a href="./employees.php">ADMINS</a></li><li class="active">CREATE ADMINS</li>';
    $('#' + config.navMenuId).find('li > a').each(function () {
        if ($(this).html() === 'ADMINS') {
            $(this).append('<span class="sr-only">(current)</span>').addClass('active');
            return false;
        }
    });
   


    /**********************************************************************************************************************************/
    //Bind event handlers
    /**********************************************************************************************************************************/
    $('#' + config.regFormId)/*.on('click', 'input[name="' + config.userPermissionsChkbx + '"][value="ALL"]', function () {
        var checked = $(this).prop('checked');
        $('input[name="' + config.userPermissionsChkbx + '"][value!="ALL"]').prop({'checked': checked, 'disabled': checked});
    })*/.parsley();
    $("#dob").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+00",
        dateFormat: "dd/mm/yy"
    });

    /****************************************************************************************************************************/
    //Private functions
    /****************************************************************************************************************************/

});