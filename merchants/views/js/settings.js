'use strict';

//Stuff to export
var settings = {};

$(function () {

    /****************** Logout Event Handlers *****************/
    $('#logout').click(function () {
        swal({
            title: "Are you sure?",
            text: 'Clicking "OK" will log you out of the system',
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function (result) {
            if (result) {
                window.location = "logout.php";
            }
        });
    });
    //Keep global variables inside here
    var config = {
        profileFormId: 'profileForm',
        rstPasswdFormId: 'rstPasswdForm',
        rstSecQuestionFormId: 'rstSecQuestionForm',
        pageNavLocId: 'pageNavLoc',
        passwdRstId: 'passwdRst',
        secQuestionRstId: 'secQuestionRst',
        formInfo: $('#formInfo'),
        settingsTablistId: '#settingsTablist  a',
        settingsTablistLiParent: $('#settingsTablist a').parent('li'),
        acctBackClass: 'acctBack',
        settingsTablistAccount: $('#settingsTablistAccount'),
        rstPasswdValidationMessage: $('#rstPasswdValidationMessage'),
        rstSecQuestionValidationMessage: $('#rstSecQuestionValidationMessage')
    };

    var globvars = {
        formDefaultValues: {
            first_name: null,
            last_name: null,
            middle_name: null,
            dob: null,
            phone: null,
            country: null
        }
    };

    /**********************************************************************************************************************************/
    //Main
    /**********************************************************************************************************************************/
    document.getElementById(config.pageNavLocId).innerHTML = '<li class="active">SETTINGS</li>';
    for (var field in globvars.formDefaultValues) {
        globvars.formDefaultValues[field] = $('#' + field).prop('placeholder') || $('#country').val();
    }

    /**********************************************************************************************************************************/
    //Bind event handlers
    /**********************************************************************************************************************************/
    $(config.settingsTablistId).add('#' + config.passwdRstId).add('#' + config.secQuestionRstId).click(function () {
        config.settingsTablistLiParent.removeClass('active');
        $(this).tab('show');
    });
    $('.' + config.acctBackClass).click(function () {
        config.settingsTablistAccount.trigger('click');
    });
    //To block the form from submiting and reloading the page
    $('#' + config.profileFormId).add('#' + config.rstPasswdFormId).add('#' + config.rstSecQuestionFormId).submit(function (event) {
        event.preventDefault();
    });
    $('#' + config.profileFormId).parsley().on('form:success', function () {
        editEmployee();
    });
    $('#' + config.rstPasswdFormId).parsley().on('form:success', function () {
        passwdRst();
    });
    $('#' + config.rstSecQuestionFormId).parsley().on('form:success', function () {
        secQuestionRst();
    });
    /*$('#' + config.passwdRstId).click(function (event) {
     event.preventDefault();
     passwdRst();
     });
     $('#' + config.secQuestionRstId).click(function (event) {
     event.preventDefault();
     secQuestionRst();
     });*/
    $("#dob").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+00",
        dateFormat: "dd/mm/yy"
    });

    /****************************************************************************************************************************/
    //Private functions
    /****************************************************************************************************************************/
    var editEmployee = function () {
        document.body.scrollTop = 150;
        config.formInfo.html('');

        if (!FormData) {
            $('#' + config.profileFormId).trigger('submit');
            return;
        }

        var val = null, formdata = new FormData(), employee = {}, displayPicture;
        ['first_name', 'last_name', 'middle_name', 'dob', 'phone', 'country'].forEach(function (field) {
            (val = $('#' + field).val()) && (val !== globvars.formDefaultValues[field]) && (employee[field] = val) && formdata.append(field, val);
        });

        (displayPicture = document.getElementById('display_picture').files[0]) && formdata.append('display_picture', displayPicture);

        if (Object.keys(employee).length || displayPicture) {
            working_alert.start();

            $.ajax({
                type: "POST",
                url: "edit_employee_e.php",
                data: formdata,
                success: function (data, textStatus, jqXHR) {
                    if (textStatus === 'success') {
                        data['display_picture_changed'] && (document.getElementById("profileDpImg").src = document.getElementById("profileDpImg").src.substring(0,document.getElementById("profileDpImg").src.lastIndexOf('/'))+"/"+data['display_picture_changed'] + "?" + Date.now());

                        if (!data['err']) {
                            config.formInfo.html('<div class="alert alert-success">Success!</div>');

                            for (var field in employee) {
                                globvars.formDefaultValues[field] = employee[field];
                            }
                        } else {
                            var errMsg;

                            switch (data['err']['error']) {
                                case 'VALIDATION':
                                    errMsg = data['err']['msg']['message'];
                                    break;
                                case 'DB':
                                    errMsg = 'Problem saving your data, pls try again';
                                    break;
                                case 'NOCHANGES':
                                    errMsg = 'There were no changes';
                                    break;
                                case 'IMAGEUPLOADERROR':
                                    errMsg = 'The display picture upload failed';
                                    break;
                                default:
                                    errMsg = 'Pls try again';
                                    break;
                            }
                            config.formInfo.html('<div class="alert alert-warning"><strong>Problem! </strong>' + errMsg + '</div>');
                        }
                    } else {
                        config.formInfo.html('<div class="alert alert-danger">Pls try again</div>');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    config.formInfo.html('<div class="alert alert-danger">Pls try again</div>');
                },
                complete: function () {
                    working_alert.finish();
                },
                dataType: 'JSON',
                //Options to tell JQuery not to process data or worry about content-type
                cache: false,
                contentType: false,
                processData: false
            });
        } else {
            config.formInfo.html('<div class="alert alert-warning"><strong>Problem! </strong>There were no changes</div>');
        }
    };

    var passwdRst = function () {
        config.rstPasswdValidationMessage.html('');
        $('#old_password').parsley().removeError('password', {updateClass: true});
        working_alert.start();

        $.ajax({
            type: "POST",
            url: "ch_passwd.php",
            data: JSON.stringify({old_password: $('#old_password').val(), password: $('#password').val(), password_confirm: $('#password_confirm').val()}),
            success: function (data, textStatus, jqXHR) {
                if (textStatus === 'success') {
                    if (!data['err']) {
                        config.rstPasswdValidationMessage.html('<div class="alert alert-success">Success!</div>');
                    } else {
                        var errMsg;

                        switch (data['err']['error']) {
                            case 'VALIDATION':
                                errMsg = data['err']['msg']['message'];
                                break;
                            case 'DB':
                                errMsg = 'Problem saving your data, pls try again';
                                break;
                            case 'NOCHANGES':
                                errMsg = 'The password is incorrect';
                                $('#old_password').parsley().addError('password', {message: errMsg, assert: true, updateClass: true});
                                break;
                            default:
                                errMsg = 'Pls try again';
                                break;
                        }
                        config.rstPasswdValidationMessage.html('<div class="alert alert-warning"><strong>Problem! </strong>' + errMsg + '</div>');
                    }
                } else {
                    config.rstPasswdValidationMessage.html('<div class="alert alert-danger">Pls try again</div>');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                config.rstPasswdValidationMessage.html('<div class="alert alert-danger">Pls try again</div>');
            },
            complete: function () {
                working_alert.finish();
            },
            dataType: 'JSON'
        });

    };

    var secQuestionRst = function () {
        config.rstSecQuestionValidationMessage.html('');
        working_alert.start();

        $.ajax({
            type: "POST",
            url: "ch_sec_question.php",
            data: JSON.stringify({security_question: $('#security_question').val(), security_answer: $('#security_answer').val()}),
            success: function (data, textStatus, jqXHR) {
                if (textStatus === 'success') {
                    if (!data['err']) {
                        config.rstSecQuestionValidationMessage.html('<div class="alert alert-success">Success!</div>');
                    } else {
                        var errMsg;

                        switch (data['err']['error']) {
                            case 'VALIDATION':
                                errMsg = data['err']['msg']['message'];
                                break;
                            case 'DB':
                                errMsg = 'Problem saving your data, pls try again';
                                break;
                            case 'NOCHANGES':
                                errMsg = 'There were no changes';
                                break;
                            default:
                                errMsg = 'Pls try again';
                                break;
                        }
                        config.rstSecQuestionValidationMessage.html('<div class="alert alert-warning"><strong>Problem! </strong>' + errMsg + '</div>');
                    }
                } else {
                    config.rstSecQuestionValidationMessage.html('<div class="alert alert-danger">Pls try again</div>');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                config.rstSecQuestionValidationMessage.html('<div class="alert alert-danger">Pls try again</div>');
            },
            complete: function () {
                working_alert.finish();
            },
            dataType: 'JSON'
        });
    };

    /****************************************************************************************************************************/
    //Private classes
    /****************************************************************************************************************************/

});