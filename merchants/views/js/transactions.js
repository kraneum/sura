/*jslint browser: true*/
/*global $ alert working_alert moment, moment */

$(document).ready(function () {

    /****************** Logout Event Handlers *****************/
    $('#logout').click(function () {
        swal({
            title: "Are you sure?",
            text: 'Clicking "OK" will log you out of the system',
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function (result) {
            if (result) {
                window.location = "logout.php";
            }
        });
    });

    var config = {};
    var globvars = {
        limit: 10
    };

    ////////////////////////////////////////////////////////////

    $('body').on('click', 'ul.nav-tabs #point_transfer_tab', function () {
        setTimeout(function () {
            fetchPointTransfers($('#pointTransferFilter').serialize() + '&limit=' + globvars.limit);
        }, 250);
    });
    $('body').on('click', 'ul.nav-tabs #pin_redemption_tab', function () {
        setTimeout(function () {
            fetchPinRedemptions(globvars.limit, 0);
        }, 250);
    });
    $('body').on('click', 'ul.nav-tabs #point_usage_tab', function () {
        setTimeout(function () {
            fetchPointUsage($('#pointUsageFilter').serialize() + '&limit=' + globvars.limit);
        }, 250);
    });
    $('body').on('submit', '#pointUsageFilter', function (event) {
        fetchPointUsage($(this).serialize() + '&limit=' + globvars.limit);
        $('.bc-wrapper > input').val('');
        event.preventDefault();
    });
    $('body').on('change', '#pointUsageFilter select', function () {
        fetchPointUsage($('#pointUsageFilter').serialize() + '&limit=' + globvars.limit);
    });
    $('body').on('change', '#pointUsageFilter .input-daterange input', function () {
        var datesDiv = $('#pointUsageFilter .input-daterange');
        if (moment(datesDiv.find('input[name=startDate]').val(), 'MM/DD/YYYY').
                isAfter(moment(datesDiv.find('input[name=endDate]').val(), 'MM/DD/YYYY'))) {
            $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                    '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                    '<strong>Date Filter Error</strong>. Start Date Must be less than End Date' +
                    '</div>').stop().slideDown().delay(3000).slideUp();
        } else {
            fetchPointUsage($('#pointUsageFilter').serialize() + '&limit=' + globvars.limit);
        }
    });
    $('body').on('submit', '#pointTransferFilter', function (event) {
        fetchPointTransfers($(this).serialize() + '&limit=' + globvars.limit);
        $('.bc-wrapper > input').val('');
        event.preventDefault();
    });

    $('body').on('change', '#pointTransferFilter select, #pointTransferFilter .input-daterange input', function () {
        var datesDiv = $('#pointTransferFilter .input-daterange');
        if (moment(datesDiv.find('input[name=startDate]').val(), 'MM/DD/YYYY').
                isAfter(moment(datesDiv.find('input[name=endDate]').val(), 'MM/DD/YYYY'))) {
            $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                    '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                    '<strong>Date Filter Error</strong>. Start Date Must be less than End Date' +
                    '</div>').stop().slideDown().delay(3000).slideUp();
        } else {
            fetchPointTransfers($('#pointTransferFilter').serialize() + '&limit=' + globvars.limit);
        }

    });

    

    /////////////////////////////////////////////////////////////

    fetchPointPurchaseCount();
    $('.input-daterange input').each(function () {
        $(this).datepicker({
            clearBtn: true,
            autoclose: true
        });
    });
    $('#location').select2();
    $('#pagination2').pagination({
        items: 1,
        itemsOnPage: globvars.limit,
        cssStyle: 'light-theme',
        onPageClick: function (pageNumber) {
            fetchPointTransfers($('#pointTransferFilter').serialize() + '&limit=' + globvars.limit + '&offset=' + globvars.limit * (pageNumber - 1));
        }
    });
    $('#pagination3').pagination({
//        items: 10,
        itemsOnPage: globvars.limit,
        cssStyle: 'light-theme',
        edges: 0,
        displayedPages: 1,
        onPageClick: function (pageNumber) {
            fetchPinRedemptions(globvars.limit, globvars.limit * (pageNumber - 1));
        }
    });
    $('#pagination4').pagination({
        items: 1,
        itemsOnPage: globvars.limit,
        cssStyle: 'light-theme',
        onPageClick: function (pageNumber) {
            fetchPointUsage($('#pointUsageFilter').serialize() + '&limit=' + globvars.limit + '&offset=' + globvars.limit * (pageNumber - 1));
        }
    });


    /////////////////////////////////////////////////////////


    function fetchPointPurchaseCount() {
        $.getJSON('../php/suracommands.php?command=get_point_purchase_count&entity=merchant&id=null', function (data) {
            $('#pagination1').pagination({
                items: data,
                itemsOnPage: globvars.limit,
                cssStyle: 'light-theme',
                edges: 0,
                displayedPages: 1,
                onPageClick: function (pageNumber) {
                    fetchPointPurchases(globvars.limit, globvars.limit * (pageNumber - 1));
                }
            });
            fetchPointPurchases(globvars.limit, 0);
        });
    }

    function fetchPointPurchases(limit, offset) {
        working_alert.start();
        $.getJSON('../php/suracommands.php?command=get_point_purchases_by_id&entity=merchant&id=null&limit=' + limit + '&offset=' + offset, function (data) {
            var content = '';
            for (var i = 0; i < data.length; i++) {
                content += '<tr>'
//                        + ' <td>' + (i + 1) + '</td>'
                        + ' <td>' + data[i]['reference'] + '</td>'
                        + ' <td>' + data[i]['point_amount'] + '</td>'
                        + ' <td>' + data[i]['previous_balance'] + '</td>'
                        + ' <td>' + moment(data[i]['date_bought']).format('ddd, MMM Do YYYY, h:mm A') + '</td>'
                        + '</tr>';
            }

            $('#point_purchase tbody').html(content);
            working_alert.finish();
        }).fail(function () {

        });
    }

    function fetchPointTransfers(specs) {
        working_alert.start();
        $.getJSON('../php/suracommands.php?command=get_point_transfers_by_id&id=null&' + specs, function (data) {
            var content = '';
            for (var i = 0; i < data.length - 1; i++) {
                content += '<tr>'
                        + ' <td>' + data[i]['reference'] + '</td>'
                        + ' <td>' + data[i]['point_worth'] + '</td>'
                        + ' <td>' + moment(data[i]['date_transacted']).format('ddd, MMM Do YYYY, h:mm A') + '</td>';
                if (data[i]['type_id'] === '5') {
                    content += ' <td>' + data[i]['to_who'] + ':<small>' + data[i]['entity'] + '</small></td>';
                } else if (data[i]['type_id'] === '13') {
                    content += ' <td>' + data[i]['entity_ref'] + ':<small>' + data[i]['entity'] + '</small></td>';
                }

                content += '</tr>';
            }

            $('#point_transfer tbody').html(content);
            working_alert.finish();
            $('#point_transfer_page_info > span').text(data[data.length - 1].count + ' Persons'); // for number of persons
            $('#pagination2').pagination('updateItems', data[data.length - 1].count);
        }).fail(function () {

        });
    }

    function fetchPinRedemptions(limit, offset) {
        working_alert.start();
        $.getJSON('../php/suracommands.php?command=get_point_redemptions_by_id&entity=merchant&id=null&limit=' + limit + '&offset=' + offset, function (data) {
            var content = '';
            for (var i = 0; i < data.length; i++) {
                content += '<tr>'
                        + ' <td>' + data[i]['id'] + '</td>'
                        + ' <td>' + data[i]['code'] + '</td>'
                        + ' <td>' + data[i]['point_worth'] + '</td>'
                        + ' <td>' + data[i]['phone'] + '</td>'
                        + ' <td>' + moment(data[i]['redeemed_date']).format('ddd, MMM Do YYYY, h:mm A') + '</td>'
                        + '</tr>';
            }

            $('#pin_redemption tbody').html(content);
            working_alert.finish();
        }).fail(function () {

        });
    }

    function fetchPointUsage(specs) {
        working_alert.start();
        $.getJSON('../php/suracommands.php?command=get_point_usages_by_id&id=null&' + specs, function (data) {

            var content = '';
            for (var i = 0; i < data.length - 1; i++) {
                content += '<tr>'
                        + ' <td>' + data[i]['reference'] + '</td>'
                        + ' <td>' + data[i]['point_worth'] + '</td>'
                        + ' <td>' + moment(data[i]['date_transacted']).format('ddd, MMM Do YYYY, h:mm A') + '</td>'
                        + ' <td>' + data[i]['to_who'] + '</td></tr>';
            }

            $('#point_usage tbody').html(content);
            working_alert.finish();
            $('#point_usage_page_info > span').text(data[data.length - 1].count + ' Persons'); // for number of persons
            $('#pagination4').pagination('updateItems', data[data.length - 1].count);
        }).fail(function () {

        });

    }

    ///////////////////////////////////////////////////////////


});