/*jslint browser: true*/
/*global $, Spinner, alert, working_alert */
$(document).ready(function () {

    /****************** Logout Event Handlers *****************/
    $('#logout').click(function () {
        swal({
            title: "Are you sure?",
            text: 'Clicking "OK" will log you out of the system',
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function (result) {
            if (result) {
                window.location = "logout.php";
            }
        });
    });

    $('#transferM2AForm').parsley();
    $('#transferM2UForm').parsley();
    $('#transferM2MForm').parsley();
    $('#transferM2ManyForm').parsley();

    function processResponse(data) {
        working_alert.finish();

        if (data['status']) {
            $('#infoModal').html('<div class="alert alert-dismissible alert-success text-center">' +
                    'Points transferred succesfully</div>').slideDown().delay(3000).slideUp();
            $('#pointBalance').html(data['points']);
        } else {
            if (data['err'] === 'INSUFFICIENT_POINTS') {
                $('#infoModal').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Points Tranfer Failed!</strong> due to insufficient points' +
                        '</div>');
            } else if (data['err'] === 'ERROR') {
                $('#infoModal').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error occured! </strong> Please try again later' +
                        '</div>');
            } else if (data['err'] === 'ID_NOT_RESOLVED') {
                $('#infoModal').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Entity does not exist</strong>' +
                        '</div>');
            } else if (data['err'] === 'INVALID_AMOUNT') {
                $('#infoModal').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Invalid amount!</strong>. Enter amount less than ' + $('#lp').html() +
                        '</div>');
            } else if (data['err'] === 'ERORR_OCCURRED') {
                $('#infoModal').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error Occurred!</strong> Please try again later' +
                        '</div>');
            } 
        }
        $("html, body").animate({scrollTop: 0}, "fast");
    }

    function processResponse_many(data) {
//        working_alert.finish();
        $('#transfer2ManyBtn').attr("disabled", false);

        if (data['status']) {
            $('#infoModal').html('<div class="alert alert-dismissible alert-success text-center">' +
                    '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                    '<strong>File Uploaded Succesfully.\r\n</strong>'+
                    'N.B - Do not reupload the file to avoid double point disbursement</div>').slideDown().delay(5000).slideUp();
            $('#transferM2ManyForm').find('#list').val('');
        } else {
            if (data['err'] === 'FAILURE') {
                $('#infoModal').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>File Upload Failed.</strong> Please try again later' +
                        '</div>').slideDown();
            } else if (data['err'] === 'VALIDATION_ERROR') {
                $('#infoModal').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Invalid File Format.</strong> Ensure the file is in .csv format' +
                        '</div>').slideDown();
            } else if (data['err'] === 'FILE_ERROR') {
                $('#infoModal').html('<div class="alert alert-dismissible alert-warning text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Invalid File.</strong> Ensure the file is valid and not empty' +
                        '</div>').slideDown();
            } else if (data['err'] === 'AMOUNT_FORMAT_ERROR') {
                $('#infoModal').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Amount Format Error. </strong> Please ensure all amounts are digits' +
                        '</div>');
            } else if (data['err'] === 'INSUFFICIENT_POINTS') {
                $('#infoModal').html('<div class="alert alert-dismissible alert-danger text-center">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Insufficient iPoints! </strong> Buy more iPoints to complete this transaction' +
                        '</div>');
            }
        }
        $("html, body").animate({scrollTop: 0}, "fast");
    }

    $('body').on('submit', '#transferM2AForm', function (e) {
        $('#infoModal').empty();

        $.post('transfer_points_to_agent.php', $(this).serialize(), function (data) {
            processResponse(data);
        }, 'JSON').fail(function () {});
        e.preventDefault();
    });

    $('body').on('submit', '#transferM2UForm', function (e) {
        $('#infoModal').empty();

        $.post('transfer_points_to_user.php', $(this).serialize(), function (data) {
            processResponse(data);
        }, 'JSON').fail(function () {});
        e.preventDefault();
    });

    $('body').on('submit', '#transferM2MForm', function (e) {
        $('#infoModal').empty();

        $.post('transfer_points_to_merchant.php', $(this).serialize(), function (data) {
            processResponse(data);
        }, 'JSON').fail(function () {});
        e.preventDefault();
    });

    $('body').on('submit', '#transferM2ManyForm', function (e) {
        $('#infoModal').empty();
        $('#insufficientFundsModal').empty();
        $('#invalidNumbersModal').empty();

        working_alert.start();
        $('#transfer2ManyBtn').attr("disabled", "disabled");

        var url = "transfer_points_to_many.php";

        $.ajax({
            type: "POST",
            url: url,
            data: new FormData(document.getElementById('transferM2ManyForm')),
            dataType: 'JSON',
            /*** Options to tell JQuery not to process data or worry about content-type ****/
            cache: false,
            contentType: false,
            processData: false,
            /****************************************/
            success: function (data) {
                processResponse_many(data);
            }
        });

        e.preventDefault();
    });


});