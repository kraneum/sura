'use strict';

//export
var ussd_value_generate_form;

(function () {

        /****************** Logout Event Handlers *****************/
    $('#logout').click(function () {
        swal({
            title: "Are you sure?",
            text: 'Clicking "OK" will log you out of the system',
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function (result) {
            if (result) {
                window.location = "logout.php";
            }
        });
    });
    
    $('#ussdValueGenerateForm').parsley();
    
    var config = {
        generateId: 'generate',
        ussdValuesId: 'ussdValues',
        ussdCodesNumbersId: 'ussdCodesNumbers',
        ussdValuesPrefixId: 'uV',
        ussdCodesNumbersPrefixId: 'uCN',
        ussdFieldsContainer: $('#ussdFieldsContainer'),
        addUSSDGenerateFieldsId: 'addUSSDGenerateFields',
        pointBalanceId: 'pointBalance',
        thisSale: $('#thisSale'),
        pointBalAftThisSale: $('#pointBalAftThisSale'),
        generateCardsId: 'cards',
        ratePerCard: 0.25
    };
    var globVars = {
        pointBalance: null,
        pointBalAftThisSale: 0,
        thisSale: 0,
        chargeCards:false,
        totalNumOfUssdCodes:null
    };

    config.ussdFieldsContainer.on("click", "#" + config.addUSSDGenerateFieldsId, function (event) {
        event.target.remove();
        ussd_value_generate_form.addRow();
    }).on("change", $("[id |= '" + config.ussdCodesNumbersPrefixId + "']").add("[id |= '" + config.ussdValuesPrefixId + "']"), function(){
        updatePoints(globVars.chargeCards);
    });
    $('#' + config.generateCardsId).change(function () {
        updatePoints(globVars.chargeCards = !!$('#' + config.generateCardsId + ':checked').val());
    });
    
    var updatePoints = function (chargeCards) {
        var totalPoints = 0, pointBalance = globVars.pointBalance || +document.getElementById(config.pointBalanceId).innerHTML, pointBalAftThisSale, numOfUssdCodes, totalNumOfUssdCodes=0;
        for (var idx = ussd_value_generate_form.addRowCt - 1; idx >= 0; --idx) {
            totalNumOfUssdCodes += (numOfUssdCodes = document.getElementById(config.ussdCodesNumbersPrefixId + '-' + idx).value);
            totalPoints += (document.getElementById(config.ussdValuesPrefixId + '-' + idx).value * numOfUssdCodes) + (!chargeCards ? 0 : numOfUssdCodes * config.ratePerCard);
        }

        pointBalAftThisSale = globVars.pointBalAftThisSale = (pointBalance - totalPoints);

        config.thisSale.html(toFixed(globVars.thisSale = totalPoints));
        config.pointBalAftThisSale.html('<span ' + (pointBalAftThisSale < 0 && 'style="color:red;"') + '>' + toFixed(pointBalAftThisSale) + '</span>');
    
        globVars.totalNumOfUssdCodes = totalNumOfUssdCodes;
    };

    var _ussdValueGenerateForm = function () {
        this.addRowCt = 0;
        this.addRow();
    };
    _ussdValueGenerateForm.prototype.addRow = function () {
        !this.addRowCt && config.ussdFieldsContainer.append('<div class="row"><div class="col-xs-4"><label for="" class="control-label">PIN VALUE</label></div><div class="col-xs-6"><label for="" class="control-label">NUMBER OF PIN CODES</label></div></div>');

        config.ussdFieldsContainer.append('<div class="row" style="margin-top:10px;"><div class="col-xs-4"><select class="form-control" id="' + config.ussdValuesPrefixId + '-' + this.addRowCt + '"><option value="5">5</option><option value="10">10</option><option selected value="20">20</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option></select></div><div class="col-xs-6"><input type="number" min="1" class="form-control" id="' + config.ussdCodesNumbersPrefixId + '-' + this.addRowCt + '" data-parsley-type="digits" data-parsley-min="1"></div><div class="form-group col-xs-1"><input type="button" style="background-color: #72be58;color: white;font-weight: bolder;" value="&plus;" class="form-control" id="' + config.addUSSDGenerateFieldsId + '" data-parsley-type="digits" data-parsley-required></div></div>');
        ++this.addRowCt;
    };
    _ussdValueGenerateForm.prototype.reset = function (newPointBalance) {
        config.ussdFieldsContainer.html('');

        config.thisSale.html('0');
        config.pointBalAftThisSale.html((globVars.pointBalance = newPointBalance) || +document.getElementById(config.pointBalanceId).innerHTML);

        this.addRowCt = 0;
        this.addRow();
    };
    _ussdValueGenerateForm.prototype.freeze = function () {
        $("[id |= '" + config.ussdCodesNumbersPrefixId + "']").add("[id |= '" + config.ussdValuesPrefixId + "']").add('#' + config.generateId).add("#" + config.addUSSDGenerateFieldsId).prop('disabled', true);
    };
    _ussdValueGenerateForm.prototype.free = function () {
        $("[id |= '" + config.ussdCodesNumbersPrefixId + "']").add("[id |= '" + config.ussdValuesPrefixId + "']").add('#' + config.generateId).add("#" + config.addUSSDGenerateFieldsId).prop('disabled', false);
    };
    _ussdValueGenerateForm.prototype.ussdPurchase = function () {
        var ussdPurchase = {orderCards: globVars.chargeCards}, ussdCodesNumbers, ussdValue;
        for (var idx = this.addRowCt - 1; idx >= 0; --idx) {
            ussdCodesNumbers = +document.getElementById(config.ussdCodesNumbersPrefixId + '-' + idx).value;
            ussdValue = +document.getElementById(config.ussdValuesPrefixId + '-' + idx).value;
            ussdCodesNumbers && (ussdPurchase[ussdValue] ? ussdPurchase[ussdValue] += ussdCodesNumbers : ussdPurchase[ussdValue] = ussdCodesNumbers);
        }
        return ussdPurchase;
    };
    _ussdValueGenerateForm.prototype.pointIsInsufficient = function () {
        return globVars.pointBalAftThisSale < 0;
    };
    _ussdValueGenerateForm.prototype.getThisSale = function () {
        return globVars.thisSale;
    };
    _ussdValueGenerateForm.prototype.getPointBalAftThisSale = function () {
        return globVars.pointBalAftThisSale;
    };
    _ussdValueGenerateForm.prototype.getTotalNumOfUssdCodes = function () {
        return globVars.totalNumOfUssdCodes;
    };

    ussd_value_generate_form = new _ussdValueGenerateForm();
})();