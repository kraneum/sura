/*jslint browser: true*/
/*global $ alert */
'use strict';

//Stuff to export
var view_ussd = {};

$(function () {

    /****************** Logout Event Handlers *****************/
    $('#logout').click(function () {
        swal({
            title: "Are you sure?",
            text: 'Clicking "OK" will log you out of the system',
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function (result) {
            if (result) {
                window.location = "logout.php";
            }
        });
    });

    //Keep global variables inside here
    var config = {
        ussdPurchasesId: 'ussdPurchases',
        ussdPurchasesTbodyId: 'ussdPurchasesTbody'
    };

    var globvars = {
        prevPage: null,
        ussdPurchasesCache: {}
    };

    /**********************************************************************************************************************************/
    //Main
    /**********************************************************************************************************************************/

    /**********************************************************************************************************************************/
    //Bind event handlers
    /**********************************************************************************************************************************/

    setTimeout(function () {
        loadPageNumber(1, function (total) {
            $('#' + config.ussdPurchasesId).pagination({
                items: total,
                itemsOnPage: 5,
                cssStyle: 'light-theme',
                //edges: 4,
                onPageClick: function (page) {
                    loadPageNumber(page);
                }
            });
        });
    }, 0);

    /****************************************************************************************************************************/
    //Private functions
    /****************************************************************************************************************************/
    var loadPageNumber = function (page, cb) {
        if (page in globvars.ussdPurchasesCache) {
            globvars.prevPage === page || displayUssdPurchases(globvars.ussdPurchasesCache[page]);
            return;
        }

        loadUssdPurchases(page, function (err, result) {
            if (err) {
                backToPrevPage();
                return;
            }

            displayUssdPurchases(result['rows']);

            globvars.ussdPurchasesCache[page] = result['rows'];

            typeof cb === 'function' && cb(result['ct']);
            
            globvars.prevPage && window.scrollTo(0, document.body.scrollHeight);

            globvars.prevPage = page;
        });
    };

    var loadUssdPurchases = function (page, cb) {
        working_alert.start();

        $.ajax({
            type: "POST",
            url: "ussd_purchases.php",
            data: JSON.stringify({page: page}),
            success: function (data, textStatus, jqXHR) {
                if (textStatus === 'success') {
                    if (!data.err) {
                        cb(null, data.result);
                    } else {
                        cb({err: data.err});
                        switch (data.err) {
                            case 'DB':
                            case 'VALIDATION':
                            default:
                                //Failed
                                break;
                        }
                    }
                } else {
                    //Fail
                    cb({err: textStatus});
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
//Create a way to go back if a page load triggered by a click fails
                cb({err: textStatus || errorThrown});
            },
            complete: function () {
                working_alert.finish();
            },
            dataType: 'JSON'
        });
    };

    var displayUssdPurchases = function (data) {
        var date, meridian, hr, min, activitySummary;//The activity_summary would now be a json!
        $('#' + config.ussdPurchasesTbodyId).html(data.reduce(function (carry, datum, index) {
            date = new Date(datum.datetimezone);
            meridian = date.getHours() > 12 ? (hr = date.getHours() - 12) && 'PM' : (hr = date.getHours()) && 'AM';
            min = date.getMinutes();
            min < 10 && (min = '0' + min);

            activitySummary = JSON.parse(datum.activity_summary);

            return carry + '<tr><th scope="row">' + (index + 1) + '</th><td>' + date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + ' ' + hr + ':' + min + ' ' + meridian + '</td><td>' + (activitySummary.user + ' ' + (!activitySummary.orderCards ? 'generated' : 'ordered') + ' the following pins: <table class="table table-bordered"><thead><tr><th></th><th>PIN VALUE</th><th>NUMBER OF PINS</th><th>VALUE</th></tr></thead><tbody>' + Object.keys(activitySummary.details).reduce(function (carry, key, index) {
                return carry + '<tr><th>' + (index + 1) + '</th><td>' + key + '</td><td>' + activitySummary.details[key] + '</td><td>' + key * activitySummary.details[key] + '</td></tr>';
            }, '') + '</tbody><tfoot><tr><th>Total</th><td></td><td>' + activitySummary.totalPins + '</td><td>' + activitySummary.totalValue + '</td></tr></tfoot></table>') + '</td><td>' + datum.codes_purchased + '</td><td>' + datum.total_value + '</td><td><a target="_blank" href="get_ussd_file.php?criteria=purchase_id&format=csv&searchValue=' + datum.id + '">Download</a></td></tr>';
        }, ''));
    };

    var backToPrevPage = function () {
//dnt change the displayed ussd purchases
//Just select the previous page
        globvars.prevPage && $('#' + config.ussdPurchasesId).pagination('selectPage', globvars.prevPage);
    };

    /****************************************************************************************************************************/
    //Private classes
    /****************************************************************************************************************************/

});