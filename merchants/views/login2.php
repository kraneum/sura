<!DOCTYPE html>

<html>
<head>
<title>Merchants - iInsurance</title>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="../img/favicon.png" />
<link rel="icon" type="image/png" href="../img/favicon.png">
<link rel="apple-touch-icon" href="../img/favicon.png">
<link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
<link href="../css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
<link href="../css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>

<style type="text/css" >
.modal-dialog {
margin-top: 10%;
}
.modal-content {
border-radius: 20px; 
padding: 0;
}
.modal-content .row {
padding: 0;
margin: 0;
}
#left {
padding: 0;
padding: 30px;
padding-top: 15px;
}
#right {
border-left: solid 1px #dadada;
}
#right > div {
margin: 35% auto ;
}
#right #login-content div:nth-child(1),
#right #login-content div:nth-child(3){
font-size: 14px;
font-weight: bold;
color: #666666;
}
#right #login-content div:nth-child(2) a{
font-size: 30px;
font-weight: bold;
color: #265a88;
}
.modal-body {
padding: 0;
margin-left: 5px;
}
.modal-header {
border-bottom: none;
padding: 0;
margin-bottom: 15px;
}
h4 {
color: #666666;
letter-spacing: 1px;
font-size: 24px;
}
label {
font-size: 17px;
color: #77777a;
font-weight: normal;
letter-spacing: 0.3px;
}
input.form-control {
border-radius: 2px;
/*padding: 15px;*/
}
form .form-group:first-of-type {
margin-left: 0;
}
@media screen and (min-width: 768px) {
.modal-dialog {
width: 800px; /* New width for default modal */
}
.modal-sm {
width: 350px; /* New width for small modal */
}
}
@media screen and (min-width: 992px) {
.modal-lg {
width: 950px; /* New width for large modal */
}

}
</style>
</head>

<body id="login" class="container" 
style="margin-top: 20px; background-image: url('../img/merchant_login_bg.jpg');
background-repeat: no-repeat;
background-position: top left;
background-position-x: 0;
background-position-y: 0;
background-size: 100%;
">
<div id="main">

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
<div class="modal-dialog">
<div class="modal-content">
<div class="row">
<div id="left" class="col-sm-7">
<div class="modal-header">
<h4><strong>Sign In</strong></h4>
</div>
<div class="modal-body">
<div>
<div id="validationMessage"></div><!--Leave Req URI-->
<form id="regForm" method="post" action="<?php echo htmlspecialchars($_SERVER['REQUEST_URI']); ?>">
<div class="form-group">
<label for="email">Merchant Email * </label>
<div class="input-group">
<div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
<input class="form-control" id="email" name="email" type="email" placeholder="Enter email" value="lrkk@merkj.com" data-parsley-required/>
</div>

</div>

<div class="form-group">
<label for="password">Password * </label>
<div class="input-group">
<div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
<input class="form-control" id="password" name="password" type="password" placeholder="Enter password" value="lrkewkrwe" data-parsley-required/>
</div>

<!--<div id="forgotten" class="pull-right small"><a href="../php/forgotpassword.php?entity=merchant">I forgot my user ID or password</a></div>-->

</div>
<div class="form-group"> 
<button type="submit" class="btn btn-primary"  name="btn_login">Login
<i class="fa fa-check btn-icon-right"></i></button>
</div>
</form>
</div>
</div>
</div>
<div id="right" class="col-md-5 text-center pull-right">
<div>
<a href="http://uiclimited.com"><img src="../img/logos/uic_login.png"/></a>
<div id="login-content">
<div>Don't have an account ?</div>
<div><a href="self_register_merchant.php" id="newuser">SIGN UP NOW</a></div>
<div>and start saving on your loyalty</div>
</div>
</div>
</div>
</div>
</div>
</div></div></div>


<script src="../js/jquery-3.1.1.js" type="text/javascript"></script>
<script src="../js/parsley.min.js" type="text/javascript"></script>
<script src="../js/bootstrap.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function () {
$('.modal').modal({
backdrop: 'static',
keyboard: false
});
$('#regForm').parsley();
});
<?php
if (isset($_POST['btn_login'])) {
echo'document.getElementById("validationMessage").innerHTML = ' .
($operationStatus['err'] === false ? '"<div class=\"alert alert-success\">Login successful</div>"' : '"<div class=\"alert alert-danger\">' . $operationStatus['message'] . '</div>";' . ($operationStatus['type'] === 'validationError' ? 'document.getElementById("' . $operationStatus['field'] . '").style.borderColor="red"' : ''));
}
?>
</script>
</body>
</html>