<!DOCTYPE html>
<html>
    <head>
        <title>iInsurance</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="icon" type="image/png" href="../img/favicon.ico">
        <link rel="apple-touch-icon" href="../img/favicon.ico">

        <link href="../css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
        <link href="../css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style_login.css" rel="stylesheet" type="text/css"/>
        <style type="text/css">

            @font-face {
                font-family: 'Avenir';
                src: url('../fonts/AvenirLTStd-Book.woff2') format('woff2'), /* Super Modern Browsers */
                    url('../fonts/AvenirLTStd-Book.woff') format('woff'), /* Pretty Modern Browsers */
                    url('../fonts/AvenirLTStd-Book.ttf') format('truetype'); /* Safari, Android, iOS */
            }
            body {
                font-family: "Avenir", sans-serif;
            }
        </style>

    </head>
    <body>

        <div class="container">

            <div class="form">

                <div class="login-img"><img src="../img/logos/uic2.png" class="img-responsive" /></div>
                <br/>
                <span id="validationMessage"></span>
                <form id ="regForm" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" enctype="multipart/form-data">

                    <div class="form-group">
                        <label class="label_" for="merchant_name">Merchant Name * </label>
                        <input class="input_" id="merchant_name" name="merchant_name" value="Supermart" type="text" placeholder="Enter merchant's name" maxlength="100" data-parsley-required/>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="merchant_write_up">Write up *</label>
                        <textarea class="input_" rows="5" id="merchant_write_up" name="merchant_write_up" placeholder="About merchant" maxlength="200" data-parsley-required>This is Supermart</textarea>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="merchant_phone">Contact phone * </label>
                        <input class="input_" id="merchant_phone" name="merchant_phone" value="07032741119" placeholder="Enter the contact phone" data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$" maxlength="50" data-parsley-required  data-parsley-remote="merchant_phone_available.php?phone={value}" data-parsley-trigger="keyup" data-parsley-remote-message="Phone number already in use" data-parsley-debounce="500" />
                    </div>

                    <div class="form-group">
                        <label class="label_" for="merchant_email">Contact email * </label>
                        <input class="input_" id="merchant_email" name="merchant_email" value="modernbabbage@gmail.com"  type="email" placeholder="Enter contact email" maxlength="100" data-parsley-required  data-parsley-remote="merchant_email_available.php?email={value}" data-parsley-trigger="keyup" data-parsley-remote-message="Email already in use" data-parsley-debounce="500" />
                    </div>

                    <div class="form-group">
                        <label class="label_" for="merchant_rc_number">RC number * </label>
                        <input class="input_" id="merchant_rc_number" name="merchant_rc_number" value="12345678" type="text" placeholder="Enter merchant's CAC registration number" maxlength="100" data-parsley-required data-parsley-remote="merchant_rcnumber_available.php?rc_number={value}" data-parsley-trigger="keyup" data-parsley-remote-message="RC number already in use" data-parsley-debounce="500"/>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="merchant_operating_country">Operating country * </label>
                        <select class="input_" id="merchant_operating_country" name="merchant_operating_country"  data-parsley-required>
                            <option value="Nigeria">Nigeria</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="merchant_head_office_address">Headquarters address * </label>
                        <input class="input_" id="merchant_head_office_address" name="merchant_head_office_address" value="Victoria Island"  type="text" placeholder="Enter Head office address" maxlength="100" data-parsley-required/>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="merchant_display_picture">Display picture * </label>
                        <input class="input_" id="merchant_display_picture" name="merchant_display_picture"  type="file" placeholder="Choose file" accept="image/jpeg, image/png" data-parsley-filemaxmegabytes="2" data-parsley-trigger="change" data-parsley-dimensions="true" data-parsley-dimensions-options='{"min_width": "100","min_height": "100"}' data-parsley-filemimetypes="image/jpeg, image/png" data-parsley-required/>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="referee_id">Referee ID (if any) </label>
                        <input class="input_" id="referee_id" name="referee_id" value="UWE4RdF5Cc1" type="text" placeholder="Enter referee ID (if any)" maxlength="20"/>
                    </div>

                    <h3>Admin details</h3>
                    <div class="form-group">
                        <label class="label_" for="admin_first_name">First Name * </label>
                        <input class="input_" id="admin_first_name" name="admin_first_name" value="Salvation"  type="text" placeholder="Enter first name" maxlength="100" data-parsley-required/>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="admin_middle_name">Middle Name </label>
                        <input  class="input_" id="admin_middle_name" name="admin_middle_name" value="Arinze"  type="text" placeholder="Enter middle name" maxlength="100"/>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="admin_last_name"> Last Name * </label>
                        <input class="input_" id="admin_last_name" name="admin_last_name" value="Arinze"  type="text" placeholder="Enter last name" maxlength="100" data-parsley-required/>
                    </div>

                    <div id="admin_dob-group" class="row form-group" style="text-align: left;">
                        <div class="col-md-12">
                            <label for="admin_dob" >Date of Birth * </label>
                            <div class="input-group" >
                                <input class="datefield input_1"  id="admin_dob" name="admin_dob" value="01/01/1990" placeholder="Enter date of birth" data-parsley-required />
                                <span class="input-group-addon input_2">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="admin_phone">Phone * </label>
                        <input class="input_" type="phone" id="admin_phone" name="admin_phone" value="07032741119" placeholder="Enter phone number" maxlength="11" data-parsley-required data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$"  data-parsley-remote="employee_phone_available.php?phone={value}" data-parsley-trigger="keyup" data-parsley-remote-message="Phone number already in use"  data-parsley-debounce="500" data-parsley-minlength="11" data-parsley-maxlength="11"  data-parsley-type="digits" data-parsley-minlength-message="This value should be 11 digits" data-parsley-maxlength-message="This value should be 11 digits" data-parsley-type-message="This value should contain digits only"/>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="admin_email">Email * </label>
                        <input class="input_" id="admin_email" name="admin_email" value="modernbabbage@gmail.com" type="email" placeholder="Enter email" maxlength="100" data-parsley-trigger="keyup" data-parsley-remote-message="Email already in use" data-parsley-remote="employee_email_available.php?email={value}" data-parsley-required data-parsley-debounce="500"/>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="admin_country">Country * </label>
                        <select class="input_" id="admin_country" name="admin_country">
                            <option value="Nigeria">Nigeria</option>
                        </select>
                    </div>


                    <div class="form-group">
                        <label class="label_" for="admin_security_question">Security question * </label>
                        <select class="input_" id="admin_security_question" name="admin_security_question"  data-parsley-required>
                            <option disabled selected>Select security question</option>
                            <?php
                            foreach ($securityQuestions as $securityQuestion) {
                                echo "<option value = '" . $securityQuestion['question'] . "'>" . $securityQuestion['question'] . '</option>';
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="admin_security_answer">Security Answer * </label>
                        <input class="input_" id="admin_security_answer" name="admin_security_answer" value="Italy" maxlength="100" type="text" placeholder="Enter security answer"  data-parsley-required/>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="admin_password">Password * </label>
                        <input class="input_" id="admin_password" name="admin_password" value="aaaa"  type="password" placeholder="Enter password" maxlength="20" data-parsley-required/>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="admin_password_confirm">Confirm Password * </label>
                        <input class="input_" id="admin_password_confirm" name="admin_password_confirm" value="aaaa" type="password" placeholder="Enter password again" maxlength="20" data-parsley-equalto="#admin_password" data-parsley-equalto-message="Password mismatch" data-parsley-required/>
                    </div>

                    <div class="form-group">
                        <label class="label_" for="admin_display_picture">Display picture * </label>
                        <input class="input_" id="admin_display_picture" name="admin_display_picture" value="aaaa" type="file" placeholder="Choose file" accept="image/jpeg, image/png" data-parsley-filemaxmegabytes="2" data-parsley-trigger="change" data-parsley-dimensions="true" data-parsley-dimensions-options='{"min_width": "100","min_height": "100"}' data-parsley-filemimetypes="image/jpeg, image/png" data-parsley-required/>
                    </div>



                    <div id="license" style="margin-bottom: 20px;">By clicking Register, you agree to our<a href=""> License Agreement</a></div>
                    <div class="form-group">
                        <div class="row">
                            <button id="btn_register" type="submit" class="btn btn-success signinbtn"  name="btn_register"><i class="fa fa-save btn-icon-left" class="text text-center"></i>Register</button>
                        </div>
                    </div>


                </form>
            </div>
            <p class="message text-center">Already Registered? <a href="login.php" style="color: #000000; font-weight: bold;">Sign In</a></p>
        </div>


        <script src="../js/jquery-3.1.1.js" type="text/javascript"></script>
        <script src="../js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<!--         <script src="../js/jquery-ui.js" type="text/javascript"></script> -->
        <script src="../js/parsley.min.js" type="text/javascript"></script>
        <script src="../js/parsley-laraextras.min.js" type="text/javascript"></script>
        <script src="../js/parsley-file-validators.js" type="text/javascript"></script>
        <script src="../js/bootstrap.js" type="text/javascript"></script>
<!--         <script src="../js/jquery-ui.min_datepicker.js" type="text/javascript"></script> -->

        <script type="text/javascript">

            $("#admin_dob").datepicker({
                format: "dd/mm/yyyy",
                clearBtn: true,
                orientation: "auto right",
                autoclose: true
            });

            $('#regForm').parsley();
            $('#admin_dob').parsley().on('field:error', function () {
                $('#admin_dob').next().remove();
                $('#admin_dob-group').append('<ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">This value is required.</li></ul>');
            });

            $('#admin_dob').parsley().on('field:success', function () {
                $('#admin_dob-group > ul').remove();
            });
        </script>

        <script type="text/javascript">
<?php
if (isset($_POST['btn_register'])) {
    echo'document.getElementById("validationMessage").innerHTML = ' .
    ($operationStatus['err'] === false ? '"Merchant successfully created"' : '"' . $operationStatus['message'] . '";' . ($operationStatus['type'] === 'validationError' ? 'document.getElementById("' . $operationStatus['field'] . '").classList.add("parsley-error")' : ''));
}
?>

            $(document).ready(function () {
                Parsley.addMessages('en', {
                    dimensions: 'The display picture dimensions should be a minimum of 100px by 100px'
                });

                $('#regForm').parsley();
            });
        </script>
    </body>
</html>

