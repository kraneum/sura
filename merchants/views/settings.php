<?php
require_once './fragments/header.php';
?>

<link href="../css/jquery-ui.min_datepicker.css" rel="stylesheet" type="text/css"/>
<div class="container-fluid body">
    <div class="row correct-row">
        <div class="col-xs-3" style="background: white; padding: 0;">
            <div>
                <ul class="nav nav-pills nav-stacked settingsTablist" id="settingsTablist" role="tablist">
                    <li class="active"><a id="settingsTablistAccount" href="#account"><strong>Account</strong><span class="pull-right glyphicon glyphicon-play"></span></a></li>
                    <li><a href="#settings"><strong>Settings & Notifications</strong><span class="pull-right glyphicon glyphicon-play"></span></a></li>
                    <li><a href="#help"><strong>Help</strong><span class="pull-right glyphicon glyphicon-play"></span></a></li>
                </ul>
            </div>
        </div>

        <div class="col-xs-8 tab-content" style="background: white; margin-left: 20px;">
            <div id="account" class="tab-pane fade in active">
                <div id="formInfo"></div>
                <div id="profile">
                    <form id="profileForm" method="post" action="edit_employee_e.php" enctype="multipart/form-data">
                        <?php
                        echo '<div class="form-group">
                            <label for="first_name">First Name * </label>
                            <input class="form-control" id="first_name" name="first_name" type="text" placeholder="' . ($employee['first_name'] ? $employee['first_name'] : 'Enter first name') . '" maxlength="100"/>
                        </div>

                        <div class="form-group">
                            <label for="middle_name">Middle Name </label>
                            <input  class="form-control" id="middle_name" name="middle_name"  type="text" placeholder="' . ($employee['middle_name'] ? $employee['middle_name'] : 'Enter middle name') . '" maxlength="100"/>
                        </div>

                        <div class="form-group">
                            <label for="last_name"> Last Name * </label>
                            <input class="form-control" id="last_name" name="last_name"  type="text" placeholder="' . ($employee['last_name'] ? $employee['last_name'] : 'Enter last name') . '" maxlength="100"/>
                        </div>

                        <div class="form-group">
                            <label for="dob">Date of Birth * </label>
                            <div class="input-group">
                                <input class="form-control" id="dob" name="dob" placeholder="' . ($employee['dob'] ? $employee['dob'] : 'Enter date of birth') . '" data-date-format="MM/DD/YYYY" maxlength="10"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone">Phone * </label>
                            <input class="form-control" type="phone" id="phone" name="phone" placeholder="' . ($employee['phone'] ? $employee['phone'] : 'Enter phone number') . '" maxlength="50" data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$"/>
                        </div>

                        <div class="form-group">
                            <label for="country">Nationality * </label>
                            <select class="form-control" id="country" name="country">
' . array_reduce([["NG", "Nigeria"], ["ZA", "South Africa"], ["US", "United States"], ["GB", "United Kingdom"], ["GH", "Ghana"], ["TG", "Togo"], ["BD", "Bangladesh"], ["BE", "Belgium"], ["BF", "Burkina Faso"], ["BG", "Bulgaria"], ["BA", "Bosnia and Herzegovina"], ["BB", "Barbados"], ["WF", "Wallis and Futuna"], ["BL", "Saint Barthelemy"], ["BM", "Bermuda"], ["BN", "Brunei"], ["BO", "Bolivia"], ["BH", "Bahrain"], ["BI", "Burundi"], ["BJ", "Benin"], ["BT", "Bhutan"], ["JM", "Jamaica"], ["BV", "Bouvet Island"], ["BW", "Botswana"], ["WS", "Samoa"], ["BQ", "Bonaire[Saint Eustatius and Saba "], ["BR", "Brazil"], ["BS", "Bahamas"], ["JE", "Jersey"], ["BY", "Belarus"], ["BZ", "Belize"], ["RU", "Russia"], ["RW", "Rwanda"], ["RS", "Serbia"], ["TL", "East Timor"], ["RE", "Reunion"], ["TM", "Turkmenistan"], ["TJ", "Tajikistan"], ["RO", "Romania"], ["TK", "Tokelau"], ["GW", "Guinea-Bissau"], ["GU", "Guam"], ["GT", "Guatemala"], ["GS", "South Georgia and the South Sandwich Islands"], ["GR", "Greece"], ["GQ", "Equatorial Guinea"], ["GP", "Guadeloupe"], ["JP", "Japan"], ["GY", "Guyana"], ["GG", "Guernsey"], ["GF", "French Guiana"], ["GE", "Georgia"], ["GD", "Grenada"], ["GA", "Gabon"], ["SV", "El Salvador"], ["GN", "Guinea"], ["GM", "Gambia"], ["GL", "Greenland"], ["GI", "Gibraltar"], ["OM", "Oman"], ["TN", "Tunisia"], ["JO", "Jordan"], ["HR", "Croatia"], ["HT", "Haiti"], ["HU", "Hungary"], ["HK", "Hong Kong"], ["HN", "Honduras"], ["HM", "Heard Island and McDonald Islands"], ["VE", "Venezuela"], ["PR", "Puerto Rico"], ["PS", "Palestinian Territory"], ["PW", "Palau"], ["PT", "Portugal"], ["SJ", "Svalbard and Jan Mayen"], ["PY", "Paraguay"], ["IQ", "Iraq"], ["PA", "Panama"], ["PF", "French Polynesia"], ["PG", "Papua New Guinea"], ["PE", "Peru"], ["PK", "Pakistan"], ["PH", "Philippines"], ["PN", "Pitcairn"], ["PL", "Poland"], ["PM", "Saint Pierre and Miquelon"], ["ZM", "Zambia"], ["EH", "Western Sahara"], ["EE", "Estonia"], ["EG", "Egypt"], ["EC", "Ecuador"], ["IT", "Italy"], ["VN", "Vietnam"], ["SB", "Solomon Islands"], ["ET", "Ethiopia"], ["SO", "Somalia"], ["ZW", "Zimbabwe"], ["SA", "Saudi Arabia"], ["ES", "Spain"], ["ER", "Eritrea"], ["ME", "Montenegro"], ["MD", "Moldova"], ["MG", "Madagascar"], ["MF", "Saint Martin"], ["MA", "Morocco"], ["MC", "Monaco"], ["UZ", "Uzbekistan"], ["MM", "Myanmar"], ["ML", "Mali"], ["MO", "Macao"], ["MN", "Mongolia"], ["MH", "Marshall Islands"], ["MK", "Macedonia"], ["MU", "Mauritius"], ["MT", "Malta"], ["MW", "Malawi"], ["MV", "Maldives"], ["MQ", "Martinique"], ["MP", "Northern Mariana Islands"], ["MS", "Montserrat"], ["MR", "Mauritania"], ["IM", "Isle of Man"], ["UG", "Uganda"], ["TZ", "Tanzania"], ["MY", "Malaysia"], ["MX", "Mexico"], ["IL", "Israel"], ["FR", "France"], ["IO", "British Indian Ocean Territory"], ["SH", "Saint Helena"], ["FI", "Finland"], ["FJ", "Fiji"], ["FK", "Falkland Islands"], ["FM", "Micronesia"], ["FO", "Faroe Islands"], ["NI", "Nicaragua"], ["NL", "Netherlands"], ["NO", "Norway"], ["NA", "Namibia"], ["VU", "Vanuatu"], ["NC", "New Caledonia"], ["NE", "Niger"], ["NF", "Norfolk Island"], ["NZ", "New Zealand"], ["NP", "Nepal"], ["NR", "Nauru"], ["NU", "Niue"], ["CK", "Cook Islands"], ["XK", "Kosovo"], ["CI", "Ivory Coast"], ["CH", "Switzerland"], ["CO", "Colombia"], ["CN", "China"], ["CM", "Cameroon"], ["CL", "Chile"], ["CC", "Cocos Islands"], ["CA", "Canada"], ["CG", "Republic of the Congo"], ["CF", "Central African Republic"], ["CD", "Democratic Republic of the Congo"], ["CZ", "Czech Republic"], ["CY", "Cyprus"], ["CX", "Christmas Island"], ["CR", "Costa Rica"], ["CW", "Curacao"], ["CV", "Cape Verde"], ["CU", "Cuba"], ["SZ", "Swaziland"], ["SY", "Syria"], ["SX", "Sint Maarten"], ["KG", "Kyrgyzstan"], ["KE", "Kenya"], ["SS", "South Sudan"], ["SR", "Suriname"], ["KI", "Kiribati"], ["KH", "Cambodia"], ["KN", "Saint Kitts and Nevis"], ["KM", "Comoros"], ["ST", "Sao Tome and Principe"], ["SK", "Slovakia"], ["KR", "South Korea"], ["SI", "Slovenia"], ["KP", "North Korea"], ["KW", "Kuwait"], ["SN", "Senegal"], ["SM", "San Marino"], ["SL", "Sierra Leone"], ["SC", "Seychelles"], ["KZ", "Kazakhstan"], ["KY", "Cayman Islands"], ["SG", "Singapore"], ["SE", "Sweden"], ["SD", "Sudan"], ["DO", "Dominican Republic"], ["DM", "Dominica"], ["DJ", "Djibouti"], ["DK", "Denmark"], ["VG", "British Virgin Islands"], ["DE", "Germany"], ["YE", "Yemen"], ["DZ", "Algeria"], ["UY", "Uruguay"], ["YT", "Mayotte"], ["UM", "United States Minor Outlying Islands"], ["LB", "Lebanon"], ["LC", "Saint Lucia"], ["LA", "Laos"], ["TV", "Tuvalu"], ["TW", "Taiwan"], ["TT", "Trinidad and Tobago"], ["TR", "Turkey"], ["LK", "Sri Lanka"], ["LI", "Liechtenstein"], ["LV", "Latvia"], ["TO", "Tonga"], ["LT", "Lithuania"], ["LU", "Luxembourg"], ["LR", "Liberia"], ["LS", "Lesotho"], ["TH", "Thailand"], ["TF", "French Southern Territories"], ["TD", "Chad"], ["TC", "Turks and Caicos Islands"], ["LY", "Libya"], ["VA", "Vatican"], ["VC", "Saint Vincent and the Grenadines"], ["AE", "United Arab Emirates"], ["AD", "Andorra"], ["AG", "Antigua and Barbuda"], ["AF", "Afghanistan"], ["AI", "Anguilla"], ["VI", "U.S. Virgin Islands"], ["IS", "Iceland"], ["IR", "Iran"], ["AM", "Armenia"], ["AL", "Albania"], ["AO", "Angola"], ["AQ", "Antarctica"], ["AS", "American Samoa"], ["AR", "Argentina"], ["AU", "Australia"], ["AT", "Austria"], ["AW", "Aruba"], ["IN", "India"], ["AX", "Aland Islands"], ["AZ", "Azerbaijan"], ["IE", "Ireland"], ["ID", "Indonesia"], ["UA", "Ukraine"], ["QA", "Qatar"], ["MZ", "Mozambique"]], function($carry, $item)use($employee) {
                            return $carry . '<option' . ($employee['country'] === $item[0] ? ' selected' : '') . ' value="' . $item[0] . '">' . $item[1] . '</option>';
                        }, '') . '<select>
                        </div>';
                        ?>
                        <div class="form-group">
                            <label for="display_picture">Display picture * </label>
                            <input class="form-control" id="display_picture" name="display_picture"  type="file" placeholder="Choose file" accept="image/jpeg, image/png" data-parsley-filemaxmegabytes="2" data-parsley-trigger="change" data-parsley-dimensions="true" data-parsley-dimensions-options='{"min_width": "100","min_height": "100"}' data-parsley-filemimetypes="image/jpeg, image/png"/>
                        </div>
                        <div class="form-group">
                            <a href="#rstPasswd" id="passwdRst" class="btn btn-primary"><i class="glyphicon glyphicon-repeat btn-icon-left"></i>CHANGE PASSWORD</a>
                            <a href="#rstSecQuestion" id="secQuestionRst" class="btn btn-primary"><i class="glyphicon glyphicon-repeat btn-icon-left"></i>CHANGE SECURITY QUESTION</a>
                            <button id="formSignup" type="submit" class="btn btn-primary formButtons"  name="btn_register"><i class="fa fa-save btn-icon-left"></i>SAVE</button>
                        </div>
                    </form>
                </div>
            </div>
            <div id="settings" class="tab-pane fade">
                <h3>Settings & Notifications</h3>
                <p>Coming soon</p>
            </div>
            <div id="help" class="tab-pane fade">
                <h3>Help</h3>
                <p>Coming soon</p>
            </div>
            <div id="rstPasswd" class="tab-pane fade">
                <h3>CHANGE PASSWORD</h3>
                <div id="rstPasswdValidationMessage"></div>
                <form id="rstPasswdForm">
                    <div class="form-group">
                        <label for="old_password">Old password * </label>
                        <input class="form-control" id="old_password" name="old_password"  type="password" placeholder="Enter old password" maxlength="20" data-parsley-required/>
                    </div>

                    <div class="form-group">
                        <label for="password">New password * </label>
                        <input class="form-control" id="password" name="password"  type="password" placeholder="Enter new password" maxlength="20" data-parsley-required/>
                    </div>

                    <div class="form-group">
                        <label for="password_confirm">Confirm Password * </label>
                        <input class="form-control" id="password_confirm" name="password_confirm"  type="password" placeholder="Enter password again" maxlength="20" data-parsley-equalto="#password" data-parsley-equalto-message="Password mismatch" data-parsley-required/>
                    </div>

                    <div class="form-group"> 
                        <a href="#account" class="btn btn-primary acctBack"><i class="fa fa-arrow-left btn-icon-left"></i>Back</a>
                        <button type="submit" class="btn btn-primary formButtons"  name="btn_register"><i class="fa fa-save btn-icon-left"></i>Save</button>
                    </div>
                </form>
            </div>
            <div id="rstSecQuestion" class="tab-pane fade">
                <h3>CHANGE SECURITY QUESTION</h3>
                <div id="rstSecQuestionValidationMessage"></div>
                <form id="rstSecQuestionForm">
                    <div class="form-group">
                        <label for="question">Security question * </label>
                        <select class="form-control" id="security_question" name="security_question"  data-parsley-required>
                            <option disabled selected>Select security question</option>
                            <?php
                            foreach ($securityQuestions as $securityQuestion) {
                                echo "<option ".($employee['security_question'] === $securityQuestion['question'] ? 'selected="selected"':'').' value = "' . $securityQuestion['question'] . '">' . $securityQuestion['question'] . '</option>';
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="answer">Security Answer * </label>
                        <input class="form-control" id="security_answer" name="security_answer" maxlength="100" type="text" placeholder="<?php echo $employee['security_answer'] ? $employee['security_answer'] : 'Enter security answer';?>"  data-parsley-required/>
                    </div>

                    <div class="form-group"> 
                        <a href="#account" class="btn btn-primary acctBack"><i class="fa fa-arrow-left btn-icon-left"></i>Back</a>
                        <button type="submit" class="btn btn-primary formButtons"  name="btn_register"><i class="fa fa-save btn-icon-left"></i>Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="workingAlert" style="display:none; position:fixed;width:100px;z-index:990;top:0;left: 50%;box-shadow:0 2px 4px rgba(0,0,0,0.2);height:30px;border-color: #f0c36d;text-align: center;background-color: #f9edbe;color:#333;font-weight:bold;">Working...</div>
<?php
require_once './fragments/footer_f.php';
?>
<script src="../js/jquery-ui.min_datepicker.js" type="text/javascript"></script>
<script src="../js/parsley-laraextras.min.js" type="text/javascript"></script>
<script src="../js/parsley-file-validators.js" type="text/javascript"></script>
<script src="../js/working_alert.js" type="text/javascript"></script>
<script src="./views/js/settings.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#profileForm').parsley();
    });
</script>
<!--<script type="text/javascript">
    $("#dob").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-80:+00"
    });
    $('#profileForm').parsley();

</script>-->

</body>
</html>