<?php
require_once './fragments/header.php';
?>
<link href="../css/simplePagination.css" rel="stylesheet" type="text/css"/>
<div id="home-main" class="container">
    <div style="margin-bottom: 30px;">
        <h4><strong><i class="fa fa-exchange"></i> View all Generated Pin Codes</strong></h4>
        <small>&emsp;&emsp;You can view pin codes generated and also download them</small>
    </div>
    <div style="margin-bottom: 20px;" class="col-sm-offset-11 col-sm-1">
        <a href="generate_ussd.php" class="btn btn-xs btn-default">Generate Codes</a>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-hover table-responsive">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>Pins summary</th>
                        <th>Pins generated</th>
                        <th>Total Value</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="ussdPurchasesTbody"></tbody>
            </table>
            <div id="ussdPurchases" style="margin-left: 35% !important"></div>
        </div>
    </div>
</div>
<?php
require_once './fragments/footer.php';
?>
<script src="../js/jquery.simplePagination.js" type="text/javascript"></script>
<script src="../js/working_alert.js" type="text/javascript"></script>
<script src="./views/js/view_ussd.js" type="text/javascript"></script>