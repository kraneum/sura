<?php
if (!isset($_SESSION)) {
    session_start();
}
if (empty($_SESSION['entity']) || $_SESSION['entity_type'] !== 'PENSION') {
    session_destroy();
    header("Location: login.php");
}

require_once('../php/date_functions.php');
require_once('../php/curl.php');

$pension_info = $_SESSION['entity'];
$id = $pension_info['id'];
$name = $pension_info['name'];
$phone = $pension_info['phone'];
$email = $pension_info['email'];
$points = $pension_info['redeemable_points'];
$desc = $pension_info['write_up'];
$name_change = $pension_info['name_change'];
$logo = $pension_info['logo_url'];
$date_joined = $pension_info['date_joined'];
$isValidated = $_SESSION['entity']['validated'];
//$activePoliciesCount = count(getData('get_active_policies_by_insurer', $id));

$fileName = ucfirst(basename($_SERVER['PHP_SELF'], ".php"));

if ($fileName != 'Settings') {

    if ($isValidated == 'FALSE' || $isValidated == 'PENDING') {
        header("Location: settings.php");
    }
}
if (empty($_SESSION['notifCount'])) {
    $notifCount = getData("get_notification_count_pension", $id)['count'];
    $_SESSION['notifCount'] = $notifCount;
} else {
    $notifCount = $_SESSION['notifCount'];
}
?>
<!DOCTYPE html>

<html>
    <head>
        <title>iInsurance</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="icon" type="image/png" href="../img/favicon.ico">
        <link rel="apple-touch-icon" href="../img/favicon.ico">

        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

        <link href="../css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/timeline.css" rel="stylesheet" type="text/css"/>
        <link href="../css/simplePagination.css" rel="stylesheet" type="text/css"/>
        <link href="../css/animate.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
        <link href="../css/ion.rangeSlider.skinFlat.css" rel="stylesheet" type="text/css"/>
        <link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style_p.css" rel="stylesheet" type="text/css"/>
        <link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.min.js" type="text/javascript"></script>-->
        <script src="../js/Chart.bundle.min.js" type="text/javascript"></script>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.php"><img src="../img/logos/uic2.png" alt="uic logo"/></a>
                </div>

                <div class="collapse navbar-collapse navbar-right" id="bs-navbar-collapse">

                    <ul class="nav navbar-nav">

                        <?php
                        if ($isValidated == 'TRUE') {
                            echo '<li><a href="home.php">DASHBOARD</a></li>';
                        }
                        ?>
                        <li class="dropdown dropdown-hover">
                            <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">

                                <?php
                                if ($isValidated == 'TRUE') {
                                    if ($notifCount > 0) {
                                        echo '<div id="notifCount" class="animated swing infinite ">' . $notifCount . '</div>';
                                    }
                                }
                                ?>

                                <i class="fa fa-bell-o"></i>
                            </a>
                            <ul class = "dropdown-menu" data-notifcount = "<?php echo $notifCount; ?>" data-notifids="" role = "menu">
                                <li id = "notif_header" class = "text-bold">Notifications <i class = "fa fa-spinner fa-pulse pull-right"></i></li>

                            </ul>
                        </li>

                        <li>
                            <a href="settings.php" title="Settings">
                                <img id = "header-logo" class = "img-responsive img-circle" width = "55px" height = "55px" src="../img/logos/<?php echo $logo ?>" alt = "pic"/>
                            </a>
                        </li>

                      </ul>

                </div>
            </div>
        </nav>

        <div id="minibar">
            <span><strong>Redeemable Points</strong>: &nbsp;
                <span id="rp"> <?= $points ?> </span> Points &nbsp;
                <a style="color: white;" id="point_info" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-content="This is the points payment and for policy purchase and renewal"><i class="fa fa-question-circle"></i></a>
            </span>&nbsp;&nbsp;
            <span id="logout" class="pull-right logout">
                Logout
            </span>
        </div>

        <div id="breadcrumb-display">
            <span class="active pull-left"><a href="home.php">Dashboard</a></span>
            <span class="active pull-left"><?php echo $fileName == 'Home' ? '' : '&nbsp;/&nbsp;' . $fileName ?></span>
            <span class="small pull-right">
                <?php
                if (!empty($pension_info['last_login'])) {
                    echo 'YOUR LAST LOGIN WAS ' . dateFormatFine($pension_info['last_login'], 'long');
                } else {
                    echo 'WELCOME, ' . $pension_info['name'];
                }
                ?> 
            </span>
        </div>
