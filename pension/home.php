<?php
require_once './fragments/header.php';

$total = getData('get_pension_signups_count', $id);
?>

<div id="home-main" class="container">
    <div id="north" class="row">

        <div id="north-sidebar" class="col-sm-3">
            Live Overview
            <hr/>
            <div class="box-shadow--2dp content-white">
                <h4 class="text-center" style="margin: 30px;"><?php echo $total ?> Sign-ups</h4><br/>
            </div>
        </div>

        <div id="north-content" class="col-sm-8 col-sm-offset-1">
            <span><i class="fa fa-line-chart"></i> Time-line chart<strong></strong></span>

            <hr/>
            <div class="box-shadow--2dp content-white">
                <div id="info"></div>
                <div class="panel-group panel-accordion" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                    Filter
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                <form id="lineChartFilter">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label for="gender">Gender </label>
                                            <select class="form-control" id="gender_l" name="gender_l">
                                                <option selected value=""></option>
                                                <option value="MALE">Male</option>
                                                <option value="FEMALE">Female</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-6">
                                            <label for="date">Date range </label>
                                            <div class="input-group input-daterange">
                                                <input type="text" class="form-control" id="startDate_l" name="startDate_l" value="" data-date-end-date="0d">
                                                <div class="input-group-addon">to</div>
                                                <input type="text" class="form-control" id="endDate_l" name="endDate_l" value="" data-date-end-date="0d">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label for="active">Active </label>
                                            <select class="form-control" id="active_l" name="active_l">
                                                <option selected value=""></option>
                                                <option value="TRUE">True</option>
                                                <option value="FALSE">False</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 10px">
                                        <div class="col-sm-offset-10 col-sm-2" style="margin-top: 30px">
                                            <button id="filterSubmit" type="submit" class="btn btn-sm btn-primary btn-rounded"  name="btn-register">
                                                <i class="fa fa-filter btn-icon-left btn-rounded"></i> Apply</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div><br>

                    <canvas id="myLineChart" width="200" height="70"></canvas>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div id="south" class="row">
        <div class="col-sm-3">&nbsp;</div>

        <div id="south-content" class="col-sm-8 col-sm-offset-1">
            Recent Activity
            <hr/>
            <div id="recent_activity" class="box-shadow--2dp content-white">

            </div>
        </div>
    </div>
</div>

<?php
include './fragments/footer.php';
?>

<script src="../js/jquery-3.1.1.js" type="text/javascript"></script>
<script src="../js/jquery-ui.js" type="text/javascript"></script>
<script src="../js/parsley.min.js" type="text/javascript"></script>
<script src="../js/bootstrap.js" type="text/javascript"></script>
<script src="../js/ion.rangeSlider.js" type="text/javascript"></script>
<script src="../js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="../js/spin.min.js" type="text/javascript"></script>
<script src="../js/spinner-config.js" type="text/javascript"></script>
<script src="../js/script_p.js" type="text/javascript"></script>

</body>
</html>
