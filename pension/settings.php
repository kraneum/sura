<?php
require_once './fragments/header.php';

$accountNo = $pension_info['account_no'];
?>

<div id="settings-main" class="container">
    <div class="col-sm-3 box-shadow--2dp" style="background: white; padding: 0;">
        <div id="settings-profile" class="text-center">
            <p><img id="settings-logo" src="../img/logos/<?php echo $logo ?>" alt="logo" width="130px" height="130px" class="img-responsive img-circle center-block"/></p>
            <p><span id = "name" class="lead"><?php echo $name ?></span></p>
            <p><span>Member since <?php echo dateFormat($date_joined, '/') ?></span></p>
        </div>
        <div id="settings-nav">
            <p id="account-nav"><strong>Account</strong> <img src="../img/icons/left_arrow_grey.png" class="pull-right"></p>
            <p id="uploaddoc-nav"><strong>Upload Logo 
                    <?php
                    if ($isValidated == 'FALSE') {
                        echo '& Documents<span class="animated infinite tada badge box-shadow--2dp" style="font-weight: normal;margin-left:15px; font-size:10px; background-color:red;">UPLOADS</span>';
                    } else if ($isValidated == 'PENDING') {
                        echo '& Documents<span class="animated infinite tada badge badge box-shadow--2dp" style="font-weight: normal;margin-left:15px; font-size:10px; background-color:orange;">PENDING</span>';
                    }
                    ?>
                </strong> <img src="../img/icons/left_arrow_grey.png" class="pull-right"></p>
            <p id="setnot-nav"><strong>FAQs</strong> <img src="../img/icons/left_arrow_grey.png" class="pull-right"></p>
            <p id="help-nav"><strong>Help</strong> <img src="../img/icons/left_arrow_grey.png" class="pull-right"/></p>
        </div>
        <a href ="../php/insurer_logout.php"><div id="settings-logout" class="text-center"><strong>LOG OUT</strong></div></a>

    </div>

    <div id="content" class="container col-sm-8 box-shadow--2dp" style="background: white; margin-left: 20px;">
        <div id="info"></div>
        <div id="profile">

            <form id ="changeNameForm" enctype="multipart/form-data">
                <div class="row">
                    <label class="col-sm-2" for="name"><span class="pull-right">Name</span> </label>
                    <div id="name_change">
                        <?php
                        if (empty($name_change) || $name_change == 'NULL') {
                            echo '<input class="col-sm-3 settings-input" style="border-right: none" id="name" name="name" type="text" value="' . $name . '" data-parsley-required />
                        <input class="settings-input col-sm-4" style="padding: 5.5px; border-left: none" id="name_doc" name="docs[]" multiple type="file" accept="image/jpeg, image/png, application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document" data-parsley-required data-parsley-trigger="change" data-parsley-filemimetypes="image/jpeg, image/png, application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document"/>
                        
                        <div class="col-sm-2"><button id="change_name" type="submit" class="btn btn-primary box-shadow--2dp settings-btn btn-rounded pull-right" name="save"><strong>SAVE</strong></button>
                        </div>';
                        } else {
                            echo '<h4 class="col-sm-5">PENDING APPROVAL...</h4>';
                        }
                        ?>
                    </div>
                </div>
            </form>
            <hr/>

            <form id ="profileForm">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3" for="desc"><span class="pull-right"> Description *</span> </label>
                        <textarea class="col-sm-5 settings-input" id="desc" name="desc"  type="text" data-parsley-required ><?php echo $desc ?></textarea>
                    </div>
                    <div class="col-sm-4"></div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3" for="email"><span class="pull-right"> Email  *</span> </label>
                        <input class="col-sm-5 settings-input" id="email" name="email" value="<?php echo $email ?>" data-parsley-required data-parsley-type="email" />
                    </div>
                    <div class="col-sm-4"></div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3" for="phone"><span class="pull-right">Phone Number *</span> </label>
                        <input class="col-sm-5 settings-input" id="phone" name="phone" value="<?php echo $phone ?>" data-parsley-required data-parsley-type="digits" data-parsley-minlength="9" data-parsley-maxlength="11"  data-parsley-type-message="This value must be digits only" data-parsley-minlength-message="This value should be between 9 and 11 digits" data-parsley-maxlength-message="This value should be between 9 and 11 digits"/>
                    </div>
                    <div class="col-sm-4"></div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3" for="acc"><span class="pull-right">Account Number *</span> </label>
                        <input class="col-sm-5 settings-input" id="acc" name="acc" value="<?php echo $accountNo ?>" placeholder="Enter bank account number" data-parsley-required data-parsley-minlength="10" data-parsley-maxlength="10"  data-parsley-type="digits" data-parsley-minlength-message="This value should be 10 digits" data-parsley-maxlength-message="This value should be 10 digits" />
                    </div>
                    <div class="col-sm-4"></div>
                </div>

                <div class="row">
                    <div class="col-sm-3"></div>
                    <span class="col-sm-5" style="margin-bottom: 8px"><strong>Account Resets</strong></span>
                    <div class="col-sm-4"></div>
                </div>

                <div class="row"> 
                    <div class="col-sm-3"></div>
                    <span class="col-sm-5" style="width: 46.5%">
                        <button type="button" id="password-reset" class="btn btn-primary box-shadow--2dp settings-btn btn-rounded btn-block" name="password_reset" ><strong>RESET PASSWORD</strong></button>
                    </span>
                    <div class="col-sm-4"></div>
                </div> 

                <div class="row"> 
                    <div class="col-sm-3"></div>
                    <span class="col-sm-5" style="width: 46.5%">
                        <button type="button" id="sq-reset" class="btn btn-primary box-shadow--2dp settings-btn btn-rounded btn-block" name="sq_reset"><strong>RESET SECURITY QUESTION</strong></button>
                    </span>
                    <div class="col-sm-4" style="width: 25.5%">
                        <button id="save" type="submit" class="btn btn-primary box-shadow--2dp settings-btn btn-rounded pull-right" name="save"><strong>SAVE</strong></button>
                    </div>
                </div>

            </form>
        </div>
        <div id="sub-content"></div>
    </div>

</div>

</div>

<?php
include './fragments/footer.php';
?>

<script src="../js/jquery-3.1.1.js" type="text/javascript"></script>
<script src="../js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="../js/parsley.min.js" type="text/javascript"></script>
<script src="../js/bootstrap.js" type="text/javascript"></script>
<script src="../js/parsley-laraextras.min.js" type="text/javascript"></script>
<script src="../js/parsley_file_validators.js" type="text/javascript"></script>
<script src="../js/spin.min.js" type="text/javascript"></script>
<script src="../js/spinner-config.js" type="text/javascript"></script>
<script src="../js/script_p.js" type="text/javascript"></script>

</body>
</html>