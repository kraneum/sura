<?php

function csv_slice($list, $offset = 0, $length = 0) {
    $i = 0;
    while (false !== ($row = fgetcsv($list))) {
        if ($i++ < $offset)
            continue;
        if (0 < $length && $length <= ($i - $offset - 1))
            break;
        yield $row;
    }
}
