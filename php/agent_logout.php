<?php

require_once 'sura_config.php';
require_once 'sura_functions.php';

if (!isset($_SESSION)) {
    session_start();
}

if (!isset($_SESSION['agentid'])) {
    header("Location: ../agent/login.php");
} else {
    $con = makeConnection();
    $$id = $_SESSION['agentid'];
    $date = (new DateTime())->format(DateTime::ISO8601);
    
    runSimpleUpdateQuery($con, 'agent', ['last_login'], ["'$date'"], ['id'], ['='], [$$id]);
    session_unset();
    session_destroy();
    header("Location: ../agent/login.php");
    exit;
}
