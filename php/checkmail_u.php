<!DOCTYPE html>

<html>
    <head>
        <title>iInsurance</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="../img/favicon.png" />
        <link rel="icon" type="image/png" href="../img/favicon.png">
        <link rel="apple-touch-icon" href="../img/favicon.png">

        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">
            body {
                padding: 0 25%;
            }
            form {
                margin: 0 20%;
            }
            #info {
                margin: 20px 0;
            }
            #content > img {
                margin-bottom: 5%;
                opacity: .5;
            }

            header > img {
                margin: 5% 0 7%;
            }
            h3 {
                margin-bottom: 20px
            }
        </style>
    </head>
    <body>

        <header><img width="250" height="250" class="img-responsive center-block" src="../img/logos/uic2.png" alt="" style="margin-top: 20%"/></header>
        <div id="content" class="text-center">
            <h3>Kindly check your mail (or spam) for an email from us containing the link</h3>
            <p style="font-style: italic;margin-top: 20px">..thanks for your loyalty</p>
            <?php
            session_start();
            
            
            ?>
            <p style="font-style: italic;margin-top: 20px">Click <a href="http://localhost/iinsurance">here to login</a></p>
        </div>

    </body>
</html>


