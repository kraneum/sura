<?php

require_once __DIR__ . '/../sura_config.php';
require_once __DIR__ . '/../sura_functions.php';


$con = makeConnection();
autoCommit($con, false);

$basicAmount = getBasicAmounts($con);
$healthPrice = $basicAmount['health'];
$generalPrice = $basicAmount['general'];

$persons = fetchUsers($con, $basicAmount);

if (!empty($persons)) {
    $leads = fetchInsuranceLeads($con);
    $health_insurer_id = $leads['health'];
    $general_insurer_id = $leads['general'];

    $healthPersons = [];
    $generalPersons = [];
    $date = new DateTime();
    $successful_debit_flag = false;

    foreach ($persons as $p) {
        if (intval($p['health_point_balance']) > $healthPrice) {
            $healthPersons[] = $p;
            $successful_debit_flag = performUserDebit('health', $healthPrice, $con, $p, $health_insurer_id, $date->format(DateTime::ISO8601));
        } else {
            $generalPersons[] = $p;
            $successful_debit_flag = performUserDebit('general', $generalPrice, $con, $p, $general_insurer_id, $date->format(DateTime::ISO8601));
        }
        
        if (!$successful_debit_flag) {
            break;
        }
    }
    
    autoCommit($con, true);
    
    if ($successful_debit_flag) {
        generateHealthPersons($con, $healthPersons, $date);
        generateGeneralPersons($con, $generalPersons, $date);
    }
}

function fetchInsuranceLeads($con) {
    $leads = runSimpleFetchQuery($con, ['health_insurer_id', 'general_insurer_id'], 'insurer_leads', ['id'], ['='], ['1'])['result'];
    return [
        'health' => $leads[0]['health_insurer_id'],
        'general' => $leads[0]['general_insurer_id']
    ];
}

function fetchUsers($con, $basicAmount) {
    $healthPrice = $basicAmount['health'];
    $generalPrice = $basicAmount['general'];
    $users = runGetAllUsersWithoutBasicPolicies($con, $healthPrice, $generalPrice)['result'];
    return $users;
}

function getBasicAmounts($con) {
    $healthPrice = 0;
    $generalPrice = 0;
    $basicAmount = runSimpleFetchQuery($con, ['price', 'type_id'], 'policies', ['insurer_id'], ['='], [0], '', '', '')['result'];
    foreach ($basicAmount as $ba) {

        if ($ba['type_id'] == 1) {
            $healthPrice = $ba['price'];
        } else {
            $generalPrice = $ba['price'];
        }
    }

    return [
        'health' => $healthPrice,
        'general' => $generalPrice
    ];
}

function performUserDebit($type, $typePrice, $con, $person, $insurerID, $date) {
    $flag = true;
    $userID = $person['id'];
    $new_health_points = 0;
    $new_general_points = 0;

    do {
        ///////////////////////////// 
        if ($type == 'health') {
            $new_health_points = $person['health_point_balance'] - $typePrice;

            $result = runSimpleUpdateQuery($con, 'users', ['health_point_balance'], ["$new_health_points"], ['id'], ['='], [$userID]);
        } else {
            $new_general_points = $person['general_point_balance'] - $typePrice;
            $result = runSimpleUpdateQuery($con, 'users', ['general_point_balance'], ["$new_general_points"], ['id'], ['='], [$userID]);
        }
        if ($result['err']['code']) {
            if ($flag) {
                $flag = false;
                break;
            }
        }

        ////////////////////////////////
        $insurer_points = runSimpleFetchQuery($con, ['redeemable_points'], 'insurer', ['id'], ['='], [$insurerID], '', '', '1')['result'];
        $new_insurer_points = intval($insurer_points) + $typePrice;
        $result = runSimpleUpdateQuery($con, 'insurer', ['redeemable_points'], ["'$new_insurer_points'"], ['id'], ['='], [$insurerID]);
        if ($result['err']['code']) {
            if ($flag) {
                $flag = false;
                break;
            }
        }
        ////////////////////////////////
        $ref = 'UICI-' . $userID . round(microtime(true) * 1000);
        $total_previous_points = intval($person['health_point_balance']) + intval($person['general_point_balance']);
        $new_total_points = $total_previous_points - $typePrice; // one must only be > 0

        $result = runSimpleInsertQuery($con, 'transactions', ['reference', 'point_worth', 'date_transacted', 'type_id', 'currency_id', 'from_whom', 'to_who', 'previous_balance_from', 'previous_balance_to'], ["'$ref'", $typePrice, "'$date'", 2, 1, $userID, $insurerID, "'$new_total_points'", "'$new_insurer_points'"]);
        if ($result['err']['code']) {
            if ($flag) {
                $flag = false;
                break;
            }
        }
        $insertedID = $result['insertedId'];
        ////////////////////
        $result = runSimpleInsertQuery($con, 'user_log', ['action', 'info', 'date', 'user_id'], ["'BASIC_POLICY_SUB'", "'$insertedID'", "'$date'", $userID]);
        if ($result['err']['code']) {
            if ($flag) {
                $flag = false;
            }
        }
    } while (false);

    if ($flag) {
        commit($con);
        return true;
    } else {
        rollback($con);
        return false;
    }
}

function generateHealthPersons($con, $healthPersons, $date) {
    $columnHeader = [['#', 'First Name', 'Last Name', 'Date of Birth', 'Gender', 'Phone',
    'Email', 'Address', 'Local Government', 'Health iPoints', 'Enroll', 'Remark']];


    $list = array_merge($columnHeader, $healthPersons);
    $list_name = $date->format('Ymd_His');

    $file = fopen('../../generated_users/health/' . $list_name . '.csv', "w");

    foreach ($list as $line) {
        unset($line['general_point_balance']);
        fputcsv($file, $line);
    }

    fclose($file);

    runSimpleInsertQuery($con, 'insurer_gen_users', ['type', 'list_name', 'date_created'], ["'HEALTH'", "'" . $list_name . "'", "'" . $date->format(DateTime::ISO8601) . "'"]);
}

function generateGeneralPersons($con, $generalPersons, $date) {
    $columnHeader = [['#', 'First Name', 'Last Name', 'Date of Birth', 'Gender', 'Phone',
    'Email', 'Address', 'Local Government', 'General iPoints', 'Enroll', 'Remark']];


    $list = array_merge($columnHeader, $generalPersons);
    $list_name = $date->format('Ymd_His');

    $file = fopen('../../generated_users/general/' . $list_name . '.csv', "w");

    foreach ($list as $line) {
        unset($line['health_point_balance']);
        fputcsv($file, $line);
    }


    fclose($file);

    runSimpleInsertQuery($con, 'insurer_gen_users', ['type', 'list_name', 'date_created'], ["'GENERAL'", "'" . $list_name . "'", "'" . $date->format(DateTime::ISO8601) . "'"]);
}
