<?php

set_time_limit(0);

require_once(__DIR__ . '/../sura_config.php');
require_once(__DIR__ . '/../sura_functions.php');
require_once(__DIR__ . '/../_csv_slice.php');

$conn = makeMedoo();

if (!$conn) {
    exit('Connection Error');
}

$number_to_process_at_a_time = 500;
$user_creation__error = false;
$points_update_error = false;
$insufficient_funds_flag = false;

////////////////////////////////
print "Running from: " . (new DateTime())->format(DateTime::ISO8601) . "/n";
/////////////////////////////////

$task = $conn->get('insurer_gen_users', '*', [
    'queued' => 'TRUE'
        ]);

if (empty($task)) {
    exit("No Tasks/n Stopped at: " . (new DateTime())->format(DateTime::ISO8601) . "/n/n");
}

$list_id = $task['id'];
$list_type = strtolower($task['type']);
$list_name = $task['list_name'];

$date = (new DateTime())->format(DateTime::ISO8601);

////////////////////////////////////////////////////////////////////// 

$list = fopen(__DIR__ . '/../../generated_users_res/' . $list_type . '/' . $list_name . '.csv', 'r') or die('Unable to open file!');

$arraylist = iterator_to_array(csv_slice($list));

if (count($arraylist) > 0) {

    $result = $conn->update('merchant_transfer_jobs', ['status' => 'PROCESSING'], ['id' => $list_id]);

    $policyID = null;
    $policyPrice = null;
    $insurer_lead_id = null;
    if ($list_type == 'health') {
        $res = $conn->get('policies', ['id', 'price'], ['insurer_id' => 0, 'type_id' => 1]);
        $policyID = $res['id'];
        $policyPrice = intval($res['price']);
        $insurer_lead_id = $conn->get('insurer_leads', ['health_insurer_id'], ['id' => 1]);
    } else {
        $res = $conn->get('policies', ['id', 'price'], ['insurer_id' => 0, 'type_id' => 2]);
        $policyID = $res['id'];
        $policyPrice = intval($res['price']);
        $insurer_lead_id = $conn->get('insurer_leads', ['general_insurer_id'], ['id' => 1]);
    }

    foreach ($arraylist as $row) {
        $user_id = filter_var($row[0], FILTER_SANITIZE_NUMBER_INT);
        $enroll = strtolower(trim($row[10]));

        if (empty($policyID)) {
            exit("No Policy ID/n Stopped at: " . (new DateTime())->format(DateTime::ISO8601) . "/n/n");
        }

        if (empty($user_id)) {
            continue;
        }

        print "$user_id $enroll /n";

        if ($enroll == 'true' || $enroll == 'yes' || $enroll == 'approved') {
            shouldEnroll($user_id, $policyID, true);
        } else {
            shouldNotEnroll($conn, $user_id, $insurer_lead_id, $policyPrice, $list_type);
        }
    }
    fclose($list);


    $result = $conn->update('insurer_gen_users', ['queued' => 'COMPLETED', 'message' => 'COMPLETED'], ['id' => $list_id]);
} else {
    $result = $conn->update('insurer_gen_users', ['queued' => 'COMPLETED', 'message' => 'NO DATA'], ['id' => $list_id]);
}

echo 'success';

////////////////////////////////////
print "Stopped at: " . (new DateTime())->format(DateTime::ISO8601) . "/n/n";
///////////////////////////////////

function shouldEnroll($user_id, $policyID) {
//    print 'In progress/n';
    require_once __DIR__ . '/../suracommands/add_user_signup.php';
    $response = add_user_signup($user_id, $policyID, true);
//    print 'Ended/n';
}

function shouldNotEnroll($conn, $id, $insurerID, $price, $list_type) {
    $userInfo = $conn->get('users', ['f_name', 'l_name', 'phone', 'health_point_balance', 'general_point_balance'], ['id' => $id]);
    $f_name = $userInfo['f_name'];
    $l_name = $userInfo['l_name'];
    $phone = $userInfo['phone'];

    $insurerInfo = $conn->get('insurer', ['id', 'name', 'email', 'redeemable_points'], ['id' => $insurerID]);
    $insurer_name = $insurerInfo['name'];
    $insurer_email = $insurerInfo['email'];

    $conn->action(function($conn) use ($id, $insurerID, $price, $list_type, $userInfo, $insurerInfo) {
        $f_name = $userInfo['f_name'];
        $l_name = $userInfo['l_name'];
        $phone = $userInfo['phone'];
        $health_point_balance = intval($userInfo['health_point_balance']);
        $general_point_balance = intval($userInfo['general_point_balance']);

        $points = null;
        $other_points = null;
        if ($list_type == 'health') {
            $points = $health_point_balance;
            $other_points = $general_point_balance;
        } else if ($list_type == 'general') {
            $points = $general_point_balance;
            $other_points = $health_point_balance;
        }

        $insurerID = $insurerInfo['id'];
        $insurer_name = $insurerInfo['name'];
        $insurer_email = $insurerInfo['email'];
        $insurer_points = intval($insurerInfo['redeemable_points']);

        $date = (new DateTime())->format(DateTime::ISO8601);
        $ref = 'UICI-' . $id . round(microtime(true) * 1000);

        //Do transactions
//        print 'In progress/n';

        $new_point_balance = intval($points) + intval($price);
        if ($list_type == 'health') {
            $conn->update('users', ['health_point_balance' => $new_point_balance], ['id' => $id]);
        } else {
            $conn->update('users', ['general_point_balance' => $new_point_balance], ['id' => $id]);
        }

        $new_insurer_points = intval($insurer_points) - intval($price);
        $conn->update('insurer', ['redeemable_points' => "$new_insurer_points"], ['id' => $insurerID]);

        $new_total_points = $new_point_balance + $other_points;
        $conn->insert('transactions', ['reference' => "$ref", 'point_worth' => $price,
            'date_transacted' => "$date", 'type_id' => 15, 'currency_id' => 1, 'from_whom' => $insurerID,
            'to_who' => $id, 'previous_balance_from' => $new_insurer_points,
            'previous_balance_to' => $new_total_points]);

        $inserted_trans_id = $conn->id();

        $result = $conn->insert('user_log', ['action' => 'POINTS_REVERSAL', 'info' => "$inserted_trans_id", 'date' => "$date", 'user_id' => $id]);

//        print 'Ended/n';
    });

    $sms_message = "iPoints worth $price have been reversed to your account by iInsurance";
    require __DIR__ . '/../suracommands/send_sms.php';
    send_sms($f_name, $sms_message, "'$phone'", 234);

    require __DIR__ . '/../suracommands/send_mail.php';
    $subject = 'Policy Sign Up from iInsurance';
    $mail = '<html><body style="padding: 0 20%; font-size:14px"><div><h3>Hi ' . $insurer_name . ', < /h3>'
            . "You signed up a user '$f_name $l_name ($phone)' for a basic policy worth $price Loyalty Points"
            . '/n/n<div style="font-size: .8em">If you’ve received this email by mistake, simply ignore it.'
            . 'Your account won\'t be created if you don\'t click the confirmation link above.'
            . '/n/nThanks for your loyalty.</div></div></html></body>';
    send_mail($insurer_email, $subject, $mail);
}
