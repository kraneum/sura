<?php
//header('Content-type:application/json;charset=utf-8');

require_once(__DIR__ . '/../sura_config.php');
require_once(__DIR__ . '/../sura_functions.php');
require_once(__DIR__ . '/../_csv_slice.php');

$con = makeConnection_merchant();

if (!$con) {
    exit('Connection Error');
}

$number_to_process_at_a_time = 500;
$user_creation__error = false;
$points_update_error = false;
$insufficient_funds_flag = false;



////////////////////////////////
print "Running from: " . (new DateTime())->format(DateTime::ISO8601) . "\n";
/////////////////////////////////

$task = runSimpleFetchQuery($con, ['*'], 'merchant_transfer_jobs', ['STATUS'], ['='], ["'QUEUED'"])['result'];
if (empty($task)) {
    exit("No Tasks/n Stopped at: " . (new DateTime())->format(DateTime::ISO8601) . "/n/n");
}
$task = $task[0];

if (empty($task)) {
    disconnectConnection($con);
    exit();
}

$id = $task['id'];
$merchant_id = $task['merchant_id'];
$employee_id = $task['employee_id'];
$rec_entity = strtolower($task['rec_entity']) . 's';
$list_name = $task['list_name'];
$current_item = intval($task['current_item']);

$date = (new DateTime())->format(DateTime::ISO8601);

////////////////////////////////////////////////////////////////////// 

$merchantInfo = runSimpleFetchQuery($con, ['id', 'name', 'point_balance'], 'merchants', ['id'], ['='], ["'$merchant_id'"], '', '', '')['result'];
if (empty($merchantInfo)) {
    autoCommit($con, true);
    return ['status' => false, 'info' => 'MERCHANT_ID_NOT_RESOLVED'];
}
$merchant_points = intval($merchantInfo[0]['point_balance']);
$merchant_name = $merchantInfo[0]['name'];

$list = fopen(__DIR__ . '/../../merchant_transfer_files/' . $rec_entity . '/' . $list_name . '.csv', 'r') or die('Unable to open file!');

$arraylist = iterator_to_array(csv_slice($list, $current_item, $number_to_process_at_a_time));



if (count($arraylist) > 0) {
    require_once __DIR__ . '/../suracommands/send_sms.php';

    autoCommit($con, false);
    $result = runSimpleUpdateQuery($con, 'merchant_transfer_jobs', ['status'], ["'PROCESSING'"], ['id'], ['='], [$id]);


    foreach ($arraylist as $row) {
        print "Current item: " . $current_item . "/n";
        $phone = filter_var($row[0], FILTER_SANITIZE_STRING);
        $amount = filter_var($row[1], FILTER_SANITIZE_NUMBER_INT);

        if (empty($phone) || empty($amount) || $amount <= 0) {
            continue;
        }

        if (preg_match('/^(\+234)\d{10}/', $phone)) {
            $phone = '0' . substr($phone, 4);
        } else if (preg_match('/^(234)\d{10}/', $phone)) {
            $phone = '0' . substr($phone, 3);
        } else if (preg_match('/^0\d{10}/', $phone)) {
            
        } else
            continue;
        
        if ($amount > $merchant_points) {
            $insufficient_funds_flag = true;
            break;
        }

        // retrieves user info...
        $recipientInfo = runSimpleFetchQuery($con, ['id', 'f_name', 'email', 'phone', 'health_point_balance', 'general_point_balance'], 'users', ['phone'], ['='], ["'$phone'"], '', '', '')['result'];
        if (!empty($recipientInfo)) {
            $userid = $recipientInfo[0]['id'];
            $rec_health_points = intval($recipientInfo[0]['health_point_balance']);
            $rec_general_points = intval($recipientInfo[0]['general_point_balance']);
            $rec_name = $recipientInfo[0]['f_name'];
            $rec_email = $recipientInfo[0]['email'];
            $rec_number = $recipientInfo[0]['phone'];
        } else {
            $new_user = createUser($con, $phone, $date);
            if (false) {
                $user_creation__error = true;
                break;
            }
            $userid = $new_user;
            $rec_health_points = 0;
            $rec_general_points = 0;
            $rec_name = 'New user';
            $rec_email = '---';
            $rec_number = $phone;
        }

        ////////////////////////////////

        $flag = true;

        $new_rec_health_points = $rec_health_points + ceil($amount / 2);
        $new_rec_general_points = $rec_general_points + floor($amount / 2);

        $new_merchant_points = $merchant_points - $amount;

        require __DIR__ . '/../../merchants/transfer_points_to_user_.php';

        if ($flag) {
            commit($con);
            $merchant_points = $new_merchant_points;
            $current_item++;

            send_sms($rec_name, 'You were transferred ' . $amount . ' points from a Merchant - ' . $merchant_name, "'$rec_number'");
        } else {
            rollback($con);
            $points_update_error = false;
            break;
        }
    }
    fclose($list);

    ////////////////////////////////////
    print "Stopped at: " . (new DateTime())->format(DateTime::ISO8601) . "/n/n";
    ///////////////////////////////////

    $message = '';
    $status = 'QUEUED';

    if ($points_update_error) {
        $message = 'Points Update Error Occurred';
        $status = 'PROCESSING';
    } else if ($user_creation__error) {
        $message = 'Error creating user';
        $status = 'PROCESSING';
    } else if ($insufficient_funds_flag) {
        $message = 'Insufficient funds';
        $status = 'PROCESSING';
    } else {
        $message = 'Success';
        if (count($arraylist) < $number_to_process_at_a_time) {
            $status = 'COMPLETED';
            $message = 'Completed';
        }
    }

    $result = runSimpleUpdateQuery($con, 'merchant_transfer_jobs', ['status', 'current_item', 'message'], ["'$status'", "'$current_item'", "'$message'"], ['id'], ['='], [$id]);
} else {
    $result = runSimpleUpdateQuery($con, 'merchant_transfer_jobs', ['status', 'current_item', 'message'], ["'COMPLETED'", "'$current_item'", "'Completed'"], ['id'], ['='], [$id]);
}


autoCommit($con, true);

disconnectConnection($con);


echo json_encode('success', JSON_PRETTY_PRINT);

/* * ********************************************* */

function createUser($con, $phone, $date) {
    $default_password = '3rfa4v';
    $password = password_hash($default_password, PASSWORD_BCRYPT);
    $res = runSimpleInsertQuery($con, "users", ['phone', 'date_joined', 'password', 'profile_pic', 'approved'], ["'$phone'", "'$date'", "'$password'", "'men.png'", "'FALSE'"]);
    if (!$res['err']['code']) {
        send_sms('', 'An account have been created for you on iInsurance. To access your account, please login at https://iinsurance.ng/insurance. Your password is ' . $default_password . '. You can change it in Settings page', "'$phone'");
        return $res['insertedId'];
    } else {
        return false;
    }
}
