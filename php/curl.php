<?php

function getData($command, ...$params) {

    $url = "http://localhost/iinsurance/php/suracommands.php?command=$command";
    if (func_num_args() > 1) {
        for ($i = 0; $i < count($params); $i++) {
            $url .= "&param" . intval($i + 1) . "=" . $params[$i];
        }
    }

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $contents = curl_exec($ch);

    curl_close($ch);
    return json_decode($contents, true);
}
