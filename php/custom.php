<?php

session_start();
require 'sura_config.php';
require 'sura_functions.php';

if (!empty($_SESSION['userid'])) {
    $id = $_SESSION['userid'];
} else {
    header("Location: ../login.php");
}

$con = makeConnection();
$meta = htmlspecialchars(strip_tags(trim($_POST['meta'])));

if ($meta === 'delete_policy') {
    $signupID = htmlspecialchars(strip_tags(trim($_POST['signupID'])));
    runSimpleDeleteQuery($con, 'user_signups', ['user_id', 'id'], [$id, $signupID])['result'];
    echo 'success';
    disconnectConnection($con);
}
