<?php

//$micro_date = microtime();
//$date_array = explode(" ", $micro_date);
//$date = date("Y/m/d H:i:s", $date_array[1]);
//echo "Date: $date" . "<br>";
//echo strtotime($date) . '<br>';
//
//echo date("Y-m-d H:i:s", 1476489600) . '<br>';
//echo date('Y-m-d', strtotime("-3 months", date('Y-m-d')));
//echo strtotime("-3 months", 1476489600).'<br>';
//echo date("Y/m/d H:i:s", 1468540800);
//echo floor(microtime(true));
//echo date('d-m-Y', strtotime("+3 months", time())).'<br>';
//
//echo (new DateTime('+2 months'))->format(DateTime::ISO8601).'<br>';
//
//echo (new DateTime())->add(new DateInterval('P2M'))->format(DateTime::ISO8601);
//
//$datetime = new DateTime();
//require_once('sura_config.php');
//require_once('sura_functions.php');
//$con = makeConnection();
//$date_to_charge = runSimpleFetchQuery($con, ['date_to_charge'], 'payment_schedule', ['user_id', 'signup_id'], ['=', '='], [24, 35], '', 'id DESC', '1')['result'];
//echo $date_to_charge.'<br>';
//$new_payday = (new DateTime($date_to_charge))->add(new DateInterval('P1M'))->format(DateTime::ISO8601);
//echo $new_payday;

function toMonth($num, $type = 'long') {
    switch ($num) {
        case '01':
            return $type === 'long' ? 'January' : 'Jan';
        case '02':
            return $type === 'long' ? 'February' : 'Feb';
        case '03':
            return $type === 'long' ? 'March' : 'Mar';
        case '04':
            return $type === 'long' ? 'April' : 'Apr';
        case '05':
            return $type === 'long' ? 'May' : 'May';
        case '06':
            return $type === 'long' ? 'June' : 'Jun';
        case '07':
            return $type === 'long' ? 'July' : 'Jul';
        case '08':
            return $type === 'long' ? 'August' : 'Aug';
        case '09':
            return $type === 'long' ? 'September' : 'Sep';
        case '10':
            return $type === 'long' ? 'October' : 'Oct';
        case '11':
            return $type === 'long' ? 'November' : 'Nov';
        case '12':
            return $type === 'long' ? 'December' : 'Dec';

        default:
            return "Incorrect Month Value";
    }
}

function dateFormat($d, $seperator) {
    return (new DateTime($d))->format('j'.$seperator.'n'.$seperator.'Y');
}

function dateFormatFine($d, $type) {
    $_date = (new DateTime($d))->format('j/n/Y');
    $str = explode('/', $_date);
    return $str[0] . ' ' . toMonth($str[1], $type) . ', ' . $str[2];
}


