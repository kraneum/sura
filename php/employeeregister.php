<?php

session_start();
require 'sura_config.php';
require 'sura_functions.php';

if (isset($_SESSION['user']) != "") {
    header("Location: dashboard.php");
}

$error = false;
$errMsg = null;

if (isset($_POST['btn-empregister'])) {
    // clean user inputs to prevent sql injections
    $fname = "'" . htmlspecialchars(strip_tags(trim($_POST['fname']))) . "'";
    $midname = "'" . htmlspecialchars(strip_tags(trim($_POST['midname']))) . "'";
    $lname = "'" . htmlspecialchars(strip_tags(trim($_POST['lname']))) . "'";
    $empID = "'" . htmlspecialchars(strip_tags(trim($_POST['empID']))) . "'";
    $insurerID = "'" . htmlspecialchars(strip_tags(trim($_POST['insurerID']))) . "'";
    $phone = "'" . htmlspecialchars(strip_tags(trim($_POST['phone']))) . "'";
    $email = "'" . htmlspecialchars(strip_tags(trim($_POST['email']))) . "'";
    $username = "'" . htmlspecialchars(strip_tags(trim($_POST['username']))) . "'";
    $password = "'" . htmlspecialchars(strip_tags(trim($_POST['password']))) . "'";

    $_email = htmlspecialchars(strip_tags(trim($_POST['email'])));
//    basic email validation
    if (!filter_var($_email, FILTER_VALIDATE_EMAIL)) {
        $error = true;
        $errMsg = "Please enter valid email address.\n";
    } else {
        $con = makeConnection_merchant();
        // check email exist or not
        $query = "SELECT email FROM company_admins WHERE email='$_email'";
        $result = mysqli_query($con, $query);
        $count = mysqli_num_rows($result);
        if ($count != 0) {
            $error = true;
            $errMsg .= "<span style=\"color:red\">Provided Email is already in use.</span>\n";
        }
    }
    // password validation
//    if (empty($pass)) {
//        $error = true;
//        $passError = "Please enter password.";
//    } else if (strlen($pass) < 6) {
//        $error = true;
//        $passError = "Password must have atleast 6 characters.";
//    }
    // password encrypt using SHA256();
//    $password = hash('sha256', $pass);
    // if there's no error, continue to signup
    if (!$error) {
        $con = makeConnection();

        $res = runSimpleInsertQuery($con, "company_admins", ['f_name', 'm_name', 'l_name', 'employee_id_no', 'insurer_id', 'phone', 'email', 'username', 'password'], [$fname, $midname, $lname, $empID, $insurerID, $phone, $email, $username, $password]);

        if ($res) {
            $error = "false";
            $errMsg = "<span style = \"color:green\">Successfully registered, you may login now</span>";
            unset($fname);
            unset($midname);
            unset($lname);
            unset($empID);
            unset($insurerID);
            unset($phone);
            unset($email);
            unset($username);
            unset($password);
        } else {
            $error = "true";
            $errMsg = "<span style=\"color:red\">Something went wrong, try again later...</span>";
        }
    }
}
