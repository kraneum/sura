<?php 



$first_four_set = array (
    "0"=>"*",
    "1"=>"d",
    "2"=>"!",
    "3"=>"}",
    "4"=>"Z",
    "5"=>"=",
    "6"=>"%",
    "7"=>"&",
    "8"=>"X",
    "9"=>"-",
);


$set_1 = array (
    "a"=>"$",
    "b"=>"w",
    "c"=>"2",
    "d"=>"d",
    "e"=>"]",
    "f"=>")",
    "g"=>"X",
    "h"=>"t",
    "i"=>"l",
    "j"=>"+",
    "k"=>"f",
    "l"=>"v",
    "m"=>"[",
    "n"=>"9",
    "o"=>"G",
    "p"=>"*",
    "q"=>"|",
    "r"=>"8",
    "s"=>"^",
    "t"=>"z",
    "u"=>"4",
    "v"=>"7",
    "w"=>"-",
    "x"=>"&",
    "y"=>"n",
    "z"=>",",
    //nums
    "0"=>"T",
    "1"=>"(",
    "2"=>"J",
    "3"=>"M",
    "4"=>"!",
    "5"=>"R",
    "6"=>"y",
    "7"=>"%",
    "8"=>"_",
    "9"=>" ",
    //extras
    " "=>"U",
    "-"=>"}",
    "="=>"F",
    "!"=>"5",
    "@"=>"c",
    "#"=>"{",
    "$"=>"P",
    "%"=>"e",
    "^"=>"=",
    "&"=>"6",
    "*"=>"s",
    "("=>"@",
    ")"=>"V",
    "_"=>"A",
    "+"=>"B",
    "{"=>"3",
    "}"=>"q",
    "["=>"o",
    "]"=>".",
    "|"=>"0",
    "."=>"1",
    ","=>"#",
);



$set_2 = array (
    "a"=>"0",
    "b"=>"Y",
    "c"=>"]",
    "d"=>"M",
    "e"=>"^",
    "f"=>"K",
    "g"=>"=",
    "h"=>"+",
    "i"=>"Z",
    "j"=>"R",
    "k"=>"1",
    "l"=>"}",
    "m"=>"{",
    "n"=>"B",
    "o"=>"N",
    "p"=>"h",
    "q"=>"[",
    "r"=>")",
    "s"=>"_",
    "t"=>" ",
    "u"=>"&",
    "v"=>"l",
    "w"=>"2",
    "x"=>"4",
    "y"=>"(",
    "z"=>"|",
    //nums
    "0"=>"#",
    "1"=>"i",
    "2"=>"k",
    "3"=>"@",
    "4"=>"!",
    "5"=>"*",
    "6"=>"u",
    "7"=>"G",
    "8"=>"o",
    "9"=>"J",
    //extras
    " "=>"7",
    "-"=>"C",
    "="=>"8",
    "!"=>"s",
    "@"=>"9",
    "#"=>"p",
    "$"=>"E",
    "%"=>"5",
    "^"=>"x",
    "&"=>"f",
    "*"=>"T",
    "("=>"W",
    ")"=>"a",
    "_"=>"V",
    "+"=>"$",
    "{"=>".",
    "}"=>"Q",
    "["=>"d",
    "]"=>",",
    "|"=>"-",
    "."=>"%",
    ","=>"3",
);


$set_3 = array (
    "a"=>"&",
    "b"=>"0",
    "c"=>"Z",
    "d"=>"i",
    "e"=>"+",
    "f"=>",",
    "g"=>"[",
    "h"=>"}",
    "i"=>"S",
    "j"=>"c",
    "k"=>"=",
    "l"=>"^",
    "m"=>"w",
    "n"=>"4",
    "o"=>"q",
    "p"=>"@",
    "q"=>"3",
    "r"=>"8",
    "s"=>"V",
    "t"=>"B",
    "u"=>"|",
    "v"=>"{",
    "w"=>" ",
    "x"=>"2",
    "y"=>"a",
    "z"=>"U",
    //nums
    "0"=>"L",
    "1"=>"t",
    "2"=>"g",
    "3"=>"_",
    "4"=>"O",
    "5"=>"%",
    "6"=>"f",
    "7"=>"$",
    "8"=>"]",
    "9"=>"E",
    //extras
    " "=>"K",
    "-"=>"1",
    "="=>"D",
    "!"=>"H",
    "@"=>"7",
    "#"=>"-",
    "$"=>"(",
    "%"=>"n",
    "^"=>"5",
    "&"=>"6",
    "*"=>"9",
    "("=>"J",
    ")"=>"p",
    "_"=>")",
    "+"=>".",
    "{"=>"Y",
    "}"=>"x",
    "["=>"*",
    "]"=>"#",
    "|"=>"m",
    "."=>"R",
    ","=>"!",
);



$set_4 = array (
    "a"=>"+",
    "b"=>"@",
    "c"=>"-",
    "d"=>" ",
    "e"=>"b",
    "f"=>"c",
    "g"=>"L",
    "h"=>"x",
    "i"=>"2",
    "j"=>"M",
    "k"=>"W",
    "l"=>"$",
    "m"=>"*",
    "n"=>"[",
    "o"=>")",
    "p"=>"_",
    "q"=>"}",
    "r"=>",",
    "s"=>"H",
    "t"=>"(",
    "u"=>"8",
    "v"=>"|",
    "w"=>"O",
    "x"=>"&",
    "y"=>"9",
    "z"=>"r",
    //nums
    "0"=>"g",
    "1"=>"z",
    "2"=>"#",
    "3"=>"=",
    "4"=>".",
    "5"=>"K",
    "6"=>"^",
    "7"=>"{",
    "8"=>"!",
    "9"=>"D",
    //extras
    " "=>"J",
    "-"=>"4",
    "="=>"f",
    "!"=>"7",
    "@"=>"6",
    "#"=>"t",
    "$"=>"I",
    "%"=>"3",
    "^"=>"0",
    "&"=>"j",
    "*"=>"V",
    "("=>"5",
    ")"=>"n",
    "_"=>"%",
    "+"=>"Q",
    "{"=>"y",
    "}"=>"p",
    "["=>"U",
    "]"=>"s",
    "|"=>"A",
    "."=>"1",
    ","=>"]",
);





$set_5 = array (
    "a"=>"c",
    "b"=>"n",
    "c"=>"V",
    "d"=>"f",
    "e"=>"b",
    "f"=>"H",
    "g"=>"|",
    "h"=>"9",
    "i"=>"z",
    "j"=>" ",
    "k"=>"3",
    "l"=>"J",
    "m"=>"5",
    "n"=>".",
    "o"=>"g",
    "p"=>"r",
    "q"=>"@",
    "r"=>"!",
    "s"=>"_",
    "t"=>"U",
    "u"=>"2",
    "v"=>"7",
    "w"=>"6",
    "x"=>"L",
    "y"=>"(",
    "z"=>"j",
    //nums
    "0"=>",",
    "1"=>"{",
    "2"=>"0",
    "3"=>"^",
    "4"=>"W",
    "5"=>"A",
    "6"=>"%",
    "7"=>"#",
    "8"=>"t",
    "9"=>"y",
    //extras
    " "=>"D",
    "-"=>"}",
    "="=>"]",
    "!"=>"*",
    "@"=>"4",
    "#"=>"=",
    "$"=>"1",
    "%"=>"-",
    "^"=>"p",
    "&"=>"+",
    "*"=>"x",
    "("=>")",
    ")"=>"&",
    "_"=>"$",
    "+"=>"M",
    "{"=>"O",
    "}"=>"[",
    "["=>"I",
    "]"=>"8",
    "|"=>"Q",
    "."=>"s",
    ","=>"K",
);


$set_6 = array (
    "a"=>"@",
    "b"=>"[",
    "c"=>"U",
    "d"=>"r",
    "e"=>"3",
    "f"=>"Q",
    "g"=>"I",
    "h"=>"6",
    "i"=>"-",
    "j"=>"x",
    "k"=>"O",
    "l"=>"b",
    "m"=>"5",
    "n"=>"t",
    "o"=>"c",
    "p"=>"1",
    "q"=>"!",
    "r"=>"(",
    "s"=>"M",
    "t"=>"z",
    "u"=>",",
    "v"=>"0",
    "w"=>"&",
    "x"=>"=",
    "y"=>"p",
    "z"=>"J",
    //nums
    "0"=>"|",
    "1"=>"s",
    "2"=>"}",
    "3"=>"_",
    "4"=>"*",
    "5"=>"H",
    "6"=>"V",
    "7"=>"9",
    "8"=>"%",
    "9"=>"$",
    //extras
    " "=>"7",
    "-"=>"W",
    "="=>"{",
    "!"=>"]",
    "@"=>"n",
    "#"=>"K",
    "$"=>"4",
    "%"=>")",
    "^"=>"#",
    "&"=>"A",
    "*"=>"f",
    "("=>"y",
    ")"=>"D",
    "_"=>"+",
    "+"=>"8",
    "{"=>"^",
    "}"=>"g",
    "["=>".",
    "]"=>"2",
    "|"=>"j",
    "."=>" ",
    ","=>"L",
);

$set_7 = array (
    "a"=>"f",
    "b"=>"1",
    "c"=>"&",
    "d"=>"@",
    "e"=>",",
    "f"=>"8",
    "g"=>"j",
    "h"=>"W",
    "i"=>"z",
    "j"=>"7",
    "k"=>"V",
    "l"=>" ",
    "m"=>"=",
    "n"=>"g",
    "o"=>"c",
    "p"=>"#",
    "q"=>"I",
    "r"=>"6",
    "s"=>"!",
    "t"=>"t",
    "u"=>"U",
    "v"=>"2",
    "w"=>"M",
    "x"=>"(",
    "y"=>"5",
    "z"=>"Q",
    //nums
    "0"=>"O",
    "1"=>"+",
    "2"=>"}",
    "3"=>"D",
    "4"=>"s",
    "5"=>"A",
    "6"=>"%",
    "7"=>"y",
    "8"=>"{",
    "9"=>"*",
    //extras
    " "=>"9",
    "-"=>"$",
    "="=>"-",
    "!"=>"4",
    "@"=>")",
    "#"=>"n",
    "$"=>".",
    "%"=>"_",
    "^"=>"[",
    "&"=>"J",
    "*"=>"p",
    "("=>"r",
    ")"=>"|",
    "_"=>"K",
    "+"=>"^",
    "{"=>"3",
    "}"=>"H",
    "["=>"x",
    "]"=>"]",
    "|"=>"b",
    "."=>"L",
    ","=>"0",
);

$set_8 = array (
    "a"=>"",
    "b"=>"",
    "c"=>"",
    "d"=>"",
    "e"=>"",
    "f"=>"",
    "g"=>"",
    "h"=>"",
    "i"=>"",
    "j"=>"",
    "k"=>"",
    "l"=>"",
    "m"=>"",
    "n"=>"",
    "o"=>"",
    "p"=>"",
    "q"=>"",
    "r"=>"",
    "s"=>"",
    "t"=>"",
    "u"=>"",
    "v"=>"",
    "w"=>"",
    "x"=>"",
    "y"=>"",
    "z"=>"",
    //nums
    "0"=>"",
    "1"=>"",
    "2"=>"",
    "3"=>"",
    "4"=>"",
    "5"=>"",
    "6"=>"",
    "7"=>"",
    "8"=>"",
    "9"=>"",
    //extras
    " "=>"",
    "-"=>"",
    "="=>"",
    "!"=>"",
    "@"=>"",
    "#"=>"",
    "$"=>"",
    "%"=>"",
    "^"=>"",
    "&"=>"",
    "*"=>"",
    "("=>"",
    ")"=>"",
    "_"=>"",
    "+"=>"",
    "{"=>"",
    "}"=>"",
    "["=>"",
    "]"=>"",
    "|"=>"",
    "."=>"",
    ","=>"",
);

$set_9 = array (
    "a"=>"",
    "b"=>"",
    "c"=>"",
    "d"=>"",
    "e"=>"",
    "f"=>"",
    "g"=>"",
    "h"=>"",
    "i"=>"",
    "j"=>"",
    "k"=>"",
    "l"=>"",
    "m"=>"",
    "n"=>"",
    "o"=>"",
    "p"=>"",
    "q"=>"",
    "r"=>"",
    "s"=>"",
    "t"=>"",
    "u"=>"",
    "v"=>"",
    "w"=>"",
    "x"=>"",
    "y"=>"",
    "z"=>"",
    //nums
    "0"=>"",
    "1"=>"",
    "2"=>"",
    "3"=>"",
    "4"=>"",
    "5"=>"",
    "6"=>"",
    "7"=>"",
    "8"=>"",
    "9"=>"",
    //extras
    " "=>"",
    "-"=>"",
    "="=>"",
    "!"=>"",
    "@"=>"",
    "#"=>"",
    "$"=>"",
    "%"=>"",
    "^"=>"",
    "&"=>"",
    "*"=>"",
    "("=>"",
    ")"=>"",
    "_"=>"",
    "+"=>"",
    "{"=>"",
    "}"=>"",
    "["=>"",
    "]"=>"",
    "|"=>"",
    "."=>"",
    ","=>"",
);




//checkForDups();
//echo encrypt($_GET['obj']);
//createRandomArray();

function createRandomArray() {
    $arr = array();
    
    $chos = $GLOBALS['set_6'];
    
    $num = count($chos);
    
    
     for ($n = 0; $n < $num; $n++) {
        $obj = $chos[array_rand($chos, 1)];
        $k = array_search($obj, $chos);
         $arr[] = $obj;
        unset($chos[$k]);
}
    
    
    print_r($arr);
}
    
    
function checkForDups() {
    
    foreach ($GLOBALS['set_7'] as $key => $value) {
 $val = $value;
        foreach($GLOBALS['set_7'] as $keyy => $valuee) {
            if (strcmp($value, $valuee) == 0 && strcmp($key, $keyy) != 0) {
                echo 'duplicate : '.$keyy.' -> '.$valuee."    --------- ";
            }
        }
}
    
}



function encrypt($str) {
    
    // index 0-1  ->  index in the full string that contains the encryption set.
    // index 2-3  ->   index of the start of the actual password.
    // (enc set index) + 1  -> the length of the encrypted atrign.
    
    
    $max_index = (strlen($str))-1;  // maximum index in string.
    $total = $max_index+24;           //maximum index after adding 24.
    $twentyfour_slice = rand (6,18);   // slice 24 addition into two.
    $twentyfour_otherside = 24-$twentyfour_slice;
    $end_of_str_index = $twentyfour_slice + $max_index; //index of first half and actual string.
    
    $index_of_enc_set = rand (($end_of_str_index+1),($total-3));
    $index_of_pass_length = $index_of_enc_set+1;
    $chosen_enc_set = rand(1,7);
    
   /* echo 'twenty four slice : '.$twentyfour_slice.'<br>';
    echo 'length of string : '.strlen($str).'<br>';
    echo 'end of string + slice : '.$end_of_str_index.'<br>';
    */
    $array_enc_set = $GLOBALS['set_'.$chosen_enc_set];
    
    $str_as_array = str_split($str);
    $encr_str_array = array();
    $encr_str = '';
    
    for ($i = 0; $i < count($str_as_array); $i++) {
        $char = $str_as_array[$i];
        $enc_char = $array_enc_set[$char];
        $encr_str_array[] = $enc_char;
    }
    
    for ($i = 0; $i < count($encr_str_array); $i++) {
        $encr_str = $encr_str.$encr_str_array[$i];
    }
    
    $pre_string = '';
    $post_string = '';
    
    for ($i = 0; $i < $twentyfour_slice; $i++) {
        
        if ($i == 0) {
            
            if ($index_of_enc_set > 9) {
                $pre_string = $pre_string.$index_of_enc_set;
            } else {
                $pre_string = $pre_string.'0'.$index_of_enc_set;
            }
            
        } else if ($i == 2) {
            
            if ($twentyfour_slice > 9) {
                $pre_string = $pre_string.($twentyfour_slice);
            } else {
                $pre_string = $pre_string.'0'.($twentyfour_slice);
            }
            
              
        } else if ($i == 1 || $i == 3) {
           
        } else {
            //$random_keyy = array_rand($array_enc_set,1);
            $pre_string = $pre_string.$array_enc_set[array_rand($array_enc_set, 1)];
        }
        
        
    }
    
    
    
    
    //echo 'start of rest of string : '.$end_of_str_index.'<br><br>';
    
    
    for ($i = 1; $i <= $twentyfour_otherside; $i++) {
        
        $curr = $i + $end_of_str_index;
        //echo 'now at ; '.$curr.'<br>';
        
        if ($curr == $index_of_enc_set) {
            $post_string = $post_string.$chosen_enc_set;
           // echo 'inserted at index : '.$curr.'<br><br>';
            
        }/* else {
            
            $post_string = $post_string.'+';
        }
        /*
        if ($i == $index_of_enc_set) {
            $post_string = $post_string.$chosen_enc_set;
            
        } */else if ($curr == $index_of_pass_length) {
            
            if (strlen($str) <= 9) {
                $post_string = $post_string.'0'.strlen($str);
            } else {
                $post_string = $post_string.strlen($str);
            }
            
            
        }else if ($i > ($index_of_pass_length+1) || $i <  $index_of_enc_set) {
        
           // $random_key = array_rand($array_enc_set,1);
            $post_string = $post_string.$array_enc_set[array_rand($array_enc_set, 1)];
        }
        
    }
    
   
    $final = $pre_string.$encr_str.$post_string;
    
   /* echo 'index of set : '.$index_of_enc_set.'<br>';
    
    echo 'last index should be : '.$total.'<br>';
    
    echo 'THE SUB STRING FROM INDEX : '.substr($final, $index_of_enc_set, 3).'<BR>';
    */
    //echo $final;
    
   // echo '<br><br><br><br><br><br>'.$pre_string.' ->'.strlen($pre_string).'<br><br>'.$encr_str.'<br><br>'.$post_string.' ->'.strlen($post_string).'<br><br><br>';
    
    
    $rest = substr($final,4);
    $first_4 = substr($final,0,4);
    $first_4_arr = str_split($first_4);
    $first_4_enc_arr = array();
    $enc_first_4 = '';
    
    for ($n = 0; $n < count($first_4_arr); $n++) {
        $chr = $first_4_arr[$n];
        $enc_chr = $GLOBALS['first_four_set'][$chr];
        $first_4_enc_arr[] = $enc_chr;
    }
    
    for ($n = 0; $n < count($first_4_enc_arr); $n++) {
        $enc_first_4 = $enc_first_4.$first_4_enc_arr[$n];
    }
    
    $final_enc = $enc_first_4.$rest;
    //echo $final_enc;
    return $final_enc;
}



function findDecrypt($set, $char) {
    foreach ($set as $key => $value) {
        if ($value == $char) {
            return $key;
        }
    }
    
    return '~';
}



function decrypt($str) {
    $first_4_enc = substr($str,0,4);    //index of set and index of start of string.
    $f_4_enc_arr = str_split($first_4_enc);
    $first_4 = ''; 
    for ($n = 0; $n < strlen($first_4_enc); $n++) {
        $in_chr = $f_4_enc_arr[$n];
        $first_4 = $first_4.findDecrypt($GLOBALS['first_four_set'], $in_chr);
    }
    
    //echo $first_4.'<br>';
    
    $index_of_set_str = substr($first_4,0,2);
    $index_of_actual_str = substr($first_4,2,2);
    
    $index_of_set = intval($index_of_set_str);
    $index_of_str = intval($index_of_actual_str);
    
    $set_str = substr($str,$index_of_set,1);
    $len_str = substr($str,($index_of_set+1),2);
    
    $set = intval($set_str);
    $len = intval($len_str);
    
    //echo 'SET : '.$set.'<br><br>';
    
    
    $enc_str = substr($str,$index_of_str,$len);
    $enc_str_arr = str_split($enc_str);
    
    $actual = '';
    for ($n = 0; $n < count($enc_str_arr); $n++) {
        $in_chr = $enc_str_arr[$n];
        $actual = $actual.findDecrypt($GLOBALS['set_'.$set], $in_chr);
        //echo $in_chr.' -> '.findDecrypt($GLOBALS['set_'.$set], $in_chr).'<br>';
    }
    
    return $actual;
    
}








?>