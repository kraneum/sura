<?php

function getArrowIcon($user_id) {
    $con = makeConnection();

    $trans = runSimpleFetchQuery($con, ['type_id'], "transactions", ["to_who"], ["="], [$user_id], "", "id DESC", "1")['result'];
    $data = null;

    if ($trans == 1) {
        $data = 'credit';
    } else if ($trans == 2) {
        $data = 'debit';
    }

    if (isset($_GET['mobile'])) {
        if (isset($_GET['pre'])) {
            echo '<pre>' . json_encode($data, JSON_PRETTY_PRINT) . '</pre>';
        } else {
            echo json_encode($data);
        }
    } else {
        return $data;
    }

    disconnectConnection($con);
}

function getInsuranceTypeIdFromMeta($meta) {
    $con = makeConnection();

    $trans = runSimpleFetchQuery($con, ['id'], "insurance_types", ["meta"], ["="], ["'$meta'"], "", "", "1")['result'];

    if (isset($_GET['mobile'])) {
        if (isset($_GET['pre'])) {
            echo '<pre>' . json_encode($trans, JSON_PRETTY_PRINT) . '</pre>';
        } else {
            echo json_encode($trans);
        }
    } else {
        return $trans;
    }

    disconnectConnection($con);
}
