<?php

require_once('sura_config.php');
require_once('sura_functions.php');
require_once('encryption/encchar.php');
require_once('hybrid.php');

if (isset($_GET['command'])) {
    if (strcmp($_GET['command'], "1") == 0) {
        createInsurer($_GET['packet']);
    }

    if (strcmp($_GET['command'], "2") == 0) {
        getMyPolicies($_GET['user_id']);
    }
    
    if (strcmp($_GET['command'], "3") == 0) {
        getMyRedemptions($_GET['user_id']);
    }

    if (strcmp($_GET['command'], "4") == 0) {
        getCountriesAndProvinces();
    }

    if (strcmp($_GET['command'], "5") == 0) {
        getPaymentStructures();
    }

    if (strcmp($_GET['command'], "6") == 0) {
        redeemCode($_GET['code'], $_GET['user_id']);
    }

    if (strcmp($_GET['command'], "7") == 0) {
        getInsuranceTypes();
    }

    if (strcmp($_GET['command'], "8") == 0) {
        getPoliciesByMeta(json_decode($_GET['keys']), json_decode($_GET['comps']), json_decode($_GET['vals']));
    }

    if (strcmp($_GET['command'], "9") == 0) {
        setPolicyTerms($_GET['url'], $_GET['policy_id']);
    }

    if (strcmp($_GET['command'], "10") == 0) {
        setPolicyImage($_GET['url'], $_GET['policy_id']);
    }

    if (strcmp($_GET['command'], "11") == 0) {
        createUser(json_decode(stripslashes($_GET['packet']), true));
    }

    if (strcmp($_GET['command'], "12") == 0) {
        createMerchant(json_decode(stripslashes($_GET['packet']), true));
    }

    if (strcmp($_GET['command'], "13") == 0) {
        createInsurancePolicy(json_decode(stripslashes($_GET['packet']), true));
    }

    if (strcmp($_GET['command'], "14") == 0) {
        createUserSignUp($_GET['user_id'], $_GET['policy_id']);
    }

    if (strcmp($_GET['command'], "15") == 0) {
        createUSSDCodes($_GET['purchase_id'], json_decode(stripslashes($_GET['points_worth']), true), json_decode(stripslashes($_GET['quantities']), true)
        );
    }

    if (strcmp($_GET['command'], "16") == 0) {
        buyPointsMerchant($_GET['merchant_id'], $_GET['point_worth']);
    }
    
    if (strcmp($_GET['command'], "17") == 0) {
        getInsurancePolicies($_GET['type_id']);
    }
    
    if (strcmp($_GET['command'], "18") == 0) {
        getInsuranceTypeIdFromMeta('auto');
    }
    
    if (strcmp($_GET['command'], "19") == 0) {
        getSecurityQuestions();
    }
    
}


function getInsurancePolicies($id) {
    $con = makeConnection();

    $ins_policies = runSimpleFetchQuery($con, ['*'], "policies", ['type_id'], ['='], [$id], "", "", "")['result'];
    
    for ($i = 0; $i < count($ins_policies); $i++) {

        $this_policy = $ins_policies[$i];
        $insurer_id = $this_policy['insurer_id'];

        $insurer = runSimpleFetchQuery($con, ['*'], "insurer", ["id"], ["="], [$insurer_id], "", "", "")['result'];
        $ins_policies[$i]['insurer'] = $insurer;
    }

    if (isset($_GET['pre'])) {
        echo '<pre>' . json_encode($ins_policies, JSON_PRETTY_PRINT) . '</pre>';
    } else {
        echo json_encode($ins_policies);
    }

    disconnectConnection($con);
}

function buyPointsMerchant($merchant_id, $point_worth) {

    $con = makeConnection();

    $insert_ = runSimpleInsertQuery(
            $con, "point_purchases", ['`merchant_id`', '`point_amount`'], [$merchant_id, $point_worth]
    )['result'];

    if ($insert_) {

        $update = runSimpleUpdateQuery(
                $con, "merchants", ['point_balance'], ['point_balance+' . $point_worth], ['id'], ['='], [$merchant_id])['result'];

        if ($update) {
            echo 1;
        } else {
            echo -1;
        }
    } else {
        echo 0;
    }

    disconnectConnection($con);
}

function createUSSDCodes($purchase_id, $point_worths, $quantities) {

    $con = makeConnection();
    $total_added = 0;

    for ($n = 0; $n < count($point_worths); $n++) {

        $p_worth = intval($point_worths[$n]);
        $quant = intval($quantities[$n]);

        for ($t = 0; $t < $quant; $t++) {

            if (insertUSSDCode($con, $p_worth, $purchase_id)['result']) {
                $total_added++;
            }
        }
    }

    echo 'total successfully added : ' . $total_added;

    disconnectConnection($con);
}

function insertUSSDCode($con, $p_worth, $purchase_id) {

    $insert_ = uniqueUSSDCodeInsert($con, 'ussd_codes', $p_worth, $purchase_id)['result'];

    if ($insert_) {
        return true;
    } else {
        return false;
    }
}

function getMyPolicies($user_id) {
    $con = makeConnection();

    $trans = runSimpleFetchQuery($con, ['*'], "user_signups", ["user_id"], ["="], [$user_id], "", "", "")['result'];

    for ($i = 0; $i < count($trans); $i++) {

        $this_signup = $trans[$i];
        $policy_id = $this_signup['policy_id'];
        
        $policy = runSimpleFetchQuery($con, ['*'], "policies", ["id"], ["="], [$policy_id], "", "", "")['result'];
        $trans[$i]['policy_info'] = $policy;

        $insurer = runSimpleFetchQuery($con, ['*'], "insurer", ["id"], ["="], [$policy[0]['insurer_id']], "", "", "")['result'];
        $trans[$i]['insurer'] = $insurer;

        $payments = runSimpleFetchQuery($con, ['*'], "payment_schedule", ["signup_id"], ["="], [$this_signup['id']], "", "", "")['result'];
        $trans[$i]['payment_schedule'] = $payments;
    }

    if (isset($_GET['pre'])) {
        echo '<pre>' . json_encode($trans, JSON_PRETTY_PRINT) . '</pre>';
    } else {
        echo json_encode($trans, JSON_PRETTY_PRINT);
    }

    disconnectConnection($con);
}

function getMyRedemptions($user_id) {
    $con = makeConnection();

    $trans = runSimpleFetchQuery($con, ['*'], "ussd_codes", ["redeemed_by"], ["="], [$user_id], "", "", "")['result'];

    if (isset($_GET['pre'])) {
        echo '<pre>' . json_encode($trans, JSON_PRETTY_PRINT) . '</pre>';
    } else {
        echo json_encode($trans, JSON_PRETTY_PRINT);
    }

    disconnectConnection($con);
}

function getCountriesAndProvinces() {
    $con = makeConnection();

    $transs = runSimpleFetchQuery($con, ['*'], "countries", [], [], [], "", "", "")['result'];

    for ($i = 0; $i < count($transs); $i++) {

        $this_country = $transs[$i];
        $country_id = $this_country['id'];
        $prv = runSimpleFetchQuery($con, ['*'], "provinces", ["country_id"], ["="], [$country_id], "", "", "")['result'];

        $transs[$i]['provinces'] = $prv;
    }

    if (isset($_GET['pre'])) {
        echo '<pre>' . json_encode($transs, JSON_PRETTY_PRINT) . '</pre>';
    } else {
        echo json_encode($transs, JSON_PRETTY_PRINT);
    }

    disconnectConnection($con);
}

function getPaymentStructures() {
    $con = makeConnection();

    $trans = runSimpleFetchQuery($con, ['*'], "payment_structures", [], [], [], "", "", "")['result'];

    if (isset($_GET['pre'])) {
        echo '<pre>' . json_encode($trans, JSON_PRETTY_PRINT) . '</pre>';
    } else {
        echo json_encode($trans, JSON_PRETTY_PRINT);
    }

    disconnectConnection($con);
}

function getInsuranceTypes() {
    $con = makeConnection();

    $ins_types = runSimpleFetchQuery($con, ['*'], "insurance_types", [], [], [], "", "", "")['result'];

    if (isset($_GET['pre'])) {
        echo '<pre>' . json_encode($ins_types, JSON_PRETTY_PRINT) . '</pre>';
    } else {
        echo json_encode($ins_types, JSON_PRETTY_PRINT);
    }

    disconnectConnection($con);
}

function redeemCode($code, $user_id) {
    $con = makeConnection();

    $code_row = runSimpleFetchQuery($con, ['*'], "ussd_codes", ["code"], ["="], ["'" . $code . "'"], "", "", "")['result'];
    if (count($code_row) > 0) {

        $this_code = $code_row[0];
        $redeemed = $this_code['redeemed'];
        if ($redeemed == 0) {


            $runUpdate = runSimpleUpdateQuery(
                    $con, "ussd_codes", ['redeemed', 'redeemed_date', 'redeemed_by'], ['1', 'CURRENT_TIMESTAMP', $user_id], ['code'], ['='], ["'" . $code . "'"])['result'];
        } else {
            echo 'code used';
        }
    } else {
        echo 'invalid code';
    }

    disconnectConnection($con);
}

function merchantsBuyPoints() {
    
}

function createUserSignUp($user_id, $policy_id) {
    $con = makeConnection();

    $insert_ = runSimpleInsertQuery(
            $con, "user_signups", ['`user_id`', '`policy_id`'], [$user_id, $policy_id]
    )['result'];

    return $insert_;

    disconnectConnection($con);
}

function getPoliciesByMeta($keys, $comp, $values) {

    $con = makeConnection();
    $output = array();
    $transactions = array();

    $comparators_in = array();

    $comparators = ['<', '<=', '=', '>=', '>'];

    for ($i = 0; $i < count($comp); $i++) {
        $index = $comp[$i];
        $chosen_comp = $comparators[$index];
        $comparators_in[] = $chosen_comp;
    }



    $policies = runSimpleFetchQuery($con, ['*'], "policies", $keys, $comparators_in, $values, "", "", "")['result'];


    if (isset($_GET['pre'])) {
        echo '<pre>' . json_encode($policies, JSON_PRETTY_PRINT) . '</pre>';
    } else {
        echo json_encode($policies, JSON_PRETTY_PRINT);
    }

    disconnectConnection($con);
}

function createUser($packet) {
    $con = makeConnection();
    //$packet_ = json_decode(stripslashes($packet), true);

    $insert_ = runSimpleInsertQueryWithIDReturn(
            $con, "users", [
        '`f_name`',
        '`m_name`',
        '`l_name`',
        '`dob`',
        '`bvn`',
        '`phone`',
        '`email`',
        '`login_username`',
        '`login_password`',
        '`date_joined`'
            ], [
        "'" . $packet['f_name'] . "'",
        "'" . $packet['m_name'] . "'",
        "'" . $packet['l_name'] . "'",
        "'" . $packet['dob'] . "'",
        "'" . $packet['bvn'] . "'",
        "'" . $packet['phone'] . "'",
        "'" . $packet['email'] . "'",
        "'" . $packet['login_username'] . "'",
        "'" . $packet['login_password'] . "'",
        'CURRENT_TIMESTAMP'
    ])['result'];

    echo $insert_;

    disconnectConnection($con);
}

function createMerchant($packet) {
    $con = makeConnection();
    //$packet_ = json_decode(stripslashes($packet), true);

    $insert_ = runSimpleInsertQueryWithIDReturn(
            $con, "merchants", [
        '`name`',
        '`write_up`',
        '`contact_phone`',
        '`contact_email`',
        '`rc_number`',
        '`login_username`',
        '`login_password`',
        '`operating_country`',
        '`operating_province`',
        '`date_joined`'
            ], [
        "'" . $packet['name'] . "'",
        "'" . $packet['write_up'] . "'",
        "'" . $packet['contact_phone'] . "'",
        "'" . $packet['contact_email'] . "'",
        "'" . $packet['rc_number'] . "'",
        "'" . $packet['login_username'] . "'",
        "'" . $packet['login_password'] . "'",
        "'" . $packet['operating_country'] . "'",
        "'" . $packet['operating_province'] . "'",
        'CURRENT_TIMESTAMP'
    ])['result'];

    echo $insert_;

    disconnectConnection($con);
}

function setPolicyTerms($url, $policy_id) {

    $con = makeConnection();

    $termUpdate = runSimpleUpdateQuery($con, "policies", ['terms_url'], ["'" . $url . "'"], ['id'], ['='], [$policy_id])['result'];

    if ($termUpdate) {
        echo 'SUCCESS';
    } else {
        echo 'FAILURE';
    }

    disconnectConnection($con);
}

function setPolicyImage($url, $policy_id) {

    $con = makeConnection();

    $imgUpdate = runSimpleUpdateQuery($con, "policies", ['image_url'], ["'" . $url . "'"], ['id'], ['='], [$policy_id])['result'];

    if ($imgUpdate) {
        echo 'SUCCESS';
    } else {
        echo 'FAILURE';
    }

    disconnectConnection($con);
}

function createInsurer($packet) {
    $con = makeConnection();
    $packet = json_decode(stripslashes($packet), true);
    print_r($packet);
    $insert_ = runSimpleInsertQueryWithIDReturn(
            $con, "insurer", [
        'name',
        'write_up',
        'contact_phone',
        'contact_email',
        'rc_number',
        'login_username',
        'login_password',
        'operating_country',
        'operating_province',
        'date_joined'
            ], [
        "'" . $packet['name'] . "'",
        "'" . $packet['write_up'] . "'",
        "'" . $packet['contact_phone'] . "'",
        "'" . $packet['contact_email'] . "'",
        "'" . $packet['rc_number'] . "'",
        "'" . $packet['login_username'] . "'",
        "'" . $packet['login_password'] . "'",
        "'" . $packet['operating_country'] . "'",
        "'" . $packet['operating_province'] . "'",
        'CURRENT_TIMESTAMP'
    ])['result'];

    echo $insert_;

    disconnectConnection($con);
}

function createInsurancePolicy($packet) {
    $con = makeConnection();
    //$packet_ = json_decode(stripslashes($packet), true);

    $insert_ = runSimpleInsertQueryWithIDReturn(
            $con, "policies", [
        '`insurer_id`',
        '`type_id`',
        '`title`',
        '`description`',
        '`price`',
        '`currency_id`',
        '`payment_schedule_id`',
        '`terms_url`',
        '`image_url`',
        'tenure'
            ], [
        "'" . $packet['insurer_id'] . "'",
        "'" . $packet['type_id'] . "'",
        "'" . $packet['title'] . "'",
        "'" . $packet['description'] . "'",
        "'" . $packet['price'] . "'",
        "'" . $packet['currency_id'] . "'",
        "'" . $packet['payment_schedule_id'] . "'",
        "'" . $packet['terms_url'] . "'",
        "'" . $packet['image_url'] . "'",
        "'" . $packet['tenure'] . "'"
    ])['result'];

    echo $insert_;

    disconnectConnection($con);
}

function getSecurityQuestions() {
    $con = makeConnection();

    $trans = runSimpleFetchQuery($con, ['*'], "security_questions", [], [], [], "", "", "")['result'];

    if (isset($_GET['pre'])) {
        echo '<pre>' . json_encode($trans, JSON_PRETTY_PRINT) . '</pre>';
    } else {
        echo json_encode($trans, JSON_PRETTY_PRINT);
    }

    disconnectConnection($con);
}

?>