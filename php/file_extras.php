<?php

function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i = 0; $i < $file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}

function newName($path, $filename) {
    $res = "$path/$filename";
    if (!file_exists($res))
        return $res;
    $fnameNoExt = pathinfo($filename, PATHINFO_FILENAME);
    $ext = pathinfo($filename, PATHINFO_EXTENSION);

    $i = 1;
    while (file_exists("$path/$fnameNoExt ($i).$ext"))
        $i++;
    return "$path/$fnameNoExt ($i).$ext";
}