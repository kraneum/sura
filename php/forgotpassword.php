<?php
$entity_type = filter_input(INPUT_GET, 'entity', FILTER_SANITIZE_STRING);

if (empty($entity_type)) {
    exit('No entity specified');
}
?>

<!DOCTYPE html>

<html>
    <head>
        <title>iInsurance</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="icon" type="image/png" href="../img/favicon.ico">
        <link rel="apple-touch-icon" href="../img/favicon.ico">

        <link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
        <link href="../css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">
            body {
                padding: 0 25%;
            }
            form {
                margin: 0 20%;
            }
            #info {
                margin: 10px 10%;
            }
        </style>
    </head>
    <body>

        <header><img class="img-responsive center-block" src="../img/logos/uic_full.png" alt=""/></header>
        <div id="info"></div>
        <div id="content">
            <form id ="forgotPasswordForm" class="form-horizontal" method="post">
                <div class="form-group">

                    <input type="hidden" name ="entity" value="<?php echo $entity_type ?>"/>
                    <label for="email">Enter Email Address * </label>
                    <input class="form-control" type="email" required="" id="email" name="email" placeholder="Enter Email" data-parsley-required data-parsley-type="email" data-parsley-length-message="This is not a valid email"/>
                </div>

                    <div class="form-group">

                        <button id="formVerify" type="submit" class="btn btn-success" style="border-radius: 50px;" name="btn-verify"><i class="fa fa-check btn-icon-left"></i> VERIFY</button>
                </div>
            </form>
        </div>

        <script src="../js/jquery-3.1.1.js" type="text/javascript"></script>
        <script src="../js/parsley.min.js" type="text/javascript"></script>
        <script src="../js/bootstrap.js" type="text/javascript"></script>

        <script type="text/javascript">
            $('#forgotPasswordForm').parsley();

            $('#forgotPasswordForm').submit(function (e) {
                $('#formVerify').html('<i class="fa fa-spinner fa-pulse fa-fw"></i> VERIFYING ...');
                $('#info').empty();
                
                var formData = {
                    'entity': $('input[name=entity]').val(),
                    'email': $('input[name=email]').val()
                };
                $.post('./suracommands.php?command=verify_email_2', formData, function (data) {
                    data = JSON.parse(data);

                    $('#formVerify').html('<i class="fa fa-check btn-icon-left"></i> VERIFY');
                    if (data['status']) {
                        $('#content').empty().load('forgotpassword_sq.php', {"entity":data['entity'], "email": data['email'], "sec_que": data['sec_que']});
                    } else {
                        if (data['err'] === 'INVALID_EMAIL') {
                            $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                                    '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                    '<strong>Verification Failure</strong></div>');
                            $('#otp').addClass('parsley-error');
                        } else if (data['err'] === 'EMAIL_NOT_RESOLVED') {
                            $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                                    '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                    '<strong>Email does not exist</strong></div>');
//                            $('#content').fadeOut('slow');

                        }
                    }

                });

                e.preventDefault();

            });
        </script>
    </body>
</html>
