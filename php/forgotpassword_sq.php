<?php
$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

$entity = $_POST['entity'];
$email = $_POST['email'];
$sec_que = $_POST['sec_que'];
?>


<form id ="securityForm" method="post">


    <input type="hidden" name ="entity" value="<?= $entity ?>"/>
    <input type="hidden" name ="email" value="<?= $email ?>"/>
    <div class="form-group">
        <label for="sec_que">Security Question </label>
        <input class="form-control" type="text" value="<?= $sec_que ?>" readonly/>
    </div>
    <div class="form-group">
        <label for="sec_que">Enter Security Answer * </label>
        <input class="form-control" type="text" required id="sec_ans" name="sec_ans" placeholder="Enter Security Answer"/>
    </div>

    <div class="form-group">

        <button id="formVerify" type="submit" class="btn btn-success" style="border-radius: 50px;" name="btn-verify"><i class="fa fa-check btn-icon-left"></i> VERIFY</button>
    </div>
</form>


<script type="text/javascript">
    $(document).ready(function () {
        $('#securityForm').parsley();

        $('#securityForm').submit(function (e) {
            $('#formVerify').html('<i class="fa fa-spinner fa-pulse fa-fw"></i> VERIFYING ...');
            $('#info').empty();

            var formData = {
                'entity': $('input[name=entity]').val(),
                'email': $('input[name=email]').val(),
                'answer': $('input[name=sec_ans]').val()
            };
            $.post('./suracommands.php?command=verify_security_answer', formData, function (data) {
                data = JSON.parse(data);

                $('#formVerify').html('<i class="fa fa-check btn-icon-left"></i> VERIFY');
                if (data['status']) {
                    $('#content').empty().html($('<h3 class="text-center">Please check your mail for the reset password link. Thanks</h3>'));
//                alert('All Good');
                } else {
                    if (data['err'] === 'INVALID_EMAIL') {
                        $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                '<strong>Verification Failure</strong></div>');
                        $('#otp').addClass('parsley-error');
                    } else if (data['err'] === 'INCORRECT_ANSWER') {
                        $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                '<strong>The answer provided was incorrect</strong></div>');
//                            $('#content').fadeOut('slow');

                    }
                }

            });

            e.preventDefault();

        });
    });
</script>
