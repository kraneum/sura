<?php
$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

$entity = $_POST['entity'];
$phone = $_POST['phone'];
$sec_que = $_POST['sec_que'];
?>


<form id ="securityForm" method="post">


    <input type="hidden" name ="entity" value="<?= $entity ?>"/>
    <input type="hidden" name ="phone" value="<?= $phone ?>"/>
    <div class="form-group">
        <label for="sec_que">Security Question </label>
        <input class="form-control" type="text" value="<?= $sec_que ?>" readonly/>
    </div>
    <div class="form-group">
        <label for="sec_que">Enter Security Answer * </label>
        <input class="form-control" type="text" required id="sec_ans" name="sec_ans" placeholder="Enter Security Answer"/>
    </div>

    <div class="form-group">

        <button id="formVerify" type="submit" class="btn btn-success" style="border-radius: 50px;" name="btn-verify"><i class="fa fa-check btn-icon-left"></i> VERIFY</button>
    </div>
</form>


<script type="text/javascript">
    $(document).ready(function () {
        $('#securityForm').parsley();

        $('#securityForm').submit(function (e) {
            $('#formVerify').html('<i class="fa fa-spinner fa-pulse fa-fw"></i> VERIFYING ...');
            $('#info').empty();

            var formData = {
                'entity': $('input[name=entity]').val(),
                'phone': $('input[name=phone]').val(),
                'answer': $('input[name=sec_ans]').val()
            };
            $.post('./suracommands.php?command=verify_security_answer_2', formData, function (data) {
                data = JSON.parse(data);

                $('#formVerify').html('<i class="fa fa-check btn-icon-left"></i> VERIFY');
                if (data['status']) {
                    $('#content').empty().load('veripge.php', {"entity": data['entity'], "id": data['id']});
                } else {
                    if (data['err'] === 'INCORRECT_ANSWER') {
                        $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                '<strong>The answer provided was incorrect</strong></div>');
//                            $('#content').fadeOut('slow');

                    }
                }

            });

            e.preventDefault();

        });
    });
</script>
