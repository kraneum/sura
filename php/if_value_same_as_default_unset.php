<?php

$ifValueSameAsDefaultUnset = function(&$value, $defaultValue) {
    if ($value === $defaultValue) {
        $value = NULL;
    }
};
