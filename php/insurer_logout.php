<?php

require_once 'sura_config.php';
require_once 'sura_functions.php';

if (!isset($_SESSION)) {
    session_start();
}

if (!isset($_SESSION['insurerid'])) {
    header("Location: ../insurer/login.php");
} else {
    $con = makeConnection();
    $id = $_SESSION['insurerid'];
    $date = (new DateTime())->format(DateTime::ISO8601);
    
    runSimpleUpdateQuery($con, 'insurer', ['last_login'], ["'$date'"], ['id'], ['='], [$id]);
    session_unset();
    session_destroy();
    header("Location: ../insurer/login.php");
    exit;
}
