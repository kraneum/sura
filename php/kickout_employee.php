<?php

require_once 'sura_config.php';
require_once 'sura_functions.php';
require_once 'user_permissions.php';

$kickoutEmployee = function($con, $id, $company_id) {
    $fetchSessionidQuery = runSimpleFetchQuery($con, ['session_id', 'user_permissions', 'company_id'], 'employees', ['id'], ['='], [$id], '', '', 1);

    if (!$fetchSessionidQuery['err']['code']) {
        if (!UserPermissions::isSuperuserPermission(+$fetchSessionidQuery['result'][0]['user_permissions'])) {
            if (+$fetchSessionidQuery['result'][0]['company_id'] === +$company_id) {
                $sessionId = $fetchSessionidQuery['result'][0]['session_id'];

                if ($sessionId) {
                    session_id($sessionId);
                    //session_start();
                    session_destroy();

                    //set the session to null
                    runSimpleUpdateQuery($con, 'employees', ['session_id'], [null], ['id'], ['='], [$id], 1);

                    return ['status' => 'SUCCESS'];
                } else {
                    return ['status' => 'SESSIONNOTFOUND'];
                }
            } else {
                return ['status' => 'CANNOTKICKOUTANOTHERCOMPANYEMPLOYEE'];
            }
        } else {
            return ['status' => 'INSUFFICIENTPRIVILEDGESTOKICKOUT'];
        }
    } else {
        return ['status' => 'DBERROR'];
    }
};
