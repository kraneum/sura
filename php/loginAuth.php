<?php

session_start();
require_once 'sura_config.php';
require_once 'sura_functions.php';


// This will never let you open login page if session is set
if (isset($_SESSION['userid']) != "") {
    header("Location: home.php");
    exit;
}

$error = false;

if (isset($_POST['btn-login'])) {

// prevent sql injections/ clear user invalid inputs
    $username = htmlspecialchars(strip_tags(trim($_POST['username'])));
    $password = htmlspecialchars(strip_tags(trim($_POST['password'])));
// prevent sql injections / clear user invalid inputs

    if (empty($username)) {
        $error = true;
        $usernameError = "Please enter your email address.";
    }
    if (empty($password)) {
        $error = true;
        $passwordError = "Please enter your password.";
    }

// if there's no error, continue to login
    if (!$error) {
        $con = makeConnection();

        $queryResult = runSimpleFetchQuery($con, ['id', 'username', 'password'], 'users', ['username', 'password'], ['=', '='], ["'$username'", "'$password'"], '', '', 1)['result'];
        if (empty($queryResult)) {
//echo 'damn';
            $queryResult = runSimpleFetchQuery($con, ['id', 'email', 'password'], 'users', ['email', 'password'], ['=', '='], ["'$username'", "'$password'"], '', '', 1)['result'];
            if (empty($queryResult)) {
                $errMSG = "Incorrect Credentials, Try again...";
            } else {
                runSimpleUpdateQuery($con, 'users', ['last_login'], ['CURRENT_TIMESTAMP'], ['email'], ['='], ["'$username'"]);
                $_SESSION['user'] = $queryResult[0]['username'];
                $_SESSION['userid'] = $queryResult[0]['id'];

                header("Location: home.php");
            }
        } else {
            runSimpleUpdateQuery($con, 'users', ['last_login'], ['CURRENT_TIMESTAMP'], ['username'], ['='], ["'$username'"]);
            $_SESSION['userid'] = $queryResult[0]['id'];

            header("Location: home.php");
        }

        disconnectConnection($con);
    }
}

    
    