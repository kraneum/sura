<?php

require_once 'sura_config.php';
require_once 'sura_functions.php';

if (!isset($_SESSION)) {
    session_start();
}

if (!isset($_SESSION['id'])) {
    header("Location: ../login.php");
} else {
    $con = makeConnection();
    $id = $_SESSION['id'];
    $date = (new DateTime())->format(DateTime::ISO8601);
    
    runSimpleUpdateQuery($con, 'users', ['last_login'], ["'$date'"], ['id'], ['='], [$id]);
    session_unset();
    session_destroy();
    header("Location: ../login.php");
    exit;
}
