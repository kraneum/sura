<?php

require_once('sql_functions.php');

$mailchimp_api_key = "86ae2472ea2eb4e1a1cf02d081fd361e-us9";
// $female_list_id = 'c792580b48';
// $male_list_id = 'c41812f7dc';


// $nnamdi = array (
// 	"email_address" => "tester3r@test.com",
// 	"status" => "subscribed",
// 	"merge_fields" => array(
// 		'FNAME' => 'Nnamdi',
// 		'LNAME' => 'Okeke'
// 		)
// 	);

// $chiwete = array (
// 	"email_address" => "testerrchi2@test.com",
// 	"status" => "subscribed",
// 	"merge_fields" => array(
// 		'FNAME' => 'Chiwete',
// 		'LNAME' => 'Njokanma'
// 		)
// 	);

// $ocheme = array (
// 	"email_address" => "testerroch1@test.com",
// 	"status" => "subscribed",
// 	"merge_fields" => array(
// 		'FNAME' => 'Ocheme',
// 		'LNAME' => 'Saleh'
// 		)
// 	);

// // $json_data_1 = json_encode($nnamdi);
// // $json_data_2 = json_encode($chiwete);
// // $json_data_3 = json_encode($ocheme);

// $list_of_users = [$nnamdi, $chiwete, $ocheme];

// signUpUser($list_of_users, $male_list_id);



if (isset($_GET['command'])) {

	$output = '';

	if(strcmp($_GET['command'], '1') == 0) {

		$output = getAllUsers();

	}

	if(strcmp($_GET['command'], '2') == 0) {

		$output = getAllMaleUsers();

	}

	if(strcmp($_GET['command'], '3') == 0) {

		$output = getAllFemaleUsers();

	}

	if(strcmp($_GET['command'], '4') == 0) {

		$output = getAllWhoLikeCategory($_GET['cat_id']);

	}

	if(strcmp($_GET['command'], '5') == 0) {

		$output = getAllWhoLikeStore($_GET['store_id']);

	}

	if(isset($_GET['to_mailchimp'])) {
		sendToMailchimp($output);
	}

	echo '<pre>'.json_encode($output, JSON_PRETTY_PRINT).'</pre>';



}




function makeConnection()
{
$DB_HostName = "localhost"; //$host;
$DB_Name = "nnamdi93_wrdp1_test"; //$database;
$DB_User = "nnamdi93_wrdp1"; //$username;
$DB_Pass = "vT9XjZuy3TlEqs"; //$password;


$con = mysqli_connect($DB_HostName,$DB_User,$DB_Pass, $DB_Name); 

return $con;
}


function disconnectConnection($con)
{
	mysqli_close($con);
}


//echo '<pre>'.json_encode(getAllWhoLikeCategory($_GET['cat_id']), JSON_PRETTY_PRINT).'</pre>';




function getAllWhoLikeCategory($cat_id) {
	$con = makeConnection();

	$the_stores = runSimpleFetchQuery($con, ['*'], "wp_term_relationships", ['term_taxonomy_id'], ['='], [$cat_id], "", "", "");

	$list_of_coupons = array();

	for($i = 0; $i < count($the_stores); $i++) {
		$this_store = $the_stores[$i];
		$list_of_coupons[] = $this_store['object_id'];
	}

	$find_in_set = "(";

		for ($n = 0; $n < count($list_of_coupons); $n++) {
			$the_id = $list_of_coupons[$n];
			$find_in_set .= "FIND_IN_SET('".$the_id."', meta_value)";

			if($n == (count($list_of_coupons)-1)) {
				$find_in_set .= ")";
} else {
	$find_in_set .= " OR ";
}
}

$sql = 'SELECT * FROM wp_usermeta WHERE meta_key = "_wpc_saved_coupons" AND '.$find_in_set;
    //echo $sql;
$metas = array();

$output = array();

$execute = mysqli_query($con,$sql) or die(mysqli_error($con));
$res1 = mysqli_num_rows($execute);

$rows = array();
while ($r = mysqli_fetch_array($execute, MYSQLI_ASSOC)) {
	$rows[] = $r;
}
$metas = $rows;
for ($r = 0; $r < count($metas); $r++) {
	$thismeta = $metas[$r];
	$user_id = $thismeta['user_id'];
	$user = runSimpleFetchQuery($con, ['*'], "wp_users", ['ID'], ['='], [$user_id], "", "", "");
	$user_meta = runSimpleFetchQuery($con, ['*'], "wp_usermeta", ['user_id'], ['='], [$user_id], "", "", "");
	for($i = 0; $i < count($user_meta); $i++) {

		$this_meta = $user_meta[$i];
		$meta_key = $this_meta['meta_key'];
		$meta_val = $this_meta['meta_value'];
		if(strcmp($meta_key, 'first_name') == 0) {
			$user[0]['wpu_first_name'] = $meta_val;
		}

		if(strcmp($meta_key, 'last_name') == 0) {
			$user[0]['wpu_last_name'] = $meta_val;
		}

	}
	$output[] = $user[0];
}

disconnectConnection($con);
return $output;
}




function getAllWhoLikeStore($store_id) {
	
	$con = makeConnection();

	$sql = 'SELECT * FROM wp_usermeta WHERE meta_key = "_wpc_favorite_stores" AND FIND_IN_SET("'.$store_id.'", meta_value)';
	$metas = array();

	$output = array();

	$execute = mysqli_query($con,$sql) or die(mysqli_error($con));
	$res1 = mysqli_num_rows($execute);

	$rows = array();
	while ($r = mysqli_fetch_array($execute, MYSQLI_ASSOC)) {
		$rows[] = $r;
	}
	$metas = $rows;

	for ($r = 0; $r < count($metas); $r++) {
		$thismeta = $metas[$r];
		$user_id = $thismeta['user_id'];
		$user = runSimpleFetchQuery($con, ['*'], "wp_users", ['ID'], ['='], [$user_id], "", "", "");
		$user_meta = runSimpleFetchQuery($con, ['*'], "wp_usermeta", ['user_id'], ['='], [$user_id], "", "", "");
		for($i = 0; $i < count($user_meta); $i++) {

			$this_meta = $user_meta[$i];
			$meta_key = $this_meta['meta_key'];
			$meta_val = $this_meta['meta_value'];
			if(strcmp($meta_key, 'first_name') == 0) {
				$user[0]['wpu_first_name'] = $meta_val;
			}

			if(strcmp($meta_key, 'last_name') == 0) {
				$user[0]['wpu_last_name'] = $meta_val;
			}

		}
		$output[] = $user[0];
	}


	disconnectConnection($con);

	return $output;
}




function getAllMaleUsers() {
	
	$con = makeConnection();

	$male_users_meta = runSimpleFetchQuery($con, ['*'], "wp_usermeta", ['meta_key', 'meta_value'], ['=', '='], ['"gender"', 0], "", "", "");
	$male_users = array();

	for ($n = 0; $n < count($male_users_meta); $n++) {

		$user_id = $male_users_meta[$n]['user_id'];
		$user = runSimpleFetchQuery($con, ['*'], "wp_users", ['ID'], ['='], [$user_id], "", "", "");
		$user_meta = runSimpleFetchQuery($con, ['*'], "wp_usermeta", ['user_id'], ['='], [$user_id], "", "", "");
		for($i = 0; $i < count($user_meta); $i++) {

			$this_meta = $user_meta[$i];
			$meta_key = $this_meta['meta_key'];
			$meta_val = $this_meta['meta_value'];
			if(strcmp($meta_key, 'first_name') == 0) {
				$user[0]['wpu_first_name'] = $meta_val;
			}

			if(strcmp($meta_key, 'last_name') == 0) {
				$user[0]['wpu_last_name'] = $meta_val;
			}

		}

		$male_users[] = $user[0];

	}


	disconnectConnection($con);

	return $male_users;
}



function getAllFemaleUsers() {
	
	$con = makeConnection();

	$male_users_meta = runSimpleFetchQuery($con, ['*'], "wp_usermeta", ['meta_key', 'meta_value'], ['=', '='], ['"gender"', 1], "", "", "");
	$male_users = array();

	for ($n = 0; $n < count($male_users_meta); $n++) {

		$user_id = $male_users_meta[$n]['user_id'];
		$user = runSimpleFetchQuery($con, ['*'], "wp_users", ['ID'], ['='], [$user_id], "", "", "");
		$user_meta = runSimpleFetchQuery($con, ['*'], "wp_usermeta", ['user_id'], ['='], [$user_id], "", "", "");
		for($i = 0; $i < count($user_meta); $i++) {

			$this_meta = $user_meta[$i];
			$meta_key = $this_meta['meta_key'];
			$meta_val = $this_meta['meta_value'];
			if(strcmp($meta_key, 'first_name') == 0) {
				$user[0]['wpu_first_name'] = $meta_val;
			}

			if(strcmp($meta_key, 'last_name') == 0) {
				$user[0]['wpu_last_name'] = $meta_val;
			}

		}

		$male_users[] = $user[0];

	}


	disconnectConnection($con);

	return $male_users;
}



function getAllUsers() {
	
	$con = makeConnection();


	$user = runSimpleFetchQuery($con, ['*'], "wp_users", [], [], [], "", "", "");
	for ($l = 0; $l < count($user); $l++) {
		$this_user = $user[$l];
		$user_id = $this_user['ID'];
		$user_meta = runSimpleFetchQuery($con, ['*'], "wp_usermeta", ['user_id'], ['='], [$user_id], "", "", "");
		for($i = 0; $i < count($user_meta); $i++) {

			$this_meta = $user_meta[$i];
			$meta_key = $this_meta['meta_key'];
			$meta_val = $this_meta['meta_value'];
			if(strcmp($meta_key, 'first_name') == 0) {
				$user[$l]['wpu_first_name'] = $meta_val;
			}

			if(strcmp($meta_key, 'last_name') == 0) {
				$user[$l]['wpu_last_name'] = $meta_val;
			}

		}
	}


	disconnectConnection($con);

	return $user;
}




function constructMChimpUser($email, $f_name, $l_name) {

	$json = array(
		'email_address' => $email,
		'status'        => 'subscribed',
		'merge_fields'  => array(
			'FNAME'     => $f_name,
			'LNAME'     => $l_name
			)
		);

	return $json;

}




function sendToMailchimp($list) {

	$out_list = array();

	for ($i = 0; $i < count($list); $i++) {

		$this_user = $list[$i];
		$mailchimp_object = constructMChimpUser($this_user['user_email'], $this_user['wpu_first_name'], $this_user['wpu_last_name']);
		$out_list[] = $mailchimp_object;

	}

	signUpUser($out_list, $_GET['list_id']);

}




function signUpUser($list_of_users, $listID) {
	 // MailChimp API credentials
	$apiKey = $GLOBALS['mailchimp_api_key'];

// MailChimp API URL
	//$memberID = md5(strtolower($email));
	$dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
	//$url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;

	$auth = base64_encode('user:'.$apiKey);

	//  // member information
	// $json = json_encode(
	// 	[
	// 	'email_address' => $email,
	// 	'status'        => 'subscribed',
	// 	'merge_fields'  => [
	// 	'FNAME'     => $f_name,
	// 	'LNAME'     => $l_name
	// 	]
	// 	]);


	$final_array = array(
		"operations" => array()
		);

	for ($n = 0; $n < count($list_of_users); $n++) {

		$this_user = $list_of_users[$n];
		$this_user["apikey"] = $apiKey;
		$operation = array(
			"method" => "POST",
			"path" => "/lists/".$listID."/members/",
			"body" => json_encode($this_user)
			);

		$final_array['operations'][] = $operation;

	}

	$final_json = json_encode($final_array);

	$url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/batches';
        // send a HTTP POST request with curl
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json','Authorization: Basic '.$auth]);
	curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/3.0');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch, CURLOPT_POST, true);
	//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $final_json);
	$result = curl_exec($ch);
	$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);

	if ($httpCode == 200) {

		return true;

	} else {

		return false;

	}

}


?>