<?php

if(!function_exists('mysqli_fetch_all')){
    function mysqli_fetch_all($result, $resulttype = MYSQLI_NUM){
        $rows = [];
        
        while($row = mysqli_fetch_array($result, $resulttype)){
            $rows[] = $row;
        }
        
        return $rows;
    }
}