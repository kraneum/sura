<?php

require_once '../php/user_permissions.php';

$parseUserPermission = function($userPermission, &$employee) {
    $userPermissions = [];

    if (!UserPermissions::isSuperuserPermission(+$userPermission)) {
        $permissions = UserPermissions::getAllPermissions();
        array_walk($permissions, function($permissionValue, $permission)use($userPermission, &$userPermissions, &$employee) {
            $employee[$permission] = UserPermissions::hasPermission($permission, $userPermission) && ($userPermissions[] = $permission);
        });
    } else {
        $userPermissions = ['SUPER'];
        $employee['SUPER'] = true;
    }

    return $userPermissions;
};
