<?php

session_start();
require 'sura_config.php';
require 'sura_functions.php';

if (isset($_SESSION['user']) != "") {
    header("Location: home.php");
}

$error = false;
$errMsg = null;

if (isset($_POST['btn-register'])) {
    // clean user inputs to prevent sql injections
    $fname = "'" . htmlspecialchars(strip_tags(trim($_POST['fname']))) . "'";
    $midname = "'" . htmlspecialchars(strip_tags(trim($_POST['midname']))) . "'";
    $lname = "'" . htmlspecialchars(strip_tags(trim($_POST['lname']))) . "'";
    $dob = "'" . htmlspecialchars(strip_tags(trim($_POST['dob']))) . "'";
    $phone = "'" . htmlspecialchars(strip_tags(trim($_POST['phone']))) . "'";
    $email = "'" . htmlspecialchars(strip_tags(trim($_POST['email']))) . "'";
    $country = "'" . htmlspecialchars(strip_tags(trim($_POST['country']))) . "'";
    $question = "'" . htmlspecialchars(strip_tags(trim($_POST['question']))) . "'";
    $answer = "'" . htmlspecialchars(strip_tags(trim($_POST['answer']))) . "'";
    $username = "'" . htmlspecialchars(strip_tags(trim($_POST['username']))) . "'";
    $password = "'" . htmlspecialchars(strip_tags(trim($_POST['password']))) . "'";


//    basic email validation
    $_email = htmlspecialchars(strip_tags(trim($_POST['email'])));
    if (!empty($_email)) {
        if (!filter_var($_email, FILTER_VALIDATE_EMAIL)) {
            $error = true;
            $errMsg = "Please enter valid email address.\n";
        } else {
            $con = makeConnection();
            // check email exist or not
            $query = "SELECT email FROM users WHERE email='$_email'";
            $result = mysqli_query($con, $query);
            $count = mysqli_num_rows($result);
            if ($count != 0) {
                $error = true;
                $errMsg .= "<span style=\"color:red\">Provided Email is already in use.</span>\n";
            }
        }
    } else {
        $error = true;
        $errMsg = "Please enter email address.\n";
    }

    // Check for duplicate usernames
    $_username = htmlspecialchars(strip_tags(trim($_POST['username'])));
    if (empty($_username)) {
        $error = true;
        $errMsg = "Please enter username.\n";
    } else {
        $con = makeConnection();
        // check email exist or not
        $query = "SELECT username FROM users WHERE email='$_username'";
        $result = mysqli_query($con, $query);
        $count = mysqli_num_rows($result);
        if ($count != 0) {
            $error = true;
            $errMsg .= "<span style=\"color:red\">Provided Username is already in use.</span>\n";
        }
    }
    // password validation
//    if (empty($pass)) {
//        $error = true;
//        $passError = "Please enter password.";
//    } else if (strlen($pass) < 6) {
//        $error = true;
//        $passError = "Password must have atleast 6 characters.";
//    }
    // password encrypt using SHA256();
//    $password = hash('sha256', $pass);
    // if there's no error, continue to signup
    if (!$error) {
        $con = makeConnection();

        $res = runSimpleInsertQuery($con, "users", ['f_name', 'm_name', 'l_name', 'dob', 'phone', 'email', 'country', 'username', 'password', 'security_question', 'security_answer', 'profile_pic'], ["'$fname'", "'$midname'", "'$lname'", "'$dob'", "'$phone'", "'$email'", "'$country'", "'$username'", "'$password'", "'$question'", "'$answer'", 'http://localhost/iinsurance/img/mypic.jpg']);
        
        if ($res) {
            $errMsg = "<span style = \"color:green\">Successfully registered, you may login now</span>";
            unset($fname);
            unset($midname);
            unset($lname);
            unset($dob);
            unset($country);
            unset($phone);
            unset($email);
            unset($username);
            unset($password);
            unset($question);
            unset($answer);
        } else {
            $error = "true";
            $errMsg = "<span style=\"color:red\">Something went wrong, try again later...</span>";
        }
    }
}
