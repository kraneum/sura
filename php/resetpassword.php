<?php
$link = filter_input(INPUT_GET, 'i', FILTER_SANITIZE_STRING);

if (empty($link)) {
    exit('No link specified');
}
?>

<!DOCTYPE html>

<html>
    <head>
        <title>iInsurance</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="icon" type="image/png" href="../img/favicon.ico">
        <link rel="apple-touch-icon" href="../img/favicon.ico">

        <link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
        <link href="../css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">
            body {
                padding: 0 25%;
            }
            form {
                margin: 0 20%;
            }
            #info {
                margin: 10px 10%;
            }
        </style>
    </head>
    <body>

        <header><img class="img-responsive center-block" src="../img/logos/uic_full.png" alt=""/></header>
        <div id="info"></div>
        <div id="content">
            <form id ="forgotPasswordForm" class="form-horizontal" method="post">
                <input type="hidden" name="link" value="<?= $link ?>">
                <div class="form-group">
                    <label for="password">Password * </label>
                    <input class="form-control" id="password" name="password" value="asas"  type="password" placeholder="Enter password" data-parsley-required data-parsley-minlength="4" data-parsley-minlength-message="This value should be 4 digits"/>
                </div>

                <div class="form-group">
                    <label for="password_confirm">Confirm Password * </label>
                    <input class="form-control" id="password_confirm" name="password_confirm" value="asas"  type="password" placeholder="Enter password again" data-parsley-equalto="#password" data-parsley-equalto-message="Password mismatch" data-parsley-required data-parsley-minlength="4" data-parsley-minlength-message="This value should be 4 digits"/>
                </div>

                <div class="form-group">

                    <button id="formVerify" type="submit" class="btn btn-success" style="border-radius: 50px;" name="btn-verify"><i class="fa fa-check btn-icon-left"></i> VERIFY</button>
                </div>
            </form>
        </div>

        <script src="../js/jquery-3.1.1.js" type="text/javascript"></script>
        <script src="../js/parsley.min.js" type="text/javascript"></script>
        <script src="../js/bootstrap.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#forgotPasswordForm').parsley();

                $('body').on('submit', '#forgotPasswordForm', function (e) {

                    $('#formVerify').html('<i class="fa fa-spinner fa-pulse fa-fw"></i> VERIFYING ...');
                    $('#info').empty();

                    var formData = {
                        'link': $('input[name=link]').val(),
                        'password': $('input[name=password]').val()
                    };

                    $.post('suracommands.php?command=reset_password',formData, function (data) {

                        $('#formVerify').html('<i class="fa fa-check btn-icon-left"></i> VERIFY');
                        if (data['status']) {
                            $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                                    '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                    '<strong>Password Change Successful. Redirecting Shortly..</strong></div>');
                        setTimeout(function (){
                            window.location.href = "prsuccess.php?entity=" + data['entity'];
                        }, 2000);
                        } else {
                            if (data['err'] === 'INVALID_LINK') {
                                $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                        '<strong>Invalid link</strong></div>');
                            } else if (data['err'] === 'INTERNAL_ERROR') {
                                $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                        '<strong>An internal error occurred</strong></div>');
                            }
                        }

                    }, 'JSON');

                    e.preventDefault();

                });
            });
        </script>
    </body>
</html>
