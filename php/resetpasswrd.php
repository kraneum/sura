<?php
$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

$entity = $_POST['entity'];
$id = $_POST['id'];
?>

<form id ="forgotPasswordForm" class="form-horizontal" method="post">
    <input type="hidden" name="entity" value="<?= $entity ?>">
    <input type="hidden" name="id" value="<?= $id ?>">
    <div class="form-group">
        <label for="password">Password * </label>
        <input class="form-control" id="password" name="password" value="asas"  type="password" placeholder="Enter password" data-parsley-required data-parsley-minlength="4" data-parsley-minlength-message="This value should be 4 digits"/>
    </div>

    <div class="form-group">
        <label for="password_confirm">Confirm Password * </label>
        <input class="form-control" id="password_confirm" name="password_confirm" value="asas"  type="password" placeholder="Enter password again" data-parsley-equalto="#password" data-parsley-equalto-message="Password mismatch" data-parsley-required data-parsley-minlength="4" data-parsley-minlength-message="This value should be 4 digits"/>
    </div>

    <div class="form-group">

        <button id="formVerify" type="submit" class="btn btn-success" style="border-radius: 50px;" name="btn-verify"><i class="fa fa-check btn-icon-left"></i> VERIFY</button>
    </div>
</form>


<script type="text/javascript">
    $(document).ready(function () {
        $('#forgotPasswordForm').parsley();

        $('body').on('submit', '#forgotPasswordForm', function (e) {

            $('#formVerify').html('<i class="fa fa-spinner fa-pulse fa-fw"></i> VERIFYING ...');
            $('#info').empty();

            var formData = {
                'id': $('input[name=id]').val(),
                'password': $('input[name=password]').val()
            };

            $.post('suracommands.php?command=reset_password_2', formData, function (data) {

                $('#formVerify').html('<i class="fa fa-check btn-icon-left"></i> VERIFY');
                if (data['status']) {
                    $('#info').html('<div class="alert alert-dismissible alert-success text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Password Change Successful. Redirecting Shortly..</strong></div>');
                    setTimeout(function () {
                        window.location.href = "prsuccess.php?entity=" + $('input[name=entity]').val();
                    }, 2000);
                } else {
                    if (data['err'] === 'INTERNAL_ERROR') {
                        $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                '<strong>An internal error occurred</strong></div>');
                    }
                }

            }, 'JSON');

            e.preventDefault();

        });
    });
</script>
