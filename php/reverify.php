<?php
if (!isset($_SESSION)) {
    session_start();
}
if (!isset($_SESSION['entity_type'])) {
    exit('No Entity Specified');
}

$entity = $_SESSION['entity_type'];
$email = $_SESSION['entity']['email'];
$phone = $_SESSION['entity']['phone'];
$name = null;
if ($entity == 'USER' || $entity == 'AGENT') {
    $name = $_SESSION['entity']['f_name'];
} else if ($entity == 'INSURER' || $entity == 'PENSION') {
    $name = $_SESSION['entity']['name'];
}
?>
<!DOCTYPE html>

<html>
    <head>
        <title>iInsurance</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="../img/favicon.png" />
        <link rel="icon" type="image/png" href="../img/favicon.png">
        <link rel="apple-touch-icon" href="../img/favicon.png">

        <link href="../css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">
            body {
                padding: 0 25%;
            }
            form {
                margin: 0 20%;
            }
            #info {
                margin: 20px 0;
            }
            #content > img {
                margin-bottom: 5%;
                opacity: .5;
            }

            header > img {
                margin: 5% 0 7%;
            }
            h3 {
                margin-bottom: 20px
            }
            form {
                padding: 0 15%;
            }
            form>div >div{
                margin: 20px 0;
            }
        </style>
    </head>
    <body>

        <header><img width="250" height="250" class="img-responsive center-block" src="../img/logos/uic_full.png" alt=""/></header>
        <div id="content" class="text-center">
            <img src="img/approved.png" alt=""/>
            <h3>Account not verified</h3>
            <p>Please select a verification method</p>
            <form id="resendForm">
                <div class="form-group">
                    <input type="hidden" name ="entity" value="<?php echo $entity ?>"/>
                    <input type="hidden" name ="email" value="<?php echo $email ?>"/>
                    <input type="hidden" name ="phone" value="<?php echo $phone ?>"/>
                    <input type="hidden" name ="name" value="<?php echo $name ?>"/>

                    <?php
                    if ($entity == 'USER' || $entity == 'AGENT') { // cos insurers validate with email alone...
                        echo '<div class="radio-s">';
                        echo '<input id="sms" id="r2" name="verify" value="sms" type="radio" checked/>';
                        echo '<label for="sms" style="font-size: 20px">By SMS</label>';
                        echo '</div>';
                    }

                    if (!empty($email)) {
                        echo '<div class="radio-s">';
                        echo '<input id="email" id="r1" name="verify" value="email" type="radio"/>';
                        echo '<label for="email" style="font-size: 20px">By E-mail</label>';
                        echo '</div>';
                    }
                    ?>
                </div>

                <div class="form-group">
                    <button id="formResend" type="submit" class="btn btn-success btn-block"  name="btn-resend"><i class="fa fa-location-arrow"></i> Resend</button>
                </div>
            </form>
        </div>

        <script src="../js/jquery-3.1.1.js" type="text/javascript"></script>
        <script src="../js/bootstrap.js" type="text/javascript"></script>

        <script type="text/javascript">
            $('#resendForm').submit(function (e) {
                $('#formResend > i').remove();
                $('#formResend').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
                var formData = null;

                if ($('#sms').is(':checked')) {
                    formData = {
                        'entity': $('input[name=entity]').val(),
                        'method': 'sms',
                        'medium': $('input[name=phone]').val(),
                        'name': $('input[name=name]').val()
                    };
                } else if ($('#email').is(':checked')) {
                    formData = {
                        'entity': $('input[name=entity]').val(),
                        'method': 'email',
                        'medium': $('input[name=email]').val(),
                        'name': $('input[name=name]').val()
                    };
                }


                $.post('suracommands.php?command=resend_verification&web', formData, function (data) {
                    data = JSON.parse(data);

                    if (data['msg'] === 'EMAIL_VERIFY') {
                        window.location.href = "checkmail.php";
                    } else if (data['msg'] === 'OTP_VERIFY') {
                        window.location.href = "veripage.php";
                    }
                });

                e.preventDefault();
            });
        </script>
    </body>
</html>


