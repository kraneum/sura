<?php

require_once __DIR__ . '/bsgateway.php';


function sendSms($name, $message, $number, $country_code = 234) {
//    echo 'Sending to ' . $number . ' ';
    $phon = substr($number, 1);
    $response = carryOutSMS('info@kraneum.com', 'krAFRICA00', 'iInsurance', $country_code . $phon, "Hi " . $name . ",\r\n" . $message . "\r\n\r\nBest,\r\niInsurance", 0);
//    echo $response . ' ';
//    return $response;
}

function carryOutSMS($username, $password, $sender, $rec, $message, $message_id) {
    $parameters = sprintf("username=%s&password=%s&sender=%s&mobiles=%s&message=%s",
            urlencode($username), urlencode($password), urlencode($sender), urlencode($rec), urlencode($message));
    $responseString = sendSMS2('http://portal.bulksmsnigeria.net/api/', $parameters);
//    return $responseString;
}

function sendSMS2($apiurl, $params) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiurl);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_exec($ch);
//    $response = curl_exec($ch);
//    return $response;
}

?>
