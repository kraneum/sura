<?php

function makeConnection() {
    $DB_HostName = "localhost"; //$host;
    $DB_Name = "uic_sura"; //$database;
    $DB_User = "root"; //$username;
    $DB_Pass = ""; //$password;

    $__con = mysqli_connect($DB_HostName, $DB_User, $DB_Pass, $DB_Name);
    if (mysqli_connect_errno()) {
        error_log("Connect failed. Error:" . mysqli_connect_error() . " Code: " . mysqli_connect_errno());
    }

    return $__con;
}

function makeConnection_user() {
    $DB_HostName = "localhost"; //$host;
    $DB_Name = "uic_sura"; //$database;
    $DB_User = "root"; //$username;
    $DB_Pass = ""; //$password;

    $__con = mysqli_connect($DB_HostName, $DB_User, $DB_Pass, $DB_Name);
    if (mysqli_connect_errno()) {
        error_log("Connect failed. Error:" . mysqli_connect_error() . " Code: " . mysqli_connect_errno());
    }

    return $__con;
}

function makeConnection_insurer() {
    $DB_HostName = "localhost"; //$host;
    $DB_Name = "uic_sura"; //$database;
    $DB_User = "root"; //$username;
    $DB_Pass = ""; //$password;

    $__con = mysqli_connect($DB_HostName, $DB_User, $DB_Pass, $DB_Name);
    if (mysqli_connect_errno()) {
        error_log("Connect failed. Error:" . mysqli_connect_error() . " Code: " . mysqli_connect_errno());
    }

    return $__con;
}

function makeConnection_merchant() {
    $DB_HostName = "localhost"; //$host;
    $DB_Name = "uic_sura"; //$database;
    $DB_User = "root"; //$username;
    $DB_Pass = ""; //$password;

    $__con = mysqli_connect($DB_HostName, $DB_User, $DB_Pass, $DB_Name);
    if (mysqli_connect_errno()) {
        error_log("Connect failed. Error:" . mysqli_connect_error() . " Code: " . mysqli_connect_errno());
    }

    return $__con;
}

//function makeConnection() {
//    $DB_HostName = "159.65.21.159"; //$host;
//    $DB_Name = "insurance"; //$database;
//    $DB_User = "iinsurance_mgr"; //$username;
//    $DB_Pass = "!~X1022b4793409d6bc776b757c5e2f609a8c6780346831ef85"; //$password;
//
//    $__con = mysqli_connect($DB_HostName, $DB_User, $DB_Pass, $DB_Name);
//    if (mysqli_connect_errno()) {
//        error_log("Connect failed. Error:" . mysqli_connect_error() . " Code: " . mysqli_connect_errno());
//    }
//
//    return $__con;
//}

function autoCommit($con, $value) {
    return mysqli_autocommit($con, $value);
}

function commit($con) {
    return mysqli_commit($con);
}

function rollback($con) {
    return mysqli_rollback($con);
}

//When u setup memcache, disable this function
function disconnectConnection($con) {
    mysqli_close($con);
}



/************ Medoo ************/
require_once __DIR__. '/Medoo.php';
function makeMedoo() {
    return new \Medoo\Medoo([
        'database_type' => 'mysql',
        'database_name' => 'uic_sura',
        'server' => 'localhost',
        'username' => 'root',
        'password' => ''
    ]);
}

?>