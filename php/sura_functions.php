<?php

//

require_once 'mysqli.php';

//returns true or false depending upon success or failure
function runSimpleUpdateQuery($con, $table, $columns, $values, $where_keys, $where_operators, $where_values, $limit = '', $offset = '') {

    $query = "UPDATE " . $table . " SET ";

    for ($i = 0, $columnsCt = count($columns); $i < $columnsCt; $i++) {
        if ($i < $columnsCt - 1) {
            $query .= $columns[$i] . "=" . ($values[$i] ? $values[$i] : 'NULL') . ", ";
        } else {
            $query .= $columns[$i] . "=" . ($values[$i] ? $values[$i] : 'NULL') . " ";
        }
    }

    $query .= "WHERE ";

    for ($i = 0, $countWhereValues = count($where_values); $i < $countWhereValues; $i++) {
        if ($i == 0) {
            $query .= $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
        } else {
            $query .= "AND " . $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
        }
    }

    $limit && ($query .= ' LIMIT ' . $limit);

    $offset && ($query .= ' OFFSET ' . $offset);

    $query .= ";";

//    echo $query;

    mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_affected_rows($con)];
}

function runCaseUpdateQuery() { // for merchants....
}

function runSuspendAdminQuery($con, $employeeId, $adminPermission, $suspend) { // for admins....
    mysqli_query($con, 'UPDATE admins SET suspended = CASE WHEN user_permissions!=' . $adminPermission . ' THEN ' . $suspend . ' ELSE suspended END WHERE id = ' . $employeeId . ' LIMIT 1');
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_affected_rows($con)];
}

function runDeleteAdminQuery($con, $employeeId, $adminPermission) {  // for admins....
    mysqli_query($con, 'UPDATE admins SET deleted = CASE WHEN user_permissions!=' . $adminPermission . ' THEN 1 ELSE deleted END WHERE id = ' . $employeeId . ' LIMIT 1');
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_affected_rows($con)];
}

function runSuspendUserQuery($con, $employeeId, $companyId, $adminPermission, $suspend) { // for merchants....
    mysqli_query($con, 'UPDATE employees SET suspended = CASE WHEN company_id=' . $companyId . ' AND user_permissions!=' . $adminPermission . ' THEN ' . $suspend . ' ELSE suspended END WHERE id = ' . $employeeId . ' LIMIT 1');
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_affected_rows($con)];
}

function runDeleteUserQuery($con, $employeeId, $companyId, $adminPermission) {  // for merchants....
    mysqli_query($con, 'UPDATE employees SET deleted = CASE WHEN company_id=' . $companyId . ' AND user_permissions!=' . $adminPermission . ' THEN 1 ELSE deleted END WHERE id = ' . $employeeId . ' LIMIT 1');
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_affected_rows($con)];
}

function runUpdatePasswdQuery($con, $employeeId, $oldPasswd, $newPasswd) { // for merchants....
    mysqli_query($con, 'UPDATE employees SET password = CASE WHEN password="' . $oldPasswd . '" THEN "' . $newPasswd . '" ELSE password END WHERE id = ' . $employeeId . ' LIMIT 1');
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_affected_rows($con)];
}

function runSubtractMerchantPointsQuery($con, $merchantId, $pointsToSubtract) { // for merchants....
    mysqli_query($con, 'UPDATE merchants SET point_balance = CASE WHEN point_balance>=' . $pointsToSubtract . ' THEN point_balance-' . $pointsToSubtract . ' ELSE point_balance END WHERE id = ' . $merchantId . ' LIMIT 1');
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_affected_rows($con)];
}

function runAddMerchantsPoints($con, $merchantId, $pointsToAdd) { // for merchants....
    mysqli_query($con, 'UPDATE merchants SET point_balance = point_balance+' . $pointsToAdd . ' WHERE id = ' . $merchantId . ' LIMIT 1');
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_affected_rows($con)];
}

function runFetchUnredeemedUssdCt($con, $merchantId) {
    $result = mysqli_query($con, 'SELECT COUNT(id) FROM ussd_codes WHERE merchant_id = ' . $merchantId . ' AND redemption_id = 0');
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_array($result)[0]];
}

function runRegisteredMerchantsCt($con) {
    $result = mysqli_query($con, 'SELECT COUNT(id) FROM merchants');
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_array($result)[0]];
}

function runRegisterdMerchantsThisMnthCt($con) {
    $firstOfThisMnth = date('Y-m-01') . 'T00:00:00+0000';
    $result = mysqli_query($con, 'SELECT COUNT(id) FROM merchants WHERE date_joined >= "' . $firstOfThisMnth . '"');
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_array($result)[0]];
}

function runUnapprovedMerchantsCt($con) {
    $result = mysqli_query($con, 'SELECT COUNT(id) FROM merchants WHERE approved = 0');
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_array($result)[0]];
}

function runFetchUssdPurchasesCt($con, $merchantId) {
    $result = mysqli_query($con, 'SELECT COUNT(id) FROM ussd_purchases WHERE merchant_id = ' . $merchantId . ' order by id DESC');
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_array($result)[0]];
}

function runFetchLast5Redeemptions($con, $merchant_id) {
//optimise this function to make use of the redemption_id index at: ussd_codes.redemption_id != 0 where clause
    $result = mysqli_query($con, 'SELECT redemptions.redeemed_date, ussd_codes.code, ussd_codes.point_worth FROM redemptions INNER JOIN ussd_codes ON redemptions.ussd_code_id = ussd_codes.id WHERE ussd_codes.merchant_id = ' . $merchant_id . ' AND ussd_codes.redemption_id != 0 ORDER BY redemptions.id DESC LIMIT 5');
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runFetchCompaniesQuery($con) {
    $result = mysqli_query($con, 'SELECT id, name FROM merchants UNION SELECT id, name FROM insurer');
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runInsertUSSDCodes($con, $codes, $pointsOrder, $purchaseId, $merchantId) {
    $values = '';
    $pointsOrderAmt = current($pointsOrder);

    foreach ($codes as $code) {
        $values .= '("' . $code . '",' . $purchaseId . ',' . key($pointsOrder) . ',' . $merchantId . '),';
        $pointsOrderAmt = $pointsOrderAmt > 1 ? $pointsOrderAmt - 1 : next($pointsOrder);
    }

//reset($pointsOrder);

    $values[strlen($values) - 1] = ' ';

    mysqli_query($con, 'REPLACE INTO ussd_codes (code, purchase_id, point_worth, merchant_id) VALUES ' . $values);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_affected_rows($con), 'insertedId' => mysqli_insert_id($con)];
}

function ussdPurchases($con, $merchant_id, $page) {
    $result = mysqli_query($con, 'SELECT ussd_purchases.id, ussd_purchases.datetimezone,ussd_purchases.total_value,ussd_purchases.codes_purchased,merchants_recent_activity.activity_summary FROM ussd_purchases INNER JOIN merchants_recent_activity ON ussd_purchases.id = merchants_recent_activity.activity_id WHERE ussd_purchases.merchant_id = ' . $merchant_id . ' AND merchants_recent_activity.activity_type="PINS_GENERATED" ORDER BY ussd_purchases.id DESC LIMIT ' . ($page - 1) * 5 . ',5');
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runPoliciesByCategoryQuery($con, $cat_id, $active, $limit = null, $offset = null) { // for users....
    $qp = "ORDER BY policies.id ASC ";

    if (strcmp($limit, "") != 0) {
        $qp .= "LIMIT " . $limit . " ";
    }

    if (strcmp($offset, "") != 0) {
        $qp .= "OFFSET " . $offset . "";
    }

    $qp .= ";";

    $result = mysqli_query($con, 'SELECT policies.* FROM policies LEFT JOIN insurance_types ON policies.type_id=insurance_types.id WHERE policies.active = ' . $active . ' AND insurance_types.id = ' . $cat_id . ' ' . $qp);
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runUICPoliciesByCategoryQuery($con, $cat_id, $active, $limit = null, $offset = null) { // for users....
    $qp = "ORDER BY policies.id ASC ";

    if (strcmp($limit, "") != 0) {
        $qp .= "LIMIT " . $limit . " ";
    }

    if (strcmp($offset, "") != 0) {
        $qp .= "OFFSET " . $offset . "";
    }

    $qp .= ";";

    $result = mysqli_query($con, 'SELECT policies.* FROM policies LEFT JOIN insurance_types ON policies.type_id=insurance_types.id WHERE policies.active = ' . $active . ' AND policies.insurer_id = 0 AND insurance_types.id = ' . $cat_id . ' ' . $qp);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runFetchPieChartDataAdminQuery($con, $startDate = null, $endDate = null) { // for users....
    $query = 'SELECT merchants.name, point_purchases.point_amount, point_purchases.date_bought  '
            . 'FROM point_purchases '
            . 'LEFT JOIN merchants '
            . 'ON merchants.id=point_purchases.merchant_id ';

    if (!empty($startDate)) {
        $query .= 'and point_purchases.date_bought >= "' . $startDate . '" ';
    }
    if (!empty($endDate)) {
        $query .= 'and point_purchases.date_bought <= "' . $endDate . '" ';
    }

//    echo $query;

    $result = mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runFetchLineChartDataAdminQuery($con, $insurer_id, $startDate = null, $endDate = null) { // for users....
    $query = 'SELECT transactions.id, transactions.point_worth, transactions.date_transacted '
            . 'FROM transactions ';

    $query .= 'WHERE transactions.to_who = ' . $insurer_id . ' AND '
            . '(type_id = 2 OR '
            . 'type_id = 3) ';


    if (!empty($startDate)) {
        $query .= 'and transactions.date_transacted >= "' . $startDate . '" ';
    }
    if (!empty($endDate)) {
        $query .= 'and transactions.date_transacted <= "' . $endDate . '" ';
    }

//    echo $query;

    $result = mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runFetchPieChartDataInsurerQuery($con, $id, $gender = null, $startDate = null, $endDate = null, $active = null, $from = null, $to = null) { // for users....
    $query = 'SELECT user_signups.id, user_signups.date_signed, policies.title, COUNT(user_signups.id) AS num_users '
            . 'FROM policies '
            . 'LEFT JOIN user_signups '
            . 'ON user_signups.policy_id = policies.id  '
            . 'LEFT JOIN insurer '
            . 'ON insurer.id = policies.insurer_id '
            . 'LEFT JOIN users '
            . 'ON users.id=user_signups.user_id ';

    $query .= 'WHERE insurer.id = ' . $id . ' ';

    if (!empty($gender)) {
        $query .= 'and users.gender = "' . $gender . '" ';
    }
    if (!empty($startDate)) {
        $query .= 'and user_signups.date_signed >= "' . $startDate . '" ';
    }
    if (!empty($endDate)) {
        $query .= 'and user_signups.date_signed <= "' . $endDate . '" ';
    }
    if (!empty($active)) {
        $query .= 'and user_signups.active = "' . $active . '" ';
    }

    $query .= 'GROUP BY user_signups.policy_id ';
    if (($from == 0 && $to == 0) || ($from == null && $to == null)) {
        $query .= 'HAVING num_users >= 0';
    } else {
        $query .= 'HAVING num_users >= ' . $from . ' AND num_users <= ' . $to;
    }

//    echo $query;

    $result = mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runFetchLineChartDataInsurerQuery($con, $id, $gender = null, $startDate = null, $endDate = null, $active = null) { // for users....
    $query = 'SELECT user_signups.* '
            . 'FROM user_signups '
            . 'LEFT JOIN users ON users.id = user_signups.user_id ';

    $query .= 'WHERE user_signups.policy_id = ' . $id . ' ';

    if (!empty($gender)) {
        $query .= 'and users.gender = "' . $gender . '" ';
    }
    if (!empty($startDate)) {
        $query .= 'and user_signups.date_signed >= "' . $startDate . '" ';
    }
    if (!empty($endDate)) {
        $query .= 'and user_signups.date_signed <= "' . $endDate . '" ';
    }
    if (!empty($active)) {
        $query .= 'and user_signups.active = "' . $active . '" ';
    }

//    echo $query;

    $result = mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runFetchLineChartDataPensionQuery($con, $id, $gender = null, $startDate = null, $endDate = null, $active = null) { // for users....
    $query = 'SELECT pension_signups.* '
            . 'FROM pension_signups '
            . 'LEFT JOIN users ON users.id = pension_signups.user_id ';

    $query .= 'WHERE pension_signups.pension_id = ' . $id . ' ';

    if (!empty($gender)) {
        $query .= 'and users.gender = "' . $gender . '" ';
    }
    if (!empty($startDate)) {
        $query .= 'and pension_signups.date_signed >= "' . $startDate . '" ';
    }
    if (!empty($endDate)) {
        $query .= 'and pension_signups.date_signed <= "' . $endDate . '" ';
    }
    if (!empty($active)) {
        $query .= 'and pension_signups.active = "' . $active . '" ';
    }

//    echo $query;

    $result = mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runFetchUsersDataQuery($con, $points, $gender = null, $active = null, $limit = null, $offset = null) { // for users....
    $query = 'SELECT id, f_name, l_name, general_point_balance, health_point_balance '
            . 'FROM users '
            . 'WHERE health_point_balance >= ' . $points
            . ' OR general_point_balance >= ' . $points;

    if (!empty($gender)) {
        $query .= ' and gender = "' . $gender . '"';
    }
    if (!empty($active)) {
        $query .= ' and active = "' . $active . '"';
    }
    if (strcmp($limit, "") != 0) {
        $query .= " LIMIT " . $limit . " ";
    }
    if (strcmp($offset, "") != 0) {
        $query .= " OFFSET " . $offset;
    }

    $query .= ";";

//    echo $query;

    $result = mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runFetchContentQuery($con, ...$params) { // for users....
    $query = 'SELECT content '
            . 'FROM content_mgt '
            . '';

    foreach ($params as $i => $param) {
        if ($i == 0) {
            $query .= 'WHERE ';
        } else {
            $query .= ' OR ';
        }
        $query .= "title = '$param'";
    }
    $query .= ";";

//    echo $query;

    $result = mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runCountQuery($con, $table, $where_keys, $where_operators, $where_values) { // for users....
    $query = "SELECT count(id) ";

    if (count($where_keys) > 0) {
        $query .= "FROM " . $table . " WHERE ";
    } else {
        $query .= "FROM " . $table;
    }

    for ($i = 0, $countWhere_values = count($where_values); $i < $countWhere_values; $i++) {
        if ($i == 0) {
            $query .= $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
        } else {
            $query .= "AND " . $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
        }
    }

    $query .= ";";

    $execute = mysqli_query($con, $query);
    if ($execute) {

        $rows = array();
        while ($r = mysqli_fetch_array($execute, MYSQLI_ASSOC)) {
            $rows[] = $r;
        }
        return ['err' => ['error' => '', 'code' => 0], 'result' => $rows];
    } else {
        return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => $execute];
    }
}

function runSimpleFetchNotificationsQuery($con, $columns_to_fetch, $table, $where_keys, $where_operators, $where_values, $group_by, $order, $limit, $offset = null) {

    $query = "SELECT ";

    if (is_array($columns_to_fetch)) {
        for ($i = 0, $countColumns_to_fetch = count($columns_to_fetch); $i < $countColumns_to_fetch; $i++) {
            if ($i < $countColumns_to_fetch - 1) {
                $query .= $columns_to_fetch[$i] . ", ";
            } else {
                $query .= $columns_to_fetch[$i] . " ";
            }
        }
    } else {
        $query .= $columns_to_fetch . " ";
    }

    if (count($where_keys) > 0) {
        $query .= "FROM " . $table . " WHERE ";
    } else {
        $query .= "FROM " . $table . ' ';
    }

    for ($i = 0, $countWhere_values = count($where_values); $i < $countWhere_values; $i++) {
        if ($i == 0) {
            $query .= '(';
            $query .= $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
        } else if ($i == 1) {
//            if (strpos($table, "user") !== false || strpos($table, "agent") !== false) {
            $query .= "OR " . $where_keys[$i] . $where_operators[$i] . $where_values[$i] . ") ";
//            } else {
//                $query .= "AND " . $where_keys[$i] . $where_operators[$i] . $where_values[$i] . ") ";
//            }
        } else if ($i == 2) {
            $query .= "AND " . $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
        }
    }

    if (strcmp($group_by, "") != 0) {
        $query .= "GROUP BY " . $group_by . " ";
    }

    if (strcmp($order, "") != 0) {
        $query .= "ORDER BY " . $order . " ";
    }

    if (strcmp($limit, "") != 0) {
        $query .= "LIMIT " . $limit . " ";
    }
    if (strcmp($offset, "") != 0) {
        $query .= "OFFSET " . $offset . " ";
    }

    $query .= ";";

//    echo $query;

    $execute = mysqli_query($con, $query);
    if ($execute) {
        $res1 = mysqli_num_rows($execute);

//Fixed a bug: $res1 = 1 instead of $res1 === 1 causes a bug
        if ($res1 === 1 && count($columns_to_fetch) == 1 && strcmp($columns_to_fetch[0], "*") != 0) {
            return ['err' => ['error' => '', 'code' => 0], 'result' => mysqli_fetch_array($execute)[0]];
        } else {
            $rows = array();
            while ($r = mysqli_fetch_array($execute, MYSQLI_ASSOC)) {
                $rows[] = $r;
            }
            return ['err' => ['error' => '', 'code' => 0], 'result' => $rows];
        }
    } else {
        return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => $execute];
    }
}

function runFetchUsersCount($con) {
    $result = mysqli_query($con, "SELECT COUNT(id) AS t_ac_ap FROM users  UNION ALL SELECT  COUNT(active) FROM users WHERE active = 'TRUE'  UNION ALL SELECT  COUNT(approved) FROM users WHERE approved = 'TRUE'");
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

//returns 1 or 0 depending upon success or failure
function copyFromTableToTable($con, $from_table, $to_table, $rows, $where_keys, $where_comps, $where_vals) {
    $query = 'INSERT INTO ' . $to_table . ' SELECT ';

    for ($n = 0; $n < count($rows); $n++) {
        $query .= $rows[$n];
        if ($n < count($rows) - 1) {
            $query .= ',';
        }
    }

    $query .= ' FROM ' . $from_table . ' WHERE ';

    for ($w = 0; $w < count($where_keys); $w++) {
        $query .= $where_keys[$w] . ' ' . $where_comps[$w] . ' ' . $where_vals[$w];
        if ($w < count($where_keys) - 1) {
            $query .= ' AND ';
        }
    }

//echo $query;

    $execute = mysqli_query($con, $query);

    if ($execute) {
        $delete_q = 'DELETE FROM ' . $from_table . ' WHERE ';
        for ($w = 0; $w < count($where_keys); $w++) {
            $delete_q = $delete_q . $where_keys[$w] . ' ' . $where_comps[$w] . ' ' . $where_vals[$w];
            if ($w < count($where_keys) - 1) {
                $delete_q = $delete_q . ' AND ';
            }
        }
//echo $delete_q;
        $execute_q = mysqli_query($con, $delete_q);


        if ($execute_q) {

            return ['err' => ['error' => '', 'code' => 0], 'result' => mysqli_affected_rows($con)];
        } else {

            $fail_q = 'DELETE FROM ' . $to_table . ' WHERE ';
            for ($w = 0; $w < count($where_keys); $w++) {
                $fail_q .= $where_keys[$w] . ' ' . $where_comps[$w] . ' ' . $where_vals[$w];
                if ($w < count($where_keys) - 1) {
                    $fail_q .= ' AND ';
                }
            }
// echo $fail_q;
            $execute_qq = mysqli_query($con, $fail_q);
            return ['err' => ['error' => '', 'code' => 0], 'result' => mysqli_affected_rows($con)];
        }
    } else {
        return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_affected_rows($con)];
    }
}

//returns true or false depending upon success or failure
function uniqueUSSDCodeInsert($con, $table, $point_worth, $purchase_id) {

    $query = "INSERT IGNORE INTO " . $table . "(code, purchase_id, point_worth) VALUES( UPPER(SUBSTRING(MD5(RAND()) FROM 1 FOR 16)), " . $purchase_id . ", " . $point_worth . " )";

//secho $query;

    mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_affected_rows($con)];
}

//returns true or false depending upon success or failure
// Now supports multiple inserts by specifying $values in arrays in array ...#venerable
function runSimpleInsertQuery($con, $table, $columns, $values) {

    $query = "INSERT INTO " . $table . " (";

    for ($i = 0, $countColumns = count($columns); $i < $countColumns; $i++) {
        if ($i < $countColumns - 1) {
            $query .= $columns[$i] . ", ";
        } else {
            $query .= $columns[$i] . " ";
        }
    }

    $query .= ") VALUES (";

    if (is_array($values[0])) {
        for ($i = 0, $c = count($values); $i < $c; $i++) {

            for ($j = 0, $countValues = count($values[$i]); $j < $countValues; $j++) {
                if ($j < $countValues - 1) {
                    $query .= $values[$i][$j] . ", ";
                } else {
                    $query .= $values[$i][$j] . "";
                }
            }

            if ($i < $c - 1) {
                $query .= "), (";
            }
        }
    } else {
        for ($i = 0, $countValues = count($values); $i < $countValues; $i++) {
            if ($i < $countValues - 1) {
                $query .= $values[$i] . ", ";
            } else {
                $query .= $values[$i] . " ";
            }
        }
    }

    $query .= ") ";
    
//    echo $query;

    mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_affected_rows($con), 'insertedId' => mysqli_insert_id($con)];
}

//Zenith d optimiser: The function below is obsolete and redundant, use runSimpleInsertQuery instead as it also returns the inserted id
//returns ID of inserted record
function runSimpleInsertQueryWithIDReturn($con, $table, $columns, $values) {

    $query = "INSERT INTO " . $table . " (";

    for ($i = 0, $countColumns = count($columns); $i < $countColumns; $i++) {
        if ($i < $countColumns - 1) {
            $query .= $columns[$i] . ", ";
        } else {
            $query .= $columns[$i] . " ";
        }
    }

    $query .= ") VALUES (";

    for ($i = 0, $countValues = count($values); $i < $countValues; $i++) {
        if ($i < $countValues - 1) {
            $query .= $values[$i] . ", ";
        } else {
            $query .= $values[$i] . " ";
        }
    }

    $query .= ") ";

    mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_insert_id($con)];
}

//returns true or false depending upon success or failure
function runSimpleDeleteQuery($con, $table, $columns, $values, $limit = 1) {

    $query = "DELETE FROM " . $table;

    $query .= " WHERE ";

    for ($i = 0, $valuesCount = count($values); $i < $valuesCount; $i++) {
        if ($i == 0) {
            $query .= $columns[$i] . " = " . $values[$i] . " ";
        } else {
            $query .= "AND " . $columns[$i] . " = " . $values[$i] . " ";
        }
    }

    $limit && ($query .= ' LIMIT ' . $limit);

    mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_affected_rows($con)];
}

//returns a numerical array
function runSimpleFetchQuery($con, $columns_to_fetch, $table, $where_keys = [], $where_operators = [], $where_values = [], $group_by = NULL, $order = NULL, $limit = NULL, $offset = null) {

    $query = "SELECT ";

    if (is_array($columns_to_fetch)) {
        for ($i = 0, $countColumns_to_fetch = count($columns_to_fetch); $i < $countColumns_to_fetch; $i++) {
            if ($i < $countColumns_to_fetch - 1) {
                $query .= $columns_to_fetch[$i] . ", ";
            } else {
                $query .= $columns_to_fetch[$i] . " ";
            }
        }
    } else {
        $query .= $columns_to_fetch . " ";
    }

    if (count($where_keys) > 0) {
        $query .= "FROM " . $table . " WHERE ";
    } else {
        $query .= "FROM " . $table . ' ';
    }

    for ($i = 0, $countWhere_values = count($where_values); $i < $countWhere_values; $i++) {
        if ($i == 0) {
            $query .= $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
        } else {
            $query .= "AND " . $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
        }
    }

    if (strcmp($group_by, "") != 0) {
        $query .= "GROUP BY " . $group_by . " ";
    }

    if (strcmp($order, "") != 0) {
        $query .= "ORDER BY " . $order . " ";
    }

    if (strcmp($limit, "") != 0) {
        $query .= "LIMIT " . $limit . " ";
    }
    if (strcmp($offset, "") != 0) {
        $query .= "OFFSET " . $offset . " ";
    }

    $query .= ";";

//    echo $query;

    $execute = mysqli_query($con, $query);
    if ($execute) {
        $res1 = mysqli_num_rows($execute);

//Fixed a bug: $res1 = 1 instead of $res1 === 1 causes a bug
        if ($res1 === 1 && count($columns_to_fetch) == 1 && strcmp($columns_to_fetch[0], "*") != 0) {
            return ['err' => ['error' => '', 'code' => 0], 'result' => mysqli_fetch_array($execute)[0]];
        } else {
            $rows = array();
            while ($r = mysqli_fetch_array($execute, MYSQLI_ASSOC)) {
                $rows[] = $r;
            }
            return ['err' => ['error' => '', 'code' => 0], 'result' => $rows];
        }
    } else {
        return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => $execute];
    }
}

//returns a numerical array
function runSimpleFetchORQuery($con, $columns_to_fetch, $table, $where_keys, $where_operators, $where_values, $group_by, $order, $limit, $offset = null) {

    $query = "SELECT ";

    if (is_array($columns_to_fetch)) {
        for ($i = 0, $countColumns_to_fetch = count($columns_to_fetch); $i < $countColumns_to_fetch; $i++) {
            if ($i < $countColumns_to_fetch - 1) {
                $query .= $columns_to_fetch[$i] . ", ";
            } else {
                $query .= $columns_to_fetch[$i] . " ";
            }
        }
    } else {
        $query .= $columns_to_fetch . " ";
    }

    if (count($where_keys) > 0) {
        $query .= "FROM " . $table . " WHERE ";
    } else {
        $query .= "FROM " . $table;
    }

    for ($i = 0, $countWhere_values = count($where_values); $i < $countWhere_values; $i++) {
        if ($i == 0) {
            $query .= $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
        } else {
            $query .= "OR " . $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
        }
    }

    if (strcmp($group_by, "") != 0) {
        $query .= "GROUP BY " . $group_by . " ";
    }

    if (strcmp($order, "") != 0) {
        $query .= "ORDER BY " . $order . " ";
    }

    if (strcmp($limit, "") != 0) {
        $query .= "LIMIT " . $limit . " ";
    }
    if (strcmp($offset, "") != 0) {
        $query .= "OFFSET " . $offset . " ";
    }

    $query .= ";";

//    echo $query;

    $execute = mysqli_query($con, $query);
    if ($execute) {
        $res1 = mysqli_num_rows($execute);

//Fixed a bug: $res1 = 1 instead of $res1 === 1 causes a bug
        if ($res1 === 1 && count($columns_to_fetch) == 1 && strcmp($columns_to_fetch[0], "*") != 0) {
            return ['err' => ['error' => '', 'code' => 0], 'result' => mysqli_fetch_array($execute)[0]];
        } else {
            $rows = array();
            while ($r = mysqli_fetch_array($execute, MYSQLI_ASSOC)) {
                $rows[] = $r;
            }
            return ['err' => ['error' => '', 'code' => 0], 'result' => $rows];
        }
    } else {
        return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => $execute];
    }
}

// returns a numerical array
function runJointFetchQuery($con, $columns_to_fetch, $tables, $connectors, $where_keys, $where_operators, $where_values, $group_by, $order, $limit, $offset = null) {

    $query = "SELECT ";

    if (is_array($columns_to_fetch)) {
        for ($i = 0; $i < count($columns_to_fetch); $i++) {
            if ($i < count($columns_to_fetch) - 1) {
                $query .= $columns_to_fetch[$i] . ", ";
            } else {
                $query .= $columns_to_fetch[$i] . " FROM ";
            }
        }
    } else {
        $query .= $columns_to_fetch . " FROM ";
    }

    for ($n = 0; $n < count($tables); $n++) {

        if ($n < count($tables) - 1) {
            $query .= '(' . $tables[$n] . ' RIGHT JOIN ';
        } else {
            $query .= $tables[$n] . ' ';
        }
    }


    for ($n = (count($connectors) - 1); $n >= 0; $n--) {

        $connector_group = $connectors[$n];

        for ($c = 0; $c < count($connector_group); $c++) {
            $connector = $connector_group[$c];
            $param_1 = $connector[0];
            $param_2 = $connector[1];
            $compa = $connector[2];

            if ($c < count($connector_group) - 1) {
                $query .= 'ON ' . $param_1 . ' ' . $compa . ' ' . $param_2 . ' AND ';
            } else {
                $query .= 'ON ' . $param_1 . ' ' . $compa . ' ' . $param_2 . ' ) ';
            }
        }
    }

    if (count($where_keys) > 0) {
        $query .= " WHERE ";
    } else {
        
    }

    for ($i = 0; $i < count($where_values); $i++) {
        if ($i == 0) {
            $query .= $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
        } else {
            $query .= "AND " . $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
        }
    }

    if (strcmp($group_by, "") != 0) {
        $query .= "GROUP BY " . $group_by . " ";
    }

    if (strcmp($order, "") != 0) {
        $query .= "ORDER BY " . $order . " ";
    }

    if (strcmp($limit, "") != 0) {
        $query .= "LIMIT " . $limit . " ";
    }

    $query .= ";";

//echo $query;

    $execute = mysqli_query($con, $query);
    if ($execute) {
        $res1 = mysqli_num_rows($execute);

//Fixed a bug: $res1 = 1 instead of $res1 === 1 causes a bug
        if ($res1 === 1 && count($columns_to_fetch) == 1 && strcmp($columns_to_fetch[0], "*") != 0) {
            return ['err' => ['error' => '', 'code' => 0], 'result' => mysqli_fetch_array($execute)[0]];
        } else {
            $rows = array();
            while ($r = mysqli_fetch_array($execute, MYSQLI_ASSOC)) {
                $rows[] = $r;
            }
            return ['err' => ['error' => '', 'code' => 0], 'result' => $rows];
        }
    } else {
        return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => $execute];
    }
}

//returns a numerical array
function runInnerJointFetchQuery($con, $columns_to_fetch, $tables, $connectors, $where_keys, $where_operators, $where_values, $group_by, $order, $limit, $offset = null) {

    $query = "SELECT ";

    if (is_array($columns_to_fetch)) {
        for ($i = 0; $i < count($columns_to_fetch); $i++) {
            if ($i < count($columns_to_fetch) - 1) {
                $query .= $columns_to_fetch[$i] . ", ";
            } else {
                $query .= $columns_to_fetch[$i] . " FROM ";
            }
        }
    } else {
        $query .= $columns_to_fetch . " FROM ";
    }

    for ($n = 0; $n < count($tables); $n++) {

        if ($n < count($tables) - 1) {
            $query .= '(' . $tables[$n] . ' INNER JOIN ';
        } else {
            $query .= $tables[$n] . ' ';
        }
    }

    for ($n = (count($connectors) - 1); $n >= 0; $n--) {

        $connector_group = $connectors[$n];

        for ($c = 0; $c < count($connector_group); $c++) {
            $connector = $connector_group[$c];
            $param_1 = $connector[0];
            $param_2 = $connector[1];
            $compa = $connector[2];

            if ($c < count($connector_group) - 1) {
                $query .= 'ON ' . $param_1 . ' ' . $compa . ' ' . $param_2 . ' AND ';
            } else {
                $query .= 'ON ' . $param_1 . ' ' . $compa . ' ' . $param_2 . ' ) ';
            }
        }
    }

    if (count($where_keys) > 0) {
        $query .= " WHERE ";
    }

    for ($i = 0; $i < count($where_values); $i++) {
        if ($i == 0) {
            $query .= $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
        } else {
            $query .= "AND " . $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
        }
    }

    if (strcmp($group_by, "") != 0) {
        $query .= "GROUP BY " . $group_by . " ";
    }

    if (strcmp($order, "") != 0) {
        $query .= "ORDER BY " . $order . " ";
    }

    if (strcmp($limit, "") != 0) {
        $query .= "LIMIT " . $limit . " ";
    }

    $query .= ";";

//echo $query;

    $execute = mysqli_query($con, $query);
    if ($execute) {
        $res1 = mysqli_num_rows($execute);

//Fixed a bug: $res1 = 1 instead of $res1 === 1 causes a bug
        if ($res1 === 1 && count($columns_to_fetch) == 1 && strcmp($columns_to_fetch[0], "*") != 0) {
            return ['err' => ['error' => '', 'code' => 0], 'result' => mysqli_fetch_array($execute)[0]];
        } else {
            $rows = array();
            while ($r = mysqli_fetch_array($execute, MYSQLI_ASSOC)) {
                $rows[] = $r;
            }
            return ['err' => ['error' => '', 'code' => 0], 'result' => $rows];
        }
    } else {
        return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => $execute];
    }
}

//returns a numerical array
function runLeftJointFetchQuery($con, $columns_to_fetch, $tables, $connectors, $where_keys, $where_operators, $where_values, $group_by, $order, $limit, $offset = null) {

    $query = "SELECT ";

    if (is_array($columns_to_fetch)) {
        for ($i = 0; $i < count($columns_to_fetch); $i++) {
            if ($i < count($columns_to_fetch) - 1) {
                $query .= $columns_to_fetch[$i] . ", ";
            } else {
                $query .= $columns_to_fetch[$i] . " FROM ";
            }
        }
    } else {
        $query .= $columns_to_fetch . " FROM ";
    }

    for ($n = 0; $n < count($tables); $n++) {

        if ($n < count($tables) - 1) {
            $query .= '(' . $tables[$n] . ' LEFT JOIN ';
        } else {
            $query .= $tables[$n] . ' ';
        }
    }

    for ($n = (count($connectors) - 1); $n >= 0; $n--) {

        $connector_group = $connectors[$n];

        for ($c = 0; $c < count($connector_group); $c++) {
            $connector = $connector_group[$c];
            $param_1 = $connector[0];
            $param_2 = $connector[1];
            $compa = $connector[2];

            if ($c < count($connector_group) - 1) {
                $query .= 'ON ' . $param_1 . ' ' . $compa . ' ' . $param_2 . ' AND ';
            } else {
                $query .= 'ON ' . $param_1 . ' ' . $compa . ' ' . $param_2 . ' ) ';
            }
        }
    }

    if (count($where_keys) > 0) {
        $query .= " WHERE ";
    } else {
        
    }

    for ($i = 0; $i < count($where_values); $i++) {
        if ($i == 0) {
            $query .= $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
        } else {
            $query .= "AND " . $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
        }
    }

    if (strcmp($group_by, "") != 0) {
        $query .= "GROUP BY " . $group_by . " ";
    }

    if (strcmp($order, "") != 0) {
        $query .= "ORDER BY " . $order . " ";
    }

    if (strcmp($limit, "") != 0) {
        $query .= "LIMIT " . $limit . " ";
    }

    if (strcmp($offset, "") != 0) {
        $query .= "OFFSET " . $offset . " ";
    }

    $query .= ";";

    echo $query;

    $execute = mysqli_query($con, $query);
    if ($execute) {
        $res1 = mysqli_num_rows($execute);

//Fixed a bug: $res1 = 1 instead of $res1 === 1 causes a bug
        if ($res1 === 1 && count($columns_to_fetch) == 1 && strcmp($columns_to_fetch[0], "*") != 0) {
            return ['err' => ['error' => '', 'code' => 0], 'result' => mysqli_fetch_array($execute)[0]];
        } else {
            $rows = array();
            while ($r = mysqli_fetch_array($execute, MYSQLI_ASSOC)) {
                $rows[] = $r;
            }
            return ['err' => ['error' => '', 'code' => 0], 'result' => $rows];
        }
    } else {
        return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => $execute];
    }
}

//returns a numerical array
function runSerialLeftJointFetchQuery($con, $columns_to_fetch, $joinedTables, $connectors, $where_keys, $where_operators, $where_values, $group_by, $order, $limit, $offset = null) {

    $query = "SELECT ";

    if (is_array($columns_to_fetch)) {
        for ($i = 0; $i < count($columns_to_fetch); $i++) {
            if ($i < count($columns_to_fetch) - 1) {
                $query .= $columns_to_fetch[$i] . ", ";
            } else {
                $query .= $columns_to_fetch[$i] . " FROM ";
            }
        }
    } else {
        $query .= $columns_to_fetch . " FROM ";
    }

    $tablesCount = count($joinedTables);
    $connectorsCount = count($connectors[0]);
    for ($t = 0; $t < $tablesCount; $t++) {

        if ($t == 0) {
            $query .= $joinedTables[$t];
        }

        if ($t < $tablesCount - 1) {
            $query .= ' LEFT JOIN ' . $joinedTables[$t + 1] . ' ON ';
            for ($c = 0; $c < $connectorsCount; $c++) {
                $query .= $connectors[$t][$c] . ' ';
            }
        }
    }


    if (count($where_keys) > 0) {
        $query .= " WHERE ";
    }

    for ($i = 0; $i < count($where_values); $i++) {
        if ($i == 0) {
            $query .= $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
        } else {
            $query .= "AND " . $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
        }
    }

    if (strcmp($group_by, "") != 0) {
        $query .= "GROUP BY " . $group_by . " ";
    }

    if (strcmp($order, "") != 0) {
        $query .= "ORDER BY " . $order . " ";
    }

    if (strcmp($limit, "") != 0) {
        $query .= "LIMIT " . $limit . " ";
    }

    if (strcmp($offset, "") != 0) {
        $query .= "OFFSET " . $offset . " ";
    }

    $query .= ";";

//    echo $query;

    $execute = mysqli_query($con, $query);

    if ($execute) {
        $res1 = mysqli_num_rows($execute);

//Fixed a bug: $res1 = 1 instead of $res1 === 1 causes a bug
//        if ($res1 === 1 && count($columns_to_fetch) == 1 && strcmp($columns_to_fetch[0], "*") != 0) {
//            return ['err' => ['error' => '', 'code' => 0], 'result' => mysqli_fetch_array($execute)[0]];
//        } else {
        $rows = array();
        while ($r = mysqli_fetch_array($execute, MYSQLI_ASSOC)) {
            $rows[] = $r;
        }
        return ['err' => ['error' => '', 'code' => 0], 'result' => $rows];
//        }
    } else {
        return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => $execute];
    }
}

function runSerialLeftJointFetchORQuery($con, $columns_to_fetch, $joinedTables, $connectors, $where_keys, $where_operators, $where_values, $group_by, $order, $limit, $offset = null) {

    $query = "SELECT ";

    if (is_array($columns_to_fetch)) {
        for ($i = 0; $i < count($columns_to_fetch); $i++) {
            if ($i < count($columns_to_fetch) - 1) {
                $query .= $columns_to_fetch[$i] . ", ";
            } else {
                $query .= $columns_to_fetch[$i] . " FROM ";
            }
        }
    } else {
        $query .= $columns_to_fetch . " FROM ";
    }

    $tablesCount = count($joinedTables);
    $connectorsCount = count($connectors[0]);
    for ($t = 0; $t < $tablesCount; $t++) {

        if ($t == 0) {
            $query .= $joinedTables[$t];
        }

        if ($t < $tablesCount - 1) {
            $query .= ' LEFT JOIN ' . $joinedTables[$t + 1] . ' ON ';
            for ($c = 0; $c < $connectorsCount; $c++) {
                $query .= $connectors[$t][$c] . ' ';
            }
        }
    }


    if (count($where_keys) > 0) {
        $query .= " WHERE ";
    }

    for ($i = 0; $i < count($where_values); $i++) {
        if ($i == 0) {
            $query .= $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
        } else {
            $query .= "OR " . $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
        }
    }

    if (strcmp($group_by, "") != 0) {
        $query .= "GROUP BY " . $group_by . " ";
    }

    if (strcmp($order, "") != 0) {
        $query .= "ORDER BY " . $order . " ";
    }

    if (strcmp($limit, "") != 0) {
        $query .= "LIMIT " . $limit . " ";
    }

    if (strcmp($offset, "") != 0) {
        $query .= "OFFSET " . $offset . " ";
    }

    $query .= ";";

//    echo $query;

    $execute = mysqli_query($con, $query);
    if ($execute) {
        $res1 = mysqli_num_rows($execute);

//Fixed a bug: $res1 = 1 instead of $res1 === 1 causes a bug
        if ($res1 === 1 && count($columns_to_fetch) == 1 && strcmp($columns_to_fetch[0], "*") != 0) {
            return ['err' => ['error' => '', 'code' => 0], 'result' => mysqli_fetch_array($execute)[0]];
        } else {
            $rows = array();
            while ($r = mysqli_fetch_array($execute, MYSQLI_ASSOC)) {
                $rows[] = $r;
            }
            return ['err' => ['error' => '', 'code' => 0], 'result' => $rows];
        }
    } else {
        return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => $execute];
    }
}

//returns a numerical array
function runUnionFetchQuery($con, $columns_to_fetch, $tables, $where_keys, $where_operators, $where_values, $group_by, $order, $limit, $offset = null) {

    $query = "";

    for ($t = 0; $t < count($tables); $t++) {

        $table = $tables[$t];

        $query .= "(SELECT ";

        if (is_array($columns_to_fetch)) {
            for ($i = 0; $i < count($columns_to_fetch); $i++) {
                if ($i < count($columns_to_fetch) - 1) {
                    $query .= $columns_to_fetch[$i] . ", ";
                } else {
                    $query .= $columns_to_fetch[$i] . " ";
                }
            }
        } else {
            $query .= $columns_to_fetch . " ";
        }

        if (count($where_keys) > 0) {
            $query .= "FROM " . $table . " WHERE ";
        } else {
            $query .= "FROM " . $table;
        }

        for ($i = 0; $i < count($where_values); $i++) {
            if ($i == 0) {
                $query .= $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
            } else {
                $query .= "AND " . $where_keys[$i] . $where_operators[$i] . $where_values[$i] . " ";
            }
        }

        if (strcmp($group_by, "") != 0) {
            $query .= "GROUP BY " . $group_by . " ";
        }

        if (strcmp($order, "") != 0) {
            $query .= "ORDER BY " . $order . " ";
        }

        if (strcmp($limit, "") != 0) {
            $query .= "LIMIT " . $limit . " ";
        }
        if (strcmp($offset, "") != 0) {
            $query .= "OFFSET " . $offset . " ";
        }


        $query .= ") ";

        if ($t < count($tables) - 1) {
            $query .= "UNION ";
        }
    }
    $query .= ";";
    echo $query;

    $execute = mysqli_query($con, $query);
    if ($execute) {
        $res1 = mysqli_num_rows($execute);

//Fixed a bug: $res1 = 1 instead of $res1 === 1 causes a bug
        if ($res1 === 1 && count($columns_to_fetch) == 1 && strcmp($columns_to_fetch[0], "*") != 0) {
            return ['err' => ['error' => '', 'code' => 0], 'result' => mysqli_fetch_array($execute)[0]];
        } else {
            $rows = array();
            while ($r = mysqli_fetch_array($execute, MYSQLI_ASSOC)) {
                $rows[] = $r;
            }
            return ['err' => ['error' => '', 'code' => 0], 'result' => $rows];
        }
    } else {
        return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => $execute];
    }
}

function runFetchUserTransactions($con, $userid, $order, $limit, $offset = null) { // for user transactions in user portal
    $query = 'SELECT * FROM transactions '
            . 'WHERE (to_who = ' . $userid . ' AND type_id = 1) '
            . 'OR (from_whom = ' . $userid . ' AND type_id = 2) '
            . 'OR (from_whom = ' . $userid . ' AND type_id = 3) '
            . 'OR (from_whom = ' . $userid . ' AND type_id = 4) '
            . 'OR (to_who = ' . $userid . ' AND type_id = 4) '
            . 'OR (to_who = ' . $userid . ' AND type_id = 7) '
            . 'OR (from_whom = ' . $userid . ' AND type_id = 9) '
            . 'OR (from_whom = ' . $userid . ' AND type_id = 10) '
            . 'OR (to_who = ' . $userid . ' AND type_id = 14) ';
    if (strcmp($order, "") != 0) {
        $query .= "ORDER BY " . $order . " ";
    }
    if (strcmp($limit, "") != 0) {
        $query .= "LIMIT " . $limit . " ";
    }
    if (strcmp($offset, "") != 0) {
        $query .= "OFFSET " . $offset . " ";
    }
    $query .= ";";

    $result = mysqli_query($con, $query);
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

/* * ******************** for admin portal ********************************* */

function runGetTransactions_user($con, $trans_type = null, $from_whom = null, $to_who = null, $startDate = null, $endDate = null, $limit = null, $offset = null) { // for admin portal....
    $query = 'SELECT transactions.id,
                transactions.reference,
                transactions.point_worth as points,
                transactions.date_transacted as date,
                transactions.type_id,
                transactions.from_whom,
                transactions.to_who,
                transaction_types.meta as type
            FROM
                transactions
            LEFT JOIN
                transaction_types ON transactions.type_id = transaction_types.id 
            WHERE  1 ';

    if (empty($trans_type)) {
        $query .= 'AND (transactions.type_id = 1 OR 
                   transactions.type_id = 2 OR 
                   transactions.type_id = 3 OR 
                   transactions.type_id = 4 OR 
                   transactions.type_id = 7 OR 
                   transactions.type_id = 9 OR 
                   transactions.type_id = 10) ';
    } else {
        $query .= "AND transactions.type_id = $trans_type ";
    }
    if (!empty($from_whom)) {
        $query .= "AND transactions.from_whom = $from_whom ";
    }
    if (!empty($to_who)) {
        $query .= "AND transactions.to_who =  $to_who ";
    }
    if (!empty($startDate)) {
        $query .= "AND transactions.date_transacted >= '$startDate'";
    }
    if (!empty($endDate)) {
        $query .= "AND transactions.date_transacted <= '$endDate'";
    }

    $query .= 'ORDER BY transactions.id DESC ';

    if (!empty($limit)) {
        $query .= "LIMIT " . $limit . " ";
    }
    if (!empty($limit) && !empty($offset)) {
        $query .= "OFFSET " . $offset . " ";
    }

    $query .= ";";

//    echo $query;

    $result = mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runGetTransactions_agent($con, $trans_type = null, $from_whom = null, $to_who = null, $startDate = null, $endDate = null, $limit = null, $offset = null) { // for admin portal....
    $query = 'SELECT point_transfers.id,
                point_transfers.reference,
                point_transfers.point_worth as points,
                point_transfers.date_transacted as date,
                point_transfers.type_id,
                point_transfers.from_whom,
                point_transfers.to_who,
                transaction_types.meta as type 
            FROM 
                point_transfers 
            LEFT JOIN
                transaction_types ON point_transfers.type_id = transaction_types.id 
            WHERE  1 ';

    if (!empty($trans_type)) {
        $query .= "AND point_transfers.type_id = $trans_type ";
    }
    if (!empty($from_whom)) {
        $query .= "AND point_transfers.from_whom = $from_whom ";
    }
    if (!empty($to_who)) {
        $query .= "AND point_transfers.to_who =  $to_who ";
    }
    if (!empty($startDate)) {
        $query .= "AND point_transfers.date_transacted >= '$startDate'";
    }
    if (!empty($endDate)) {
        $query .= "AND point_transfers.date_transacted <= '$endDate'";
    }

    $query .= 'ORDER BY point_transfers.id DESC ';

    if (!empty($limit)) {
        $query .= "LIMIT " . $limit . " ";
    }
    if (!empty($limit) && !empty($offset)) {
        $query .= "OFFSET " . $offset . " ";
    }

    $query .= ";";

//    echo $query;

    $result = mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runGetTransactions_merchant($con, $trans_type, $from_whom = null, $to_who = null, $startDate = null, $endDate = null, $limit = null, $offset = null) { // for admin portal....
    $query = null;

    if ($trans_type == '5') {
        $query = 'SELECT point_transfers.id,
                point_transfers.reference,
                point_transfers.point_worth as points,
                point_transfers.date_transacted as date,
                point_transfers.type_id,
                point_transfers.from_whom,
                point_transfers.to_who,
                transaction_types.meta as type 
            FROM 
                point_transfers 
            LEFT JOIN 
                transaction_types ON point_transfers.type_id = transaction_types.id 
            WHERE point_transfers.type_id = 5 ';

        if (!empty($from_whom)) {
            $query .= "AND point_transfers.from_whom = $from_whom ";
        }
        if (!empty($to_who)) {
            $query .= "AND point_transfers.to_who =  $to_who ";
        }
        if (!empty($startDate)) {
            $query .= "AND point_transfers.date_transacted >= '$startDate' ";
        }
        if (!empty($endDate)) {
            $query .= "AND point_transfers.date_transacted <= '$endDate' ";
        }

        $query .= 'ORDER BY point_transfers.id DESC ';
    } else {
        $query = 'SELECT point_purchases.id,
                point_purchases.reference,
                point_purchases.point_amount as points,
                point_purchases.date_bought as date,
                point_purchases.type_id,  
                merchants.name, 
                transaction_types.meta as type 
            FROM 
                point_purchases 
            LEFT JOIN 
                merchants ON point_purchases.merchant_id = merchants.id 
            LEFT JOIN 
                transaction_types ON transaction_types.id = point_purchases.type_id 
            WHERE 1 ';

        if (!empty($from_whom)) {
            $query .= "AND point_purchases.merchant_id = $from_whom ";
        }
        if (!empty($startDate)) {
            $query .= "AND point_purchases.date_bought >= '$startDate' ";
        }
        if (!empty($endDate)) {
            $query .= "AND point_purchases.date_bought <= '$endDate' ";
        }

        $query .= 'ORDER BY point_purchases.id DESC ';
    }

    if (!empty($limit)) {
        $query .= "LIMIT " . $limit . " ";
    }
    if (!empty($limit) && !empty($offset)) {
        $query .= "OFFSET " . $offset . " ";
    }

    $query .= ";";

//    echo $query;

    $result = mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runGetTransactions_insurer($con, $trans_type = null, $from_whom = null, $to_who = null, $startDate = null, $endDate = null, $limit = null, $offset = null) { // for admin portal....
    $query = 'SELECT transactions.id,
                transactions.reference,
                transactions.point_worth as points,
                transactions.date_transacted as date,
                transactions.type_id,
                transactions.from_whom,
                transactions.to_who,
                transaction_types.meta as type
            FROM
                transactions
            LEFT JOIN
                transaction_types ON transactions.type_id = transaction_types.id 
            WHERE  1 ';

    if (empty($trans_type)) {
        $query .= 'AND (transactions.type_id = 2 OR 
                   transactions.type_id = 3) ';
    } else {
        $query .= "AND transactions.type_id = $trans_type ";
    }
    if (!empty($from_whom)) {
        $query .= "AND transactions.from_whom = $from_whom ";
    }
    if (!empty($to_who)) {
        $query .= "AND transactions.to_who =  $to_who ";
    }
    if (!empty($startDate)) {
        $query .= "AND transactions.date_transacted >= '$startDate'";
    }
    if (!empty($endDate)) {
        $query .= "AND transactions.date_transacted <= '$endDate'";
    }

    $query .= 'ORDER BY transactions.id DESC ';

    if (!empty($limit)) {
        $query .= "LIMIT " . $limit . " ";
    }
    if (!empty($limit) && !empty($offset)) {
        $query .= "OFFSET " . $offset . " ";
    }

    $query .= ";";

//    echo $query;

    $result = mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runGetTransactions_insurer_sms($con, $to_who = null, $startDate = null, $endDate = null, $limit = null, $offset = null) { // for admin portal....
    $query = 'SELECT sms_transactions.id,
                sms_transactions.ref as reference,
                sms_transactions.point_worth as points,
                sms_transactions.date_transacted as date,
                sms_transactions.insurer_id,
                sms_transactions.previous_balance,
                insurer.name as insurer_name
            FROM
                sms_transactions
            LEFT JOIN
                insurer ON sms_transactions.insurer_id = insurer.id 
            WHERE  1 ';

    if (!empty($to_who)) {
        $query .= "AND sms_transactions.insurer_id =  $to_who ";
    }
    if (!empty($startDate)) {
        $query .= "AND sms_transactions.date_transacted >= '$startDate'";
    }
    if (!empty($endDate)) {
        $query .= "AND sms_transactions.date_transacted <= '$endDate'";
    }

    $query .= 'ORDER BY sms_transactions.id DESC ';

    if (!empty($limit)) {
        $query .= "LIMIT " . $limit . " ";
    }
    if (!empty($limit) && !empty($offset)) {
        $query .= "OFFSET " . $offset . " ";
    }

    $query .= ";";

//    echo $query;

    $result = mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runGetTransactions_pension($con, $from_whom = null, $to_who = null, $startDate = null, $endDate = null, $limit = null, $offset = null) { // for admin portal....
    $query = 'SELECT transactions.id,
                transactions.reference,
                transactions.point_worth as points,
                transactions.date_transacted as date,
                transactions.type_id,
                transactions.from_whom,
                transactions.to_who,
                transaction_types.meta as type
            FROM
                transactions
            LEFT JOIN
                transaction_types ON transactions.type_id = transaction_types.id 
            WHERE transactions.type_id = 10 ';

    if (!empty($from_whom)) {
        $query .= "AND transactions.from_whom = $from_whom ";
    }
    if (!empty($to_who)) {
        $query .= "AND transactions.to_who =  $to_who ";
    }
    if (!empty($startDate)) {
        $query .= "AND transactions.date_transacted >= '$startDate'";
    }
    if (!empty($endDate)) {
        $query .= "AND transactions.date_transacted <= '$endDate'";
    }

    $query .= 'ORDER BY transactions.id DESC ';

    if (!empty($limit)) {
        $query .= "LIMIT " . $limit . " ";
    }
    if (!empty($limit) && !empty($offset)) {
        $query .= "OFFSET " . $offset . " ";
    }

    $query .= ";";

//    echo $query;

    $result = mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runGetTransactions_ecommerce($con, $from_whom = null, $to_who = null, $startDate = null, $endDate = null, $limit = null, $offset = null) { // for admin portal....
    $query = 'SELECT transactions.id,
                transactions.reference,
                transactions.point_worth as points,
                transactions.date_transacted as date,
                transactions.type_id,
                transactions.from_whom,
                transactions.to_who,
                transaction_types.meta as type
            FROM
                transactions
            LEFT JOIN
                transaction_types ON transactions.type_id = transaction_types.id 
            WHERE transactions.type_id = 9 ';

    if (!empty($from_whom)) {
        $query .= "AND transactions.from_whom = $from_whom ";
    }
    if (!empty($to_who)) {
        $query .= "AND transactions.to_who =  $to_who ";
    }
    if (!empty($startDate)) {
        $query .= "AND transactions.date_transacted >= '$startDate'";
    }
    if (!empty($endDate)) {
        $query .= "AND transactions.date_transacted <= '$endDate'";
    }

    $query .= 'ORDER BY transactions.id DESC ';

    if (!empty($limit)) {
        $query .= "LIMIT " . $limit . " ";
    }
    if (!empty($limit) && !empty($offset)) {
        $query .= "OFFSET " . $offset . " ";
    }

    $query .= ";";

//    echo $query;

    $result = mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

// Transaction search //
function runTransactionsSearch_user_from($con, $where_value, $limit = 10) { // for admin user transactions search
    $query = "SELECT 
                    id, concat(phone,' (', f_name,' ', l_name , ')') as label
                FROM
                    users WHERE phone LIKE '$where_value%' 
                UNION ALL SELECT 
                    id, concat(phone,' (', f_name,' ', l_name , ')') as label
                FROM
                    agent WHERE phone LIKE '$where_value%' 
                UNION ALL SELECT 
                    id, name as label
                FROM
                    merchants WHERE name LIKE '$where_value%' 
                LIMIT $limit";

    $result = mysqli_query($con, $query);
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runTransactionsSearch_user_to($con, $where_value, $limit = 10) { // for admin user transactions search
    $query = "SELECT 
                    id, concat(phone,' (', f_name,' ', l_name , ')') as label
                FROM
                    users WHERE phone LIKE '$where_value%' 
                UNION ALL SELECT 
                    id, name as label
                FROM
                    insurer WHERE name LIKE '$where_value%' 
                UNION ALL SELECT 
                    id, name as label
                FROM
                    pension WHERE name LIKE '$where_value%' 
                        UNION ALL SELECT 
                    id, name as label
                FROM
                    e_commerce WHERE name LIKE '$where_value%' 
                LIMIT $limit";

    $result = mysqli_query($con, $query);
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runTransactionsSearch_agent_from($con, $where_value, $limit = 10) { // for admin user transactions search
    $query = "SELECT 
                    id, concat(phone,' (', f_name,' ', l_name , ')') as label
                FROM
                    agent WHERE phone LIKE '$where_value%' 
                UNION ALL SELECT 
                    id, name as label
                FROM
                    merchants WHERE name LIKE '$where_value%' 
                LIMIT $limit";


    $result = mysqli_query($con, $query);
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runTransactionsSearch_agent_to($con, $where_value, $limit = 10) { // for admin user transactions search
    $query = "SELECT 
                    phone as id, concat(phone,' (', f_name,' ', l_name , ')') as label
                FROM
                    users WHERE phone LIKE '$where_value%' 
                UNION ALL SELECT 
                    id, concat(phone,' (', f_name,' ', l_name , ')') as label
                FROM
                    agent WHERE phone LIKE '$where_value%' 
                LIMIT $limit";


    $result = mysqli_query($con, $query);
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runTransactionsSearch_IPS_from($con, $where_value, $limit = 10) { // for admin user transactions search
    $query = "SELECT 
                    id, concat(phone,' (', f_name,' ', l_name , ')') as label
                FROM
                    users WHERE phone LIKE '$where_value%' 
                LIMIT $limit";

    $result = mysqli_query($con, $query);
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runTransactionsSearch_insurer_to($con, $where_value, $limit = 10) { // for admin user transactions search
    $query = "SELECT 
                    id, name as label
                FROM
                    insurer WHERE name LIKE '$where_value%'  
                LIMIT $limit";

    $result = mysqli_query($con, $query);
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runTransactionsSearch_pension_to($con, $where_value, $limit = 10) { // for admin user transactions search
    $query = "SELECT 
                    id, name as label
                FROM
                    pension WHERE name LIKE '$where_value%'  
                LIMIT $limit";

    $result = mysqli_query($con, $query);
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runTransactionsSearch_ecommerce_to($con, $where_value, $limit = 10) { // for admin user transactions search
    $query = "SELECT 
                    id, name as label
                FROM
                    e_commerce WHERE name LIKE '$where_value%'  
                LIMIT $limit";

    $result = mysqli_query($con, $query);
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runTransactionsSearch_merchant_from($con, $where_value, $limit = 10) { // for admin user transactions search
    $query = "SELECT 
                    id, name as label
                FROM
                    merchants WHERE name LIKE '$where_value%' 
                LIMIT $limit";


    $result = mysqli_query($con, $query);
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runTransactionsSearch_merchant_to($con, $where_value, $limit = 10) { // for admin user transactions search
    $query = "SELECT 
                    id, concat(phone,' (', f_name,' ', l_name , ')') as label
                FROM
                    agent WHERE phone LIKE '$where_value%' 
                LIMIT $limit";


    $result = mysqli_query($con, $query);
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

/* * ***************************************************************** */

function runQuerySearch($con, $entity, $queryString, $limit = null, $offset = null) { // for retrieving entities for admin (notifications)
    $query = null;

    if ($entity == 'users' || $entity == 'agent') {
        $query = "SELECT 
                    id, f_name, l_name, email, phone 
                FROM 
                    $entity WHERE f_name LIKE '$queryString%' OR l_name LIKE '$queryString%' ";
    } else if ($entity == 'merchants') {
        $query = "SELECT 
                    id, name, contact_email as email, contact_phone as phone 
                FROM 
                    $entity WHERE name LIKE '$queryString%' ";
    } else {
        $query = "SELECT 
                    id, name, email, phone 
                FROM 
                    $entity WHERE name LIKE '$queryString%' ";
    }

    if (strcmp($limit, "") != 0) {
        $query .= "LIMIT $limit ";
    }
    if (strcmp($offset, "") != 0) {
        $query .= "OFFSET $offset ";
    }

    $result = mysqli_query($con, $query);
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

/* * ************************   Merchants    ************************************** */

function runGetPointUsage($con, $point_worth = null, $from_whom = null, $to_who = null, $gender = null, $location = null, $startDate = null, $endDate = null, $limit = null, $offset = null) { // for merchant portal....
    $query = null;

    $query = 'SELECT point_transfers.*  
            FROM 
                point_transfers 
            LEFT JOIN 
                users ON users.phone = point_transfers.to_who 
            WHERE point_transfers.type_id = 12 ';

    if (!empty($point_worth)) {
        $query .= "AND point_transfers.point_worth = $point_worth ";
    }
    if (!empty($from_whom)) {
        $query .= "AND point_transfers.from_whom = $from_whom ";
    }
    if (!empty($to_who)) {
        $query .= "AND point_transfers.to_who =  $to_who ";
    }
    if (!empty($gender)) {
        $query .= "AND users.gender =  '$gender' ";
    }
    if (!empty($location)) {
        $query .= "AND users.local_gov =  '$location' ";
    }
    if (!empty($startDate)) {
        $query .= "AND point_transfers.date_transacted >= '$startDate' ";
    }
    if (!empty($endDate)) {
        $query .= "AND point_transfers.date_transacted <= '$endDate' ";
    }

    $query .= 'ORDER BY point_transfers.id DESC ';


    if (!empty($limit)) {
        $query .= "LIMIT " . $limit . " ";
    }
    if (!empty($limit) && !empty($offset)) {
        $query .= "OFFSET " . $offset . " ";
    }

    $query .= ";";

//    echo $query;

    $result = mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runGetPointTransfer($con, $transtype = null, $point_worth = null, $from_whom = null, $to_who = null, $startDate = null, $endDate = null, $limit = null, $offset = null) { // for merchant portal....
    $query = null;

    $query = "SELECT point_transfers.*
            FROM 
                point_transfers ";

    if (empty($transtype)) {
        $query .= " WHERE (point_transfers.type_id = 5 OR point_transfers.type_id = 13) ";
    } else {
        $query .= " WHERE point_transfers.type_id = $transtype ";
    }

    if (!empty($point_worth)) {
        $query .= "AND point_transfers.point_worth = $point_worth ";
    }
    if (!empty($from_whom)) {
        $query .= "AND point_transfers.from_whom = $from_whom ";
    }
    if (!empty($to_who)) {
        $query .= "AND point_transfers.to_who =  $to_who ";
    }
    if (!empty($startDate)) {
        $query .= "AND point_transfers.date_transacted >= '$startDate' ";
    }
    if (!empty($endDate)) {
        $query .= "AND point_transfers.date_transacted <= '$endDate' ";
    }

    $query .= 'ORDER BY point_transfers.id DESC ';


    if (!empty($limit)) {
        $query .= "LIMIT " . $limit . " ";
    }
    if (!empty($limit) && !empty($offset)) {
        $query .= "OFFSET " . $offset . " ";
    }

    $query .= ";";

//    echo $query;

    $result = mysqli_query($con, $query);

    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runGetUserBasicPolicies($con, $where_value) {
    $query = "SELECT
                    user_signups.id,
                    user_signups.user_id,
                    user_signups.policy_id,
                    user_signups.date_signed,
                    user_signups.active,
                    user_signups.date_withdrawn
                    FROM
                    user_signups
                    WHERE
                    exists ( SELECT 1 FROM policies WHERE insurer_id = 0 AND policies.id = user_signups.policy_id )
                        and user_id = " . $where_value;

    $result = mysqli_query($con, $query);
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

function runGetAllUsersWithoutBasicPolicies($con, $health_price, $general_price) {
    $query = "SELECT
                users.id,
                users.f_name,
                users.l_name,
                users.dob,
                users.gender,
                users.phone,
                users.email,
                users.address,
                users.local_gov,
                users.health_point_balance,
                users.general_point_balance
                    FROM
                            users 
                    WHERE
                            NOT EXISTS (
                    SELECT
                            1 
                    FROM
                            user_signups
                            JOIN policies ON user_signups.policy_id = policies.id 
                    WHERE
                            user_signups.user_id = users.id 
                            AND policies.insurer_id = 0 
                            ) 
                            AND (health_point_balance > " . $health_price .
                            " OR general_point_balance > " . $general_price . ");";
//    echo $query;

    $result = mysqli_query($con, $query);
    return ['err' => ['error' => mysqli_error($con), 'code' => mysqli_errno($con)], 'result' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
}

?>
