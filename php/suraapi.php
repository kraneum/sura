<?php

if (isset($_POST['command'])) {

    if (strcmp($_POST['command'], "createinsurer") == 0) {
        if (isset($_POST['packet'])) {
            $packet = $_POST['packet'];
        }

        $ch = curl_init("http://localhost/iinsurance/php/suraCommands.php?command=1&packet=" . json_encode($packet));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $contents = curl_exec($ch);
        echo $contents;
        curl_close($ch);
    }

    if (strcmp($_POST['command'], "getmypolicies") == 0) {
        if (isset($_POST['user_id'])) {
            $packet = $_POST['user_id'];
        }
        $ch = curl_init("http://localhost/iinsurance/php/suraCommands.php?command=2&user_id=$packet&pre=true");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $contents = curl_exec($ch);
        echo $contents;
        curl_close($ch);
    }

    if (strcmp($_POST['command'], "getmyredemptions") == 0) {
        if (isset($_POST['user_id'])) {
            $packet = $_POST['user_id'];
        }
        $ch = curl_init("http://localhost/iinsurance/php/suraCommands.php?command=3&user_id=$packet&pre=true");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $contents = curl_exec($ch);
        echo $contents;
        curl_close($ch);
    }

    if (strcmp($_POST['command'], "getCandP") == 0) {
        $ch = curl_init("http://localhost/iinsurance/php/suraCommands.php?command=4");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $contents = curl_exec($ch);
        echo $contents;
        curl_close($ch);
    }
    if (strcmp($_POST['command'], "getpaymentstructures") == 0) {
        $ch = curl_init("http://localhost/iinsurance/php/suraCommands.php?command=5");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $contents = curl_exec($ch);
        echo $contents;
        curl_close($ch);
    }

    if (strcmp($_POST['command'], "redeemcode") == 0) {
        if (isset($_POST['user_id']) && isset($_POST['code'])) {
            $user_id = $_POST['user_id'];
            $code = $_POST['code'];
        }
        $ch = curl_init("http://localhost/iinsurance/php/suraCommands.php?command=6&user_id=$user_id&code=$code");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $contents = curl_exec($ch);
        echo $contents;
        curl_close($ch);
    }
    if (strcmp($_POST['command'], "getinsurancetypes") == 0) {
        $ch = curl_init("http://localhost/iinsurance/php/suraCommands.php?command=7&pre=true");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $contents = curl_exec($ch);
        echo $contents;
        curl_close($ch);
    }

    if (strcmp($_POST['command'], "getpoliciesbymeta") == 0) {
        if (isset($_POST['keys']) && isset($_POST['comps']) && isset($_POST['vals'])) {
            $keys = $_POST['keys'];
            $comps = $_POST['comps'];
            $vals = $_POST['vals'];
        }
        $ch = curl_init("http://localhost/iinsurance/php/suraCommands.php?command=8&&keys=$keys&&comps=$comps&&vals=$vals");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $contents = curl_exec($ch);
        echo $contents;
        curl_close($ch);
    }

    if (strcmp($_POST['command'], "setpolicyterms") == 0) {
        if (isset($_POST['url']) && isset($_POST['policy_id'])) {
            $url = $_POST['url'];
            $code = $_POST['policy_di'];
        }
        $ch = curl_init("http://localhost/iinsurance/php/suraCommands.php?command=9&url=$url&code=$code");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $contents = curl_exec($ch);
        echo $contents;
        curl_close($ch);
    }

    if (strcmp($_POST['command'], "setpolicyimage") == 0) {
        if (isset($_POST['url']) && isset($_POST['policy_id'])) {
            $url = $_POST['url'];
            $policy_id = $_POST['policy_id'];
        }
        $ch = curl_init("http://localhost/iinsurance/php/suraCommands.php?command=10&url=$url&policy_id=$policy_id");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $contents = curl_exec($ch);
        echo $contents;
        curl_close($ch);
    }

    if (strcmp($_POST['command'], "createuser") == 0) {
        if (isset($_POST['packet'])) {
            $packet = $_POST['packet'];
        }

        $ch = curl_init("http://localhost/iinsurance/php/suraCommands.php?command=11&packet=" . json_encode($packet));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_exec($ch);
        curl_close($ch);
    }

    if (strcmp($_POST['command'], "createmerchant") == 0) {
        if (isset($_POST['packet'])) {
            $packet = $_POST['packet'];
        }
        $ch = curl_init("http://localhost/iinsurance/php/suraCommands.php?command=12&packet=" . json_encode($packet));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_exec($ch);
        curl_close($ch);
    }

    if (strcmp($_POST['command'], "createinspolicy") == 0) {
        if (isset($_POST['packet'])) {
            $packet = $_POST['packet'];
        }

        $ch = curl_init("http://localhost/iinsurance/php/suraCommands.php?command=13&packet=" . json_encode($packet));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_exec($ch);
        curl_close($ch);
    }

    if (strcmp($_POST['command'], "createusersignup") == 0) {
        if (isset($_POST['url']) && isset($_POST['policy_id'])) {
            $user_id = $_POST['url'];
            $code = $_POST['policy_id'];
        }
        $ch = curl_init("http://localhost/iinsurance/php/suraCommands.php?command=14");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $contents = curl_exec($ch);
        echo $contents;
        curl_close($ch);
    }

    if (strcmp($_POST['command'], "createussdcodes") == 0) {
        if (isset($_POST['points_worth']) && isset($_POST['quantities'])) {
            $pw = $_POST['points_worth'];
            $quan = $_POST['quantities'];
        }
        $ch = curl_init("http://localhost/iinsurance/php/suraCommands.php?command=15&points_worth=$pw&quantities=$quan");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $contents = curl_exec($ch);
        echo $contents;
        curl_close($ch);
    }

    if (strcmp($_POST['command'], "buypointsmerchant") == 0) {
        if (isset($_POST['merchant_id']) && isset($_POST['point_worth'])) {
            $user_id = $_POST['merchant_id'];
            $code = $_POST['point_worth'];
        }
        $ch = curl_init("http://localhost/iinsurance/php/suraCommands.php?command=16");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $contents = curl_exec($ch);
        echo $contents;
        curl_close($ch);
    }
}

?>