<?php

if (isset($_GET['command'])) {
    if (file_exists('sura_config.php') && file_exists('sura_functions.php')) {
        require_once('sura_config.php');
        require_once('sura_functions.php');

        $params = [];

        $command = basename($_GET['command']);

        if (file_exists('./suracommands/content_mgt/' . $command . '.php')) {
            require_once './suracommands/content_mgt/' . $command . '.php';

            $response = call_user_func_array($command, $params);
            send_response(200, "Success", $response);
        } else { // when wrong command is sent ...
            send_response(405, "Invalid Command ", NULL);
        }
    } else { // couldn't find sura_config of sura_functions...
        send_response(500, "Internal Server Error", NULL);
    }
} else { // when command is not set...
    send_response(400, "Invalid Request", NULL);
}

function send_response($status, $status_message, $data) {
    header("HTTP/1.1 $status $status_message");
    echo json_encode($data, JSON_PRETTY_PRINT);
}
