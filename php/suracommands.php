<?php

if (isset($_GET['command'])) {
    if (file_exists(__DIR__ . '/Medoo.php') && file_exists(__DIR__ . '/sura_config.php') && file_exists(__DIR__ . '/sura_functions.php')) {
        require_once(__DIR__ . '/Medoo.php');
        require_once(__DIR__ . '/sura_config.php');
        require_once(__DIR__ . '/sura_functions.php');

        $params = [];

        $command = basename($_GET['command']);
        unset($_GET['command']);

        if (isset($_GET['web'])) {
            unset($_GET['web']);
            if (!isset($_SESSION)) {
                session_start();
            }
            $params[] = $_SESSION['entity']['id'];
            $params = array_merge($params, $_GET);
        } else {
            $params = $_GET;
        }

        if (file_exists(__DIR__ . '/suracommands/' . $command . '.php')) {
            require_once __DIR__ . '/suracommands/' . $command . '.php';
            $response = NULL;

            try {
                $response = call_user_func_array($command, $params);
            } catch (ArgumentCountError $e) {
                $response = ['err' => $e->getMessage()];
            }
            send_response(200, "Success", $response);
        } else { // when wrong command is sent ...
            send_response(405, "Invalid Command ", NULL);
        }
    } else { // couldn't find sura_config of sura_functions...
        send_response(500, "Internal Server Error", NULL);
    }
} else { // when command is not set...
    send_response(400, "Invalid Request", NULL);
}

function send_response($status, $status_message, $data) {
    header("HTTP/1.1 $status $status_message");
    echo json_encode($data, JSON_PRETTY_PRINT);
}

?>