<?php

session_name('UICIMA');
session_start();

function activate_uic_policy_by_id() {
    require_once('../php/form_validate.php');

    $con = makeConnection();

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        // clean user inputs to prevent sql injections
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if (!$_POST) {
            return ['status' => false, 'err' => 'INVALID_DETAILS'];
        }

        $policyID = $_POST['policyid'];


        $selfInfo = runSimpleFetchQuery($con, ['id', 'email'], 'admins', ['id'], ['='], [$_SESSION['id']], '', '', 1)['result'];
        if (empty($selfInfo)) {
            disconnectConnection($con);
            return ['status' => false, 'err' => 'NO_SUCH_ADMIN', 'message' => 'Enter correct admin login details'];
        }
        $self_email = $selfInfo[0]['email'];

        $policyInfo = runSimpleFetchQuery($con, ['id', 'title'], 'policies', ['id', 'insurer_id'], ['=', '='], [$policyID, 0], '', '', 1)['result'];
        if (empty($policyInfo)) {
            disconnectConnection($con);
            return ['status' => false, 'err' => 'NO_SUCH_POLICY', 'message' => 'Verify policy ID'];
        }
        $policy_title = $policyInfo[0]['title'];

        ////////////////////////////////////////////////////////////////
        autoCommit($con, false);
        $flag = true;
        $date = (new DateTime())->format(DateTime::ISO8601);

        do {
            $res = runSimpleInsertQuery($con, 'admins_activities', ['admin', 'entity', 'summary', 'datetimezone'], ["'$self_email'", "'$policy_title'", "'activate_basic_policy'", "'$date'"]);
            if ($res['err']['code']) {
                $flag = false;
                break;
            }
            $res = runSimpleUpdateQuery($con, 'policies', ['active'], ["'TRUE'"], ['id', 'insurer_id'], ['=', '='], [$policyID, 0]);
            if ($res['err']['code']) {
                $flag = false;
            }
        } while (false);

        if ($flag) {
            commit($con);
            $message['status'] = true;
            $message['info'] = 'SUCCESS';
        } else {
            rollback($con);
            $message['status'] = false;
            $message['info'] = 'ERROR_OCCURRED';
        }
    } else {
        $message['status'] = false;
        $message['info'] = 'WRONG_REQUEST_METHOD';
    }


    autoCommit($con, true);
    disconnectConnection($con);

    return $message;
}
