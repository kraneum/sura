<?php

function add_pension_signup($id, $pensionID) {
    $con = makeConnection();
    $flag = true;
    autoCommit($con, false);

    if (empty($id)) {
        disconnectConnection($con);
        return ['status' => false, 'err' => 'NO_USERID'];
    }
    if (empty($pensionID)) {
        disconnectConnection($con);
        return ['status' => false, 'err' => 'NO_PENSIONID'];
    }
    $pension_info = runSimpleFetchQuery($con, ['id', 'name'], 'pension', ['id'], ['='], [$pensionID], '', '', '1')['result'];
    if (empty($pension_info)) {
        disconnectConnection($con);
        return ['status' => false, 'err' => 'INVALID_PENSION_ID'];
    }
    $pensioner_id = $pension_info[0]['id'];
    $pensioner_name = $pension_info[0]['name'];

    $userInfo = runSimpleFetchQuery($con, ['f_name', 'phone'], 'users', ['id'], ['='], [$id], '', '', '')['result'];
    if (empty($userInfo)) {
        disconnectConnection($con);
        return ['status' => false, 'err' => 'NO_SUCH_USER'];
    }
    $f_name = $userInfo[0]['f_name'];
    $phone = $userInfo[0]['phone'];

    $date = (new DateTime())->format(DateTime::ISO8601);

    /////////////////////////////////////////////////

    if ($flag == true) {

        $signup_id = null;
        $result = runSimpleInsertQuery($con, 'pension_signups', ['user_id', 'pension_id', 'date_signed'], ["'$id'", "'$pensionID'", "'$date'"]);
        if ($result['err']['code']) {
            if ($flag) {
                $flag = false;
            }
        } else {
            $signup_id = $result['insertedId'];
        }

        $result = runSimpleInsertQuery($con, 'user_log', ['action', 'info', 'date', 'user_id'], ["'PENSION_SIGNUP'", "'$pensionID'", "'$date'", $id]);
        if ($result['err']['code']) {
            if ($flag) {
                $flag = false;
            }
        }
        $result = runSimpleInsertQuery($con, 'pension_log', ['action', 'info', 'date', 'pension_id'], ["'PENSION_SIGNUP'", "'$id'", "'$date'", $pensionID]);
        if ($result['err']['code']) {
            if ($flag) {
                $flag = false;
            }
        }

        if ($flag) {
            commit($con);
            $message['info'] = 'SUCCESS';

            require 'sms/sms_sender.php';
            sendSms($f_name, "You just added a pension manager named " . $pensioner_name, "'$phone'", 234);
        } else {
            rollback($con);
            $message['info'] = 'ERROR';
        }
    } else {
        rollback($con);
        $message['info'] = 'FAILURE';
    }

    autoCommit($con, true);

    disconnectConnection($con);
    return $message;
}
