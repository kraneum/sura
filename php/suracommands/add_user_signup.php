<?php

function add_user_signup($id, $policyID, $auto_assign = false) {
    $con = makeConnection_user();
    $flag = true;
    autoCommit($con, false);



    $policyInfo = runSimpleFetchQuery($con, ['title', 'price', 'insurer_id', 'tenure', 'type_id'], 'policies', ['id'], ['='], [$policyID], '', '', '1')['result'];
    $policy_title = $policyInfo[0]['title'];
    $price = intval($policyInfo[0]['price']);
    $tenure = $policyInfo[0]['tenure'];
    $insurer_id = $policyInfo[0]['insurer_id'];
    $type_id = $policyInfo[0]['type_id'];

    $in_months = runSimpleFetchQuery($con, ['in_months'], 'payment_structures', ['id'], ['='], [$tenure], '', '', '1')['result'];
    $new_payday = (new DateTime())->add(new DateInterval('P' . $in_months . 'M'))->format(DateTime::ISO8601);

    $userInfo = runSimpleFetchQuery($con, ['f_name', 'l_name', 'phone', 'health_point_balance', 'general_point_balance', 'address', 'dob', 'gender', 'local_gov', 'next_of_kin', 'kin_phone'], 'users', ['id'], ['='], [$id], '', '', '')['result'];
    if (!$auto_assign) {
        if (empty($userInfo[0]['f_name']) ||
                empty($userInfo[0]['l_name']) ||
                empty($userInfo[0]['phone']) ||
                empty($userInfo[0]['address']) ||
                empty($userInfo[0]['dob']) ||
                empty($userInfo[0]['gender']) ||
                empty($userInfo[0]['local_gov']) ||
                empty($userInfo[0]['next_of_kin']) ||
                empty($userInfo[0]['kin_phone'])
        ) {
            echo json_encode(['status' => false, 'info' => 'USER_DETAILS_INCOMPLETE']);
            autoCommit($con, true);
            disconnectConnection($con);
            exit();
        }
    }
    $f_name = $userInfo[0]['f_name'];
    $l_name = $userInfo[0]['l_name'];
    $phone = $userInfo[0]['phone'];
    $health_point_balance = intval($userInfo[0]['health_point_balance']);
    $general_point_balance = intval($userInfo[0]['general_point_balance']);

    $points = null;
    $other_points = null;
    if ($type_id == '1') {
        $points = $health_point_balance;
        $other_points = $general_point_balance;
    } else if ($type_id == '2') {
        $points = $general_point_balance;
        $other_points = $health_point_balance;
    }

    $insurerID = null;
    $insurer_points = 0;
    $insurer_name = null;
    $insurer_email = null;

    $temp = null;

    if ($insurer_id == 0) {
        if ($type_id == '1') {
            $temp = runSimpleFetchQuery($con, ['id', 'name', 'email', 'redeemable_points'], 'insurer', ['id'], ['='], [' (select health_insurer_id from insurer_leads) '], '', '', '1')['result'];
        } else if ($type_id == '2') {
            $temp = runSimpleFetchQuery($con, ['id', 'name', 'email', 'redeemable_points'], 'insurer', ['id'], ['='], [' (select general_insurer_id from insurer_leads) '], '', '', '1')['result'];
        }
    } else {
        $basicPolicyInfo = runGetUserBasicPolicies($con, $id)['result'];
        if (empty($basicPolicyInfo)) {
            echo json_encode(['status' => false, 'info' => 'NO_BASIC_POLICY']);
            autoCommit($con, true);
            disconnectConnection($con);
            exit();
        }
        $temp = runSimpleFetchQuery($con, ['id', 'name', 'email', 'redeemable_points'], 'insurer', ['id'], ['='], [$insurer_id], '', '', '1')['result'];
    }

    $insurerID = $temp[0]['id'];
    $insurer_name = $temp[0]['name'];
    $insurer_email = $temp[0]['email'];
    $insurer_points = intval($temp[0]['redeemable_points']);

    $date = (new DateTime())->format(DateTime::ISO8601);
    $ref = 'UICI-' . $id . round(microtime(true) * 1000);

    /////////////////////////////////////////////////////////////////////////

    if ($points >= $price || $auto_assign) {

        $signup_id = null;

        do {
            $result = runSimpleInsertQuery($con, 'user_signups', ['user_id', 'policy_id', 'date_signed'], ["'$id'", "'$policyID'", "'$date'"]);
            if ($result['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            } else {
                $signup_id = $result['insertedId'];
            }

            if ($insurer_id == '0') {
                $result = runSimpleInsertQuery($con, 'uic_signups', ['user_signup_id', 'insurer_id', 'date_signed'], ["'$signup_id'", "'$insurerID'", "'$date'"]);

                if ($result['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }
            }

            /*             * ************ For when the subscriber is buying policy  ******************** */
            if (!$auto_assign) {
                $new_point_balance = $points - $price;
                if ($type_id == '1') {
                    $result = runSimpleUpdateQuery($con, 'users', ['health_point_balance'], ["'$new_point_balance'"], ['id'], ['='], [$id]);
                    if ($result['err']['code']) {
                        if ($flag) {
                            $flag = false;
                        }
                    }
                } else if ($type_id == '2') {
                    $result = runSimpleUpdateQuery($con, 'users', ['general_point_balance'], ["'$new_point_balance'"], ['id'], ['='], [$id]);
                    if ($result['err']['code']) {
                        if ($flag) {
                            $flag = false;
                        }
                    }
                }

                $new_insurer_points = $insurer_points + $price;
                $result = runSimpleUpdateQuery($con, 'insurer', ['redeemable_points'], ["'$new_insurer_points'"], ['id'], ['='], [$insurerID]);
                if ($result['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }

                $new_total_points = $new_point_balance + $other_points;
                $result = runSimpleInsertQuery($con, 'transactions', ['reference', 'point_worth', 'date_transacted', 'type_id', 'currency_id', 'from_whom', 'to_who', 'previous_balance_from', 'previous_balance_to'], ["'$ref'", $price, "'$date'", 2, 1, $id, $insurerID, "'$new_total_points'", "'$new_insurer_points'"]);
                if ($result['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }
            }
            /////////////////////////////


            $result = runSimpleInsertQuery($con, 'payment_schedule', ['date_to_charge', 'user_id', 'signup_id', 'amount'], ["'$new_payday'", "'$id'", "'$signup_id'", "'$price'"]);
            if ($result['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }
            $result = runSimpleInsertQuery($con, 'user_log', ['action', 'info', 'date', 'user_id'], ["'BUY_POLICY'", "'$policyID'", "'$date'", $id]);
            if ($result['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }
            $result = runSimpleInsertQuery($con, 'insurer_log', ['action', 'info', 'date', 'insurer_id'], ["'USER_SIGNUP'", "'$policyID'", "'$date'", $insurerID]);
            if ($result['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }
        } while (false);

        if ($flag) {
            commit($con);
            $message['info'] = 'SUCCESS';
            if ($type_id == '1') {
                $message['policy_type'] = 'Health';
            } else if ($type_id == '2') {
                $message['policy_type'] = 'General';
            }
            if (!$auto_assign) {
                $message['points'] = number_format($new_point_balance);
            }

            if (session_status() === PHP_SESSION_ACTIVE) {
                if ($type_id == '1') {
                    $_SESSION['entity']['health_point_balance'] = $new_point_balance;
                } else if ($type_id == '2') {
                    $_SESSION['entity']['general_point_balance'] = $new_point_balance;
                }
                $_SESSION['last_transaction'] = 'debit';
            }

            $sms_message = "You purchased a policy '" . $policy_title . "' by " . $insurer_name;
            if ($auto_assign) {
                $sms_message = "You have been automatically subscribed to a policy '" . $policy_title . "' by" . $insurer_name;
            }
            require_once 'send_sms.php';
            send_sms($f_name, $sms_message, "'$phone'", 234);

            if (!$auto_assign) {
                require 'send_mail.php';
                $subject = 'Policy Sign Up from iInsurance';
                $mail = '<html><body style="padding: 0 20%; font-size:14px"><div><h3>Hi ' . $insurer_name . ', </h3>'
                        . "A user '$f_name $l_name ($phone)' signed up for a policy named $policy_title worth $price Loyalty Points"
                        . '<br><br><div style="font-size: .8em">If you’ve received this email by mistake, simply ignore it.'
                        . 'Your account won\'t be created if you don\'t click the confirmation link above.'
                        . '<br><br>Thanks for your loyalty.</div></div></html></body>';
                send_mail($insurer_email, $subject, $mail);
            }
        } else {
            rollback($con);
            $message['info'] = 'ERROR';
        }
    } else {
        rollback($con);
        $message['info'] = 'FAILURE';
    }

    autoCommit($con, true);

    disconnectConnection($con);
    return $message;
}
