<?php

$signup_id = null;

do {
    $result = runSimpleInsertQuery($con, 'user_signups', ['user_id', 'policy_id', 'date_signed'], ["'$id'", "'$policyID'", "'$date'"]);
    if ($result['err']['code']) {
        if ($flag) {
            $flag = false;
        }
    } else {
        $signup_id = $result['insertedId'];
    }

    if ($insurer_id == '0') {
        $result = runSimpleInsertQuery($con, 'uic_signups', ['user_signup_id', 'insurer_id', 'date_signed'], ["'$signup_id'", "'$insurerID'", "'$date'"]);

        if ($result['err']['code']) {
            if ($flag) {
                $flag = false;
            }
        }
    }

    $new_point_balance = $points - $price;
    if ($type_id == '1') {
        $result = runSimpleUpdateQuery($con, 'users', ['health_point_balance'], ["'$new_point_balance'"], ['id'], ['='], [$id]);
        if ($result['err']['code']) {
            if ($flag) {
                $flag = false;
            }
        }
    } else if ($type_id == '2') {
        $result = runSimpleUpdateQuery($con, 'users', ['general_point_balance'], ["'$new_point_balance'"], ['id'], ['='], [$id]);
        if ($result['err']['code']) {
            if ($flag) {
                $flag = false;
            }
        }
    }
    
    $new_insurer_points = $insurer_points + $price;
    $result = runSimpleUpdateQuery($con, 'insurer', ['redeemable_points'], ["'$new_insurer_points'"], ['id'], ['='], [$insurerID]);
    if ($result['err']['code']) {
        if ($flag) {
            $flag = false;
        }
    }

    $new_total_points = $new_point_balance + $other_points;
    $result = runSimpleInsertQuery($con, 'transactions', ['reference', 'point_worth', 'date_transacted', 'type_id', 'currency_id', 'from_whom', 'to_who', 'previous_balance_from', 'previous_balance_to'], ["'$ref'", $price, "'$date'", 2, 1, $id, $insurerID, "'$new_total_points'", "'$new_insurer_points'"]);
    if ($result['err']['code']) {
        if ($flag) {
            $flag = false;
        }
    }

    $result = runSimpleInsertQuery($con, 'payment_schedule', ['date_to_charge', 'user_id', 'signup_id', 'amount'], ["'$new_payday'", "'$id'", "'$signup_id'", "'$price'"]);
    if ($result['err']['code']) {
        if ($flag) {
            $flag = false;
        }
    }
    $result = runSimpleInsertQuery($con, 'user_log', ['action', 'info', 'date', 'user_id'], ["'BUY_POLICY'", "'$policyID'", "'$date'", $id]);
    if ($result['err']['code']) {
        if ($flag) {
            $flag = false;
        }
    }
    $result = runSimpleInsertQuery($con, 'insurer_log', ['action', 'info', 'date', 'insurer_id'], ["'USER_SIGNUP'", "'$policyID'", "'$date'", $insurerID]);
    if ($result['err']['code']) {
        if ($flag) {
            $flag = false;
        }
    }
} while (false);


