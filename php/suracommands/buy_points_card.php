<?php

function buy_points_card($id) {

    if (!$_SESSION) {
        echo json_encode(['status' => false, 'err' => 'FRAUD']);
        exit();
    }

    $con = makeConnection_user();

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }

            $ref = $_POST['ref'];
            $amount = $_POST['amount'];

            autoCommit($con, false);
            $flag = true;
            $date = (new DateTime())->format(DateTime::ISO8601);

            $point_balances = runSimpleFetchQuery($con, ['health_point_balance', 'general_point_balance'], 'users', ['id'], ['='], [$id], '', '', '')['result'];
            $health_point_balance = $point_balances[0]['health_point_balance'];
            $general_point_balance = $point_balances[0]['general_point_balance'];


            ///////////////////////////////////////////////////////

            do {
                $new_health_points = $health_point_balance + ceil($amount / 2);
                $new_general_points = $general_point_balance + floor($amount / 2);
                $result = runSimpleUpdateQuery($con, 'users', ['health_point_balance', 'general_point_balance'], ["$new_health_points", "$new_general_points"], ['id'], ['='], [$id]);
                if ($result['err']['code']) {
                    $flag = false;
                    break;
                }

                $previous_points = $health_point_balance + $general_point_balance;
                $new_points = $previous_points + $amount;
                $result = runSimpleInsertQuery($con, 'transactions', ['reference', 'point_worth', 'date_transacted', 'type_id', 'from_whom', 'to_who', 'previous_balance_to', 'previous_balance_from'], ["'$ref'", $amount, "'$date'", 14, 0, $id, $new_points, "'NULL'"]);
                if ($result['err']['code']) {
                    $flag = false;
                    break;
                }
                $trans_id = $result['insertedId'];

                $result = runSimpleInsertQuery($con, 'user_log', ['action', 'info', 'date', 'user_id'], ["'BUY_POINTS'", "'$trans_id'", "'$date'", $id]);
                if ($result['err']['code']) {
                    $flag = false;
                }
            } while (false);

            ///////////////////////////////////////////////

            if ($flag) {
                commit($con);
                $message['status'] = true;
                $message['info'] = 'SUCCESS';
                $message['health_points'] = number_format($new_health_points);
                $message['general_points'] = number_format($new_general_points);

                $_SESSION['entity']['health_point_balance'] = $new_health_points;
                $_SESSION['entity']['general_point_balance'] = $new_general_points;
                $_SESSION['last_transaction'] = 'credit';
//            require_once 'php/sms/sms_sender.php';
//            sendSms($rec_name, 'You were transferred ' . $amount . ' points from a Merchant' . ' - ' . $merchant_name, "$rec_number");
            } else {
                rollback($con);
                $message['status'] = false;
                $message['info'] = 'ERROR';
            }
        } else {
            $message['status'] = false;
            $message['err'] = 'WRONG_REQUEST_TYPE';
        }
    } else {
        $message['status'] = false;
        $message['err'] = 'DATABASE_CONNECT_ERROR';
    }

    autoCommit($con, true);

    disconnectConnection($con);

    return $message;
}
