<?php
require_once('sura_config.php');
require_once('sura_functions.php');

function buy_points_merchant($merchant_id, $point_worth) {

    $con = makeConnection_merchant();

    $insert_ = runSimpleInsertQuery(
            $con, "point_purchases", ['`merchant_id`', '`point_amount`'], [$merchant_id, $point_worth]
    )['result'];

    if ($insert_) {

        $update = runSimpleUpdateQuery(
                $con, "merchants", ['point_balance'], ['point_balance+' . $point_worth], ['id'], ['='], [$merchant_id])['result'];

        if ($update) {
            echo 1;
        } else {
            echo -1;
        }
    } else {
        echo 0;
    }

    disconnectConnection($con);
}