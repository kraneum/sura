<?php

function buy_sms_credit($id) {
    require_once('../php/form_validate.php');
    $con = makeConnection_insurer();
    $flag = true;
    autoCommit($con, false);

    $response = ['status' => true, 'err' => null];

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // clean user inputs to prevent sql injections
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }

            $amount = $_POST['amount'];

            $validationResult = $form_validate([
                'amount' => 'required|number',
                    ], [
                'amount' => $amount,
            ]);

            $sms_points = intval(runSimpleFetchQuery($con, ['sms_wallet'], 'insurer', ['id'], ['='], [$id], '', '', 1)['result']);
            $new_sms_points = $sms_points + $amount;

            $date = (new DateTime())->format(DateTime::ISO8601);
            $ref = 'UICI-' . $id . round(microtime(true) * 1000);

            if (!empty($validationResult)) {
                $response['status'] = false;
                $response['err'] = 'VALIDATION_ERROR';
                $response['errVal'] = $validationResult;
            } else {

                $result = runSimpleUpdateQuery($con, 'insurer', ['sms_wallet'], ["'$new_sms_points'"], ['id'], ['='], [$id]);
                if ($result['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }

                $result = runSimpleInsertQuery($con, 'sms_transactions', ['ref', 'point_worth', 'date_transacted', 'currency_id', 'insurer_id', 'previous_balance'], ["'$ref'", $amount, "'$date'", 1, $id, $new_sms_points]);
                if ($result['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }

                $result = runSimpleInsertQuery($con, 'insurer_log', ['action', 'info', 'date', 'insurer_id'], ["'BOUGHT_SMS_POINTS'", "'$amount'", "'$date'", $id]);
                if ($result['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }

                if ($flag) {
                    commit($con);
                    $message['info'] = 'SUCCESS';
                    $message['points'] = number_format($new_sms_points);

                    if ($_SESSION) {
                        $_SESSION['entity']['sms_wallet'] = $new_sms_points;
                    }
                } else {
                    rollback($con);
                    $response['status'] = false;
                    $response['err'] = 'FAILURE';
                }
            }
        } else {
            rollback($con);
            $response['status'] = false;
            $response['err'] = 'REGISTRATION_ERROR';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    autoCommit($con, true);

    disconnectConnection($con);

    return $message;
}
