<?php

function buy_sms_points_card($id) {

    if (!$_SESSION) {
        echo json_encode(['status' => false, 'err' => 'FRAUD']);
        exit();
    }

    $con = makeConnection_insurer();

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }

            $ref = $_POST['ref'];
            $amount = $_POST['amount'];

            autoCommit($con, false);
            $flag = true;
            $date = (new DateTime())->format(DateTime::ISO8601);

            $sms_points = intval(runSimpleFetchQuery($con, ['sms_wallet'], 'insurer', ['id'], ['='], [$id], '', '', 1)['result']);

            $new_sms_points = $sms_points + $amount;

            ///////////////////////////////////////////////////////

            do {
                $result = runSimpleUpdateQuery($con, 'insurer', ['sms_wallet'], ["'$new_sms_points'"], ['id'], ['='], [$id]);
                if ($result['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }

                $result = runSimpleInsertQuery($con, 'sms_transactions', ['ref', 'point_worth', 'date_transacted', 'insurer_id', 'previous_balance'], ["'$ref'", $amount, "'$date'", $id, "'$sms_points'"]);
                if ($result['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }

                $result = runSimpleInsertQuery($con, 'insurer_log', ['action', 'info', 'date', 'insurer_id'], ["'BOUGHT_SMS_POINTS'", "'$amount'", "'$date'", $id]);
                if ($result['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }
            } while (false);

            ///////////////////////////////////////////////

            if ($flag) {
                commit($con);
                $message['status'] = true;
                $message['info'] = 'SUCCESS';
                $message['points'] = $new_sms_points;
                $_SESSION['entity']['sms_wallet'] = $new_sms_points;
//            require_once '../php/sms/sms_sender.php';
//            sendSms($rec_name, 'You were transferred ' . $amount . ' points from a Merchant' . ' - ' . $merchant_name, "$rec_number");
            } else {
                rollback($con);
                $message['status'] = false;
                $message['info'] = 'ERROR';
            }
        } else {
            $message['status'] = false;
            $message['err'] = 'WRONG_REQUEST_TYPE';
        }
    } else {
        $message['status'] = false;
        $message['err'] = 'DATABASE_CONNECT_ERROR';
    }

    autoCommit($con, true);

    disconnectConnection($con);

    return $message;
}
