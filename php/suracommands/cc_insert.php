<?php

function cc_insert() {
    session_start();
    require_once('../php/form_validate.php');

    $con = makeConnection();
    autoCommit($con, false);
    $response = ['status' => true, 'err' => null];

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // clean user inputs to prevent sql injections
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }

            $name = $_POST['name'];
            $phone = $_POST['phone'];
            $location = $_POST['location'];
            $message = $_POST['message'];
            $type = $_POST['type'];
            $id = $_SESSION['id'];

            $validationResult = $form_validate([
                'name' => 'required|maxlength:100',
                'phone' => 'required|phone|maxlength:11',
                'location' => 'required|maxlength:50',
                'message' => 'required|maxlength:2000',
                'type' => 'required|maxlength:20',
                    ], [
                'name' => $name,
                'phone' => $phone,
                'location' => $location,
                'message' => $message,
                'type' => $type
            ]);

            if (!empty($validationResult)) {
                $response['status'] = false;
                $response['err'] = 'VALIDATION_ERROR';
                $response['errVal'] = $validationResult;
            } else {
                $date = (new DateTime())->format(DateTime::ISO8601);

                $res = runSimpleInsertQuery($con, "cc_messages", ['name', 'phone', 'location', 'message', 'type', 'date', 'agent_id'], ["'$name'", "'$phone'", "'$location'", "'$message'", "'$type'", "'$date'", "'$id'"]);

                if (!$res['err']['code']) {

                    unset($name);
                    unset($phone);
                    unset($location);
                    unset($message);
                    unset($type);
                    commit($con);
                } else {
                    rollback($con);
                    $response['status'] = false;
                    $response['err'] = 'REGISTRATION_ERROR';
                }
            }
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    autoCommit($con, true);
    return $response;
}
