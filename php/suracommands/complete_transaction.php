<?php

function complete_transaction() {
    require_once('../php/form_validate.php');

    $con = makeConnection();
    $flag = true;
    autoCommit($con, false);
    $response = [];

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // clean user inputs to prevent sql injections
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                disconnectConnection($con);
                return ['status' => false, 'err' => 'INVALID_DETAILS1'];
            }

//            print_r($_POST);
            $otp = $_POST['otp'];
            $secret_key = $_POST['api_key'];

            $validationResult = $form_validate([
                'otp' => 'required|number|maxlength:6',
                'secret_key' => 'required',
                    ], [
                'otp' => $otp,
                'secret_key' => $secret_key
            ]);


            if (!empty($validationResult)) {
                $response['status'] = false;
                $response['err'] = 'VALIDATION_ERROR';
            } else {
                $date = (new DateTime())->format(DateTime::ISO8601);

                $storeInfo = runSimpleFetchQuery($con, ['id', 'name', 'points'], 'e_commerce', ['api_key'], ['='], [$secret_key], '', '', 1)['result'];
                if (empty($storeInfo)) {
                    disconnectConnection($con);
                    return ['status' => false, 'err' => 'NO_SUCH_STORE'];
                }
                $store_id = $storeInfo[0]['id'];
                $store_name = $storeInfo[0]['name'];
                $store_points = intval($storeInfo[0]['points']);

                $verifyInfo = runSimpleFetchQuery($con, ['*'], 'verify_transactions', ['otp', 'ecommerce_id'], ['=', '='], [$otp, $store_id], '', '', 1)['result'];
                if (empty($verifyInfo)) {
                    disconnectConnection($con);
                    return ['status' => false, 'err' => 'INVALID_OTP'];
                }
                $verify_id = $verifyInfo[0]['id'];
                $user_id = $verifyInfo[0]['user_id'];
                $pending_trans_id = $verifyInfo[0]['trans_id'];
                $date_expire = $verifyInfo[0]['date_expire'];
                if ($date > $date_expire) {
                    disconnectConnection($con);
                    return ['status' => false, 'err' => 'EXPIRED_OTP'];
                }

                $_transInfo = runSimpleFetchQuery($con, ['amount', 'status'], 'pending_transactions', ['id'], ['='], [$pending_trans_id], '', '', 1)['result'];
                if (empty($_transInfo)) {
                    disconnectConnection($con);
                    return ['status' => false, 'err' => 'NO_SUCH_TRANSACTION'];
                }
                $amount = $_transInfo[0]['amount'];
                $status = $_transInfo[0]['status'];
                if ($status == 'COMPLETED') {
                    disconnectConnection($con);
                    return ['status' => false, 'err' => 'INVALID_TRANSACTION'];
                }

                $userInfo = runSimpleFetchQuery($con, ['id', 'f_name', 'phone', 'point_balance'], 'users', ['id'], ['='], [$user_id], '', '', 1)['result'];
                if (empty($userInfo)) {
                    disconnectConnection($con);
                    return ['status' => false, 'err' => 'NO_SUCH_USER'];
                }
                $user_id = $userInfo[0]['id'];
                $f_name = $userInfo[0]['f_name'];
                $phone = $userInfo[0]['phone'];
                $point_balance = intval($userInfo[0]['point_balance']);

                $date = (new DateTime())->format(DateTime::ISO8601);
                $ref = 'UICI-' . $user_id . round(microtime(true) * 1000);

                /*                 * ******************************************************************************** */

                if ($point_balance >= $amount) {


                    $new_point_balance = $point_balance - $amount;

                    $result = runSimpleUpdateQuery($con, 'users', ['point_balance'], ["'$new_point_balance'"], ['id'], ['='], [$user_id]);
                    if ($result['err']['code']) {
                        if ($flag) {
                            $flag = false;
                        }
                    }

                    $new_store_points = $store_points + $amount;
                    $result = runSimpleUpdateQuery($con, 'e_commerce', ['points'], ["'$new_store_points'"], ['id'], ['='], [$store_id]);
                    if ($result['err']['code']) {
                        if ($flag) {
                            $flag = false;
                        }
                    }

                    $_trans_id = NULL;
                    $result = runSimpleInsertQuery($con, 'transactions', ['reference', 'point_worth', 'date_transacted', 'type_id', 'currency_id', 'from_whom', 'to_who', 'previous_balance_from', 'previous_balance_to'], ["'$ref'", $amount, "'$date'", 9, 1, $user_id, $store_id, "'$new_point_balance'", "'$new_store_points'"]);
                    if ($result['err']['code']) {
                        if ($flag) {
                            $flag = false;
                        }
                    } else {
                        $_trans_id = $result['insertedId'];
                    }

                    $result = runSimpleInsertQuery($con, 'user_log', ['action', 'info', 'date', 'user_id'], ["'MAKE_PAYMENT'", "'$_trans_id'", "'$date'", $user_id]);
                    if ($result['err']['code']) {
                        if ($flag) {
                            $flag = false;
                        }
                    }

                    $result = runSimpleUpdateQuery($con, 'pending_transactions', ['status'], ["'COMPLETED'"], ['id'], ['='], ["'$pending_trans_id'"]);
                    if ($result['err']['code']) {
                        if ($flag) {
                            $flag = false;
                        }
                    }

                    $result = runSimpleUpdateQuery($con, 'verify_transactions', ['verified'], ["'TRUE'"], ['id'], ['='], ["'$verify_id'"]);
                    if ($result['err']['code']) {
                        if ($flag) {
                            $flag = false;
                        }
                    }

                    if ($flag) {
                        commit($con);
                        $response['status'] = true;
                        $response['info'] = 'SUCCESS';

                        require 'send_sms.php';
                        send_sms($f_name, "A payment of " . $amount . " points was made to " . ucfirst($store_name), "'$phone'");
                    } else {
                        rollback($con);
                        $response['info'] = 'FAILURE';
                    }
                } else {
                    rollback($con);
                    $response['info'] = 'INSUFFICIENT_POINTS';
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'INVALID_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    autoCommit($con, true);

    disconnectConnection($con);
    return $response;
}
