<?php
require_once('sura_config.php');
require_once('sura_functions.php');

function create_insurance_policy($packet) {
    $con = makeConnection_i();
    //$packet_ = json_decode(stripslashes($packet), true);

    $insert_ = runSimpleInsertQueryWithIDReturn(
            $con, "policies", [
        '`insurer_id`',
        '`type_id`',
        '`title`',
        '`description`',
        '`price`',
        '`currency_id`',
        '`payment_schedule_id`',
        '`terms_url`',
        '`image_url`',
        'tenure'
            ], [
        "'" . $packet['insurer_id'] . "'",
        "'" . $packet['type_id'] . "'",
        "'" . $packet['title'] . "'",
        "'" . $packet['description'] . "'",
        "'" . $packet['price'] . "'",
        "'" . $packet['currency_id'] . "'",
        "'" . $packet['payment_schedule_id'] . "'",
        "'" . $packet['terms_url'] . "'",
        "'" . $packet['image_url'] . "'",
        "'" . $packet['tenure'] . "'"
    ])['result'];

    echo $insert_;

    disconnectConnection($con);
}