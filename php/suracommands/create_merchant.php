<?php
require_once('sura_config.php');
require_once('sura_functions.php');

function create_merchant($packet) {
    $con = makeConnection();
    //$packet_ = json_decode(stripslashes($packet), true);

    $insert_ = runSimpleInsertQueryWithIDReturn(
            $con, "merchants", [
        '`name`',
        '`write_up`',
        '`contact_phone`',
        '`contact_email`',
        '`rc_number`',
        '`login_username`',
        '`login_password`',
        '`operating_country`',
        '`operating_province`',
        '`date_joined`'
            ], [
        "'" . $packet['name'] . "'",
        "'" . $packet['write_up'] . "'",
        "'" . $packet['contact_phone'] . "'",
        "'" . $packet['contact_email'] . "'",
        "'" . $packet['rc_number'] . "'",
        "'" . $packet['login_username'] . "'",
        "'" . $packet['login_password'] . "'",
        "'" . $packet['operating_country'] . "'",
        "'" . $packet['operating_province'] . "'",
        'CURRENT_TIMESTAMP'
    ])['result'];

    echo $insert_;

    disconnectConnection($con);
}