<?php
require_once('sura_config.php');
require_once('sura_functions.php');

function create_user($packet) {
    $con = makeConnection();
    //$packet_ = json_decode(stripslashes($packet), true);

    $insert_ = runSimpleInsertQueryWithIDReturn(
            $con, "users", [
        '`f_name`',
        '`m_name`',
        '`l_name`',
        '`dob`',
        '`bvn`',
        '`phone`',
        '`email`',
        '`login_username`',
        '`login_password`',
        '`date_joined`'
            ], [
        "'" . $packet['f_name'] . "'",
        "'" . $packet['m_name'] . "'",
        "'" . $packet['l_name'] . "'",
        "'" . $packet['dob'] . "'",
        "'" . $packet['bvn'] . "'",
        "'" . $packet['phone'] . "'",
        "'" . $packet['email'] . "'",
        "'" . $packet['login_username'] . "'",
        "'" . $packet['login_password'] . "'",
        'CURRENT_TIMESTAMP'
    ])['result'];

    echo $insert_;

    disconnectConnection($con);
}