<?php
require_once('sura_config.php');
require_once('sura_functions.php');

function create_user_sign_up($user_id, $policy_id) {
    $con = makeConnection();

    $insert_ = runSimpleInsertQuery(
            $con, "user_signups", ['`user_id`', '`policy_id`'], [$user_id, $policy_id]
    )['result'];

    return $insert_;

    disconnectConnection($con);
}