<?php
require_once('sura_config.php');
require_once('sura_functions.php');

function create_ussd_codes($purchase_id, $point_worths, $quantities) {

    $con = makeConnection_merchant();
    $total_added = 0;

    for ($n = 0; $n < count($point_worths); $n++) {

        $p_worth = intval($point_worths[$n]);
        $quant = intval($quantities[$n]);

        for ($t = 0; $t < $quant; $t++) {

            if (insertUSSDCode($con, $p_worth, $purchase_id)['result']) {
                $total_added++;
            }
        }
    }

    echo 'total successfully added : ' . $total_added;

    disconnectConnection($con);
}