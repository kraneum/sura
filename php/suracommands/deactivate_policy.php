<?php

function deactivate_policy($id) {

    $con = makeConnection();
    autoCommit($con, false);

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        // clean user inputs to prevent sql injections
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if (!$_POST) {
            return ['status' => false, 'err' => 'INVALID_DETAILS'];
        }

        $policyID = trim($_POST['policyID']);

        if (empty($policyID)) {
            $message['info'] = 'VALIDATION_ERROR';
        } else {
            $flag = true;
            $date = (new DateTime())->format(DateTime::ISO8601);

            $res = runSimpleInsertQuery($con, 'insurer_log', ['action', 'info', 'date', 'insurer_id'], ["'DEACTIVATE_POLICY'", "'$policyID'", "'$date'", $id]);
            if ($res['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }
            $res = runSimpleUpdateQuery($con, 'policies', ['active'], ["'FALSE'"], ['id', 'insurer_id'], ['=', '='], [$policyID, $id]);
            if ($res['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }
            if ($flag) {
                if ($res['result'] == 0 && !$res['err']['code']) {
                    rollback($con);
                    autoCommit($con, true);
                    $message['info'] = 'NO_CHANGES';
                } else if (!$res['err']['code']) {
                    commit($con);
                    autoCommit($con, true);
                    $message['info'] = 'SUCCESS';
                } else {
                    rollback($con);
                    autoCommit($con, true);
                    $message['info'] = 'FAILURE';
                }
            } else {
                rollback($con);
                autoCommit($con, true);
                $message['info'] = 'ERROR_OCCURRED';
            }
        }
    } else {
        $message['info'] = 'WRONG_REQUEST_METHOD';
    }

    autoCommit($con, true);
    disconnectConnection($con);

    return $message;
}
