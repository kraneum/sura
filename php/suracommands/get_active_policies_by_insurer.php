<?php

function get_active_policies_by_insurer($id) {
    $con = makeConnection_insurer();
    
    $data = runSimpleFetchQuery($con, ['id'], 'policies', ['insurer_id', 'active'], ['=', '='], [$id, 1], '', '', '')['result'];

    disconnectConnection($con);
    return $data;
}
