<?php

function get_agent_by_id($id) {
    $con = makeConnection();
    global $$id;

    $trans = runSimpleFetchQuery($con, ['id', 'f_name', 'm_name', 'l_name', 'gender', 'dob', 'phone',
        'email', 'date_joined', 'last_login', 'profile_pic', 'points', 'active'], "agent", ["id"], ["="], [$id], "", "", "1")['result'];
    disconnectConnection($con);
    
    return $trans;
}