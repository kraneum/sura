<?php

if (!isset($_SESSION)) {
    session_start();
}
if (!isset($_SESSION['agentid'])) {
    exit("No agent id");
} else {
    $$id = $_SESSION['agentid'];
}

require_once('../php/sura_config.php');
require_once('../php/sura_functions.php');

function get_agent_referrals() {
    $con = makeConnection();
    global $$id;

    $trans = runSimpleFetchQuery($con, ['id', 'name', 'rc_number'], "merchants", ["referee"], ["="], [$$id], "", "", "")['result'];
    disconnectConnection($con);

    return $trans;
}
