<?php

function get_agents_count() {
    $con = makeConnection();
    $trans = [];

    $_trans = runSimpleFetchQuery($con, ['id', 'active'], 'agent', [], [], [], '', 'id DESC', '')['result'];

    $trans['total'] = count($_trans);
    $a_count = 0;

    foreach ($_trans as $this_trans) {
        $this_trans['active'] == 'TRUE' ? $a_count++ : '';
    }
    
    $trans['active'] = $a_count;
    
    disconnectConnection($con);
    return $trans;
}
