<?php

function get_all_agents($limit = 0, $offset = 0, $spec = '') {

    $con = makeConnection();
    $spec = $spec . '%';

    if ($limit <= 0) {
        $ins = runSimpleFetchORQuery($con, ['id', 'f_name', 'm_name', 'l_name', 'dob', 'phone', 'email', 
            'gender', 'points', 'last_login', 'date_joined',  'active'], "agent", 
                ['f_name', 'l_name'], [' LIKE ', ' LIKE '], ["'$spec'", "'$spec'"], "", "", "", "")['result'];
    } else {
        $ins = runSimpleFetchORQuery($con, ['id', 'f_name', 'm_name', 'l_name', 'dob', 'phone', 'email', 
            'gender','last_login', 'date_joined', 'points', 'active'], "agent", 
                ['f_name', 'l_name'], [' LIKE ', ' LIKE '], ["'$spec'", "'$spec'"], "", "", "$limit", "$offset")['result'];
    }


    disconnectConnection($con);
    return $ins;
}
