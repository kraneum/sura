<?php

function get_all_general_insurers($limit = 0, $offset = 0) {

    $con = makeConnection();
    
    if ($limit <= 0) {
        $ins = runSimpleFetchQuery($con, ['id', 'name', 'write_up', 'rc_number', 'insurance_type as type_id', 'phone', 'email',
                    'redeemable_points', 'account_no', 'sms_wallet', 'name_change', 'validated', 'approved', 'logo_url'], "insurer", 
                ['insurance_type'], ['='], ['2'], "", "", "", "")['result'];
    } else {
        $ins = runSimpleFetchQuery($con, ['id', 'name', 'write_up', 'rc_number', 'insurance_type as type_id', 'phone', 'email',
                    'redeemable_points', 'account_no', 'sms_wallet', 'name_change', 'validated', 'approved', 'logo_url'], "insurer", 
                ['insurance_type'], ['='], ['2'], "", "", "$limit", "$offset")['result'];
    }
    
    $ins[]['base_insurer_id'] = runSimpleFetchQuery($con, ['general_insurer_id'], "insurer_leads", [], [], [], "", "", "1")['result'];

    disconnectConnection($con);
    return $ins;
}
