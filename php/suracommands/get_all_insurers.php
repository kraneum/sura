<?php

function get_all_insurers($limit = 0, $offset = 0, $spec = '') {

    $con = makeConnection_insurer();
    
    $spec = $spec . '%';

    if ($limit <= 0) {
        $ins = runSimpleFetchQuery($con, ['id', 'name', 'write_up', 'rc_number', 'insurance_type as type_id', 'phone', 'email',
                    'redeemable_points', 'account_no', 'sms_wallet', 'name_change', 'validated', 'approved', 'logo_url'], "insurer", 
                ['name'], [' LIKE '], ["'$spec'"], "", "", "", "")['result'];
    } else {
        $ins = runSimpleFetchQuery($con, ['id', 'name', 'write_up', 'rc_number', 'insurance_type as type_id', 'phone', 'email',
                    'redeemable_points', 'account_no', 'sms_wallet', 'name_change', 'validated', 'approved', 'logo_url'], "insurer", 
                ['name'], [' LIKE '], ["'$spec'"], "", "", "$limit", "$offset")['result'];
    }

    $count = count($ins);

    for ($i = 0; $i < $count; $i++) {

        $this_signup = $ins[$i];

        $insurance_type = runSimpleFetchQuery($con, ['name'], "insurance_types", ["id"], ["="], [$this_signup['type_id']], "", "", "")['result'];
        $ins[$i]['type_name'] = $insurance_type;

        $insurer_docs_directory = '../insurer_docs/docs_id_' . $this_signup['id'] . '/';
        $docs = glob($insurer_docs_directory . '*.*');
        foreach ($docs as &$doc) { // ... '&' so as to pass by reference
            $doc = basename($doc);
        }
        $ins[$i]['docs'] = $docs;

        $name_docs_directory = '../insurer_docs/docs_id_' . $this_signup['id'] . '/Change of Name Docs/';
        $ndocs = glob($name_docs_directory . '*.*');
        foreach ($ndocs as &$ndoc) {
            $ndoc = basename($ndoc);
        }
        $ins[$i]['name_docs'] = $ndocs;
    }


    disconnectConnection($con);
    return $ins;
}
