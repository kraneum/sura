<?php

function get_all_merchants($limit = 0, $offset = 0, $spec = '') {
    $con = makeConnection();
    $merchants = null;
    $spec = $spec . '%';

    if ($limit <= 0) {
        $merchants = runSimpleFetchQuery($con, ['id', 'name', 'write_up', 'rc_number', 'contact_phone', 'contact_email','point_balance',
                    'name_change', 'deleted', 'approved', 'display_picture'], "merchants", ['name'], [' LIKE '], ["'$spec'"], "", "", "", "")['result'];
    } else {
        $merchants = runSimpleFetchQuery($con, ['id', 'name', 'write_up', 'rc_number', 'contact_phone', 'contact_email','point_balance',
                    'name_change', 'deleted', 'approved', 'display_picture'], "merchants", ['name'], [' LIKE '], ["'$spec'"], "", "", "$limit", "$offset")['result'];
    }

    $count = count($merchants);

    for ($i = 0; $i < $count; $i++) {

        $this_signup = $merchants[$i];

        $merchant_docs_directory = '../merchant_docs/docs_id_' . $this_signup['id'] . '/';
        $docs = glob($merchant_docs_directory . '*.*');
        foreach ($docs as &$doc) { // ... '&' so as to pass by reference
            $doc = basename($doc);
        }
        $merchants[$i]['docs'] = $docs;

        $name_docs_directory = '../merchant_docs/docs_id_' . $this_signup['id'] . '/Change of Name Docs/';
        $ndocs = glob($name_docs_directory . '*.*');
        foreach ($ndocs as &$ndoc) {
            $ndoc = basename($ndoc);
        }
        $merchants[$i]['name_docs'] = $ndocs;
    }

    disconnectConnection($con);
    return $merchants;
}
