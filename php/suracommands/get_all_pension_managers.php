<?php

function get_all_pension_managers($limit = 0, $offset = 0, $spec = '') {
    $con = makeConnection();
    $pen = null;
    $spec = $spec . '%';

    if ($limit <= 0) {
        $pen = runSimpleFetchQuery($con, ['id', 'name', 'write_up', 'rc_number', 'phone', 'email', 'redeemable_points', 'account_no',
                    'name_change', 'validated', 'approved', 'logo_url'], "pension", ['name'], [' LIKE '], ["'$spec'"], "", "", "", "")['result'];
    } else {
        $pen = runSimpleFetchQuery($con, ['id', 'name', 'write_up', 'rc_number', 'phone', 'email', 'redeemable_points', 'account_no',
                    'name_change', 'validated', 'approved', 'logo_url'], "pension", ['name'], [' LIKE '], ["'$spec'"], "", "", "$limit", "$offset")['result'];
    }

    $count = count($pen);

    for ($i = 0; $i < $count; $i++) {

        $this_signup = $pen[$i];

        $pension_docs_directory = '../pension_docs/docs_id_' . $this_signup['id'] . '/';
        $docs = glob($pension_docs_directory . '*.*');
        foreach ($docs as &$doc) { // ... '&' so as to pass by reference
            $doc = basename($doc);
        }
        $pen[$i]['docs'] = $docs;

        $name_docs_directory = '../pension_docs/docs_id_' . $this_signup['id'] . '/Change of Name Docs/';
        $ndocs = glob($name_docs_directory . '*.*');
        foreach ($ndocs as &$ndoc) {
            $ndoc = basename($ndoc);
        }
        $pen[$i]['name_docs'] = $ndocs;
    }

    disconnectConnection($con);
    return $pen;
}
