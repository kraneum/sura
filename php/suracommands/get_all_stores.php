<?php

function get_all_stores($limit = 0, $offset = 0, $spec = '') {

    $con = makeConnection();
    $spec = $spec . '%';

    if ($limit <= 0) {
        $stores = runSimpleFetchQuery($con, ['id', 'name', 'write_up', 'phone', 'email','points', 'account_no', 'date_joined', 'active'], 
                "e_commerce", ['name'], [' LIKE '], ["'$spec'"], "", "", "", "")['result'];
    } else {
        $stores = runSimpleFetchQuery($con, ['id', 'name', 'write_up', 'phone', 'email','points', 'account_no', 'date_joined', 'active'],
                "e_commerce", ['name'], [' LIKE '], ["'$spec'"], "", "", "$limit", "$offset")['result'];
    }

    disconnectConnection($con);
    return $stores;
}
