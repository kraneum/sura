<?php

function get_all_users($limit = 0, $offset = 0, $spec = '') {

    $con = makeConnection();
    $users = null;
    $spec = $spec . '%';

    if ($limit <= 0) {
        $users = runSimpleFetchQuery($con, ['users.id', 'users.f_name','users.m_name','users.l_name','users.gender','users.dob','users.email','users.phone','users.health_point_balance','users.general_point_balance', 'users.date_joined','users.last_login','users.active','users.approved'], 
                'users', ['f_name', 'l_name'], [' LIKE ', ' LIKE '], ["'$spec'", "'$spec'"], "", "id", "", "")['result'];
    } else {
        $users = runSimpleFetchQuery($con, ['users.id', 'users.f_name','users.m_name','users.l_name','users.gender','users.dob','users.email','users.phone','users.health_point_balance','users.general_point_balance', 'users.date_joined','users.last_login','users.active','users.approved'], 
                'users', ['f_name', 'l_name'], [' LIKE ', ' LIKE '], ["'$spec'", "'$spec'"], "", "id", "$limit", "$offset")['result'];
    }

    disconnectConnection($con);
    return $users;
}
