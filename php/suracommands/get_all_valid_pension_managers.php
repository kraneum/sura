<?php

function get_all_valid_pension_managers($limit = 0, $offset = 0) {
    $con = makeConnection();
    $trans = null;
    
    if ($limit <= 0) {
        $trans = runSimpleFetchQuery($con, ['id', 'name', 'write_up', 'phone', 'email', 'more_info_link', 'logo_url'], "pension", ["validated", 'approved'], ["=", '='], ['TRUE', 'TRUE'], "", "", "")['result'];
    } else {
        $trans = runSimpleFetchQuery($con, ['id', 'name', 'write_up', 'phone', 'email', 'more_info_link', 'logo_url'], "pension", ["validated", 'approved'], ["=", '='], ['TRUE', 'TRUE'], "", "$limit", "$offset")['result'];
    }
    
    disconnectConnection($con);
    return $trans;
}
