<?php

function get_api_key() {
    require_once('../php/form_validate.php');

    $con = makeConnection();
    $response = [];

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // clean user inputs to prevent sql injections
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                disconnectConnection($con);
                return ['status' => false, 'err' => 'INVALID_DETAILS1'];
            }

//            print_r($_POST);
            $email = $_POST['email'];
            $password = $_POST['password'];

            $validationResult = $form_validate([
                'email' => 'required|maxlength:50',
                'password' => 'required|maxlength:50'
                    ], [
                'email' => $email,
                'password' => $password
            ]);
            
            if (!empty($validationResult)) {
                $response['status'] = false;
                $response['err'] = 'VALIDATION_ERROR';
            } else {

                $queryResult = runSimpleFetchQuery($con, ['api_key'], 'e_commerce', ['email', 'password'], ['=', '='], ["'$email'", "'$password'"], '', '', 1)['result'];

                if (empty($queryResult)) {
                    $response['status'] = false;
                    $response['err'] = 'INVALID_DETAILS2';
                } else {
                    $response['status'] = true;
                    $response['api_key'] = $queryResult;
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'INVALID_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    disconnectConnection($con);
    return $response;
}
