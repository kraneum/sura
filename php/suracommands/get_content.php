<?php

function get_content(...$params) {
    $con = makeConnection();

    $trans = runFetchContentQuery($con, ...$params)['result'];
    
    disconnectConnection($con);
    
    return $trans;
}