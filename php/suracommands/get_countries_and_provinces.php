<?php
require_once('sura_config.php');
require_once('sura_functions.php');

function get_countries_and_provinces() {
    $con = makeConnection();

    $transs = runSimpleFetchQuery($con, ['*'], "countries", [], [], [], "", "", "")['result'];

    for ($i = 0; $i < count($transs); $i++) {

        $this_country = $transs[$i];
        $country_id = $this_country['id'];
        $prv = runSimpleFetchQuery($con, ['*'], "provinces", ["country_id"], ["="], [$country_id], "", "", "")['result'];

        $transs[$i]['provinces'] = $prv;
    }

    if (isset($_GET['pre'])) {
        echo '<pre>' . json_encode($transs, JSON_PRETTY_PRINT) . '</pre>';
    } else {
        echo json_encode($transs, JSON_PRETTY_PRINT);
    }

    disconnectConnection($con);
}