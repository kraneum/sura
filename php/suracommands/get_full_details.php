<?php

function get_full_details() {
    require_once(__DIR__ . '/../form_validate.php');

    $con = makeMedoo();

    if (!$con) {
        return ['status' => false, 'err' => 'DATABASE_CONNECT_ERROR'];
    }
    if ($_SERVER['REQUEST_METHOD'] != 'POST') {
        return ['status' => false, 'err' => 'WRONG_REQUEST_TYPE'];
    }


    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    if (!$_POST) {
        return ['status' => false, 'err' => 'INVALID_DETAILS', 'desc' => 'Check the payload sent'];
    }

    $user_type = isset($_POST['usertype']) ? $_POST['usertype'] : null;
    $user_email = isset($_POST['email']) ? $_POST['email'] : null;
    $user_phone = isset($_POST['phone']) ? $_POST['phone'] : null;

    $validationResult = $form_validate([
        'usertype' => 'required',
        'email' => 'email',
        'phone' => 'phone|minlength:9|maxlength:11'
            ], [
        'usertype' => $user_type,
        'email' => $user_email,
        'phone' => $user_phone
    ]);

    if (!empty($validationResult)) {
        return ['status' => false, 'err' => 'VALIDATION_ERROR', 'desc' => $validationResult];
    }

    $user_details = [];

    if ($user_type == 'subscriber') {
        $balances = $con->get("users", [
            "id",
            "phone",
            "health_point_balance",
            "general_point_balance"
                ], [
            "OR" => [
                "email" => $user_email,
                "phone" => $user_phone
            ]
        ]);
        if (!empty($balances)) {
            $user_id_ = $balances['id'];
            $user_phone_ = $balances['phone'];
            $pol_count = $con->query(
                            'SELECT COUNT(id) as num_signups FROM user_signups WHERE user_id = ' . $user_id_
                    )->fetchAll(PDO::FETCH_ASSOC);
            $pol_active = $con->select("user_signups", "*", [
                "user_id" => $user_id_,
                "active" => 'TRUE'
            ]);
            $point_transfers = $con->select("point_transfers", '*', [
                'to_who' => '08166601864',
                'ORDER' => ['id' => 'DESC'],
                'LIMIT' => 5,
            ]);

            $user_details['health_ipoints'] = $balances['health_point_balance'];
            $user_details['general_ipoints'] = $balances['general_point_balance'];
            $user_details['num_signups'] = $pol_count[0]['num_signups'];
            $user_details['active_policies'] = $pol_active;
            $user_details['last_5_transfers'] = $point_transfers;
        }
    } else if ($user_type == 'merchant') {
        $merchant = $con->get("merchants", '*', [
            "OR" => [
                "contact_email" => $user_email,
                "contact_phone" => $user_phone
            ]
        ]);
        $user_details = $merchant ?: [];
    } else if ($user_type == 'underwriter') {
        $underwriter = $con->get("insurer", [
            'id', 'name', 'write_up', 'insurance_type', 'date_joined', 'phone', 'email', 'rc_number',
            'operating_country', 'redeemable_points', 'more_info_link',
            'logo_url', 'sms_wallet', 'last_login', 'validated', 'approved'
                ], [
            "OR" => [
                "email" => $user_email,
                "phone" => $user_phone
            ]
        ]);

        if (!empty($underwriter)) {
            $policies = $con->select('policies', '*', [
                'insurer_id'=>$underwriter['id']
            ]);
            $user_details = $underwriter ?: [];
            $user_details['policies'] = $policies;
        }
    } else if ($user_type == 'admin') {
        $admin = $con->get("admins", [
            'id', 'first_name', 'middle_name', 'last_name', 'dob', 'phone', 
            'email', 'display_picture_file_name','date_joined', 'last_login'
                ], [
            "OR" => [
                "email" => $user_email,
                "phone" => $user_phone
            ]
        ]);
        $user_details = $admin ?: [];
    }


    return $user_details;
}
