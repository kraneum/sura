<?php

function get_generated_users($type_id = null, $limit = 20, $offset = 0) {

    $con = makeMedoo();

    if (!$con) {
        return ['status' => false, 'err' => 'DATABASE_CONNECT_ERROR'];
    }
    $options = [
        'LIMIT' => [$offset, $limit],
        'ORDER' => ['date_created']
    ];
    $response = null;

    if (empty($type_id)) {
        $response = $con->select("insurer_gen_users", '*', $options);
    } else if ($type_id == '1') {
        $options['type'] = 'HEALTH';
        $response = $con->select("insurer_gen_users", '*', $options);
    } else if ($type_id == '2') {
        $options['type'] = 'GENERAL';
        $response = $con->select("insurer_gen_users", '*', $options);
    }


    return $response;
}
