<?php

function get_generated_users_count() {
    $con = makeMedoo();
    
    $count = $con->count("insurer_gen_users");
    
    return $count;
}
