<?php

function get_health_insurer_by_user($user_id) {
    $con = makeConnection_user();

    $insurer_id = runSimpleFetchQuery($con, ['health_insurer'], "users", ["id"], ["="], [$user_id], "", "", "")['result'];
    $insurer = runSimpleFetchQuery($con, ['id', 'logo_url', 'name'], "insurer", ["id"], ["="], [$insurer_id], "", "", "")['result'];
    
    $trans['insurer'] = $insurer;
    
    disconnectConnection($con);
    
    return $trans;
}