<?php

require_once('sura_config.php');
require_once('sura_functions.php');

function get_insurance_policies($id) {
    $con = makeConnection_user();
    $type_id = $id;

    $ins_policies = runSimpleFetchQuery($con, ['*'], "policies", ['type_id', 'active'], ['=', '='], [$type_id, 1], "", "", "")['result'];

    for ($i = 0; $i < count($ins_policies); $i++) {

        $this_policy = $ins_policies[$i];
        $insurer_id = $this_policy['insurer_id'];

        $insurer = runSimpleFetchQuery($con, ['*'], "insurer", ["id"], ["="], [$insurer_id], "", "", "")['result'];
        $ins_policies[$i]['insurer'] = $insurer;
        
        $tenure_id = $this_policy['insurer_id'];
        $tenure_info = runSimpleFetchQuery($con, ['*'], "payment_structures", ['id'], ['='], [$tenure_id], "", "", "")['result'];
        $ins_policies[$i]['tenure_info'] = $tenure_info;
    }

    disconnectConnection($con);

    return $ins_policies;
}
