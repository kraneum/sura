<?php

function get_insurance_type_id_from_meta($meta) {
    $con = makeConnection_user();

    $trans = runSimpleFetchQuery($con, ['id'], "insurance_types", ["meta"], ["="], ["'$meta'"], "", "", "1")['result'];

    disconnectConnection($con);
    return $trans;
}
