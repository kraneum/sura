<?php

function get_insurance_types() {
    $con = makeConnection_user();

    $ins_types = runSimpleFetchQuery($con, ['*'], "insurance_types", [], [], [], "", "", "")['result'];

    disconnectConnection($con);
    return $ins_types;
}
