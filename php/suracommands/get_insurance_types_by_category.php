<?php

function get_insurance_types_by_category($id) {
    $con = makeConnection_user();
    
    $ins_types = runSimpleFetchORQuery($con, ['*'], "insurance_types", [], [], [], "", "", "")['result'];

    disconnectConnection($con);
    return $ins_types;
}
