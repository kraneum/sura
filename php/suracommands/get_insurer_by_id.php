<?php

function get_insurer_by_id($id) {

    $con = makeConnection_insurer();

    $ins = runSimpleFetchQuery($con, ['id', 'name', 'write_up', 'rc_number', 'account_no', 'insurance_type as type_id', 'phone', 'email',
                'redeemable_points', 'sms_wallet', 'last_login', 'name_change', 'validated', 'approved', 'logo_url'], "insurer", ['id'], ['='], [$id], "", "", "1")['result'];

    if ($ins) {

        $this_ins = $ins[0];

        $insurance_type = runSimpleFetchQuery($con, ['name'], "insurance_types", ["id"], ["="], [$this_ins['type_id']], "", "", "")['result'];
        $ins[0]['type_name'] = $insurance_type;

        $insurer_docs_directory = '../insurer_docs/docs_id_' . $this_ins['id'] . '/';
        $docs = glob($insurer_docs_directory . '*.*');
        foreach ($docs as &$doc) { // ... '&' so as to pass by reference
            $doc = basename($doc);
        }
        $ins[0]['docs'] = $docs;

        $name_docs_directory = '../insurer_docs/docs_id_' . $this_ins['id'] . '/Change of Name Docs/';
        $ndocs = glob($name_docs_directory . '*.*');
        foreach ($ndocs as &$ndoc) {
            $ndoc = basename($ndoc);
        }
        $ins[0]['name_docs'] = $ndocs;
    }

    disconnectConnection($con);
    return $ins;
}
