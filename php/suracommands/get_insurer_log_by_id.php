<?php

function get_insurer_log_by_id($id, $limit = 0, $offset = 0) {
    $con = makeConnection_insurer();
    
    $trans = null;
    
    if ($limit <= 0) {
        $trans = runSimpleFetchQuery($con, ['*'], "insurer_log", ["insurer_id"], ["="], [$id], "", "id DESC", "")['result'];
    } else {
        $trans = runSimpleFetchQuery($con, ['*'], "insurer_log", ["insurer_id"], ["="], [$id], "", "id DESC", "$limit", "$offset")['result'];
    }

    for ($i = 0; $i < count($trans); $i++) {

        $this_signup = $trans[$i];
        $policy_id = $this_signup['policy_id'];

        $policy = runSimpleFetchQuery($con, ['*'], "policies", ["id"], ["="], [$policy_id], "", "", "")['result'];
        $trans[$i]['policy_info'] = $policy;
    }

    disconnectConnection($con);
    return $trans;
}
