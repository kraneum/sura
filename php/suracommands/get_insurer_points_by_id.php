<?php

function get_insurer_points_by_id($id) { // parameter due to call from php file...
    $con = makeConnection();

    $trans['points'] = runSimpleFetchQuery($con, ['redeemable_points'], "insurer", ["id"], ["="], [$id], "", "", "1")['result'];

    disconnectConnection($con);
    return $trans;
}
