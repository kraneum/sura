<?php

function get_insurer_policies($id, $limit = 0, $offset = 0) {
    $con = makeConnection_insurer();
    $trans = null;
    
    if ($limit <= 0) {
        $trans = runSimpleFetchQuery($con, ['*'], "policies", ["insurer_id", "active"], ["=", "="], [$id, 1], "", "id DESC", "")['result'];
    } else {
        $trans = runSimpleFetchQuery($con, ['*'], "policies", ["insurer_id", "active"], ["=", "="], [$id, 1], "", "id DESC", "$limit", "$offset")['result'];
    }
    
    for ($i = 0; $i < count($trans); $i++) {

        $this_policy = $trans[$i];
        $type_id = $this_policy['type_id'];

        $type_info = runSimpleFetchQuery($con, ['*'], "insurance_types", ["id"], ["="], [$type_id], "", "", "")['result'];
        $trans[$i]['type_info'] = $type_info;
        
        $tenure_id = $this_policy['tenure'];
        $tenure_info = runSimpleFetchQuery($con, ['*'], "payment_structures", ['id'], ['='], [$tenure_id], "", "", "1")['result'];
        $trans[$i]['tenure_info'] = $tenure_info;

    }

    disconnectConnection($con);
    return $trans;
}
