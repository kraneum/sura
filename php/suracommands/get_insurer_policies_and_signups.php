<?php

function get_insurer_policies_and_signups($id) {
    $con = makeConnection();

    $ins_policies = runSimpleFetchQuery($con, ['id', 'title'], "policies", ['insurer_id', 'active'], ['=', '='], [$id, 1], "", "", "")['result'];
    $count = 0;
    
    for ($i = 0; $i < count($ins_policies); $i++) {

        $this_policy = $ins_policies[$i];
        $policy_id = $this_policy['id'];

        $num = runCountQuery($con, "user_signups", ['policy_id'], ['='], [$policy_id])['result'][0]['count(id)'];
        $ins_policies[$i]['no_of_signups'] = $num;
        $count += intval($num);
    }
    $ins_policies[count($ins_policies)]['count'] = $count;

    disconnectConnection($con);

    return $ins_policies;
}
