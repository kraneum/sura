<?php

function get_insurer_policies_count_by_id($id) {
    $con = makeConnection_insurer();
    $trans = null;

    $transActive = count(runSimpleFetchQuery($con, ['*'], "policies", ["insurer_id", "active"], ["=", "="], [$id, "'TRUE'"], "", "id DESC", "")['result']);
    $transInactive = count(runSimpleFetchQuery($con, ['*'], "policies", ["insurer_id", "active"], ["=", "="], [$id, "'FALSE'"], "", "id DESC", "")['result']);

    $trans['total'] = $transActive + $transInactive;
    $trans['active'] = $transActive;
    $trans['Inactive'] = $transInactive;
    
    disconnectConnection($con);
    return $trans;
}
