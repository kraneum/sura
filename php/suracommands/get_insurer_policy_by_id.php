<?php

function get_insurer_policy_by_id($id, $policyID) { // parameter due to call from php file...
    $con = makeConnection_insurer();

    $trans = runSimpleFetchQuery($con, ['*'], "policies", ["id", 'insurer_id'], ["=", "="], [$policyID, $id], "", "", "1")['result'];

//    for ($i = 0; $i < count($trans); $i++) {
//
//        $this_signup = $trans[$i];
//        $type_id = $this_signup['type_id'];
//
//        $type_info = runSimpleFetchQuery($con, ['*'], "insurance_types", ["id"], ["="], [$type_id], "", "", "")['result'];
//        $trans[$i]['type_info'] = $type_info;
//    }

    disconnectConnection($con);
    return $trans;
}
