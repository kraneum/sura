<?php

function get_insurers_by_type($type) {
    $con = makeConnection_insurer();

    $trans = runSimpleFetchQuery($con, ['*'], "insurer", ["insurance_type"], ["="], [$type], "", "", "")['result'];
    disconnectConnection($con);
    
    return $trans;
}