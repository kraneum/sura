<?php

function get_insurers_count() {
    $con = makeConnection_insurer();
    $trans = [];

    $_trans = runSimpleFetchQuery($con, ['id', 'validated', 'approved'], 'insurer', [], [], [], '', 'id DESC', '')['result'];

    $trans['total'] = count($_trans);
    $v_count = 0;
    $a_count = 0;

    foreach ($_trans as $this_trans) {
        $this_trans['validated'] == 'TRUE' ? $v_count++ : '';
        $this_trans['approved'] == 'TRUE' ? $a_count++ : '';
    }
    
    $trans['validated'] = $v_count;
    $trans['approved'] = $a_count;
    
    disconnectConnection($con);
    return $trans;
}
