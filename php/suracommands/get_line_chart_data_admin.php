<?php

function get_line_chart_data_admin($_startDate = null, $_endDate = null) {
    $con = makeConnection();

    $startDate = null;
    $endDate = null;

    if (!empty($_startDate)) {
        $startDate = formatDate($_startDate);
    }
    if (!empty($_endDate)) {
        $endDate = formatDate($_endDate);
    }


    $res = runSimpleFetchQuery($con, ['id', 'name'], "insurer", [], [], [], "", "", "", "")['result'];
    $signups = null;

    for ($i = 0; $i < count($res); $i++) {
        $insurer_id = $res[$i]['id'];
        $signups = runFetchLineChartDataAdminQuery($con, $insurer_id, $startDate, $endDate)['result'];
        $res[$i]['purchases'] = $signups;
    }

    disconnectConnection($con);

    return $res;
}

function formatDate($_date) {
    $_date = explode('/', $_date);
    $_date = $_date[2] . '-' . $_date[1] . '-' . $_date[0];
    $date = (new DateTime($_date))->format(DateTime::ISO8601);
    return $date;
}
