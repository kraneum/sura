<?php

function get_line_chart_data_insurer($id, $gender = null, $_startDate = null, $_endDate = null, $active = null, $_num_users = null) {
    $con = makeConnection_insurer();

    $startDate = null;
    $endDate = null;
    $from = null;
    $to = null;

    if (!empty($_startDate)) {
        $startDate = formatDate($_startDate);
    }
    if (!empty($_endDate)) {
        $endDate = formatDate($_endDate);
    }

    if (!empty($_num_users)) {
        $num_users = explode(';', $_num_users);
        $from = $num_users[0];
        $to = $num_users[1];
    }

    $res = runSimpleFetchQuery($con, ['id', 'title'], "policies", ['insurer_id'], ['='], ["'$id'"], "", "", "", "")['result'];
    $count = 0;

    for ($i = 0; $i < count($res); $i++) {
        $policy_id = $res[$i]['id'];
        $signups = runFetchLineChartDataInsurerQuery($con, $policy_id, $gender, $startDate, $endDate, $active)['result'];
        $tempCount = count($signups);
        if (($from == 0 && $to == 0) || ($tempCount >= $from && $tempCount <= $to)) {
            $res[$i]['signups'] = $signups;
            $count += $tempCount;
        } else {
            $res[$i]['signups'] = [];
        }
    }

    $res[count($res)]['total_signups'] = $count;

    disconnectConnection($con);

    return $res;
}

function formatDate($_date) {
    $_date = explode('/', $_date);
    $_date = $_date[2] . '-' . $_date[1] . '-' . $_date[0];
    $date = (new DateTime($_date))->format(DateTime::ISO8601);
    return $date;
}
