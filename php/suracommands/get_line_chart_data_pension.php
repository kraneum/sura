<?php

function get_line_chart_data_pension($id, $gender = null, $_startDate = null, $_endDate = null, $active = null, $_num_users = null) {
    $con = makeConnection();

    $startDate = null;
    $endDate = null;
    $from = null;
    $to = null;

    if (!empty($_startDate)) {
        $startDate = formatDate($_startDate);
//        echo $startDate;
    }
    if (!empty($_endDate)) {
        $endDate = formatDate($_endDate);
//        echo $endDate;
    }

    if (!empty($_num_users)) {
        $num_users = explode(';', $_num_users);
        $from = $num_users[0];
        $to = $num_users[1];
    }

    $res = runSimpleFetchQuery($con, ['id', 'name'], "pension", ['id'], ['='], ["'$id'"], "", "", "", "")['result'];
    $count = 0;

    $pensioner_id = $res[0]['id'];
    $signups = runFetchLineChartDataPensionQuery($con, $pensioner_id, $gender, $startDate, $endDate, $active)['result'];
    $res[0]['signups'] = $signups;

    $res[0]['total_signups'] = count($signups);

    disconnectConnection($con);

    return $res;
}

function formatDate($_date) {
    $_date = explode('/', $_date);
    $_date = $_date[2] . '-' . $_date[1] . '-' . $_date[0];
    $date = (new DateTime($_date))->format(DateTime::ISO8601);
    return $date;
}
