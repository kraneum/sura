<?php

function get_merchant_by_id($id) {

    $con = makeConnection_merchant();

    $merchant = runSimpleFetchQuery($con, ['id', 'name', 'write_up', 'rc_number', 'contact_phone', 'contact_email','point_balance',
                    'name_change', 'deleted', 'approved', 'display_picture'], "merchants", ['id'], ['='], [$id], "", "", "1")['result'];

    if ($merchant) {

        $this_ins = $merchant[0];

        $merchant_docs_directory = '../merchant_docs/docs_id_' . $this_ins['id'] . '/';
        $docs = glob($merchant_docs_directory . '*.*');
        foreach ($docs as &$doc) { // ... '&' so as to pass by reference
            $doc = basename($doc);
        }
        $merchant[0]['docs'] = $docs;

        $name_docs_directory = '../merchant_docs/docs_id_' . $this_ins['id'] . '/Change of Name Docs/';
        $ndocs = glob($name_docs_directory . '*.*');
        foreach ($ndocs as &$ndoc) {
            $ndoc = basename($ndoc);
        }
        $merchant[0]['name_docs'] = $ndocs;
    }

    disconnectConnection($con);
    return $merchant;
}
