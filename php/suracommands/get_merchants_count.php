<?php

function get_merchants_count() {
    $con = makeConnection_merchant();
    $trans = [];

    $_trans = runSimpleFetchQuery($con, ['id', 'deleted', 'approved'], 'merchants', [], [], [], '', 'id DESC', '')['result'];

    $trans['total'] = count($_trans);
    $v_count = 0;
    $a_count = 0;

    foreach ($_trans as $this_trans) {
        $this_trans['deleted'] == '0' ? $a_count++ : '';
        $this_trans['approved'] == '1' ? $v_count++ : '';
    }
    
    $trans['validated'] = $v_count;
    $trans['approved'] = $a_count;
    
    disconnectConnection($con);
    return $trans;
}
