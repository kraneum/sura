<?php

function get_my_payments($id, $signup_id, $limit = 0, $offset = 0) {
    $con = makeConnection_user();
    $trans = null;

    if ($limit <= 0) {
        $trans = runSimpleFetchQuery($con, ['*'], "payment_schedule", ["user_id", "signup_id"], ["=", "="], [$id, $signup_id], "", "id DESC", "")['result'];
    } else {
        $trans = runSimpleFetchQuery($con, ['*'], "payment_schedule", ["user_id", "signup_id"], ["=", "="], [$id, $signup_id], "", "id DESC", "$limit", "$offset")['result'];
    }

    disconnectConnection($con);
    return $trans;
}
