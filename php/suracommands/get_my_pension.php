<?php

function get_my_pension($id) {
    $con = makeConnection();
    
    $trans = runSimpleFetchQuery($con, ['*'], "pension_signups", ["user_id"], ["="], [$id], "", "id DESC", "")['result'];

    for ($i = 0; $i < count($trans); $i++) {

        $this_signup = $trans[$i];
        $pension_id = $this_signup['pension_id'];
        

        $pension_manager = runSimpleFetchQuery($con, ['name', 'write_up', 'more_info_link', 'logo_url', 'phone', 'email'], "pension", ["id"], ["="], [$pension_id], "", "", "1")['result'];
        $trans[$i]['pension_manager'] = $pension_manager;
        
        
    }


    disconnectConnection($con);
    return $trans;
}
