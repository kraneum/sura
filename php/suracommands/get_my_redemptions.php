<?php
if (!isset($_SESSION)){
    session_start();
}
if (!isset($_SESSION['userid'])) {
    exit();
} else {
    $id = $_SESSION['userid'];
}
require_once('sura_config.php');
require_once('sura_functions.php');

function get_my_redemptions() {
    $con = makeConnection_user();
    global $id;
    $trans = runSimpleFetchQuery($con, ['*'], "ussd_codes", ["redeemed_by"], ["="], [$id], "", "", "")['result'];

    if (isset($_GET['pre'])) {
        echo '<pre>' . json_encode($trans, JSON_PRETTY_PRINT) . '</pre>';
    } else {
        echo json_encode($trans, JSON_PRETTY_PRINT);
    }

    disconnectConnection($con);
}