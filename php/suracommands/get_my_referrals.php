<?php

function get_my_referrals($id, $entity = null, $limit = 0, $offset = 0) {
    $con = makeConnection_user();
    $entity = $entity == null ? 'user' : $entity;
    
    $trans = null;

    if ($limit <= 0) {
        $trans = runSerialLeftJointFetchQuery($con, ['referrals.*', 'merchants.name as merchant_name'], ['referrals', 'referees', 'merchants'], [['referees.id = referrals.referee_id'], ['referrals.merchant_id = merchants.id']], ['referees.entity_id', 'referees.entity'], ['=', '='], ["$id", "'$entity'"], '', 'referrals.id DESC', '')['result'];
    } else {
        $trans = runSerialLeftJointFetchQuery($con, ['referrals.*', 'merchants.name as merchant_name'], ['referrals', 'referees', 'merchants'], [['referees.id = referrals.referee_id'], ['referrals.merchant_id = merchants.id']], ['referees.entity_id', 'referees.entity'], ['=', '='], ["$id", "'$entity'"], '', 'referrals.id DESC', "$limit", "$offset")['result'];
    }

    disconnectConnection($con);
    return $trans;
}
