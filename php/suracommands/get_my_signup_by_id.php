<?php

function get_my_signup_by_id($id, $signup_id) {
    $con = makeConnection_user();
    $trans = runSimpleFetchQuery($con, ['*'], "user_signups", ['id', "user_id"], ["=", "="], [$signup_id, $id], "", "id DESC", "")['result'];

    for ($i = 0; $i < count($trans); $i++) {

        $this_signup = $trans[$i];
        $policy_id = $this_signup['policy_id'];

        $policy = runSimpleFetchQuery($con, ['*'], "policies", ["id"], ["="], [$policy_id], "", "", "")['result'];
        $trans[$i]['policy_info'] = $policy;
        
        $typeID = $policy[0]['type_id'];
        $ins_type = runSimpleFetchQuery($con, ['name'], 'insurance_types', ['id'], ['='], [$typeID], '', '', '')['result'];
        $trans[$i]['policy_info'][0]['insurance_type'] = $ins_type;

        $insurerID = $policy[0]['insurer_id'];
        if($insurerID == 0) {
            $insurerID = runSimpleFetchQuery($con, ['insurer_id'], "uic_signups", ["user_signup_id"], ["="], [$this_signup['id']], "", "", "")['result'];
            $insurer = runSimpleFetchQuery($con, ['*'], "insurer", ["id"], ["="], [$insurerID], "", "", "")['result'];
        } else {
            $insurer = runSimpleFetchQuery($con, ['*'], "insurer", ["id"], ["="], [$insurerID], "", "", "")['result'];
        }
        $trans[$i]['insurer'] = $insurer;

        $payments = runSimpleFetchQuery($con, ['*'], "payment_schedule", ["signup_id"], ["="], [$this_signup['id']], "", "", "")['result'];
        $trans[$i]['payment_schedule'] = $payments;
    }

    disconnectConnection($con);
    return $trans;
}
