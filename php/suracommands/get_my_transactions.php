<?php

function get_my_transactions($id, $limit = 0, $offset = 0) {
    $con = makeConnection_user();
    $trans = null;

    if ($limit <= 0) {
        $trans = runFetchUserTransactions($con, $id, "id DESC", "")['result'];
    } else {
        $trans = runFetchUserTransactions($con, $id, "id DESC", "$limit", "$offset")['result'];
    }
    
    for ($i = 0; $i < count($trans); $i++) {
        
        $this_trans = $trans[$i];
        $from_whom_id = $this_trans['from_whom'];
        $to_who_id = $this_trans['to_who'];
        $type_id = $this_trans['type_id'];

        $ref = null; // for determing the display name of other transfer party...
        $trans_type = null; // for determining whether sender or receiver...

        if ($type_id == 1) { // for point top up...
            $ref = runSimpleFetchQuery($con, ['name'], "merchants", ["id"], ["="], [$from_whom_id], "", "", "")['result'];
            $trans_type = 'credit';
        } else if ($type_id == 2 || $type_id == 3) { // for insurance signup...
            $ref = runSimpleFetchQuery($con, ['name'], "insurer", ["id"], ["="], [$to_who_id], "", "", "")['result'];
            $trans_type = 'debit';
        } else if ($type_id == 4) { // for user to user transfers...
            if ($id == $from_whom_id) {
                $ref = runSimpleFetchQuery($con, ['phone'], "users", ["id"], ["="], [$to_who_id], "", "", "")['result'];
                $trans_type = 'debit';
            } else if ($id == $to_who_id) {
                $ref = runSimpleFetchQuery($con, ['phone'], "users", ["id"], ["="], [$from_whom_id], "", "", "")['result'];
                $trans_type = 'credit';
            }
        } else if ($type_id == 7) {// for agent to user transfers...
            $ref = $from_whom_id;
            $trans_type = 'credit';
        } else if ($type_id == 9) {// for payments made to e-merchants...
            $ref = runSimpleFetchQuery($con, ['name'], "e_commerce", ["id"], ["="], [$to_who_id], "", "", "")['result'];
            $trans_type = 'debit';
        } else if ($type_id == 10) {// for pension topup...
            $ref = runSimpleFetchQuery($con, ['name'], "pension", ["id"], ["="], [$to_who_id], "", "", "")['result'];
            $trans_type = 'debit';
        } else if ($type_id == 14) {// for pension topup...
            $ref = 'UICI';
            $trans_type = 'credit';
        }

        $trans[$i]['trans_type'] = $trans_type;
        $trans[$i]['ref'] = $ref;
    }

    disconnectConnection($con);
    return $trans;
}
