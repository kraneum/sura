<?php

function get_notification_count_agent($id) {
    $con = makeConnection();

    $_trans = runSimpleFetchNotificationsQuery($con, ['id','agent_id'], 'agent_notifications', ['agent_id', 'agent_id', 'unread'], ['=', '=', '='], ["'$id'", "'0'", "'TRUE'"], '', 'id DESC', '')['result'];


    foreach ($_trans as $this_trans) {
        $notif_id = $this_trans['id'];
        $agent_id = $this_trans['agent_id'];

        if ($agent_id == 0) {
            $ref = runSimpleFetchQuery($con, ['id'], "uic_notif_ref", ["notif_id", 'entity', 'entity_id'], ["=", '=', '='], [$notif_id, "'AGENT'", "'$id'"], "", "", "1")['result'];
            if (empty($ref)) {
                $trans[] = $this_trans;
            }
        } else {
            $trans[] = $this_trans;
        }
    }

    disconnectConnection($con);
    return ['count' => count($trans)];
}
