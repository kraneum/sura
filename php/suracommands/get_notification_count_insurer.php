<?php

function get_notification_count_insurer($id) {
    $con = makeConnection_insurer();
    $trans = [];

    $_trans = runSimpleFetchNotificationsQuery($con, ['id','insurer_id'], 'insurer_notifications', ['insurer_id', 'insurer_id', 'unread'], ['=', '=', '='], ["'$id'", "'0'", "'TRUE'"], '', 'id DESC', '')['result'];

    foreach ($_trans as $this_trans) {
        $notif_id = $this_trans['id'];
        $insurer_id = $this_trans['insurer_id'];

        if ($insurer_id == 0) {
            $ref = runSimpleFetchQuery($con, ['id'], "uic_notif_ref", ["notif_id", 'entity', 'entity_id'], ["=", '=', '='], [$notif_id, "'INSURER'", "'$id'"], "", "", "1")['result'];
            if (empty($ref)) {
                $trans[] = $this_trans;
            }
        } else {
            $trans[] = $this_trans;
        }
    }
    
    disconnectConnection($con);
    return ['count' => count($trans)];
}
