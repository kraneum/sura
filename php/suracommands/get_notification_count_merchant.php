<?php

function get_notification_count_merchant($id) {
    $con = makeConnection_merchant();

    $_trans = runSimpleFetchNotificationsQuery($con, ['id','merchant_id'], 'merchant_notifications', ['merchant_id', 'merchant_id', 'unread'], ['=', '=', '='], ["'$id'", "'0'", "'TRUE'"], '', 'id DESC', '')['result'];


    foreach ($_trans as $this_trans) {
        $notif_id = $this_trans['id'];
        $merchant_id = $this_trans['merchant_id'];

        if ($merchant_id == 0) {
            $ref = runSimpleFetchQuery($con, ['id'], "uic_notif_ref", ["notif_id", 'entity', 'entity_id'], ["=", '=', '='], [$notif_id, "'MERCHANT'", "'$id'"], "", "", "1")['result'];
            if (empty($ref)) {
                $trans[] = $this_trans;
            }
        } else {
            $trans[] = $this_trans;
        }
    }

    disconnectConnection($con);
    return ['count' => count($trans)];
}
