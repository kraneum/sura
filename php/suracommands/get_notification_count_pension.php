<?php

function get_notification_count_pension($id) {
    $con = makeConnection();

    $_trans = runSimpleFetchNotificationsQuery($con, ['id','pension_id'], 'pension_notifications', ['pension_id', 'pension_id', 'unread'], ['=', '=', '='], ["'$id'", "'0'", "'TRUE'"], '', 'id DESC', '')['result'];


    foreach ($_trans as $this_trans) {
        $notif_id = $this_trans['id'];
        $pension_id = $this_trans['pension_id'];

        if ($pension_id == 0) {
            $ref = runSimpleFetchQuery($con, ['id'], "uic_notif_ref", ["notif_id", 'entity', 'entity_id'], ["=", '=', '='], [$notif_id, "'PENSION'", "'$id'"], "", "", "1")['result'];
            if (empty($ref)) {
                $trans[] = $this_trans;
            }
        } else {
            $trans[] = $this_trans;
        }
    }

    disconnectConnection($con);
    return ['count' => count($trans)];
}
