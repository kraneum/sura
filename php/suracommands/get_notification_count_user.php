<?php

function get_notification_count_user($id, $limit = 0, $offset = 0) {
    $con = makeConnection_user();
    $trans = [];

    $_trans = runSimpleFetchNotificationsQuery($con, ['id','sender_id', 'rec_id'], 'user_notifications', ['rec_id', 'rec_id', 'unread'], ['=', '=', '='], ["'$id'", "'0'", "'TRUE'"], '', 'id DESC', '')['result'];


    foreach ($_trans as $this_trans) {
        $notif_id = $this_trans['id'];
        $sender_id = $this_trans['sender_id'];
        $rec_id = $this_trans['rec_id'];

        if ($sender_id == 0 || $rec_id == 0) {
            $ref = runSimpleFetchQuery($con, ['id'], "uic_notif_ref", ["notif_id", 'entity', 'entity_id'], ["=", '=', '='], [$notif_id, "'USER'", "'$id'"], "", "", "1")['result'];
            if (empty($ref)) {
                $trans[] = $this_trans;
            }
        } else {
            $trans[] = $this_trans;
        }
    }

    disconnectConnection($con);
    return ['count' => count($trans)];
}
