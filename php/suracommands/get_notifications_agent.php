<?php

$trans = [];
$remaining = true;

function get_notifications_agent($id, $limit = 0, $offset = 0) {
    $con = makeConnection();
    global $trans, $remaining;

    if ($limit <= 0) {
        $_trans = runSimpleFetchNotificationsQuery($con, ['id', 'message', 'agent_id', 'unread'], 'agent_notifications', ['agent_id', 'agent_id', 'unread'], ['=', '=', '='], ["'$id'", "'0'", "'TRUE'"], '', 'id DESC', '')['result'];
    } else {
        $_trans = runSimpleFetchNotificationsQuery($con, ['id', 'message', 'agent_id', 'unread'], 'agent_notifications', ['agent_id', 'agent_id', 'unread'], ['=', '=', '='], ["'$id'", "'0'", "'TRUE'"], '', 'id DESC', "$limit", "$offset")['result'];

        if (count($_trans) < $limit) {
            $remaining = false;
        }
    }

    foreach ($_trans as $this_trans) {
        $notif_id = $this_trans['id'];
        $agent_id = $this_trans['agent_id'];

        if ($agent_id == 0) {
            $ref = runSimpleFetchQuery($con, ['id'], "uic_notif_ref", ["notif_id", 'entity', 'entity_id'], ["=", '=', '='], [$notif_id, "'AGENT'", "'$id'"], "", "", "1")['result'];
            if (empty($ref)) {
                $trans[] = $this_trans;
            }
        } else {
            $trans[] = $this_trans;
        }

        if (count($trans) == $limit && $limit > 0) {
            break;
        }
    }

    if (count($trans) == $limit || $remaining == false || $limit <= 0) {
        disconnectConnection($con);
    } else {
        update($id, $limit, $offset);
    }
    return $trans;
}

function update($id, $limit, $offset) {
    get_notifications_agent($id, $limit, $offset + $limit);
}
