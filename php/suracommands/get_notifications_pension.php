<?php

$trans = [];
$remaining = true;

function get_notifications_pension($id, $limit = 0, $offset = 0) {
    $con = makeConnection();
    global $trans, $remaining;

    if ($limit <= 0) {
        $_trans = runSimpleFetchNotificationsQuery($con, ['id', 'message', 'pension_id', 'unread'], 'pension_notifications', ['pension_id', 'pension_id', 'unread'], ['=', '=', '='], ["'$id'", "'0'", "'TRUE'"], '', 'id DESC', '')['result'];
    } else {
        $_trans = runSimpleFetchNotificationsQuery($con, ['id', 'message', 'pension_id', 'unread'], 'pension_notifications', ['pension_id', 'pension_id', 'unread'], ['=', '=', '='], ["'$id'", "'0'", "'TRUE'"], '', 'id DESC', "$limit", "$offset")['result'];

        if (count($_trans) < $limit) {
            $remaining = false;
        }
    }

    foreach ($_trans as $this_trans) {
        $notif_id = $this_trans['id'];
        $pension_id = $this_trans['pension_id'];

        if ($pension_id == 0) {
            $ref = runSimpleFetchQuery($con, ['id'], "uic_notif_ref", ["notif_id", 'entity', 'entity_id'], ["=", '=', '='], [$notif_id, "'PENSION'", "'$id'"], "", "", "1")['result'];
            if (empty($ref)) {
                $trans[] = $this_trans;
            }
        } else {
            $trans[] = $this_trans;
        }

        if (count($trans) == $limit && $limit > 0) {
            break;
        }
    }

    if (count($trans) == $limit || $remaining == false || $limit <= 0) {
        disconnectConnection($con);
    } else {
        update($id, $limit, $offset);
    }
    return $trans;
}

function update($id, $limit, $offset) {
    get_notifications_pension($id, $limit, $offset + $limit);
}
