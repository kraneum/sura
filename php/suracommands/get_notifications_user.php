<?php

$trans = [];
$remaining = true;

function get_notifications_user($id, $limit = 10, $offset = 0) {
    $con = makeConnection_user();
    global $trans, $remaining;


    if ($limit <= 0) {
        $_trans = runSimpleFetchNotificationsQuery($con, ['id', 'message', 'sender_id', 'rec_id', 'unread'], 'user_notifications', ['rec_id', 'rec_id', 'unread'], ['=', '=', '='], ["'$id'", "'0'", "'TRUE'"], '', 'id DESC', '')['result'];
    } else {
        $_trans = runSimpleFetchNotificationsQuery($con, ['id', 'message', 'sender_id', 'rec_id', 'unread'], 'user_notifications', ['rec_id', 'rec_id', 'unread'], ['=', '=', '='], ["'$id'", "'0'", "'TRUE'"], '', 'id DESC', "$limit", "$offset")['result'];
        if (count($_trans) < $limit) {
            $remaining = false;
        }
    }

    foreach ($_trans as $this_trans) {
        $notif_id = $this_trans['id'];
        $sender_id = $this_trans['sender_id'];
        $rec_id = $this_trans['rec_id'];

        if ($sender_id == 0 || $rec_id == 0) {
            $ref = runSimpleFetchQuery($con, ['id'], "uic_notif_ref", ["notif_id", 'entity', 'entity_id'], ["=", '=', '='], [$notif_id, "'USER'", "'$id'"], "", "", "1")['result'];
            if (empty($ref)) {
                $trans[] = $this_trans;
            }
        } else {
            $trans[] = $this_trans;
        }

        if (count($trans) == $limit && $limit > 0) {
            break;
        }
    }

    if (count($trans) == $limit || $remaining == false || $limit <= 0) {
        disconnectConnection($con);
    } else {
        update($id, $limit, $offset);
    }
    return $trans;
}

function update($id, $limit, $offset) {
    get_notifications_user($id, $limit, $offset + $limit);
}
