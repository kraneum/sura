<?php

function get_payment_structures() {
    $con = makeConnection();

    $trans = runSimpleFetchQuery($con, ['*'], "payment_structures", [], [], [], "", "", "")['result'];

    disconnectConnection($con);
    return $trans;
}
