<?php

function get_pension_manager_by_id($id) {

    $con = makeConnection();

    $pen = runSimpleFetchQuery($con, ['id', 'name', 'write_up', 'rc_number', 'account_no', 'phone', 'email',
                'redeemable_points', 'name_change', 'last_login', 'validated', 'approved', 'logo_url'], "pension", ['id'], ['='], [$id], "", "", "1")['result'];

    if ($pen) {
        $this_ins = $pen[0];

        $pension_docs_directory = '../pension_docs/docs_id_' . $this_ins['id'] . '/';
        $docs = glob($pension_docs_directory . '*.*');
        foreach ($docs as &$doc) { // ... '&' so as to pass by reference
            $doc = basename($doc);
        }
        $pen[0]['docs'] = $docs;

        $name_docs_directory = '../pension_docs/docs_id_' . $this_ins['id'] . '/Change of Name Docs/';
        $ndocs = glob($name_docs_directory . '*.*');
        foreach ($ndocs as &$ndoc) {
            $ndoc = basename($ndoc);
        }
        $pen[0]['name_docs'] = $ndocs;
    }

    disconnectConnection($con);
    return $pen;
}
