<?php

function get_pension_signups_count($id) {
    $con = makeConnection();
    $trans = null;

    $trans = count(runSimpleFetchQuery($con, ['*'], "pension_signups", ["pension_id"], ["="], [$id], "", "id DESC", "")['result']);

    
    disconnectConnection($con);
    return $trans;
}
