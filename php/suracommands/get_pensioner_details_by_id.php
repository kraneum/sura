<?php

function get_pensioner_details_by_id($id) {
    $con = makeConnection();
    
    $trans = runSimpleFetchQuery($con, ['name', 'write_up', 'phone', 'email', 'rc_number', 'logo_url', 'more_info_link'], "pension", ["id"], ["="], [$id], "", "id DESC", "")['result'];

    disconnectConnection($con);
    return $trans;
}
