<?php

function get_pie_chart_data($id, $gender = null, $_startDate = null, $_endDate = null, $active = null, $_num_users = null) {
    $con = makeConnection_user();
    
    $startDate = null;
    $endDate = null;
    $from = null;
    $to = null;

    if (!empty($_startDate)) {
        $startDate = formatDate($_startDate);
    }
    if (!empty($_endDate)) {
        $endDate = formatDate($_endDate);
    }

    if (!empty($_num_users)) {
        $num_users = explode(';', $_num_users);
        $from = $num_users[0];
        $to = $num_users[1];
    }

    $res = runFetchPieChartDataQuery($con, $id, $gender, $startDate, $endDate, $active, $from, $to)['result'];

    $count = 0;

    foreach ($res as $r) {
        $count += intval($r['num_users']);
    }
    $res[count($res)]['count'] = $count;

    disconnectConnection($con);

    return $res;
}

function formatDate($_date) {
    $_date = split('/', $_date);
    $_date = $_date[2] . '-' . $_date[1] . '-' . $_date[0];
    $date = (new DateTime($_date))->format(DateTime::ISO8601);
    return $date;
}
