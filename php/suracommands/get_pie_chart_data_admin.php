<?php

function get_pie_chart_data_admin($_startDate = null, $_endDate = null) {
    $con = makeConnection();

    $startDate = null;
    $endDate = null;

    if (!empty($_startDate)) {
        $startDate = formatDate($_startDate);
    }
    if (!empty($_endDate)) {
        $endDate = formatDate($_endDate);
    }

    $merchants = runSimpleFetchQuery($con, ['name'], 'merchants', [], [], [], '', '', '')['result'];

    foreach ($merchants as &$m) { // .. observe the pass by reference symbol '&'
        $m['points'] = 0;
    }
    
    $merchantCount = count($merchants);

    $res = runFetchPieChartDataAdminQuery($con, $startDate, $endDate)['result'];
    $resCount = count($res);

    for ($i = 0; $i < $resCount; $i++) {
        $_mer = $res[$i]['name'];
        for ($j = 0; $j < $merchantCount; $j++) {
            if ($merchants[$j]['name'] == $_mer) {
                $merchants[$j]['points'] += intval($res[$i]['point_amount']);
            }
        }
    }
    
    disconnectConnection($con);

    return $merchants;
}

function formatDate($_date) {
    $_date = explode('/', $_date);
    $_date = $_date[2] . '-' . $_date[1] . '-' . $_date[0];
    $date = (new DateTime($_date))->format(DateTime::ISO8601);
    return $date;
}
