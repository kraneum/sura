<?php

function get_point_purchase_count($entity, $id = null) {

    $con = makeConnection_user();

    session_start();
    $id = $id == 'null' ? $_SESSION['company_id'] : $id;

    if ($entity == 'merchant') {
        $ins = runSimpleFetchQuery($con, [' count(id) '], "point_purchases", ['merchant_id'], ['='], ["$id"], "", "", "")['result'];
    }

    disconnectConnection($con);
    return $ins;
}
