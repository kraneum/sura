<?php

function get_point_purchases_by_id($entity, $id = null, $limit = 0, $offset = 0) {

    $con = makeConnection();

    session_start();
    $id = $id == 'null' ? $_SESSION['company_id'] : $id;

    if ($entity == 'merchant') {
        if ($limit <= 0) {
            $ins = runSimpleFetchQuery($con, ['*'], "point_purchases", ['merchant_id'], ['='], ["$id"], "", "id desc", "")['result'];
        } else {
            $ins = runSimpleFetchQuery($con, ['*'], "point_purchases", ['merchant_id'], ['='], ["$id"], "", "id desc", "$limit", "$offset")['result'];
        }
    }

    disconnectConnection($con);
    return $ins;
}
