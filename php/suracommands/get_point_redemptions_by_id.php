<?php

function get_point_redemptions_by_id($entity, $id = null, $limit = 0, $offset = 0) {

    $con = makeConnection();

    session_start();
    $id = $id == 'null' ? $_SESSION['company_id'] : $id;

    $columns = [
        'redemptions.id',
        'ussd_codes.code',
        'ussd_codes.point_worth',
        'users.phone',
        'ussd_codes.merchant_id',
        'redemptions.redeemed_date'];

    if ($entity == 'merchant') {
        if ($limit <= 0) {
            $res = runSerialLeftJointFetchQuery($con, $columns, ['ussd_codes', 'redemptions', 'users'], [
                        ['ussd_codes.redemption_id', '=', 'redemptions.id'],
                        ['redemptions.redeemed_by', '=', 'users.id']
                            ], ['ussd_codes.merchant_id', 'ussd_codes.redemption_id'], ['=', '!='], [$id, 0], "", "redemptions.id desc", "", "")['result'];
        } else {
            $res = runSerialLeftJointFetchQuery($con, $columns, ['ussd_codes', 'redemptions', 'users'], [
                        ['ussd_codes.redemption_id', '=', 'redemptions.id'],
                        ['redemptions.redeemed_by', '=', 'users.id']
                            ], ['ussd_codes.merchant_id', 'ussd_codes.redemption_id'], ['=', '!='], [$id, 0], "", "redemptions.id desc", "$limit", "$offset")['result'];
            ;
        }
    }

    disconnectConnection($con);
    return $res;
}
