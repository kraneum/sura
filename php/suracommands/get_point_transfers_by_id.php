<?php

function get_point_transfers_by_id($id = null, $transtype = null, $point_worth = null, $to = null, $_startDate = null, $_endDate = null, $limit = 0, $offset = 0) {

    $con = makeConnection();

    session_start();
    $from = $id == 'null' ? $_SESSION['company_id'] : $id;

    $startDate = null;
    $endDate = null;

    if (!empty($_startDate)) {
        $startDate = (new DateTime($_startDate))->format(DateTime::ISO8601);
    }
    if (!empty($_endDate)) {
        $endDate = (new DateTime($_endDate))->format(DateTime::ISO8601);
    }

    $trans = runGetPointTransfer($con, $transtype, $point_worth, $from, $to, $startDate, $endDate, $limit, $offset)['result'];

    foreach ($trans as $i => $t) {
        if ($t['type_id'] == 5) {
//            $res = runSimpleFetchQuery($con, ['id'], "agent", ['id'], ['='], [$t['to_who']], "", "", 1)['result'];
//            $trans[$i]['entity_ref'] = $res;
            $trans[$i]['entity'] = 'agent';
        } else if ($t['type_id'] == 13) {
            $res = runSimpleFetchQuery($con, ["name"], "merchants", ['id'], ['='], [$t['to_who']], "", "", 1)['result'];
            $trans[$i]['entity_ref'] = $res;
            $trans[$i]['entity'] = 'merchant';
        }
    }
    
    $trans[] = ['count' => count(runGetPointTransfer($con, $transtype, $point_worth, $from, $to, $startDate, $endDate)['result'])];

    disconnectConnection($con);
    return $trans;
}
