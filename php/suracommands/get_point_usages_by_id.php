<?php

function get_point_usages_by_id($id = null, $point_worth = null, $to = null, $gender = null, 
        $location = null,  $_startDate = null, $_endDate = null, $limit = 0, $offset = 0) {

    $con = makeConnection();
    
    session_start();
    $from = $id == 'null' ? $_SESSION['company_id'] : $id;
    
    $startDate = null;
    $endDate = null;

    if (!empty($_startDate)) {
        $startDate = (new DateTime($_startDate))->format(DateTime::ISO8601);
    }
    if (!empty($_endDate)) {
        $endDate = (new DateTime($_endDate))->format(DateTime::ISO8601);
    }

    $trans = runGetPointUsage($con, $point_worth, $from, $to, $gender, $location, $startDate, $endDate, $limit, $offset)['result'];

    $trans[] = ['count' => count(runGetPointUsage($con, $point_worth, $from, $to, $gender, $location, $startDate, $endDate)['result'])];

    disconnectConnection($con);
    return $trans;
}
