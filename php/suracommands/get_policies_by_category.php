<?php

function get_policies_by_category($cat_id, $limit = 0, $offset = 0, $active = 1) {
    $con = makeConnection_user();

    $trans = null;

    if ($limit <= 0) {
        $trans = runPoliciesByCategoryQuery($con, $cat_id, $active, "")['result'];
    } else {
        $trans = runPoliciesByCategoryQuery($con, $cat_id, $active, "$limit", "$offset")['result'];
    }
    
    for ($i = 0; $i < count($trans); $i++) {

        $this_policy = $trans[$i];
        $insurer_id = $this_policy['insurer_id'];
        $tenure_id = $this_policy['tenure'];
        
        $tenure = runSimpleFetchQuery($con, ['*'], "payment_structures", ["id"], ["="], [$tenure_id], "", "", "")['result'];
        $trans[$i]['tenure_info'] = $tenure;

        $insurer = runSimpleFetchQuery($con, ['*'], "insurer", ["id"], ["="], [$insurer_id], "", "", "")['result'];
        $trans[$i]['insurer'] = $insurer;
    }

    disconnectConnection($con);
    return $trans;
}
