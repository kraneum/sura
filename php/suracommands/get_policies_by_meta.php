<?php
require_once('sura_config.php');
require_once('sura_functions.php');

function get_policies_by_meta($keys, $comp, $values) {

    $con = makeConnection_user();
    $output = array();
    $transactions = array();

    $comparators_in = array();

    $comparators = ['<', '<=', '=', '>=', '>'];

    for ($i = 0; $i < count($comp); $i++) {
        $index = $comp[$i];
        $chosen_comp = $comparators[$index];
        $comparators_in[] = $chosen_comp;
    }



    $policies = runSimpleFetchQuery($con, ['*'], "policies", $keys, $comparators_in, $values, "", "", "")['result'];


    if (isset($_GET['pre'])) {
        echo '<pre>' . json_encode($policies, JSON_PRETTY_PRINT) . '</pre>';
    } else {
        echo json_encode($policies, JSON_PRETTY_PRINT);
    }

    disconnectConnection($con);
}