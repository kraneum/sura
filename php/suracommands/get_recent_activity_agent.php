<?php

function get_recent_activity_agent($id, $limit = 0, $offset = 0) {
    $con = makeConnection();
    $trans = null;

    if ($limit <= 0) {
        $trans = runSimpleFetchQuery($con, ['*'], "agent_log", ["agent_id"], ["="], [$id], "", "id DESC", "")['result'];
    } else {
        $trans = runSimpleFetchQuery($con, ['*'], "agent_log", ["agent_id"], ["="], [$id], "", "id DESC", "$limit", "$offset")['result'];
    }

    for ($i = 0; $i < count($trans); $i++) {

        $this_trans = $trans[$i];
        $action = $this_trans['action'];
        $info = $this_trans['info'];

        $ref = null; // refers to "info" information...

        if ($action == 'TRANSFER_POINTS_USER') {
            $ref = runSimpleFetchQuery($con, ['*'], "point_transfers", ["id"], ["="], [$info], "", "", "1")['result'];
            $ref[0]['user'] = $ref[0]['to_who'];
        } else if ($action == 'TRANSFER_POINTS_AGENT') {
            $ref = runSimpleFetchQuery($con, ['*'], "point_transfers", ["id"], ["="], [$info], "", "", "1")['result'];
            $ref[0]['agent'] = runSimpleFetchQuery($con, ['phone'], "agent", ["id"], ["="], [$ref[0]['to_who']], "", "", "1")['result'];
        } else if ($action == 'RECEIVED_POINTS_MERCHANT') {
            $ref = runSimpleFetchQuery($con, ['*'], "point_transfers", ["id"], ["="], [$info], "", "", "1")['result'];
            $ref[0]['merchant'] = runSimpleFetchQuery($con, ['name'], "merchants", ["id"], ["="], [$ref[0]['from_whom']], "", "", "1")['result'];
        } else if ($action == 'RECEIVED_POINTS_AGENT') {// for agent to user transfers...
            $ref = runSimpleFetchQuery($con, ['*'], "point_transfers", ["id"], ["="], [$info], "", "", "1")['result'];
            $ref[0]['agent'] = runSimpleFetchQuery($con, ['phone'], "agent", ["id"], ["="], [$ref[0]['from_whom']], "", "", "1")['result'];
        }

        $trans[$i]['ref'] = $ref;
    }

    disconnectConnection($con);
    return $trans;
}
