<?php

function get_recent_activity_insurer($id, $limit = 0, $offset = 0) {
    $con = makeConnection_insurer();
    $trans = null;

    if ($limit <= 0) {
        $trans = runSimpleFetchQuery($con, ['*'], "insurer_log", ["insurer_id"], ["="], [$id], "", "id DESC", "")['result'];
    } else {
        $trans = runSimpleFetchQuery($con, ['*'], "insurer_log", ["insurer_id"], ["="], [$id], "", "id DESC", "$limit", "$offset")['result'];
    }
    
    for ($i = 0; $i < count($trans); $i++) {
        
        $this_trans = $trans[$i];
        $action = $this_trans['action'];
        $info = $this_trans['info'];
        
        $ref = null; // refers to "info" information...

        if ($action == 'CREATE_POLICY' || $action == 'EDIT_POLICY' || $action == 'DEACTIVATE_POLICY' || $action == 'USER_SIGNUP' || $action == 'POLICY_RENEW') {
            $ref = runSimpleFetchQuery($con, ['*'], "policies", ["id"], ["="], [$info], "", "", "1")['result'];
        }

        $trans[$i]['ref'] = $ref;
    }

    disconnectConnection($con);
    return $trans;
}
