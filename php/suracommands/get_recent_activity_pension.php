<?php

function get_recent_activity_pension($id, $limit = 0, $offset = 0) {
    $con = makeConnection();
    $trans = null;

    if ($limit <= 0) {
        $trans = runSimpleFetchQuery($con, ['*'], "pension_log", ["pension_id"], ["="], [$id], "", "id DESC", "")['result'];
    } else {
        $trans = runSimpleFetchQuery($con, ['*'], "pension_log", ["pension_id"], ["="], [$id], "", "id DESC", "$limit", "$offset")['result'];
    }
    
    for ($i = 0; $i < count($trans); $i++) {
        
        $this_trans = $trans[$i];
        $action = $this_trans['action'];
        $info = $this_trans['info'];
        
        $ref = null; // refers to "info" information...

        if ($action == 'PENSION_SIGNUP') {
            $ref = runSimpleFetchQuery($con, ['f_name', 'l_name', 'phone'], "users", ["id"], ["="], [$info], "", "", "1")['result'];
        } else if ($action == 'PENSION_TOPUP') {
            $ref = runSimpleFetchQuery($con, ['point_worth', 'from_whom'], "transactions", ["id"], ["="], [$info], "", "", "1")['result'];
            $ref[0]['user'] = runSimpleFetchQuery($con, ['f_name', 'l_name', 'phone'], "users", ["id"], ["="], [$ref[0]['from_whom']], "", "", "1")['result'];
        }

        $trans[$i]['ref'] = $ref;
    }

    disconnectConnection($con);
    return $trans;
}
