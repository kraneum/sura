<?php

function get_recent_activity_user($id, $limit = 0, $offset = 0) {
    $con = makeConnection_user();
    $trans = null;

    if ($limit <= 0) {
        $trans = runSimpleFetchQuery($con, ['*'], "user_log", ["user_id"], ["="], [$id], "", "id DESC", "")['result'];
    } else {
        $trans = runSimpleFetchQuery($con, ['*'], "user_log", ["user_id"], ["="], [$id], "", "id DESC", "$limit", "$offset")['result'];
    }

    for ($i = 0; $i < count($trans); $i++) {

        $this_trans = $trans[$i];
        $action = $this_trans['action'];
        $info = $this_trans['info'];

        $ref = null; // refers to "info" information...

        if ($action == 'BUY_POLICY' || $action == 'RENEW_POLICY') {
            $ref = runSimpleFetchQuery($con, ['*'], "policies", ["id"], ["="], [$info], "", "", "1")['result'];
        } else if ($action == 'LOAD_PIN') {
            $ref = runSimpleFetchQuery($con, ['*'], "ussd_codes", ["id"], ["="], [$info], "", "", "1")['result'];
            $ref[0]['merchant'] = runSimpleFetchQuery($con, ['name'], "merchants", ["id"], ["="], [$ref[0]['merchant_id']], "", "", "1")['result'];
        } else if ($action == 'MAKE_PAYMENT') {
            $ref = runSimpleFetchQuery($con, ['*'], "transactions", ["id"], ["="], [$info], "", "", "1")['result'];
            $ref[0]['store'] = runSimpleFetchQuery($con, ['name'], "e_commerce", ["id"], ["="], [$ref[0]['to_who']], "", "", "1")['result'];
        } else if ($action == 'TRANSFER_POINTS') {
            $ref = runSimpleFetchQuery($con, ['*'], "transactions", ["id"], ["="], [$info], "", "", "1")['result'];
            $ref[0]['user'] = runSimpleFetchQuery($con, ['phone'], "users", ["id"], ["="], [$ref[0]['to_who']], "", "", "1")['result'];
        } else if ($action == 'RECEIVED_POINTS_USER') {
            $ref = runSimpleFetchQuery($con, ['*'], "transactions", ["id"], ["="], [$info], "", "", "1")['result'];
            $ref[0]['user'] = runSimpleFetchQuery($con, ['phone'], "users", ["id"], ["="], [$ref[0]['from_whom']], "", "", "1")['result'];
        } else if ($action == 'RECEIVED_POINTS_AGENT') {// for agent to user transfers...
            $ref = runSimpleFetchQuery($con, ['*'], "transactions", ["id"], ["="], [$info], "", "", "1")['result'];
            $ref[0]['agent'] = runSimpleFetchQuery($con, ['phone'], "agent", ["id"], ["="], [$ref[0]['from_whom']], "", "", "1")['result'];
        } else if ($action == 'PENSION_SIGNUP') {// for agent to user transfers...
            $ref = runSimpleFetchQuery($con, ['name', 'phone', 'email'], "pension", ["id"], ["="], [$info], "", "", "1")['result'];
        } else if ($action == 'PENSION_TOPUP') {// for agent to user transfers...
            $ref = runSimpleFetchQuery($con, ['*'], "transactions", ["id"], ["="], [$info], "", "", "1")['result'];
//            $ref[0]['pension_manager'] = runSimpleFetchQuery($con, ['name', 'phone', 'email'], "pension", ["id"], ["="], [$ref[0]['to_who']], "", "", "1")['result'];
        } else if ($action == 'RECEIVED_POINTS_MERCHANT') {// for agent to user transfers...
            $ref = runSimpleFetchQuery($con, ['*'], "transactions", ["id"], ["="], [$info], "", "", "1")['result'];
            $ref[0]['merchant'] = runSimpleFetchQuery($con, ['name'], "merchants", ["id"], ["="], [$ref[0]['from_whom']], "", "", "1")['result'];
        } else if ($action == 'BUY_POINTS') {// for agent to user transfers...
            $ref = runSimpleFetchQuery($con, ['*'], "transactions", ["id"], ["="], [$info], "", "", "1")['result'];
            $ref[0]['from'] = 'UICI';
        } else if ($action == 'BASIC_POLICY_SUB') {// for user subscription to basic policy
            $ref = runSimpleFetchQuery($con, ['*'], "transactions", ["id"], ["="], [$info], "", "", "1")['result'];
        }

        $trans[$i]['ref'] = $ref;
    }

    disconnectConnection($con);
    return $trans;
}
