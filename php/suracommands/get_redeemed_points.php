<?php

function get_redeemed_points($id, $limit = 0, $offset = 0) {
    $con = makeConnection_user();
    
    if ($limit <= 0) {
        $trans = runSimpleFetchQuery($con, ['*'], "redemptions", ["redeemed_by"], ["="], [$id], "", "id DESC", "")['result'];
    } else {
        $trans = runSimpleFetchQuery($con, ['*'], "redemptions", ["redeemed_by"], ["="], [$id], "", "id DESC", "$limit", "$offset")['result'];
    }
    

    for ($i = 0; $i < count($trans); $i++) {

        $this_trans = $trans[$i];
        $ussd_code_id = $this_trans['ussd_code_id'];
        
        $ussd_code_info = runSimpleFetchQuery($con, ['*'], "ussd_codes", ["id"], ["="], [$ussd_code_id], "", "", "")['result'];
        $trans[$i]['ussd_code_id'] = $ussd_code_info;
        
        $merchant_id = $ussd_code_info[0]['merchant_id'];
        $merchant_info = runSimpleFetchQuery($con, ['name'], "merchants", ["id"], ["="], [$merchant_id], "", "", "")['result'];
        $trans[$i]['merchant'] = $merchant_info;
    }

    disconnectConnection($con);
    return $trans;
}
