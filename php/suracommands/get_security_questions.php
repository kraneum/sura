<?php

function get_security_questions() {
    $con = makeConnection_user();

    $trans = runSimpleFetchQuery($con, ['question'], "security_questions", [], [], [], "", "", "")['result'];

    disconnectConnection($con);

    return $trans;
}
