<?php

function get_store_by_id($id) {

    $con = makeConnection();

        $stores = runSimpleFetchQuery($con, ['id', 'name', 'write_up', 'phone', 'email','points', 'account_no', 'date_joined', 'active'], 
                "e_commerce", ['id'], ['='], [$id], "", "", "", "")['result'];
    

    disconnectConnection($con);
    return $stores;
}
