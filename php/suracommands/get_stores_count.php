<?php

function get_stores_count() {
    $con = makeConnection();
    $trans = [];

    $_trans = runSimpleFetchQuery($con, ['id', 'active'], 'e_commerce', [], [], [], '', '', '')['result'];

    $trans['total'] = count($_trans);
    $a_count = 0;

    foreach ($_trans as $this_trans) {
        $this_trans['active'] == 'TRUE' ? $a_count++ : '';
    }
    
    $trans['active'] = $a_count;
    
    disconnectConnection($con);
    return $trans;
}
