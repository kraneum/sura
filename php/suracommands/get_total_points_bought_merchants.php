<?php

function get_total_points_bought_merchants() {
    $con = makeConnection_merchant();
    $amt = 0;

    $points = runSimpleFetchQuery($con, ['point_amount'], 'point_purchases', [], [], [], '', '', '')['result'];

    foreach ($points as $point) {
        $amt += $point['point_amount'];
    }

    disconnectConnection($con);
    
    return ['total_amount' => $amt];
}
