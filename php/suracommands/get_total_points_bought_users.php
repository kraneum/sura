<?php

function get_total_points_bought_users() {
    $con = makeConnection_user();
    $amt = 0;

    $points = runSimpleFetchORQuery($con, ['point_worth'], 'transactions', ['type_id', 'type_id'], ['=', '='], [2, 3], '', '', '')['result'];

    foreach ($points as $point) {
        $amt += $point['point_worth'];
    }

    disconnectConnection($con);
    
    return ['total_amount' => $amt];
}
