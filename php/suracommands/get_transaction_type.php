<?php

if (isset($_SESSION['userid'])) {
    $id = $_SESSION['userid'];
} else {
    exit('get_transaction_type: No id specified');
}

$fileName = basename($_SERVER['PHP_SELF'], ".php");
if ($fileName !== 'settings' && $fileName !== 'home' && $fileName !== 'points' && $fileName !== 'transactions') {
    require_once('sura_config.php');
    require_once('sura_functions.php');
}

function get_transaction_type() {
    $con = makeConnection();
    global $id;

    $trans = runSimpleFetchQuery($con, ['type_id'], "transactions", ["to_who"], ["="], [$id], "", "id DESC", "1")['result'];
    disconnectConnection($con);

    return ($trans == 1 ? 'credit' : ($trans == 2 ? 'debit' : ($trans == 3 ? 'renew' : ($trans == 4 ? 'transfer' : ($trans == 5 ? 'transferred' : null)))));
}
