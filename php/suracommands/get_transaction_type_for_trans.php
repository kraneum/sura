<?php

if (!isset($_SESSION)){
    session_start();
}
if (!isset($_SESSION['userid'])) {
    exit();
} else {
    $id = $_SESSION['userid'];
}

$fileName = basename($_SERVER['PHP_SELF'], ".php");
if ($fileName !== 'home') {
    require_once('sura_config.php');
    require_once('sura_functions.php');
}

function get_transaction_type_for_trans($id) {
    $con = makeConnection_user();
    global $id;

    $trans = runSimpleFetchQuery($con, ['type_id'], "transactions", ["to_who", "id"], ["=", "="], [$id, $id], "", "id DESC", "1")['result'];
    disconnectConnection($con);
    
    return ($trans == 1 ? 'credit' : ($trans == 2 ? 'debit' : ($trans == 3 ? 'renew' : ($trans == 4 ? 'transfer' : ($trans == 5 ? 'transferred' : null)))));
}