<?php

function get_transactions_ecommerce($transtype = null, $from = null, $to = null, $_startDate = null, $_endDate = null, $limit = null, $offset = 0) {
    $con = makeConnection();

    $startDate = null;
    $endDate = null;

    if (!empty($_startDate)) {
        $startDate = (new DateTime($_startDate))->format(DateTime::ISO8601);
    }
    if (!empty($_endDate)) {
        $endDate = (new DateTime($_endDate))->format(DateTime::ISO8601);
    }

    $trans = runGetTransactions_ecommerce($con, $from, $to, $startDate, $endDate, $limit, $offset)['result'];

    $count = count($trans);

    for ($i = 0; $i < $count; $i++) {

        $this_trans = $trans[$i];
        $from_whom_id = $this_trans['from_whom'];
        $to_who_id = $this_trans['to_who'];

        $from = runSimpleFetchQuery($con, ['id', 'f_name', 'l_name', 'phone'], "users", ["id"], ["="], [$from_whom_id], "", "", "")['result'];
        $to = runSimpleFetchQuery($con, ['name'], "e_commerce", ["id"], ["="], [$to_who_id], "", "", "")['result'];

        $trans[$i]['from'] = $from;
        $trans[$i]['to'] = $to;
    }

    disconnectConnection($con);
    return $trans;
}
