<?php

function get_transactions_insurer($trans_type = null, $from = null, $to = null, $_startDate = null, $_endDate = null, $limit = null, $offset = 0) {
    $con = makeConnection_insurer();

    $startDate = null;
    $endDate = null;

    if (!empty($_startDate)) {
        $startDate = (new DateTime($_startDate))->format(DateTime::ISO8601);
    }
    if (!empty($_endDate)) {
        $endDate = (new DateTime($_endDate))->format(DateTime::ISO8601);
    }

    $trans = runGetTransactions_insurer($con, $trans_type, $from, $to, $startDate, $endDate, $limit, $offset)['result'];

    $count = count($trans);

    for ($i = 0; $i < $count; $i++) {

        $this_trans = $trans[$i];
        $from_whom_id = $this_trans['from_whom'];
        $to_who_id = $this_trans['to_who'];
        $type_id = $this_trans['type_id'];

        $from = runSimpleFetchQuery($con, ['id', 'f_name', 'l_name', 'phone'], "users", ["id"], ["="], [$from_whom_id], "", "", "")['result'];
        $to = runSimpleFetchQuery($con, ['name'], "insurer", ["id"], ["="], [$to_who_id], "", "", "")['result'];

        $trans[$i]['from'] = $from;
        $trans[$i]['to'] = $to;
    }

    disconnectConnection($con);
    return $trans;
}
