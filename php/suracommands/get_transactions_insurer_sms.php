<?php

function get_transactions_insurer_sms($to = null, $_startDate = null, $_endDate = null, $limit = null, $offset = 0) {
    $con = makeConnection_insurer();

    $startDate = null;
    $endDate = null;

    if (!empty($_startDate)) {
        $startDate = (new DateTime($_startDate))->format(DateTime::ISO8601);
    }
    if (!empty($_endDate)) {
        $endDate = (new DateTime($_endDate))->format(DateTime::ISO8601);
    }

    $trans = runGetTransactions_insurer_sms($con, $to, $startDate, $endDate, $limit, $offset)['result'];

    disconnectConnection($con);
    return $trans;
}
