<?php

function get_transactions_user($trans_type = null, $from = null, $to = null, $_startDate = null, $_endDate = null,$limit = null, $offset = 0) {
    $con = makeConnection_user();

    $startDate = null;
    $endDate = null;

    if (!empty($_startDate)) {
        $startDate = (new DateTime($_startDate))->format(DateTime::ISO8601);
    }
    if (!empty($_endDate)) {
        $endDate = (new DateTime($_endDate))->format(DateTime::ISO8601);
    }

    $trans = runGetTransactions_user($con, $trans_type, $from, $to, $startDate, $endDate, $limit, $offset)['result'];

    $count = count($trans);

    for ($i = 0; $i < $count; $i++) {

        $this_trans = $trans[$i];
        $from_whom_id = $this_trans['from_whom'];
        $to_who_id = $this_trans['to_who'];
        $type_id = $this_trans['type_id'];

        $from = null;
        $to = null;

        if ($type_id == 1) { // for point top up...
            $from = runSimpleFetchQuery($con, ['name'], "merchants", ["id"], ["="], [$from_whom_id], "", "", "")['result'];
            $to = runSimpleFetchQuery($con, ['id', 'f_name', 'l_name', 'phone'], "users", ["id"], ["="], [$to_who_id], "", "", "")['result'];
        } else if ($type_id == 2 || $type_id == 3) { // for insurance signup...
            $from = runSimpleFetchQuery($con, ['id', 'f_name', 'l_name', 'phone'], "users", ["id"], ["="], [$from_whom_id], "", "", "")['result'];
            $to = runSimpleFetchQuery($con, ['name'], "insurer", ["id"], ["="], [$to_who_id], "", "", "")['result'];
        } else if ($type_id == 4) { // for user to user transfers...
            $from = runSimpleFetchQuery($con, ['id', 'f_name', 'l_name', 'phone'], "users", ["id"], ["="], [$from_whom_id], "", "", "")['result'];
            $to = runSimpleFetchQuery($con, ['id', 'f_name', 'l_name', 'phone'], "users", ["id"], ["="], [$to_who_id], "", "", "")['result'];
        } else if ($type_id == 7) {// for agent to user transfers...
            $from = runSimpleFetchQuery($con, ['id', 'f_name', 'l_name', 'phone'], "agent", ["id"], ["="], [$from_whom_id], "", "", "")['result'];
            $to = runSimpleFetchQuery($con, ['id', 'f_name', 'l_name', 'phone'], "users", ["id"], ["="], [$to_who_id], "", "", "")['result'];
        } else if ($type_id == 9) {// for payments made to e-merchants...
            $from = runSimpleFetchQuery($con, ['id', 'f_name', 'l_name', 'phone'], "users", ["id"], ["="], [$from_whom_id], "", "", "")['result'];
            $to = runSimpleFetchQuery($con, ['name'], "e_commerce", ["id"], ["="], [$to_who_id], "", "", "")['result'];
        } else if ($type_id == 10) {// for pension topup...
            $from = runSimpleFetchQuery($con, ['id', 'f_name', 'l_name', 'phone'], "users", ["id"], ["="], [$from_whom_id], "", "", "")['result'];
            $to = runSimpleFetchQuery($con, ['name'], "pension", ["id"], ["="], [$to_who_id], "", "", "")['result'];
        }

        $trans[$i]['from'] = $from;
        $trans[$i]['to'] = $to;
    }

    disconnectConnection($con);
    return $trans;
}

