<?php

function get_transfers_by_agent_id($id, $limit = 0, $offset = 0) {
    $con = makeConnection();

    if ($limit <= 0) {
        $trans = runSimpleFetchORQuery($con, ['*'], "point_transfers", ["from_whom", "to_who"], ["=", "="], [$id, $id], "", "id DESC", "")['result'];
    } else {
        $trans = runSimpleFetchORQuery($con, ['*'], "point_transfers", ["from_whom", "to_who"], ["=", "="], [$id, $id], "", "id DESC", "$limit", "$offset")['result'];
    }

    for ($i = 0; $i < count($trans); $i++) {
        $this_trans = $trans[$i];
        $type_id = $this_trans['type_id'];
        $ref = null; // for determing the display name of other transfer party...
        $trans_type = null; // for determining whether sender or receiver...

        if ($type_id == 5) {
            $ref = runSimpleFetchQuery($con, ['name'], "merchants", ["id"], ["="], [$this_trans['from_whom']], "", "", "")['result'];
            $trans_type = 'credit';
        } else if ($type_id == 6) {
            if ($id == $this_trans['from_whom']) {
                $ref = runSimpleFetchQuery($con, ['id'], "agent", ["id"], ["="], [$this_trans['to_who']], "", "", "")['result'];
                $trans_type = 'debit';
            } else if ($id == $this_trans['to_who']) {
                $ref = runSimpleFetchQuery($con, ['id'], "agent", ["id"], ["="], [$this_trans['from_whom']], "", "", "")['result'];
                $trans_type = 'credit';
            }
        } else if ($type_id == 7) {
            $ref = $this_trans['to_who'];
            $trans_type = 'debit';
        }

        $trans[$i]['trans_type'] = $trans_type;
        $trans[$i]['ref'] = $ref;
    }

    disconnectConnection($con);
    return $trans;
}
