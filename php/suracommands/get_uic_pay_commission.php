<?php

function get_uic_pay_commission() {
    $con = makeConnection();

    $trans = runSimpleFetchQuery($con, ['pay_commission'], "system_settings", ["id"], ["="], [1], "", "", "1")['result'];
    disconnectConnection($con);
    
    return ["pay_commission"=>$trans];
}