<?php

function get_uic_policies($id = null) {
    $con = makeConnection();

    $columns = ['policies.*', 'insurance_types.name AS type', 'payment_structures.in_months AS duration', 'payment_structures.name AS duration_desc'];

    if ($id == null) {
        $trans = runSerialLeftJointFetchQuery($con, $columns, ['policies', 'insurance_types', 'payment_structures'], [['policies.type_id = insurance_types.id'], ['policies.tenure = payment_structures.id']], ["policies.insurer_id"], ["="], [0], "", "", "")['result'];
    } else {
        $trans = runSerialLeftJointFetchQuery($con, $columns, ['policies', 'insurance_types', 'payment_structures'], [['policies.type_id = insurance_types.id'], ['policies.tenure = payment_structures.id']], ['policies.id', "policies.insurer_id"], ['=', "="], [$id, 0], "", "", "")['result'];
    }

    disconnectConnection($con);
    return $trans;
}
