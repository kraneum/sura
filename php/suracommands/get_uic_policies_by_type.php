<?php

function get_uic_policies_by_type($type_id) {
    $con = makeConnection();

    $columns = ['policies.*', 'insurance_types.name AS type', 'payment_structures.in_months AS duration', 'payment_structures.name AS duration_desc'];

        $trans = runSerialLeftJointFetchQuery($con, $columns, ['policies', 'insurance_types', 'payment_structures'], [['policies.type_id = insurance_types.id'], ['policies.tenure = payment_structures.id']], ["policies.insurer_id", "policies.type_id"], ["=", "="], [0, "'$type_id'"], "", "", "")['result'];

    disconnectConnection($con);
    return $trans;
}
