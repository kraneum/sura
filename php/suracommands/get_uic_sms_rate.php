<?php

function get_uic_sms_rate() {
    $con = makeConnection();

    $trans = runSimpleFetchQuery($con, ['sms_price'], "system_settings", ["id"], ["="], [1], "", "", "1")['result'];
    disconnectConnection($con);
    
    return ["sms_rate"=>$trans];
}