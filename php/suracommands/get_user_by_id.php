<?php

function get_user_by_id($id) {
    $con = makeConnection_user();

    $user = runSerialLeftJointFetchQuery($con, ['users.id', 'users.f_name','users.m_name','users.l_name','users.gender','users.dob','users.email','users.phone','users.point_balance','users.dedicated_points','users.pension_points',  'pension.name as pension_manager', 'insurer.name as health_insurer', 'users.date_joined','users.last_login','users.active','users.approved','users.next_of_kin','users.kin_phone','users.kin_relationship'], 
                ['users', 'insurer', 'pension_signups', 'pension'], [
                    ['users.health_insurer', '=', 'insurer.id'],
                    ['users.id', '=', 'pension_signups.user_id'],
                    ['pension_signups.pension_id', '=', 'pension.id',]
                        ], ['users.id'], ['='], [$id], "", "id", "", "")['result'];
    
    disconnectConnection($con);
    
    return $user;
}