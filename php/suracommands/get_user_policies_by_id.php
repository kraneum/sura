<?php

function get_user_policies_by_id($id) {
    $con = makeConnection_user();

    $user_signups = runSerialLeftJointFetchQuery($con, [
                'user_signups.id as signup_id', 'user_signups.policy_id', 'user_signups.date_signed', 'user_signups.active', 'user_signups.date_withdrawn',
                'insurer.name as insurer','insurer.logo_url as insurer_logo_url',
                'policies.title', 'policies.description', 'policies.price',
                'payment_structures.in_months as tenure',
                    ], ['user_signups', 'policies', 'payment_structures', 'insurer'], [
                ['user_signups.policy_id', '=', 'policies.id'],
                ['policies.tenure', '=', 'payment_structures.id'],
                ['policies.insurer_id', '=', 'insurer.id']
                    ], ['user_signups.user_id'], ['='], [$id], "", "", "")['result'];
    
    $usersignupsCount = count($user_signups);
    
    for ($i = 0; $i < $usersignupsCount; $i++) {
        if ($user_signups[$i]['insurer'] == null) {
            $uic_signup = runSerialLeftJointFetchQuery($con, [
                'insurer.name', 'insurer.logo_url'
                    ], ['insurer', 'uic_signups'], [
                ['uic_signups.insurer_id', '=', 'insurer.id']
                    ], ['uic_signups.user_signup_id'], ['='], [$user_signups[$i]['signup_id']], "", "", "")['result'];
            
            $user_signups[$i]['insurer'] = $uic_signup[0]['name'];
            $user_signups[$i]['insurer_logo_url'] = $uic_signup[0]['logo_url'];
            
//            $user_signups[$i]['insurer'] = 'xxxxxxxxxxx';
        }
        $ps = runSimpleFetchQuery($con, ['id', 'ref', 'date_to_charge'], 'payment_schedule', ['signup_id'], ['='], [$user_signups[$i]['signup_id']], '', 'id desc', '')['result'];
        
        $user_signups[$i]['payment_schedule'] = $ps;
    }
    
    $activeSignups = [];
    $inactiveSignups = [];

    foreach ($user_signups as $signup) {
        if ($signup['active'] == 'TRUE') {
            $activeSignups[] = $signup;
        } else {
            $inactiveSignups[] = $signup;
        }
    }

    disconnectConnection($con);
    return ['active' => $activeSignups, 'inactive' => $inactiveSignups];
}
