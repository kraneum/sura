<?php

function get_user_signups_count($id) {
    $con = makeConnection_user();
    $trans = runSimpleFetchQuery($con, ['*'], "user_signups", ["user_id"], ["="], [$id], "", "", "")['result'];

    $activeCount = 0;
    $inactiveCount = 0;

    foreach ($trans as $t) {
        if ($t['active'] == 'TRUE') {
            $activeCount++;
        } else {
            $inactiveCount++;
        }
    }

    disconnectConnection($con);
    return ['active' => $activeCount, 'inactive' => $inactiveCount];
}
