<?php

function get_users_count() {
    $con = makeConnection_user();
    $trans = [];

    $_trans = runFetchUsersCount($con)['result'];

    $trans['total'] = $_trans[0]['t_ac_ap'];
    $trans['active'] = $_trans[1]['t_ac_ap'];
    $trans['approved'] = $_trans[2]['t_ac_ap'];

    disconnectConnection($con);
    return $trans;
}
