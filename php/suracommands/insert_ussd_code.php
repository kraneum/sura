<?php
require_once('sura_config.php');
require_once('sura_functions.php');

function insert_ussd_code($con, $p_worth, $purchase_id) {

    $insert_ = uniqueUSSDCodeInsert($con, 'ussd_codes', $p_worth, $purchase_id)['result'];

    if ($insert_) {
        return true;
    } else {
        return false;
    }
}