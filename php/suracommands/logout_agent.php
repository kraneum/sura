<?php

function logout_agent($id) {

    $con = makeConnection();
    $date = (new DateTime())->format(DateTime::ISO8601);

    runSimpleUpdateQuery($con, 'agent', ['last_login'], ["'$date'"], ['id'], ['='], [$id]);
    session_unset();
    session_destroy();

    disconnectConnection($con);
    return 'SUCCESS';
}
