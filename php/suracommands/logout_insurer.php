<?php

function logout_insurer($id) {

    $con = makeConnection_insurer();
    $date = (new DateTime())->format(DateTime::ISO8601);

    runSimpleUpdateQuery($con, 'insurer', ['last_login'], ["'$date'"], ['id'], ['='], [$id]);
    session_unset();
    session_destroy();

    disconnectConnection($con);
    return 'SUCCESS';
}
