<?php

function logout_pension($id) {

    $con = makeConnection();
    $date = (new DateTime())->format(DateTime::ISO8601);

    runSimpleUpdateQuery($con, 'pension', ['last_login'], ["'$date'"], ['id'], ['='], [$id]);
    session_unset();
    session_destroy();

    disconnectConnection($con);
    return 'SUCCESS';
}
