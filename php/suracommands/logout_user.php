<?php

function logout_user($id) {

    $con = makeConnection_user();
    $date = (new DateTime())->format(DateTime::ISO8601);

    runSimpleUpdateQuery($con, 'users', ['last_login'], ["'$date'"], ['id'], ['='], [$id]);
    session_unset();
    session_destroy();

    disconnectConnection($con);
    return 'SUCCESS';
}
