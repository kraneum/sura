<?php

function perform_transaction() {
    require_once('../php/form_validate.php');

    $con = makeConnection();
    $flag = true;
    autoCommit($con, false);
    $response = [];

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // clean user inputs to prevent sql injections
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                disconnectConnection($con);
                return ['status' => false, 'err' => 'INVALID_DETAILS1'];
            }

//            print_r($_POST);
            $phone = $_POST['phone'];
            $amount = $_POST['amount'];
            $secret_key = $_POST['api_key'];

            $validationResult = $form_validate([
                'phone' => 'required|number|maxlength:11',
                'amount' => 'required|number',
                'secret_key' => 'required',
                    ], [
                'phone' => $phone,
                'amount' => $amount,
                'secret_key' => $secret_key
            ]);


            if (!empty($validationResult)) {
                $response['status'] = false;
                $response['err'] = 'VALIDATION_ERROR';
            } else {

                $storeInfo = runSimpleFetchQuery($con, ['id', 'name', 'points'], 'e_commerce', ['api_key'], ['='], [$secret_key], '', '', 1)['result'];
                if (empty($storeInfo)) {
                    disconnectConnection($con);
                    return ['status' => false, 'err' => 'NO_SUCH_STORE'];
                }
                $store_id = $storeInfo[0]['id'];
                $store_name = $storeInfo[0]['name'];
                $store_points = intval($storeInfo[0]['points']);

                $userInfo = runSimpleFetchQuery($con, ['id', 'f_name', 'point_balance'], 'users', ['phone'], ['='], [$phone], '', '', 1)['result'];
                if (empty($userInfo)) {
                    disconnectConnection($con);
                    return ['status' => false, 'err' => 'NO_SUCH_USER'];
                }
                $user_id = $userInfo[0]['id'];
                $f_name = $userInfo[0]['f_name'];
                $point_balance = intval($userInfo[0]['point_balance']);
                
                $_date = new DateTime();
                $date = $_date->format(DateTime::ISO8601);
                $date_expire = $_date->add(new DateInterval('PT22M'))->format(DateTime::ISO8601);

                ///////////////////////////////////////

                if ($point_balance >= $amount) {
                    require_once 'verification_keygen.php';
                    $otp = generateOTP();
                    
                    $result = runSimpleInsertQuery($con, 'pending_transactions', ['amount', 'user_id', 'ecommerce_id', 'date'], ["'$amount'", "'$user_id'", "'$store_id'", "'$date'"]);
                    if ($result['err']['code']) {
                        if ($flag) {
                            $flag = false;
                        }
                    } else {
                        $_trans_id = $result['insertedId'];
                    }

                    $result = runSimpleInsertQuery($con, 'verify_transactions', ['otp', 'user_id', 'ecommerce_id', 'date_expire', 'trans_id'], ["'$otp'", "'$user_id'", "'$store_id'", "'$date_expire'", "'$_trans_id'"]);
                    if ($result['err']['code']) {
                        if ($flag) {
                            $flag = false;
                        }
                    } else {
                        $_trans_id = $result['insertedId'];
                    }
                    
                    

                    if ($flag) {
                        commit($con);
                        $response['status'] = true;
                        $response['info'] = 'VERIFY_OTP';

                        require 'send_sms.php';
                        send_sms($f_name, "Your OTP verfification number for a payment of $amount points to " . ucfirst($store_name) . " is $otp.\r\nIt will expire in 20 mins. Thanks", "'$phone'");
                    } else {
                        rollback($con);
                        $response['info'] = 'FAILURE';
                    }
                } else {
                    rollback($con);
                    $response['info'] = 'INSUFFICIENT_POINTS';
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'INVALID_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    autoCommit($con, true);

    disconnectConnection($con);
    return $response;
}
