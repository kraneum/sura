<?php

function publish_new_policy($id) {
    require_once('../php/form_validate.php');

    $con = makeConnection_insurer();
    autoCommit($con, false);

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        // clean user inputs to prevent sql injections
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if (!$_POST) {
            return ['status' => false, 'err' => 'INVALID_DETAILS'];
        }

        $type = trim($_POST['type']);
        $title = trim($_POST['title']);
        $desc = trim($_POST['desc']);
        $extlink = trim($_POST['extlink']);
        $price = trim($_POST['price']);
        $tenure = trim($_POST['tenure']);

        $validationResult = $form_validate([
            'type' => 'required|maxlength:50',
            'title' => 'required|maxlength:100',
            'desc' => 'required|maxlength:100',
            'extlink' => 'required|url|maxlength:500',
            'price' => 'required|digits|maxlength:10',
            'tenure' => 'required|maxlength:20'
                ], [
            'type' => $type,
            'title' => $title,
            'desc' => $desc,
            'extlink' => $extlink,
            'price' => $price,
            'tenure' => $tenure
        ]);

        if (!empty($validationResult)) {
            $message['info'] = 'VALIDATION_ERROR';
            $message['errVal'] = $validationResult;
        } else {
            $flag = true;
            $date = (new DateTime())->format(DateTime::ISO8601);
            $insertedID = null;
            
            $res = runSimpleInsertQuery($con, 'policies', ['insurer_id', 'type_id', 'title', 'description', 'more_info_link', 'price', 'date_added', 'tenure'], [$id, "'$type'", "'$title'", "'$desc'", "'$extlink'", "'$price'", "'$date'", $tenure]);
            if ($res['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            } else {
                $insertedID = $res['insertedId'];
            }

            $res = runSimpleInsertQuery($con, 'insurer_log', ['action', 'info', 'date', 'insurer_id'], ["'CREATE_POLICY'", "'$insertedID'", "'$date'", $id]);
            if ($res['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }

            if ($flag) {
                if (!$res['err']['code']) {
                    commit($con);
                    autoCommit($con, true);
                    $message['info'] = 'SUCCESS';
                } else {
                    rollback($con);
                    autoCommit($con, true);
                    $message['info'] = 'FAILURE';
                }
            } else {
                rollback($con);
                autoCommit($con, true);
                $message['info'] = 'ERROR_OCCURRED';
            }
        }
    } else {
        $message['info'] = 'WRONG_REQUEST_METHOD';
    }

    autoCommit($con, true);
    disconnectConnection($con);

    return $message;
}
