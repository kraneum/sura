<?php

session_name('UICIMA');
session_start();

function publish_uic_policy() {
    require_once('../php/form_validate.php');

    $con = makeConnection();

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        // clean user inputs to prevent sql injections
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if (!$_POST) {
            return ['status' => false, 'err' => 'INVALID_DETAILS'];
        }

        $type = trim($_POST['ins_type']);
        $title = trim($_POST['title']);
        $desc = trim($_POST['desc']);
        $extlink = trim($_POST['extlink']);
        $price = trim($_POST['price']);
        $tenure = trim($_POST['tenure']);

        $validationResult = $form_validate([
            'type' => 'required|maxlength:50',
            'title' => 'required|maxlength:100',
            'desc' => 'required|maxlength:100',
            'extlink' => 'required|maxlength:500',
            'price' => 'required|maxlength:10',
            'tenure' => 'required|maxlength:10'
                ], [
            'type' => $type,
            'title' => $title,
            'desc' => $desc,
            'extlink' => $extlink,
            'price' => $price,
            'tenure' => $tenure
        ]);

        $selfInfo = runSimpleFetchQuery($con, ['id', 'email'], 'admins', ['id'], ['='], [$_SESSION['id']], '', '', 1)['result'];
        if (empty($selfInfo)) {
            disconnectConnection($con);
            return ['status' => false, 'err' => 'NO_SUCH_ADMIN', 'message' => 'Enter correct admin login details'];
        }
        $self_email = $selfInfo[0]['email'];

        if (!empty($validationResult)) {
            $message['status'] = false;
            $message['info'] = 'VALIDATION_ERROR';
            $message['errVal'] = $validationResult;
        } else {
            autoCommit($con, false);
            $flag = true;
            $date = (new DateTime())->format(DateTime::ISO8601);

            do {
                $res = runSimpleInsertQuery($con, 'admins_activities', ['admin', 'entity', 'summary', 'datetimezone'], ["'$self_email'", "'$title'", "'edit_policy'", "'$date'"]);
                if ($res['err']['code']) {
                    $flag = false;
                    break;
                }
                $res = runSimpleInsertQuery($con, 'policies', ['insurer_id', 'type_id', 'title', 'description', 'more_info_link', 'price', 'date_added', 'tenure'], [0, "'$type'", "'$title'", "'$desc'", "'$extlink'", "'$price'", "'$date'", $tenure]);
                if ($res['err']['code']) {
                    $flag = false;
                }
            } while (false);

            if ($flag) {
                if ($res['result'] == 0 && !$res['err']['code']) {
                    rollback($con);
                    $message['status'] = false;
                    $message['info'] = 'NO_CHANGES';
                } else if (!$res['err']['code']) {
                    commit($con);
                    $message['status'] = true;
                    $message['info'] = 'SUCCESS';
                } else {
                    rollback($con);
                    $message['status'] = false;
                    $message['info'] = 'FAILURE';
                }
            } else {
                rollback($con);
                $message['status'] = false;
                $message['info'] = 'ERROR_OCCURRED';
            }
        }
    } else {
        $message['status'] = false;
        $message['info'] = 'WRONG_REQUEST_METHOD';
    }


    autoCommit($con, true);
    disconnectConnection($con);

    return $message;
}
