<?php

function query_users($gender, $active, $points,  $limit = 0, $offset = 0) {
    $con = makeConnection_insurer();
    
    if ($limit <= 0) {
        $trans = runFetchUsersDataQuery($con, $points, $gender, $active)['result'];
    } else {
        $trans = runFetchUsersDataQuery($con, $points, $gender, $active, $limit, $offset)['result'];
    }

    $trans = array_filter($trans, function ($p) {
        return $p['f_name'];
    });
    disconnectConnection($con);
    return $trans;
}
