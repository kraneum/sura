<?php
session_start();

if (!isset($_SESSION['userid'])) {
    exit();
} else {
    $id = $_SESSION['userid'];
}
require_once('sura_config.php');
require_once('sura_functions.php');

function redeem_code($code) {
    $con = makeConnection();
    global $id;
    $code_row = runSimpleFetchQuery($con, ['*'], "ussd_codes", ["code"], ["="], ["'" . $code . "'"], "", "", "")['result'];
    if (count($code_row) > 0) {

        $this_code = $code_row[0];
        $redeemed = $this_code['redeemed'];
        if ($redeemed == 0) {


            $runUpdate = runSimpleUpdateQuery(
                    $con, "ussd_codes", ['redeemed', 'redeemed_date', 'redeemed_by'], ['1', 'CURRENT_TIMESTAMP', $id], ['code'], ['='], ["'" . $code . "'"])['result'];
        } else {
            echo 'code used';
        }
    } else {
        echo 'invalid code';
    }

    disconnectConnection($con);
}