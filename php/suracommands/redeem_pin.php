<?php

function redeem_pin($id) {
    $con = makeConnection_user();
    $flag = true;
    autoCommit($con, false);
    $pinCode = htmlspecialchars(strip_tags(trim($_POST['pinCode'])));
    $date = (new DateTime())->format(DateTime::ISO8601);
    $ref = 'UICI-' . $id . round(microtime(true) * 1000);
    
    if (empty($pinCode)) {
        disconnectConnection($con);
        return ['status' => false, 'err' => 'NULL_PINCODE'];
    }

    $data = runSimpleFetchQuery($con, ['id', 'merchant_id', 'point_worth', 'redemption_id'], 'ussd_codes', ['code'], ['='], ["'$pinCode'"], '', '', '')['result'];
    if (count($data) == 1) {
        if ($data[0]['redemption_id'] == 0) { // ... if not redeemed already
            $ussd_code_id = $data[0]['id'];
            $point_worth = $data[0]['point_worth'];
            $merchantID = $data[0]['merchant_id'];
            
            $health_point_balance = runSimpleFetchQuery($con, ['health_point_balance'], 'users', ['id'], ['='], [$id], '', '', '')['result'];
            $general_point_balance = runSimpleFetchQuery($con, ['general_point_balance'], 'users', ['id'], ['='], [$id], '', '', '')['result'];
            
            
            $result = runSimpleInsertQuery($con, 'redemptions', ['redeemed_date', 'redeemed_by', 'ussd_code_id'], ["'$date'", $id, $ussd_code_id]);
//            echo print_r($result);
            if ($result['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            } else {
                $redemption_id = $result['insertedId'];
            }
            
            $previous_points = $health_point_balance + $general_point_balance;
            $new_points = $previous_points + $point_worth;
            $result = runSimpleInsertQuery($con, 'transactions', ['reference', 'point_worth', 'date_transacted', 'type_id', 'currency_id', 'from_whom', 'to_who', 'previous_balance_to', 'previous_balance_from'], ["'$ref'", $point_worth, "'$date'", 1, 1, $merchantID, $id, $new_points, "'NULL'"]);
            if ($result['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }

            $result = runSimpleUpdateQuery($con, 'ussd_codes', ['redemption_id'], [$redemption_id], ['id'], ['='], [$ussd_code_id]);
            if ($result['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }
            
            $new_health_points = $health_point_balance + ceil($point_worth / 2);
            $new_general_points = $general_point_balance + floor($point_worth / 2);
            $result = runSimpleUpdateQuery($con, 'users', ['health_point_balance', 'general_point_balance'], 
                    ["$new_health_points", "$new_general_points"], ['id'], ['='], [$id]);
            if ($result['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }
            
            $result = runSimpleInsertQuery($con, 'user_log', ['action', 'info', 'date', 'user_id'], ["'LOAD_PIN'", "'$ussd_code_id'", "'$date'", $id]);
            if ($result['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }

            if ($flag) {
                commit($con);
                $message['info'] = 'SUCCESS';
                $message['health_points'] = number_format($new_health_points);
                $message['general_points'] = number_format($new_general_points);

                if ($_SESSION) {
                    $_SESSION['entity']['health_point_balance'] = $new_health_points;
                    $_SESSION['entity']['general_point_balance'] = $new_general_points;
                    $_SESSION['last_transaction'] = 'credit';
                }
            } else {
                rollback($con);
                $message['info'] = 'ERROR';
            }
        } else {
            rollback($con);
            $message['info'] = 'ALREADY_REDEEMED';
        }
    } else {
        rollback($con);
        autoCommit($con, true);
        $message['info'] = 'FAILURE';
    }

    autoCommit($con, true);

    disconnectConnection($con);

    return $message;
}

