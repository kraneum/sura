<?php

require_once 'verification_keygen.php';

function register_agent() {
    require_once('../php/form_validate.php');

    $con = makeConnection();
    autoCommit($con, false);
    $response = ['status' => true, 'err' => null];

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            // clean user inputs to prevent sql injections
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }

            // clean user inputs to prevent sql injections
            $fname = trim($_POST['fname']);
            $midname = trim($_POST['midname']);
            $lname = trim($_POST['lname']);
            $gender = trim($_POST['gender']);
            $dob = trim($_POST['dob']);
            $phone = trim($_POST['phone']);
            $email = trim($_POST['email']);
            $question = trim($_POST['question']);
            $answer = trim($_POST['answer']);
            $password = trim($_POST['password']);
            $method = trim($_POST['method']);

            $validationResult = $form_validate([
                'fname' => 'required|maxlength:100',
                'midname' => 'maxlength:100',
                'lname' => 'required|maxlength:100',
                'gender' => 'required|maxlength:100',
                'dob' => 'required|dob|maxlength:100',
                'phone' => 'required|phone|maxlength:11',
                'question' => 'required|maxlength:100',
                'answer' => 'required|maxlength:100',
                'password' => 'required|maxlength:100',
                'method' => 'required'
                    ], [
                'fname' => $fname,
                'midname' => $midname,
                'lname' => $lname,
                'gender' => $gender,
                'dob' => $dob,
                'phone' => $phone,
                'question' => $question,
                'answer' => $answer,
                'password' => $password,
                'method' => $method
            ]);

            $profile_pic = null;

            if (strcasecmp($gender, "male") == 0) {
                $profile_pic = 'men.png';
            } else {
                $profile_pic = 'women.png';
            }

            if (!empty($validationResult)) {
                $response['status'] = false;
                $response['err'] = 'VALIDATION_ERROR';
                $response['errVal'] = $validationResult;
            } else {
                $date = (new DateTime())->format(DateTime::ISO8601);
                $ref_id = md5($phone);
                if (!empty($email)) {
                    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
                    $email = filter_var($email, FILTER_VALIDATE_EMAIL);
                }
                $password = password_hash($password, PASSWORD_BCRYPT);
                $res = runSimpleInsertQuery($con, "agent", ['f_name', 'm_name', 'l_name', 'gender', 'dob', 'phone', 'email', 'date_joined', 'password', 'security_question', 'security_answer', 'profile_pic', 'ref_id', 'approved'], ["'$fname'", "'$midname'", "'$lname'", "'$gender'", "'$dob'", "'$phone'", "'$email'", "'$date'", "'$password'", "'$question'", "'$answer'", "'$profile_pic'", "'$ref_id'", "'FALSE'"]);

                if (!$res['err']['code']) {
                    $insertedId = $res['insertedId'];
                    if ($method == 'sms' || empty($email)) {
                        require 'sms/sms_sender.php';
                        if (!isset($_SESSION)) {
                            session_start();
                        }
                        $_SESSION['entity']['id'] = $insertedId;
                        $_SESSION['entity_type'] = 'AGENT';
                        $link = generateOTP();
                        $res = runSimpleInsertQuery($con, "verify_entity", ['entity_id', 'link', 'role'], [$insertedId, "'$link'", "'AGENT'"]);
                        sendSms($fname, 'Your OTP verification number is ' . $link, "'$phone'", 234);
                        $response['msg'] = 'OTP_VERIFY';
                    } else if ($method == 'email') {
                        require 'send_mail.php';
                        $link = generateLink('a');
                        $fullLink = $link . $insertedId;
                        $res = runSimpleInsertQuery($con, "verify_entity", ['entity_id', 'link', 'role'], [$insertedId, "'$link'", "'AGENT'"]);
                        $subject = 'Verify Registration';
                        $message = '<html><body style="padding: 0 20%; font-size:14px"><div><h3>Dear ' . $fname . ', </h3>'
                                . 'Please confirm that you\'ve registered a user account on UIC Innovations and '
                                . 'that you are the owner of this email address by clicking on this button below<br>'
                                . '<div style="text-align:center; margin:30px 0;">'
                                . '<a style="padding: 15px; background-color: #12a8e9; font-size: 14px; border-radius: 7px; color: white; font-weight: bold;text-decoration: none" '
                                . 'href="http://localhost/iinsurance/php/verify_email.php?i=' . $fullLink . '">Verify Email</a></div>'
                                . '<br><div>If button doesn\'t work, copy and paste the link in your browser: http://localhost/iinsurance/php/verify_email.php?i=' . $fullLink
                                . '<br><br><div style="font-size: .8em">If you’ve received this email by mistake, simply ignore it.'
                                . 'Your account won\'t be created if you don\'t click the confirmation link above.'
                                . '<br><br>Thanks for your loyalty.</div></div></html></body>';

                        send_mail($email, $subject, $message);
                        $response['msg'] = 'EMAIL_VERIFY';
                    }
                    unset($fname);
                    unset($midname);
                    unset($lname);
                    unset($gender);
                    unset($dob);
                    unset($phone);
                    unset($email);
                    unset($password);
                    unset($question);
                    unset($answer);

                    if (!$res['err']['code']) {
                        commit($con);
                    } else {
                        rollback($con);
                    }
                } else if ($res['err']['code'] == 1062) {
                    rollback($con);
                    $response['status'] = false;
                    $response['err'] = 'DUPLICATE_ERROR';
                    $response['errColumn'] = '';
                    if (strrpos($res['err']['error'], "email") !== false) {
                        $response['errColumn'] = 'Email';
                    } else if (strrpos($res['err']['error'], "phone") !== false) {
                        $response['errColumn'] = 'Phone';
                    }
                } else {
                    rollback($con);
                    $response['status'] = false;
                    $response['err'] = 'REGISTRATION_ERROR';
                }
            }
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    autoCommit($con, true);

    return $response;
}
