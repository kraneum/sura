<?php

require_once('verification_keygen.php');

function register_pension() {
    require_once('../php/form_validate.php');

    $con = makeConnection();
    autoCommit($con, false);
    $response = ['status' => true, 'err' => null];

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            // clean user inputs to prevent sql injections
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }

            // clean user inputs to prevent sql injections
            $name = trim($_POST['name']);
            $desc = trim($_POST['desc']);
            $phone = trim($_POST['phone']);
            $email = trim($_POST['email']);
            $rc = trim($_POST['rc']);
            $extlink = trim($_POST['extlink']);
            $question = trim($_POST['question']);
            $answer = trim($_POST['answer']);
            $password = trim($_POST['password']);

            $validationResult = $form_validate([
                'name' => 'required|maxlength:100',
                'desc' => 'required|maxlength:200',
                'phone' => 'required|phone|maxlength:11',
                'email' => 'required|email|maxlength:100',
                'rc' => 'required|maxlength:100',
                'extlink' => 'required|url|maxlength:500',
                'question' => 'required|maxlength:100',
                'answer' => 'required|maxlength:100',
                'password' => 'required|maxlength:100'
                    ], [
                'name' => $name,
                'desc' => $desc,
                'phone' => $phone,
                'email' => $email,
                'rc' => $rc,
                'extlink' => $extlink,
                'question' => $question,
                'answer' => $answer,
                'password' => $password
            ]);

            $logo = 'uic.png';

            if (!empty($validationResult)) {
                $response['status'] = false;
                $response['err'] = 'VALIDATION_ERROR';
                $response['errVal'] = $validationResult;
            } else {
                $date = (new DateTime())->format(DateTime::ISO8601);
                $password = password_hash($password, PASSWORD_BCRYPT);
                $res = runSimpleInsertQuery($con, "pension", ['name', 'write_up', 'date_joined', 'phone', 'email', 'rc_number', 'more_info_link', 'security_question', 'security_answer', 'password', 'logo_url'], ["'$name'", "'$desc'", "'$date'", "'$phone'", "'$email'", "'$rc'", "'$extlink'", "'$question'", "'$answer'", "'$password'", "'$logo'"]);

                if (!$res['err']['code']) {
                    require 'send_mail.php';
                    $insertedId = $res['insertedId'];
                    $link = generateLink('p');
                    $fullLink = $link . $insertedId;
                    $res = runSimpleInsertQuery($con, "verify_entity", ['entity_id', 'link', 'role'], [$insertedId, "'$link'", "'PENSION'"]);
                    $subject = 'Verify Registration';
                    $message = '<html><body style="padding: 0 20%; font-size:14px"><div><h3>Dear ' . $name . ', </h3>'
                            . 'Please confirm that you\'ve registered an pension account on UIC Innovations and '
                            . 'that you are the owner of this email address by clicking on this button below<br>'
                            . '<div style="text-align:center; margin:30px 0;">'
                            . '<a style="padding: 15px; background-color: #12a8e9; font-size: 14px; border-radius: 7px; color: white; font-weight: bold;text-decoration: none" '
                            . 'href="http://localhost/iinsurance/php/verify_email.php?i=' . $fullLink . '">Verify Email</a></div>'
                            . '<br><div>If button doesn\'t work, copy and paste the link in your browser: http://localhost/iinsurance/php/verify_email.php?i=' . $fullLink
                            . '<br><br><div style="font-size: .8em">If you’ve received this email by mistake, simply ignore it.'
                            . 'Your account won\'t be created if you don\'t click the confirmation link above.'
                            . '<br><br>Thanks for your loyalty.</div></div></html></body>';
                    
                    send_mail($email, $subject, $message);
                    unset($name);
                    unset($desc);
                    unset($rc);
                    unset($phone);
                    unset($email);
                    unset($question);
                    unset($answer);
                    unset($password);
                } else if ($res['err']['code'] == 1062) {
                    $response['status'] = false;
                    $response['err'] = 'DUPLICATE_ERROR';
                    $response['errColumn'] = '';
                    if (strrpos($res['err']['error'], "email") !== false) {
                        $response['errColumn'] = 'Email';
                    } else if (strrpos($res['err']['error'], "username") !== false) {
                        $response['errColumn'] = 'Username';
                    }
                } else {
                    $response['status'] = false;
                    $response['err'] = 'REGISTRATION_ERROR';
                    $response['errCode'] = $res['err'];
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    autoCommit($con, true);
    return $response;
}
