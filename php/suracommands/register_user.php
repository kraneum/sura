<?php

require_once 'verification_keygen.php';

function register_user() {
    require_once('../php/form_validate.php');

    $con = makeConnection_user();
    autoCommit($con, false);
    $response = ['status' => true, 'err' => null];

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // clean user inputs to prevent sql injections
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }

            $fname = trim($_POST['fname']);
            $lname = trim($_POST['lname']);
            $phone = trim($_POST['phone']);
            $email = trim($_POST['email']);
            $question = trim($_POST['question']);
            $answer = trim($_POST['answer']);
            $password = trim($_POST['password']);
            $method = trim($_POST['method']);

            $validationResult = $form_validate([
                'fname' => 'required|maxlength:100',
                'lname' => 'required|maxlength:100',
                'email' => 'email',
                'phone' => 'required|phone|maxlength:11',
                'question' => 'required|maxlength:100',
                'answer' => 'required|maxlength:100',
                'password' => 'required|maxlength:100',
                'method' => 'required'
                    ], [
                'fname' => $fname,
                'lname' => $lname,
                'email' => $email,
                'phone' => $phone,
                'question' => $question,
                'answer' => $answer,
                'password' => $password,
                'method' => $method
            ]);

            $profile_pic = null;

//            if (strcasecmp($gender, "MALE") == 0) {
            $profile_pic = 'men.png';
//            } else {
//                $profile_pic = 'women.png';
//            }

            if (!empty($validationResult)) {
                $response['status'] = false;
                $response['err'] = 'VALIDATION_ERROR';
                $response['errVal'] = $validationResult;
            } else {
                $date = (new DateTime())->format(DateTime::ISO8601);
                if (!empty($email)) {
                    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
                    $email = filter_var($email, FILTER_VALIDATE_EMAIL);
                }

//                $new_health_insurer_index = null;
//                $health_insurers = [];
//
//                $healthIns_id = runSimpleFetchQuery($con, ['id'], "insurance_types", ["meta"], ["="], ["'health'"], '', '', '1', '')['result'];
//                $health_ins = runSimpleFetchQuery($con, ['id', 'name'], "insurer", ['insurance_type'], ['='], ["'$healthIns_id'"], '', '', '', '')['result'];
//
//                foreach ($health_ins as $h) { // creates array of health insurers
//                    $health_insurers[] = $h['id'];
//                }
//
//                $last_user_health_insurer = runSimpleFetchQuery($con, ['health_insurer'], "users", [], [], [], '', 'id DESC', '1', '')['result'];
//
//                $index = array_search($last_user_health_insurer, $health_insurers);
//
//                if ($index == (count($health_insurers) - 1)) {
//                    $new_health_insurer_index = 0;
//                } else {
//                    $new_health_insurer_index = $index + 1;
//                }

                $flag = true;

                do {
                    $password = password_hash($password, PASSWORD_BCRYPT);
                    $res = runSimpleInsertQuery($con, "users", ['f_name', 'l_name', 'phone', 'email', 'date_joined', 'password', 'security_question', 'security_answer', 'profile_pic', 'approved'], ["'$fname'", "'$lname'", "'$phone'", "'$email'", "'$date'", "'$password'", "'$question'", "'$answer'", "'$profile_pic'", "'FALSE'"]);
                    if ($res['err']['code']) {
                        $flag = false;
                        break;
                    }
                    $insertedId = $res['insertedId'];

                    $referral_id = generateLink('U', 9) . "$insertedId";
                    $res = runSimpleInsertQuery($con, 'referees', ['entity', 'entity_id', 'referral_id'], ["'USER'", $insertedId, "'$referral_id'"]);
                    if ($res['err']['code']) {
                        $flag = false;
                    }
                } while (false);

                if ($flag) {

                    if ($method == 'sms' || empty($email)) {
                        require 'send_sms.php';
                        if (!isset($_SESSION)) {
                            session_start();
                        }
                        $_SESSION['entity']['id'] = $insertedId;
                        $_SESSION['entity_type'] = 'USER';
                        $_SESSION['verify_otp'] = "TRUE";
                        $link = generateOTP();
                        $res = runSimpleInsertQuery($con, "verify_entity", ['entity_id', 'link', 'role'], [$insertedId, "'$link'", "'USER'"]);

                        send_sms($fname, 'Your OTP verification number is ' . $link, "'$phone'");
                        $response['msg'] = 'OTP_VERIFY';
                    } else if ($method == 'email') {
                        require 'send_mail.php';
                        $link = generateLink('u');
                        $fullLink = $link . $insertedId;
                        $res = runSimpleInsertQuery($con, "verify_entity", ['entity_id', 'link', 'role'], [$insertedId, "'$link'", "'USER'"]);
                        $subject = 'Verify Registration';
                        $message = '<html><body style="padding: 0 20%; font-size:14px; font-family: Arial, Helvetica, sans-serif"><div><h3>Dear ' . $fname . ', </h3>'
                                . 'Please confirm that you\'ve registered a user account on UIC Innovations and '
                                . 'that you are the owner of this email address by clicking on this button below<br>'
                                . '<div style="text-align:center; margin:30px 0;">'
                                . '<a style="padding: 15px; background-color: #12a8e9; font-size: 14px; border-radius: 7px; color: white; font-weight: bold;text-decoration: none" '
                                . 'href="http://localhost/iinsurance/php/verify_email.php?i=' . $fullLink . '">Verify Email</a></div>'
                                . '<br><div>If button doesn\'t work, copy and paste the link in your browser: http://localhost/iinsurance/php/verify_email.php?i=' . $fullLink
                                . '<br><br><div style="font-size: .8em">If you’ve received this email by mistake, simply ignore it.'
                                . 'Your account won\'t be created if you don\'t click the confirmation link above.'
                                . '<br><br>Thanks for your loyalty.</div></div></html></body>';
//                        $headers = 'From: modernbabbage@gmail.com' . "\r\n" .
//                            'Reply-To: no-reply@uiclimited.com' . "\r\n" .
//                            'MIME-Version: 1.0' . "\r\n" .
//                            'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
                        send_mail($email, $subject, $message);
                        $response['msg'] = 'EMAIL_VERIFY';
                    }

                    unset($fname);
                    unset($lname);
                    unset($phone);
                    unset($email);
                    unset($question);
                    unset($answer);
                    unset($password);

                    if (!$res['err']['code']) {
                        commit($con);
                    } else {
                        rollback($con);
                    }
                } else if ($res['err']['code'] == 1062) {
                    rollback($con);
                    $response['status'] = false;
                    $response['err'] = 'DUPLICATE_ERROR';
                    $response['errColumn'] = '';
                    if (strrpos($res['err']['error'], "email") !== false) {
                        $response['errColumn'] = 'Email';
                    } else if (strrpos($res['err']['error'], "phone") !== false) {
                        $response['errColumn'] = 'Phone Number';
                    }
                } else {
                    rollback($con);
                    $response['status'] = false;
                    $response['err'] = 'REGISTRATION_ERROR';
                }
            }
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    autoCommit($con, true);
    disconnectConnection($con);
    return $response;
}
