<?php

require_once 'verification_keygen.php';

function register_user_test() {
    require_once('../php/form_validate.php');

    $con = makeConnection_user();
    $response = ['status' => true, 'err' => null];

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // clean user inputs to prevent sql injections
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }

            $phone = trim($_POST['phone']);
            $password = trim($_POST['password']);

            $validationResult = $form_validate([
                'phone' => 'required|phone|maxlength:11',
                'password' => 'required|maxlength:100',
                    ], [
                'phone' => $phone,
                'password' => $password,
            ]);

            $profile_pic = 'dummy.gif';

            if (!empty($validationResult)) {
                $response['status'] = false;
                $response['err'] = 'VALIDATION_ERROR';
                $response['errVal'] = $validationResult;
            } else {
                $date = (new DateTime())->format(DateTime::ISO8601);

                $res = runSimpleInsertQuery($con, "users", ['phone', 'date_joined', 'password', 'profile_pic', 'approved'], ["'$phone'", "'$date'", "'$password'", "'$profile_pic'", "'FALSE'"]);

                if (!$res['err']['code']) {
                    require 'send_sms.php';
                    send_sms('', 'You have just registered on UICI Platform. To access your account, please login at http://uic.kranuem.com/login.php. Your password is '.$password.'. You can change it in Settings page', "'$phone'");
                } else if ($res['err']['code'] == 1062) {
                    $response['status'] = false;
                    $response['err'] = 'DUPLICATE_ERROR';
                    $response['errColumn'] = '';
                    if (strrpos($res['err']['error'], "email") !== false) {
                        $response['errColumn'] = 'Email';
                    } else if (strrpos($res['err']['error'], "phone") !== false) {
                        $response['errColumn'] = 'Phone Number';
                    }
                } else if ($res['err']['code']) {
                    $response['status'] = false;
                    $response['err'] = 'REGISTRATION_ERROR';
                }
            }
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }


    autoCommit($con, false);
    return $response;
}
