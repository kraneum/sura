<?php

function renew_policy($id) {
    $con = makeConnection_user();
    $flag = true;
    autoCommit($con, false);

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            // clean user inputs to prevent sql injections
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }

            $signupID = trim($_POST['signupID']);

            ////////////////

            $policyID = runSimpleFetchQuery($con, ['policy_id'], 'user_signups', ['id', 'user_id'], ['=', '='], [$signupID, $id], '', '', '1')['result'];

            //////////////////

            $policyInfo = runSimpleFetchQuery($con, ['title', 'price', 'tenure', 'type_id'], 'policies', ['id'], ['='], [$policyID], '', '', '1')['result']; // retrieves policy info for signupID
            $price = $policyInfo[0]['price'];
            $type_id = $policyInfo[0]['type_id'];
            $policy_title = $policyInfo[0]['title'];
            $in_months = runSimpleFetchQuery($con, ['in_months'], 'payment_structures', ['id'], ['='], [$policyInfo[0]['tenure']], '', '', '1')['result'];
            $scheduleInfo = runSimpleFetchQuery($con, ['id', 'date_to_charge'], 'payment_schedule', ['user_id', 'signup_id'], ['=', '='], [$id, $signupID], '', 'id DESC', '1')['result'];
            if (!empty($scheduleInfo)) {
                $last_date_id = $scheduleInfo[0]['id'];
                $date_to_charge = $scheduleInfo[0]['date_to_charge'];
            }
            $new_payday = (new DateTime($date_to_charge))->add(new DateInterval('P' . $in_months . 'M'))->format(DateTime::ISO8601);

            ///////////////////

            $userInfo = runSimpleFetchQuery($con, ['id', 'f_name', 'l_name', 'phone', 'health_point_balance', 'general_point_balance'], 'users', ['id'], ['='], [$id], '', '', '')['result'];
            $user_id = $userInfo[0]['id'];
            $f_name = $userInfo[0]['f_name'];
            $l_name = $userInfo[0]['l_name'];
            $phone = $userInfo[0]['phone'];
            $health_point_balance = intval($userInfo[0]['health_point_balance']);
            $general_point_balance = intval($userInfo[0]['general_point_balance']);

            $points = null;
            $other_points = null;
            if ($type_id == '1') {
                $points = $health_point_balance;
                $other_points = $general_point_balance;
            } else if ($type_id == '2') {
                $points = $general_point_balance;
                $other_points = $health_point_balance;
            }

            ///////////////////

            $insurerID = runSimpleFetchQuery($con, ['insurer_id'], 'policies', ['id'], ['='], [$policyID], '', '', '1')['result'];
            if ($insurerID == 0) {
                $insurerID = runSimpleFetchQuery($con, ['insurer_id'], 'uic_signups', ['user_signup_id'], ['='], [$signupID], '', '', '1')['result'];
            }
            $insurerInfo = runSimpleFetchQuery($con, ['name', 'email', 'redeemable_points'], 'insurer', ['id'], ['='], [$insurerID], '', '', '')['result'];
            $insurer_points = intval($insurerInfo[0]['redeemable_points']);
            $insurer_name = $insurerInfo[0]['name'];
            $insurer_email = $insurerInfo[0]['email'];

            /////////////////

            $date = (new DateTime())->format(DateTime::ISO8601);
            $ref = 'UICI-' . $user_id . round(microtime(true) * 1000);

            ////////////////

            if ($points >= $price) {

                do {

                    $new_point_balance = $points - $price;

                    if ($type_id == '1') {
                        $result = runSimpleUpdateQuery($con, 'users', ['health_point_balance'], ["'$new_point_balance'"], ['id'], ['='], [$id]);
                        if ($result['err']['code']) {
                            if ($flag) {
                                $flag = false;
                                break;
                            }
                        }
                    } else if ($type_id == '2') {
                        $result = runSimpleUpdateQuery($con, 'users', ['general_point_balance'], ["'$new_point_balance'"], ['id'], ['='], [$id]);
                        if ($result['err']['code']) {
                            if ($flag) {
                                $flag = false;
                                break;
                            }
                        }
                    }

                    $new_insurer_points = $insurer_points + $price;
                    $result = runSimpleUpdateQuery($con, 'insurer', ['redeemable_points'], ["'$new_insurer_points'"], ['id'], ['='], [$insurerID]);
                    if ($result['err']['code']) {
                        $flag = false;
                        break;
                    }

                    $result = runSimpleInsertQuery($con, 'transactions', ['reference', 'point_worth', 'date_transacted', 'type_id', 'currency_id', 'from_whom', 'to_who', 'previous_balance_from', 'previous_balance_to'], ["'$ref'", "'$price'", "'$date'", 3, 1, $id, $insurerID, "'$new_point_balance'", "'$new_insurer_points'"])['err']['code'];
                    if ($result) {
                        $flag = false;
                        break;
                    }

                    $result = runSimpleUpdateQuery($con, 'payment_schedule', ['date_to_charge'], ["'$date'"], ['id'], ['='], [$last_date_id])['err']['code'];
                    if ($result) {
                        $flag = false;
                        break;
                    }
                    $result = runSimpleInsertQuery($con, 'payment_schedule', ["'$ref'", 'date_to_charge', 'user_id', 'signup_id', 'amount'], ["'$ref'", "'$new_payday'", $id, $signupID, "'$price'"])['err']['code'];
                    if ($result) {
                        $flag = false;
                        break;
                    }
                    $result = runSimpleInsertQuery($con, 'user_log', ['action', 'info', 'date', 'user_id'], ["'RENEW_POLICY'", "'$policyID'", "'$date'", $id]);
                    if ($result['err']['code']) {
                        $flag = false;
                        break;
                    }

                    $result = runSimpleInsertQuery($con, 'insurer_log', ['action', 'info', 'date', 'insurer_id'], ["'POLICY_RENEW'", "'$policyID'", "'$date'", $insurerID]);
                    if ($result['err']['code']) {
                        $flag = false;
                    }
                } while (false);

                if ($flag) {
                    commit($con);
                    
                    $message['info'] = 'SUCCESS';
                    if ($type_id == '1') {
                        $message['policy_type'] = 'health';
                    } else  if ($type_id == '2') {
                        $message['policy_type'] = 'general';
                    }
                    $message['points'] = number_format($new_point_balance);

                    if ($_SESSION) {
                        if ($type_id == '1') {
                            $_SESSION['entity']['health_point_balance'] = $new_point_balance;
                        } else if ($type_id == '2') {
                            $_SESSION['entity']['general_point_balance'] = $new_point_balance;
                        }
                        $_SESSION['last_transaction'] = 'debit';
                    }

                    require 'send_mail.php';
                    $subject = 'Policy Renewal from UICI';
                    $mail = '<html><body style="padding: 0 20%; font-size:14px"><div><h3>Hi ' . $insurer_name . ', </h3>'
                            . "A user '$f_name $l_name ($phone)' signed up for a policy named $policy_title worth $price Loyalty Points"
                            . '<br><br><div style="font-size: .8em">If you’ve received this email by mistake, simply ignore it.'
                            . 'Your account won\'t be created if you don\'t click the confirmation link above.'
                            . '<br><br>Thanks for your loyalty.</div></div></html></body>';
                    send_mail($insurer_email, $subject, $mail);
                } else {
                    rollback($con);
                    autoCommit($con, true);
                    $message['info'] = 'ERROR';
                }
            } else {
                $message['info'] = 'FAILURE';
            }
//    } else {
//        $message['info'] = 'ALREADY_SIGNED_UP';
//    }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    autoCommit($con, true);
    disconnectConnection($con);
    return $message;
}
