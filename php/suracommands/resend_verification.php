<?php

require_once 'verification_keygen.php';
require_once 'sms/sms_sender.php';

function resend_verification($id) {
    $con = makeConnection();

    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    if (!$_POST) {
        return ['status' => false, 'err' => 'INVALID_DETAILS'];
    }
    
    $entity = trim($_POST['entity']);
    $method = trim($_POST['method']);
    $medium = trim($_POST['medium']);
    $name = trim($_POST['name']);

    if ($method == 'email') {
        if ($entity == 'USER') {
            $entity_char = 'u';
        } else if ($entity == 'INSURER') {
            $entity_char = 'i';
        } else if ($entity == 'AGENT') {
            $entity_char = 'a';
        } else if ($entity == 'PENSION') {
            $entity_char = 'p';
        }
        
        $link = generateLink($entity_char);
        $fullLink = $link . $id;
        $res = runSimpleInsertQuery($con, "verify_entity", ['entity_id', 'link', 'role'], [$id, "'$link'", "'$entity'"]);
        $to = $medium;
        $subject = 'Verify Registration';
        $message = '<html><body style="padding: 0 20%; font-size:14px"><div><h3>Dear ' . $name . ', </h3>'
                . 'Please confirm that you\'ve registered a user account on iInsurance and '
                . 'that you are the owner of this email address by clicking on this button below<br>'
                . '<div style="text-align:center; margin:30px 0;">'
                . '<a style="padding: 15px; background-color: #12a8e9; font-size: 14px; border-radius: 7px; color: white; font-weight: bold;text-decoration: none" '
                . 'href="http://localhost/iinsurance/php/verify_email.php?i=' . $fullLink . '">Verify Email</a></div>'
                . '<br><div>If button doesn\'t work, copy and paste the link in your browser: http://localhost/iinsurance/php/verify_email.php?i=' . $fullLink
                . '<br><br><div style="font-size: .8em">If you’ve received this email by mistake, simply ignore it.'
                . 'Your account won\'t be created if you don\'t click the confirmation link above.'
                . '<br><br>Thanks for your loyalty.</div></div></html></body>';
        $headers = 'From: modernbabbage@gmail.com' . "\r\n" .
                'Reply-To: no-reply@uiclimited.com' . "\r\n" .
                'MIME-Version: 1.0' . "\r\n" .
                'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
        mail($to, $subject, $message, $headers);
        $response['msg'] = 'EMAIL_VERIFY';
    } else if ($method == 'sms') {
        $_SESSION['role'] = $entity;
        $link = generateOTP();
        $res = runSimpleInsertQuery($con, "verify_entity", ['entity_id', 'link', 'role'], [$id, "'$link'", "'$entity'"]);
        sendSms("$name", 'Your OTP verification number is ' . $link, "'$medium'", 234);
        $response['msg'] = 'OTP_VERIFY';
    }

    return $response;
}
