<?php

function reset_password() {
    $con = makeConnection();
    autoCommit($con, false);
    $flag = true;
    $response = ['status' => true, 'msg' => null];

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                echo json_encode(['status' => false, 'err' => 'INVALID_REQUEST_PARAMETERS']);
                exit();
            }

            $sentLink = $_POST['link'];
            $password = $_POST['password'];

            $link = substr($sentLink, 0, 30);
            $id = substr($sentLink, 30);
            $entity = getEntity($link[0]);

            $res = runSimpleFetchQuery($con, ['id', 'role', 'entity_id', 'verified'], 'verify_entity', ['entity_id', 'link', 'role'], ['=', '=', '='], [$id, "'$link'", "'$entity'"], '', '', 1);

            if (!$res['err']['code']) {
                $res = $res['result'];
                if (count($res) > 0) {
                    $verified = $res[0]['verified'];
                    $entityid = $res[0]['entity_id'];
                    $role = $res[0]['role'];
                    if ($verified == 'FALSE') {
                        do {
                            $result = runSimpleUpdateQuery($con, 'verify_entity', ['verified'], ["'TRUE'"], ['entity_id', 'link'], ['=', '='], [$entityid, "'$link'"], 1);
                            if ($result['err']['code']) {
                                if ($flag) {
                                    $flag = false;
                                    break;
                                }
                            }
                            $result = updatePassword($con, $password, $entityid, $role);
                            if ($result['err']['code']) {
                                if ($flag) {
                                    $flag = false;
                                    break;
                                }
                            }
                        } while (false);

                        if ($flag) {
                            commit($con);
                            $response['status'] = true;
                            $response['entity'] = $entity;
                        } else {
                            rollback($con);
                            $response['status'] = false;
                            $response['err'] = 'VERIFIED';
                        }
                    } else {
                        $response['status'] = false;
                        $response['err'] = 'INVALID_LINK';
                    }
                } else {
                    $response['status'] = false;
                    $response['err'] = 'INVALID_LINK';
                }
            } else {
                $response['status'] = false;
                $response['err'] = 'INTERNAL_ERROR';
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECTION_ERROR';
    }

    autoCommit($con, true);
    disconnectConnection($con);
    return $response;
}

function updatePassword($con,$password, $entityid, $role) {
    if ($role === 'USER') {
        $result = runSimpleUpdateQuery($con, 'users', ['password'], ["'$password'"], ['id'], ['='], [$entityid]);
    } else if ($role === 'INSURER') {
        $result = runSimpleUpdateQuery($con, 'insurer', ['password'], ["'$password'"], ['id'], ['='], [$entityid]);
    } else if ($role === 'AGENT') {
        $result = runSimpleUpdateQuery($con, 'agent', ['password'], ["'$password'"], ['id'], ['='], [$entityid]);
    } else if ($role === 'PENSION') {
        $result = runSimpleUpdateQuery($con, 'pension', ['password'], ["'$password'"], ['id'], ['='], [$entityid]);
    }
    return $result;
}

function getEntity($e) {
    if ($e == 'u') {
        return 'user';
    } else if ($e == 'a') {
        return 'agent';
    } else if ($e == 'i') {
        return 'insurer';
    } else if ($e == 'p') {
        return 'pension';
    }else if ($e == 'p') {
        return 'merchant';
    }
}
