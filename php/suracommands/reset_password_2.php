<?php

function reset_password_2() {
    $con = makeConnection_user();
    autoCommit($con, false);
    $flag = true;
    $response = ['status' => true, 'msg' => null];

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                echo json_encode(['status' => false, 'err' => 'INVALID_REQUEST_PARAMETERS']);
                exit();
            }

            $entityid = $_POST['id'];
            $password = $_POST['password'];

            $result = runSimpleUpdateQuery($con, 'users', ['password'], ["'$password'"], ['id'], ['='], [$entityid]);
            if ($result['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }

            if ($flag) {
                commit($con);
                $response['status'] = true;
            } else {
                rollback($con);
                $response['status'] = false;
                $response['err'] = 'INTERNAL_ERROR';
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECTION_ERROR';
    }

    autoCommit($con, true);
    disconnectConnection($con);
    return $response;
}
