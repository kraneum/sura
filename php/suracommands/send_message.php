<?php

function send_message($id) {
    require_once('../php/form_validate.php');

    $con = makeConnection();
    autoCommit($con, false);

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }
            $type = trim($_POST['type']);
            $mes = trim($_POST['message']);
            $entity = trim($_POST['entity']);

            $validationResult = $form_validate([
                'message' => 'required|maxlength:1000',
                    ], [
                'message' => $mes,
            ]);

            if (!empty($validationResult)) {
                $message['info'] = 'VALIDATION_ERROR';
            } else {

                $flag = true;
                $date = (new DateTime())->format(DateTime::ISO8601);
                $res = null;
                
                if ($entity === 'USER') {
                    $res = runSimpleInsertQuery($con, 'user_log', ['action', 'date', 'user_id'], ["'SEND_MESSAGE'", "'$date'", $id]);
                } else if ($entity === 'AGENT') {
                    $res = runSimpleInsertQuery($con, 'agent_log', ['action', 'date', 'agent_id'], ["'SEND_MESSAGE'", "'$date'", $id]);
                } else if ($entity === 'INSURER') {
                    $res = runSimpleInsertQuery($con, 'insurer_log', ['action', 'date', 'insurer_id'], ["'SEND_MESSAGE'", "'$date'", $id]);
                }
                if ($res['err']['code']) {
                    if ($flag) {
                        $flag = false;
                        echo '1';
                    }
                }

                $res = runSimpleInsertQuery($con, 'contact', ['type', 'message', 'date', 'entity_id', 'entity'], ["'SUPPORT'", "'$mes'", "'$date'", "'$id'", "'$entity'"]);
                if ($res['err']['code']) {
                    if ($flag) {
                        $flag = false;
                        echo '2';
                    }
                }

                if ($flag) {
                    commit($con);
                    autoCommit($con, true);
                    $message['info'] = 'SUCCESS';
                } else {
                    rollback($con);
                    autoCommit($con, true);
                    $message['info'] = 'ERROR_OCCURRED';
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }
    disconnectConnection($con);

    return $message;
}
