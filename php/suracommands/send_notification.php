<?php

function send_notification($id) {
    require_once('../php/form_validate.php');
    $valid = true;
    $con = makeConnection();
    autoCommit($con, false);
    $response = ['status' => true, 'err' => null];

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }
            $mes = trim($_POST['message']);
            $userids = trim($_POST['userids']);

            $userids = explode(',', $userids);

            $validationResult = $form_validate([
                'message' => 'required|maxlength:100',
                    ], [
                'message' => $mes,
            ]);

            $sms_points = runSimpleFetchQuery($con, ['sms_wallet'], 'insurer', ['id'], ['='], [$id], '', '', 1)['result'];

            if ($sms_points < count($userids)) {
                $valid = false;
                $response['status'] = 'false';
                $response['err'] = 'INSUFFICIENT_POINTS';
            }

            $new_sms_points = null;

            $names = [];
            $phones = [];
            if ($valid) {
                foreach ($userids as $userid) {
                    $temp = runSimpleFetchQuery($con, ['f_name', 'phone'], 'users', ['id'], ['='], ["$userid"], '', '', 1)['result'];
                    $names[] = $temp[0]['f_name'];
                    $phones[] = $temp[0]['phone'];
                }
            }

            if (!empty($validationResult)) {
                $message['info'] = 'VALIDATION_ERROR';
            } else if ($valid) {
                $flag = true;
                $date = (new DateTime())->format(DateTime::ISO8601);
                $res = null;
                $new_sms_points = $sms_points - count($userids);
                
                $result = runSimpleUpdateQuery($con, 'insurer', ['sms_wallet'], ["'$new_sms_points'"], ['id'], ['='], [$id]);
                if ($result['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }
                
                $res = runSimpleInsertQuery($con, 'insurer_log', ['action', 'date', 'insurer_id'], ["'SENT_NOTIFICATION'", "'$date'", $id]);
                if ($res['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }

                $values = [];

                foreach ($userids as $userid) {
                    $values[] = ["'$mes'", "'$userid'", "'$id'", "'INSURER'", "'$date'"];
                }

                $res = runSimpleInsertQuery($con, 'user_notifications', ['message', 'rec_id', 'sender_id', 'sender_entity', 'date'], $values);
                if ($res['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }

                if ($flag) {
                    commit($con);
                    autoCommit($con, true);
                    $message['info'] = 'SUCCESS';
                    $message['points'] = $new_sms_points;
                    require 'sms/sms_sender.php';
                    for ($i = 0; $i < count($userids); $i++) {
                        sendSms($names[$i], $mes, "'$phones[$i]'", 234);
                    }
                } else {
                    rollback($con);
                    autoCommit($con, true);
                    $message['info'] = 'ERROR_OCCURRED';
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }
    disconnectConnection($con);

    return $message;
}
