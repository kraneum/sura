<?php

function send_sms($name, $message, $phone, $code = 234) {
    require_once __DIR__ . '/../sms/sms_sender.php';

    sendSms($name, $message, "'$phone'", $code);

    return 'SUCCESS';
}
