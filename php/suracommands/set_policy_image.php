<?php
require_once('sura_config.php');
require_once('sura_functions.php');

function set_policy_image($url, $policy_id) {

    $con = makeConnection();

    $imgUpdate = runSimpleUpdateQuery($con, "policies", ['image_url'], ["'" . $url . "'"], ['id'], ['='], [$policy_id])['result'];

    if ($imgUpdate) {
        echo 'SUCCESS';
    } else {
        echo 'FAILURE';
    }

    disconnectConnection($con);
}