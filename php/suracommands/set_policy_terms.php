<?php
require_once('sura_config.php');
require_once('sura_functions.php');

function set_policy_terms($url, $policy_id) {

    $con = makeConnection();

    $termUpdate = runSimpleUpdateQuery($con, "policies", ['terms_url'], ["'" . $url . "'"], ['id'], ['='], [$policy_id])['result'];

    if ($termUpdate) {
        echo 'SUCCESS';
    } else {
        echo 'FAILURE';
    }

    disconnectConnection($con);
}