<?php

function topup_pension($id) {
    $con = makeConnection();
    $flag = true;
    autoCommit($con, false);
    $amount = intval(htmlspecialchars(strip_tags(trim($_POST['amount']))));

    if (empty($amount)) {
        disconnectConnection($con);
        return ['status' => false, 'err' => 'NULL_PINCODE'];
    }

    if ($amount <= 0) {
        disconnectConnection($con);
        return ['status' => false, 'err' => 'INVALID_AMOUNT'];
    }

    $userInfo = runSimpleFetchQuery($con, ['f_name', 'phone', 'point_balance', 'dedicated_points', 'pension_points'], 'users', ['id'], ['='], [$id], '', '', '')['result'];
    if (empty($userInfo)) {
        disconnectConnection($con);
        return ['status' => false, 'err' => 'NO_SUCH_USER'];
    }
    $f_name = $userInfo[0]['f_name'];
    $phone = $userInfo[0]['phone'];
    $point_balance = intval($userInfo[0]['point_balance']);
    $dedicated_points = intval($userInfo[0]['dedicated_points']);
    $pension_points = intval($userInfo[0]['pension_points']);

    if ($point_balance < $amount) {
        disconnectConnection($con);
        return ['status' => false, 'err' => 'INSUFFICIENT_BALANCE'];
    }

    $_pensioner_id = runSimpleFetchQuery($con, ['pension_id'], 'pension_signupS', ['user_id'], ['='], [$id], '', '', '1')['result']; // for retrieving pensioner_id...
    $pension_info = runSimpleFetchQuery($con, ['id', 'name', 'redeemable_points'], 'pension', ['id'], ['='], [$_pensioner_id], '', '', '1')['result'];
    if (empty($pension_info)) {
        disconnectConnection($con);
        return ['status' => false, 'err' => 'NO_SUCH_PENSIONER'];
    }
    $pensioner_id = $pension_info[0]['id'];
    $pensioner_name = $pension_info[0]['name'];
    $pensioner_points = intval($pension_info[0]['redeemable_points']);

    $date = (new DateTime())->format(DateTime::ISO8601);

    $new_amount = floor((80 / 100) * $amount);
    $dedicated_points_temp = $amount - $new_amount;

    $new_point_balance = $point_balance - $amount;
    $new_pension_points = $pension_points + $new_amount;
    $new_dedicated_points = $dedicated_points + $dedicated_points_temp;

    $new_pensioner_points = $pensioner_points + $new_amount;

    ///////////////////////////////////////////////////////

    if ($flag == true) {

        $result = runSimpleUpdateQuery($con, 'users', ['point_balance', 'dedicated_points', 'pension_points'], ["'$new_point_balance'", "'$new_dedicated_points'", "'$new_pension_points'"], ['id'], ['='], [$id]);
        if ($result['err']['code']) {
            if ($flag) {
                $flag = false;
            }
        }

        $result = runSimpleUpdateQuery($con, 'pension', ['redeemable_points'], ["'$new_pensioner_points'"], ['id'], ['='], [$pensioner_id]);
        if ($result['err']['code']) {
            if ($flag) {
                $flag = false;
//                print_r($result);
            }
        }

        $trans_id = null;
        $result = runSimpleInsertQuery($con, 'transactions', ['reference', 'point_worth', 'date_transacted', 'type_id', 'currency_id', 'from_whom', 'to_who', 'previous_balance_from', 'previous_balance_to'], ['"#453435"', $new_amount, "'$date'", 10, 1, $id, $pensioner_id, "'$new_point_balance'", "'$new_pensioner_points'"]);
        if ($result['err']['code']) {
            if ($flag) {
                $flag = false;
            }
        } else {
            $trans_id = $result['insertedId'];
        }


        $result = runSimpleInsertQuery($con, 'user_log', ['action', 'info', 'date', 'user_id'], ["'PENSION_TOPUP'", "'$trans_id'", "'$date'", $id]);
        if ($result['err']['code']) {
            if ($flag) {
                $flag = false;
            }
        }
        $result = runSimpleInsertQuery($con, 'pension_log', ['action', 'info', 'date', 'pension_id'], ["'PENSION_TOPUP'", "'$trans_id'", "'$date'", $pensioner_id]);
        if ($result['err']['code']) {
            if ($flag) {
                $flag = false;
            }
        }


        if ($flag) {
            commit($con);
            $message['info'] = 'SUCCESS';
            $message['points'] = number_format($new_point_balance);
            $message['dedicated_points'] = number_format($new_dedicated_points);
            $message['pension_points'] = number_format($new_pension_points);

            if ($_SESSION) {
                $_SESSION['entity']['point_balance'] = $new_point_balance;
                $_SESSION['entity']['dedicated_points'] = $new_dedicated_points;
                $_SESSION['entity']['pension_points'] = $new_pension_points;
                $_SESSION['last_transaction'] = 'debit';
            }
            require 'sms/sms_sender.php';
            sendSms($f_name, "You topped up your pension with " . $new_amount . " points", "'$phone'", 234);
        } else {
            rollback($con);
            $message['info'] = 'ERROR';
        }
    } else {
        rollback($con);
        $message['info'] = 'FAILURE';
    }

    autoCommit($con, true);

    disconnectConnection($con);
    return $message;
}
