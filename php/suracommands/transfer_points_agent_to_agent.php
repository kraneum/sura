<?php

function transfer_points_agent_to_agent($id) {
    $con = makeConnection();
    $flag = true;
    autoCommit($con, false);

    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    if (!$_POST) {
        return ['status' => false, 'err' => 'INVALID_DETAILS'];
    }

    $phone = trim($_POST['phone']);
    $amount = trim($_POST['amount']);
    $date = (new DateTime())->format(DateTime::ISO8601);
    $ref = 'UICI-' . $id . round(microtime(true) * 1000);

    $recipientInfo = runSimpleFetchQuery($con, ['id', 'f_name', 'phone', 'points'], 'agent', ['phone'], ['='], ["'$phone'"], '', '', '')['result'];
    if ($recipientInfo) {
        $rec_points = intval($recipientInfo[0]['points']);
        $rec_name = $recipientInfo[0]['f_name'];
        $rec_number = $recipientInfo[0]['phone'];
    }
    
    $selfInfo = runSimpleFetchQuery($con, ['id', 'phone', 'points'], 'agent', ['id'], ['='], [$id], '', '', '')['result'];
    $agentid = $selfInfo[0]['id'];
    $ownNumber = $selfInfo[0]['phone'];
    $points = intval($selfInfo[0]['points']);

    if (!$recipientInfo) {
        autoCommit($con, true);
        $message['info'] = 'ID_NOT_RESOLVED';
    } else if ($agentid === $ownNumber) {
        autoCommit($con, true);
        $message['info'] = 'SELF_TRANSFER';
    } else if ($amount <= 0) {
        autoCommit($con, true);
        $message['info'] = 'INVALID_AMOUNT';
    } else if ($amount > $points) {
        autoCommit($con, true);
        $message['info'] = 'INSUFFICIENT_POINTS';
    } else {
        $new_rec_points = $rec_points + $amount;
        $new_points = $points - $amount;

        $result = runSimpleUpdateQuery($con, 'agent', ['points'], ["$new_points"], ['id'], ['='], [$id]);
        if ($result['err']['code']) {
            if ($flag) {
                $flag = false;
            }
        }

        $result = runSimpleUpdateQuery($con, 'agent', ['points'], ["$new_rec_points"], ['id'], ['='], ["'$agentid'"]);
        if ($result['err']['code']) {
            if ($flag) {
                $flag = false;
            }
        }

        $result = runSimpleInsertQuery($con, 'point_transfers', ['reference', 'point_worth', 'date_transacted', 'type_id', 'currency_id', 'from_whom', 'to_who', 'previous_balance_from', 'previous_balance_to'], ["'$ref'", "'$amount'", "'$date'", 6, 1, "'$id'", "'$agentid'", "'$new_points'", "'$new_rec_points'"]);
        if ($result['err']['code']) {
            if ($flag) {
                $flag = false;
            }
        }
        $insertedID = $result['insertedId'];

        $result = runSimpleInsertQuery($con, 'agent_log', ['action', 'info', 'date', 'agent_id'], ["'TRANSFER_POINTS_AGENT'", "'$insertedID'", "'$date'", $id]);
        if ($result['err']['code']) {
            if ($flag) {
                $flag = false;
            }
        }

        $result = runSimpleInsertQuery($con, 'agent_log', ['action', 'info', 'date', 'agent_id'], ["'RECEIVED_POINTS_AGENT'", "'$insertedID'", "'$date'", $agentid]);
        if ($result['err']['code']) {
            if ($flag) {
                $flag = false;
            }
        }

        if ($flag) {
            commit($con);
            autoCommit($con, true);
            $message['info'] = 'SUCCESS';
            $message['points'] = number_format($new_points);
            if (isset($_SESSION)) {
                $_SESSION['last_transfer_type'] = 'debit';
                $_SESSION['entity']['points'] = $new_points;
            }
            require 'send_sms.php';
            send_sms($rec_name, 'You were transferred ' . $amount . ' points from an Agent' . ' - ' . $ownNumber, "'$rec_number'", 234);
        } else {
            rollback($con);
            autoCommit($con, true);
            $message['info'] = 'ERROR';
        }
    }
    disconnectConnection($con);

    return $message;
}
