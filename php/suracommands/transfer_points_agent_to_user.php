<?php

function transfer_points_agent_to_user($id) {
    $con = makeConnection();
    $flag = true;
    autoCommit($con, false);

    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    if (!$_POST) {
        return ['status' => false, 'err' => 'INVALID_DETAILS'];
    }

    $phones = $_POST['phone'];
    $amounts = $_POST['amount'];

    $date = (new DateTime())->format(DateTime::ISO8601);
    $ref = 'UICI-' . $id . round(microtime(true) * 1000);
    
    $rec_ids = [];
    $rec_points = [];
    $valid = true;

    $selfInfo = runSimpleFetchQuery($con, ['f_name', 'phone', 'points'], 'agent', ['id'], ['='], [$id], '', '', '')['result'];
    $self_name = $selfInfo[0]['f_name'];
    $selfPhone = $selfInfo[0]['phone'];
    $selfPoints = intval($selfInfo[0]['points']);
    $duplicates = null;

    if (!(array_unique($phones) == $phones)) { // checks for duplicate phone numbers
        $duplicates = array_diff_assoc($phones, array_unique($phones)); // returns assaociative array of 'index => duplicate_number'
        $valid = false;
        $message['info'] = 'DUPLICATE_NUMBERS';
        $message['extra'] = $duplicates;
    }

    if ($valid) {
        foreach ($phones as $i => $phone) { // check for registered numbers
            $recipientInfo = runSimpleFetchQuery($con, ['id', 'health_point_balance', 'general_point_balance'], 'users', ['phone'], ['='], ["'$phone'"], '', '', '')['result'];
            if ($recipientInfo) {
                $rec_ids[] = $recipientInfo[0]['id'];
                $health_rec_points[] = intval($recipientInfo[0]['health_point_balance']);
                $general_rec_points[] = intval($recipientInfo[0]['general_point_balance']);
            } else {
                $valid = false;
                $message['info'] = 'NUMBER_NOT_RESOLVED';
                $message['extra'] = +$i + 1;
                break;
            }
        }
    }

    $total = 0;
    if ($valid) {
        foreach ($amounts as $i => $amount) {
            $total += $amount;
        }
    }
    if ($total <= 0 && $valid) {
        $message['info'] = 'INVALID_AMOUNT';
    } else if ($total > $selfPoints && $valid) {
        $message['info'] = 'INSUFFICIENT_POINTS';
    } else if ($valid) {

        for ($i = 0; $i < count($phones); $i++) {
            $old_health_rec_points = intval($health_rec_points[$i]);
            $old_general_rec_points = intval($general_rec_points[$i]);
            $current_rec_id = $rec_ids[$i];
            $current_amount = intval($amounts[$i]);
            $current_phone = $phones[$i];

            $new_health_rec_points = $old_health_rec_points + ceil($current_amount/2);
            $new_general_rec_points = $old_general_rec_points + floor($current_amount/2);
            $selfPoints = $selfPoints - $current_amount;

            $result = runSimpleUpdateQuery($con, 'agent', ['points'], ["$selfPoints"], ['id'], ['='], [$id]);
            if ($result['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }
            
            $result = runSimpleUpdateQuery($con, 'users', ['health_point_balance', 'general_point_balance'], 
                    ["$new_health_rec_points", "$new_general_rec_points"], ['phone'], ['='], ["'$current_phone'"]);
            if ($result['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }

            $new_rec_points = $new_health_rec_points + $new_general_rec_points;
            $result = runSimpleInsertQuery($con, 'point_transfers', ['reference', 'point_worth', 'date_transacted', 'type_id', 'currency_id', 'from_whom', 'to_who', 'previous_balance_from', 'previous_balance_to'], ["'$ref'", "'$current_amount'", "'$date'", 7, 1, "'$id'", "'$current_phone'", "'$selfPoints'", "'$new_rec_points'"]);
            if ($result['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }
            $insertedID_p = $result['insertedId'];

            $result = runSimpleInsertQuery($con, 'transactions', ['reference', 'point_worth', 'date_transacted', 'type_id', 'currency_id', 'from_whom', 'to_who', 'previous_balance_from', 'previous_balance_to'], ["'$ref'", "'$current_amount'", "'$date'", 7, 1, $id, $current_rec_id, $selfPoints, $new_rec_points]);
            if ($result['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }
            $insertedID_t = $result['insertedId'];

            $result = runSimpleInsertQuery($con, 'agent_log', ['action', 'info', 'date', 'agent_id'], ["'TRANSFER_POINTS_USER'", "'$insertedID_p'", "'$date'", $id]);
            if ($result['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }

            $result = runSimpleInsertQuery($con, 'user_log', ['action', 'info', 'date', 'user_id'], ["'RECEIVED_POINTS_AGENT'", "'$insertedID_t'", "'$date'", $current_rec_id]);
            if ($result['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }
        }

        if ($flag) {
            commit($con);
            $message['info'] = 'SUCCESS';
            $message['points'] = number_format($selfPoints);
            if (isset($_SESSION)) {
                $_SESSION['last_transfer_type'] = 'debit';
                $_SESSION['entity']['points'] = $selfPoints;
            }
            require 'send_sms.php';
            for ($i = 0; $i < count($phones); $i++) {
                send_sms('', 'You were transferred ' . $amounts[$i] . ' points from an Agent' . ' - ' . $selfPhone, "'$phones[$i]'", 234);
            }
        } else {
            rollback($con);
            $message['info'] = 'ERROR';
        }
    }

    autoCommit($con, true);
    disconnectConnection($con);

    return $message;
}
