<?php

function transfer_points_user_to_user($id) {
    $con = makeConnection_user();
    $flag = true;
    autoCommit($con, false);

    if ($con) {
        // check for existence of a signed up basic policy
        $basicPolicyInfo = runGetUserBasicPolicies($con, $id)['result'];
        if (empty($basicPolicyInfo)) {
            echo json_encode(['status' => false, 'info' => 'NO_BASIC_POLICY']);
            autoCommit($con, true);
            disconnectConnection($con);
            exit();
        }
        ///////////////////
        
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            // clean user inputs to prevent sql injections
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'info' => 'INVALID_DETAILS'];
            }
            $phone = trim($_POST['phone']);
            $amount = intval(trim($_POST['amount']));
            $wallet = ($_POST['wallet']);
            $date = (new DateTime())->format(DateTime::ISO8601);
            $ref = 'UICI-' . $id . round(microtime(true) * 1000);
            
            $main_wallet_column = null;
            $other_wallet_column = null;
            
            if ($wallet == 'health') {
                $main_wallet_column = 'health_point_balance';
                $other_wallet_column = 'general_point_balance';
            } else if ($wallet == 'general'){
                $main_wallet_column = 'general_point_balance';
                $other_wallet_column = 'health_point_balance';
            }

            $recipientInfo = runSimpleFetchQuery($con, ['id', 'f_name', 'phone', $main_wallet_column, $other_wallet_column], 'users', ['phone'], ['='], ["'$phone'"], '', '', '')['result'];
            if (empty($recipientInfo)) {
                autoCommit($con, true);
                return ['status' => false, 'info' => 'NUMBER_NOT_RESOLVED'];
            }
            $rec_id = $recipientInfo[0]['id'];
            $rec_number = $recipientInfo[0]['phone'];
            $rec_main_points = intval($recipientInfo[0][$main_wallet_column]);
            $rec_other_points = intval($recipientInfo[0][$other_wallet_column]);
            $rec_fname = $recipientInfo[0]['f_name'];

            $selfInfo = runSimpleFetchQuery($con, ['f_name', 'phone', $main_wallet_column, $other_wallet_column], 'users', ['id'], ['='], [$id], '', '', '')['result'];
            if (empty($selfInfo)) {
                autoCommit($con, true);
                return ['status' => false, 'info' => 'INVALID_USER'];
            }
            $ownNumber = $selfInfo[0]['phone'];
            $main_points = intval($selfInfo[0][$main_wallet_column]);
            $other_points = intval($selfInfo[0][$other_wallet_column]);
            $self_fname = $selfInfo[0]['f_name'];

            if ($rec_number === $ownNumber) {
                autoCommit($con, true);
                $message['info'] = 'SELF_TRANSFER';
            } else if ($amount <= 0) {
                autoCommit($con, true);
                $message['info'] = 'INVALID_AMOUNT';
            } else if ($amount > $main_points) {
                autoCommit($con, true);
                $message['info'] = 'INSUFFICIENT_POINTS';
            } else {
                $new_main_rec_points = $rec_main_points + $amount;
                $new_main_points = $main_points - $amount;

                $result = runSimpleUpdateQuery($con, 'users', [$main_wallet_column], ["$new_main_points"], ['id'], ['='], [$id]);
                if ($result['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }

                $result = runSimpleUpdateQuery($con, 'users', [$main_wallet_column], ["$new_main_rec_points"], ['phone'], ['='], ["'$phone'"]);
                if ($result['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }
                
                $new_points = $new_main_points + $other_points;
                $new_rec_points = $new_main_rec_points + $rec_other_points;
                $result = runSimpleInsertQuery($con, 'transactions', ['reference', 'point_worth', 'date_transacted', 'type_id', 'currency_id', 'from_whom', 'to_who', 'previous_balance_from', 'previous_balance_to'], ["'$ref'", "'$amount'", "'$date'", 4, 1, $id, $rec_id, $new_points, $new_rec_points]);
                if ($result['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }
                $insertedID = $result['insertedId'];
                
                $result = runSimpleInsertQuery($con, 'user_log', ['action', 'info', 'date', 'user_id'], ["'TRANSFER_POINTS'", "'$insertedID'", "'$date'", $id]);
                if ($result['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }
                $result = runSimpleInsertQuery($con, 'user_log', ['action', 'info', 'date', 'user_id'], ["'RECEIVED_POINTS_USER'", "'$insertedID'", "'$date'", $rec_id]);
                if ($result['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }

                if ($flag) {
                    commit($con);
                    autoCommit($con, true);
                    $message['info'] = 'SUCCESS';
                    $message['points'] = number_format($new_main_points);
                    $message['wallet'] = $wallet;
                    if (isset($_SESSION)) {
                        $_SESSION['entity'][$main_wallet_column] = $new_main_points;
                        $_SESSION['last_transaction'] = 'debit';
                    }
                    require 'send_sms.php';
                    send_sms($rec_fname, 'You were transferred ' . $amount . ' points from ' . $self_fname . ' - ' . $ownNumber, "'$rec_number'", 234);
                } else {
                    rollback($con);
                    autoCommit($con, true);
                    $message['info'] = 'ERROR';
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    autoCommit($con, true);
    disconnectConnection($con);

    return $message;
}
