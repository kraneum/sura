<?php

function update_agent_profile($id) {
    require_once('../php/form_validate.php');

    $con = makeConnection();
    autoCommit($con, false);

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }

            $fname = trim($_POST['fname']);
            $lname = trim($_POST['lname']);
            $dob = trim($_POST['dob']);
            $gender = trim($_POST['gender']);
            $email = trim($_POST['email']);
            $phone = trim($_POST['phone']);

            $validationResult = $form_validate([
                'fname' => 'required|maxlength:100',
                'lname' => 'required|maxlength:100',
                'dob' => 'required|dob|maxlength:100',
                'gender' => 'required|maxlength:10',
                'phone' => 'required|phone|maxlength:11',
                'email' => 'required|email|maxlength:100',
                    ], [
                'fname' => $fname,
                'lname' => $lname,
                'dob' => $dob,
                'gender' => $gender,
                'phone' => $phone,
                'email' => $email,
            ]);

            if (!empty($validationResult)) {
                $message['info'] = 'VALIDATION_ERROR';
                $message['errVal'] = $validationResult;
            } else {
                $flag = true;
                $date = (new DateTime())->format(DateTime::ISO8601);

                $res = runSimpleInsertQuery($con, 'agent_log', ['action', 'date', 'agent_id'], ["'EDIT_PROFILE'", "'$date'", $id]);
                if ($res['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }

                $res = runSimpleUpdateQuery($con, 'agent', ['f_name', 'l_name', 'gender', 'dob', 'email', 'phone'],
                        ["'$fname'", "'$lname'", "'$gender'", "'$dob'", "'$email'", "'$phone'"], ['id'], ['='], [$id]);
                if ($res['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }

                if ($flag) {
                    if ($res['result'] == 0 && !$res['err']['code']) {
                        rollback($con);
                        autoCommit($con, true);
                        $message['info'] = 'NO_EDIT';
                    } else if (!$res['err']['code']) {
                        commit($con);
                        autoCommit($con, true);
                        $message['info'] = 'SUCCESS';
                        if ($_SESSION) {
                            $_SESSION['entity']['f_name'] = $fname;
                            $_SESSION['entity']['l_name'] = $lname;
                            $_SESSION['entity']['phone'] = $phone;
                            $_SESSION['entity']['email'] = $email;
                            $_SESSION['entity']['dob'] = $dob;
                            $_SESSION['entity']['gender'] = $gender;
                        }
                    } else {
                        rollback($con);
                        autoCommit($con, true);
                        $message['info'] = 'FAILURE';
                        $message['details'] = $res['err'];
                    }
                } else {
                    rollback($con);
                    autoCommit($con, true);
                    $message['info'] = 'ERROR_OCCURRED';
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }
    autoCommit($con, true);

    disconnectConnection($con);

    return $message;
}
