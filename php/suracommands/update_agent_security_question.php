<?php

function update_agent_security_question($id) {
    require_once('../php/form_validate.php');

    $con = makeConnection();
    autoCommit($con, false);

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }

            $question = trim($_POST['question']);
            $answer = trim($_POST['answer']);

            $validationResult = $form_validate([
                'question' => 'required|maxlength:100',
                'answer' => 'required|maxlength:100'
                    ], [
                'question' => $question,
                'answer' => $answer
            ]);

            if (!empty($validationResult)) {
                $message['info'] = 'VALIDATION_ERROR';
            } else {
                $flag = true;
                $date = (new DateTime())->format(DateTime::ISO8601);

                $res = runSimpleInsertQuery($con, 'agent_log', ['action', 'date', 'agent_id'], ["'CHANGE_SEC_QUE'", "'$date'", $id]);
                if ($res['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }
                $result = runSimpleUpdateQuery($con, 'agent', ['security_question', 'security_answer'], ["'$question'", "'$answer'"], ['id'], ['='], [$id]);
                if ($res['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }

                if ($flag) {
                    if (!$result['err']['code']) {
                        $bool = $result['result'];
                        if ($bool > 0) {
                            commit($con);
                            autoCommit($con, true);
                            $message['info'] = 'SUCCESS';
                        } else {
                            rollback($con);
                            autoCommit($con, true);
                            $message['info'] = 'FAILURE';
                        }
                    } else {
                        rollback($con);
                        autoCommit($con, true);
                        $message['info'] = 'DATABASE_ERROR';
                    }
                } else {
                    rollback($con);
                    autoCommit($con, true);
                    $message['info'] = 'ERROR_OCCURRED';
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }
    autoCommit($con, true);

    disconnectConnection($con);
    return $message;
}
