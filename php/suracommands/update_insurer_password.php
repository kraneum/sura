<?php

function update_insurer_password($id) {
    require_once('../php/form_validate.php');

    $con = makeConnection_insurer();
    autoCommit($con, false);

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }

            $oldPassword = trim($_POST['oldPassword']);
            $newPassword = trim($_POST['newPassword']);

            $validationResult = $form_validate([
                'oldPassword' => 'required|maxlength:100',
                'newPassword' => 'required|maxlength:100'
                    ], [
                'oldPassword' => $oldPassword,
                'newPassword' => $newPassword
            ]);

            if (!empty($validationResult)) {
                $message['info'] = 'VALIDATION_ERROR';
            } else {
                $pass = runSimpleFetchQuery($con, ['password'], 'insurer', ['id'], ['='], [$id], '', '', '')['result'];
                if (password_verify($oldPassword, $pass)) {
                    if (password_verify($newPassword, $pass)) {
                        $message['info'] = 'NO_CHANGES';
                    } else {
                        $flag = true;
                        $date = (new DateTime())->format(DateTime::ISO8601);

                        $res = runSimpleInsertQuery($con, 'insurer_log', ['action', 'date', 'insurer_id'], ["'CHANGE_PASSWORD'", "'$date'", $id]);
                        if ($res['err']['code']) {
                            if ($flag) {
                                $flag = false;
                            }
                        }
                        $newPassword = password_hash($newPassword, PASSWORD_BCRYPT);
                        $res = runSimpleUpdateQuery($con, 'insurer', ['password'], ["'$newPassword'"], ['id'], ['='], [$id]);
                        if ($res['err']['code']) {
                            if ($flag) {
                                $flag = false;
                            }
                        }

                        if ($flag) {
                            if (!$res['err']['code']) {
                                $bool = $res['result'];
                                if ($bool > 0) {
                                    commit($con);
                                    autoCommit($con, true);
                                    $message['info'] = 'SUCCESS';
                                } else {
                                    rollback($con);
                                    autoCommit($con, true);
                                    $message['info'] = 'FAILURE';
                                }
                            }
                        } else {
                            rollback($con);
                            autoCommit($con, true);
                            $message['info'] = 'ERROR_OCCURRED';
                        }
                    }
                } else {
                    $message['info'] = 'INCORRECT_PASSWORD';
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    autoCommit($con, true);
    disconnectConnection($con);

    return $message;
}
