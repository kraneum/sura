<?php

function update_notifications_pension($id) {

    $con = makeConnection();
    autoCommit($con, false);

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }
            $_notifIDs = trim($_POST['notifIDs']);

            $notifIDs = explode(',', $_notifIDs);

            $flag = true;

            foreach ($notifIDs as $notifID) {
                $trans = runSimpleFetchQuery($con, ['id', 'pension_id'], 'pension_notifications', ['id'], ['='], ["'$notifID'"], '', '', '1')['result'];

                $pension_id = $trans[0]['pension_id'];

                if ($pension_id == 0) {
                    $res = runSimpleInsertQuery($con, "uic_notif_ref", ['notif_id', 'entity', 'entity_id'], ["'$notifID'", "'PENSION'", "'$id'"]);
                    if ($res['err']['code']) {
                        if ($flag) {
                            $flag = false;
                            break;
                        }
                    }
                } else {
                    $res = runSimpleUpdateQuery($con, 'pension_notifications', ['unread'], ["'FALSE'"], ['id'], ['='], ["'$notifID'"]);
                    if ($res['err']['code']) {
                        if ($flag) {
                            $flag = false;
                            break;
                        }
                    }
                }
            }
            if ($flag) {
                commit($con);
                if ($_SESSION) {
                    $notifCount = intval($_SESSION['notifCount']);
                    $_SESSION['notifCount'] = $notifCount = $notifCount - count($notifIDs);
                }
                $message['info'] = 'SUCCESS';
                $message['notifCount'] = $notifCount;
            } else {
                rollback($con);
                $message['info'] = 'ERROR_OCCURRED';
            }
        } else {
            $message['status'] = false;
            $message['err'] = 'WRONG_REQUEST_TYPE';
        }
    } else {
        $message['status'] = false;
        $message['err'] = 'DATABASE_CONNECT_ERROR';
    }

    autoCommit($con, true);
    disconnectConnection($con);
    return $message;
}
