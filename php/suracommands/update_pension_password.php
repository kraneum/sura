<?php

function update_pension_password($id) {
    require_once('../php/form_validate.php');

    $con = makeConnection();
    autoCommit($con, false);

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }

            $oldPassword = trim($_POST['oldPassword']);
            $newPassword = trim($_POST['newPassword']);

            $validationResult = $form_validate([
                'oldPassword' => 'required|maxlength:100',
                'newPassword' => 'required|maxlength:100'
                    ], [
                'oldPassword' => $oldPassword,
                'newPassword' => $newPassword
            ]);

            if (!empty($validationResult)) {
                $message['info'] = 'VALIDATION_ERROR';
            } else {
                $pass = runSimpleFetchQuery($con, ['password'], 'pension', ['id'], ['='], [$id], '', '', '')['result'];
                if ($pass === $oldPassword) {
                    if ($pass === $newPassword) {
                        $message['info'] = 'NO_CHANGES';
                    } else {
                        $flag = true;
                        $date = (new DateTime())->format(DateTime::ISO8601);

                        $res = runSimpleInsertQuery($con, 'pension_log', ['action', 'date', 'pension_id'], ["'CHANGE_PASSWORD'", "'$date'", $id]);
                        if ($res['err']['code']) {
                            if ($flag) {
                                $flag = false;
                            }
                        }
                        $res = runSimpleUpdateQuery($con, 'pension', ['password'], ["'$newPassword'"], ['id'], ['='], [$id]);
                        if ($res['err']['code']) {
                            if ($flag) {
                                $flag = false;
                            }
                        }

                        if ($flag) {
                            if (!$res['err']['code']) {
                                $bool = $res['result'];
                                if ($bool > 0) {
                                    commit($con);
                                    autoCommit($con, true);
                                    $message['info'] = 'SUCCESS';
                                } else {
                                    rollback($con);
                                    autoCommit($con, true);
                                    $message['info'] = 'FAILURE';
                                }
                            }
                        } else {
                            rollback($con);
                            autoCommit($con, true);
                            $message['info'] = 'ERROR_OCCURRED';
                        }
                    }
                } else {
                    $message['info'] = 'INCORRECT_PASSWORD';
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    autoCommit($con, true);
    disconnectConnection($con);

    return $message;
}
