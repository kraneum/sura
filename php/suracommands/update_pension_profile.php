<?php

function update_pension_profile($id) {
    require_once('../php/form_validate.php');

    $con = makeConnection();
    autoCommit($con, false);

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }

            $desc = trim($_POST['desc']);
            $acc = trim($_POST['acc']);
            $email = trim($_POST['email']);
            $phone = trim($_POST['phone']);

            $validationResult = $form_validate([
                'desc' => 'required|maxlength:100',
                'acc' => 'required|maxlength:10',
                'phone' => 'required|phone|maxlength:11',
                'email' => 'required|email|maxlength:100'
                    ], [
                'desc' => $desc,
                'acc' => $acc,
                'phone' => $phone,
                'email' => $email
            ]);

            if (!empty($validationResult)) {
                $message['info'] = 'VALIDATION_ERROR';
                $message['errVal'] = $validationResult;
            } else {
                $flag = true;
                $date = (new DateTime())->format(DateTime::ISO8601);

                $res = runSimpleInsertQuery($con, 'pension_log', ['action', 'date', 'pension_id'], ["'EDIT_PROFILE'", "'$date'", $id]);
                if ($res['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }

                $res = runSimpleUpdateQuery($con, 'pension', ['write_up', 'account_no', 'email', 'phone'],
                        ["'$desc'", "'$acc'", "'$email'", "'$phone'"], ['id'], ['='], [$id]);
                if ($res['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }

                if ($flag) {
                    if (!$res['err']['code'] && $res['result'] == 0) {
                        commit($con);
                        autoCommit($con, true);
                        $message['info'] = 'NO_EDIT';
                    } else if (!$res['err']['code']) {
                        commit($con);
                        autoCommit($con, true);
                        $message['info'] = 'SUCCESS';
                        if ($_SESSION) {
                            $_SESSION['entity']['phone'] = $phone;
                            $_SESSION['entity']['email'] = $email;
                            $_SESSION['entity']['write_up'] = $desc;
                            $_SESSION['entity']['account_no'] = $acc;
                        }
                    } else {
                        rollback($con);
                        autoCommit($con, true);
                        $message['info'] = 'FAILURE';
                    }
                } else {
                    rollback($con);
                    autoCommit($con, true);
                    $message['info'] = 'ERROR_OCCURRED';
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    autoCommit($con, true);
    disconnectConnection($con);

    return $message;
}
