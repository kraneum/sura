<?php

function update_policy_info($id) {
    require_once('../php/form_validate.php');

    $con = makeConnection_user();
    autoCommit($con, false);

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        // clean user inputs to prevent sql injections
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if (!$_POST) {
            return ['status' => false, 'err' => 'INVALID_DETAILS'];
        }

        $policyID = trim($_POST['policyID']);
        $title = trim($_POST['title']);
        $desc = trim($_POST['desc']);
        $extlink = trim($_POST['extlink']);
        $price = trim($_POST['price']);
        $tenure = trim($_POST['tenure']);

        $validationResult = $form_validate([
            'policyID' => 'required|maxlength:100',
            'title' => 'required|maxlength:100',
            'desc' => 'required|maxlength:100',
            'extlink' => 'required|url|maxlength:500',
            'price' => 'required|digits|maxlength:10',
            'tenure' => 'required|maxlength:10'
                ], [
            'policyID' => $policyID,
            'title' => $title,
            'desc' => $desc,
            'extlink' => $extlink,
            'price' => $price,
            'tenure' => $tenure
        ]);

        if (!empty($validationResult)) {
            $message['info'] = 'VALIDATION_ERROR';
            $message['errVal'] = $validationResult;
        } else {
            $flag = true;
            $date = (new DateTime())->format(DateTime::ISO8601);

            $res = runSimpleInsertQuery($con, 'insurer_log', ['action', 'info', 'date', 'insurer_id'], ["'EDIT_POLICY'", "'$policyID'", "'$date'", $id]);
            if ($res['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }
            $res = runSimpleUpdateQuery($con, 'policies', ['title', 'description', 'more_info_link', 'price', 'tenure'], ["'$title'", "'$desc'", "'$extlink'", "'$price'", "'$tenure'"], ['id', 'insurer_id'], ['=', '='], [$policyID, $id]);
            if ($res['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }

            if ($flag) {
                if ($res['result'] == 0 && !$res['err']['code']) {
                    rollback($con);
                    autoCommit($con, true);
                    $message['info'] = 'NO_CHANGES';
                } else if (!$res['err']['code']) {
                    commit($con);
                    autoCommit($con, true);
                    $message['info'] = 'SUCCESS';
                } else {
                    rollback($con);
                    autoCommit($con, true);
                    $message['info'] = 'FAILURE';
                }
            } else {
                rollback($con);
                autoCommit($con, true);
                $message['info'] = 'ERROR_OCCURRED';
            }
        }
    } else {
        $message['info'] = 'WRONG_REQUEST_METHOD';
    }


    autoCommit($con, true);
    disconnectConnection($con);

    return $message;
}
