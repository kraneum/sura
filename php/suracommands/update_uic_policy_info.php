<?php
session_name('UICIMA');
session_start();

function update_uic_policy_info() {
    require_once('../php/form_validate.php');

    $con = makeConnection_user();

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        // clean user inputs to prevent sql injections
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if (!$_POST) {
            return ['status' => false, 'err' => 'INVALID_DETAILS'];
        }

        $policyID = $_POST['policyid'];
        $title = $_POST['title'];
        $desc = $_POST['desc'];
        $extlink = $_POST['extlink'];
        $price = $_POST['price'];
        $tenure = $_POST['tenure'];

        $validationResult = $form_validate([
            'policyid' => 'required|maxlength:100',
            'title' => 'required|maxlength:100',
            'desc' => 'required|maxlength:250',
            'extlink' => 'required|maxlength:500',
            'price' => 'required|number',
            'tenure' => 'required|maxlength:10'
                ], [
            'policyid' => $policyID,
            'title' => $title,
            'desc' => $desc,
            'extlink' => $extlink,
            'price' => $price,
            'tenure' => $tenure
        ]);

        $selfInfo = runSimpleFetchQuery($con, ['id', 'email'], 'admins', ['id'], ['='], [$_SESSION['id']], '', '', 1)['result'];
        if (empty($selfInfo)) {
            disconnectConnection($con);
            return ['status' => false, 'err' => 'NO_SUCH_ADMIN', 'message' => 'Enter correct admin login details'];
        }
        $self_email = $selfInfo[0]['email'];

        if (!empty($validationResult)) {
            $message['status'] = false;
            $message['info'] = 'VALIDATION_ERROR';
            $message['errVal'] = $validationResult;
        } else {
            autoCommit($con, false);
            $flag = true;
            $date = (new DateTime())->format(DateTime::ISO8601);

            do {
                $res = runSimpleInsertQuery($con, 'admins_activities', ['admin', 'entity', 'summary', 'datetimezone'], 
                        ["'$self_email'", "'$title'", "'edit_policy'", "'$date'"]);
                if ($res['err']['code']) {
                    $flag = false;
                    break;
                }
                $res = runSimpleUpdateQuery($con, 'policies', ['title', 'description', 'more_info_link', 'price', 'tenure'], ["'$title'", "'$desc'", "'$extlink'", "'$price'", "'$tenure'"], ['id', 'insurer_id'], ['=', '='], [$policyID, 0]);
                if ($res['err']['code']) {
                    $flag = false;
                }
            } while (false);

            if ($flag) {
                if ($res['result'] == 0 && !$res['err']['code']) {
                    rollback($con);
                    $message['status'] = false;
                    $message['info'] = 'NO_CHANGES';
                } else if (!$res['err']['code']) {
                    commit($con);
                    $message['status'] = true;
                    $message['info'] = 'SUCCESS';
                } else {
                    rollback($con);
                    $message['status'] = false;
                    $message['info'] = 'FAILURE';
                }
            } else {
                rollback($con);
                $message['status'] = false;
                $message['info'] = 'ERROR_OCCURRED';
            }
        }
    } else {
        $message['status'] = false;
        $message['info'] = 'WRONG_REQUEST_METHOD';
    }


    autoCommit($con, true);
    disconnectConnection($con);

    return $message;
}
