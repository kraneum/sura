<?php

function update_user_profile($id) {
    require_once('../php/form_validate.php');

    $con = makeConnection_user();
    autoCommit($con, false);

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }

            $fname = trim($_POST['fname']);
            $lname = trim($_POST['lname']);
            $dob = trim($_POST['dob']);
            $gender = trim($_POST['gender']);
            $email = trim($_POST['email']);
            $address = trim($_POST['address']);
            $lga = trim($_POST['local_gov']);
            $kin_name = trim($_POST['kin_name']);
            $kin_rel = trim($_POST['kin_rel']);
            $kin_phone = trim($_POST['kin_phone']);

            $validationResult = $form_validate([
                'fname' => 'required|maxlength:100',
                'lname' => 'required|maxlength:100',
                'dob' => 'required|dob|maxlength:100',
                'gender' => 'required|maxlength:10',
                'address' => 'required',
                'local_gov' => 'required',
                'email' => 'required|email|maxlength:100',
                'kin_name' => 'required|maxlength:100',
                'kin_rel' => 'required|maxlength:100',
                'kin_phone' => 'required|maxlength:11'
                    ], [
                'fname' => $fname,
                'lname' => $lname,
                'dob' => $dob,
                'gender' => $gender,
                'address' => $address,
                'local_gov' => $lga,
                'email' => $email,
                'kin_name' => $kin_name,
                'kin_rel' => $kin_rel,
                'kin_phone' => $kin_phone
            ]);

            if (!empty($validationResult)) {
                $message['info'] = 'VALIDATION_ERROR';
                $message['errVal'] = $validationResult;
            } else {
                $flag = true;
                $date = (new DateTime())->format(DateTime::ISO8601);

                $res = runSimpleInsertQuery($con, 'user_log', ['action', 'date', 'user_id'], ["'EDIT_PROFILE'", "'$date'", $id]);
                if ($res['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }

                $res = runSimpleUpdateQuery($con, 'users', ['f_name', 'l_name', 'dob', 'gender', 'email', 'address', 'local_gov', 'next_of_kin', 'kin_phone', 'kin_relationship'], ["'$fname'", "'$lname'", "'$dob'", "'$gender'", "'$email'", "'$address'", "'$lga'", "'$kin_name'", "'$kin_phone'", "'$kin_rel'"], ['id'], ['='], ["'$id'"]);
                if ($res['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }

                if ($flag) {
                    if ($res['result'] == 0 && !$res['err']['code']) {
                        rollback($con);
                        autoCommit($con, true);
                        $message['info'] = 'NO_EDIT';
                    } else if (!$res['err']['code']) {
                        commit($con);
                        autoCommit($con, true);
                        $message['info'] = 'SUCCESS';
                        if ($_SESSION) {
                            $_SESSION['entity']['f_name'] = $fname;
                            $_SESSION['entity']['l_name'] = $lname;
                            $_SESSION['entity']['address'] = $address;
                            $_SESSION['entity']['local_gov'] = $lga;
                            $_SESSION['entity']['email'] = $email;
                            $_SESSION['entity']['dob'] = $dob;
                            $_SESSION['entity']['gender'] = $gender;
                            $_SESSION['entity']['next_of_kin'] = $kin_name;
                            $_SESSION['entity']['kin_phone'] = $kin_phone;
                            $_SESSION['entity']['kin_relationship'] = $kin_rel;
                        }
                    } else if ($res['err']['code'] == 1062) {
                        rollback($con);
                        autoCommit($con, true);
                        $message['info'] = 'DUPLICATE_ENTRY';
                    } else {
                        rollback($con);
                        autoCommit($con, true);
                        $message['info'] = 'FAILURE';
                    }
                } else {
                    rollback($con);
                    autoCommit($con, true);
                    $message['info'] = 'ERROR_OCCURRED';
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }
    autoCommit($con, false);
    disconnectConnection($con);

    return $message;
}
