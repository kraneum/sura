<?php

function upload_insurer_logo($id) {
    require_once('../php/form_validate.php');
    $con = makeConnection_insurer();
    autoCommit($con, false);

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $logo = $_FILES['logo'];
            $validationResult = $form_validate([
                'logo' => 'filerequired|filemaxmegabytes:2|filemimetypes:image/jpeg,image/png|imagedimensions:min_width=100,min_height=100'
                    ], ['logo' => $logo]);

            if (!empty($validationResult)) {
                $message['info'] = 'VALIDATION_ERROR';
                $message['errVal'] = $validationResult;
            } else if (!getimagesize($logo["tmp_name"])) {
                $message['info'] = 'FILE_ERROR';
            } else {
                $uploaddir = '../img/insurers/';
                $logo_name = 'logo_insurer_id_' . $id . '.' . pathinfo(basename($logo["name"]), PATHINFO_EXTENSION);
                $uploadfile = $uploaddir . $logo_name;

                if (file_exists($uploadfile)) {
                    unlink($uploadfile);
                }

                if (move_uploaded_file($logo["tmp_name"], $uploadfile)) {
                    $flag = true;
                    $date = (new DateTime())->format(DateTime::ISO8601);

                    $res = runSimpleInsertQuery($con, 'insurer_log', ['action', 'date', 'insurer_id'], ["'CHANGE_LOGO'", "'$date'", $id]);
                    if ($res['err']['code']) {
                        if ($flag) {
                            $flag = false;
                        }
                    }
                    $res = runSimpleUpdateQuery($con, "insurer", ['logo_url'], ["'$logo_name'"], ['id'], ['='], [$id]);
                    if ($res['err']['code']) {
                        if ($flag) {
                            $flag = false;
                        }
                    }
                    if ($flag) {
                        if (!$res['err']['code']) {
                            commit($con);
                            autoCommit($con, true);
                            $message['info'] = 'SUCCESS';
                            $message['path'] = $logo_name;
                            if ($_SESSION) {
                                $_SESSION['entity']['logo_url'] = $logo_name;
                            }
                        } else {
                            rollback($con);
                            autoCommit($con, true);
                            $message['info'] = 'FAILURE';
                        }
                    } else {
                        rollback($con);
                        autoCommit($con, true);
                        $message['info'] = 'ERROR_OCCURRED';
                    }
                } else {
                    $message['info'] = 'UPLOAD_ERROR';
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    autoCommit($con, true);

    disconnectConnection($con);
    return $message;
}
