<?php

function upload_insurer_subscribers($insurerid) {
    require_once('../php/form_validate.php');
    $con = makeConnection_insurer();


    if (!$con) {
        return ['status' => false, 'err' => 'DATABASE_CONNECT_ERROR'];
    }
    if ($_SERVER['REQUEST_METHOD'] != 'POST') {
        return ['status' => false, 'err' => 'WRONG_REQUEST_TYPE'];
    }


    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    if (!$_POST) {
        return ['status' => false, 'err' => 'INVALID_DETAILS', 'desc' => 'Check the payload sent'];
    }

    $rowid = $_POST['rowid'];
    $typeid = $_POST['typeid'];
    $remarks = $_POST['remarks'];
    $list = $_FILES['list'];
    $validationResult = $form_validate([
        'rowid' => 'required|int',
        'typeid' => 'required|int',
        'remarks' => 'string',
        'list' => 'filerequired|filemimetypes:text/csv,text/plain,application/csv,application/vnd.ms-excel,text/x-csv,text/comma-separated-values,application/csv,application/excel,application/vnd.msexcel,text/anytext'
            ], [
        'rowid' => $rowid,
        'typeid' => $typeid,
        'remarks' => $remarks,
        'list' => $list
    ]);

    if (!empty($validationResult)) {
        return ['status' => false, 'err' => 'VALIDATION_ERROR', 'desc' => $validationResult];
    }
    if (!($list["tmp_name"])) {
        return ['status' => false, 'err' => 'FILE_ERROR'];
    }


    $type = $typeid == 1 ? 'health' : 'general';
    $uploaddir = __DIR__ . "/../../generated_users_res/$type/";
    $list_name = runSimpleFetchQuery($con, ['list_name'], 'insurer_gen_users', ['id'], ['='], [$rowid], '', '', 1)['result'];
    $list_name = $list_name . '.' . pathinfo($list['name'], PATHINFO_EXTENSION);
    $uploadfile = $uploaddir . $list_name;

    if (file_exists($uploadfile)) {
        unlink($uploadfile);
    }

    if (!move_uploaded_file($list["tmp_name"], $uploadfile)) {
        return ['status' => false, 'err' => 'UPLOAD_ERROR'];
    }

    autoCommit($con, false);
    $flag = true;
    $date = (new DateTime())->format(DateTime::ISO8601);
    $response = null;

    do {
        $res = runSimpleInsertQuery($con, 'insurer_log', ['action', 'date', 'insurer_id'], ["'UPLOAD_SUBSCRIBERS'", "'$date'", $insurerid]);
        if ($res['err']['code']) {
            if ($flag) {
                $flag = false;
                break;
            }
        }
        $res = runSimpleUpdateQuery($con, "insurer_gen_users", ['status', 'date_resolved', 'comments', 'queued'], ["'TRUE'", "'$date'", "'$remarks'", "'TRUE'"], ['id'], ['='], [$rowid]);
        if ($res['err']['code']) {
            if ($flag) {
                $flag = false;
            }
        }
    } while (false);

    if ($flag) {
        commit($con);
        $response = ['status' => true];
    } else {
        rollback($con);
        $response = ['status' => false, 'err' => 'ERROR_OCCURRED'];
    }


    autoCommit($con, true);
    disconnectConnection($con);
    return $response;
}
