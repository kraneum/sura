<?php

function upload_pension_documents($id) {
    require_once('../php/form_validate.php');
    require_once('../php/file_extras.php');
    $con = makeConnection();
    autoCommit($con, false);

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $docs = reArrayFiles($_FILES['docs']);
            $docsCount = count($docs);

            if (!$docsCount) {
                $response['status'] = false;
                $response['info'] = 'NO_FILES';
            }

            $validationResult = null;
            $valid = true;

//            print_r($docs);

            for ($i = 0; $i < $docsCount; $i++) {
                if (!$docs[$i]["tmp_name"]) {
                    $valid = false;
                    break;
                }
                if (!empty($validationResult)) {
                    break;
                }
                $validationResult = $form_validate([
                    'docs' => 'filerequired|filemimetypes:image/jpeg,image/png,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                        ], [
                    'docs' => $docs[$i]
                ]);
            }

            if (!$valid) {
                $response['status'] = false;
                $response['info'] = 'FILE_ERROR';
            } else if (!empty($validationResult)) {
                $response['status'] = false;
                $response['info'] = 'VALIDATION_ERROR';
                $response['errVal'] = $validationResult;
            } else {
                if (!file_exists('../pension_docs/docs_id_' . $id)) {
                    mkdir('../pension_docs/docs_id_' . $id, 0777, true);
                }
                $uploaddir = '../pension_docs/docs_id_' . $id . '/';
                $moveFlag = true;
                for ($i = 0; $i < $docsCount; $i++) {

                    $doc_name = $docs[$i]["name"];
                    $uploadfile = $uploaddir . $doc_name;

                    if (file_exists($uploadfile)) {
                        $uploadfile = newName($uploaddir, $doc_name);
                    }

                    if (!move_uploaded_file($docs[$i]["tmp_name"], $uploadfile)) {
                        $moveFlag = false;
                        $response['info'] = 'UPLOAD_ERROR';
                        break;
                    }
                }

                if ($moveFlag) {
                    $flag = true;
                    $date = (new DateTime())->format(DateTime::ISO8601);

                    $res = runSimpleInsertQuery($con, 'pension_log', ['action', 'date', 'pension_id'], ["'UPLOAD_DOCUMENTS'", "'$date'", $id]);
                    if ($res['err']['code']) {
                        if ($flag) {
                            $flag = false;
                        }
                    }
                    $res = runSimpleUpdateQuery($con, "pension", ['validated'], ["'PENDING'"], ['id'], ['='], [$id]);
                    if ($res['err']['code']) {
                        if ($flag) {
                            $flag = false;
                        }
                    }

                    if ($flag) {
                        commit($con);
                        $_SESSION['entity']['validated'] = 'PENDING';
                        $message['info'] = 'SUCCESS';
                    } else {
                        rollback($con);
                        $message['info'] = 'ERROR_OCCURRED';
                    }
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    autoCommit($con, true);
    disconnectConnection($con);
    return $message;
}

