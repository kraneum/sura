<?php

function upload_profile_pic_user($id) {
    require_once('../php/form_validate.php');
    $con = makeConnection_user();

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $pic = $_FILES['pic'];
            $validationResult = $form_validate([
                'pic' => 'filerequired|filemaxmegabytes:4|filemimetypes:image/jpeg,image/png|imagedimensions:min_width=100,min_height=100'
                    ], ['pic' => $pic]);

            if (!empty($validationResult)) {
                $message['info'] = 'VALIDATION_ERROR';
                $message['errVal'] = $validationResult;
            } else if (!getimagesize($pic["tmp_name"])) {
                $message['info'] = 'FILE_ERROR';
            } else {
                $uploaddir = '../img/users/';
                $pic_name = 'pic_id_' . $id . '.' . pathinfo(basename($pic["name"]), PATHINFO_EXTENSION);
                $uploadfile = $uploaddir . $pic_name;

                if (file_exists($uploadfile)) {
                    unlink($uploadfile);
                }

                if (move_uploaded_file($pic["tmp_name"], $uploadfile)) {
                    $flag = true;
                    $date = (new DateTime())->format(DateTime::ISO8601);

                    $res = runSimpleInsertQuery($con, 'user_log', ['action', 'date', 'user_id'], ["'CHANGE_PROFILE_PIC'", "'$date'", $id]);
                    if ($res['err']['code']) {
                        if ($flag) {
                            $flag = false;
                        }
                    }
                    $res = runSimpleUpdateQuery($con, "users", ['profile_pic'], ["'$pic_name'"], ['id'], ['='], [$id]);
                    if ($res['err']['code']) {
                        if ($flag) {
                            $flag = false;
                        }
                    }

                    if ($flag) {
                        if (!$res['err']['code']) {
                            commit($con);
                            autoCommit($con, true);
                            $message['info'] = 'SUCCESS';
                            $message['path'] = $pic_name;
                            if ($_SESSION) {
                                $_SESSION['entity']['profile_pic'] = $pic_name;
                            }
                        } else {
                            rollback($con);
                            autoCommit($con, true);
                            $message['info'] = 'FAILURE';
                        }
                    } else {
                        rollback($con);
                        autoCommit($con, true);
                        $message['info'] = 'ERROR_OCCURRED';
                    }
                } else {
                    $message['info'] = 'UPLOAD_ERROR';
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'WRONG_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    autoCommit($con, true);

    disconnectConnection($con);
    return $message;
}
