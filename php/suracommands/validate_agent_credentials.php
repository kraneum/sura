<?php

function validate_agent_credentials() {
    $con = makeConnection();
    $error = false;
    $response = ['status' => true, 'err' => null];

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }

            if (empty($_POST['id'])) {
                return ['status' => false, 'err' => 'NO_EMAIL'];
            }
            if (empty($_POST['password'])) {
                return ['status' => false, 'err' => 'NO_PASSWORD'];
            }

// prevent sql injections

            $id = filter_var(trim($_POST['id']));
            $login_type = ctype_digit($id) ? 'phone' : 'email';

            if ($login_type == 'email') {
                $id = filter_var($id, FILTER_SANITIZE_EMAIL);
                $id = filter_var($id, FILTER_VALIDATE_EMAIL);
            } else {
                $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
            }


            $password = filter_var(trim($_POST['password']), FILTER_SANITIZE_STRING);
//////////////////            

            if (!empty($id) && !empty($password)) {

                $queryResult = null;

                if ($login_type == 'email') {
                    $queryResult = runSimpleFetchQuery($con, ['id', 'f_name', 'm_name', 'l_name', 'gender', 'dob', 'phone', 'email', 'date_joined', 'last_login', 'profile_pic', 'points', 'ref_id', 'approved'], 'agent', ['email', 'password'], ['=', '='], ["'$id'", "'$password'"], '', '', 1)['result'];
                } else {
                    $queryResult = runSimpleFetchQuery($con, ['id', 'f_name', 'm_name', 'l_name', 'gender', 'dob', 'phone', 'email', 'date_joined', 'last_login', 'profile_pic', 'points', 'ref_id', 'approved'], 'agent', ['phone', 'password'], ['=', '=',], ["'$id'", "'$password'"], '', '', 1)['result'];
                }


                if (empty($queryResult)) {
                    $error = false;
                    $response['status'] = false;
                    $response['err'] = 'INVALID';
                } else {
                    if (isset($_SESSION)) {
                        session_destroy();
                    }
                    session_start();
                    if ($queryResult[0]['approved'] == 'FALSE') {
                        $_SESSION['entity_type'] = 'AGENT';
                        $response['err'] = 'UNAPPROVED';
                    }
                    $_SESSION['entity_type'] = 'AGENT';
                    $_SESSION['entity'] = $queryResult[0];
                }

                disconnectConnection($con);
            }
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    return $response;
}
