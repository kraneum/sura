<?php

function validate_cc_credentials() {
    $con = makeConnection();
    $error = false;
    $response = [];

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['err' => 'INVALID_DETAILS'];
            }

            if (empty($_POST['username']) || empty($_POST['password'])) {
                return ['err' => 'EMPTY_VALUE'];
            }

            $username = $_POST['username'];
            $password = $_POST['password'];
            /*             * *************************************************************** */

            if (!$error) {
                $queryResult = null;

                $queryResult = runSimpleFetchQuery($con, ['id'], 'cc_dept', ['username', 'password'], ['=', '=',], ["'$username'", "'$password'"], '', '', 1)['result'];

                if (empty($queryResult)) {
                    $response['err'] = 'INVALID';
                } else {
                    if (isset($_SESSION)) {
                        session_destroy();
                    }
                    session_start();
                    $_SESSION['id'] = $queryResult;
                    $response['status'] = true;
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'INVALID_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    disconnectConnection($con);
    return $response;
}
