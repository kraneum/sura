<?php

function validate_insurer_credentials() {
    $con = makeConnection();
    $error = false;
    $response = ['status' => true, 'err' => null];

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }
            if (empty($_POST['email'])) {
                return ['status' => false, 'err' => 'NO_EMAIL'];
            }
            if (empty($_POST['password'])) {
                return ['status' => false, 'err' => 'NO_PASSWORD'];
            }

// prevent sql injections

            $id = filter_var(trim($_POST['email']));
            $id = filter_var($id, FILTER_SANITIZE_EMAIL);
            $id = filter_var($id, FILTER_VALIDATE_EMAIL);

            $password = filter_var(trim($_POST['password']), FILTER_SANITIZE_STRING);
//////////////////            
// if there's no error, continue to login
            if (!empty($id) && !empty($password)) {
                $isValid = false;

                $queryResult = runSimpleFetchQuery($con, ['id', 'name', 'password', 'write_up', 'date_joined', 'email', 'phone', 'account_no', 'insurance_type', 'last_login', 'logo_url', 'validated', 'approved', 'name_change', 'sms_wallet', 'redeemable_points'], 'insurer', ['email'], ['='], ["'$id'"], '', '', 1)['result'];
                !empty($queryResult) && $isValid = password_verify($password, $queryResult[0]['password']);

                if (!$isValid) {
                    $error = false;
                    $response['status'] = false;
                    $response['err'] = 'INVALID';
                } else {
                    unset($queryResult[0]['password']);
                    $lead_ids = runSimpleFetchQuery($con, ['health_insurer_id', 'general_insurer_id'], 'insurer_leads', ['id'], ['='], [1], '', '', 1)['result'];
                    if (isset($_SESSION)) {
                        session_destroy();
                    }
                    session_start();
                    if ($queryResult[0]['approved'] == 'FALSE') {
                        $_SESSION['entity_type'] = 'INSURER';
                        $_SESSION['verify'] = 'TRUE';
                        $response['err'] = 'UNAPPROVED';
                    }
                    $_SESSION['entity'] = $queryResult[0];
                    $_SESSION['entity_type'] = 'INSURER';
                    
                    if ($queryResult[0]['insurance_type'] == 1) {
                        $_SESSION['entity']['is_lead'] = $lead_ids[0]['health_insurer_id'] == $queryResult[0]['id'];
                    } else {
                        $_SESSION['entity']['is_lead'] = $lead_ids[0]['general_insurer_id'] == $queryResult[0]['id'];
                    }
                    
                }

                disconnectConnection($con);
            }
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    return $response;
}
