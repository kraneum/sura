<?php

function validate_pension_credentials() {
    $con = makeConnection();
    $error = false;
    $response = ['status' => true, 'err' => null];

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                disconnectConnection($con);
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }
            if (empty($_POST['email'])) {
                disconnectConnection($con);
                return ['status' => false, 'err' => 'NO_EMAIL'];
            }
            if (empty($_POST['password'])) {
                disconnectConnection($con);
                return ['status' => false, 'err' => 'NO_PASSWORD'];
            }

// prevent sql injections

            $id = filter_var(trim($_POST['email']));
            $id = filter_var($id, FILTER_SANITIZE_EMAIL);
            $id = filter_var($id, FILTER_VALIDATE_EMAIL);

            $password = filter_var(trim($_POST['password']), FILTER_SANITIZE_STRING);
//////////////////            
// if there's no error, continue to login
            if (!empty($id) && !empty($password)) {

                $queryResult = runSimpleFetchQuery($con, ['id', 'name', 'write_up', 'date_joined', 'email', 'phone', 'account_no', 'last_login', 'logo_url', 'validated', 'approved', 'name_change', 'redeemable_points'], 'pension', ['email', 'password'], ['=', '='], ["'$id'", "'$password'"], '', '', 1)['result'];

                if (empty($queryResult)) {
                    $error = false;
                    $response['status'] = false;
                    $response['err'] = 'INVALID';
                } else {
                    if (isset($_SESSION)) {
                        session_destroy();
                    }
                    session_start();
                    if ($queryResult[0]['approved'] == 'FALSE') {
                        $_SESSION['entity_type'] = 'PENSION';
                        $_SESSION['verify'] = 'TRUE';
                        $response['err'] = 'UNAPPROVED';
                    }
                    $_SESSION['entity'] = $queryResult[0];
                    $_SESSION['entity_type'] = 'PENSION';
                }

                disconnectConnection($con);
            }
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    return $response;
}
