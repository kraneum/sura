<?php

function validate_user_credentials() {
    $con = makeConnection_user();
    $error = false;
    $response = ['status' => true, 'err' => null];

    if ($con) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if (!$_POST) {
                return ['status' => false, 'err' => 'INVALID_DETAILS'];
            }

            if (empty($_POST['id'])) {
                return ['status' => false, 'err' => 'NO_EMAIL'];
            }
            if (empty($_POST['password'])) {
                return ['status' => false, 'err' => 'NO_PASSWORD'];
            }

// prevent sql injections

            $id = ($_POST['id']);
            $login_type = ctype_digit($id) ? 'phone' : 'email';

            if ($login_type == 'email') {
                $id = filter_var($id, FILTER_SANITIZE_EMAIL);
                $id = filter_var($id, FILTER_VALIDATE_EMAIL);
            } else {
                $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
            }


            $password = filter_var(trim($_POST['password']), FILTER_SANITIZE_STRING);
//////////////////            

            if (!empty($id) && !empty($password)) {
                $queryResult = null;
                $isValid = false;

                $columns = ['id', 'f_name', 'm_name', 'password', 'l_name', 'gender', 'dob', 'phone', 'email', 'address', 'local_gov', 'date_joined', 'last_login', 'profile_pic', 'health_point_balance', 'general_point_balance', 'approved', 'health_insurer', 'next_of_kin', 'kin_phone', 'kin_relationship'];

                if ($login_type == 'email') {
                    $queryResult = runSimpleFetchQuery($con, $columns, 'users', ['email'], ['='], ["'$id'"], '', '', 1)['result'];
                    !empty($queryResult) && $isValid = password_verify($password, $queryResult[0]['password']);
                } else {
                    $queryResult = runSimpleFetchQuery($con, $columns, 'users', ['phone'], ['='], ["'$id'"], '', '', 1)['result'];
                    !empty($queryResult) && $isValid = password_verify($password, $queryResult[0]['password']);
                }


                if (!$isValid) {
                    $error = false;
                    $response['status'] = false;
                    $response['err'] = 'INVALID';
                } else {
                    unset($queryResult[0]['password']);
                    $queryResult[0]['referral_id'] = runSimpleFetchQuery($con, 'referral_id', 'referees', ['entity', 'entity_id'], ['=', '='], ["'USER'", "'" . $queryResult[0]['id'] . "'"], '', '', 1)['result'];
                    if (isset($_SESSION)) {
                        session_destroy();
                    }
                    session_start();
                    if ($queryResult[0]['approved'] == 'FALSE') {
                        $_SESSION['entity_type'] = 'USER';
                        $response['err'] = 'UNAPPROVED';
                    }
                    $_SESSION['entity_type'] = 'USER';
                    $_SESSION['entity'] = $queryResult[0];
                }
            }
        } else {
            $response['status'] = false;
            $response['err'] = 'INVALID_REQUEST_TYPE';
        }
    } else {
        $response['status'] = false;
        $response['err'] = 'DATABASE_CONNECT_ERROR';
    }

    disconnectConnection($con);
    return $response;
}
