<?php

function verify_email_2() {
    $con = makeConnection_user();

    $response = ['status' => true, 'msg' => null];

    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    $entity = $_POST['entity'];
    $email = $_POST['email'];

    $email = filter_var($email, FILTER_VALIDATE_EMAIL);

    if (!$email) {
        $response['status'] = false;
        $response['err'] = 'INVALID_EMAIL';
        $response['msg'] = 'The email entered is not an actual email.';
    } else {
        $table = null;
        
        switch ($entity)  {
            case 'user':
                $table = 'users';
                break;
            case 'merchant':
                $table = 'merchants';
                break;
            default :
                $table = $entity;
        }

        $res = runSimpleFetchQuery($con, ['id', 'security_question'], $table, ['email'], ['='], ["'$email'"], '', '', 1)['result'];
//        echo print_r($res);
        if (empty($res)) {
            $response['status'] = false;
            $response['err'] = 'EMAIL_NOT_RESOLVED';
            $response['msg'] = 'The email is not found on the platform.';
        } else {
            $response['status'] = true;
            $response['entity'] = $entity;
            $response['email'] = $email;
            $response['sec_que'] = $res[0]['security_question'];
        }
    }

    disconnectConnection($con);
    
    return $response;
}
