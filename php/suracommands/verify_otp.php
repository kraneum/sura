<?php

function verify_otp($id) {
    $con = makeConnection_user();
    autoCommit($con, false);
    $flag = true;
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    if (!$_POST) {
        return ['status' => false, 'err' => 'INVALID_DETAILS'];
    }
    $otp = trim($_POST['otp']);
    $role = trim($_POST['role']);
    $table = null;
    if ($role == 'USER') {
        $table = 'users';
    } else if ($role == 'AGENT') {
        $table = 'agent';
    }else if ($role == 'INSURER') {
        $table = 'insurer';
    }else if ($role == 'MERCHANT') {
        $table = 'merchants';
    }

    $res = runSimpleFetchQuery($con, ['id', 'role', 'entity_id', 'verified'], 'verify_entity', ['entity_id', 'link', 'role'], ['=', '=', '='], ["'$id'", "'$otp'", "'$role'"], '', '', 1);

    if (!$res['err']['code']) {
        $res = $res['result'];
        if (count($res) > 0) {
            $verified = $res[0]['verified'];
            $entityid = $res[0]['entity_id'];
            if ($verified == 'FALSE') {
                $result = runSimpleUpdateQuery($con, 'verify_entity', ['verified'], ["'TRUE'"], ['entity_id', 'link'], ['=', '='], [$entityid, "'$otp'"]);
                if ($result['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }
                $result = $result = runSimpleUpdateQuery($con, $table, ['approved'], ["'TRUE'"], ['id'], ['='], [$entityid]);
                if ($result['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }

                if ($flag) {
                    commit($con);
                } else {
                    rollback($con);
                }
                autoCommit($con, true);
                session_unset();
                $message['status'] = 'SUCCESS';
            } else {
                session_unset();
                $message['status'] = 'ALREADY_VERIFIED';
            }
        } else {
            $message['status'] = 'INVALID';
            $message['error'] = $res;
        }
    } else {
        $message['status'] = 'FAILURE';
    }

    return $message;
}
