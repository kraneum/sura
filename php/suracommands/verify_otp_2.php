<?php

function verify_otp_2() {
    $con = makeConnection_user();
    autoCommit($con, false);
    $flag = true;
    $response = ['status' => true, 'msg' => null];

    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    if (!$_POST) {
        echo json_encode(['status' => false, 'err' => 'INVALID_DETAILS']);
    }

    $entity = $_POST['entity'];
    $otp = $_POST['otp'];
    $id = $_POST['id'];


    $res = runSimpleFetchQuery($con, ['id', 'role', 'entity_id', 'verified'], 'verify_entity', ['entity_id', 'link', 'role'], ['=', '=', '='], [$id, "'$otp'", "'$entity'"], '', '', 1);
    if (!$res['err']['code']) {
        $res = $res['result'];
        if (count($res) > 0) {
            $verified = $res[0]['verified'];
            $entityid = $res[0]['entity_id'];
            $role = $res[0]['role'];
            if ($verified == 'FALSE') {
                $result = runSimpleUpdateQuery($con, 'verify_entity', ['verified'], ["'TRUE'"], ['entity_id', 'link'], ['=', '='], [$entityid, "'$otp'"]);
                if ($result['err']['code']) {
                    if ($flag) {
                        $flag = false;
                    }
                }
                
                if ($flag) {
                    commit($con);
                    $response['status'] = TRUE;
                    $response['entity'] = $entity;
                    $response['id'] = $id;
                    
                } else {
                    rollback($con);
                }
                autoCommit($con, true);
                
            } else {
                $response['status'] = FALSE;
                $response['err'] = 'INVALID_LINK';
            }
        } else {
            $response['status'] = FALSE;
            $response['err'] = 'INVALID_LINK';
        }
    } else {
        $response['status'] = FALSE;
        $response['err'] = 'INTERNAL_ERROR';
    }

    return $response;
}
