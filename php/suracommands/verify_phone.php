<?php

function verify_phone() {
    $con = makeConnection_user();

    $response = ['status' => true, 'msg' => null];

    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    $entity = $_POST['entity'];
    $phone = $_POST['phone'];

    $table = 'users';

    $res = runSimpleFetchQuery($con, ['id', 'security_question'], $table, ['phone'], ['='], ["'$phone'"], '', '', 1)['result'];

    if (empty($res)) {
        $response['status'] = false;
        $response['err'] = 'PHONE_NOT_RESOLVED';
        $response['msg'] = 'The phone number is not found on the platform.';
    } else {
        $response['status'] = true;
        $response['entity'] = $entity;
        $response['phone'] = $phone;
        $response['sec_que'] = $res[0]['security_question'];
    }


    disconnectConnection($con);

    return $response;
}
