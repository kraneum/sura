<?php

function verify_security_answer() {
    $con = makeConnection_user();

    $response = ['status' => true, 'msg' => null];

    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    $entity = $_POST['entity'];
    $email = $_POST['email'];
    $answer = $_POST['answer'];

    $email = filter_var($email, FILTER_VALIDATE_EMAIL);

    if (!$email) {
        $response['status'] = false;
        $response['err'] = 'INVALID_EMAIL';
        $response['msg'] = 'The email sent is not an actual email.';
    } else {
        $table = null;

        switch ($entity) {
            case 'user':
                $table = 'users';
                break;
            default :
                $table = $entity;
        }

        $res = runSimpleFetchQuery($con, ['id', 'email'], $table, ['email', 'security_answer'], ['=', '='], ["'$email'", "'$answer'"], '', '', 1)['result'];

        if (empty($res)) {
            $response['status'] = false;
            $response['err'] = 'INCORRECT_ANSWER';
            $response['msg'] = 'The security answer provided was incorrect.';
        } else {
            $response['status'] = true;
            $entity_id = $res[0]['id'];
            
            require 'send_mail.php';
            require_once 'verification_keygen.php';
            
            $link = generateLink($entity); // ..in verification_keygen.php
            $fullLink = $link . $entity_id;
            
            $res = runSimpleInsertQuery($con, "verify_entity", ['entity_id', 'link', 'role'], [$entity_id, "'$link'", "'". strtoupper($entity)."'"]);
            
            $subject = 'Reset Password';
            $message = '<html><body style="padding: 0 20%; border:1px thin grey; font-size:14px;'
                    . 'font-family: Arial, Helvetica, sans-serif"><div><h3>Hi, </h3>'
                    . 'Please click the password below to reset your password<br>'
                    . '<div style="text-align:center; margin:30px 0;">'
                    . '<a style="padding: 15px; background-color: #12a8e9; font-size: 14px; border-radius: 7px; color: white; font-weight: bold;text-decoration: none" '
                    . 'href="http://localhost/iinsurance/php/resetpassword.php?i=' . $fullLink . '">Reset Password</a></div>'
                    . '<br><div>If button doesn\'t work, copy and paste the link in your browser: http://localhost/iinsurance/php/resetpassword.php?i=' . $fullLink
                    . '<br><br><div style="font-size: .8em">If you’ve received this email by mistake, simply ignore it.'
                    . '<br><br>Thanks for your loyalty.</div></div></html></body>';
                    
            send_mail($email, $subject, $message);
        }
    }

    disconnectConnection($con);

    return $response;
}
