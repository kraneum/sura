<?php

function verify_security_answer_2() {
    $con = makeConnection_user();

    $response = ['status' => true, 'msg' => null];

    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    $entity = $_POST['entity'];
    $phone = $_POST['phone'];
    $answer = $_POST['answer'];


    $table = 'users';

    $res = runSimpleFetchQuery($con, ['id', 'f_name', 'phone'], $table, ['phone', 'security_answer'], ['=', '='], ["'$phone'", "'$answer'"], '', '', 1)['result'];


    if (empty($res)) {
        $response['status'] = false;
        $response['err'] = 'INCORRECT_ANSWER';
        $response['msg'] = 'The security answer provided was incorrect.';
    } else {
        $entity_id = $res[0]['id'];
        $fname = $res[0]['f_name'];
        $phone = $res[0]['phone'];
        
        $response['status'] = true;
        $response['id'] = $entity_id;
        $response['entity'] = $entity;
        
        require 'send_sms.php';
        require_once 'verification_keygen.php';

        $otp = generateOTP(); // ..in verification_keygen.php

        $res = runSimpleInsertQuery($con, "verify_entity", ['entity_id', 'link', 'role'], [$entity_id, "'$otp'", "'" . strtoupper($entity) . "'"]);

        send_sms($fname, 'Your OTP verification number is ' . $otp, "'$phone'");
    }


    disconnectConnection($con);

    return $response;
}
