<?php

class UserPermissions {

    const permissions = [
        //use a neutral value for admin/super user so we wont need to update permissions later
        'SUPER' => -1,
        'DUMMY' => 0,
        'VIEW' => 1,
        'CREATE' => 2,
        'UPDATE' => 4,
        'DELETE' => 8
    ];

    public static function hasPermission($permission, $userPermission) {
        $selfPermissions = self::permissions;
        
        return self::isValidPermission($permission) && (($selfPermissions['SUPER'] === $userPermission) || ($permission === 'DUMMY') || (($permission !== 'SUPER') && ($selfPermissions[$permission] & $userPermission)));
    }

    public static function grantUserPermission($permission, $userPermission) {
        //Warning!!! Never grant superuser permissions here!, use grantSuperuserPermission instead
        return self::isValidPermission($permission) ? (self::permissions[$permission] | $userPermission) :$userPermission;
    }
    
    public static function grantSuperuserPermission() {
        return self::permissions['SUPER'];
    }
    
    public static function grantDummyUserPermission() {
        return self::permissions['DUMMY'];
    }
    
    public static function revokeUserPermission($permission, $userPermission) {
        //Warning: No need to revoke superuser permissions so i didnt implement such a function, but never the less, dnt try to revoke a superuser's priviledges with this function
        return self::isValidPermission($permission) ? ((self::permissions[$permission] & $userPermission) ^ $userPermission) :$userPermission;
    }
    
    public static function isSuperuserPermission($userPermission) {
        return (self::permissions['SUPER'] === $userPermission);
    }
    public static function getAllPermissions() {
        $selfPermissions = self::permissions;
        
        unset($selfPermissions['DUMMY']);
        
        return $selfPermissions;
    }

    
    private static function isValidPermission($permission) {
        return array_key_exists($permission, self::permissions);
    }

}
