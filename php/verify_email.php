<?php

require './sura_config.php';
require './sura_functions.php';

$con = makeConnection_user();
autoCommit($con, false);
$flag = true;
$sentLink = htmlspecialchars(strip_tags(trim($_GET['i'])));

$link = substr($sentLink, 0, 30);
$id = substr($sentLink, 30);
$entity = getEntity($link[0]);
$res = runSimpleFetchQuery($con, ['id', 'role', 'entity_id', 'verified'], 'verify_entity', ['entity_id', 'link', 'role'], ['=', '=', '='], [$id, "'$link'", "'$entity'"], '', '', 1);
if (!$res['err']['code']) {
    $res = $res['result'];
    if (count($res) > 0) {
        $verified = $res[0]['verified'];
        $entityid = $res[0]['entity_id'];
        $role = $res[0]['role'];
        if ($verified == 'FALSE') {
            $result = runSimpleUpdateQuery($con, 'verify_entity', ['verified'], ["'TRUE'"], ['entity_id', 'link'], ['=', '='], [$entityid, "'$link'"]);
            if ($result['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }
            $result = approvedEntity($entityid, $role, $verified);
            if ($result['err']['code']) {
                if ($flag) {
                    $flag = false;
                }
            }

            if ($flag) {
                commit($con);
            } else {
                rollback($con);
            }
            autoCommit($con, true);
            redirect($role, $verified);
        } else {
            redirect($role, $verified);
        }
    } else {
        echo 'Invalid Link';
        die();
    }
} else {
    echo 'Error occurred';
    die();
}

function approvedEntity($entityid, $role, $verified) {
    global $con;
    if ($role === 'USER' && $verified == 'FALSE') {
        $result = runSimpleUpdateQuery($con, 'users', ['approved'], ["'TRUE'"], ['id'], ['='], [$entityid]);
    } else if ($role === 'INSURER' && $verified == 'FALSE') {
        $result = runSimpleUpdateQuery($con, 'insurer', ['approved'], ["'TRUE'"], ['id'], ['='], [$entityid]);
    } else if ($role === 'MERCHANT' && $verified == 'FALSE') {
        
    } else if ($role === 'ADMIN' && $verified == 'FALSE') {
        
    } else if ($role === 'AGENT' && $verified == 'FALSE') {
        $result = runSimpleUpdateQuery($con, 'agent', ['approved'], ["'TRUE'"], ['id'], ['='], [$entityid]);
    } else if ($role === 'PENSION' && $verified == 'FALSE') {
        $result = runSimpleUpdateQuery($con, 'pension', ['approved'], ["'TRUE'"], ['id'], ['='], [$entityid]);
    }
    return $result;
}

function redirect($role, $verified) {
    if ($role === 'USER') {
        if ($verified == 'FALSE') {
            header('Location: verisuccess.php?entity=user');
        } else {
            header('Location: verisuccess.php?entity=user&already');
        }
    } else if ($role === 'INSURER') {
        if ($verified == 'FALSE') {
            header('Location: verisuccess.php?entity=insurer');
        } else {
            header('Location: verisuccess.php?entity=insurer&already');
        }
    } else if ($role === 'MERCHANT') {
        if ($verified == 'FALSE') {
            header('Location: verisuccess.php?entity=merchant');
        } else {
            header('Location: verisuccess.php?entity=merchant&already');
        }
    } else if ($role === 'ADMIN') {
        if ($verified == 'FALSE') {
            header('Location: verisuccess.php?entity=admin');
        } else {
            header('Location: verisuccess.php?entity=admin&already');
        }
    } else if ($role === 'AGENT') {
        if ($verified == 'FALSE') {
            header('Location: verisuccess.php?entity=agent');
        } else {
            header('Location: verisuccess.php?entity=agent&already');
        }
    } else if ($role === 'PENSION') {
        if ($verified == 'FALSE') {
            header('Location: verisuccess.php?entity=pension');
        } else {
            header('Location: verisuccess.php?entity=pension&already');
        }
    }
}

function getEntity($e) {
    if ($e == 'u') {
        return 'USER';
    } else if ($e == 'a') {
        return 'AGENT';
    } else if ($e == 'i') {
        return 'INSURER';
    } else if ($e == 'p') {
        return 'PENSION';
    }
}
