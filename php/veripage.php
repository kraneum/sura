<?php
if (!isset($_SESSION)) {
    session_start();
}
if (!isset($_SESSION['entity_type'])) {
    exit('Forbidden');
}

$entity_type = $_SESSION['entity_type'];
?>

<!DOCTYPE html>

<html>
    <head>
        <title>iInsurance</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="icon" type="image/png" href="../img/favicon.ico">
        <link rel="apple-touch-icon" href="../img/favicon.ico">

        <link href="../css/parsley.css" rel="stylesheet" type="text/css"/>
        <link href="../css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/formstyles.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">
            body {
                padding: 10% 25%;
            }
            form {
                margin: 0 20%;
            }
            #info {
                margin: 20px 0;
            }
        </style>
    </head>
    <body>

        <header><img class="img-responsive center-block" src="../img/logos/uic2.png" alt=""/></header>
        <div id="info"></div>
        <div id="content">
            <form id ="verificationForm" class="form-horizontal" method="post">
                <div class="form-group">
                    <input type="hidden" name ="entity" value="<?php echo $entity_type ?>"/>
                    <label for="otp">Enter OTP * </label>
                    <input class="form-control" type="text" size="6" id="otp" name="otp" placeholder="Enter OTP" data-parsley-required data-parsley-type="digits" data-parsley-length="[6, 6]" data-parsley-length-message="This value must be 6 digits"/>
                </div>

                <div class="form-group">
                    <button id="formVerify" type="submit" class="btn btn-success formButtons"  name="btn-verify"><i class="fa fa-check btn-icon-left"></i> VERIFY</button>
                </div>
            </form>
        </div>

        <script src="../js/jquery-3.1.1.js" type="text/javascript"></script>
        <script src="../js/parsley.min.js" type="text/javascript"></script>
        <script src="../js/bootstrap.js" type="text/javascript"></script>

        <script type="text/javascript">
            $('#verificationForm').parsley();

            $('#verificationForm').submit(function (e) {
                $('#formVerify').html('<i class="fa fa-spinner fa-pulse fa-fw"></i> VERIFYING ...');
                var formData = {
                    'otp': $('input[name=otp]').val(),
                    'role': $('input[name=entity]').val()
                };
                $.post('./suracommands.php?command=verify_otp&web', formData, function (data) {
                    data = JSON.parse(data);

                    $('#formVerify').html('<i class="fa fa-check btn-icon-left"></i> VERIFY');
                    if (data['status'] === 'SUCCESS') {
                        window.location.href = "verisuccess.php?entity=" + $('input[name=entity]').val().toString().toLowerCase();
                    } else if (data['status'] === 'INVALID') {
                        $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                '<strong>Verification Failure</strong></div>');
                        $('#otp').addClass('parsley-error');

                    } else if (data['status'] === 'ALREADY_VERIFIED') {
                        $('#info').html('<div class="alert alert-dismissible alert-warning text-center">' +
                                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                '<strong>Already verified.</strong> Redirecting to login shortly..</div>');
                        $('#content').fadeOut('slow');
                        setTimeout(function () {
                            window.location.href = "login.php";
                        }, 2000);
                    } else if (data['status'] === 'FAILURE') {
                        $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                '<strong>Internal error occurred.</strong> Please try again later</div>');
                    }
                });

                e.preventDefault();
            });
        </script>
    </body>
</html>
