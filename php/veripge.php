<?php
$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

$entity_type = $_POST['entity'];
$id = $_POST['id'];
?>


<form id ="forgotPasswordForm" class="form-horizontal" method="post">
    <div class="form-group">
        <input type="hidden" name ="entity" value="<?php echo $entity_type ?>"/>
        <input type="hidden" name ="id" value="<?php echo $id ?>"/>
        <label for="otp">Enter OTP * </label>
        <input class="form-control" type="text" size="6" id="otp" name="otp" placeholder="Enter OTP" data-parsley-required data-parsley-type="digits" data-parsley-length="[6, 6]" data-parsley-length-message="This value must be 6 digits"/>
    </div>

    <div class="form-group">
        <button id="formVerify" type="submit" class="btn btn-success formButtons"  name="btn-verify"><i class="fa fa-check btn-icon-left"></i> VERIFY</button>
    </div>
</form>


<script type="text/javascript">
    $('#forgotPasswordForm').parsley();

    $('#forgotPasswordForm').submit(function (e) {
        $('#formVerify').html('<i class="fa fa-spinner fa-pulse fa-fw"></i> VERIFYING ...');
        var formData = {
            'otp': $('input[name=otp]').val(),
            'entity': $('input[name=entity]').val(),
            'id': $('input[name=id]').val()
        };
        $.post('./suracommands.php?command=verify_otp_2', formData, function (data) {
            data = JSON.parse(data);

            $('#formVerify').html('<i class="fa fa-check btn-icon-left"></i> VERIFY');
            if (data['status']) {
                $('#content').empty().load('resetpasswrd.php', {"entity": data['entity'], "id": data['id']});
            } else {
                if (data['err'] === 'INVALID_LINK') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Verification Failure</strong></div>');
                    $('#otp').addClass('parsley-error');

                } else if (data['err'] === 'INTERNAL_ERROR') {
                    $('#info').html('<div class="alert alert-dismissible alert-danger text-center">' +
                            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>Internal error occurred.</strong> Please try again later</div>');
                }
            }
        });

        e.preventDefault();
    });
</script>