<!DOCTYPE html>

<html>
    <head>
        <title>iInsurance</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="../img/favicon.png" />
        <link rel="icon" type="image/png" href="../img/favicon.png">
        <link rel="apple-touch-icon" href="../img/favicon.png">

        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">
            body {
                padding: 0 25%;
            }
            form {
                margin: 0 20%;
            }
            #info {
                margin: 20px 0;
            }
            #content > img {
                margin-bottom: 5%;
                opacity: .5;
            }

            header > img {
                margin: 5% 0 7%;
            }
            h3 {
                margin-bottom: 20px
            }
        </style>
    </head>
    <body>

        <header><img width="250" height="250" class="img-responsive center-block" src="../img/logos/uic_full.png" alt=""/></header>
        <div id="content" class="text-center">
            <img src="img/approved.png" alt=""/>
            <h3><?php
                if (isset($_GET['already'])) {
                    echo 'Account already verified';
                } else {
                    echo 'Account verification successful';
                }
                ?>
            </h3> Redirecting shortly...
            <p style="font-style: italic;margin-top: 20px">..thanks for your loyalty</p>
        </div>

        <script type="text/javascript">
            $urlParam = function (name) {
                var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                if (results === null) {
                    return null;
                } else {
                    return results[1] || 0;
                }
            };
            var entity = $urlParam('entity');
            setTimeout(function () {
                if (entity === 'user') {
                    window.location.href = "../login.php";
                } else if (entity === 'insurer') {
                    window.location.href = "../insurer/login.php";
                } else if (entity === 'admin') {
                    window.location.href = "../admin/login.php";
                } else if (entity === 'merchant') {
                    window.location.href = "../merchant/login.php";
                } else if (entity === 'agent') {
                    window.location.href = "../agent/login.php";
                } else if (entity === 'pension') {
                    window.location.href = "../pension/login.php";
                }
            }, 2000);
        </script>
    </body>
</html>


