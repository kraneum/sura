<?php

if (!isset($_SESSION)) {
    session_start();
}

$id = null;
if (!isset($_SESSION['userid'])) {
    $id = $_SESSION['userid'];
} else if (!isset($_SESSION['insurerid'])){
    $id = $_SESSION['insurerid'];
}

require_once('sura_commands.php');