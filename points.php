<?php
require './header.php';
include_once './_add_points.php';
include_once './_transfer_points.php';
?>

<div id="home-main" class="container">
    <div id="points-main" class="points">
        <div class="row">
            <div class="col-sm-8">
                <h4><strong><i class="fa fa-exchange"></i> View all iPoint transfers</strong></h4>
                <small>&emsp;&emsp;You can redeem your USSD pins as well as purchase/transfer iPoints here</small>
            </div>
            <div class="col-sm-4">
                <span class="pull-right text-bold ">
                    Health iPoints: <?php echo $health_loyalty_points ?> | General iPoints: <?php echo $general_loyalty_points ?>
                </span>
            </div>
        </div>
        
        <div>
            <div class="row">
                <!-- Redeemed Points -->
                <div class="col-sm-6 lead"></div>
                <div class="col-sm-6">
                    <div class="ui-group-buttons pull-right" style="margin-right: 20px;">
                        <a id="pointsButton" type = "button" class = "btn btn-primary btn-sm" data-toggle = "modal" data-target = "#transferModal">
                            Transfer iPoints
                        </a>
                        <div class="or"></div>
                        <a id="pointsButton" type = "button" class = "btn btn-success btn-sm" data-toggle = "modal" data-target = "#pointsModal">
                            Buy/Redeem iPoints
                        </a> 
                    </div>
                </div>

            </div>

            <div class="clearfix"></div>

            <div id ="content" style="margin-top: 20px;">
                <div id="table" style="display: none">
                    <div class="table-responsive">
                        <table class="table">

                            <thead>
                                <tr>
                                    <th class="text-center">Date</th>
                                    <th class="text-center">Merchant</th>
                                    <th class="text-center">Amount in iPoints</th>
                                </tr>
                            </thead>

                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    <div id="pagination" class="center-block"></div>
                </div>
            </div>

        </div>
    </div>

</div>

<?php
include './footer.php';
?>
<script src="js/jquery-3.1.1.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
<script src="js/parsley.min.js" type="text/javascript"></script>
<script src="js/jquery.simplePagination.js" type="text/javascript"></script>
<script src="js/spin.min.js" type="text/javascript"></script>
<script src="js/spinner-config.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
</body>
</html>