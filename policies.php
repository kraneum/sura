<?php
require_once './header.php';
?>
<div id="home-main" class="container">
    <!-- <div id="policies-main" class="policies"> -->
    <h4><strong><i class="fa fa-calendar"></i> View all policies</strong></h4>
    <small>&emsp;&emsp;You can renew them ahead of time by clicking on it</small>
    <div class="row">
        <a href = "addnewpolicy.php" class="btn btn-xs btn-success pull-right" style="margin-bottom: 20px; margin-right: 5%;"><i class="fa fa-plus"></i> Add New</a>
    </div>

    <!-- <div id="content-header">
        <span class="lead" style="line-height:14px;"></span>
    </div> -->
    <!-- <div> -->
    <div class="row container">

        <h4 class="text-success text-center" style="margin-bottom: 2em;"><strong>Active Policies</strong></h4>

        <?php
        $active_policies = getData('get_user_policies_by_id', $id)['active'];

        if (count($active_policies) > 0) {
            for ($i = 0; $i < count($active_policies); $i++) {
                if ($i % 6 == 0 && $i != 0) {
                    echo '</div>';
                    echo '<div class="row container">';
                }

                $signupID = $active_policies[$i]['signup_id'];
                $name = $active_policies[$i]['insurer'];
                $logo = $active_policies[$i]['insurer_logo_url'];
                echo '<div class="col-sm-2 col-md-2 col-lg-2">';
                echo "<a href=\"policydetails.php?signupid=$signupID\">";
                echo '<div class="thumbnail" style="padding: 10px">';
                echo "<img src=\"img/insurers/$logo\" alt=\"...\" class=\"\" style=\"max-height=60px;max-width=100%;width=auto;height=auto;\">";
                echo '<div class="caption text-center">';
                echo "<span>$name</span>";
                echo '</div></div>';
                echo '</a>';
                echo '</div>';
            }
            echo '</div>';
        } else {
            echo '<p style="padding: 50px;" class="text-center text-muted text-italic">No Active Policies Signed up for. '
            . 'Click <a href="addnewpolicy.php">here</a> to sign up</p>';
        }
        ?>
        <h4 class="text-danger text-center" style="margin-bottom: 2em;"><strong>Inactive Policies</strong></h4>

        <?php
        $inactive_policies = getData('get_user_policies_by_id', $id)['inactive'];

        if (count($inactive_policies) > 0) {
            for ($i = 0; $i < count($inactive_policies); $i++) {
                if ($i % 6 == 0 && $i != 0) {
                    echo '</div>';
                    echo '<div class="row container">';
                }

                $signupID = $inactive_policies[$i]['signup_id'];
                $name = $inactive_policies[$i]['insurer'];
                $logo = $inactive_policies[$i]['insurer_logo_url'];
                echo '<div class="col-sm-2 col-md-2 col-lg-2">';
                echo "<a href=\"policydetails.php?signupid=$signupID\">";
                echo '<div class="thumbnail" style="padding: 10px;">';
                echo "<img src=\"img/insurers/$logo\" alt=\"...\" class=\"img-responsive\">";
                echo '<div class="caption text-center">';
                echo "<span>$name</span>";
                echo '</div></div>';
                echo '</a>';
                echo '</div>';
            }
            echo '</div>';
            echo '</div>';
        } else {
            echo '<p style="padding: 50px;" class="text-center text-muted text-italic">No Inactive Policies.';
        }
        ?>


    </div>
    <!--  </div> -->
    <!-- </div> -->
</div>

<?php
include './footer.php';
?>

<script src="js/jquery-3.1.1.js" type="text/javascript"></script>
<script src="js/jquery-ui.js" type="text/javascript"></script>
<script src="js/parsley.min.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
</body>
</html>