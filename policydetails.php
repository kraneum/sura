<?php
if (empty($_GET['signupid'])) {
    header('Location: policies.php');
} else {
    require_once './header.php';
    include_once './_add_points.php';
    $signupID = $_GET['signupid'];
    $signupInfo = getData('get_my_signup_by_id', $id, $signupID);
    $signup_active = $signupInfo[0]['active'];
    $policy_active = $signupInfo[0]['policy_info'][0]['active'];
    $logo = $signupInfo[0]['insurer'][0]['logo_url'];
    $name = $signupInfo[0]['insurer'][0]['name'];
    $email = $signupInfo[0]['insurer'][0]['email'];
    $phone = $signupInfo[0]['insurer'][0]['phone'];
    $insurance_type = $signupInfo[0]['policy_info'][0]['insurance_type'];
    $accountNo = $signupInfo[0]['insurer'][0]['account_no'];
    $amount = $signupInfo[0]['policy_info'][0]['price'];
    $status = $signup_active == 'TRUE' ? '<span style="color:green">Active</span>' : '<span style="color:red">Inactive</span>';
    $date = null;
    $payment_schedule_info = $signupInfo[0]['payment_schedule'];
    if (count($payment_schedule_info) != 0) {
        $paymentInfo = $signupInfo[0]['payment_schedule'][count($payment_schedule_info) - 1]['date_to_charge'];
        $_date = new DateTime($paymentInfo);
        if ($_date < new DateTime() && $policy_active == 'TRUE' && $signup_active == 'TRUE') {
            $status = '<span style="color:orange">Dormant</span>';
        } else if ($_date < new DateTime() && $policy_active == 'FALSE') {
            $status = '<span style="color:red">Inactive</span>';
        }
        $date = $_date->format('j/n/Y');
        $str = explode('/', $date);
        $date = $str[0] . ' ' . toMonth($str[1]) . ', ' . $str[2];
    }
}
?>
<div id="policy-details-main" class="container policies">
        <h4><strong><i class="fa fa-calendar"></i> Policy details</strong></h4>
        <small>&emsp;&emsp;You can renew and view automated renew dates</small>

        <div id="content-header" style="margin-top: 10px;">
        <img style="display: inline; margin-bottom: 8px;" src="img/insurers/<?php echo $logo; ?>" alt="" class="img-circle img-responsive" width="35px" height="35px"/>
        <span class="lead" style="padding-left: 5px;">
            <strong id='name' data-signupid="<?php echo $signupID ?>"><?php echo$name ?></strong>
        </span>
    </div>

    <div>
        <div class="container" style="padding:25px;">
            <div id="info" ></div>
            <div class="row" >
                <div class="col-sm-3"><strong>Insurance Type</strong><br/><span><?php echo $insurance_type ?></span></div>
                <div class="col-sm-3"><strong>Email</strong><br/><span><?php echo $email ?></span></div>
                <div class="col-sm-3"><strong>Phone</strong><br/><span><?php echo $phone ?></span></div>

                <div class="col-sm-3"><strong>Status</strong><br/><span><?php echo $status ?></span></div>
            </div>
            <div class="clearfix" style="margin-bottom: 2.5%;"></div>
            <div class="row">
                <div class="col-sm-3"><strong>Next Payment Due</strong><br/><span id="next-pay-day"><?php echo $date ?></span></div>
                <div class="col-sm-3"><strong>Payment Amount</strong><br/><?php echo $amount ?> iPoints</div>
                <div class="col-sm-3"><strong>Renewal Method</strong><br/><?php echo $policy_active == 'TRUE' ? '<a href="" data-toggle="modal" data-target="#renewModal">Pay with iPoints</a>' : '<span class="small" style="color: red">This policy is no longer available</span>' ?></div>
                <div class="modal fade" id="renewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4>Confirm Renewal</h4>
                            </div>
                            <div class="modal-body">
                                This renewal cost  <?php echo $amount ?> Loyalty Points. Having read the <a href="#">Terms</a> and <a href="#">Conditions</a>, do you wish to continue?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-rounded box-shadow--2dp" data-dismiss="modal">Cancel</button>
                                <button id="continue"  class="btn btn-success btn-rounded box-shadow--2dp" data-signupid = "<?php echo $signupID ?>">Continue</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix" style="margin-bottom: 2%;"></div>
                <div>
                    <a style="margin-right: 2.5%;" class="btn btn-danger btn-rounded btn-sm pull-right box-shadow--2dp" data-toggle="modal" data-target="#confirm-delete">Delete</a>

                    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4>Confirm Delete</h4>
                                </div>
                                <div class="modal-body">
                                    Deleting this policy will be irreversible. Do you want to continue?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default btn-rounded box-shadow--2dp" data-dismiss="modal">Cancel</button>
                                    <button id="deletePolicy" onclick="deletePolicy(<?php echo $signupID ?>)" class="btn btn-danger btn-ok btn-rounded  box-shadow--2dp">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <span class="lead" style="margin-top: 20px; margin-left: 10px;">Renewals</span>
                <div id ="content">
                    <div id="table" style="display: none">
                        <div class="table-responsive">
                            <table class="table table-striped">

                                <thead>
                                    <tr>
                                        <th class="text-center">Payment Date</th>
                                        <th class="text-center">Amount</th>
                                        <th class="text-center">Ref</th>
                                    </tr>
                                </thead>

                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <div id="pagination" class="center-block"></div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin: 0 1% 5%">
            <a class="pull-left btn-xs btn-default btn-rounded box-shadow--2dp" href="policies.php"><i class="fa fa-arrow-left" ></i> Back to Policies</a>
            <a class="pull-right btn-xs btn-success btn-rounded box-shadow--2dp" href="addnewpolicy.php">Add New Policy <i class="fa fa-arrow-right" ></i></a>
        </div>
    </div>
</div>

<?php
include './footer.php';
?>

<script src="js/jquery-3.1.1.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
<script src="js/jquery.simplePagination.js" type="text/javascript"></script>
<script src="js/spin.min.js" type="text/javascript"></script>
<script src="js/spinner-config.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
</body>
</html>
