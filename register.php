<?php
require './php/curl.php';

$data = getData('get_security_questions');
?>
<!DOCTYPE html>

<html>
    <head>
        <title>iInsurance</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="img/favicon.ico" />
        <link rel="icon" type="image/png" href="img/favicon.ico">
        <link rel="apple-touch-icon" href="img/favicon.ico">

        <link href="css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/parsley.css" rel="stylesheet" type="text/css"/>
        <link href="css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="css/smt-bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/select2.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/formstyles.css" rel="stylesheet" type="text/css"/>
        <link href="css/style_login.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">
            @font-face {
                font-family: 'Avenir';
                src: url('fonts/AvenirLTStd-Book.woff2') format('woff2'), /* Super Modern Browsers */
                    url('fonts/AvenirLTStd-Book.woff') format('woff'), /* Pretty Modern Browsers */
                    url('fonts/AvenirLTStd-Book.ttf') format('truetype'); /* Safari, Android, iOS */
            }
            body {
                font-family: "Avenir", sans-serif;
            }
        </style>


    </head>
    <body>

        <div class="container">

            <div class="form">

                <div class="login-img"><img src="img/logos/uic2.png" class="img-responsive" /></div>
                <div class="alert alert-danger alert-dismissable" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong id="info"></strong>
                </div>

                <form id ="regForm" method="post" action="#">
                    <div class="form-group">
                        <label for="fname" class="label_">First Name * </label>
                        <input id="fname" name="fname" type="text" value="Micheal" placeholder="Enter first name" class="input_"  data-parsley-required/>
                    </div>

                    <div class="form-group" >
                        <label for="lname" class="label_"> Last Name * </label>
                        <input id="lname" name="lname"  type="text"  value="Opara" placeholder="Enter last name" class="input_" data-parsley-required/>
                    </div>

                    <div class="form-group">
                        <label for="phone" class="label_">Phone * </label>
                        <input id="phone" name="phone"  value="09057183476" placeholder="Enter phone number" class="input_" data-parsley-required data-parsley-minlength="11" data-parsley-maxlength="11"  data-parsley-type="digits" data-parsley-minlength-message="This value should be 11 digits" data-parsley-maxlength-message="This value should be 11 digits" data-parsley-type-message="This value should contain digits only"  data-parsley-remote="user_phone_available.php?phone={value}" data-parsley-trigger="keyup" data-parsley-remote-message="Phone number already in use" data-parsley-debounce="500"/>
                    </div>

                    <div class="form-group">
                        <label for="email" class="label_">Email </label>
                        <input id="email" name="email" value="modernbabbage@gmail.com"  type="email" class="input_" placeholder="Enter email"  data-parsley-remote="user_email_available.php?email={value}" data-parsley-trigger="keyup" data-parsley-remote-message="Email already in use" data-parsley-debounce="500"/>
                    </div>

                    <div class="form-group">
                        <label for="question" class="label_">Security question * </label>
                        <select id="question" name="question" class="select_" data-parsley-required>
                            <option disabled selected>Select security question</option>
                            <?php
                            for ($i = 0; $i < count($data); $i++) {
                                echo '<option value = "' . $data[$i]['question'] . '">' . $data[$i]['question'] . '</option>';
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group" >
                        <label for="answer" class="label_">Security Answer * </label>
                        <input id="answer" name="answer" value="Spain"  type="text" class="input_" placeholder="Enter security answer"  data-parsley-required/>
                    </div>

                    <div class="form-group">
                        <label for="password" class="label_">Password * </label>
                        <input id="password" name="password_r" value="asas"  type="password" class="input_" placeholder="Enter password"  data-parsley-required/>
                    </div>

                    <div class="form-group">
                        <label for="password_confirm" class="label_">Confirm Password * </label>
                        <input id="password_confirm" name="password_confirm" value="asas"  type="password" class="input_" placeholder="Enter password again" data-parsley-equalto="#password" data-parsley-equalto-message="Password mismatch" data-parsley-required/>
                    </div>

                    <div class="form-group">
                        <label for="password_confirm">Verify Registration * </label>
                        <div class="radio-s">
                            <input id="r2" name="verify" value="sms" type="radio" checked/>
                            <label for="r2" style="font-size: 16px">By SMS</label>
                        </div>
                        <div class="radio-s">
                            <input id="r1" name="verify" value="email" type="radio" />
                            <label for="r1" style="font-size: 16px">By E-mail</label>
                        </div>

                    </div>
                    <div id="license" style="margin-bottom: 20px;">By clicking Sign up, you agree to our<a href="#"> Terms of Use</a> and <a href="#"> Privacy statement </a></div>
                    <div class="form-group">
                        <div class="row">
                            <button id="formSignup" type="submit" class="btn btn-success signinbtn"  name="btn-register"><i class="fa fa-lock"></i> SIGN UP</button>
                        </div>
                    </div>
                </form>

            </div>
            <p class="text text-center">Already registered? <a href="login.php" style="color: #000000; font-weight: bold;">Sign In</a></p>

        </div>

        <script src="js/jquery-3.1.1.js" type="text/javascript"></script>
        <script src="js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="js/parsley.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="js/select2.min.js" type="text/javascript"></script>

        <script type="text/javascript">

            $("#dob").datepicker({
                format: "dd/mm/yyyy",
                clearBtn: true,
                orientation: "auto right",
                autoclose: true
            });

            $('#regForm').parsley();
            $('#dob').parsley().on('field:error', function () {
                $('#dob').next().remove();
                $('#dob-group').append('<ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">This value is required.</li></ul>');
            });

            $('#dob').parsley().on('field:success', function () {
                $('#dob-group > ul').remove();
            });
        </script>

        <!-- Login and Registration Form -->
        <script type="text/javascript">
            $(document).ready(function () {
                $('#regForm').parsley();
                $('.js-example-basic-single').select2();

                $('body').on('submit', '#regForm', (function (event) {
                    $('#formSignup').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
                    $('.alert').css("display", "none");
                    $('#info').empty();

                    var formData = {
                        'fname': $('input[name=fname]').val(),
                        'midname': $('input[name=midname]').val(),
                        'lname': $('input[name=lname]').val(),
                        'gender': $("#gender option:selected").val(),
                        'dob': $('input[name=dob]').val(),
                        'phone': $('input[name=phone]').val(),
                        'email': $('input[name=email]').val(),
                        'address': $('input[name=address]').val(),
                        'local_gov': $('input[name=local_gov]').val(),
                        'question': $("#question option:selected").text(),
                        'answer': $('input[name=answer]').val(),
                        'password': $('input[name=password_r]').val(),
                        'method': $('input[name=verify]:checked').val()
                    };
                    $.post('./php/suracommands.php?command=register_user', formData, function (data) {
                        data = JSON.parse(data);

                        $('#formSignup').html('<i class="fa fa-lock btn-icon-left"></i> SIGN UP');
                        if (data['status']) {
                            if (data['msg'] === 'EMAIL_VERIFY') {
                                window.location.href = "php/checkmail_u.php";
                            }

                            if (data['msg'] === 'OTP_VERIFY') {
                                window.location.href = "php/veripage.php";
                            }
                        } else {
                            $('.alert').css("display", "block");
                            if (data['err'] === 'VALIDATION_ERROR') {
                                var obj = data['errVal']['field'];
                                var error = data['errVal']['message'];
                                $('#info').append(error);
                                $('#' + obj).addClass('parsley-error');
                                $("html, body, #left").animate({scrollTop: 0}, "fast");
                            } else if (data['err'] === 'DUPLICATE_ERROR') {
                                $('#info').append(data['errColumn'] + ' already in use');
                                $('#regForm #' + data['errColumn'].toString().toLowerCase()).addClass('parsley-error');
                                $("html, body, #left").animate({scrollTop: 0}, "fast");
                            } else if (data['err'] === 'REGISTRATION_ERROR') {
                                $('#info').append('Registration Failed. Please try again...');
                                $("html, body, #left").animate({scrollTop: 0}, "fast");
                            } else if (data['err'] === 'DATABASE_CONNECT_ERROR') {
                                $('#info').append('Internal error occurred');
                                $("html, body, #left").animate({scrollTop: 0}, "fast");
                            }
                        }
                    });
                    event.preventDefault();
                }));

            });
        </script>
    </body>
</html>