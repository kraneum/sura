<?php
require './header.php';
?>

<div id="home-main" class="container">
    <div id="transaction-main" class="transaction " >
        <div style="margin-bottom: 30px;">
            <h4><strong><i class="fa fa-exchange"></i> View all transactions</strong></h4>
            <small>&emsp;&emsp;This shows a list of iPoint transactions performed</small>
        </div>

        <div id="content" >

            <div id="table" style="display: none; ">
                <div class="table-responsive">
                    <table class="table">

                        <thead>
                            <tr style="font-weight: bolder;">
                                <th class="text-center">Date</th>
                                <th class="text-center">Company/Individual</th>
                                <th class="text-center">Credit/Debit</th>
                                <th class="text-center">iPoint Balance</th>
                            </tr>
                        </thead>

                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div id="pagination" class="center-block"></div>s
            </div>
        </div>
    </div>
</div>

<script src="js/jquery-3.1.1.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
<script src="js/jquery.simplePagination.js" type="text/javascript"></script>
<script src="js/spin.min.js" type="text/javascript"></script>
<script src="js/spinner-config.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
<?php
include 'footer.php';
?>

